package com.portfolio.platform.uirenew.alarm;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.format.DateFormat;
import android.util.SparseIntArray;
import com.fossil.e23;
import com.fossil.er4;
import com.fossil.ew4;
import com.fossil.g93;
import com.fossil.h93;
import com.fossil.i93;
import com.fossil.jr4;
import com.fossil.sz2;
import com.fossil.wp4;
import com.fossil.zt4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmPresenter extends g93 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a((er4) null);
    @DexIgnore
    public String e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public SparseIntArray h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public MFUser j;
    @DexIgnore
    public String k;
    @DexIgnore
    public /* final */ h93 l;
    @DexIgnore
    public /* final */ String m;
    @DexIgnore
    public /* final */ ArrayList<Alarm> n;
    @DexIgnore
    public /* final */ Alarm o;
    @DexIgnore
    public /* final */ SetAlarms p;
    @DexIgnore
    public /* final */ AlarmHelper q;
    @DexIgnore
    public /* final */ DeleteAlarm r;
    @DexIgnore
    public /* final */ UserRepository s;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return AlarmPresenter.t;
        }

        @DexIgnore
        public /* synthetic */ a(er4 er4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<DeleteAlarm.d, DeleteAlarm.b> {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmPresenter a;

        @DexIgnore
        public b(AlarmPresenter alarmPresenter) {
            this.a = alarmPresenter;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* renamed from: a */
        public void onSuccess(DeleteAlarm.d dVar) {
            jr4.b(dVar, "responseValue");
            AlarmHelper c = this.a.q;
            Context applicationContext = PortfolioApp.Z.c().getApplicationContext();
            jr4.a(applicationContext, "PortfolioApp.instance.applicationContext");
            c.b(applicationContext);
            this.a.l.a();
            this.a.l.x();
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public void a(DeleteAlarm.b bVar) {
            jr4.b(bVar, "errorValue");
            AlarmHelper c = this.a.q;
            Context applicationContext = PortfolioApp.Z.c().getApplicationContext();
            jr4.a(applicationContext, "PortfolioApp.instance.applicationContext");
            c.b(applicationContext);
            this.a.l.a();
            int b = bVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = AlarmPresenter.u.a();
            local.d(a2, "deleteAlarm() - deleteAlarm - onError - lastErrorCode = " + b);
            if (b != 1101) {
                if (b == 8888) {
                    this.a.l.c();
                    return;
                } else if (!(b == 1112 || b == 1113)) {
                    this.a.l.T();
                    return;
                }
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.a());
            jr4.a(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            h93 m = this.a.l;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                m.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<SetAlarms.d, SetAlarms.b> {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmPresenter a;

        @DexIgnore
        public c(AlarmPresenter alarmPresenter) {
            this.a = alarmPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetAlarms.d dVar) {
            jr4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(AlarmPresenter.u.a(), "saveAlarm SetAlarms onSuccess");
            this.a.l.a();
            Alarm b = this.a.o;
            if (b == null) {
                b = dVar.a();
            }
            this.a.l.a(b, true);
            this.a.j();
        }

        @DexIgnore
        public void a(SetAlarms.b bVar) {
            jr4.b(bVar, "errorValue");
            this.a.k = bVar.a().getUri();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = AlarmPresenter.u.a();
            local.d(a2, "saveAlarm SetAlarms onError - uri: " + this.a.k);
            this.a.l.a();
            int c = bVar.c();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = AlarmPresenter.u.a();
            local2.d(a3, "saveAlarm - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.l.c();
                    return;
                } else if (!(c == 1112 || c == 1113)) {
                    if (this.a.o == null) {
                        this.a.l.a(bVar.a(), false);
                        return;
                    } else {
                        this.a.l.T();
                        return;
                    }
                }
            }
            List<PermissionCodes> convertBLEPermissionErrorCode = PermissionCodes.convertBLEPermissionErrorCode(bVar.b());
            jr4.a(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            h93 m = this.a.l;
            Object[] array = convertBLEPermissionErrorCode.toArray(new PermissionCodes[0]);
            if (array != null) {
                PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                m.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        String simpleName = AlarmPresenter.class.getSimpleName();
        jr4.a((Object) simpleName, "AlarmPresenter::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public AlarmPresenter(h93 h93, String str, ArrayList<Alarm> arrayList, Alarm alarm, SetAlarms setAlarms, AlarmHelper alarmHelper, DeleteAlarm deleteAlarm, UserRepository userRepository) {
        jr4.b(h93, "mView");
        jr4.b(str, "mDeviceId");
        jr4.b(arrayList, "mAlarms");
        jr4.b(setAlarms, "mSetAlarms");
        jr4.b(alarmHelper, "mAlarmHelper");
        jr4.b(deleteAlarm, "mDeleteAlarm");
        jr4.b(userRepository, "mUserRepository");
        this.l = h93;
        this.m = str;
        this.n = arrayList;
        this.o = alarm;
        this.p = setAlarms;
        this.q = alarmHelper;
        this.r = deleteAlarm;
        this.s = userRepository;
    }

    @DexIgnore
    public void f() {
        throw null;
        // ew4 unused = zt4.b(e(), (CoroutineContext) null, (CoroutineStart) null, new AlarmPresenter$start$Anon1(this, (wp4) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(t, "stop");
        this.p.g();
        this.r.g();
        PortfolioApp.Z.c(this);
    }

    @DexIgnore
    public void h() {
        this.l.b();
        DeleteAlarm deleteAlarm = this.r;
        String str = this.m;
        ArrayList<Alarm> arrayList = this.n;
        Alarm alarm = this.o;
        if (alarm != null) {
            deleteAlarm.a(new DeleteAlarm.c(str, arrayList, alarm), new b(this));
        } else {
            jr4.a();
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        e23 e23 = com.fossil.e23.d;
        h93 h93 = this.l;
        if (h93 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmFragment");
        } else if (e23.a(e23, ((i93) h93).getContext(), "SET_ALARMS", false, 4, (Object) null)) {
            this.l.b();
            Calendar instance = Calendar.getInstance();
            jr4.a(instance, "Calendar.getInstance()");
            String t2 = sz2.t(instance.getTime());
            String str = this.k;
            if (str == null) {
                StringBuilder sb = new StringBuilder();
                MFUser mFUser = this.j;
                sb.append(mFUser != null ? mFUser.getUserId() : null);
                sb.append(':');
                Calendar instance2 = Calendar.getInstance();
                jr4.a(instance2, "Calendar.getInstance()");
                sb.append(instance2.getTimeInMillis());
                str = sb.toString();
            }
            String str2 = str;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = t;
            local.d(str3, "saveAlarm - uri: " + str2);
            int[] a2 = this.g ? a(this.h) : new int[0];
            String str4 = this.e;
            if (str4 != null) {
                int i2 = this.f;
                int i3 = i2 / 60;
                int i4 = i2 % 60;
                if (a2 != null) {
                    boolean z = this.g;
                    jr4.a((Object) t2, "createdAt");
                    Alarm alarm = new Alarm((String) null, str2, str4, i3, i4, a2, true, z, t2, t2, 0, 1024, (er4) null);
                    Alarm alarm2 = this.o;
                    if (alarm2 != null) {
                        alarm2.setActive(this.i);
                        Alarm alarm3 = this.o;
                        String str5 = this.e;
                        if (str5 != null) {
                            alarm3.setTitle(str5);
                            this.o.setTotalMinutes(this.f);
                            this.o.setRepeated(this.g);
                            this.o.setDays(a2);
                            Iterator<Alarm> it = this.n.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                Alarm next = it.next();
                                if (jr4.a((Object) next.getUri(), (Object) this.o.getUri())) {
                                    ArrayList<Alarm> arrayList = this.n;
                                    arrayList.set(arrayList.indexOf(next), this.o);
                                    break;
                                }
                            }
                        } else {
                            jr4.a();
                            throw null;
                        }
                    } else {
                        this.n.add(alarm);
                    }
                    FLogger.INSTANCE.getLocal().d(t, "saveAlarm SetAlarms");
                    SetAlarms setAlarms = this.p;
                    String str6 = this.m;
                    ArrayList<Alarm> arrayList2 = this.n;
                    Alarm alarm4 = this.o;
                    if (alarm4 != null) {
                        alarm = alarm4;
                    }
                    setAlarms.a(new SetAlarms.c(str6, arrayList2, alarm), new c(this));
                    return;
                }
                jr4.a();
                throw null;
            }
            jr4.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void j() {
        FLogger.INSTANCE.getLocal().d(t, "onSetAlarmsSuccess()");
        this.q.b(PortfolioApp.Z.c());
        PortfolioApp.Z.c().l(this.m);
    }

    @DexIgnore
    @SuppressLint("WrongConstant")
    public final void k() {
         int i2 = Calendar.getInstance().get(10);
        int i3 = Calendar.getInstance().get(12);
        boolean z = true;
        if (Calendar.getInstance().get(9) != 1) {
            z = false;
        }
        b(String.valueOf(i2), String.valueOf(i3), z);
    }

    @DexIgnore
    public void l() {
        this.l.a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    public final void b(String str, String str2, boolean z) {
        int i2;
        int i3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = t;
        local.d(str3, "updateTime: hourValue = " + str + ", minuteValue = " + str2 + ", isPM = " + z);
        int i4 = 0;
        try {
            Integer valueOf = Integer.valueOf(str);
            jr4.a(valueOf, "Integer.valueOf(hourValue)");
            i3 = valueOf.intValue();
            try {
                Integer valueOf2 = Integer.valueOf(str2);
                jr4.a(valueOf2, "Integer.valueOf(minuteValue)");
                i2 = valueOf2.intValue();
            } catch (Exception e2) {
                // e = e2;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = t;
                local2.e(str4, "Exception when parse time e=" + e2);
                i2 = 0;
                if (DateFormat.is24HourFormat(PortfolioApp.Z.c())) {
                }
            }
        } catch (Exception e3) {
            // e = e3;
            i3 = 0;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            String str42 = t;
            local22.e(str42, "Exception when parse time e=" + e3);
            i2 = 0;
            if (DateFormat.is24HourFormat(PortfolioApp.Z.c())) {
            }
        }
        if (DateFormat.is24HourFormat(PortfolioApp.Z.c())) {
            if (z) {
                i4 = i3 == 12 ? 12 : i3 + 12;
            } else if (i3 != 12) {
                i4 = i3;
            }
            this.f = (i4 * 60) + i2;
            return;
        }
        if (z) {
            i4 = i3 == 12 ? 12 : i3 + 12;
        } else if (i3 != 12) {
            i4 = i3;
        }
        this.f = (i4 * 60) + i2;
    }

    @DexIgnore
    public void a(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "updateDaysRepeat: isStateEnabled = " + z + ", day = " + i2);
        if (z) {
            SparseIntArray sparseIntArray = this.h;
            if (sparseIntArray != null) {
                sparseIntArray.put(i2, i2);
            } else {
                jr4.a();
                throw null;
            }
        } else {
            SparseIntArray sparseIntArray2 = this.h;
            if (sparseIntArray2 != null) {
                sparseIntArray2.delete(i2);
            } else {
                jr4.a();
                throw null;
            }
        }
        Alarm alarm = this.o;
        if (alarm != null) {
            boolean z2 = true;
            if (alarm.getDays() != null) {
                SparseIntArray sparseIntArray3 = this.h;
                if (sparseIntArray3 != null) {
                    int size = sparseIntArray3.size();
                    int[] days = this.o.getDays();
                    if (days != null) {
                        if (size == days.length) {
                            int[] days2 = this.o.getDays();
                            if (days2 != null) {
                                int length = days2.length;
                                int i3 = 0;
                                while (true) {
                                    if (i3 >= length) {
                                        z2 = false;
                                        break;
                                    }
                                    int i4 = days2[i3];
                                    SparseIntArray sparseIntArray4 = this.h;
                                    if (sparseIntArray4 == null) {
                                        jr4.a();
                                        throw null;
                                    } else if (sparseIntArray4.indexOfKey(i4) < 0) {
                                        break;
                                    } else {
                                        i3++;
                                    }
                                }
                            } else {
                                jr4.a();
                                throw null;
                            }
                        }
                        this.l.d(z2);
                    } else {
                        jr4.a();
                        throw null;
                    }
                } else {
                    jr4.a();
                    throw null;
                }
            } else {
                SparseIntArray sparseIntArray5 = this.h;
                if (sparseIntArray5 == null) {
                    jr4.a();
                    throw null;
                } else if (sparseIntArray5.size() > 0) {
                    this.l.d(true);
                } else {
                    this.l.d(false);
                }
            }
        }
        SparseIntArray sparseIntArray6 = this.h;
        if (sparseIntArray6 == null) {
            jr4.a();
            throw null;
        } else if (sparseIntArray6.size() == 0) {
            this.g = false;
            this.l.k(false);
        }
    }

    @DexIgnore
    public void a(boolean z) {
        this.g = z;
        Alarm alarm = this.o;
        if (alarm != null) {
            if (this.g != alarm.isRepeated()) {
                this.l.d(true);
            } else {
                this.l.d(false);
            }
        }
        if (this.g) {
            SparseIntArray sparseIntArray = this.h;
            if (sparseIntArray == null) {
                jr4.a();
                throw null;
            } else if (sparseIntArray.size() == 0) {
                this.h = a(new int[]{2, 3, 4, 5, 6, 7, 1});
                h93 h93 = this.l;
                SparseIntArray sparseIntArray2 = this.h;
                if (sparseIntArray2 != null) {
                    h93.a(sparseIntArray2);
                } else {
                    jr4.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        jr4.b(str, "hourValue");
        jr4.b(str2, "minuteValue");
        b(str, str2, z);
        Alarm alarm = this.o;
        if (alarm == null) {
            return;
        }
        if (this.f != alarm.getTotalMinutes()) {
            this.l.d(true);
        } else {
            this.l.d(false);
        }
    }

    @DexIgnore
    public final SparseIntArray a(int[] iArr) {
        int i2 = 0;
        int length = iArr != null ? iArr.length : 0;
        if (length <= 0) {
            return new SparseIntArray();
        }
        SparseIntArray sparseIntArray = new SparseIntArray();
        while (i2 < length) {
            if (iArr != null) {
                int i3 = iArr[i2];
                sparseIntArray.put(i3, i3);
                i2++;
            } else {
                jr4.a();
                throw null;
            }
        }
        return sparseIntArray;
    }

    @DexIgnore
    public final int[] a(SparseIntArray sparseIntArray) {
        int size = sparseIntArray != null ? sparseIntArray.size() : 0;
        if (size <= 0) {
            return null;
        }
        int i2 = size;
        int i3 = 0;
        while (i3 < i2) {
            if (sparseIntArray != null) {
                if (sparseIntArray.keyAt(i3) != sparseIntArray.valueAt(i3)) {
                    sparseIntArray.removeAt(i3);
                    i2--;
                    i3--;
                }
                i3++;
            } else {
                jr4.a();
                throw null;
            }
        }
        if (sparseIntArray != null) {
            int size2 = sparseIntArray.size();
            if (size2 <= 0) {
                return null;
            }
            int[] iArr = new int[size2];
            for (int i4 = 0; i4 < size2; i4++) {
                iArr[i4] = sparseIntArray.valueAt(i4);
            }
            return iArr;
        }
        jr4.a();
        throw null;
    }
}
