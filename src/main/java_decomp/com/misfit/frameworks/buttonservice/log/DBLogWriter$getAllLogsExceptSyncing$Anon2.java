package com.misfit.frameworks.buttonservice.log;

import com.fossil.aj6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xg6;
import com.fossil.yd6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$getAllLogsExceptSyncing$Anon2 extends sf6 implements ig6<il6, xe6<? super List<LogEvent>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends xg6 implements hg6<Log, LogEvent> {
        @DexIgnore
        public /* final */ /* synthetic */ Gson $gson;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(Gson gson) {
            super(1);
            this.$gson = gson;
        }

        @DexIgnore
        public final LogEvent invoke(Log log) {
            wg6.b(log, "it");
            try {
                LogEvent logEvent = (LogEvent) this.$gson.a(log.getContent(), LogEvent.class);
                logEvent.setTag(Integer.valueOf(log.getId()));
                return logEvent;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = DBLogWriter.TAG;
                local.e(access$getTAG$cp, ", getAllLogs(), ex: " + e.getMessage());
                return null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$getAllLogsExceptSyncing$Anon2(DBLogWriter dBLogWriter, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dBLogWriter;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        DBLogWriter$getAllLogsExceptSyncing$Anon2 dBLogWriter$getAllLogsExceptSyncing$Anon2 = new DBLogWriter$getAllLogsExceptSyncing$Anon2(this.this$0, xe6);
        dBLogWriter$getAllLogsExceptSyncing$Anon2.p$ = (il6) obj;
        return dBLogWriter$getAllLogsExceptSyncing$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DBLogWriter$getAllLogsExceptSyncing$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return aj6.g(aj6.d(yd6.b(this.this$0.logDao.getAllLogEventsExcept(Log.Flag.SYNCING)), new Anon1_Level2(new Gson())));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
