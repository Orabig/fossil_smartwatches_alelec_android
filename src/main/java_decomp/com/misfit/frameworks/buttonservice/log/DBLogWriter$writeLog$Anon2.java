package com.misfit.frameworks.buttonservice.log;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import com.misfit.frameworks.buttonservice.log.db.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$writeLog$Anon2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$writeLog$Anon2(DBLogWriter dBLogWriter, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dBLogWriter;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        DBLogWriter$writeLog$Anon2 dBLogWriter$writeLog$Anon2 = new DBLogWriter$writeLog$Anon2(this.this$0, xe6);
        dBLogWriter$writeLog$Anon2.p$ = (il6) obj;
        return dBLogWriter$writeLog$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DBLogWriter$writeLog$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        DBLogWriter.IDBLogWriterCallback access$getCallback$p;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            int countExcept = this.this$0.logDao.countExcept(Log.Flag.SYNCING);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = DBLogWriter.TAG;
            local.d(access$getTAG$cp, ".writeLog(), dbCount=" + countExcept);
            if (countExcept >= this.this$0.thresholdValue && (access$getCallback$p = this.this$0.callback) != null) {
                access$getCallback$p.onReachDBThreshold();
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
