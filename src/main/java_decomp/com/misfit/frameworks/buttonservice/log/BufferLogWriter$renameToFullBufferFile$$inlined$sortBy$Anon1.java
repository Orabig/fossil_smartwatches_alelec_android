package com.misfit.frameworks.buttonservice.log;

import com.fossil.kj6;
import com.fossil.mj6;
import com.fossil.ue6;
import com.fossil.wg6;
import java.io.File;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$renameToFullBufferFile$$inlined$sortBy$Anon1<T> implements Comparator<T> {
    @DexIgnore
    public final int compare(T t, T t2) {
        File file = (File) t;
        mj6 mj6 = new mj6("\\d+");
        wg6.a((Object) file, "it");
        String name = file.getName();
        wg6.a((Object) name, "it.name");
        String str = null;
        kj6 find$default = mj6.find$default(mj6, name, 0, 2, (Object) null);
        String value = find$default != null ? find$default.getValue() : null;
        File file2 = (File) t2;
        mj6 mj62 = new mj6("\\d+");
        wg6.a((Object) file2, "it");
        String name2 = file2.getName();
        wg6.a((Object) name2, "it.name");
        kj6 find$default2 = mj6.find$default(mj62, name2, 0, 2, (Object) null);
        if (find$default2 != null) {
            str = find$default2.getValue();
        }
        return ue6.a(value, str);
    }
}
