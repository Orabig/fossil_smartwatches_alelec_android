package com.misfit.frameworks.buttonservice.log;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$mMessageBroadcastReceiver$1$onReceive$4", f = "RemoteFLogger.kt", l = {95}, m = "invokeSuspend")
public final class RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger$mMessageBroadcastReceiver$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2(RemoteFLogger$mMessageBroadcastReceiver$Anon1 remoteFLogger$mMessageBroadcastReceiver$Anon1, xe6 xe6) {
        super(2, xe6);
        this.this$0 = remoteFLogger$mMessageBroadcastReceiver$Anon1;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2 remoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2 = new RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2(this.this$0, xe6);
        remoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2.p$ = (il6) obj;
        return remoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            RemoteFLogger remoteFLogger = this.this$0.this$0;
            this.L$0 = il6;
            this.label = 1;
            if (remoteFLogger.flushBuffer(this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
