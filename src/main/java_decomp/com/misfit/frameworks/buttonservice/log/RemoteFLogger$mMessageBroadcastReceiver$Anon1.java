package com.misfit.frameworks.buttonservice.log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.kc6;
import com.fossil.ll6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoteFLogger$mMessageBroadcastReceiver$Anon1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$0;

    @DexIgnore
    public RemoteFLogger$mMessageBroadcastReceiver$Anon1(RemoteFLogger remoteFLogger) {
        this.this$0 = remoteFLogger;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0089, code lost:
        if ((!com.fossil.wg6.a((java.lang.Object) r6.this$0.floggerName, (java.lang.Object) r1)) != false) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00b2, code lost:
        if ((!com.fossil.wg6.a((java.lang.Object) r6.this$0.floggerName, (java.lang.Object) r1)) != false) goto L_0x00b4;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b7 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    public void onReceive(Context context, Intent intent) {
        String str;
        String stringExtra;
        BufferLogWriter access$getBufferLogWriter$p;
        RemoteFLogger.SessionSummary sessionSummary;
        wg6.b(context, "context");
        wg6.b(intent, "intent");
        String stringExtra2 = intent.getStringExtra("action");
        RemoteFLogger.MessageTarget fromValue = RemoteFLogger.MessageTarget.Companion.fromValue(intent.getIntExtra(RemoteFLogger.MESSAGE_TARGET_KEY, RemoteFLogger.MessageTarget.TO_ALL_FLOGGER.getValue()));
        if (intent.hasExtra(RemoteFLogger.MESSAGE_SENDER_KEY)) {
            str = intent.getStringExtra(RemoteFLogger.MESSAGE_SENDER_KEY);
            wg6.a((Object) str, "intent.getStringExtra(MESSAGE_SENDER_KEY)");
        } else {
            str = "";
        }
        FLogger.INSTANCE.getLocal().d(RemoteFLogger.TAG, "mMessageBroadcastReceiver.onReceive(), logAction=" + stringExtra2 + ", target=" + fromValue + ", sender=" + str);
        int i = RemoteFLogger.WhenMappings.$EnumSwitchMapping$0[fromValue.ordinal()];
        boolean z = false;
        if (i != 1) {
            if (i == 2) {
                z = !wg6.a((Object) this.this$0.floggerName, (Object) str);
            } else if (i != 3) {
                throw new kc6();
            } else if (!this.this$0.isMainFLogger) {
            }
            if (!z && stringExtra2 != null) {
                switch (stringExtra2.hashCode()) {
                    case -1960182802:
                        if (stringExtra2.equals(RemoteFLogger.MESSAGE_ACTION_END_SESSION) && (stringExtra = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_SUMMARY_KEY)) != null) {
                            RemoteFLogger.SessionSummary sessionSummary2 = (RemoteFLogger.SessionSummary) this.this$0.summarySessionMap.remove(stringExtra);
                            return;
                        }
                        return;
                    case -1885432660:
                        if (stringExtra2.equals(RemoteFLogger.MESSAGE_ACTION_FULL_BUFFER) && (access$getBufferLogWriter$p = this.this$0.bufferLogWriter) != null) {
                            access$getBufferLogWriter$p.forceFlushBuffer();
                            return;
                        }
                        return;
                    case 308052341:
                        if (stringExtra2.equals(RemoteFLogger.MESSAGE_ACTION_START_SESSION)) {
                            String stringExtra3 = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_SUMMARY_KEY);
                            String stringExtra4 = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_SERIAL);
                            if (stringExtra3 != null) {
                                RemoteFLogger remoteFLogger = this.this$0;
                                wg6.a((Object) stringExtra4, "serial");
                                remoteFLogger.startSession(stringExtra3, stringExtra4);
                                return;
                            }
                            return;
                        }
                        return;
                    case 1513386699:
                        if (stringExtra2.equals(RemoteFLogger.MESSAGE_ACTION_ERROR_RECORDED)) {
                            String stringExtra5 = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_SUMMARY_KEY);
                            String stringExtra6 = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_ERROR_CODE);
                            if (stringExtra5 != null && stringExtra6 != null && (sessionSummary = (RemoteFLogger.SessionSummary) this.this$0.summarySessionMap.get(stringExtra5)) != null) {
                                sessionSummary.getErrors().add(stringExtra6);
                                return;
                            }
                            return;
                        }
                        return;
                    case 1920521161:
                        if (stringExtra2.equals(RemoteFLogger.MESSAGE_ACTION_FLUSH)) {
                            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2(this, (xe6) null), 3, (Object) null);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            } else {
                return;
            }
        } else {
            if (this.this$0.isMainFLogger) {
            }
            if (!z) {
                return;
            }
            return;
        }
        z = true;
        if (!z) {
        }
    }
}
