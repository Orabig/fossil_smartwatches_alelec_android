package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.ji;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.log.db.LogDatabase$init$1", f = "LogDatabase.kt", l = {}, m = "invokeSuspend")
public final class LogDatabase$init$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LogDatabase this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ LogDatabase$init$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(LogDatabase$init$Anon1 logDatabase$init$Anon1) {
            this.this$0 = logDatabase$init$Anon1;
        }

        @DexIgnore
        public final void run() {
            ji openHelper = this.this$0.this$0.getOpenHelper();
            wg6.a((Object) openHelper, "openHelper");
            openHelper.a().b("CREATE TRIGGER IF NOT EXISTS delete_keep_2000 after insert on log WHEN (select count(*) from log) >= 2000 BEGIN DELETE FROM log WHERE id NOT IN  (SELECT id FROM log ORDER BY timeStamp desc limit 2000); END");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LogDatabase$init$Anon1(LogDatabase logDatabase, xe6 xe6) {
        super(2, xe6);
        this.this$0 = logDatabase;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        LogDatabase$init$Anon1 logDatabase$init$Anon1 = new LogDatabase$init$Anon1(this.this$0, xe6);
        logDatabase$init$Anon1.p$ = (il6) obj;
        return logDatabase$init$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((LogDatabase$init$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.runInTransaction(new Anon1_Level2(this));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
