package com.misfit.frameworks.buttonservice.communite.ble;

import com.misfit.frameworks.buttonservice.model.FirmwareData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface IPairDeviceSession {
    @DexIgnore
    void onAuthorizeDevice(long j);

    @DexIgnore
    void onLinkServerSuccess(boolean z, int i);

    @DexIgnore
    void updateFirmware(FirmwareData firmwareData);
}
