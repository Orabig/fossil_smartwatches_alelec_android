package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.cd6;
import com.fossil.hg6;
import com.fossil.wg6;
import com.fossil.xg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon1 extends xg6 implements hg6<cd6, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, ISessionSdkCallback iSessionSdkCallback, FLogger.Session session) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$callback$inlined = iSessionSdkCallback;
        this.$logSession$inlined = session;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((cd6) obj);
        return cd6.a;
    }

    @DexIgnore
    public final void invoke(cd6 cd6) {
        wg6.b(cd6, "it");
        this.$callback$inlined.onAuthorizeDeviceSuccess();
    }
}
