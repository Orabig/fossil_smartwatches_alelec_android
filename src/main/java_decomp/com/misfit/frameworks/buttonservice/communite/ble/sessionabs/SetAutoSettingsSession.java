package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SetAutoSettingsSession extends EnableMaintainingSession {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoSettingsSession(CommunicateMode communicateMode, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.DEVICE_SETTING, communicateMode, bleAdapterImpl, bleSessionCallback);
        wg6.b(communicateMode, "communicateMode");
        wg6.b(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    public abstract BleState getStartState();

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return getStartState();
    }
}
