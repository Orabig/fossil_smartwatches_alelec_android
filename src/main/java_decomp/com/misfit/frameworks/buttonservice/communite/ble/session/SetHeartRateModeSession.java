package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetHeartRateModeSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ HeartRateMode heartRateMode;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetHeartRateModeState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetHeartRateModeState() {
            super(SetHeartRateModeSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            if (SetHeartRateModeSession.this.heartRateMode != HeartRateMode.NONE) {
                SetHeartRateModeSession setHeartRateModeSession = SetHeartRateModeSession.this;
                setHeartRateModeSession.log("Set Heart Rate Mode: " + SetHeartRateModeSession.this.heartRateMode);
                b70 b70 = new b70();
                b70.a(SetHeartRateModeSession.this.heartRateMode.toSDKHeartRateMode());
                this.task = SetHeartRateModeSession.this.getBleAdapter().setDeviceConfig(SetHeartRateModeSession.this.getLogSession(), b70.a(), this);
                if (this.task == null) {
                    SetHeartRateModeSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetHeartRateModeSession setHeartRateModeSession2 = SetHeartRateModeSession.this;
            setHeartRateModeSession2.log("Not Set Heart Rate Mode: " + SetHeartRateModeSession.this.heartRateMode);
            SetHeartRateModeSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            SetHeartRateModeSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetHeartRateModeSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetHeartRateModeSession(HeartRateMode heartRateMode2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_HEART_RATE_MODE, bleAdapterImpl, bleSessionCallback);
        wg6.b(heartRateMode2, "heartRateMode");
        wg6.b(bleAdapterImpl, "bleAdapterV2");
        this.heartRateMode = heartRateMode2;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetHeartRateModeSession setHeartRateModeSession = new SetHeartRateModeSession(this.heartRateMode, getBleAdapter(), getBleSessionCallback());
        setHeartRateModeSession.setDevice(getDevice());
        return setHeartRateModeSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_HEART_RATE_MODE_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_HEART_RATE_MODE_STATE;
        String name = SetHeartRateModeState.class.getName();
        wg6.a((Object) name, "SetHeartRateModeState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
