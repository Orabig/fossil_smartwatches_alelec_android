package com.misfit.frameworks.buttonservice.communite.ble;

import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ISetWatchParamStateSession {
    @DexIgnore
    void doNextState();

    @DexIgnore
    void onGetWatchParamFailed();

    @DexIgnore
    void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping);
}
