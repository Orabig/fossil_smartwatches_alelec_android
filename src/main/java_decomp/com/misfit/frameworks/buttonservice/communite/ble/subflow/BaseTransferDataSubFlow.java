package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.fitness.FitnessData;
import com.fossil.j60;
import com.fossil.k60;
import com.fossil.l60;
import com.fossil.m60;
import com.fossil.n60;
import com.fossil.nh6;
import com.fossil.o60;
import com.fossil.p60;
import com.fossil.q60;
import com.fossil.qd6;
import com.fossil.r60;
import com.fossil.rc6;
import com.fossil.s60;
import com.fossil.vd6;
import com.fossil.wg6;
import com.fossil.yb0;
import com.fossil.zb0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.db.DataCollectorHelper;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFSyncLog;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseTransferDataSubFlow extends SubFlow {
    @DexIgnore
    public int activeMinute;
    @DexIgnore
    public int activeMinuteGoal;
    @DexIgnore
    public int awakeMin;
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public long calorie;
    @DexIgnore
    public long calorieGoal;
    @DexIgnore
    public long dailyStepInfo;
    @DexIgnore
    public int deepMin;
    @DexIgnore
    public long distanceInCentimeter;
    @DexIgnore
    public /* final */ boolean isClearDataOptional;
    @DexIgnore
    public boolean isNewDevice;
    @DexIgnore
    public int lightMin;
    @DexIgnore
    public List<FitnessData> mSyncData;
    @DexIgnore
    public /* final */ MFLog mflog;
    @DexIgnore
    public long realTimeSteps;
    @DexIgnore
    public long realTimeStepsInfo;
    @DexIgnore
    public long stepGoal;
    @DexIgnore
    public MFSyncLog syncLog;
    @DexIgnore
    public long syncTime;
    @DexIgnore
    public int totalSleepInMin;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class EraseDataFileState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public EraseDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = BaseTransferDataSubFlow.this.getBleAdapter().eraseDataFile(BaseTransferDataSubFlow.this.getLogSession(), this);
            if (this.task == null) {
                BaseTransferDataSubFlow.this.stopSubFlow(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onEraseDataFilesFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (BaseTransferDataSubFlow.this.isClearDataOptional()) {
                BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                BaseTransferDataSubFlow.this.stopSubFlow(0);
                return;
            }
            BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CLEAR_DATA);
        }

        @DexIgnore
        public void onEraseDataFilesSuccess() {
            stopTimeout();
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Erase DataFiles timeout. Cancel.");
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public zb0<HashMap<s60, r60>> task;

        @DexIgnore
        public GetDeviceConfigState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<s60, r60> hashMap) {
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            nh6 nh6 = nh6.a;
            Object[] objArr = {hashMap.get(s60.TIME), hashMap.get(s60.BATTERY), hashMap.get(s60.BIOMETRIC_PROFILE), hashMap.get(s60.DAILY_STEP), hashMap.get(s60.DAILY_STEP_GOAL), hashMap.get(s60.DAILY_CALORIE), hashMap.get(s60.DAILY_CALORIE_GOAL), hashMap.get(s60.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(s60.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(s60.DAILY_DISTANCE), hashMap.get(s60.DAILY_SLEEP), hashMap.get(s60.DAILY_DISTANCE), hashMap.get(s60.DAILY_DISTANCE), hashMap.get(s60.INACTIVE_NUDGE), hashMap.get(s60.VIBE_STRENGTH), hashMap.get(s60.DO_NOT_DISTURB_SCHEDULE)};
            String format = String.format("Get configuration  " + BaseTransferDataSubFlow.this.getSerial() + "\n" + "\t[timeConfiguration: \n" + "\ttime = %s,\n" + "\tbattery = %s,\n" + "\tbiometric = %s,\n" + "\tdaily steps = %s,\n" + "\tdaily step goal = %s, \n" + "\tdaily calorie: %s, \n" + "\tdaily calorie goal: %s, \n" + "\tdaily total active minute: %s, \n" + "\tdaily active minute goal: %s, \n" + "\tdaily distance: %s, \n" + "\tdaily sleep: %s, \n" + "\tinactive nudge: %s, \n" + "\tvibration strength: %s, \n" + "\tdo not disturb schedule: %s, \n" + "\t]", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            baseTransferDataSubFlow.log(format);
        }

        @DexIgnore
        private final void readConfig(HashMap<s60, r60> hashMap) {
            if (!BaseTransferDataSubFlow.this.getUserProfile().isNewDevice()) {
                p60 p60 = (r60) hashMap.get(s60.DAILY_STEP_GOAL);
                if (p60 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                    if (p60 != null) {
                        baseTransferDataSubFlow.stepGoal = p60.getStep();
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepGoalConfig");
                    }
                }
                o60 o60 = (r60) hashMap.get(s60.DAILY_STEP);
                if (o60 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                    if (o60 != null) {
                        baseTransferDataSubFlow2.realTimeSteps = o60.getStep();
                        BaseTransferDataSubFlow baseTransferDataSubFlow3 = BaseTransferDataSubFlow.this;
                        baseTransferDataSubFlow3.realTimeStepsInfo = baseTransferDataSubFlow3.realTimeSteps;
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepConfig");
                    }
                }
                j60 j60 = (r60) hashMap.get(s60.DAILY_ACTIVE_MINUTE_GOAL);
                if (j60 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow4 = BaseTransferDataSubFlow.this;
                    if (j60 != null) {
                        baseTransferDataSubFlow4.activeMinuteGoal = j60.getMinute();
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig");
                    }
                }
                q60 q60 = (r60) hashMap.get(s60.DAILY_TOTAL_ACTIVE_MINUTE);
                if (q60 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow5 = BaseTransferDataSubFlow.this;
                    if (q60 != null) {
                        baseTransferDataSubFlow5.activeMinute = q60.getMinute();
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig");
                    }
                }
                l60 l60 = (r60) hashMap.get(s60.DAILY_CALORIE_GOAL);
                if (l60 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow6 = BaseTransferDataSubFlow.this;
                    if (l60 != null) {
                        baseTransferDataSubFlow6.calorieGoal = l60.getCalorie();
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig");
                    }
                }
                k60 k60 = (r60) hashMap.get(s60.DAILY_CALORIE);
                if (k60 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow7 = BaseTransferDataSubFlow.this;
                    if (k60 != null) {
                        baseTransferDataSubFlow7.calorie = k60.getCalorie();
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieConfig");
                    }
                }
                m60 m60 = (r60) hashMap.get(s60.DAILY_DISTANCE);
                if (m60 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow8 = BaseTransferDataSubFlow.this;
                    if (m60 != null) {
                        baseTransferDataSubFlow8.distanceInCentimeter = m60.getCentimeter();
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyDistanceConfig");
                    }
                }
                r60 r60 = hashMap.get(s60.DAILY_SLEEP);
                if (r60 != null) {
                    if (!(r60 instanceof n60)) {
                        r60 = null;
                    }
                    n60 n60 = (n60) r60;
                    if (n60 != null) {
                        BaseTransferDataSubFlow.this.totalSleepInMin = n60.getTotalSleepInMinute();
                        BaseTransferDataSubFlow.this.awakeMin = n60.getAwakeInMinute();
                        BaseTransferDataSubFlow.this.lightMin = n60.getLightSleepInMinute();
                        BaseTransferDataSubFlow.this.deepMin = n60.getDeepSleepInMinute();
                        return;
                    }
                    return;
                }
                return;
            }
            BaseTransferDataSubFlow.this.log("We skip read real time steps cause by full sync");
        }

        @DexIgnore
        public boolean onEnter() {
            this.task = BaseTransferDataSubFlow.this.getBleAdapter().getDeviceConfig(BaseTransferDataSubFlow.this.getLogSession(), this);
            if (this.task != null) {
                startTimeout();
                return true;
            } else if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return true;
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
                return true;
            }
        }

        @DexIgnore
        public void onGetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_GET_CONFIG);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        public void onGetDeviceConfigSuccess(HashMap<s60, r60> hashMap) {
            wg6.b(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            readConfig(hashMap);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Get device configuration timeout. Cancel.");
            zb0<HashMap<s60, r60>> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PlayDeviceAnimationState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public PlayDeviceAnimationState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = BaseTransferDataSubFlow.this.getBleAdapter().playDeviceAnimation(BaseTransferDataSubFlow.this.getLogSession(), this);
            if (this.task == null) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onPlayDeviceAnimation(boolean z, bc0 bc0) {
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.addFailureCode(201);
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Play device animation timeout. Cancel.");
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ProcessAndStoreDataState extends BleStateAbs {
        @DexIgnore
        public ProcessAndStoreDataState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setTimeout(CommuteTimeService.y);
            BaseTransferDataSubFlow.this.log("Process and store data of device with serial " + BaseTransferDataSubFlow.this.getSerial());
        }

        @DexIgnore
        private final Bundle bundleData() {
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.SYNC_RESULT, BaseTransferDataSubFlow.this.getSyncTime());
            bundle.putLong(ButtonService.Companion.getREALTIME_STEPS(), BaseTransferDataSubFlow.this.realTimeSteps);
            return bundle;
        }

        @DexIgnore
        private final List<FitnessData> readDataFromCache() {
            ArrayList arrayList = new ArrayList();
            List<DataFile> allDataFiles = DataFileProvider.getInstance(BaseTransferDataSubFlow.this.getBleAdapter().getContext()).getAllDataFiles(BaseTransferDataSubFlow.this.getSyncTime(), BaseTransferDataSubFlow.this.getSerial());
            Gson gson = new Gson();
            if (allDataFiles.size() > 0) {
                wg6.a((Object) allDataFiles, "cacheData");
                long j = -1;
                int i = 0;
                for (DataFile dataFile : allDataFiles) {
                    try {
                        wg6.a((Object) dataFile, "it");
                        FitnessData fitnessData = (FitnessData) gson.a(dataFile.getDataFile(), FitnessData.class);
                        wg6.a((Object) fitnessData, "fitnessData");
                        arrayList.add(fitnessData);
                        if (dataFile.getSyncTime() != j) {
                            if (j != -1) {
                                BaseTransferDataSubFlow.this.log("Got cache data of " + j + ", fitness data size=" + i);
                            }
                            j = dataFile.getSyncTime();
                            i = 0;
                        }
                        i++;
                    } catch (Exception e) {
                        BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Got cache data of ");
                        wg6.a((Object) dataFile, "it");
                        sb.append(dataFile.getSyncTime());
                        sb.append(" exception=");
                        sb.append(e);
                        baseTransferDataSubFlow.log(sb.toString());
                    }
                }
                if (j != -1 && i > 0) {
                    BaseTransferDataSubFlow.this.log("Got cache data of " + j + ", fitness data size=" + i);
                }
            } else {
                BaseTransferDataSubFlow.this.log("No cache data.");
            }
            return arrayList;
        }

        @DexIgnore
        private final void saveAsCache(List<FitnessData> list) {
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("Save latest sync data as cache, sync time=" + BaseTransferDataSubFlow.this.getSyncTime() + ", fitness data size=" + list.size());
            int i = 0;
            for (T next : list) {
                int i2 = i + 1;
                if (i >= 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(BaseTransferDataSubFlow.this.getSyncTime());
                    sb.append('_');
                    sb.append(i);
                    DataCollectorHelper.saveDataFile(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), new DataFile(sb.toString(), new Gson().a((FitnessData) next), BaseTransferDataSubFlow.this.getSerial(), BaseTransferDataSubFlow.this.getSyncTime()));
                    i = i2;
                } else {
                    qd6.c();
                    throw null;
                }
            }
        }

        @DexIgnore
        public boolean onEnter() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onEnter ProcessAndStoreDataState");
            List<FitnessData> readDataFromCache = readDataFromCache();
            saveAsCache(BaseTransferDataSubFlow.this.mSyncData);
            readDataFromCache.addAll(BaseTransferDataSubFlow.this.mSyncData);
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("set syncdata from cache data with size " + readDataFromCache.size());
            BaseTransferDataSubFlow.this.mSyncData = readDataFromCache;
            startTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseTransferDataSubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                bleSessionCallback.onReceivedSyncData(bundleData());
                return true;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Process and Store data timeout.");
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public final void updateCurrentStepsAndStepGoal(boolean z, UserProfile userProfile) {
            wg6.b(userProfile, "newUserProfile");
            if (z) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", keep setting in device");
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", " + "{" + "currentSteps=" + userProfile.getCurrentSteps() + ", stepGoal=" + userProfile.getGoalSteps() + ',' + "activeMinute=" + userProfile.getActiveMinute() + ", activeMinuteGoal=" + userProfile.getActiveMinuteGoal() + ',' + "calories=" + userProfile.getCalories() + ", caloriesGoal=" + userProfile.getCaloriesGoal() + ',' + "distanceInCentimeter=" + userProfile.getDistanceInCentimeter() + "totalSleepMin=" + userProfile.getTotalSleepInMinute() + "awakeMin=" + userProfile.getAwakeInMinute() + "lightMin=" + userProfile.getLightSleepInMinute() + "deepMin=" + userProfile.getDeepSleepInMinute() + "}");
            }
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(userProfile.getCurrentSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(userProfile.getGoalSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(userProfile.getActiveMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(userProfile.getActiveMinuteGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(userProfile.getCalories());
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(userProfile.getCaloriesGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(userProfile.getDistanceInCentimeter());
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(userProfile.getTotalSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(userProfile.getAwakeInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(userProfile.getLightSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(userProfile.getDeepSleepInMinute());
            } else {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(BaseTransferDataSubFlow.this.realTimeSteps);
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(BaseTransferDataSubFlow.this.stepGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(BaseTransferDataSubFlow.this.activeMinute);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(BaseTransferDataSubFlow.this.activeMinuteGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(BaseTransferDataSubFlow.this.calorie);
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(BaseTransferDataSubFlow.this.calorieGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(BaseTransferDataSubFlow.this.distanceInCentimeter);
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(BaseTransferDataSubFlow.this.totalSleepInMin);
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(BaseTransferDataSubFlow.this.awakeMin);
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(BaseTransferDataSubFlow.this.lightMin);
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(BaseTransferDataSubFlow.this.deepMin);
            }
            FLogger.INSTANCE.updateSessionDetailInfo(new SessionDetailInfo(BaseTransferDataSubFlow.this.getBleAdapter().getBatteryLevel(), (int) BaseTransferDataSubFlow.this.realTimeStepsInfo, (int) BaseTransferDataSubFlow.this.dailyStepInfo));
            MFSyncLog syncLog = BaseTransferDataSubFlow.this.getSyncLog();
            if (syncLog != null) {
                syncLog.setRealTimeStep(BaseTransferDataSubFlow.this.getUserProfile().getCurrentSteps());
            }
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadDataFileState extends BleStateAbs {
        @DexIgnore
        public yb0<FitnessData[]> task;

        @DexIgnore
        public ReadDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setMaxRetries(3);
        }

        @DexIgnore
        public void cancelCurrentBleTask() {
            super.cancelCurrentBleTask();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "cancelCurrentBleTask is " + this.task);
            yb0<FitnessData[]> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            try {
                this.task = BaseTransferDataSubFlow.this.getBleAdapter().readDataFile(BaseTransferDataSubFlow.this.getLogSession(), BaseTransferDataSubFlow.this.getUserProfile().getUserBiometricData().toSDKBiometricProfile(), this);
                if (this.task == null) {
                    BaseTransferDataSubFlow.this.stopSubFlow(10000);
                    return true;
                }
                startTimeout();
                return true;
            } catch (Exception e) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Read Data Files:  ex=" + e.getMessage());
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.errorLog("ReadDataFileState: catchException=" + e.getMessage(), FLogger.Component.BLE, ErrorCodeBuilder.Step.UNKNOWN, ErrorCodeBuilder.AppError.UNKNOWN);
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
                return true;
            }
        }

        @DexIgnore
        public void onReadDataFilesFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), BaseTransferDataSubFlow.this.getSerial())) {
                BaseTransferDataSubFlow.this.log("Reach the limit retry. Stop.");
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
            }
        }

        @DexIgnore
        public void onReadDataFilesProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
            wg6.b(fitnessDataArr, "data");
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".onReadDataFilesSuccess(), sdk = " + new Gson().a(fitnessDataArr));
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("data size from SDK " + fitnessDataArr.length);
            vd6.a(BaseTransferDataSubFlow.this.mSyncData, (T[]) fitnessDataArr);
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Read DataFiles timeout. Cancel.");
            yb0<FitnessData[]> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseTransferDataSubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, UserProfile userProfile2, BleSession.BleSessionCallback bleSessionCallback2, boolean z) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        wg6.b(str, "tagName");
        wg6.b(bleSession, "bleSession");
        wg6.b(session, "logSession");
        wg6.b(str2, "serial");
        wg6.b(bleAdapterImpl, "bleAdapter");
        wg6.b(userProfile2, "userProfile");
        this.mflog = mFLog;
        this.userProfile = userProfile2;
        this.bleSessionCallback = bleSessionCallback2;
        this.isClearDataOptional = z;
        MFLog mFLog2 = this.mflog;
        this.syncLog = (MFSyncLog) (!(mFLog2 instanceof MFSyncLog) ? null : mFLog2);
        this.realTimeSteps = this.userProfile.getCurrentSteps();
        this.stepGoal = this.userProfile.getGoalSteps();
        this.activeMinute = this.userProfile.getActiveMinute();
        this.activeMinuteGoal = this.userProfile.getActiveMinuteGoal();
        this.calorie = this.userProfile.getCalories();
        this.calorieGoal = this.userProfile.getCaloriesGoal();
        this.distanceInCentimeter = this.userProfile.getDistanceInCentimeter();
        this.totalSleepInMin = this.userProfile.getTotalSleepInMinute();
        this.awakeMin = this.userProfile.getAwakeInMinute();
        this.lightMin = this.userProfile.getLightSleepInMinute();
        this.deepMin = this.userProfile.getDeepSleepInMinute();
        this.isNewDevice = this.userProfile.isNewDevice();
        this.realTimeStepsInfo = this.userProfile.getCurrentSteps();
        this.dailyStepInfo = this.userProfile.getCurrentSteps();
        this.mSyncData = new ArrayList();
        this.syncTime = System.currentTimeMillis() / ((long) 1000);
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    public final List<FitnessData> getSyncData() {
        log("getSyncData dataSize " + this.mSyncData.size());
        return this.mSyncData;
    }

    @DexIgnore
    public final MFSyncLog getSyncLog() {
        return this.syncLog;
    }

    @DexIgnore
    public final long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE;
        String name = PlayDeviceAnimationState.class.getName();
        wg6.a((Object) name, "PlayDeviceAnimationState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.GET_DEVICE_CONFIG_STATE;
        String name2 = GetDeviceConfigState.class.getName();
        wg6.a((Object) name2, "GetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.ERASE_DATA_FILE_STATE;
        String name3 = EraseDataFileState.class.getName();
        wg6.a((Object) name3, "EraseDataFileState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.READ_DATA_FILE_STATE;
        String name4 = ReadDataFileState.class.getName();
        wg6.a((Object) name4, "ReadDataFileState::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<SubFlow.SessionState, String> sessionStateMap5 = getSessionStateMap();
        SubFlow.SessionState sessionState5 = SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE;
        String name5 = ProcessAndStoreDataState.class.getName();
        wg6.a((Object) name5, "ProcessAndStoreDataState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
    }

    @DexIgnore
    public final boolean isClearDataOptional() {
        return this.isClearDataOptional;
    }

    @DexIgnore
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE));
        return true;
    }

    @DexIgnore
    public final void setSyncLog(MFSyncLog mFSyncLog) {
        this.syncLog = mFSyncLog;
    }

    @DexIgnore
    public final void setSyncTime(long j) {
        this.syncTime = j;
    }

    @DexIgnore
    public final void updateCurrentStepAndStepGoalFromApp(boolean z, UserProfile userProfile2) {
        wg6.b(userProfile2, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".updateCurrentStepAndStepGoalFromApp() - currentState=" + getMCurrentState());
        if (getMCurrentState() instanceof ProcessAndStoreDataState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((ProcessAndStoreDataState) mCurrentState).updateCurrentStepsAndStepGoal(z, userProfile2);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow.ProcessAndStoreDataState");
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String tag2 = getTAG();
        local2.d(tag2, "Inside " + getTAG() + ".updateCurrentStepAndStepGoalFromApp - cannot update current steps and step goal " + "to device caused by current state null or not an instance of ProcessAndStoreDataState.");
    }
}
