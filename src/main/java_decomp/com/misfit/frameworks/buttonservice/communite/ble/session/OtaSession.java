package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseOTASubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.log.MFOtaLog;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FirmwareUtils;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OtaSession extends EnableMaintainingSession {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ int OTA_MAX_RETRIES; // = 7;
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public /* final */ BleAdapterImpl bleAdapterV2;
    @DexIgnore
    public /* final */ BleCommunicator.CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public byte[] firmwareBytes; // = new byte[0];
    @DexIgnore
    public /* final */ FirmwareData firmwareData;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<? extends MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public MFOtaLog otaLog;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public UserBiometricData userBiometricData;
    @DexIgnore
    public /* final */ UserProfile userProfile;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class OTASubFlow extends BaseOTASubFlow {
        @DexIgnore
        public OTASubFlow() {
            super(OtaSession.this.getTAG(), OtaSession.this, OtaSession.this.getMfLog(), OtaSession.this.getLogSession(), OtaSession.this.getSerial(), OtaSession.this.getBleAdapterV2(), OtaSession.this.getFirmwareData(), OtaSession.this.firmwareBytes, OtaSession.this.getBleSessionCallback(), OtaSession.this.getCommunicationResultCallback());
        }

        @DexIgnore
        public void onStop(int i) {
            if (i == 0) {
                OtaSession otaSession = OtaSession.this;
                otaSession.enterStateAsync(otaSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
                return;
            }
            OtaSession.this.stop(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        public /* final */ /* synthetic */ OtaSession this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public TransferSettingsSubFlow(OtaSession otaSession) {
            super(otaSession.getTAG(), otaSession, otaSession.otaLog, otaSession.getLogSession(), true, otaSession.getSerial(), otaSession.getBleAdapterV2(), otaSession.getUserProfile(), otaSession.multiAlarmSettings, otaSession.complicationAppMappingSettings, otaSession.watchAppMappingSettings, otaSession.backgroundConfig, otaSession.notificationFilterSettings, otaSession.localizationData, otaSession.microAppMappings, otaSession.secondTimezoneOffset, otaSession.getBleSessionCallback());
            this.this$0 = otaSession;
        }

        @DexIgnore
        public void onStop(int i) {
            this.this$0.stop(i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OtaSession(FirmwareData firmwareData2, UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback2) {
        super(SessionType.SPECIAL, CommunicateMode.OTA, bleAdapterImpl, bleSessionCallback);
        wg6.b(firmwareData2, "firmwareData");
        wg6.b(userProfile2, "userProfile");
        wg6.b(bleAdapterImpl, "bleAdapterV2");
        this.firmwareData = firmwareData2;
        this.userProfile = userProfile2;
        this.bleAdapterV2 = bleAdapterImpl;
        this.communicationResultCallback = communicationResultCallback2;
        setLogSession(FLogger.Session.OTA);
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        MFLog end = MFLogManager.getInstance(getBleAdapter().getContext()).end(CommunicateMode.OTA, getSerial());
        getExtraInfoReturned().putInt(ButtonService.Companion.getLOG_ID(), end != null ? end.getStartTimeEpoch() : 0);
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    public BleSession copyObject() {
        OtaSession otaSession = new OtaSession(this.firmwareData, this.userProfile, this.bleAdapterV2, getBleSessionCallback(), this.communicationResultCallback);
        otaSession.setDevice(getDevice());
        return otaSession;
    }

    @DexIgnore
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        FLogger.INSTANCE.getRemote().d(FLogger.Component.BLE, FLogger.Session.OTA, getSerial(), getTAG(), "doNextState() when set watchParam");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.OtaSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public final BleAdapterImpl getBleAdapterV2() {
        return this.bleAdapterV2;
    }

    @DexIgnore
    public final BleCommunicator.CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public final FirmwareData getFirmwareData() {
        return this.firmwareData;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.OTA_STATE);
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public void initSettings() {
        this.userBiometricData = DevicePreferenceUtils.getAutoBiometricSettings(getContext());
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.OTA_STATE;
        String name = OTASubFlow.class.getName();
        wg6.a((Object) name, "OTASubFlow::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name2 = TransferSettingsSubFlow.class.getName();
        wg6.a((Object) name2, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.OtaSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public boolean onStart(Object... objArr) {
        wg6.b(objArr, "params");
        String firmwareVersion = this.firmwareData.getFirmwareVersion();
        String checkSum = this.firmwareData.getCheckSum();
        boolean isEmbedded = this.firmwareData.isEmbedded();
        this.firmwareBytes = FirmwareUtils.INSTANCE.readFirmware(this.firmwareData, getContext());
        if (getRetriesCounter() == 0) {
            this.otaLog = MFLogManager.getInstance(getBleAdapter().getContext()).startOtaLog(getSerial());
            setMfLog(this.otaLog);
            MFOtaLog mFOtaLog = this.otaLog;
            if (mFOtaLog != null) {
                String firmwareVersion2 = getBleAdapter().getFirmwareVersion();
                mFOtaLog.setFirmwareVersion(firmwareVersion2);
                mFOtaLog.setSerial(getSerial());
                mFOtaLog.setSdkVersion(ButtonService.Companion.getSDKVersion());
                mFOtaLog.setOldFirmware(firmwareVersion2);
                mFOtaLog.setNewFirmware(firmwareVersion);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.i(tag, "START OTA SESSION - targetFirmware=" + firmwareVersion + ", fileLen=" + this.firmwareBytes.length + ", stepGoal=" + this.userProfile.getGoalSteps() + ", locationServiceEnabled=" + LocationUtils.isLocationEnable(getContext()) + ", locationPermissionAllowed=" + LocationUtils.isLocationPermissionGranted(getContext()));
        StringBuilder sb = new StringBuilder();
        sb.append("Start new ota session: stepGoal=");
        sb.append(this.userProfile.getGoalSteps());
        sb.append(", locationServiceEnabled=");
        sb.append(LocationUtils.isLocationEnable(getContext()));
        sb.append(", locationPermissionAllowed=");
        sb.append(LocationUtils.isLocationPermissionGranted(getContext()));
        log(sb.toString());
        log("Verifying firmware...");
        log("- Version: " + firmwareVersion);
        log("- Checksum: " + checkSum);
        log("- Length: " + this.firmwareBytes.length);
        log("- In-app Bundled: " + isEmbedded);
        if (isEmbedded) {
            log("In-app bundled firmware, skip verifying!");
        } else if (FirmwareUtils.INSTANCE.verifyFirmware(this.firmwareBytes, checkSum)) {
            log("Verified: OK");
        } else {
            log("Verified: FAILED. Skip OTA");
            stop(FailureCode.FAILED_TO_OTA_FILE_NOT_READY);
            return false;
        }
        return super.onStart(Arrays.copyOf(objArr, objArr.length));
    }

    @DexIgnore
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        wg6.b(str, "serial");
        wg6.b(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTA;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.OtaSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }
}
