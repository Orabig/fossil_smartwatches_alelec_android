package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.misfit.frameworks.buttonservice.log.FailureCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReadRssiSession$onStart$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ReadRssiSession this$0;

    @DexIgnore
    public ReadRssiSession$onStart$Anon1(ReadRssiSession readRssiSession) {
        this.this$0 = readRssiSession;
    }

    @DexIgnore
    public final void run() {
        this.this$0.stop(FailureCode.BLUETOOTH_IS_DISABLED);
    }
}
