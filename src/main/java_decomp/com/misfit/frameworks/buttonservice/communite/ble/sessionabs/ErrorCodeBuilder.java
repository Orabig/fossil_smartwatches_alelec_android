package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.ac0;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.yj6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ErrorCodeBuilder {
    @DexIgnore
    public static /* final */ ErrorCodeBuilder INSTANCE; // = new ErrorCodeBuilder();

    @DexIgnore
    public enum AppError {
        BLUETOOTH_DISABLED("001"),
        LOCATION_SERVICE_DISABLED("002"),
        LOCATION_ACCESS_DENIED("003"),
        FILE_NOT_READY("004"),
        FIRMWARE_NOT_MATCH("005"),
        COMMAND_TIME_OUT("006"),
        USER_CANCELLED("007"),
        SESSION_INTERRUPT("008"),
        APP_KILLED("009"),
        BUTTON_SERVICE_CRASHED("010"),
        APP_LAYER_CRASHED("011"),
        NETWORK_ERROR("012"),
        SERVER_MAINTENANCE("013"),
        BIOMETRIC_BUILD_FAILED("014"),
        DEVICE_NOT_FOUND("015"),
        GET_TEMPERATURE_FAILED("021"),
        COMMUTE_INFO_NOT_AVAILABLE("022"),
        NOT_FETCH_CURRENT_LOCATION("030"),
        UNKNOWN("999");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public AppError(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum Component {
        APP("1"),
        SDK("2"),
        UNKNOWN("9");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public Component(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum Step {
        START_SCAN("001", "start scan"),
        STOP_SCAN("002", "stop scan"),
        RETRIEVE_DEVICE_BY_SERIAL("003", "retrieve device by serial"),
        ENABLE_MAINTAINING_CONNECTION("004", "enable maintaining connection"),
        DISCONNECT("005", "disconnect"),
        FETCH_DEVICE_INFORMATION("006", "fetch device information"),
        PLAY_ANIMATION("007", "play animation"),
        READ_RSSI("008", "read rssi"),
        OTA("009", Constants.OTA),
        SYNC("010", "sync"),
        CLEAN_DEVICE("011", "clean device"),
        GET_CONFIG("012", "get config"),
        SET_CONFIG("013", "set config"),
        SET_COMPLICATION("014", "set complication"),
        SET_WATCH_APP("015", "set watch app"),
        SET_ALARM("016", "set alarm"),
        SEND_RESPOND("017", "send respond"),
        SET_NOTIFICATION_FILTER("018", "set notification filter"),
        SEND_APP_NOTIFICATION("019", "send app notification"),
        REQUEST_HAND("020", "request hand"),
        MOVE_HAND("021", "move hand"),
        RELEASE_HAND("022", "release hand"),
        SET_CALIBRATION_POSITION("023", "set calibration position"),
        SEND_TRACK_INFO("024", "send track info"),
        NOTIFY_MUSIC_EVENT("025", "notify music event"),
        LINK_DEVICE("026", "link device"),
        READ_WORKOUT("027", "read workout"),
        STOP_WORKOUT("028", "stop workout"),
        RETRIEVE_DEVICE_REQUEST_FROM_OUTSIDER("029", "retrieve device request from outsider"),
        GET_ALARM("030", "get alarm"),
        SET_BACKGROUND_IMAGE("031", "set background image"),
        SET_FRONT_LIGHT_ENABLE("032", "set front light enable"),
        ERASE_DATA("033", "erase data"),
        GET_NOTIFICATION_FILTER("034", "get notification filter"),
        GET_WORKOUT_SESSION("035", "get workout session"),
        STOP_WORKOUT_SESSION("036", "stop workout session"),
        GENERATE_PAIRING_KEY("037", "generate pairing key"),
        START_AUTHEN("038", "start authen"),
        SWAP_PAIRING_KEY("039", "swap pairing key"),
        EXCHANGE_SECRET_KEY("040", "exchange secret key"),
        STORE_KEY_TO_CLOUD("041", "store key to cloud"),
        SET_MICRO_APP("042", "set micro app"),
        SETUP("043", "setup"),
        SET_LOCALIZATION("044", "set localization"),
        ENABLE_HID_CONNECTION("045", "enable HID connection"),
        VERIFY_SECRET_KEY("046", "verify secret key"),
        SET_WATCH_PARAM("047", "set watch param"),
        SYNC_CURRENT_DEVICE("052", "sync current device"),
        AUTHORIZE_DEVICE("053", "authorize device"),
        SET_REPLY_MESSAGE_MAPPING("054", "reply message mapping"),
        UNKNOWN("999", "unknown");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ String nameValue;
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final Step fromValue(String str) {
                wg6.b(str, ServerSetting.VALUE);
                if (wg6.a((Object) str, (Object) Step.START_SCAN.getValue())) {
                    return Step.START_SCAN;
                }
                if (wg6.a((Object) str, (Object) Step.STOP_SCAN.getValue())) {
                    return Step.STOP_SCAN;
                }
                if (wg6.a((Object) str, (Object) Step.RETRIEVE_DEVICE_BY_SERIAL.getValue())) {
                    return Step.RETRIEVE_DEVICE_BY_SERIAL;
                }
                if (wg6.a((Object) str, (Object) Step.ENABLE_MAINTAINING_CONNECTION.getValue())) {
                    return Step.ENABLE_MAINTAINING_CONNECTION;
                }
                if (wg6.a((Object) str, (Object) Step.DISCONNECT.getValue())) {
                    return Step.DISCONNECT;
                }
                if (wg6.a((Object) str, (Object) Step.FETCH_DEVICE_INFORMATION.getValue())) {
                    return Step.FETCH_DEVICE_INFORMATION;
                }
                if (wg6.a((Object) str, (Object) Step.PLAY_ANIMATION.getValue())) {
                    return Step.PLAY_ANIMATION;
                }
                if (wg6.a((Object) str, (Object) Step.READ_RSSI.getValue())) {
                    return Step.READ_RSSI;
                }
                if (wg6.a((Object) str, (Object) Step.OTA.getValue())) {
                    return Step.OTA;
                }
                if (wg6.a((Object) str, (Object) Step.SYNC.getValue())) {
                    return Step.SYNC;
                }
                if (wg6.a((Object) str, (Object) Step.CLEAN_DEVICE.getValue())) {
                    return Step.CLEAN_DEVICE;
                }
                if (wg6.a((Object) str, (Object) Step.GET_CONFIG.getValue())) {
                    return Step.GET_CONFIG;
                }
                if (wg6.a((Object) str, (Object) Step.SET_CONFIG.getValue())) {
                    return Step.SET_CONFIG;
                }
                if (wg6.a((Object) str, (Object) Step.SET_COMPLICATION.getValue())) {
                    return Step.SET_COMPLICATION;
                }
                if (wg6.a((Object) str, (Object) Step.SET_WATCH_APP.getValue())) {
                    return Step.SET_WATCH_APP;
                }
                if (wg6.a((Object) str, (Object) Step.SET_ALARM.getValue())) {
                    return Step.SET_ALARM;
                }
                if (wg6.a((Object) str, (Object) Step.SEND_RESPOND.getValue())) {
                    return Step.SEND_RESPOND;
                }
                if (wg6.a((Object) str, (Object) Step.SET_NOTIFICATION_FILTER.getValue())) {
                    return Step.SET_NOTIFICATION_FILTER;
                }
                if (wg6.a((Object) str, (Object) Step.SEND_APP_NOTIFICATION.getValue())) {
                    return Step.SEND_APP_NOTIFICATION;
                }
                if (wg6.a((Object) str, (Object) Step.REQUEST_HAND.getValue())) {
                    return Step.REQUEST_HAND;
                }
                if (wg6.a((Object) str, (Object) Step.MOVE_HAND.getValue())) {
                    return Step.MOVE_HAND;
                }
                if (wg6.a((Object) str, (Object) Step.RELEASE_HAND.getValue())) {
                    return Step.RELEASE_HAND;
                }
                if (wg6.a((Object) str, (Object) Step.SET_CALIBRATION_POSITION.getValue())) {
                    return Step.SET_CALIBRATION_POSITION;
                }
                if (wg6.a((Object) str, (Object) Step.SEND_TRACK_INFO.getValue())) {
                    return Step.SEND_TRACK_INFO;
                }
                if (wg6.a((Object) str, (Object) Step.NOTIFY_MUSIC_EVENT.getValue())) {
                    return Step.NOTIFY_MUSIC_EVENT;
                }
                if (wg6.a((Object) str, (Object) Step.LINK_DEVICE.getValue())) {
                    return Step.LINK_DEVICE;
                }
                if (wg6.a((Object) str, (Object) Step.READ_WORKOUT.getValue())) {
                    return Step.READ_WORKOUT;
                }
                if (wg6.a((Object) str, (Object) Step.STOP_WORKOUT.getValue())) {
                    return Step.STOP_WORKOUT;
                }
                if (wg6.a((Object) str, (Object) Step.RETRIEVE_DEVICE_REQUEST_FROM_OUTSIDER.getValue())) {
                    return Step.RETRIEVE_DEVICE_REQUEST_FROM_OUTSIDER;
                }
                if (wg6.a((Object) str, (Object) Step.SET_BACKGROUND_IMAGE.getValue())) {
                    return Step.SET_BACKGROUND_IMAGE;
                }
                if (wg6.a((Object) str, (Object) Step.SET_FRONT_LIGHT_ENABLE.getValue())) {
                    return Step.SET_FRONT_LIGHT_ENABLE;
                }
                if (wg6.a((Object) str, (Object) Step.SYNC_CURRENT_DEVICE.getValue())) {
                    return Step.SYNC_CURRENT_DEVICE;
                }
                if (wg6.a((Object) str, (Object) Step.UNKNOWN.getValue())) {
                    return Step.UNKNOWN;
                }
                return Step.UNKNOWN;
            }

            @DexIgnore
            public /* synthetic */ Companion(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((qg6) null);
        }
        */

        @DexIgnore
        public Step(String str, String str2) {
            this.value = str;
            this.nameValue = str2;
        }

        @DexIgnore
        public final String getNameValue() {
            return this.nameValue;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public final String build(Step step, Component component, AppError appError) {
        wg6.b(step, "step");
        wg6.b(component, "component");
        wg6.b(appError, "appError");
        return step.getValue() + component.getValue() + appError.getValue();
    }

    @DexIgnore
    public final String build(Step step, Component component, ac0 ac0) {
        wg6.b(step, "step");
        wg6.b(component, "component");
        wg6.b(ac0, "sdkError");
        return step.getValue() + component.getValue() + yj6.a(String.valueOf(ac0.getErrorCode().getCode()), 3, '0');
    }
}
