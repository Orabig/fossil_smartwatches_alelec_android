package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.x60;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetInactiveNudgeConfigSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ InactiveNudgeData mInactiveNudgeData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetInactiveNudgeConfigState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetInactiveNudgeConfigState() {
            super(SetInactiveNudgeConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            b70 b70 = new b70();
            b70.a(SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStartHour(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStartMinute(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStopHour(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStopMinute(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getRepeatInterval(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.isEnable() ? x60.a.ENABLE : x60.a.DISABLE);
            this.task = SetInactiveNudgeConfigSession.this.getBleAdapter().setDeviceConfig(SetInactiveNudgeConfigSession.this.getLogSession(), b70.a(), this);
            if (this.task == null) {
                SetInactiveNudgeConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetInactiveNudgeConfigSession.this.getContext(), SetInactiveNudgeConfigSession.this.getSerial())) {
                SetInactiveNudgeConfigSession.this.log("Reach the limit retry. Stop.");
                SetInactiveNudgeConfigSession.this.stop(FailureCode.FAILED_TO_SET_INACTIVE_NUDGE_CONFIG);
            }
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetInactiveNudgeConfigSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            SetInactiveNudgeConfigSession.this.log("Set Inactive nudge timeout. Cancel.");
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_INACTIVE_NUDGE_CONFIG, bleAdapterImpl, bleSessionCallback);
        wg6.b(inactiveNudgeData, "mInactiveNudgeData");
        wg6.b(bleAdapterImpl, "bleAdapterV2");
        this.mInactiveNudgeData = inactiveNudgeData;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetInactiveNudgeConfigSession setInactiveNudgeConfigSession = new SetInactiveNudgeConfigSession(this.mInactiveNudgeData, getBleAdapter(), getBleSessionCallback());
        setInactiveNudgeConfigSession.setDevice(getDevice());
        return setInactiveNudgeConfigSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_INACTIVE_NUDGE_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_INACTIVE_NUDGE_STATE;
        String name = SetInactiveNudgeConfigState.class.getName();
        wg6.a((Object) name, "SetInactiveNudgeConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
