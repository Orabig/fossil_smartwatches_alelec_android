package com.misfit.frameworks.buttonservice.communite.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleSession$enterTaskWithDelayTime$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Runnable $task;

    @DexIgnore
    public BleSession$enterTaskWithDelayTime$Anon1(Runnable runnable) {
        this.$task = runnable;
    }

    @DexIgnore
    public final void run() {
        this.$task.run();
    }
}
