package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.j80;
import com.fossil.o80;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NoneWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public NoneWatchAppMapping() {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getEMPTY());
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    public o80 toSDKSetting() {
        return new j80();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NoneWatchAppMapping(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
    }
}
