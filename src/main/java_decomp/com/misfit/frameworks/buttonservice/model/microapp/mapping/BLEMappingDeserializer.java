package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import com.fossil.du3;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.lu3;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomizationDeserializer;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLEMappingDeserializer implements hu3<BLEMapping> {
    @DexIgnore
    public BLEMapping deserialize(JsonElement jsonElement, Type type, gu3 gu3) throws lu3 {
        FLogger.INSTANCE.getLocal().d(BLEMappingDeserializer.class.getName(), jsonElement.toString());
        int b = jsonElement.d().a("mType").b();
        du3 du3 = new du3();
        du3.a(BLECustomization.class, new BLECustomizationDeserializer());
        if (b == 1) {
            return (BLEMapping) du3.a().a(jsonElement, LinkMapping.class);
        }
        if (b != 2) {
            return null;
        }
        return (BLEMapping) du3.a().a(jsonElement, MicroAppMapping.class);
    }
}
