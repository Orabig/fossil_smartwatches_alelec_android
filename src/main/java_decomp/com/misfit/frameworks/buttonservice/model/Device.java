package com.misfit.frameworks.buttonservice.model;

import com.misfit.frameworks.common.enums.DeviceOwnership;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Device {
    @DexIgnore
    public static /* final */ int DEFAULT_OTA_RESET_TIME; // = 15000;
    @DexIgnore
    public DeviceOwnership deviceOwnership;
    @DexIgnore
    public String fastPairId;
    @DexIgnore
    public String macAddress;
    @DexIgnore
    public String name;
    @DexIgnore
    public int requestId;
    @DexIgnore
    public int rssi;
    @DexIgnore
    public String serial;

    @DexIgnore
    public Device() {
    }

    @DexIgnore
    public DeviceOwnership getDeviceOwnership() {
        return this.deviceOwnership;
    }

    @DexIgnore
    public String getFastPairId() {
        return this.fastPairId;
    }

    @DexIgnore
    public String getMacAddress() {
        return this.macAddress;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public int getOTAResetTime() {
        return DEFAULT_OTA_RESET_TIME;
    }

    @DexIgnore
    public int getRequestId() {
        return this.requestId;
    }

    @DexIgnore
    public int getRssi() {
        return this.rssi;
    }

    @DexIgnore
    public String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public void setDeviceOwnership(DeviceOwnership deviceOwnership2) {
        this.deviceOwnership = deviceOwnership2;
    }

    @DexIgnore
    public void setFastPairId(String str) {
        this.fastPairId = str;
    }

    @DexIgnore
    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setRequestId(int i) {
        this.requestId = i;
    }

    @DexIgnore
    public void setRssi(int i) {
        this.rssi = i;
    }

    @DexIgnore
    public void setSerial(String str) {
        this.serial = str;
    }

    @DexIgnore
    public void updateRssi(int i) {
        this.rssi = i;
    }

    @DexIgnore
    public Device(String str, String str2, String str3, int i, String str4) {
        this.serial = str;
        this.name = str2;
        this.macAddress = str3;
        this.rssi = i;
        this.fastPairId = str4;
    }
}
