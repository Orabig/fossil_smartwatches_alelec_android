package com.misfit.frameworks.buttonservice.model.animation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class VibrationSetting {
    @DexIgnore
    public int interval;
    @DexIgnore
    public boolean isLong;
    @DexIgnore
    public short numRepeat;

    @DexIgnore
    public VibrationSetting(short s, int i, boolean z) {
        this.numRepeat = s;
        this.interval = i;
        this.isLong = z;
    }

    @DexIgnore
    public int getInterval() {
        return this.interval;
    }

    @DexIgnore
    public short getNumRepeat() {
        return this.numRepeat;
    }

    @DexIgnore
    public boolean isLong() {
        return this.isLong;
    }

    @DexIgnore
    public void setInterval(int i) {
        this.interval = i;
    }

    @DexIgnore
    public void setIsLong(boolean z) {
        this.isLong = z;
    }

    @DexIgnore
    public void setNumRepeat(short s) {
        this.numRepeat = s;
    }
}
