package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.fossil.e60;
import com.fossil.kc0;
import com.fossil.wg6;
import com.fossil.x50;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SecondTimezoneComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public String location;
    @DexIgnore
    public int utcOffsetInMinutes;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SecondTimezoneComplicationAppMapping(String str, int i) {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getTIMEZONE_2_TYPE());
        wg6.b(str, "location");
        this.location = str;
        this.utcOffsetInMinutes = i;
    }

    @DexIgnore
    public String getHash() {
        String str = getMType() + ":" + this.location + ":" + this.utcOffsetInMinutes;
        wg6.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public x50 toSDKSetting() {
        return new e60(new kc0(this.location, this.utcOffsetInMinutes));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.location);
        parcel.writeInt(this.utcOffsetInMinutes);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SecondTimezoneComplicationAppMapping(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        this.utcOffsetInMinutes = parcel.readInt();
    }
}
