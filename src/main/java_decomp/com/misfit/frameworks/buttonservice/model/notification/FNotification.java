package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FNotification implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public /* final */ String appName;
    @DexIgnore
    public /* final */ String iconFwPath;
    @DexIgnore
    public /* final */ NotificationBaseObj.ANotificationType notificationType;
    @DexIgnore
    public /* final */ String packageName;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<FNotification> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public FNotification createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new FNotification(parcel);
        }

        @DexIgnore
        public FNotification[] newArray(int i) {
            return new FNotification[i];
        }
    }

    @DexIgnore
    public FNotification(String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
        wg6.b(str, "appName");
        wg6.b(str2, "packageName");
        wg6.b(str3, "iconFwPath");
        wg6.b(aNotificationType, "notificationType");
        this.appName = str;
        this.packageName = str2;
        this.iconFwPath = str3;
        this.notificationType = aNotificationType;
    }

    @DexIgnore
    public static /* synthetic */ FNotification copy$default(FNotification fNotification, String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType, int i, Object obj) {
        if ((i & 1) != 0) {
            str = fNotification.appName;
        }
        if ((i & 2) != 0) {
            str2 = fNotification.packageName;
        }
        if ((i & 4) != 0) {
            str3 = fNotification.iconFwPath;
        }
        if ((i & 8) != 0) {
            aNotificationType = fNotification.notificationType;
        }
        return fNotification.copy(str, str2, str3, aNotificationType);
    }

    @DexIgnore
    public final String component1() {
        return this.appName;
    }

    @DexIgnore
    public final String component2() {
        return this.packageName;
    }

    @DexIgnore
    public final String component3() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final NotificationBaseObj.ANotificationType component4() {
        return this.notificationType;
    }

    @DexIgnore
    public final FNotification copy(String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
        wg6.b(str, "appName");
        wg6.b(str2, "packageName");
        wg6.b(str3, "iconFwPath");
        wg6.b(aNotificationType, "notificationType");
        return new FNotification(str, str2, str3, aNotificationType);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FNotification)) {
            return false;
        }
        FNotification fNotification = (FNotification) obj;
        return wg6.a((Object) this.appName, (Object) fNotification.appName) && wg6.a((Object) this.packageName, (Object) fNotification.packageName) && wg6.a((Object) this.iconFwPath, (Object) fNotification.iconFwPath) && wg6.a((Object) this.notificationType, (Object) fNotification.notificationType);
    }

    @DexIgnore
    public final String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public final String getIconFwPath() {
        return this.iconFwPath;
    }

    @DexIgnore
    public final NotificationBaseObj.ANotificationType getNotificationType() {
        return this.notificationType;
    }

    @DexIgnore
    public final String getPackageName() {
        return this.packageName;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.appName;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.packageName;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.iconFwPath;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        NotificationBaseObj.ANotificationType aNotificationType = this.notificationType;
        if (aNotificationType != null) {
            i = aNotificationType.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "FNotification(appName=" + this.appName + ", packageName=" + this.packageName + ", iconFwPath=" + this.iconFwPath + ", notificationType=" + this.notificationType + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.appName);
        parcel.writeString(this.packageName);
        parcel.writeString(this.iconFwPath);
        parcel.writeInt(this.notificationType.ordinal());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public FNotification(Parcel parcel) {
        this(r0, r2, r3 != null ? r3 : r1, NotificationBaseObj.ANotificationType.values()[parcel.readInt()]);
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        String str = "";
        readString = readString == null ? str : readString;
        String readString2 = parcel.readString();
        readString2 = readString2 == null ? str : readString2;
        String readString3 = parcel.readString();
    }

    @DexIgnore
    public FNotification() {
        this("", "", "", NotificationBaseObj.ANotificationType.values()[1]);
    }
}
