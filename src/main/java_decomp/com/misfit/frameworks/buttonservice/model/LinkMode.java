package com.misfit.frameworks.buttonservice.model;

import com.misfit.frameworks.buttonservice.model.microapp.mapping.LinkMapping;
import com.misfit.frameworks.common.enums.Action;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum LinkMode implements Serializable {
    RING_PHONE {
        @DexIgnore
        public boolean isHidMode() {
            return false;
        }
    },
    CONTROL_MUSIC {
        @DexIgnore
        public boolean isHidMode() {
            return true;
        }
    },
    TAKE_PHOTO {
        @DexIgnore
        public boolean isHidMode() {
            return true;
        }
    },
    GOAL_TRACKING {
        @DexIgnore
        public boolean isHidMode() {
            return false;
        }
    };

    @DexIgnore
    public static LinkMode fromAction(int i) {
        if (Action.Music.isActionBelongToThisType(i)) {
            return CONTROL_MUSIC;
        }
        if (Action.Selfie.isActionBelongToThisType(i)) {
            return TAKE_PHOTO;
        }
        if (Action.GoalTracking.isActionBelongToThisType(i)) {
            return GOAL_TRACKING;
        }
        return RING_PHONE;
    }

    @DexIgnore
    public static LinkMode fromLinkMappings(List<LinkMapping> list) {
        if (list == null || list.isEmpty()) {
            return RING_PHONE;
        }
        return fromAction(list.get(0).getAction());
    }

    @DexIgnore
    public static LinkMode fromMappings(List<Mapping> list) {
        if (list == null || list.isEmpty()) {
            return RING_PHONE;
        }
        return fromAction(list.get(0).getAction());
    }

    @DexIgnore
    public static List<LinkMode> getLinkModesFromMappings(List<Mapping> list) {
        HashMap hashMap = new HashMap();
        if (list != null && !list.isEmpty()) {
            for (Mapping action : list) {
                int action2 = action.getAction();
                hashMap.put(fromAction(action2), Integer.valueOf(action2));
            }
        }
        return new ArrayList(hashMap.keySet());
    }

    @DexIgnore
    public static boolean hasLinkModeInMappings(List<Mapping> list, LinkMode linkMode) {
        if (list == null || list.isEmpty()) {
            return false;
        }
        for (Mapping action : list) {
            if (fromAction(action.getAction()) == linkMode) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public abstract boolean isHidMode();
}
