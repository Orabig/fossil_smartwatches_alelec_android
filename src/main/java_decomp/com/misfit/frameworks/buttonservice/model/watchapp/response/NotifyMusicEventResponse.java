package com.misfit.frameworks.buttonservice.model.watchapp.response;

import android.os.Parcel;
import com.fossil.kc6;
import com.fossil.l70;
import com.fossil.m70;
import com.fossil.n70;
import com.fossil.qg6;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotifyMusicEventResponse extends MusicResponse {
    @DexIgnore
    public /* final */ MusicMediaAction musicAction;
    @DexIgnore
    public /* final */ MusicMediaStatus status;

    @DexIgnore
    public enum MusicMediaAction {
        PLAY,
        PAUSE,
        TOGGLE_PLAY_PAUSE,
        NEXT,
        PREVIOUS,
        VOLUME_UP,
        VOLUME_DOWN,
        NONE;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

                /*
                static {
                    $EnumSwitchMapping$0 = new int[l70.values().length];
                    $EnumSwitchMapping$0[l70.PLAY.ordinal()] = 1;
                    $EnumSwitchMapping$0[l70.PAUSE.ordinal()] = 2;
                    $EnumSwitchMapping$0[l70.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                    $EnumSwitchMapping$0[l70.NEXT.ordinal()] = 4;
                    $EnumSwitchMapping$0[l70.PREVIOUS.ordinal()] = 5;
                    $EnumSwitchMapping$0[l70.VOLUME_UP.ordinal()] = 6;
                    $EnumSwitchMapping$0[l70.VOLUME_DOWN.ordinal()] = 7;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final MusicMediaAction fromSDKMusicAction(l70 l70) {
                wg6.b(l70, "sdkMusicAction");
                switch (WhenMappings.$EnumSwitchMapping$0[l70.ordinal()]) {
                    case 1:
                        return MusicMediaAction.PLAY;
                    case 2:
                        return MusicMediaAction.PAUSE;
                    case 3:
                        return MusicMediaAction.TOGGLE_PLAY_PAUSE;
                    case 4:
                        return MusicMediaAction.NEXT;
                    case 5:
                        return MusicMediaAction.PREVIOUS;
                    case 6:
                        return MusicMediaAction.VOLUME_UP;
                    case 7:
                        return MusicMediaAction.VOLUME_DOWN;
                    default:
                        return MusicMediaAction.NONE;
                }
            }

            @DexIgnore
            public /* synthetic */ Companion(qg6 qg6) {
                this();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[MusicMediaAction.values().length];
                $EnumSwitchMapping$0[MusicMediaAction.PLAY.ordinal()] = 1;
                $EnumSwitchMapping$0[MusicMediaAction.PAUSE.ordinal()] = 2;
                $EnumSwitchMapping$0[MusicMediaAction.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                $EnumSwitchMapping$0[MusicMediaAction.NEXT.ordinal()] = 4;
                $EnumSwitchMapping$0[MusicMediaAction.PREVIOUS.ordinal()] = 5;
                $EnumSwitchMapping$0[MusicMediaAction.VOLUME_UP.ordinal()] = 6;
                $EnumSwitchMapping$0[MusicMediaAction.VOLUME_DOWN.ordinal()] = 7;
                $EnumSwitchMapping$0[MusicMediaAction.NONE.ordinal()] = 8;
            }
            */
        }

        /*
        static {
            Companion = new Companion((qg6) null);
        }
        */

        @DexIgnore
        public final l70 toSDKMusicAction() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return l70.PLAY;
                case 2:
                    return l70.PAUSE;
                case 3:
                    return l70.TOGGLE_PLAY_PAUSE;
                case 4:
                    return l70.NEXT;
                case 5:
                    return l70.PREVIOUS;
                case 6:
                    return l70.VOLUME_UP;
                case 7:
                    return l70.VOLUME_DOWN;
                case 8:
                    return null;
                default:
                    throw new kc6();
            }
        }
    }

    @DexIgnore
    public enum MusicMediaStatus {
        SUCCESS,
        NO_MUSIC_PLAYER,
        FAIL_TO_TRIGGER;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[MusicMediaStatus.values().length];
                $EnumSwitchMapping$0[MusicMediaStatus.SUCCESS.ordinal()] = 1;
                $EnumSwitchMapping$0[MusicMediaStatus.NO_MUSIC_PLAYER.ordinal()] = 2;
                $EnumSwitchMapping$0[MusicMediaStatus.FAIL_TO_TRIGGER.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public final m70 toSDKMusicActionStatus() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return m70.SUCCESS;
            }
            if (i == 2) {
                return m70.NO_MUSIC_PLAYER;
            }
            if (i == 3) {
                return m70.FAIL_TO_TRIGGER;
            }
            throw new kc6();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(MusicMediaAction musicMediaAction, MusicMediaStatus musicMediaStatus) {
        super("Music Event_" + musicMediaAction);
        wg6.b(musicMediaAction, "musicAction");
        wg6.b(musicMediaStatus, "status");
        this.musicAction = musicMediaAction;
        this.status = musicMediaStatus;
    }

    @DexIgnore
    public String getHash() {
        String str = this.musicAction + ":" + this.status;
        wg6.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final MusicMediaAction getMusicAction() {
        return this.musicAction;
    }

    @DexIgnore
    public final MusicMediaStatus getStatus() {
        return this.status;
    }

    @DexIgnore
    public String toRemoteLogString() {
        return super.toRemoteLogString() + ", musicAction=" + this.musicAction + ", status=" + this.status;
    }

    @DexIgnore
    public final n70 toSDKMusicEvent() {
        l70 sDKMusicAction = this.musicAction.toSDKMusicAction();
        if (sDKMusicAction != null) {
            return new n70(sDKMusicAction, this.status.toSDKMusicActionStatus());
        }
        return null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.musicAction.ordinal());
        parcel.writeInt(this.status.ordinal());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        this.musicAction = MusicMediaAction.values()[parcel.readInt()];
        this.status = MusicMediaStatus.values()[parcel.readInt()];
    }
}
