package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.AppNotification;
import com.fossil.NotificationFlag;
import com.fossil.NotificationType;
import com.fossil.a80;
import com.fossil.kc6;
import com.fossil.qg6;
import com.fossil.rd6;
import com.fossil.s70;
import com.fossil.t70;
import com.fossil.wg6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.LifeCountDownObject;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class NotificationBaseObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<NotificationBaseObj> CREATOR; // = new NotificationBaseObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public /* final */ int LIFE_COUNT_DOWN; // = 7;
    @DexIgnore
    public /* final */ LifeCountDownObject lifeCountDownObject; // = new LifeCountDownObject(this.LIFE_COUNT_DOWN);
    @DexIgnore
    public String message;
    @DexIgnore
    public NotificationControlActionStatus notificationControlActionStatus;
    @DexIgnore
    public NotificationControlActionType notificationControlActionType;
    @DexIgnore
    public List<ANotificationFlag> notificationFlags;
    @DexIgnore
    public ANotificationType notificationType;
    @DexIgnore
    public String sender;
    @DexIgnore
    public int senderId; // = -1;
    @DexIgnore
    public String title;
    @DexIgnore
    public int uid;

    @DexIgnore
    public enum ANotificationFlag {
        SILENT,
        IMPORTANT,
        PRE_EXISTING,
        ALLOW_USER_REPLY_MESSAGE,
        ALLOW_USER_ACCEPT_CALL,
        ALLOW_USER_REJECT_CALL;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[ANotificationFlag.values().length];
                $EnumSwitchMapping$0[ANotificationFlag.SILENT.ordinal()] = 1;
                $EnumSwitchMapping$0[ANotificationFlag.IMPORTANT.ordinal()] = 2;
                $EnumSwitchMapping$0[ANotificationFlag.PRE_EXISTING.ordinal()] = 3;
                $EnumSwitchMapping$0[ANotificationFlag.ALLOW_USER_REPLY_MESSAGE.ordinal()] = 4;
                $EnumSwitchMapping$0[ANotificationFlag.ALLOW_USER_ACCEPT_CALL.ordinal()] = 5;
                $EnumSwitchMapping$0[ANotificationFlag.ALLOW_USER_REJECT_CALL.ordinal()] = 6;
            }
            */
        }

        @DexIgnore
        public final NotificationFlag toSDKNotificationFlag() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return NotificationFlag.SILENT;
                case 2:
                    return NotificationFlag.IMPORTANT;
                case 3:
                    return NotificationFlag.PRE_EXISTING;
                case 4:
                    return NotificationFlag.ALLOW_USER_REPLY_ACTION;
                case 5:
                    return NotificationFlag.ALLOW_USER_POSITIVE_ACTION;
                case 6:
                    return NotificationFlag.ALLOW_USER_NEGATIVE_ACTION;
                default:
                    throw new kc6();
            }
        }
    }

    @DexIgnore
    public enum ANotificationType {
        UNSUPPORTED,
        INCOMING_CALL,
        TEXT,
        NOTIFICATION,
        EMAIL,
        CALENDAR,
        MISSED_CALL,
        REMOVED;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[ANotificationType.values().length];
                $EnumSwitchMapping$0[ANotificationType.UNSUPPORTED.ordinal()] = 1;
                $EnumSwitchMapping$0[ANotificationType.INCOMING_CALL.ordinal()] = 2;
                $EnumSwitchMapping$0[ANotificationType.TEXT.ordinal()] = 3;
                $EnumSwitchMapping$0[ANotificationType.NOTIFICATION.ordinal()] = 4;
                $EnumSwitchMapping$0[ANotificationType.EMAIL.ordinal()] = 5;
                $EnumSwitchMapping$0[ANotificationType.CALENDAR.ordinal()] = 6;
                $EnumSwitchMapping$0[ANotificationType.MISSED_CALL.ordinal()] = 7;
                $EnumSwitchMapping$0[ANotificationType.REMOVED.ordinal()] = 8;
            }
            */
        }

        @DexIgnore
        public final NotificationType toSDKNotificationType() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return NotificationType.UNSUPPORTED;
                case 2:
                    return NotificationType.INCOMING_CALL;
                case 3:
                    return NotificationType.TEXT;
                case 4:
                    return NotificationType.NOTIFICATION;
                case 5:
                    return NotificationType.EMAIL;
                case 6:
                    return NotificationType.CALENDAR;
                case 7:
                    return NotificationType.MISSED_CALL;
                case 8:
                    return NotificationType.REMOVED;
                default:
                    throw new kc6();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public enum NotificationControlActionStatus {
        SUCCESS,
        FAILED,
        NOT_FOUND;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[NotificationControlActionStatus.values().length];
                $EnumSwitchMapping$0[NotificationControlActionStatus.SUCCESS.ordinal()] = 1;
                $EnumSwitchMapping$0[NotificationControlActionStatus.FAILED.ordinal()] = 2;
                $EnumSwitchMapping$0[NotificationControlActionStatus.NOT_FOUND.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public final s70 toSDKNotificationType() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return s70.SUCCESS;
            }
            if (i == 2) {
                return s70.FAILED;
            }
            if (i == 3) {
                return s70.NOT_FOUND;
            }
            throw new kc6();
        }
    }

    @DexIgnore
    public enum NotificationControlActionType {
        ACCEPT_PHONE_CALL,
        REJECT_PHONE_CALL,
        DISMISS_NOTIFICATION,
        REPLY_MESSAGE;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[NotificationControlActionType.values().length];
                $EnumSwitchMapping$0[NotificationControlActionType.ACCEPT_PHONE_CALL.ordinal()] = 1;
                $EnumSwitchMapping$0[NotificationControlActionType.REJECT_PHONE_CALL.ordinal()] = 2;
                $EnumSwitchMapping$0[NotificationControlActionType.DISMISS_NOTIFICATION.ordinal()] = 3;
                $EnumSwitchMapping$0[NotificationControlActionType.REPLY_MESSAGE.ordinal()] = 4;
            }
            */
        }

        @DexIgnore
        public final t70 toSDKNotificationType() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return t70.ACCEPT_PHONE_CALL;
            }
            if (i == 2) {
                return t70.REJECT_PHONE_CALL;
            }
            if (i == 3) {
                return t70.DISMISS_NOTIFICATION;
            }
            if (i == 4) {
                return t70.REPLY_MESSAGE;
            }
            throw new kc6();
        }
    }

    @DexIgnore
    public NotificationBaseObj() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final LifeCountDownObject getLifeCountDownObject() {
        return this.lifeCountDownObject;
    }

    @DexIgnore
    public final String getMessage() {
        String str = this.message;
        if (str != null) {
            return str;
        }
        wg6.d("message");
        throw null;
    }

    @DexIgnore
    public final NotificationControlActionStatus getNotificationControlActionStatus() {
        NotificationControlActionStatus notificationControlActionStatus2 = this.notificationControlActionStatus;
        if (notificationControlActionStatus2 != null) {
            return notificationControlActionStatus2;
        }
        wg6.d("notificationControlActionStatus");
        throw null;
    }

    @DexIgnore
    public final NotificationControlActionType getNotificationControlActionType() {
        NotificationControlActionType notificationControlActionType2 = this.notificationControlActionType;
        if (notificationControlActionType2 != null) {
            return notificationControlActionType2;
        }
        wg6.d("notificationControlActionType");
        throw null;
    }

    @DexIgnore
    public final List<ANotificationFlag> getNotificationFlags() {
        List<ANotificationFlag> list = this.notificationFlags;
        if (list != null) {
            return list;
        }
        wg6.d("notificationFlags");
        throw null;
    }

    @DexIgnore
    public final ANotificationType getNotificationType() {
        ANotificationType aNotificationType = this.notificationType;
        if (aNotificationType != null) {
            return aNotificationType;
        }
        wg6.d("notificationType");
        throw null;
    }

    @DexIgnore
    public final String getSender() {
        String str = this.sender;
        if (str != null) {
            return str;
        }
        wg6.d(RemoteFLogger.MESSAGE_SENDER_KEY);
        throw null;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.senderId;
    }

    @DexIgnore
    public final String getTitle() {
        String str = this.title;
        if (str != null) {
            return str;
        }
        wg6.d(Explore.COLUMN_TITLE);
        throw null;
    }

    @DexIgnore
    public final int getUid() {
        return this.uid;
    }

    @DexIgnore
    public final void setMessage(String str) {
        wg6.b(str, "<set-?>");
        this.message = str;
    }

    @DexIgnore
    public final void setNotificationControlActionStatus(NotificationControlActionStatus notificationControlActionStatus2) {
        wg6.b(notificationControlActionStatus2, "<set-?>");
        this.notificationControlActionStatus = notificationControlActionStatus2;
    }

    @DexIgnore
    public final void setNotificationControlActionType(NotificationControlActionType notificationControlActionType2) {
        wg6.b(notificationControlActionType2, "<set-?>");
        this.notificationControlActionType = notificationControlActionType2;
    }

    @DexIgnore
    public final void setNotificationFlags(List<ANotificationFlag> list) {
        wg6.b(list, "<set-?>");
        this.notificationFlags = list;
    }

    @DexIgnore
    public final void setNotificationType(ANotificationType aNotificationType) {
        wg6.b(aNotificationType, "<set-?>");
        this.notificationType = aNotificationType;
    }

    @DexIgnore
    public final void setSender(String str) {
        wg6.b(str, "<set-?>");
        this.sender = str;
    }

    @DexIgnore
    public final void setSenderId(int i) {
        this.senderId = i;
    }

    @DexIgnore
    public final void setTitle(String str) {
        wg6.b(str, "<set-?>");
        this.title = str;
    }

    @DexIgnore
    public final void setUid(int i) {
        this.uid = i;
    }

    @DexIgnore
    public String toRemoteLogString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public abstract AppNotification toSDKNotification();

    @DexIgnore
    public final List<a80> toSDKNotificationFlags(List<? extends ANotificationFlag> list) {
        wg6.b(list, "$this$toSDKNotificationFlags");
        ArrayList arrayList = new ArrayList(rd6.a(list, 10));
        for (ANotificationFlag sDKNotificationFlag : list) {
            arrayList.add(sDKNotificationFlag.toSDKNotificationFlag());
        }
        return arrayList;
    }

    @DexIgnore
    public final void toSilentNotification() {
        List<ANotificationFlag> list = this.notificationFlags;
        if (list == null) {
            return;
        }
        if (list != null) {
            list.add(ANotificationFlag.PRE_EXISTING);
        } else {
            wg6.d("notificationFlags");
            throw null;
        }
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(getClass().getName());
    }

    @DexIgnore
    public NotificationBaseObj(Parcel parcel) {
        wg6.b(parcel, "parcel");
    }
}
