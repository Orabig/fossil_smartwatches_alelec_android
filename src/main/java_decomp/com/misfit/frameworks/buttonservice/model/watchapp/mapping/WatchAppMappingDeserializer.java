package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import com.fossil.du3;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.lu3;
import com.fossil.wg6;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import com.portfolio.platform.data.model.PinObject;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppMappingDeserializer implements hu3<WatchAppMapping> {
    @DexIgnore
    public WatchAppMapping deserialize(JsonElement jsonElement, Type type, gu3 gu3) throws lu3 {
        wg6.b(jsonElement, PinObject.COLUMN_JSON_DATA);
        wg6.b(type, "typeOfT");
        wg6.b(gu3, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String name = WatchAppMappingDeserializer.class.getName();
        wg6.a((Object) name, "WatchAppMappingDeserializer::class.java.name");
        local.d(name, jsonElement.toString());
        JsonElement a = jsonElement.d().a(WatchAppMapping.Companion.getFIELD_TYPE());
        wg6.a((Object) a, "jsonType");
        int b = a.b();
        du3 du3 = new du3();
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getDIAGNOTICS()) {
            return (WatchAppMapping) du3.a().a(jsonElement, DiagnosticsWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWELLNESS_DASHBOARD()) {
            return (WatchAppMapping) du3.a().a(jsonElement, WellnessWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWORK_OUT()) {
            return (WatchAppMapping) du3.a().a(jsonElement, WorkoutWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getEMPTY()) {
            return (WatchAppMapping) du3.a().a(jsonElement, NoneWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getMUSIC()) {
            return (WatchAppMapping) du3.a().a(jsonElement, MusicWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getNOTIFICATION_PANEL()) {
            return (WatchAppMapping) du3.a().a(jsonElement, NotificationPanelWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getSTOP_WATCH()) {
            return (WatchAppMapping) du3.a().a(jsonElement, StopWatchWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getTIMER()) {
            return (WatchAppMapping) du3.a().a(jsonElement, TimerWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWEATHER()) {
            return (WatchAppMapping) du3.a().a(jsonElement, WeatherWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getCOMMUTE_TIME()) {
            return (WatchAppMapping) du3.a().a(jsonElement, CommuteTimeWatchAppMapping.class);
        }
        return null;
    }
}
