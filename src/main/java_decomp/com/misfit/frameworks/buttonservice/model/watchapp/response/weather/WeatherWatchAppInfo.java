package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.h70;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rd6;
import com.fossil.w80;
import com.fossil.wg6;
import com.fossil.x80;
import com.fossil.y80;
import com.fossil.z80;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.portfolio.platform.data.model.MFUser;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherWatchAppInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public /* final */ CurrentWeatherInfo currentWeatherInfo;
    @DexIgnore
    public /* final */ List<WeatherDayForecast> dayForecast; // = new ArrayList();
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public /* final */ List<WeatherHourForecast> hourForecast; // = new ArrayList();
    @DexIgnore
    public /* final */ String location;
    @DexIgnore
    public /* final */ UserDisplayUnit.TemperatureUnit temperatureUnit;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherWatchAppInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public WeatherWatchAppInfo createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new WeatherWatchAppInfo(parcel);
        }

        @DexIgnore
        public WeatherWatchAppInfo[] newArray(int i) {
            return new WeatherWatchAppInfo[i];
        }
    }

    @DexIgnore
    public WeatherWatchAppInfo(String str, UserDisplayUnit.TemperatureUnit temperatureUnit2, CurrentWeatherInfo currentWeatherInfo2, List<WeatherDayForecast> list, List<WeatherHourForecast> list2, long j) {
        wg6.b(str, "location");
        wg6.b(temperatureUnit2, MFUser.TEMPERATURE_UNIT);
        wg6.b(currentWeatherInfo2, "currentWeatherInfo");
        wg6.b(list, "dayForecast");
        wg6.b(list2, "hourForecast");
        this.location = str;
        this.temperatureUnit = temperatureUnit2;
        this.currentWeatherInfo = currentWeatherInfo2;
        this.dayForecast.addAll(list);
        this.hourForecast.addAll(list2);
        this.expiredAt = j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final z80 toSDKWeatherAppInfo() {
        List<WeatherDayForecast> list = this.dayForecast;
        ArrayList arrayList = new ArrayList(rd6.a(list, 10));
        for (WeatherDayForecast sDKWeatherDayForecast : list) {
            arrayList.add(sDKWeatherDayForecast.toSDKWeatherDayForecast());
        }
        List<WeatherHourForecast> list2 = this.hourForecast;
        ArrayList arrayList2 = new ArrayList(rd6.a(list2, 10));
        for (WeatherHourForecast sDKWeatherHourForecast : list2) {
            arrayList2.add(sDKWeatherHourForecast.toSDKWeatherHourForecast());
        }
        long j = this.expiredAt;
        String str = this.location;
        h70 sDKTemperatureUnit = this.temperatureUnit.toSDKTemperatureUnit();
        w80 sDKCurrentWeatherInfo = this.currentWeatherInfo.toSDKCurrentWeatherInfo();
        Object[] array = arrayList2.toArray(new y80[0]);
        if (array != null) {
            y80[] y80Arr = (y80[]) array;
            Object[] array2 = arrayList.toArray(new x80[0]);
            if (array2 != null) {
                return new z80(j, str, sDKTemperatureUnit, sDKCurrentWeatherInfo, y80Arr, (x80[]) array2);
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.location);
        parcel.writeString(this.temperatureUnit.getValue());
        parcel.writeParcelable(this.currentWeatherInfo, i);
        parcel.writeTypedList(this.dayForecast);
        parcel.writeTypedList(this.hourForecast);
        parcel.writeLong(this.expiredAt);
    }

    @DexIgnore
    public WeatherWatchAppInfo(Parcel parcel) {
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        UserDisplayUnit.TemperatureUnit.Companion companion = UserDisplayUnit.TemperatureUnit.Companion;
        String readString2 = parcel.readString();
        this.temperatureUnit = companion.fromValue(readString2 == null ? "C" : readString2);
        CurrentWeatherInfo currentWeatherInfo2 = (CurrentWeatherInfo) parcel.readParcelable(CurrentWeatherInfo.class.getClassLoader());
        this.currentWeatherInfo = currentWeatherInfo2 == null ? new CurrentWeatherInfo() : currentWeatherInfo2;
        parcel.readTypedList(this.dayForecast, WeatherDayForecast.CREATOR);
        parcel.readTypedList(this.hourForecast, WeatherHourForecast.CREATOR);
        this.expiredAt = parcel.readLong();
    }
}
