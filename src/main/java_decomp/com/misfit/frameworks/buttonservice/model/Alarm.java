package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.vu3;
import com.j256.ormlite.field.DatabaseField;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Arrays;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Alarm implements Parcelable, Comparable<Alarm> {
    @DexIgnore
    public static /* final */ short ALARM_RING_DEFAULT_DURATION; // = 1000;
    @DexIgnore
    public static /* final */ short ALARM_SMART_MINS; // = 10;
    @DexIgnore
    public static /* final */ short ALARM_SNOOZE_MINS; // = 5;
    @DexIgnore
    public static /* final */ String COLUMN_ALARM_MESSAGE; // = "alarmMessage";
    @DexIgnore
    public static /* final */ String COLUMN_ALARM_MINUTE; // = "alarmMinute";
    @DexIgnore
    public static /* final */ String COLUMN_ALARM_TITLE; // = "alarmTitle";
    @DexIgnore
    public static /* final */ String COLUMN_CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DAYS; // = "days";
    @DexIgnore
    public static /* final */ String COLUMN_IS_ACTIVE; // = "isActiveAlarm";
    @DexIgnore
    public static /* final */ String COLUMN_IS_REPEAT_ALARM; // = "isRepeat";
    @DexIgnore
    public static /* final */ String COLUMN_OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String COLUMN_URI; // = "uri";
    @DexIgnore
    public static /* final */ Parcelable.Creator<Alarm> CREATOR; // = new Anon1();
    @DexIgnore
    public static /* final */ String FORMAT_DATE_ISO; // = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "alarm";
    @DexIgnore
    @vu3("alarmMessage")
    @DatabaseField(columnName = "alarmMessage")
    public String alarmMessage;
    @DexIgnore
    @vu3("alarmMinute")
    @DatabaseField(columnName = "alarmMinute")
    public int alarmMinute;
    @DexIgnore
    @vu3("alarmTitle")
    @DatabaseField(columnName = "alarmTitle")
    public String alarmTitle;
    @DexIgnore
    @vu3("createdAt")
    @DatabaseField(columnName = "createdAt")
    public String createdAt;
    @DexIgnore
    @vu3("days")
    @DatabaseField(columnName = "days")
    public String days;
    @DexIgnore
    @vu3("isActiveAlarm")
    @DatabaseField(columnName = "isActiveAlarm")
    public boolean isActive;
    @DexIgnore
    @vu3("isRepeat")
    @DatabaseField(columnName = "isRepeat")
    public boolean isRepeat;
    @DexIgnore
    public boolean isSynced;
    @DexIgnore
    @vu3("objectId")
    @DatabaseField(columnName = "objectId")
    public String objectId;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @vu3("updatedAt")
    @DatabaseField(columnName = "updatedAt")
    public String updatedAt;
    @DexIgnore
    @vu3("uri")
    @DatabaseField(columnName = "uri", id = true)
    public String uri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<Alarm> {
        @DexIgnore
        public Alarm createFromParcel(Parcel parcel) {
            return new Alarm(parcel);
        }

        @DexIgnore
        public Alarm[] newArray(int i) {
            return new Alarm[i];
        }
    }

    @DexIgnore
    public Alarm(String str, String str2, int i, boolean z, String str3, boolean z2, String str4) {
        this.alarmTitle = str;
        this.alarmMessage = str2;
        this.alarmMinute = i;
        this.isRepeat = z;
        this.createdAt = str3;
        this.updatedAt = str3;
        this.isActive = z2;
        this.uri = str4 + ":" + String.valueOf(Calendar.getInstance().getTimeInMillis());
    }

    @DexIgnore
    private boolean compareDays(int[] iArr, int[] iArr2) {
        if (iArr == null && iArr2 == null) {
            return true;
        }
        if ((iArr != null && iArr2 == null) || iArr == null || iArr.length != iArr2.length) {
            return false;
        }
        Arrays.sort(iArr);
        Arrays.sort(iArr2);
        return Arrays.equals(iArr, iArr2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Alarm)) {
            return false;
        }
        Alarm alarm = (Alarm) obj;
        if (!this.uri.equals(alarm.uri) || !this.alarmTitle.equals(alarm.alarmTitle) || !this.alarmMessage.equals(alarm.alarmMessage) || this.alarmMinute != alarm.alarmMinute || this.isActive != alarm.isActive || this.isRepeat != alarm.isRepeat || !compareDays(getDays(), alarm.getDays())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public String getAlarmMessage() {
        return this.alarmMessage;
    }

    @DexIgnore
    public long getAlarmMillis() {
        return ((long) (this.alarmMinute * 60)) * 1000;
    }

    @DexIgnore
    public int getAlarmMinute() {
        return this.alarmMinute;
    }

    @DexIgnore
    public String getAlarmTitle() {
        return this.alarmTitle;
    }

    @DexIgnore
    public String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public int[] getDays() {
        String str = this.days;
        int i = 0;
        int length = (str == null || str.contains("null")) ? 0 : this.days.length();
        if (length <= 0) {
            return null;
        }
        String[] split = this.days.substring(1, length - 1).split(",");
        int length2 = split.length;
        int[] iArr = new int[length2];
        while (i < length2) {
            try {
                iArr[i] = Integer.parseInt(split[i].trim());
                i++;
            } catch (NumberFormatException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("Alarm", ".getDays - ex=" + e.toString());
                return null;
            }
        }
        return iArr;
    }

    @DexIgnore
    public String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public String getUri() {
        return this.uri;
    }

    @DexIgnore
    public boolean is(Alarm alarm) {
        return alarm != null && this.uri.equals(alarm.uri);
    }

    @DexIgnore
    public boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public boolean isRepeat() {
        return this.isRepeat;
    }

    @DexIgnore
    public boolean isSynced() {
        return this.isSynced;
    }

    @DexIgnore
    public void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public void setAlarmMessage(String str) {
        this.alarmMessage = str;
    }

    @DexIgnore
    public void setAlarmMinute(int i) {
        this.alarmMinute = i;
    }

    @DexIgnore
    public void setAlarmTitle(String str) {
        this.alarmTitle = str;
    }

    @DexIgnore
    public void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public void setDays(int[] iArr) {
        this.days = iArr != null ? Arrays.toString(iArr) : "";
    }

    @DexIgnore
    public void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setRepeat(boolean z) {
        this.isRepeat = z;
    }

    @DexIgnore
    public void setSynced(boolean z) {
        this.isSynced = z;
    }

    @DexIgnore
    public void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public void setUri(String str) {
        this.uri = str;
    }

    @DexIgnore
    public void update(Alarm alarm) {
        this.alarmTitle = alarm.alarmTitle;
        this.alarmMessage = alarm.alarmMessage;
        this.alarmMinute = alarm.alarmMinute;
        this.isRepeat = alarm.isRepeat;
        this.createdAt = alarm.createdAt;
        this.isActive = alarm.isActive;
        this.updatedAt = alarm.updatedAt;
        this.isSynced = alarm.isSynced;
        this.days = alarm.days;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.alarmTitle);
        parcel.writeString(this.alarmMessage);
        parcel.writeInt(this.alarmMinute);
        parcel.writeByte(this.isRepeat ? (byte) 1 : 0);
        parcel.writeString(this.createdAt);
        parcel.writeString(this.objectId);
        parcel.writeByte(this.isActive ? (byte) 1 : 0);
        parcel.writeString(this.updatedAt);
        parcel.writeString(this.uri);
        parcel.writeByte(this.isSynced ? (byte) 1 : 0);
        parcel.writeString(this.days);
    }

    @DexIgnore
    public Alarm clone() {
        Alarm alarm = new Alarm();
        alarm.alarmTitle = this.alarmTitle;
        alarm.alarmMessage = this.alarmMessage;
        alarm.alarmMinute = this.alarmMinute;
        alarm.isRepeat = this.isRepeat;
        alarm.createdAt = this.createdAt;
        alarm.objectId = this.objectId;
        alarm.isActive = this.isActive;
        alarm.updatedAt = this.updatedAt;
        alarm.uri = this.uri;
        alarm.isSynced = true;
        alarm.days = this.days;
        alarm.pinType = this.pinType;
        return alarm;
    }

    @DexIgnore
    public int compareTo(Alarm alarm) {
        int i;
        int i2;
        if (this.isActive && !alarm.isActive) {
            return -1;
        }
        if ((!this.isActive && alarm.isActive) || (i = this.alarmMinute) > (i2 = alarm.alarmMinute)) {
            return 1;
        }
        if (i < i2) {
            return -1;
        }
        return 0;
    }

    @DexIgnore
    public Alarm(String str, String str2, int i, boolean z, String str3, boolean z2, int[] iArr, String str4) {
        this.alarmTitle = str;
        this.alarmMessage = str2;
        this.alarmMinute = i;
        this.isRepeat = z;
        this.createdAt = str3;
        this.updatedAt = str3;
        this.isActive = z2;
        this.uri = str4 + ":" + String.valueOf(Calendar.getInstance().getTimeInMillis());
        this.days = Arrays.toString(iArr);
    }

    @DexIgnore
    public Alarm() {
    }

    @DexIgnore
    public Alarm(Parcel parcel) {
        this.alarmTitle = parcel.readString();
        this.alarmMessage = parcel.readString();
        this.alarmMinute = parcel.readInt();
        boolean z = true;
        this.isRepeat = parcel.readByte() != 0;
        this.createdAt = parcel.readString();
        this.objectId = parcel.readString();
        this.isActive = parcel.readByte() != 0;
        this.updatedAt = parcel.readString();
        this.uri = parcel.readString();
        this.isSynced = parcel.readByte() == 0 ? false : z;
        this.days = parcel.readString();
    }
}
