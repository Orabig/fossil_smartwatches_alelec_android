package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.e90;
import com.fossil.rc0;
import com.fossil.tc0;
import com.fossil.v90;
import com.fossil.w40;
import com.fossil.wg6;
import com.fossil.x90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeTravelMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public int mTravelTimeInMinute;

    @DexIgnore
    public CommuteTimeTravelMicroAppResponse(int i) {
        super(e90.COMMUTE_TIME_TRAVEL_MICRO_APP);
        this.mTravelTimeInMinute = i;
    }

    @DexIgnore
    public tc0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    public tc0 getSDKDeviceResponse(x90 x90, w40 w40) {
        wg6.b(x90, "deviceRequest");
        if (!(x90 instanceof v90) || w40 == null) {
            return null;
        }
        return new rc0((v90) x90, w40, this.mTravelTimeInMinute);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mTravelTimeInMinute);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeTravelMicroAppResponse(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        this.mTravelTimeInMinute = parcel.readInt();
    }
}
