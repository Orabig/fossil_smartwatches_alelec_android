package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.k70;
import com.fossil.kc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.x80;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherDayForecast implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public /* final */ float highTemperature;
    @DexIgnore
    public /* final */ float lowTemperature;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition weatherCondition;
    @DexIgnore
    public /* final */ WeatherWeekDay weekDay;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherDayForecast> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public WeatherDayForecast createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new WeatherDayForecast(parcel);
        }

        @DexIgnore
        public WeatherDayForecast[] newArray(int i) {
            return new WeatherDayForecast[i];
        }
    }

    @DexIgnore
    public enum WeatherWeekDay {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[WeatherWeekDay.values().length];
                $EnumSwitchMapping$0[WeatherWeekDay.SUNDAY.ordinal()] = 1;
                $EnumSwitchMapping$0[WeatherWeekDay.MONDAY.ordinal()] = 2;
                $EnumSwitchMapping$0[WeatherWeekDay.TUESDAY.ordinal()] = 3;
                $EnumSwitchMapping$0[WeatherWeekDay.WEDNESDAY.ordinal()] = 4;
                $EnumSwitchMapping$0[WeatherWeekDay.THURSDAY.ordinal()] = 5;
                $EnumSwitchMapping$0[WeatherWeekDay.FRIDAY.ordinal()] = 6;
                $EnumSwitchMapping$0[WeatherWeekDay.SATURDAY.ordinal()] = 7;
            }
            */
        }

        @DexIgnore
        public final k70 toSDKWeekDay() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return k70.SUNDAY;
                case 2:
                    return k70.MONDAY;
                case 3:
                    return k70.TUESDAY;
                case 4:
                    return k70.WEDNESDAY;
                case 5:
                    return k70.THURSDAY;
                case 6:
                    return k70.FRIDAY;
                case 7:
                    return k70.SATURDAY;
                default:
                    throw new kc6();
            }
        }
    }

    @DexIgnore
    public WeatherDayForecast(float f, float f2, WeatherComplicationAppInfo.WeatherCondition weatherCondition2, WeatherWeekDay weatherWeekDay) {
        wg6.b(weatherCondition2, "weatherCondition");
        wg6.b(weatherWeekDay, "weekDay");
        this.highTemperature = f;
        this.lowTemperature = f2;
        this.weatherCondition = weatherCondition2;
        this.weekDay = weatherWeekDay;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final x80 toSDKWeatherDayForecast() {
        return new x80(this.weekDay.toSDKWeekDay(), this.highTemperature, this.lowTemperature, this.weatherCondition.toSdkWeatherCondition());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeFloat(this.highTemperature);
        parcel.writeFloat(this.lowTemperature);
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeInt(this.weekDay.ordinal());
    }

    @DexIgnore
    public WeatherDayForecast(Parcel parcel) {
        wg6.b(parcel, "parcel");
        this.highTemperature = parcel.readFloat();
        this.lowTemperature = parcel.readFloat();
        this.weatherCondition = WeatherComplicationAppInfo.WeatherCondition.values()[parcel.readInt()];
        this.weekDay = WeatherWeekDay.values()[parcel.readInt()];
    }
}
