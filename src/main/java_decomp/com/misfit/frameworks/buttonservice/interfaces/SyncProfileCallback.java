package com.misfit.frameworks.buttonservice.interfaces;

import android.os.Bundle;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.DeviceErrorState;
import com.misfit.frameworks.buttonservice.model.DeviceTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface SyncProfileCallback {
    @DexIgnore
    void onFail(CommunicateMode communicateMode, int i, String str, DeviceTask deviceTask, DeviceErrorState deviceErrorState);

    @DexIgnore
    void onGattConnectionStateChanged(String str, int i);

    @DexIgnore
    void onSuccess(CommunicateMode communicateMode, int i, String str, Bundle bundle);
}
