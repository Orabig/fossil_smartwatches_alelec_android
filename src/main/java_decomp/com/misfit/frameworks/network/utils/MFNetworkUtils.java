package com.misfit.frameworks.network.utils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFNetworkUtils {
    @DexIgnore
    public static void checkOwnerShip() {
    }

    @DexIgnore
    public static void disableActivityTracker() {
    }

    @DexIgnore
    public static void enableActivityTracker() {
    }
}
