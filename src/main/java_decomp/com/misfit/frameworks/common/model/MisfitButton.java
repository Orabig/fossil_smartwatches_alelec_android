package com.misfit.frameworks.common.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MisfitButton implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<MisfitButton> CREATOR; // = new Anon1();
    @DexIgnore
    public static /* final */ String KEY_MAPPINGS; // = "mappings";
    @DexIgnore
    public static /* final */ String KEY_SERIAL; // = "serial";
    @DexIgnore
    public static /* final */ String KEY_STATUS; // = "status";
    @DexIgnore
    public String mMappings;
    @DexIgnore
    public String mSerial;
    @DexIgnore
    public MisfitButtonState mStatus; // = MisfitButtonState.STATE_UNKNOWN;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<MisfitButton> {
        @DexIgnore
        public MisfitButton createFromParcel(Parcel parcel) {
            return new MisfitButton(parcel);
        }

        @DexIgnore
        public MisfitButton[] newArray(int i) {
            return new MisfitButton[i];
        }
    }

    @DexIgnore
    public MisfitButton(String str, MisfitButtonState misfitButtonState, String str2) {
        this.mSerial = str;
        this.mStatus = misfitButtonState;
        this.mMappings = str2;
    }

    @DexIgnore
    public static MisfitButton fromJson(String str) {
        try {
            Log.i("fromJson", str);
            JSONObject jSONObject = new JSONObject(str);
            return new MisfitButton(jSONObject.getString("serial"), MisfitButtonState.getValue(jSONObject.getInt("status")), jSONObject.getString("mappings"));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof MisfitButton) && ((MisfitButton) obj).mSerial.equals(this.mSerial);
    }

    @DexIgnore
    public String getMappings() {
        return this.mMappings;
    }

    @DexIgnore
    public String getSerial() {
        return this.mSerial;
    }

    @DexIgnore
    public MisfitButtonState getState() {
        return this.mStatus;
    }

    @DexIgnore
    public int hashCode() {
        return this.mSerial.hashCode();
    }

    @DexIgnore
    public String toJson() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("serial", this.mSerial);
            jSONObject.put("status", this.mStatus.getId());
            jSONObject.put("mappings", this.mMappings);
            return jSONObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public String toString() {
        return this.mSerial + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + this.mStatus;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mSerial);
        parcel.writeInt(this.mStatus.getId());
        String str = this.mMappings;
        if (str != null) {
            parcel.writeString(str.toString());
        }
    }

    @DexIgnore
    public MisfitButton(Parcel parcel) {
        this.mSerial = parcel.readString();
        this.mStatus = MisfitButtonState.getValue(parcel.readInt());
        this.mMappings = parcel.readString();
    }
}
