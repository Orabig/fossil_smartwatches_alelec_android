package com.misfit.frameworks.common.model;

import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFBRpm {
    @DexIgnore
    public float rpm;
    @DexIgnore
    public long timeStamp;

    @DexIgnore
    public MFBRpm(float f, long j) {
        this.rpm = f;
        this.timeStamp = j;
    }

    @DexIgnore
    public float getRpm() {
        return this.rpm;
    }

    @DexIgnore
    public long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public void setRpm(float f) {
        this.rpm = f;
    }

    @DexIgnore
    public void setTimeStamp(long j) {
        this.timeStamp = j;
    }

    @DexIgnore
    public JSONArray toJson() {
        try {
            JSONArray jSONArray = new JSONArray();
            jSONArray.put((double) this.rpm);
            jSONArray.put(this.timeStamp);
            return jSONArray;
        } catch (Exception unused) {
            return null;
        }
    }
}
