package com.misfit.frameworks.common.constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Constants {
    @DexIgnore
    public static /* final */ String ACCESS_TOKEN; // = "access_token";
    @DexIgnore
    public static /* final */ String ACTION; // = "action";
    @DexIgnore
    public static /* final */ String ACTION_ANSWER; // = "com.misfit.link.message.answer";
    @DexIgnore
    public static /* final */ String ACTION_API_RESPONSE; // = "com.misfit.link.api.response";
    @DexIgnore
    public static /* final */ String ACTION_BUTTON_ERROR; // = "com.misfit.link.event.error";
    @DexIgnore
    public static /* final */ String ACTION_BUTTON_EVENT; // = "com.misfit.link.event";
    @DexIgnore
    public static /* final */ String ACTION_COMMAND; // = "com.misfit.link.message.command";
    @DexIgnore
    public static /* final */ String ACTION_DB_EVENT; // = "com.misfit.link.db.event";
    @DexIgnore
    public static /* final */ String ACTION_DOWNLOAD_FW_COMPLETED; // = "com.misfit.link.update_fw";
    @DexIgnore
    public static /* final */ String ACTION_NETWORK_FRAMEWORK_RESPONSE; // = "com.misfit.frameworks.response";
    @DexIgnore
    public static /* final */ String ACTION_OTA_EVENT; // = "com.misfit.link.ota.event";
    @DexIgnore
    public static /* final */ String ACTIVITY; // = "activity";
    @DexIgnore
    public static /* final */ String API_KEY; // = "api_key";
    @DexIgnore
    public static /* final */ String APP_VERSION; // = "app_version";
    @DexIgnore
    public static /* final */ String AUTH_TOKEN; // = "auth_token";
    @DexIgnore
    public static /* final */ String AVERAGE_RPM; // = "avg_rpm";
    @DexIgnore
    public static /* final */ String AVERAGE_SPEED; // = "avg_speed";
    @DexIgnore
    public static /* final */ String BATTERY; // = "battery";
    @DexIgnore
    public static /* final */ String BATTERY_LATEST_READ_TIME; // = "battery_latest_read_time";
    @DexIgnore
    public static /* final */ String BOLT; // = "bolt";
    @DexIgnore
    public static /* final */ String BOLTS; // = "bolts";
    @DexIgnore
    public static /* final */ String BOLT_FIRMWARE_VERSION; // = "firmwareVersion";
    @DexIgnore
    public static /* final */ String BOLT_MODEL; // = "leo";
    @DexIgnore
    public static /* final */ int BOLT_RSSI_ERROR; // = -1000;
    @DexIgnore
    public static /* final */ String BUTTON_COUNT; // = "button_count";
    @DexIgnore
    public static /* final */ String BUTTON_DISCOVERED; // = "button_discovered";
    @DexIgnore
    public static /* final */ String BUTTON_GALLERY_LIST; // = "button_gallery_list";
    @DexIgnore
    public static /* final */ String BUTTON_MODEL; // = "FL.2.1";
    @DexIgnore
    public static /* final */ String BUTTON_TYPE; // = "button_type";
    @DexIgnore
    public static /* final */ String CALLBACK; // = "callback";
    @DexIgnore
    public static /* final */ String CALL_ID; // = "call_id";
    @DexIgnore
    public static /* final */ String CALORIES; // = "calories";
    @DexIgnore
    public static /* final */ String CHANGE_LOG; // = "change_log";
    @DexIgnore
    public static /* final */ String CHART; // = "chart";
    @DexIgnore
    public static /* final */ String CHECKSUM; // = "checksum";
    @DexIgnore
    public static /* final */ String CLIENT_CREATED_AT; // = "client_created_at";
    @DexIgnore
    public static /* final */ String CODE; // = "code";
    @DexIgnore
    public static /* final */ String COMMAND; // = "command";
    @DexIgnore
    public static /* final */ String COMMANDS; // = "commands";
    @DexIgnore
    public static /* final */ String CONFIRM_PASSWORD; // = "confirm_password";
    @DexIgnore
    public static /* final */ String CONNECTION_STATE; // = "com.misfit.fw.connectionstate";
    @DexIgnore
    public static /* final */ String CONTENT_TYPE; // = "application/json; charset=utf-8";
    @DexIgnore
    public static /* final */ String CREATED_AT; // = "created_at";
    @DexIgnore
    public static /* final */ String CUCUMBER_API_KEY_VALUE; // = "QmlraW5nQXBw";
    @DexIgnore
    public static /* final */ String CUCUMBER_MODEL; // = "FL.2.2";
    @DexIgnore
    public static /* final */ String CUSTOM; // = "custom";
    @DexIgnore
    public static /* final */ String DAILY_POINTS; // = "daily_points";
    @DexIgnore
    public static /* final */ String DAILY_STEPS; // = "daily_steps";
    @DexIgnore
    public static /* final */ String DATA; // = "data";
    @DexIgnore
    public static /* final */ String DEBUG; // = "debug";
    @DexIgnore
    public static /* final */ String DEFAULTS; // = "defaults";
    @DexIgnore
    public static /* final */ String DEFAULT_FIRMWARE; // = "FL2.2.8r";
    @DexIgnore
    public static /* final */ String DEFAULT_FLASH_BOLT_FW; // = "3.7.9";
    @DexIgnore
    public static /* final */ String DEFAULT_PASS_CODE; // = "01010101010101010101010101010101";
    @DexIgnore
    public static /* final */ String DESC; // = "desc";
    @DexIgnore
    public static /* final */ String DETAIL; // = "detail";
    @DexIgnore
    public static /* final */ String DEVICE; // = "device";
    @DexIgnore
    public static /* final */ String DEVICES; // = "devices";
    @DexIgnore
    public static /* final */ String DEVICE_MODEL; // = "device_model";
    @DexIgnore
    public static /* final */ String DEVICE_MODEL2; // = "deviceModel";
    @DexIgnore
    public static /* final */ String DEVICE_MODEL_FLASH; // = "FL";
    @DexIgnore
    public static /* final */ String DISTANCE; // = "distance";
    @DexIgnore
    public static /* final */ String DOWNLOAD_URL; // = "download_url";
    @DexIgnore
    public static /* final */ String DURATION; // = "duration";
    @DexIgnore
    public static /* final */ String ELEVATION_GAIN; // = "elevation_gain";
    @DexIgnore
    public static /* final */ String EMAIL; // = "email";
    @DexIgnore
    public static /* final */ String END_TIME; // = "end_time";
    @DexIgnore
    public static /* final */ String ENTER_BACKGROUND; // = "enterForeground";
    @DexIgnore
    public static /* final */ String ERROR_RESPONSE_KEY_CODE; // = "code";
    @DexIgnore
    public static /* final */ String ERROR_RESPONSE_KEY_DEVELOPE_MESSAGE; // = "developerMessage";
    @DexIgnore
    public static /* final */ String ERROR_RESPONSE_KEY_MESSAGE; // = "message";
    @DexIgnore
    public static /* final */ String ERROR_RESPONSE_KEY_MORE_INFO; // = "moreInfo";
    @DexIgnore
    public static /* final */ String ERROR_RESPONSE_KEY_STATUS; // = "status";
    @DexIgnore
    public static /* final */ String EVENT; // = "event";
    @DexIgnore
    public static /* final */ String EXTRA_INFO; // = "extra_info";
    @DexIgnore
    public static /* final */ String EXTRA_INFO_BUTTON; // = "extra_infos";
    @DexIgnore
    public static /* final */ String FACEBOOK; // = "facebook";
    @DexIgnore
    public static /* final */ String FACEBOOK_KEY_ACCESS_TOKEN; // = "access_token";
    @DexIgnore
    public static /* final */ String FACEBOOK_KEY_AGE_RANGE; // = "age_range";
    @DexIgnore
    public static /* final */ String FACEBOOK_KEY_EXPIRATION_DATE; // = "expiration_date";
    @DexIgnore
    public static /* final */ String FACEBOOK_KEY_FIRST_NAME; // = "first_name";
    @DexIgnore
    public static /* final */ String FACEBOOK_KEY_GENDER; // = "gender";
    @DexIgnore
    public static /* final */ String FACEBOOK_KEY_ID; // = "id";
    @DexIgnore
    public static /* final */ String FACEBOOK_KEY_LAST_NAME; // = "last_name";
    @DexIgnore
    public static /* final */ String FACEBOOK_KEY_NAME; // = "name";
    @DexIgnore
    public static /* final */ String FIND; // = "find";
    @DexIgnore
    public static /* final */ String FIRMWARE_REVISION_STRING; // = "firmwareRevisionString";
    @DexIgnore
    public static /* final */ String FIRMWARE_VERSION; // = "firmware_version";
    @DexIgnore
    public static /* final */ String FLASH_MODEL; // = "FL.2.0";
    @DexIgnore
    public static /* final */ String FRAMEWORKS_SHAREPREFS; // = "com.mifsit.frameworks";
    @DexIgnore
    public static /* final */ String FROM_DATE; // = "from_date";
    @DexIgnore
    public static /* final */ String GATT_STATE; // = "gattState";
    @DexIgnore
    public static /* final */ String GESTURE; // = "gesture";
    @DexIgnore
    public static /* final */ String GROUP_ID; // = "groupId";
    @DexIgnore
    public static /* final */ String HOME_API_KEY_VALUE; // = "23477788";
    @DexIgnore
    public static /* final */ String ID; // = "id";
    @DexIgnore
    public static /* final */ String IF_AUTHORIZATION; // = "authorization";
    @DexIgnore
    public static /* final */ String IF_AUTH_TOKEN; // = "auth_token";
    @DexIgnore
    public static /* final */ String IF_BEARER; // = "Bearer ";
    @DexIgnore
    public static /* final */ String IF_EVENT_TYPE; // = "EventType";
    @DexIgnore
    public static /* final */ String IF_EXTRA_INFO_DEVICE_MODEL; // = "deviceModel";
    @DexIgnore
    public static /* final */ String IF_EXTRA_INFO_GESTURE; // = "gesture";
    @DexIgnore
    public static /* final */ String IF_EXTRA_INFO_SERIAL_NUMBER; // = "serialNumber";
    @DexIgnore
    public static /* final */ String IF_MODEL_TYPE; // = "ModelType";
    @DexIgnore
    public static /* final */ String IF_OCCURRED_AT; // = "OccurredAt";
    @DexIgnore
    public static /* final */ String IF_SEND_IF_TO_SERVER_SUCCESS; // = "success";
    @DexIgnore
    public static /* final */ String IF_SERIAL_NUMBER; // = "SerialNo";
    @DexIgnore
    public static /* final */ String IF_VERIFY_ENABLE_CONNECTED; // = "connected";
    @DexIgnore
    public static /* final */ String INFO; // = "info";
    @DexIgnore
    public static /* final */ String INSTALLTION; // = "installation";
    @DexIgnore
    public static /* final */ String IS_ACTIVITY_TRACKER_ENABLED; // = "is_activity_tracker_enabled";
    @DexIgnore
    public static /* final */ String IS_CURRENT; // = "isCurrent";
    @DexIgnore
    public static /* final */ String IS_LATEST; // = "is_latest";
    @DexIgnore
    public static /* final */ String IS_LATEST_FIRMWARE; // = "is_latest_firmware";
    @DexIgnore
    public static /* final */ String IS_OTA_SUCCEED; // = "is_ota_succeed";
    @DexIgnore
    public static /* final */ String IS_OVERRIDING_MODE; // = "is_overriding_mode";
    @DexIgnore
    public static /* final */ String JSON_KEY_ACTION; // = "action";
    @DexIgnore
    public static /* final */ String JSON_KEY_ETAG; // = "etag";
    @DexIgnore
    public static /* final */ String JSON_KEY_LIMIT; // = "limit";
    @DexIgnore
    public static /* final */ String JSON_KEY_META; // = "_meta";
    @DexIgnore
    public static /* final */ String JSON_KEY_OFFSET; // = "offset";
    @DexIgnore
    public static /* final */ String JSON_KEY_TOTAL; // = "total";
    @DexIgnore
    public static /* final */ String KEEP_BOLT_CONNECTION; // = "keepConnection";
    @DexIgnore
    public static /* final */ String KEY_MARKETING_OPTIN; // = "marketingOptIn";
    @DexIgnore
    public static /* final */ String KEY_SHARE_DATA_WITH_BRAND; // = "shareDataWithBrand";
    @DexIgnore
    public static /* final */ String LAST_ERROR; // = "last_error";
    @DexIgnore
    public static /* final */ String LAST_LOCATION; // = "last_location";
    @DexIgnore
    public static /* final */ String LAST_TIME_CONNECTED; // = "last_time_connected";
    @DexIgnore
    public static /* final */ String LAT; // = "lat";
    @DexIgnore
    public static /* final */ String LINK_API_KEY_VALUE; // = "Rmxhc2ggQnV0dG9u";
    @DexIgnore
    public static /* final */ String LINK_STATUS; // = "link_status";
    @DexIgnore
    public static /* final */ String LIST_SERIAL; // = "listSerial";
    @DexIgnore
    public static /* final */ String LOCATION_UPDATED; // = "com.misfit.fw.location";
    @DexIgnore
    public static /* final */ String LOG; // = "log";
    @DexIgnore
    public static /* final */ String LON; // = "lon";
    @DexIgnore
    public static /* final */ String MAC_ADDRESS; // = "macAddress";
    @DexIgnore
    public static /* final */ String MAP; // = "map";
    @DexIgnore
    public static /* final */ String MAPPINGS; // = "mappings";
    @DexIgnore
    public static /* final */ String MESSAGE; // = "message";
    @DexIgnore
    public static /* final */ String META; // = "meta";
    @DexIgnore
    public static /* final */ String MICRO_APP_ID; // = "micro_app_id";
    @DexIgnore
    public static /* final */ String MODE; // = "mode";
    @DexIgnore
    public static /* final */ String MP3_EXTENSION; // = "mp3";
    @DexIgnore
    public static /* final */ String MSG; // = "msg";
    @DexIgnore
    public static /* final */ String MUSIC; // = "music";
    @DexIgnore
    public static /* final */ String NAME; // = "name";
    @DexIgnore
    public static /* final */ String NETWORK_COMMAND; // = "com.misfit.frameworks.response";
    @DexIgnore
    public static /* final */ String NETWORK_RESPONSE; // = "com.misfit.frameworks.response";
    @DexIgnore
    public static /* final */ String NEW_PASSWORD; // = "new_password";
    @DexIgnore
    public static /* final */ String OAUTH_LINK; // = "link";
    @DexIgnore
    public static /* final */ String OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String OGG_EXTENSION; // = "ogg";
    @DexIgnore
    public static /* final */ String OLD_PASSWORD; // = "old_password";
    @DexIgnore
    public static /* final */ String ONBOARDING; // = "onboarding";
    @DexIgnore
    public static /* final */ String OS_VERSION; // = "os_version";
    @DexIgnore
    public static /* final */ String OTA; // = "ota";
    @DexIgnore
    public static /* final */ String OTA_PROCESS; // = "ota_process";
    @DexIgnore
    public static /* final */ String OWNERSHIP_CODE; // = "ownership_code";
    @DexIgnore
    public static /* final */ String OWNERSHIP_MESSAGE; // = "ownership_message";
    @DexIgnore
    public static /* final */ String PASSCODE; // = "passcode";
    @DexIgnore
    public static /* final */ String PASSWORD; // = "password";
    @DexIgnore
    public static /* final */ String PAUSE_RESUME; // = "pause_resume";
    @DexIgnore
    public static /* final */ String PLUTO; // = "pluto";
    @DexIgnore
    public static /* final */ String PLUTO_GOLD; // = "S20RZ";
    @DexIgnore
    public static /* final */ String PLUTO_GUN_METAL; // = "S20GZ";
    @DexIgnore
    public static /* final */ String PLUTO_MODEL; // = "S2";
    @DexIgnore
    public static /* final */ String PRESO; // = "preso";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_ACCESS_TOKEN; // = "accessToken";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_ACTIVE_DEVICE; // = "activeDeviceId";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_ACTIVE_DEVICE_ID; // = "activeDeviceId";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_AGREE_LATEST_PRIVACY; // = "agreeLatestPrivacy";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_AGREE_LATEST_TERMS; // = "agreeLatestTerm";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_AUTHTYPE; // = "authType";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_BIRTHDAY; // = "birthday";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_BRAND; // = "brand";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_DIAGNOSTIC_ENABLE; // = "diagnosticEnabled";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_EMAIL_OPT_IN; // = "emailOptIn";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_EMAIL_PROGRESS; // = "emailProgress";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_EXPIRED_AT; // = "expiredAt";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_EXTERNALID; // = "externalId";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_FIRST_NAME; // = "firstName";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_GENDER; // = "gender";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_HEIGHT_IN_CM; // = "heightInCentimeters";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_HREF; // = "href";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_IMAGE; // = "image";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_INTEGRATIONS; // = "integrations";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_IS_ACTIVITY_ONBOARDING_COMPLETE; // = "isActivityOnboardingComplete";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_IS_CURIOSITY_ONBOARDING_COMPLETE; // = "isCuriosityOnboardingComplete";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_IS_LINK_ONBOARDING_COMPLETE; // = "isLinkOnboardingComplete";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_IS_NOTIFICATION_ONBOARDING_COMPLETE; // = "isNotificationsOnboardingComplete";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_IS_ONBOARDING_COMPLETE; // = "isOnboardingComplete";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_LAST_NAME; // = "lastName";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_MESSAGE; // = "message";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_PROFILE_PIC; // = "profilePicture";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_REFRESH_TOKEN; // = "refreshToken";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_REGISTER_DATE; // = "registerDate";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_REGISTRATION_COMPLETE; // = "registrationComplete";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_UID; // = "uid";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_UNIT; // = "unit";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_UNITS_DISTANCE; // = "distance";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_UNITS_HEIGHT; // = "height";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_UNITS_WEIGHT; // = "weight";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_UNIT_GROUP; // = "unitGroup";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_USERNAME; // = "username";
    @DexIgnore
    public static /* final */ String PROFILE_KEY_WEIGHT_IN_GRAMS; // = "weightInGrams";
    @DexIgnore
    public static /* final */ int READ_TIMEOUT; // = 10000;
    @DexIgnore
    public static /* final */ String REGION; // = "region";
    @DexIgnore
    public static /* final */ String REGION_CENTER_LAT; // = "region_center_lat";
    @DexIgnore
    public static /* final */ String REGION_CENTER_LONG; // = "region_center_long";
    @DexIgnore
    public static /* final */ String REGION_SPAN_LAT; // = "region_span_lat";
    @DexIgnore
    public static /* final */ String REGION_SPAN_LONG; // = "region_span_long";
    @DexIgnore
    public static /* final */ String REQUEST_ID; // = "request_id";
    @DexIgnore
    public static /* final */ int REQUEST_TIMEOUT; // = 10000;
    @DexIgnore
    public static /* final */ String RESULT; // = "result";
    @DexIgnore
    public static /* final */ String RINGTONE; // = "ringtone";
    @DexIgnore
    public static /* final */ String RINGTONE_ASSET_PATH; // = "ringtones";
    @DexIgnore
    public static /* final */ String RINGTONE_DEFAULT; // = "Callisto";
    @DexIgnore
    public static /* final */ String RINGTONE_EXTENSION; // = "extension";
    @DexIgnore
    public static /* final */ String RINGTONE_SERIAL; // = "serialNumber";
    @DexIgnore
    public static /* final */ String RING_MY_PHONE; // = "ringMyPhone";
    @DexIgnore
    public static /* final */ String RSSI; // = "rssi";
    @DexIgnore
    public static /* final */ String SCANNED_BUTTON; // = "scanned_button";
    @DexIgnore
    public static /* final */ String SELFIE; // = "selfie";
    @DexIgnore
    public static /* final */ String SERIAL_NUMBER; // = "serial_number";
    @DexIgnore
    public static /* final */ String SERIAL_NUMBER_STRING; // = "serialNumberString";
    @DexIgnore
    public static /* final */ String SERVER_ID; // = "serverId";
    @DexIgnore
    public static /* final */ String SERVICE; // = "service";
    @DexIgnore
    public static /* final */ String SERVICE_RECEIVER; // = "service_receiver";
    @DexIgnore
    public static /* final */ String SERVICE_RESPONSE; // = "service_response";
    @DexIgnore
    public static /* final */ String SESSION; // = "session";
    @DexIgnore
    public static /* final */ String SESSIONS; // = "sessions";
    @DexIgnore
    public static /* final */ String SESSION_ID; // = "session_id";
    @DexIgnore
    public static /* final */ String SHINE_API_KEY_VALUE; // = "76801581";
    @DexIgnore
    public static /* final */ String START_TIME; // = "start_time";
    @DexIgnore
    public static /* final */ String STATE; // = "state";
    @DexIgnore
    public static /* final */ String STRAVA_RESULT; // = "strava_result";
    @DexIgnore
    public static /* final */ String SYNC_ID; // = "syncId";
    @DexIgnore
    public static /* final */ String SYNC_RESULT; // = "com.misfit.fw.syncresult";
    @DexIgnore
    public static /* final */ String THIRD_PARTIES; // = "third_parties";
    @DexIgnore
    public static /* final */ String THIRD_PARTY; // = "thirdparty";
    @DexIgnore
    public static /* final */ String THIRD_PARTY_APP_ID; // = "app_id";
    @DexIgnore
    public static /* final */ String THIRD_PARTY_SUBSCRIBING_STATUS; // = "enable_streaming";
    @DexIgnore
    public static /* final */ String THUMBNAIL_MAP_CONTENT; // = "thumbnail_map_content";
    @DexIgnore
    public static /* final */ String THUMBNAIL_MAP_LINK; // = "thumbnail_map_link";
    @DexIgnore
    public static /* final */ String TOKEN; // = "token";
    @DexIgnore
    public static /* final */ String TO_DATE; // = "to_date";
    @DexIgnore
    public static /* final */ String TZ_OFFSET; // = "tz_offset";
    @DexIgnore
    public static /* final */ String UNLINKED_UUID; // = "UNLINKED-UUID";
    @DexIgnore
    public static /* final */ String UPDATED_AT; // = "updated_at";
    @DexIgnore
    public static /* final */ String UPDATED_TIME; // = "updated_time";
    @DexIgnore
    public static /* final */ String USER_ID; // = "user_id";
    @DexIgnore
    public static /* final */ String USER_SETTING; // = "settings";
    @DexIgnore
    public static /* final */ String USER_TYPE; // = "type";
    @DexIgnore
    public static /* final */ String UUID; // = "uuid";
    @DexIgnore
    public static /* final */ String VARIANT_ID; // = "variant_id";
    @DexIgnore
    public static /* final */ String VERSION_NUM; // = "version_number";
    @DexIgnore
    public static /* final */ String YO_BEARER_TOKEN; // = "Bearer ";
    @DexIgnore
    public static /* final */ String YO_CONTACTS; // = "contacts";
    @DexIgnore
    public static /* final */ String YO_CONTACTS_POST; // = "contacts";
    @DexIgnore
    public static /* final */ String YO_ERROR_POST; // = "error";
    @DexIgnore
    public static /* final */ String YO_HEADER_TOKEN; // = "Authorization";
    @DexIgnore
    public static /* final */ String YO_PARAMS_CONTACT; // = "to";
    @DexIgnore
    public static /* final */ String YO_PARAMS_LINK; // = "link";
    @DexIgnore
    public static /* final */ String YO_PARAMS_SOUND; // = "sound";
    @DexIgnore
    public static /* final */ String YO_PASSWORD; // = "password";
    @DexIgnore
    public static /* final */ String YO_SUCCESS_SEND_MESSAGE; // = "success";
    @DexIgnore
    public static /* final */ String YO_TOKEN; // = "token";
    @DexIgnore
    public static /* final */ String YO_TOKEN_POST; // = "tok";
    @DexIgnore
    public static /* final */ String YO_USER_NAME; // = "username";
    @DexIgnore
    public static /* final */ String YO_VALUE_LINK; // = "";
    @DexIgnore
    public static /* final */ String YO_VALUE_SOUND; // = "yo.mp3";
}
