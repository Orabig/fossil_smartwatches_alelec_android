package com.misfit.frameworks.common.entities;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ButtonModel {
    @DexIgnore
    public long id;
    @DexIgnore
    public String name;

    @DexIgnore
    public ButtonModel(long j, String str) {
        this.id = j;
        this.name = str;
    }

    @DexIgnore
    public long getId() {
        return this.id;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String toString() {
        return "[ButtonModel: id=" + this.id + ", name=" + this.name + "]";
    }
}
