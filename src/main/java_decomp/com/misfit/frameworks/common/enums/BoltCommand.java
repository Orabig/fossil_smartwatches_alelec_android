package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum BoltCommand {
    START_SCAN,
    STOP_SCAN,
    READ_RSSI,
    OAD,
    REGISTER,
    UNREGISTER,
    INIT_BOLT,
    CONNECTION_STATE,
    BLINK,
    SET_GROUP,
    EVENT,
    OAD_PROGRESS,
    DISCONNECT
}
