package com.portfolio.platform.cloudimage;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Constants {
    @DexIgnore
    public static /* final */ Constants INSTANCE; // = new Constants();
    @DexIgnore
    public static /* final */ String MAIN_TAG; // = "CloudImage_";
    @DexIgnore
    public static /* final */ int MAX_RETRY; // = 3;

    @DexIgnore
    public enum CalibrationType {
        TYPE_HOUR("calibration_hour"),
        TYPE_MINUTE("calibration_minute"),
        TYPE_SUB_EYE("calibration_subeye"),
        TYPE_COMPLETE("calibration_complete"),
        NONE("none");
        
        @DexIgnore
        public /* final */ String type;

        @DexIgnore
        public CalibrationType(String str) {
            this.type = str;
        }

        @DexIgnore
        public final String getType() {
            return this.type;
        }
    }

    @DexIgnore
    public enum DeviceType {
        TYPE_BRAND_LOGO("device_brand_logo"),
        TYPE_LARGE("device_large"),
        TYPE_MID("device_mid"),
        TYPE_SMALL("device_small"),
        NONE("none");
        
        @DexIgnore
        public /* final */ String type;

        @DexIgnore
        public DeviceType(String str) {
            this.type = str;
        }

        @DexIgnore
        public final String getType() {
            return this.type;
        }
    }

    @DexIgnore
    public enum DownloadAssetType {
        DEVICE,
        CALIBRATION,
        WEAR_OS,
        BOTH
    }

    @DexIgnore
    public enum Feature {
        DEVICE("DEVICE_4.0"),
        CALIBRATION("CALIBRATION");
        
        @DexIgnore
        public /* final */ String feature;

        @DexIgnore
        public Feature(String str) {
            this.feature = str;
        }

        @DexIgnore
        public final String getFeature() {
            return this.feature;
        }
    }

    @DexIgnore
    public enum Resolution {
        MDPI("MDPI"),
        HDPI("HDPI"),
        XHDPI("XHDPI"),
        XXHDPI("XXHDPI");
        
        @DexIgnore
        public /* final */ String resolution;

        @DexIgnore
        public Resolution(String str) {
            this.resolution = str;
        }

        @DexIgnore
        public final String getResolution() {
            return this.resolution;
        }
    }
}
