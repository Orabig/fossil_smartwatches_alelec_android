package com.portfolio.platform.cloudimage;

import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1", f = "CloudImageRunnable.kt", l = {183, 186, 189}, m = "invokeSuspend")
public final class CloudImageRunnable$returnLogoDownloaded$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzippedPath;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$1", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
            this.$filePath1 = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, this.$filePath1, xe6);
            anon1_Level2.p$ = (il6) obj;
            return anon1_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath1);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$2", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
            this.$filePath2 = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$filePath2, xe6);
            anon2_Level2.p$ = (il6) obj;
            return anon2_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath2);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$3", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon3_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, xe6 xe6) {
            super(2, xe6);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon3_Level2 anon3_Level2 = new Anon3_Level2(this.this$0, xe6);
            anon3_Level2.p$ = (il6) obj;
            return anon3_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon3_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.this$0.mListener;
                if (access$getMListener$p == null) {
                    return null;
                }
                access$getMListener$p.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), "");
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$returnLogoDownloaded$Anon1(CloudImageRunnable cloudImageRunnable, String str, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cloudImageRunnable;
        this.$destinationUnzippedPath = str;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1 = new CloudImageRunnable$returnLogoDownloaded$Anon1(this.this$0, this.$destinationUnzippedPath, xe6);
        cloudImageRunnable$returnLogoDownloaded$Anon1.p$ = (il6) obj;
        return cloudImageRunnable$returnLogoDownloaded$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CloudImageRunnable$returnLogoDownloaded$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            String str = this.$destinationUnzippedPath + '/' + this.this$0.type + ".webp";
            String str2 = this.$destinationUnzippedPath + '/' + this.this$0.type + ".png";
            if (AssetUtil.INSTANCE.checkFileExist(str)) {
                cn6 c = zl6.c();
                Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, str, (xe6) null);
                this.L$0 = il6;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 1;
                if (gk6.a(c, anon1_Level2, this) == a) {
                    return a;
                }
            } else if (AssetUtil.INSTANCE.checkFileExist(str2)) {
                cn6 c2 = zl6.c();
                Anon2_Level2 anon2_Level2 = new Anon2_Level2(this, str2, (xe6) null);
                this.L$0 = il6;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 2;
                if (gk6.a(c2, anon2_Level2, this) == a) {
                    return a;
                }
            } else {
                cn6 c3 = zl6.c();
                Anon3_Level2 anon3_Level2 = new Anon3_Level2(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 3;
                if (gk6.a(c3, anon3_Level2, this) == a) {
                    return a;
                }
            }
        } else if (i == 1 || i == 2 || i == 3) {
            String str3 = (String) this.L$2;
            String str4 = (String) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
