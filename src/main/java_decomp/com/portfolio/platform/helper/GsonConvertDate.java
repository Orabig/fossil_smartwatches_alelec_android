package com.portfolio.platform.helper;

import com.fossil.bk4;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.wg6;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PinObject;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonConvertDate implements hu3<Date> {
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return r6;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x003d */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0046 A[Catch:{ Exception -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0057 A[SYNTHETIC, Splitter:B:18:0x0057] */
    public Date deserialize(JsonElement jsonElement, Type type, gu3 gu3) {
        wg6.b(jsonElement, PinObject.COLUMN_JSON_DATA);
        wg6.b(type, "typeOfT");
        wg6.b(gu3, "context");
        String f = jsonElement.f();
        wg6.a((Object) f, "dateAsString");
        if (f.length() == 0) {
            return new Date(0);
        }
        Date date = bk4.a(DateTimeZone.getDefault(), f).toDate();
        wg6.a((Object) date, "DateHelper.getServerDate\u2026), dateAsString).toDate()");
        try {
            SimpleDateFormat simpleDateFormat = bk4.a.get();
            if (simpleDateFormat == null) {
                Date parse = simpleDateFormat.parse(jsonElement.f());
                if (parse != null) {
                    return parse;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("GsonConvertDate", "deserialize - json=" + jsonElement.f() + ", e=" + e);
            e.printStackTrace();
            return new Date(0);
        }
    }
}
