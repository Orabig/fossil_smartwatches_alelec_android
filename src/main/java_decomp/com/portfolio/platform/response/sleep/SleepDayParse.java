package com.portfolio.platform.response.sleep;

import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SleepDayParse {
    @DexIgnore
    public Date date;
    @DexIgnore
    public int goalMinutes;
    @DexIgnore
    public String owner;
    @DexIgnore
    public boolean reachGoal;
    @DexIgnore
    public int totalSleepMinutes;
    @DexIgnore
    public int[] totalSleepStateDistInMinute;
    @DexIgnore
    public int totalSleeps;

    @DexIgnore
    public MFSleepDay getMFSleepBySleepDayParse() {
        if (this.totalSleepStateDistInMinute == null) {
            return null;
        }
        SleepDistribution sleepDistribution = new SleepDistribution();
        int[] iArr = this.totalSleepStateDistInMinute;
        if (iArr.length > 2) {
            sleepDistribution = new SleepDistribution(iArr[0], iArr[1], iArr[2]);
        }
        return new MFSleepDay(this.date, this.goalMinutes, this.totalSleepMinutes, sleepDistribution, new DateTime(Calendar.getInstance().getTimeInMillis()), new DateTime(Calendar.getInstance().getTimeInMillis()));
    }
}
