package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.fossil.NotificationAppHelper;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppPackageRemoveReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public NotificationsRepository a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.receiver.AppPackageRemoveReceiver$onReceive$1", f = "AppPackageRemoveReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $packageName;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AppPackageRemoveReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(AppPackageRemoveReceiver appPackageRemoveReceiver, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = appPackageRemoveReceiver;
            this.$packageName = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$packageName, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                AppFilter a = NotificationAppHelper.b.a(this.$packageName, MFDeviceFamily.DEVICE_FAMILY_SAM);
                AppFilter a2 = NotificationAppHelper.b.a(this.$packageName, MFDeviceFamily.DEVICE_FAMILY_DIANA);
                if (a != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = AppPackageRemoveReceiver.b;
                    local.d(b, "remove app " + this.$packageName);
                    this.this$0.a().removeAppFilter(a);
                }
                if (a2 != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String b2 = AppPackageRemoveReceiver.b;
                    local2.d(b2, "remove app " + this.$packageName);
                    this.this$0.a().removeAppFilter(a2);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        wg6.a((Object) simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public AppPackageRemoveReceiver() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final NotificationsRepository a() {
        NotificationsRepository notificationsRepository = this.a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        wg6.d("notificationsRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String str;
        wg6.b(context, "context");
        wg6.b(intent, "intent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = b;
        local.d(str2, "onReceive " + intent.getAction());
        if (wg6.a((Object) "android.intent.action.PACKAGE_FULLY_REMOVED", (Object) intent.getAction())) {
            Uri data = intent.getData();
            if (data == null || (str = data.getSchemeSpecificPart()) == null) {
                str = "";
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local2.d(str3, "package removed " + str);
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b(this, str, (xe6) null), 3, (Object) null);
        }
    }
}
