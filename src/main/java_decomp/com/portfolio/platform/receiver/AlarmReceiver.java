package com.portfolio.platform.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.jm4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmReceiver extends BroadcastReceiver {
    @DexIgnore
    public UserRepository a;
    @DexIgnore
    public an4 b;
    @DexIgnore
    public DeviceRepository c;
    @DexIgnore
    public AlarmHelper d;
    @DexIgnore
    public AlarmsRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.receiver.AlarmReceiver$onReceive$1", f = "AlarmReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public /* final */ /* synthetic */ Intent $intent;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(AlarmReceiver alarmReceiver, int i, Intent intent, Context context, xe6 xe6) {
            super(2, xe6);
            this.this$0 = alarmReceiver;
            this.$action = i;
            this.$intent = intent;
            this.$context = context;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$action, this.$intent, this.$context, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r5v6, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                MFUser currentUser = this.this$0.e().getCurrentUser();
                int i = this.$action;
                if (i == 0) {
                    Alarm findNextActiveAlarm = this.this$0.b().findNextActiveAlarm();
                    String stringExtra = this.$intent.getStringExtra("DEF_ALARM_RECEIVER_USER_ID");
                    if (currentUser == null || TextUtils.isEmpty(stringExtra) || (true ^ wg6.a((Object) currentUser.getUserId(), (Object) stringExtra)) || findNextActiveAlarm == null) {
                        FLogger.INSTANCE.getLocal().e("AlarmReceiver", "onReceive - user=null||empty||equals=false");
                        this.this$0.a().a(this.$context);
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putInt("DEF_ALARM_RECEIVER_ACTION", 3);
                        bundle.putString("DEF_ALARM_RECEIVER_USER_ID", currentUser.getUserId());
                        Intent intent = new Intent(this.$context, AlarmReceiver.class);
                        intent.setAction("com.portfolio.platform.ALARM_RECEIVER");
                        intent.putExtras(bundle);
                        AlarmManager alarmManager = (AlarmManager) this.$context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
                        if (alarmManager != null) {
                            alarmManager.setExact(0, AlarmHelper.d.a(findNextActiveAlarm.getMillisecond()), PendingIntent.getBroadcast(this.$context, 101, intent, 134217728));
                        }
                    }
                } else if (i == 2) {
                    FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive - ACTION_ALARM_REPEAT");
                    try {
                        String e = PortfolioApp.get.instance().e();
                        if (e.length() == 0) {
                            return cd6.a;
                        }
                        long g = this.this$0.d().g(e);
                        long e2 = this.this$0.d().e(e);
                        long currentTimeMillis = System.currentTimeMillis();
                        long j = (long) 432000000;
                        if (currentTimeMillis - e2 > j && currentTimeMillis - g > j) {
                            FLogger.INSTANCE.getLocal().d("AlarmReceiver", "Remind to sync success");
                            this.this$0.d().a(currentTimeMillis, e);
                            MFUser currentUser2 = PortfolioApp.get.instance().u().getCurrentUser();
                            String firstName = currentUser2 != null ? currentUser2.getFirstName() : null;
                            if (firstName != null) {
                                nh6 nh6 = nh6.a;
                                String a = jm4.a(this.$context, 2131887166);
                                wg6.a((Object) a, "LanguageHelper.getString\u2026ext, R.string.hello_user)");
                                Object[] objArr = {firstName};
                                String format = String.format(a, Arrays.copyOf(objArr, objArr.length));
                                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                                String a2 = jm4.a(this.$context, 2131886651);
                                FossilNotificationBar.a aVar = FossilNotificationBar.c;
                                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                                wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                                wg6.a((Object) a2, "message");
                                aVar.a(applicationContext, format, a2);
                                if (FossilDeviceSerialPatternUtil.isDianaDevice(e)) {
                                    FLogger.INSTANCE.getLocal().d("AlarmReceiver", "Remind to sync send notificaiton to watch");
                                    PortfolioApp.get.instance().b(e, (NotificationBaseObj) new DianaNotificationObj(99, DianaNotificationObj.AApplicationName.Companion.getFOSSIL().getNotificationType(), DianaNotificationObj.AApplicationName.Companion.getFOSSIL(), "Smart Watch", "Fossil", -1, a2, qd6.d(NotificationBaseObj.ANotificationFlag.IMPORTANT), (Long) null, (NotificationBaseObj.NotificationControlActionStatus) null, (NotificationBaseObj.NotificationControlActionType) null, 1792, (qg6) null));
                                }
                            }
                        }
                    } catch (Exception e3) {
                        FLogger.INSTANCE.getLocal().e("AlarmReceiver", e3.getMessage());
                        e3.printStackTrace();
                    }
                } else if (i == 3) {
                    String stringExtra2 = this.$intent.getStringExtra("DEF_ALARM_RECEIVER_USER_ID");
                    if (currentUser == null || TextUtils.isEmpty(stringExtra2) || (!wg6.a((Object) currentUser.getUserId(), (Object) stringExtra2))) {
                        FLogger.INSTANCE.getLocal().e("AlarmReceiver", "onReceive - user=null||empty||equals=false");
                        this.this$0.a().a(this.$context);
                    } else {
                        this.this$0.a().a();
                        this.this$0.a().d(this.$context);
                    }
                }
                if (TextUtils.isEmpty(PortfolioApp.get.instance().e())) {
                    FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive - active device is EMPTY!!!");
                    return cd6.a;
                }
                Device deviceBySerial = this.this$0.c().getDeviceBySerial(PortfolioApp.get.instance().e());
                if (deviceBySerial != null) {
                    int x = this.this$0.d().x();
                    int intExtra = this.$intent.getIntExtra("REQUEST_CODE", -1);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("AlarmReceiver", "onReceive - extraCode=" + intExtra + ", lowBatteryLevel=" + x + ", deviceBattery=" + deviceBySerial.getBatteryLevel());
                    if (intExtra == 0) {
                        if (deviceBySerial.getBatteryLevel() >= x) {
                            this.this$0.a().f(this.$context);
                        } else {
                            this.this$0.a().b(this.$context);
                        }
                    }
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.d;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        wg6.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository b() {
        AlarmsRepository alarmsRepository = this.e;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        wg6.d("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository c() {
        DeviceRepository deviceRepository = this.c;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        wg6.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final an4 d() {
        an4 an4 = this.b;
        if (an4 != null) {
            return an4;
        }
        wg6.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final UserRepository e() {
        UserRepository userRepository = this.a;
        if (userRepository != null) {
            return userRepository;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wg6.b(context, "context");
        PortfolioApp.get.instance().g().a(this);
        FLogger.INSTANCE.getLocal().d("AlarmReceiver", "onReceive");
        if (intent != null) {
            int intExtra = intent.getIntExtra("DEF_ALARM_RECEIVER_ACTION", -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AlarmReceiver", "onReceive - action=" + intExtra);
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b(this, intExtra, intent, context, (xe6) null), 3, (Object) null);
        }
    }
}
