package com.portfolio.platform.news.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.an4;
import com.fossil.cj4;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.zm4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationReceiver extends BroadcastReceiver {
    @DexIgnore
    public an4 a;
    @DexIgnore
    public cj4 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public NotificationReceiver() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive");
        if (zm4.p.a().n().b() == null) {
            FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive - user is NULL!!!");
            return;
        }
        int intExtra = intent != null ? intent.getIntExtra("ACTION_EVENT", 0) : 0;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("NotificationReceiver", "onReceive - actionEvent=" + intExtra);
        if (intExtra == 1) {
            PortfolioApp instance = PortfolioApp.get.instance();
            cj4 cj4 = this.b;
            if (cj4 != null) {
                instance.a(cj4, false, 10);
            } else {
                wg6.d("mDeviceSettingFactory");
                throw null;
            }
        }
    }
}
