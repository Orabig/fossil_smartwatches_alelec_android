package com.portfolio.platform.uirenew.connectedapps;

import android.app.Activity;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.cw1;
import com.fossil.cw4;
import com.fossil.dl6;
import com.fossil.dw4;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.gv1;
import com.fossil.gw4$c$a;
import com.fossil.gw4$d$a;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.kk4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.th4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yv1;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConnectedAppsPresenter extends cw4 implements kk4.d {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a((qg6) null);
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public /* final */ dw4 g;
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ kk4 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ConnectedAppsPresenter.j;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends cw1<Status> {
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ConnectedAppsPresenter connectedAppsPresenter, Activity activity, int i) {
            super(activity, i);
            this.c = connectedAppsPresenter;
        }

        @DexIgnore
        public void b(Status status) {
            wg6.b(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = ConnectedAppsPresenter.k.a();
            local.d(a, "onGFLogout.onUnresolvableFailure(): isSuccess = " + status.F() + ", statusMessage = " + status.C() + ", statusCode = " + status.B());
            if (status.B() == 17 || status.B() == 5010) {
                this.c.j();
                return;
            }
            this.c.g.B(true);
            this.c.g.o(status.B());
        }

        @DexIgnore
        /* renamed from: c */
        public void a(Status status) {
            wg6.b(status, "status");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = ConnectedAppsPresenter.k.a();
            local.d(a, "onGFLogout.onSuccess(): isSuccess = " + status.F() + ", statusMessage = " + status.C());
            if (status.F()) {
                this.c.j();
                return;
            }
            this.c.g.B(true);
            this.c.g.o(status.B());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1", f = "ConnectedAppsPresenter.kt", l = {43}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ConnectedAppsPresenter connectedAppsPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = connectedAppsPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            boolean z;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                boolean e = this.this$0.i.e();
                dl6 a2 = this.this$0.c();
                gw4$c$a gw4_c_a = new gw4$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.Z$0 = e;
                this.label = 1;
                if (gk6.a(a2, gw4_c_a, this) == a) {
                    return a;
                }
                z = e;
            } else if (i == 1) {
                z = this.Z$0;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = ConnectedAppsPresenter.k.a();
            local.d(a3, "starts isGFConnected=" + this.this$0.i.e());
            this.this$0.i.a((kk4.d) this.this$0);
            this.this$0.g.B(z);
            if (!z && this.this$0.i.f() && !this.this$0.f) {
                this.this$0.i.i();
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(MFUser mFUser, xe6 xe6, ConnectedAppsPresenter connectedAppsPresenter) {
            super(2, xe6);
            this.$it = mFUser;
            this.this$0 = connectedAppsPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.$it, xe6, this.this$0);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                gw4$d$a gw4_d_a = new gw4$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, gw4_d_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = ConnectedAppsPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "ConnectedAppsPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public ConnectedAppsPresenter(dw4 dw4, UserRepository userRepository, PortfolioApp portfolioApp, kk4 kk4) {
        wg6.b(dw4, "mView");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(portfolioApp, "mApp");
        wg6.b(kk4, "mGoogleFitHelper");
        this.g = dw4;
        this.h = userRepository;
        this.i = kk4;
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        Log.d(j, "stop");
        MFUser mFUser = this.e;
        if (mFUser != null) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(mFUser, (xe6) null, this), 3, (Object) null);
        }
        this.i.a((kk4.d) null);
    }

    @DexIgnore
    public void h() {
        if (!PortfolioApp.get.instance().y()) {
            this.g.o(601);
            this.g.B(this.i.e());
        } else if (this.i.e()) {
            this.i.g();
        } else if (this.i.d()) {
            this.i.a();
        } else {
            kk4 kk4 = this.i;
            dw4 dw4 = this.g;
            if (dw4 != null) {
                kk4.a((Fragment) (ConnectedAppsFragment) dw4);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
        }
    }

    @DexIgnore
    public void i() {
        this.i.a();
    }

    @DexIgnore
    public final void j() {
        this.i.b();
        String name = th4.GoogleFit.getName();
        wg6.a((Object) name, "Integration.GoogleFit.getName()");
        b(name);
        this.g.B(false);
    }

    @DexIgnore
    public void k() {
        this.g.a(this);
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "connectedAppName");
        MFUser mFUser = this.e;
        if (mFUser != null) {
            List<String> integrations = mFUser.getIntegrations();
            wg6.a((Object) integrations, "mUser!!.integrations");
            integrations.remove(str);
            MFUser mFUser2 = this.e;
            if (mFUser2 != null) {
                mFUser2.setIntegrations(integrations);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d(j, "onGFConnectionSuccess");
        String name = th4.GoogleFit.getName();
        wg6.a((Object) name, "Integration.GoogleFit.getName()");
        a(name);
        this.g.B(true);
    }

    @DexIgnore
    public void a(gv1 gv1) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onGFConnectionFailed - Cause: " + gv1);
        if (gv1 == null) {
            return;
        }
        if (gv1.D()) {
            this.f = false;
            this.g.a(gv1);
            return;
        }
        this.g.o(gv1.p());
    }

    @DexIgnore
    public void a(yv1<Status> yv1) {
        if (yv1 != null) {
            dw4 dw4 = this.g;
            if (dw4 != null) {
                FragmentActivity activity = ((ConnectedAppsFragment) dw4).getActivity();
                if (activity != null) {
                    yv1.a(new b(this, activity, 3));
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.ConnectedAppsFragment");
            }
        }
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "connectedAppName");
        MFUser mFUser = this.e;
        if (mFUser != null) {
            List<String> integrations = mFUser.getIntegrations();
            wg6.a((Object) integrations, "mUser!!.integrations");
            if (!integrations.contains(str)) {
                integrations.add(str);
            }
            MFUser mFUser2 = this.e;
            if (mFUser2 != null) {
                mFUser2.setIntegrations(integrations);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
