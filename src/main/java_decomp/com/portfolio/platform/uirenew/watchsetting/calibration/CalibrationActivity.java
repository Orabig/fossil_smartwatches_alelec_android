package com.portfolio.platform.uirenew.watchsetting.calibration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.iv5;
import com.fossil.kv5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.watchsetting.CalibrationFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CalibrationActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public kv5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            Intent intent = new Intent(context, CalibrationActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        CalibrationFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = CalibrationFragment.s.a();
            a((Fragment) b, 2131362119);
        }
        PortfolioApp.get.instance().g().a(new iv5(b)).a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        a(false);
    }
}
