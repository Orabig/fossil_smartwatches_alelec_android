package com.portfolio.platform.uirenew.customview.imagecropper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.fossil.imagefilters.FilterType;
import com.fossil.jw4;
import com.fossil.kw4;
import com.fossil.lw4;
import com.fossil.mw4;
import com.fossil.nw4;
import com.fossil.pb;
import com.fossil.x24;
import com.portfolio.platform.uirenew.customview.imagecropper.CropOverlayView;
import java.lang.ref.WeakReference;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CropImageView extends FrameLayout {
    @DexIgnore
    public int A;
    @DexIgnore
    public g B;
    @DexIgnore
    public f C;
    @DexIgnore
    public h D;
    @DexIgnore
    public i E;
    @DexIgnore
    public e F;
    @DexIgnore
    public Uri G;
    @DexIgnore
    public int H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public RectF L;
    @DexIgnore
    public int M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public Uri O;
    @DexIgnore
    public WeakReference<kw4> P;
    @DexIgnore
    public WeakReference<jw4> Q;
    @DexIgnore
    public /* final */ ImageView a;
    @DexIgnore
    public /* final */ CropOverlayView b;
    @DexIgnore
    public /* final */ Matrix c;
    @DexIgnore
    public /* final */ Matrix d;
    @DexIgnore
    public /* final */ float[] e;
    @DexIgnore
    public /* final */ float[] f;
    @DexIgnore
    public mw4 g;
    @DexIgnore
    public Bitmap h;
    @DexIgnore
    public Bitmap i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public k u;
    @DexIgnore
    public FilterType v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements CropOverlayView.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(boolean z) {
            CropImageView.this.a(z, true);
            g a2 = CropImageView.this.B;
            if (a2 != null && !z) {
                a2.a(CropImageView.this.getCropRect());
            }
            f b = CropImageView.this.C;
            if (b != null && z) {
                b.a(CropImageView.this.getCropRect());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public b(Bitmap bitmap, Uri uri, Bitmap bitmap2, Uri uri2, Exception exc, float[] fArr, Rect rect, Rect rect2, int i, int i2) {
        }
    }

    @DexIgnore
    public enum c {
        RECTANGLE,
        OVAL
    }

    @DexIgnore
    public enum d {
        OFF,
        ON_TOUCH,
        ON
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(CropImageView cropImageView, b bVar);
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        void a(Rect rect);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(Rect rect);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        void a(CropImageView cropImageView, Uri uri, Exception exc);
    }

    @DexIgnore
    public enum j {
        NONE,
        SAMPLING,
        RESIZE_INSIDE,
        RESIZE_FIT,
        RESIZE_EXACT
    }

    @DexIgnore
    public enum k {
        FIT_CENTER,
        CENTER,
        CENTER_CROP,
        CENTER_INSIDE
    }

    @DexIgnore
    public CropImageView(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public void c(int i2, int i3) {
        this.b.b(i2, i3);
    }

    @DexIgnore
    public boolean d() {
        return this.p;
    }

    @DexIgnore
    public boolean e() {
        return this.q;
    }

    @DexIgnore
    public final void f() {
        float[] fArr = this.e;
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        fArr[2] = (float) this.h.getWidth();
        float[] fArr2 = this.e;
        fArr2[3] = 0.0f;
        fArr2[4] = (float) this.h.getWidth();
        this.e[5] = (float) this.h.getHeight();
        float[] fArr3 = this.e;
        fArr3[6] = 0.0f;
        fArr3[7] = (float) this.h.getHeight();
        this.c.mapPoints(this.e);
        float[] fArr4 = this.f;
        fArr4[0] = 0.0f;
        fArr4[1] = 0.0f;
        fArr4[2] = 100.0f;
        fArr4[3] = 0.0f;
        fArr4[4] = 100.0f;
        fArr4[5] = 100.0f;
        fArr4[6] = 0.0f;
        fArr4[7] = 100.0f;
        this.c.mapPoints(fArr4);
    }

    @DexIgnore
    public final void g() {
        CropOverlayView cropOverlayView = this.b;
        if (cropOverlayView != null) {
            cropOverlayView.setVisibility((!this.x || this.h == null) ? 4 : 0);
        }
    }

    @DexIgnore
    public Pair<Integer, Integer> getAspectRatio() {
        return new Pair<>(Integer.valueOf(this.b.getAspectRatioX()), Integer.valueOf(this.b.getAspectRatioY()));
    }

    @DexIgnore
    public Bitmap getBitmap() {
        return this.h;
    }

    @DexIgnore
    public float[] getCropPoints() {
        RectF cropWindowRect = this.b.getCropWindowRect();
        float f2 = cropWindowRect.left;
        float f3 = cropWindowRect.top;
        float f4 = cropWindowRect.right;
        float f5 = cropWindowRect.bottom;
        float[] fArr = {f2, f3, f4, f3, f4, f5, f2, f5};
        this.c.invert(this.d);
        this.d.mapPoints(fArr);
        for (int i2 = 0; i2 < fArr.length; i2++) {
            fArr[i2] = fArr[i2] * ((float) this.H);
        }
        return fArr;
    }

    @DexIgnore
    public Rect getCropRect() {
        int i2 = this.H;
        Bitmap bitmap = this.h;
        if (bitmap == null) {
            return null;
        }
        return lw4.a(getCropPoints(), bitmap.getWidth() * i2, i2 * bitmap.getHeight(), this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY());
    }

    @DexIgnore
    public c getCropShape() {
        return this.b.getCropShape();
    }

    @DexIgnore
    public RectF getCropWindowRect() {
        CropOverlayView cropOverlayView = this.b;
        if (cropOverlayView == null) {
            return null;
        }
        return cropOverlayView.getCropWindowRect();
    }

    @DexIgnore
    public Bitmap getCroppedImage() {
        return a(0, 0, j.NONE);
    }

    @DexIgnore
    public void getCroppedImageAsync() {
        b(0, 0, j.NONE);
    }

    @DexIgnore
    public d getGuidelines() {
        return this.b.getGuidelines();
    }

    @DexIgnore
    public int getImageResource() {
        return this.t;
    }

    @DexIgnore
    public Uri getImageUri() {
        return this.G;
    }

    @DexIgnore
    public Bitmap getInitializeBitmap() {
        return this.i;
    }

    @DexIgnore
    public int getLoadedSampleSize() {
        return this.H;
    }

    @DexIgnore
    public int getMaxZoom() {
        return this.A;
    }

    @DexIgnore
    public int getRotatedDegrees() {
        return this.o;
    }

    @DexIgnore
    public k getScaleType() {
        return this.u;
    }

    @DexIgnore
    public Rect getWholeImageRect() {
        int i2 = this.H;
        Bitmap bitmap = this.h;
        if (bitmap == null) {
            return null;
        }
        return new Rect(0, 0, bitmap.getWidth() * i2, bitmap.getHeight() * i2);
    }

    @DexIgnore
    public final void h() {
        if (!this.y) {
            return;
        }
        if (this.h != null || this.P == null) {
            WeakReference<jw4> weakReference = this.Q;
        }
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.r <= 0 || this.s <= 0) {
            a(true);
            return;
        }
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.width = this.r;
        layoutParams.height = this.s;
        setLayoutParams(layoutParams);
        if (this.h != null) {
            float f2 = (float) (i4 - i2);
            float f3 = (float) (i5 - i3);
            a(f2, f3, true, false);
            if (this.L != null) {
                int i6 = this.M;
                if (i6 != this.j) {
                    this.o = i6;
                    a(f2, f3, true, false);
                }
                this.c.mapRect(this.L);
                this.b.setCropWindowRect(this.L);
                a(false, false);
                this.b.a();
                this.L = null;
            } else if (this.N) {
                this.N = false;
                a(false, false);
            }
        } else {
            a(true);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        Bitmap bitmap = this.h;
        if (bitmap != null) {
            if (size2 == 0) {
                size2 = bitmap.getHeight();
            }
            double width = size < this.h.getWidth() ? ((double) size) / ((double) this.h.getWidth()) : Double.POSITIVE_INFINITY;
            double height = size2 < this.h.getHeight() ? ((double) size2) / ((double) this.h.getHeight()) : Double.POSITIVE_INFINITY;
            if (width == Double.POSITIVE_INFINITY && height == Double.POSITIVE_INFINITY) {
                i5 = this.h.getWidth();
                i4 = this.h.getHeight();
            } else if (width <= height) {
                i4 = (int) (((double) this.h.getHeight()) * width);
                i5 = size;
            } else {
                i5 = (int) (((double) this.h.getWidth()) * height);
                i4 = size2;
            }
            int a2 = a(mode, size, i5);
            int a3 = a(mode2, size2, i4);
            this.r = a2;
            this.s = a3;
            setMeasuredDimension(this.r, this.s);
            return;
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            if (this.P == null && this.G == null && this.h == null && this.t == 0) {
                FilterType serializable = bundle.getSerializable("FILTER_TYPE");
                Uri uri = (Uri) bundle.getParcelable("LOADED_IMAGE_URI");
                if (uri != null) {
                    String string = bundle.getString("LOADED_IMAGE_STATE_BITMAP_KEY");
                    if (string != null) {
                        Pair<String, WeakReference<Bitmap>> pair = lw4.g;
                        Bitmap bitmap = (pair == null || !((String) pair.first).equals(string)) ? null : (Bitmap) ((WeakReference) lw4.g.second).get();
                        lw4.g = null;
                        if (bitmap != null && !bitmap.isRecycled()) {
                            a(bitmap, 0, uri, bundle.getInt("LOADED_SAMPLE_SIZE"), 0);
                        }
                    }
                    if (this.G == null) {
                        a(uri, serializable);
                    }
                } else {
                    int i2 = bundle.getInt("LOADED_IMAGE_RESOURCE");
                    if (i2 > 0) {
                        setImageResource(i2);
                    } else {
                        Uri uri2 = (Uri) bundle.getParcelable("LOADING_IMAGE_URI");
                        if (uri2 != null) {
                            a(uri2, serializable);
                        }
                    }
                }
                int i3 = bundle.getInt("DEGREES_ROTATED");
                this.M = i3;
                this.o = i3;
                Rect rect = (Rect) bundle.getParcelable("INITIAL_CROP_RECT");
                if (rect != null && (rect.width() > 0 || rect.height() > 0)) {
                    this.b.setInitialCropWindowRect(rect);
                }
                RectF rectF = (RectF) bundle.getParcelable("CROP_WINDOW_RECT");
                if (rectF != null && (rectF.width() > 0.0f || rectF.height() > 0.0f)) {
                    this.L = rectF;
                }
                this.b.setCropShape(c.valueOf(bundle.getString("CROP_SHAPE")));
                this.z = bundle.getBoolean("CROP_AUTO_ZOOM_ENABLED");
                this.A = bundle.getInt("CROP_MAX_ZOOM");
                this.p = bundle.getBoolean("CROP_FLIP_HORIZONTALLY");
                this.q = bundle.getBoolean("CROP_FLIP_VERTICALLY");
            }
            super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        kw4 kw4;
        if (this.G == null && this.h == null && this.t < 1) {
            return super.onSaveInstanceState();
        }
        Bundle bundle = new Bundle();
        Uri uri = this.G;
        if (this.w && uri == null && this.t < 1) {
            uri = lw4.a(getContext(), this.h, this.O);
            this.O = uri;
        }
        if (!(uri == null || this.h == null)) {
            String uuid = UUID.randomUUID().toString();
            lw4.g = new Pair<>(uuid, new WeakReference(this.h));
            bundle.putString("LOADED_IMAGE_STATE_BITMAP_KEY", uuid);
        }
        WeakReference<kw4> weakReference = this.P;
        if (!(weakReference == null || (kw4 = (kw4) weakReference.get()) == null)) {
            bundle.putParcelable("LOADING_IMAGE_URI", kw4.a());
        }
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putParcelable("LOADED_IMAGE_URI", uri);
        bundle.putSerializable("FILTER_TYPE", this.v);
        bundle.putInt("LOADED_IMAGE_RESOURCE", this.t);
        bundle.putInt("LOADED_SAMPLE_SIZE", this.H);
        bundle.putInt("DEGREES_ROTATED", this.o);
        bundle.putParcelable("INITIAL_CROP_RECT", this.b.getInitialCropWindowRect());
        lw4.c.set(this.b.getCropWindowRect());
        this.c.invert(this.d);
        this.d.mapRect(lw4.c);
        bundle.putParcelable("CROP_WINDOW_RECT", lw4.c);
        bundle.putString("CROP_SHAPE", this.b.getCropShape().name());
        bundle.putBoolean("CROP_AUTO_ZOOM_ENABLED", this.z);
        bundle.putInt("CROP_MAX_ZOOM", this.A);
        bundle.putBoolean("CROP_FLIP_HORIZONTALLY", this.p);
        bundle.putBoolean("CROP_FLIP_VERTICALLY", this.q);
        return bundle;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.N = i4 > 0 && i5 > 0;
    }

    @DexIgnore
    public void setAutoZoomEnabled(boolean z2) {
        if (this.z != z2) {
            this.z = z2;
            a(false, false);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void setCropRect(Rect rect) {
        this.b.setInitialCropWindowRect(rect);
    }

    @DexIgnore
    public void setCropShape(c cVar) {
        this.b.setCropShape(cVar);
    }

    @DexIgnore
    public void setFixedAspectRatio(boolean z2) {
        this.b.setFixedAspectRatio(z2);
    }

    @DexIgnore
    public void setFlippedHorizontally(boolean z2) {
        if (this.p != z2) {
            this.p = z2;
            a((float) getWidth(), (float) getHeight(), true, false);
        }
    }

    @DexIgnore
    public void setFlippedVertically(boolean z2) {
        if (this.q != z2) {
            this.q = z2;
            a((float) getWidth(), (float) getHeight(), true, false);
        }
    }

    @DexIgnore
    public void setGuidelines(d dVar) {
        this.b.setGuidelines(dVar);
    }

    @DexIgnore
    public void setImageBitmap(Bitmap bitmap) {
        this.b.setInitialCropWindowRect((Rect) null);
        a(bitmap, 0, (Uri) null, 1, 0);
    }

    @DexIgnore
    public void setImageResource(int i2) {
        if (i2 != 0) {
            this.b.setInitialCropWindowRect((Rect) null);
            a(BitmapFactory.decodeResource(getResources(), i2), i2, (Uri) null, 1, 0);
        }
    }

    @DexIgnore
    public void setMaxZoom(int i2) {
        if (this.A != i2 && i2 > 0) {
            this.A = i2;
            a(false, false);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void setMultiTouchEnabled(boolean z2) {
        if (this.b.b(z2)) {
            a(false, false);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public void setOnCropImageCompleteListener(e eVar) {
        this.F = eVar;
    }

    @DexIgnore
    public void setOnCropWindowChangedListener(h hVar) {
        this.D = hVar;
    }

    @DexIgnore
    public void setOnSetCropOverlayMovedListener(f fVar) {
        this.C = fVar;
    }

    @DexIgnore
    public void setOnSetCropOverlayReleasedListener(g gVar) {
        this.B = gVar;
    }

    @DexIgnore
    public void setOnSetImageUriCompleteListener(i iVar) {
        this.E = iVar;
    }

    @DexIgnore
    public void setRotatedDegrees(int i2) {
        int i3 = this.o;
        if (i3 != i2) {
            a(i2 - i3);
        }
    }

    @DexIgnore
    public void setSaveBitmapToInstanceState(boolean z2) {
        this.w = z2;
    }

    @DexIgnore
    public void setScaleType(k kVar) {
        if (kVar != this.u) {
            this.u = kVar;
            this.I = 1.0f;
            this.K = 0.0f;
            this.J = 0.0f;
            this.b.f();
            requestLayout();
        }
    }

    @DexIgnore
    public void setShowCropOverlay(boolean z2) {
        if (this.x != z2) {
            this.x = z2;
            g();
        }
    }

    @DexIgnore
    public void setShowProgressBar(boolean z2) {
        if (this.y != z2) {
            this.y = z2;
            h();
        }
    }

    @DexIgnore
    public void setSnapRadius(float f2) {
        if (f2 >= 0.0f) {
            this.b.setSnapRadius(f2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v46, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public CropImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Bundle bundleExtra;
        this.c = new Matrix();
        this.d = new Matrix();
        this.e = new float[8];
        this.f = new float[8];
        this.v = FilterType.ATKINSON_DITHERING;
        this.w = false;
        this.x = true;
        this.y = true;
        this.z = true;
        this.H = 1;
        this.I = 1.0f;
        nw4 nw4 = null;
        Intent intent = context instanceof Activity ? ((Activity) context).getIntent() : null;
        if (!(intent == null || (bundleExtra = intent.getBundleExtra("CROP_IMAGE_EXTRA_BUNDLE")) == null)) {
            nw4 = bundleExtra.getParcelable("CROP_IMAGE_EXTRA_OPTIONS");
        }
        if (nw4 == null) {
            nw4 = new nw4();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.CropImageView, 0, 0);
                try {
                    nw4.p = obtainStyledAttributes.getBoolean(10, nw4.p);
                    nw4.q = obtainStyledAttributes.getInteger(0, nw4.q);
                    nw4.r = obtainStyledAttributes.getInteger(1, nw4.r);
                    nw4.e = k.values()[obtainStyledAttributes.getInt(26, nw4.e.ordinal())];
                    nw4.h = obtainStyledAttributes.getBoolean(2, nw4.h);
                    nw4.i = obtainStyledAttributes.getBoolean(24, nw4.i);
                    nw4.j = obtainStyledAttributes.getInteger(19, nw4.j);
                    nw4.a = c.values()[obtainStyledAttributes.getInt(27, nw4.a.ordinal())];
                    nw4.d = d.values()[obtainStyledAttributes.getInt(13, nw4.d.ordinal())];
                    nw4.b = obtainStyledAttributes.getDimension(30, nw4.b);
                    nw4.c = obtainStyledAttributes.getDimension(31, nw4.c);
                    nw4.o = obtainStyledAttributes.getFloat(16, nw4.o);
                    nw4.s = obtainStyledAttributes.getDimension(8, nw4.s);
                    nw4.t = obtainStyledAttributes.getInteger(9, nw4.t);
                    nw4.u = obtainStyledAttributes.getDimension(7, nw4.u);
                    nw4.v = obtainStyledAttributes.getDimension(6, nw4.v);
                    nw4.w = obtainStyledAttributes.getDimension(5, nw4.w);
                    nw4.x = obtainStyledAttributes.getInteger(4, nw4.x);
                    nw4.y = obtainStyledAttributes.getDimension(15, nw4.y);
                    nw4.z = obtainStyledAttributes.getInteger(14, nw4.z);
                    nw4.A = obtainStyledAttributes.getInteger(3, nw4.A);
                    nw4.f = obtainStyledAttributes.getBoolean(28, this.x);
                    nw4.g = obtainStyledAttributes.getBoolean(29, this.y);
                    nw4.u = obtainStyledAttributes.getDimension(7, nw4.u);
                    nw4.B = (int) obtainStyledAttributes.getDimension(23, (float) nw4.B);
                    nw4.C = (int) obtainStyledAttributes.getDimension(22, (float) nw4.C);
                    nw4.D = (int) obtainStyledAttributes.getFloat(21, (float) nw4.D);
                    nw4.E = (int) obtainStyledAttributes.getFloat(20, (float) nw4.E);
                    nw4.F = (int) obtainStyledAttributes.getFloat(18, (float) nw4.F);
                    nw4.G = (int) obtainStyledAttributes.getFloat(17, (float) nw4.G);
                    nw4.W = obtainStyledAttributes.getBoolean(11, nw4.W);
                    nw4.X = obtainStyledAttributes.getBoolean(11, nw4.X);
                    this.w = obtainStyledAttributes.getBoolean(25, this.w);
                    if (obtainStyledAttributes.hasValue(0) && obtainStyledAttributes.hasValue(0) && !obtainStyledAttributes.hasValue(10)) {
                        nw4.p = true;
                    }
                } finally {
                    obtainStyledAttributes.recycle();
                }
            }
        }
        nw4.a();
        this.u = nw4.e;
        this.z = nw4.h;
        this.A = nw4.j;
        this.x = nw4.f;
        this.y = nw4.g;
        this.p = nw4.W;
        this.q = nw4.X;
        View inflate = LayoutInflater.from(context).inflate(2131558461, this, true);
        this.a = (ImageView) inflate.findViewById(2131361803);
        this.a.setScaleType(ImageView.ScaleType.MATRIX);
        this.b = (CropOverlayView) inflate.findViewById(2131361798);
        this.b.setCropWindowChangeListener(new a());
        this.b.setInitialAttributeValues(nw4);
        h();
    }

    @DexIgnore
    public void b(int i2, int i3) {
        this.b.a(i2, i3);
    }

    @DexIgnore
    public boolean c() {
        return this.b.c();
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.b.setAspectRatioX(i2);
        this.b.setAspectRatioY(i3);
        setFixedAspectRatio(true);
    }

    @DexIgnore
    public void b(int i2, int i3, j jVar) {
        a(i2, i3, jVar, (Uri) null, (Bitmap.CompressFormat) null, 0);
    }

    @DexIgnore
    public final void b() {
        if (this.h != null && (this.t > 0 || this.G != null)) {
            this.h.recycle();
        }
        this.h = null;
        this.t = 0;
        this.G = null;
        this.H = 1;
        this.o = 0;
        this.I = 1.0f;
        this.J = 0.0f;
        this.K = 0.0f;
        this.c.reset();
        this.O = null;
        this.a.setImageBitmap((Bitmap) null);
        g();
    }

    @DexIgnore
    public Bitmap a(int i2, int i3, j jVar) {
        Bitmap bitmap;
        j jVar2 = jVar;
        if (this.h == null) {
            return null;
        }
        this.a.clearAnimation();
        int i4 = 0;
        int i5 = jVar2 != j.NONE ? i2 : 0;
        if (jVar2 != j.NONE) {
            i4 = i3;
        }
        if (this.G == null || (this.H <= 1 && jVar2 != j.SAMPLING)) {
            bitmap = lw4.a(this.h, getCropPoints(), this.o, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), this.p, this.q).a;
        } else {
            bitmap = lw4.a(getContext(), this.G, getCropPoints(), this.o, this.h.getWidth() * this.H, this.h.getHeight() * this.H, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), i5, i4, this.p, this.q).a;
        }
        return lw4.a(bitmap, i5, i4, jVar2);
    }

    @DexIgnore
    public void a() {
        this.D = null;
        this.C = null;
        this.B = null;
        this.F = null;
        this.E = null;
    }

    @DexIgnore
    public void a(Bitmap bitmap, pb pbVar) {
        int i2;
        Bitmap bitmap2;
        if (bitmap == null || pbVar == null) {
            bitmap2 = bitmap;
            i2 = 0;
        } else {
            lw4.b a2 = lw4.a(bitmap, pbVar);
            Bitmap bitmap3 = a2.a;
            int i3 = a2.b;
            this.j = i3;
            i2 = i3;
            bitmap2 = bitmap3;
        }
        this.b.setInitialCropWindowRect((Rect) null);
        a(bitmap2, 0, (Uri) null, 1, i2);
    }

    @DexIgnore
    public void a(Uri uri, FilterType filterType) {
        if (uri != null) {
            this.v = filterType;
            WeakReference<kw4> weakReference = this.P;
            kw4 kw4 = weakReference != null ? (kw4) weakReference.get() : null;
            if (kw4 != null) {
                kw4.cancel(true);
            }
            b();
            this.L = null;
            this.M = 0;
            this.b.setInitialCropWindowRect((Rect) null);
            this.P = new WeakReference<>(new kw4(this, uri, filterType));
            ((kw4) this.P.get()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            h();
        }
    }

    @DexIgnore
    public void a(int i2) {
        int i3;
        int i4 = i2;
        if (this.h != null) {
            if (i4 < 0) {
                i3 = (i4 % 360) + 360;
            } else {
                i3 = i4 % 360;
            }
            boolean z2 = !this.b.c() && ((i3 > 45 && i3 < 135) || (i3 > 215 && i3 < 305));
            lw4.c.set(this.b.getCropWindowRect());
            RectF rectF = lw4.c;
            float height = (z2 ? rectF.height() : rectF.width()) / 2.0f;
            RectF rectF2 = lw4.c;
            float width = (z2 ? rectF2.width() : rectF2.height()) / 2.0f;
            if (z2) {
                boolean z3 = this.p;
                this.p = this.q;
                this.q = z3;
            }
            this.c.invert(this.d);
            lw4.d[0] = lw4.c.centerX();
            lw4.d[1] = lw4.c.centerY();
            float[] fArr = lw4.d;
            fArr[2] = 0.0f;
            fArr[3] = 0.0f;
            fArr[4] = 1.0f;
            fArr[5] = 0.0f;
            this.d.mapPoints(fArr);
            this.o = (this.o + i3) % 360;
            a((float) getWidth(), (float) getHeight(), true, false);
            this.c.mapPoints(lw4.e, lw4.d);
            float[] fArr2 = lw4.e;
            double pow = Math.pow((double) (fArr2[4] - fArr2[2]), 2.0d);
            float[] fArr3 = lw4.e;
            float f2 = width;
            this.I = (float) (((double) this.I) / Math.sqrt(pow + Math.pow((double) (fArr3[5] - fArr3[3]), 2.0d)));
            this.I = Math.max(this.I, 1.0f);
            a((float) getWidth(), (float) getHeight(), true, false);
            this.c.mapPoints(lw4.e, lw4.d);
            float[] fArr4 = lw4.e;
            double pow2 = Math.pow((double) (fArr4[4] - fArr4[2]), 2.0d);
            float[] fArr5 = lw4.e;
            double sqrt = Math.sqrt(pow2 + Math.pow((double) (fArr5[5] - fArr5[3]), 2.0d));
            float f3 = (float) (((double) height) * sqrt);
            float f4 = (float) (((double) f2) * sqrt);
            RectF rectF3 = lw4.c;
            float[] fArr6 = lw4.e;
            rectF3.set(fArr6[0] - f3, fArr6[1] - f4, fArr6[0] + f3, fArr6[1] + f4);
            this.b.f();
            this.b.setCropWindowRect(lw4.c);
            a((float) getWidth(), (float) getHeight(), true, false);
            a(false, false);
            this.b.a();
        }
    }

    @DexIgnore
    public void a(kw4.a aVar) {
        this.P = null;
        h();
        if (aVar.e == null) {
            this.j = aVar.d;
            if (this.i == null) {
                Bitmap bitmap = aVar.b;
                this.i = bitmap.copy(bitmap.getConfig(), false);
            }
            a(aVar.f, 0, aVar.a, aVar.c, aVar.d);
        }
        i iVar = this.E;
        if (iVar != null) {
            iVar.a(this, aVar.a, aVar.e);
        }
    }

    @DexIgnore
    public void a(jw4.a aVar) {
        this.Q = null;
        h();
        e eVar = this.F;
        if (eVar != null) {
            eVar.a(this, new b(this.h, this.G, aVar.a, aVar.b, aVar.c, getCropPoints(), getCropRect(), getWholeImageRect(), getRotatedDegrees(), aVar.d));
        }
    }

    @DexIgnore
    public final void a(Bitmap bitmap, int i2, Uri uri, int i3, int i4) {
        Bitmap bitmap2 = this.h;
        if (bitmap2 == null || !bitmap2.equals(bitmap)) {
            this.a.clearAnimation();
            b();
            bitmap.copy(bitmap.getConfig(), true);
            this.h = bitmap;
            this.a.setImageBitmap(this.h);
            this.G = uri;
            this.t = i2;
            this.H = i3;
            this.o = i4;
            a((float) getWidth(), (float) getHeight(), true, false);
            CropOverlayView cropOverlayView = this.b;
            if (cropOverlayView != null) {
                cropOverlayView.f();
                g();
            }
        }
    }

    @DexIgnore
    public void a(int i2, int i3, j jVar, Uri uri, Bitmap.CompressFormat compressFormat, int i4) {
        CropImageView cropImageView;
        j jVar2 = jVar;
        Bitmap bitmap = this.h;
        if (bitmap != null) {
            this.a.clearAnimation();
            WeakReference<jw4> weakReference = this.Q;
            jw4 jw4 = weakReference != null ? (jw4) weakReference.get() : null;
            if (jw4 != null) {
                jw4.cancel(true);
            }
            int i5 = jVar2 != j.NONE ? i2 : 0;
            int i6 = jVar2 != j.NONE ? i3 : 0;
            int width = bitmap.getWidth() * this.H;
            int height = bitmap.getHeight();
            int i7 = this.H;
            int i8 = height * i7;
            if (this.G == null || (i7 <= 1 && jVar2 != j.SAMPLING)) {
                jw4 jw42 = r0;
                jw4 jw43 = new jw4(this, bitmap, getCropPoints(), this.o, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), i5, i6, this.p, this.q, jVar, uri, compressFormat, i4);
                cropImageView = this;
                cropImageView.Q = new WeakReference<>(jw42);
            } else {
                jw4 jw44 = r0;
                jw4 jw45 = new jw4(this, this.G, getCropPoints(), this.o, width, i8, this.b.c(), this.b.getAspectRatioX(), this.b.getAspectRatioY(), i5, i6, this.p, this.q, jVar, uri, compressFormat, i4);
                this.Q = new WeakReference<>(jw44);
                cropImageView = this;
            }
            ((jw4) cropImageView.Q.get()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            h();
            return;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00db  */
    public final void a(boolean z2, boolean z3) {
        float f2;
        float f3;
        int width = getWidth();
        int height = getHeight();
        if (this.h != null && width > 0 && height > 0) {
            RectF cropWindowRect = this.b.getCropWindowRect();
            if (z2) {
                if (cropWindowRect.left < 0.0f || cropWindowRect.top < 0.0f || cropWindowRect.right > ((float) width) || cropWindowRect.bottom > ((float) height)) {
                    a((float) width, (float) height, false, false);
                }
            } else if (this.z || this.I > 1.0f) {
                if (this.I < ((float) this.A)) {
                    float f4 = (float) width;
                    if (cropWindowRect.width() < f4 * 0.5f) {
                        float f5 = (float) height;
                        if (cropWindowRect.height() < 0.5f * f5) {
                            f2 = Math.min((float) this.A, Math.min(f4 / ((cropWindowRect.width() / this.I) / 0.64f), f5 / ((cropWindowRect.height() / this.I) / 0.64f)));
                            if (this.I > 1.0f) {
                                float f6 = (float) width;
                                if (cropWindowRect.width() > f6 * 0.65f || cropWindowRect.height() > ((float) height) * 0.65f) {
                                    f3 = Math.max(1.0f, Math.min(f6 / ((cropWindowRect.width() / this.I) / 0.51f), ((float) height) / ((cropWindowRect.height() / this.I) / 0.51f)));
                                    if (!this.z) {
                                        f3 = 1.0f;
                                    }
                                    if (f3 > 0.0f && f3 != this.I) {
                                        if (z3) {
                                            if (this.g == null) {
                                                this.g = new mw4(this.a, this.b);
                                            }
                                            this.g.b(this.e, this.c);
                                        }
                                        this.I = f3;
                                        a((float) width, (float) height, true, z3);
                                    }
                                }
                            }
                            f3 = f2;
                            if (!this.z) {
                            }
                            if (z3) {
                            }
                            this.I = f3;
                            a((float) width, (float) height, true, z3);
                        }
                    }
                }
                f2 = 0.0f;
                if (this.I > 1.0f) {
                }
                f3 = f2;
                if (!this.z) {
                }
                if (z3) {
                }
                this.I = f3;
                a((float) width, (float) height, true, z3);
            }
            h hVar = this.D;
            if (hVar != null && !z2) {
                hVar.a();
            }
        }
    }

    @DexIgnore
    public final void a(float f2, float f3, boolean z2, boolean z3) {
        float f4;
        if (this.h != null) {
            float f5 = 0.0f;
            if (f2 > 0.0f && f3 > 0.0f) {
                this.c.invert(this.d);
                RectF cropWindowRect = this.b.getCropWindowRect();
                this.d.mapRect(cropWindowRect);
                this.c.reset();
                this.c.postTranslate((f2 - ((float) this.h.getWidth())) / 2.0f, (f3 - ((float) this.h.getHeight())) / 2.0f);
                f();
                int i2 = this.o;
                if (i2 > 0) {
                    this.c.postRotate((float) i2, lw4.b(this.e), lw4.c(this.e));
                    f();
                }
                float min = Math.min(f2 / lw4.h(this.e), f3 / lw4.d(this.e));
                k kVar = this.u;
                if (kVar == k.FIT_CENTER || ((kVar == k.CENTER_INSIDE && min < 1.0f) || (min > 1.0f && this.z))) {
                    this.c.postScale(min, min, lw4.b(this.e), lw4.c(this.e));
                    f();
                }
                float f6 = this.p ? -this.I : this.I;
                float f7 = this.q ? -this.I : this.I;
                this.c.postScale(f6, f7, lw4.b(this.e), lw4.c(this.e));
                f();
                this.c.mapRect(cropWindowRect);
                if (z2) {
                    if (f2 > lw4.h(this.e)) {
                        f4 = 0.0f;
                    } else {
                        f4 = Math.max(Math.min((f2 / 2.0f) - cropWindowRect.centerX(), -lw4.e(this.e)), ((float) getWidth()) - lw4.f(this.e)) / f6;
                    }
                    this.J = f4;
                    if (f3 <= lw4.d(this.e)) {
                        f5 = Math.max(Math.min((f3 / 2.0f) - cropWindowRect.centerY(), -lw4.g(this.e)), ((float) getHeight()) - lw4.a(this.e)) / f7;
                    }
                    this.K = f5;
                } else {
                    this.J = Math.min(Math.max(this.J * f6, -cropWindowRect.left), (-cropWindowRect.right) + f2) / f6;
                    this.K = Math.min(Math.max(this.K * f7, -cropWindowRect.top), (-cropWindowRect.bottom) + f3) / f7;
                }
                this.c.postTranslate(this.J * f6, this.K * f7);
                cropWindowRect.offset(this.J * f6, this.K * f7);
                this.b.setCropWindowRect(cropWindowRect);
                f();
                this.b.invalidate();
                if (z3) {
                    this.g.a(this.e, this.c);
                    this.a.startAnimation(this.g);
                } else {
                    this.a.setImageMatrix(this.c);
                }
                a(false);
            }
        }
    }

    @DexIgnore
    public static int a(int i2, int i3, int i4) {
        if (i2 == 1073741824) {
            return i3;
        }
        return i2 == Integer.MIN_VALUE ? Math.min(i4, i3) : i4;
    }

    @DexIgnore
    public final void a(boolean z2) {
        if (this.h != null && !z2) {
            this.b.a((float) getWidth(), (float) getHeight(), (((float) this.H) * 100.0f) / lw4.h(this.f), (((float) this.H) * 100.0f) / lw4.d(this.f));
        }
        this.b.a(z2 ? null : this.e, getWidth(), getHeight());
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        this.a.clearAnimation();
        if (this.h != null && (this.t > 0 || this.G != null)) {
            this.h.recycle();
        }
        this.h = null;
        this.h = bitmap;
        this.a.setImageBitmap(this.h);
    }
}
