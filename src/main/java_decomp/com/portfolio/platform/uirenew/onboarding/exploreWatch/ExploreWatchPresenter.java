package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import android.content.Context;
import com.fossil.af6;
import com.fossil.bq5$b$a;
import com.fossil.bq5$c$a;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xp5;
import com.fossil.yp5;
import com.fossil.zl6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ExploreWatchPresenter extends xp5 {
    @DexIgnore
    public /* final */ yp5 e;
    @DexIgnore
    public /* final */ cj4 f;
    @DexIgnore
    public /* final */ DeviceRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp $portfolioApp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ExploreWatchPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(PortfolioApp portfolioApp, xe6 xe6, ExploreWatchPresenter exploreWatchPresenter) {
            super(2, xe6);
            this.$portfolioApp = portfolioApp;
            this.this$0 = exploreWatchPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$portfolioApp, xe6, this.this$0);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                bq5$b$a bq5_b_a = new bq5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, bq5_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.$portfolioApp.a(this.this$0.f, false, 13);
            this.this$0.j().A0();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$start$1", f = "ExploreWatchPresenter.kt", l = {40}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ExploreWatchPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ExploreWatchPresenter exploreWatchPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = exploreWatchPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                bq5$c$a bq5_c_a = new bq5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, bq5_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.j().u((List) obj);
            this.this$0.j().g();
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public ExploreWatchPresenter(yp5 yp5, cj4 cj4, DeviceRepository deviceRepository) {
        wg6.b(yp5, "mView");
        wg6.b(cj4, "mDeviceSettingFactory");
        wg6.b(deviceRepository, "mDeviceRepository");
        this.e = yp5;
        this.f = cj4;
        this.g = deviceRepository;
    }

    @DexIgnore
    public void a(boolean z) {
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(PortfolioApp.get.instance(), (xe6) null, this), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final List<Explore> i() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        explore.setTitle(jm4.a((Context) PortfolioApp.get.instance(), 2131886799));
        explore.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886796));
        explore.setExploreType(Explore.ExploreType.WRIST_FLICK);
        Explore explore2 = new Explore();
        explore2.setTitle(jm4.a((Context) PortfolioApp.get.instance(), 2131886797));
        explore2.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886794));
        explore2.setExploreType(Explore.ExploreType.DOUBLE_TAP);
        Explore explore3 = new Explore();
        explore3.setTitle(jm4.a((Context) PortfolioApp.get.instance(), 2131886800));
        explore3.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886795));
        explore3.setExploreType(Explore.ExploreType.PRESS_AND_HOLD);
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        return arrayList;
    }

    @DexIgnore
    public final yp5 j() {
        return this.e;
    }

    @DexIgnore
    public void k() {
        this.e.a(this);
    }
}
