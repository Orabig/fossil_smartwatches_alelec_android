package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import android.content.Intent;
import com.fossil.WatchFaceHelper;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ik6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.u85;
import com.fossil.u85$e$a;
import com.fossil.vi4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zi4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetDianaPresetToWatchUseCase extends m24<u85.c, u85.d, u85.b> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public DianaPreset e;
    @DexIgnore
    public /* final */ e f; // = new e();
    @DexIgnore
    public /* final */ DianaPresetRepository g;
    @DexIgnore
    public /* final */ ComplicationLastSettingRepository h;
    @DexIgnore
    public /* final */ WatchAppLastSettingRepository i;
    @DexIgnore
    public /* final */ WatchFaceRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public b(int i, ArrayList<Integer> arrayList) {
            wg6.b(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ DianaPreset a;

        @DexIgnore
        public c(DianaPreset dianaPreset) {
            wg6.b(dianaPreset, "mPreset");
            this.a = dianaPreset;
        }

        @DexIgnore
        public final DianaPreset a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements BleCommandResultManager.b {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r11v12, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (SetDianaPresetToWatchUseCase.this.d()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetDianaPresetToWatchUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_PRESET_APPS_DATA) {
                    SetDianaPresetToWatchUseCase.this.a(false);
                    if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                        FLogger.INSTANCE.getLocal().d("SetDianaPresetToWatchUseCase", "onReceive - success");
                        SetDianaPresetToWatchUseCase.this.a(new d());
                        return;
                    }
                    FLogger.INSTANCE.getLocal().d("SetDianaPresetToWatchUseCase", "onReceive - failed isSettingChangedOnly");
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra2);
                    }
                    DianaPreset e = SetDianaPresetToWatchUseCase.this.e();
                    if (e != null) {
                        if (ik6.b(SetDianaPresetToWatchUseCase.this.b(), (af6) null, (ll6) null, new u85$e$a(e, (xe6) null, this, intExtra2, integerArrayListExtra), 3, (Object) null) != null) {
                            return;
                        }
                    }
                    SetDianaPresetToWatchUseCase.this.a(new b(intExtra2, integerArrayListExtra));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase", f = "SetDianaPresetToWatchUseCase.kt", l = {86}, m = "run")
    public static final class f extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SetDianaPresetToWatchUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = setDianaPresetToWatchUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((u85.c) null, (xe6<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase", f = "SetDianaPresetToWatchUseCase.kt", l = {96}, m = "setPresetToDb")
    public static final class g extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SetDianaPresetToWatchUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = setDianaPresetToWatchUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((DianaPreset) null, (xe6<? super cd6>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public SetDianaPresetToWatchUseCase(DianaPresetRepository dianaPresetRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, WatchFaceRepository watchFaceRepository) {
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        wg6.b(complicationLastSettingRepository, "mLastSettingRepository");
        wg6.b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        wg6.b(watchFaceRepository, "watchFaceRepository");
        this.g = dianaPresetRepository;
        this.h = complicationLastSettingRepository;
        this.i = watchAppLastSettingRepository;
        this.j = watchFaceRepository;
    }

    @DexIgnore
    public String c() {
        return "SetDianaPresetToWatchUseCase";
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final DianaPreset e() {
        return this.e;
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.SET_PRESET_APPS_DATA);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.SET_PRESET_APPS_DATA);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase} */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00e2, code lost:
        if (com.fossil.hf6.a(com.portfolio.platform.PortfolioApp.get.instance().a(r2, r1, r9, r3)) == null) goto L_0x00e6;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(u85.c cVar, xe6<Object> xe6) {
        f fVar;
        int i2;
        CoroutineUseCase coroutineUseCase;
        BackgroundConfig backgroundConfig;
        String str;
        WatchAppMappingSettings watchAppMappingSettings;
        ComplicationAppMappingSettings complicationAppMappingSettings;
        SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase;
        if (xe6 instanceof f) {
            fVar = (f) xe6;
            int i3 = fVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                fVar.label = i3 - Integer.MIN_VALUE;
                Object obj = fVar.result;
                Object a2 = ff6.a();
                i2 = fVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d("SetDianaPresetToWatchUseCase", "executeUseCase");
                    if (cVar != null) {
                        this.d = true;
                        DianaPreset a3 = cVar.a();
                        this.e = this.g.getActivePresetBySerial(PortfolioApp.get.instance().e());
                        String e2 = PortfolioApp.get.instance().e();
                        WatchAppMappingSettings b2 = zi4.b(a3.getWatchapps(), new Gson());
                        ComplicationAppMappingSettings a4 = zi4.a(a3.getComplications(), new Gson());
                        WatchFace watchFaceWithId = this.j.getWatchFaceWithId(a3.getWatchFaceId());
                        BackgroundConfig loadBackgroundConfig = watchFaceWithId != null ? WatchFaceHelper.loadBackgroundConfig(watchFaceWithId, a3.getComplications()) : null;
                        fVar.L$0 = this;
                        fVar.L$1 = cVar;
                        fVar.L$2 = cVar;
                        fVar.L$3 = a3;
                        fVar.L$4 = e2;
                        fVar.L$5 = b2;
                        fVar.L$6 = a4;
                        fVar.L$7 = loadBackgroundConfig;
                        fVar.label = 1;
                        if (a(a3, (xe6<? super cd6>) fVar) == a2) {
                            return a2;
                        }
                        setDianaPresetToWatchUseCase = this;
                        str = e2;
                        watchAppMappingSettings = b2;
                        complicationAppMappingSettings = a4;
                        backgroundConfig = loadBackgroundConfig;
                    } else {
                        coroutineUseCase = this;
                        coroutineUseCase.a(new b(-1, new ArrayList()));
                        return new Object();
                    }
                } else if (i2 == 1) {
                    backgroundConfig = (BackgroundConfig) fVar.L$7;
                    complicationAppMappingSettings = (ComplicationAppMappingSettings) fVar.L$6;
                    watchAppMappingSettings = (WatchAppMappingSettings) fVar.L$5;
                    str = (String) fVar.L$4;
                    DianaPreset dianaPreset = (DianaPreset) fVar.L$3;
                    c cVar2 = (c) fVar.L$2;
                    c cVar3 = (c) fVar.L$1;
                    nc6.a(obj);
                    setDianaPresetToWatchUseCase = (SetDianaPresetToWatchUseCase) fVar.L$0;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                coroutineUseCase = setDianaPresetToWatchUseCase;
            }
        }
        fVar = new f(this, xe6);
        Object obj2 = fVar.result;
        Object a22 = ff6.a();
        i2 = fVar.label;
        if (i2 != 0) {
        }
        coroutineUseCase = setDianaPresetToWatchUseCase;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final /* synthetic */ Object a(DianaPreset dianaPreset, xe6<? super cd6> xe6) {
        g gVar;
        int i2;
        SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase;
        Iterator<DianaPresetComplicationSetting> it;
        Iterator<DianaPresetWatchAppSetting> it2;
        if (xe6 instanceof g) {
            gVar = (g) xe6;
            int i3 = gVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                gVar.label = i3 - Integer.MIN_VALUE;
                Object obj = gVar.result;
                Object a2 = ff6.a();
                i2 = gVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("SetDianaPresetToWatchUseCase", "setPresetToDb " + dianaPreset);
                    dianaPreset.setActive(true);
                    DianaPresetRepository dianaPresetRepository = this.g;
                    gVar.L$0 = this;
                    gVar.L$1 = dianaPreset;
                    gVar.label = 1;
                    if (dianaPresetRepository.upsertPreset(dianaPreset, gVar) == a2) {
                        return a2;
                    }
                    setDianaPresetToWatchUseCase = this;
                } else if (i2 == 1) {
                    dianaPreset = (DianaPreset) gVar.L$1;
                    setDianaPresetToWatchUseCase = (SetDianaPresetToWatchUseCase) gVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Calendar instance = Calendar.getInstance();
                wg6.a((Object) instance, "Calendar.getInstance()");
                String u = bk4.u(instance.getTime());
                it = dianaPreset.getComplications().iterator();
                while (it.hasNext()) {
                    DianaPresetComplicationSetting next = it.next();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("SetDianaPresetToWatchUseCase", "setPresetToWatch success save last user setting " + next.getSettings());
                    if (!vi4.a(next.getSettings())) {
                        ComplicationLastSettingRepository complicationLastSettingRepository = setDianaPresetToWatchUseCase.h;
                        String id = next.getId();
                        wg6.a((Object) u, "updatedAt");
                        String settings = next.getSettings();
                        if (settings == null) {
                            settings = "";
                        }
                        complicationLastSettingRepository.upsertComplicationLastSetting(new ComplicationLastSetting(id, u, settings));
                    }
                }
                it2 = dianaPreset.getWatchapps().iterator();
                while (it2.hasNext()) {
                    DianaPresetWatchAppSetting next2 = it2.next();
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("SetDianaPresetToWatchUseCase", "setPresetToWatch success save last user setting " + next2.getSettings());
                    if (!vi4.a(next2.getSettings())) {
                        WatchAppLastSettingRepository watchAppLastSettingRepository = setDianaPresetToWatchUseCase.i;
                        String id2 = next2.getId();
                        wg6.a((Object) u, "updatedAt");
                        String settings2 = next2.getSettings();
                        if (settings2 == null) {
                            settings2 = "";
                        }
                        watchAppLastSettingRepository.upsertWatchAppLastSetting(new WatchAppLastSetting(id2, u, settings2));
                    }
                }
                return cd6.a;
            }
        }
        gVar = new g(this, xe6);
        Object obj2 = gVar.result;
        Object a22 = ff6.a();
        i2 = gVar.label;
        if (i2 != 0) {
        }
        Calendar instance2 = Calendar.getInstance();
        wg6.a((Object) instance2, "Calendar.getInstance()");
        String u2 = bk4.u(instance2.getTime());
        it = dianaPreset.getComplications().iterator();
        while (it.hasNext()) {
        }
        it2 = dianaPreset.getWatchapps().iterator();
        while (it2.hasNext()) {
        }
        return cd6.a;
    }
}
