package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.hn5;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jh6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeBackgroundViewModel extends td {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<hn5.b> a; // = new MutableLiveData<>();
    @DexIgnore
    public b b; // = new b((Integer) null, (Integer) null, 3, (qg6) null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;

        @DexIgnore
        public b() {
            this((Integer) null, (Integer) null, 3, (qg6) null);
        }

        @DexIgnore
        public b(Integer num, Integer num2) {
            this.a = num;
            this.b = num2;
        }

        @DexIgnore
        public final Integer a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.a;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(Integer num, Integer num2, int i, qg6 qg6) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2);
        }

        @DexIgnore
        public final void a(Integer num, Integer num2) {
            this.a = num;
            this.b = num2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeBackgroundViewModel$saveColor$1", f = "CustomizeBackgroundViewModel.kt", l = {45, 46, 58}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $backgroundColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeBackgroundViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CustomizeBackgroundViewModel customizeBackgroundViewModel, String str, String str2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeBackgroundViewModel;
            this.$id = str;
            this.$backgroundColor = str2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$id, this.$backgroundColor, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0090  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x009f  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00fd A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0106  */
        public final Object invokeSuspend(T t) {
            T t2;
            jh6 jh6;
            il6 il6;
            jh6 jh62;
            T a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(t);
                il6 il62 = this.p$;
                jh62 = new jh6();
                ThemeRepository b = this.this$0.c;
                String str = this.$id;
                this.L$0 = il62;
                this.L$1 = jh62;
                this.L$2 = jh62;
                this.label = 1;
                T themeById = b.getThemeById(str, this);
                if (themeById == a) {
                    return a;
                }
                jh6 = jh62;
                T t3 = themeById;
                il6 = il62;
                t = t3;
            } else if (i == 1) {
                jh62 = (jh6) this.L$2;
                nc6.a(t);
                il6 il63 = (il6) this.L$0;
                jh6 = (jh6) this.L$1;
                il6 = il63;
            } else if (i == 2) {
                jh62 = (jh6) this.L$2;
                il6 = (il6) this.L$0;
                nc6.a(t);
                jh6 = (jh6) this.L$1;
                if (t == null) {
                    t2 = (Theme) t;
                    jh62.element = t2;
                    jh6 jh63 = new jh6();
                    jh63.element = null;
                    if (this.$backgroundColor != null) {
                        for (Style style : ((Theme) jh6.element).getStyles()) {
                            if (wg6.a((Object) style.getKey(), (Object) Explore.COLUMN_BACKGROUND)) {
                                style.setValue(this.$backgroundColor);
                                jh63.element = hf6.a(Color.parseColor(this.$backgroundColor));
                            }
                        }
                    }
                    b c = this.this$0.b;
                    T t4 = jh63.element;
                    c.a((Integer) t4, (Integer) t4);
                    this.L$0 = il6;
                    this.L$1 = jh6;
                    this.L$2 = jh63;
                    this.label = 3;
                    if (this.this$0.c.upsertUserTheme((Theme) jh6.element, this) == a) {
                        return a;
                    }
                    this.this$0.a();
                    return cd6.a;
                }
                wg6.a();
                throw null;
            } else if (i == 3) {
                jh6 jh64 = (jh6) this.L$2;
                jh6 jh65 = (jh6) this.L$1;
                il6 il64 = (il6) this.L$0;
                nc6.a(t);
                this.this$0.a();
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            t2 = (Theme) t;
            if (t2 == null) {
                ThemeRepository b2 = this.this$0.c;
                this.L$0 = il6;
                this.L$1 = jh6;
                this.L$2 = jh62;
                this.label = 2;
                t = b2.getCurrentTheme(this);
                if (t == a) {
                    return a;
                }
                if (t == null) {
                }
            }
            jh62.element = t2;
            jh6 jh632 = new jh6();
            jh632.element = null;
            if (this.$backgroundColor != null) {
            }
            b c2 = this.this$0.b;
            T t42 = jh632.element;
            c2.a((Integer) t42, (Integer) t42);
            this.L$0 = il6;
            this.L$1 = jh6;
            this.L$2 = jh632;
            this.label = 3;
            if (this.this$0.c.upsertUserTheme((Theme) jh6.element, this) == a) {
            }
            this.this$0.a();
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = CustomizeBackgroundViewModel.class.getSimpleName();
        wg6.a((Object) simpleName, "CustomizeBackgroundViewM\u2026el::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public CustomizeBackgroundViewModel(ThemeRepository themeRepository) {
        wg6.b(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.b);
    }

    @DexIgnore
    public final MutableLiveData<hn5.b> b() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        int i;
        String a2 = CustomizeBackgroundFragment.p.a();
        if (a2 != null) {
            i = Color.parseColor(a2);
        } else {
            i = Color.parseColor(e);
        }
        this.b.a(Integer.valueOf(i), Integer.valueOf(i));
        a();
    }

    @DexIgnore
    public final void a(int i, int i2) {
        if (i == 301) {
            this.b.a(Integer.valueOf(i2), Integer.valueOf(i2));
            a();
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wg6.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = d;
        local.d(str3, "saveColor backgroundColor=" + str2);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new c(this, str, str2, (xe6) null), 3, (Object) null);
    }
}
