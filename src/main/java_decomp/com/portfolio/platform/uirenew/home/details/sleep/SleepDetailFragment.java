package com.portfolio.platform.uirenew.home.details.sleep;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.ej5;
import com.fossil.fj5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.nh6;
import com.fossil.oj3;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tk4;
import com.fossil.wg6;
import com.fossil.xi5;
import com.fossil.yi5;
import com.fossil.yk4;
import com.fossil.zd4;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleProgressBar;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDetailFragment extends BaseFragment implements yi5, View.OnClickListener {
    @DexIgnore
    public static /* final */ a B; // = new a((qg6) null);
    @DexIgnore
    public HashMap A;
    @DexIgnore
    public /* final */ Calendar f; // = Calendar.getInstance();
    @DexIgnore
    public ax5<zd4> g;
    @DexIgnore
    public xi5 h;
    @DexIgnore
    public fj5 i;
    @DexIgnore
    public ej5 j;
    @DexIgnore
    public int o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ String y;
    @DexIgnore
    public /* final */ String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final SleepDetailFragment a(Date date) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            SleepDetailFragment sleepDetailFragment = new SleepDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            sleepDetailFragment.setArguments(bundle);
            return sleepDetailFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements oj3.b {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailFragment a;

        @DexIgnore
        public b(SleepDetailFragment sleepDetailFragment, zd4 zd4) {
            this.a = sleepDetailFragment;
        }

        @DexIgnore
        public final void a(TabLayout.g gVar, int i) {
            wg6.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.y) && !TextUtils.isEmpty(this.a.z)) {
                int parseColor = Color.parseColor(this.a.y);
                int parseColor2 = Color.parseColor(this.a.z);
                gVar.b(2131231004);
                if (i == this.a.o) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ SleepDetailFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ zd4 b;

        @DexIgnore
        public c(SleepDetailFragment sleepDetailFragment, zd4 zd4) {
            this.a = sleepDetailFragment;
            this.b = zd4;
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailFragment", "onPageScrolled " + i);
            SleepDetailFragment.super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.a.z)) {
                int parseColor = Color.parseColor(this.a.z);
                TabLayout.g b4 = this.b.u.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.y) && this.a.o != i) {
                int parseColor2 = Color.parseColor(this.a.y);
                TabLayout.g b5 = this.b.u.b(this.a.o);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.a.o = i;
            ViewPager2 viewPager2 = this.b.J;
            wg6.a((Object) viewPager2, "binding.rvHeartRateSleep");
            viewPager2.setCurrentItem(i);
        }
    }

    @DexIgnore
    public SleepDetailFragment() {
        String b2 = ThemeManager.l.a().b("nonBrandSurface");
        this.s = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("backgroundDashboard");
        this.t = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("secondaryText");
        this.u = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("primaryText");
        this.v = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
        String b6 = ThemeManager.l.a().b("nonBrandDisableCalendarDay");
        this.w = Color.parseColor(b6 == null ? "#FFFFFF" : b6);
        String b7 = ThemeManager.l.a().b("nonBrandNonReachGoal");
        this.x = Color.parseColor(b7 == null ? "#FFFFFF" : b7);
        this.y = ThemeManager.l.a().b("disabledButton");
        this.z = ThemeManager.l.a().b("primaryColor");
        ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
    }

    @DexIgnore
    public void c0() {
        zd4 a2;
        ConstraintLayout constraintLayout;
        ax5<zd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null && (constraintLayout = a2.r) != null) {
            constraintLayout.setVisibility(8);
        }
    }

    @DexIgnore
    public void d(ArrayList<ej5.a> arrayList) {
        zd4 a2;
        ConstraintLayout constraintLayout;
        wg6.b(arrayList, "sleepHeartRateDatumData");
        ax5<zd4> ax5 = this.g;
        if (!(ax5 == null || (a2 = ax5.a()) == null || (constraintLayout = a2.r) == null)) {
            constraintLayout.setVisibility(0);
        }
        ej5 ej5 = this.j;
        if (ej5 != null) {
            ej5.a(arrayList);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.A;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "SleepDetailFragment";
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("SleepDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362542:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362543:
                    xi5 xi5 = this.h;
                    if (xi5 != null) {
                        xi5.j();
                        return;
                    }
                    return;
                case 2131362609:
                    xi5 xi52 = this.h;
                    if (xi52 != null) {
                        xi52.i();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        zd4 a2;
        wg6.b(layoutInflater, "inflater");
        SleepDetailFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        zd4 a3 = kb.a(layoutInflater, 2131558610, viewGroup, false, e1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        Date date = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            date = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        Calendar calendar = this.f;
        wg6.a((Object) calendar, "mCalendar");
        calendar.setTime(date);
        wg6.a((Object) a3, "binding");
        a(a3);
        this.g = new ax5<>(this, a3);
        ax5<zd4> ax5 = this.g;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        SleepDetailFragment.super.onPause();
        xi5 xi5 = this.h;
        if (xi5 != null) {
            xi5.g();
        }
    }

    @DexIgnore
    public void onResume() {
        SleepDetailFragment.super.onResume();
        xi5 xi5 = this.h;
        if (xi5 != null) {
            Calendar calendar = this.f;
            wg6.a((Object) calendar, "mCalendar");
            Date time = calendar.getTime();
            wg6.a((Object) time, "mCalendar.time");
            xi5.a(time);
        }
        xi5 xi52 = this.h;
        if (xi52 != null) {
            xi52.f();
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        xi5 xi5 = this.h;
        if (xi5 != null) {
            xi5.a(bundle);
        }
        SleepDetailFragment.super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public void p(List<fj5.b> list) {
        zd4 a2;
        TabLayout tabLayout;
        zd4 a3;
        TabLayout tabLayout2;
        wg6.b(list, "listOfSleepSessionUIData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDayDetailChart - baseModel=" + list);
        if (list.size() > 1) {
            ax5<zd4> ax5 = this.g;
            if (!(ax5 == null || (a3 = ax5.a()) == null || (tabLayout2 = a3.u) == null)) {
                tabLayout2.setVisibility(0);
            }
        } else {
            ax5<zd4> ax52 = this.g;
            if (!(ax52 == null || (a2 = ax52.a()) == null || (tabLayout = a2.u) == null)) {
                tabLayout.setVisibility(8);
            }
        }
        fj5 fj5 = this.i;
        if (fj5 != null) {
            fj5.a(list);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v2, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public final void a(zd4 zd4) {
        String str;
        zd4.D.setOnClickListener(this);
        zd4.E.setOnClickListener(this);
        zd4.F.setOnClickListener(this);
        zd4.r.setBackgroundColor(this.t);
        zd4.q.setBackgroundColor(this.s);
        zd4.t.setBackgroundColor(this.s);
        xi5 xi5 = this.h;
        if ((xi5 != null ? xi5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            str = ThemeManager.l.a().b("dianaSleepTab");
        } else {
            str = ThemeManager.l.a().b("hybridSleepTab");
        }
        this.p = str;
        this.q = ThemeManager.l.a().b("nonBrandActivityDetailBackground");
        this.r = ThemeManager.l.a().b("onDianaSleepTab");
        this.i = new fj5(new ArrayList());
        ViewPager2 viewPager2 = zd4.K;
        wg6.a((Object) viewPager2, "rvSleeps");
        viewPager2.setAdapter(this.i);
        TabLayout tabLayout = zd4.u;
        wg6.a((Object) tabLayout, "binding.cpiSleep");
        tabLayout.setBackgroundTintList(ColorStateList.valueOf(this.s));
        new oj3(zd4.u, zd4.K, new b(this, zd4)).a();
        viewPager2.a(new c(this, zd4));
        viewPager2.setCurrentItem(this.o);
        this.j = new ej5(new ArrayList());
        ViewPager2 viewPager22 = zd4.J;
        wg6.a((Object) viewPager22, "rvHeartRateSleep");
        viewPager22.setAdapter(this.j);
        viewPager22.setCurrentItem(this.o);
        viewPager22.setUserInputEnabled(true);
    }

    @DexIgnore
    public void a(xi5 xi5) {
        wg6.b(xi5, "presenter");
        this.h = xi5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v3, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v7, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v8, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void a(Date date, boolean z2, boolean z3, boolean z4) {
        zd4 a2;
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDay - date=" + date + ", isCreateAt=" + z2 + ", isToday=" + z3 + ", isDateAfter=" + z4);
        Calendar calendar = this.f;
        wg6.a((Object) calendar, "mCalendar");
        calendar.setTime(date);
        int i2 = this.f.get(7);
        ax5<zd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            Object r1 = a2.z;
            wg6.a((Object) r1, "binding.ftvDayOfMonth");
            r1.setText(String.valueOf(this.f.get(5)));
            if (z2) {
                Object r6 = a2.E;
                wg6.a((Object) r6, "binding.ivBackDate");
                r6.setVisibility(4);
            } else {
                Object r62 = a2.E;
                wg6.a((Object) r62, "binding.ivBackDate");
                r62.setVisibility(0);
            }
            if (z3 || z4) {
                Object r8 = a2.F;
                wg6.a((Object) r8, "binding.ivNextDate");
                r8.setVisibility(8);
                if (z3) {
                    Object r5 = a2.A;
                    wg6.a((Object) r5, "binding.ftvDayOfWeek");
                    r5.setText(jm4.a(getContext(), 2131886449));
                    return;
                }
                Object r63 = a2.A;
                wg6.a((Object) r63, "binding.ftvDayOfWeek");
                r63.setText(yk4.b.b(i2));
                return;
            }
            Object r7 = a2.F;
            wg6.a((Object) r7, "binding.ivNextDate");
            r7.setVisibility(0);
            Object r64 = a2.A;
            wg6.a((Object) r64, "binding.ftvDayOfWeek");
            r64.setText(yk4.b.b(i2));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v17, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v18, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v7, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v8, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v29, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v31, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v32, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v33, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v35, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v36, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v12, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v13, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v40, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v41, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v42, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v43, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v44, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v45, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v46, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v47, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v49, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v50, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v52, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v53, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r6v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r6v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r6v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r6v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r6v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r6v23, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v24, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v26, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v27, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v31, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v33, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v35, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v36, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(MFSleepDay mFSleepDay) {
        zd4 a2;
        int i2;
        int i3;
        MFSleepDay mFSleepDay2 = mFSleepDay;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDayDetail - sleepDay=" + mFSleepDay2);
        ax5<zd4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            wg6.a((Object) a2, "binding");
            View d = a2.d();
            wg6.a((Object) d, "binding.root");
            Context context = d.getContext();
            if (mFSleepDay2 != null) {
                i2 = mFSleepDay.getSleepMinutes();
                i3 = mFSleepDay.getGoalMinutes();
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (i2 > 0) {
                int i4 = i2 / 60;
                int i5 = i2 - (i4 * 60);
                Object r4 = a2.x;
                wg6.a((Object) r4, "binding.ftvDailyValue");
                r4.setText(String.valueOf(i4));
                Object r42 = a2.v;
                wg6.a((Object) r42, "binding.ftvDailyUnit");
                String a3 = jm4.a(context, 2131886653);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026Abbreviations_Hours__Hrs)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    r42.setText(lowerCase);
                    if (i5 > 0) {
                        Object r43 = a2.y;
                        wg6.a((Object) r43, "binding.ftvDailyValue2");
                        nh6 nh6 = nh6.a;
                        Locale locale = Locale.US;
                        wg6.a((Object) locale, "Locale.US");
                        Object[] objArr = {Integer.valueOf(i5)};
                        String format = String.format(locale, "%02d", Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        r43.setText(format);
                        Object r44 = a2.w;
                        wg6.a((Object) r44, "binding.ftvDailyUnit2");
                        String a4 = jm4.a(context, 2131886655);
                        wg6.a((Object) a4, "LanguageHelper.getString\u2026reviations_Minutes__Mins)");
                        if (a4 != null) {
                            String lowerCase2 = a4.toLowerCase();
                            wg6.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                            r44.setText(lowerCase2);
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        Object r45 = a2.y;
                        wg6.a((Object) r45, "binding.ftvDailyValue2");
                        r45.setText("");
                        Object r46 = a2.w;
                        wg6.a((Object) r46, "binding.ftvDailyUnit2");
                        r46.setText("");
                    }
                    ViewPager2 viewPager2 = a2.K;
                    wg6.a((Object) viewPager2, "binding.rvSleeps");
                    viewPager2.setVisibility(0);
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                Object r47 = a2.x;
                wg6.a((Object) r47, "binding.ftvDailyValue");
                r47.setText("");
                Object r48 = a2.v;
                wg6.a((Object) r48, "binding.ftvDailyUnit");
                String a5 = jm4.a(context, 2131886494);
                wg6.a((Object) a5, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                if (a5 != null) {
                    String upperCase = a5.toUpperCase();
                    wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    r48.setText(upperCase);
                    Object r49 = a2.y;
                    wg6.a((Object) r49, "binding.ftvDailyValue2");
                    r49.setText("");
                    Object r410 = a2.w;
                    wg6.a((Object) r410, "binding.ftvDailyUnit2");
                    r410.setText("");
                    ViewPager2 viewPager22 = a2.K;
                    wg6.a((Object) viewPager22, "binding.rvSleeps");
                    viewPager22.setVisibility(8);
                    TabLayout tabLayout = a2.u;
                    wg6.a((Object) tabLayout, "binding.cpiSleep");
                    tabLayout.setVisibility(8);
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            int i6 = i3 > 0 ? (i2 * 100) / i3 : -1;
            if (i2 >= i3 && i3 > 0) {
                a2.A.setTextColor(this.s);
                a2.z.setTextColor(this.s);
                a2.v.setTextColor(this.s);
                a2.x.setTextColor(this.s);
                a2.w.setTextColor(this.s);
                a2.y.setTextColor(this.s);
                Object r5 = a2.F;
                wg6.a((Object) r5, "binding.ivNextDate");
                r5.setSelected(true);
                Object r52 = a2.E;
                wg6.a((Object) r52, "binding.ivBackDate");
                r52.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                wg6.a((Object) constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                Object r53 = a2.A;
                wg6.a((Object) r53, "binding.ftvDayOfWeek");
                r53.setSelected(true);
                Object r54 = a2.z;
                wg6.a((Object) r54, "binding.ftvDayOfMonth");
                r54.setSelected(true);
                View view = a2.G;
                wg6.a((Object) view, "binding.line");
                view.setSelected(true);
                Object r55 = a2.x;
                wg6.a((Object) r55, "binding.ftvDailyValue");
                r55.setSelected(true);
                Object r56 = a2.v;
                wg6.a((Object) r56, "binding.ftvDailyUnit");
                r56.setSelected(true);
                String str = this.r;
                if (str != null) {
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.x.setTextColor(Color.parseColor(str));
                    a2.v.setTextColor(Color.parseColor(str));
                    a2.y.setTextColor(Color.parseColor(str));
                    a2.w.setTextColor(Color.parseColor(str));
                    a2.G.setBackgroundColor(Color.parseColor(str));
                    a2.F.setColorFilter(Color.parseColor(str));
                    a2.E.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.p;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i2 > 0) {
                a2.z.setTextColor(this.v);
                a2.A.setTextColor(this.u);
                a2.v.setTextColor(this.x);
                a2.x.setTextColor(this.v);
                a2.w.setTextColor(this.x);
                a2.y.setTextColor(this.v);
                View view2 = a2.G;
                wg6.a((Object) view2, "binding.line");
                view2.setSelected(false);
                Object r57 = a2.F;
                wg6.a((Object) r57, "binding.ivNextDate");
                r57.setSelected(false);
                Object r58 = a2.E;
                wg6.a((Object) r58, "binding.ivBackDate");
                r58.setSelected(false);
                int i7 = this.x;
                a2.G.setBackgroundColor(i7);
                a2.F.setColorFilter(i7);
                a2.E.setColorFilter(i7);
                String str3 = this.q;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.z.setTextColor(this.v);
                a2.A.setTextColor(this.u);
                a2.x.setTextColor(this.w);
                a2.v.setTextColor(this.w);
                a2.w.setTextColor(this.w);
                a2.y.setTextColor(this.w);
                Object r59 = a2.F;
                wg6.a((Object) r59, "binding.ivNextDate");
                r59.setSelected(false);
                Object r510 = a2.E;
                wg6.a((Object) r510, "binding.ivBackDate");
                r510.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                wg6.a((Object) constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                Object r511 = a2.A;
                wg6.a((Object) r511, "binding.ftvDayOfWeek");
                r511.setSelected(false);
                Object r512 = a2.z;
                wg6.a((Object) r512, "binding.ftvDayOfMonth");
                r512.setSelected(false);
                View view3 = a2.G;
                wg6.a((Object) view3, "binding.line");
                view3.setSelected(false);
                Object r513 = a2.x;
                wg6.a((Object) r513, "binding.ftvDailyValue");
                r513.setSelected(false);
                Object r514 = a2.v;
                wg6.a((Object) r514, "binding.ftvDailyUnit");
                r514.setSelected(false);
                int i8 = this.x;
                a2.G.setBackgroundColor(i8);
                a2.F.setColorFilter(i8);
                a2.E.setColorFilter(i8);
                String str4 = this.q;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i6 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.H;
                wg6.a((Object) flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                Object r411 = a2.C;
                wg6.a((Object) r411, "binding.ftvProgressValue");
                r411.setText(jm4.a(context, 2131887067));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.H;
                wg6.a((Object) flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i6);
                Object r515 = a2.C;
                wg6.a((Object) r515, "binding.ftvProgressValue");
                r515.setText(i6 + "%");
            }
            if (i3 < 60) {
                Object r2 = a2.B;
                wg6.a((Object) r2, "binding.ftvGoalValue");
                nh6 nh62 = nh6.a;
                String a6 = jm4.a(context, 2131886404);
                wg6.a((Object) a6, "LanguageHelper.getString\u2026Page_Title__OfNumberMins)");
                Object[] objArr2 = {Integer.valueOf(i3)};
                String format2 = String.format(a6, Arrays.copyOf(objArr2, objArr2.length));
                wg6.a((Object) format2, "java.lang.String.format(format, *args)");
                r2.setText(format2);
                return;
            }
            Object r22 = a2.B;
            wg6.a((Object) r22, "binding.ftvGoalValue");
            nh6 nh63 = nh6.a;
            String a7 = jm4.a(context, 2131886546);
            wg6.a((Object) a7, "LanguageHelper.getString\u2026ecord_Title__OfNumberHrs)");
            Object[] objArr3 = {tk4.a(((float) i3) / 60.0f, 1)};
            String format3 = String.format(a7, Arrays.copyOf(objArr3, objArr3.length));
            wg6.a((Object) format3, "java.lang.String.format(format, *args)");
            r22.setText(format3);
        }
    }
}
