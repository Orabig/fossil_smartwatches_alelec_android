package com.portfolio.platform.uirenew.home.alerts.hybrid;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.f15;
import com.fossil.g15;
import com.fossil.h15$c$a;
import com.fossil.hi4;
import com.fossil.ik6;
import com.fossil.ld;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.p06;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.uh4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zv4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsHybridPresenter extends f15 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.get.instance().f();
    @DexIgnore
    public ArrayList<Alarm> f; // = new ArrayList<>();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public volatile boolean h;
    @DexIgnore
    public /* final */ g15 i;
    @DexIgnore
    public /* final */ AlarmHelper j;
    @DexIgnore
    public /* final */ SetAlarms k;
    @DexIgnore
    public /* final */ AlarmsRepository l;
    @DexIgnore
    public /* final */ an4 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HomeAlertsHybridPresenter.n;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<zv4.d, zv4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public b(HomeAlertsHybridPresenter homeAlertsHybridPresenter, Alarm alarm) {
            this.a = homeAlertsHybridPresenter;
            this.b = alarm;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetAlarms.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsHybridPresenter.o.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + dVar.a().getUri() + ", alarmId = " + dVar.a().getId());
            this.a.i.a();
            this.a.b(this.b, true);
        }

        @DexIgnore
        public void a(SetAlarms.b bVar) {
            wg6.b(bVar, "errorValue");
            this.a.i.a();
            int c = bVar.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsHybridPresenter.o.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.i.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.i.w();
                }
                this.a.b(bVar.a(), false);
                return;
            }
            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(bVar.b());
            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            g15 h = this.a.i;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
            if (array != null) {
                uh4[] uh4Arr = (uh4[]) array;
                h.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                this.a.b(bVar.a(), false);
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;

        @DexIgnore
        public c(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            this.a = homeAlertsHybridPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            if (str == null || str.length() == 0) {
                this.a.i.a(true);
            } else {
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new h15$c$a(this, str, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridPresenter a;

        @DexIgnore
        public d(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            this.a = homeAlertsHybridPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            g15 unused = this.a.i;
        }
    }

    /*
    static {
        String simpleName = HomeAlertsHybridPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "HomeAlertsHybridPresenter::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public HomeAlertsHybridPresenter(g15 g15, AlarmHelper alarmHelper, SetAlarms setAlarms, AlarmsRepository alarmsRepository, an4 an4) {
        wg6.b(g15, "mView");
        wg6.b(alarmHelper, "mAlarmHelper");
        wg6.b(setAlarms, "mSetAlarms");
        wg6.b(alarmsRepository, "mAlarmRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        this.i = g15;
        this.j = alarmHelper;
        this.k = setAlarms;
        this.l = alarmsRepository;
        this.m = an4;
    }

    @DexIgnore
    public void j() {
        this.i.a(this);
    }

    @DexIgnore
    @p06
    public final void onSetAlarmEventEndComplete(hi4 hi4) {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmEventEndComplete()");
        if (hi4 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + hi4);
            if (hi4.b()) {
                String a2 = hi4.a();
                Iterator<Alarm> it = this.f.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (wg6.a((Object) next.getUri(), (Object) a2)) {
                        next.setActive(false);
                    }
                }
                this.i.s();
            }
        }
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(n, "start");
        this.k.f();
        PortfolioApp.get.b((Object) this);
        LiveData<String> liveData = this.e;
        g15 g15 = this.i;
        if (g15 != null) {
            liveData.a((HomeAlertsHybridFragment) g15, new c(this));
            BleCommandResultManager.d.a(CommunicateMode.SET_LIST_ALARM);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(n, "stop");
        this.e.b(new d(this));
        this.k.g();
        PortfolioApp.get.c(this);
    }

    @DexIgnore
    public void h() {
        this.g = !this.g;
        this.m.d(this.g);
        this.i.h(this.g);
        this.i.n(this.g);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void i() {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmsSuccess");
        this.j.d(PortfolioApp.get.instance());
        PortfolioApp instance = PortfolioApp.get.instance();
        Object a2 = this.e.a();
        if (a2 != null) {
            wg6.a(a2, "mActiveSerial.value!!");
            instance.l((String) a2);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(Alarm alarm) {
        CharSequence charSequence = (CharSequence) this.e.a();
        if (charSequence == null || charSequence.length() == 0) {
            FLogger.INSTANCE.getLocal().d(n, "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.f.size() < 32) {
            g15 g15 = this.i;
            Object a2 = this.e.a();
            if (a2 != null) {
                wg6.a(a2, "mActiveSerial.value!!");
                g15.a((String) a2, this.f, alarm);
                return;
            }
            wg6.a();
            throw null;
        } else {
            this.i.r();
        }
    }

    @DexIgnore
    public final void b(Alarm alarm, boolean z) {
        wg6.b(alarm, "editAlarm");
        Iterator<Alarm> it = this.f.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (wg6.a((Object) next.getUri(), (Object) alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.f;
                ArrayList<Alarm> arrayList2 = arrayList;
                arrayList2.set(arrayList.indexOf(next), Alarm.copy$default(alarm, (String) null, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 4095, (Object) null));
                if (!z) {
                    break;
                }
                i();
            }
            Alarm alarm2 = alarm;
        }
        this.i.c(this.f);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.alarm.usecase.SetAlarms] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$b] */
    public void a(Alarm alarm, boolean z) {
        wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        CharSequence charSequence = (CharSequence) this.e.a();
        if (!(charSequence == null || charSequence.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "enableAlarm - alarmTotalMinues: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.i.b();
            Object r6 = this.k;
            Object a2 = this.e.a();
            if (a2 != null) {
                wg6.a(a2, "mActiveSerial.value!!");
                r6.a(new SetAlarms.c((String) a2, this.f, alarm), new b(this, alarm));
                return;
            }
            wg6.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(n, "enableAlarm - Current Active Device Serial Is Empty");
    }
}
