package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.h64;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.uc5;
import com.fossil.ut4;
import com.fossil.vc5;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityOverviewWeekFragment extends BaseFragment implements vc5 {
    @DexIgnore
    public ax5<h64> f;
    @DexIgnore
    public uc5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "ActivityOverviewWeekFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        h64 a2;
        OverviewWeekChart overviewWeekChart;
        ax5<h64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewWeekChart = a2.q) != null) {
            uc5 uc5 = this.g;
            if ((uc5 != null ? uc5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                overviewWeekChart.a("dianaStepsTab", "nonBrandNonReachGoal");
            } else {
                overviewWeekChart.a("hybridStepsTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        h64 a2;
        wg6.b(layoutInflater, "inflater");
        ActivityOverviewWeekFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onCreateView");
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558503, viewGroup, false, e1()));
        j1();
        ax5<h64> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        ActivityOverviewWeekFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onResume");
        j1();
        uc5 uc5 = this.g;
        if (uc5 != null) {
            uc5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        ActivityOverviewWeekFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onStop");
        uc5 uc5 = this.g;
        if (uc5 != null) {
            uc5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(ut4 ut4) {
        h64 a2;
        OverviewWeekChart overviewWeekChart;
        wg6.b(ut4, "baseModel");
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "showWeekDetails");
        ax5<h64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            ut4.a aVar = ut4.a;
            wg6.a((Object) overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            wg6.a((Object) context, "it.context");
            BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
            overviewWeekChart.a(ut4);
        }
    }

    @DexIgnore
    public void a(uc5 uc5) {
        wg6.b(uc5, "presenter");
        this.g = uc5;
    }
}
