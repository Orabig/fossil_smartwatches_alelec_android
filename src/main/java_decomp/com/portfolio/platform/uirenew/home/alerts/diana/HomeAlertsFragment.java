package com.portfolio.platform.uirenew.home.alerts.diana;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.bu4;
import com.fossil.c34;
import com.fossil.cl4;
import com.fossil.gy5;
import com.fossil.hh4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lx5;
import com.fossil.nx4;
import com.fossil.ox4;
import com.fossil.qg6;
import com.fossil.qx4;
import com.fossil.rc6;
import com.fossil.ta4;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.y04;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsFragment extends BasePermissionFragment implements ox4, bu4, View.OnClickListener {
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public ax5<ta4> g;
    @DexIgnore
    public nx4 h;
    @DexIgnore
    public c34 i;
    @DexIgnore
    public DoNotDisturbScheduledTimeFragment j;
    @DexIgnore
    public DoNotDisturbScheduledTimePresenter o;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HomeAlertsFragment a() {
            return new HomeAlertsFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c34.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment a;

        @DexIgnore
        public b(HomeAlertsFragment homeAlertsFragment) {
            this.a = homeAlertsFragment;
        }

        @DexIgnore
        public void a(Alarm alarm) {
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            HomeAlertsFragment.b(this.a).a(alarm, !alarm.isActive());
        }

        @DexIgnore
        public void b(Alarm alarm) {
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            HomeAlertsFragment.b(this.a).a(alarm);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment a;

        @DexIgnore
        public c(HomeAlertsFragment homeAlertsFragment) {
            this.a = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                wg6.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 6, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment a;

        @DexIgnore
        public d(HomeAlertsFragment homeAlertsFragment) {
            this.a = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersActivity.C.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment a;

        @DexIgnore
        public e(HomeAlertsFragment homeAlertsFragment) {
            this.a = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            gy5.a(view);
            NotificationAppsActivity.C.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment a;

        @DexIgnore
        public f(HomeAlertsFragment homeAlertsFragment) {
            this.a = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationCallsAndMessagesActivity.C.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment a;

        @DexIgnore
        public g(HomeAlertsFragment homeAlertsFragment) {
            this.a = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeAlertsFragment.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment a;

        @DexIgnore
        public h(HomeAlertsFragment homeAlertsFragment) {
            this.a = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            DoNotDisturbScheduledTimeFragment a2 = this.a.j;
            if (a2 != null) {
                a2.r(0);
            }
            DoNotDisturbScheduledTimeFragment a3 = this.a.j;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, DoNotDisturbScheduledTimeFragment.s.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment a;

        @DexIgnore
        public i(HomeAlertsFragment homeAlertsFragment) {
            this.a = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            DoNotDisturbScheduledTimeFragment a2 = this.a.j;
            if (a2 != null) {
                a2.r(1);
            }
            DoNotDisturbScheduledTimeFragment a3 = this.a.j;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, DoNotDisturbScheduledTimeFragment.s.a());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ nx4 b(HomeAlertsFragment homeAlertsFragment) {
        nx4 nx4 = homeAlertsFragment.h;
        if (nx4 != null) {
            return nx4;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void Q(boolean z) {
        if (z) {
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        kl4 g12 = g1();
        if (g12 != null) {
            g12.a("");
        }
    }

    @DexIgnore
    public void c(List<Alarm> list) {
        wg6.b(list, "alarms");
        c34 c34 = this.i;
        if (c34 != null) {
            c34.a(list);
        } else {
            wg6.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void h(String str) {
        Object r0;
        wg6.b(str, "notificationAppOverView");
        ax5<ta4> ax5 = this.g;
        if (ax5 != null) {
            ta4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.t) != 0) {
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return "HomeAlertsFragment";
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void m(boolean z) {
        Context context;
        int i2;
        ax5<ta4> ax5 = this.g;
        if (ax5 != null) {
            ta4 a2 = ax5.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.F;
                wg6.a((Object) switchCompat, "it.swScheduled");
                switchCompat.setChecked(z);
                ConstraintLayout constraintLayout = a2.s;
                wg6.a((Object) constraintLayout, "it.clScheduledTimeContainer");
                constraintLayout.setVisibility(z ? 0 : 8);
                Object r0 = a2.u;
                if (z) {
                    context = getContext();
                    if (context != null) {
                        i2 = 2131100009;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    context = getContext();
                    if (context != null) {
                        i2 = 2131100402;
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                r0.setTextColor(w6.a(context, i2));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null && view.getId() == 2131361955) {
            nx4 nx4 = this.h;
            if (nx4 != null) {
                nx4.a((Alarm) null);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v8, types: [android.view.View, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r7v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v22, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r7v26, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r6v24, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HomeAlertsFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        ta4 a2 = kb.a(layoutInflater, 2131558564, viewGroup, false, e1());
        this.j = getChildFragmentManager().b(DoNotDisturbScheduledTimeFragment.s.a());
        if (this.j == null) {
            this.j = DoNotDisturbScheduledTimeFragment.s.b();
        }
        String b2 = ThemeManager.l.a().b("onPrimaryButton");
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231028);
            if (drawable != null) {
                drawable.setTint(parseColor);
                a2.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        Object r6 = a2.q;
        wg6.a((Object) r6, "binding.btnAdd");
        cl4.a(r6, this);
        a2.C.setOnClickListener(new d(this));
        a2.y.setOnClickListener(new e(this));
        a2.z.setOnClickListener(new f(this));
        a2.F.setOnClickListener(new g(this));
        a2.B.setOnClickListener(new h(this));
        a2.A.setOnClickListener(new i(this));
        hh4 hh4 = a2.x;
        if (hh4 != null) {
            ConstraintLayout constraintLayout = hh4.q;
            wg6.a((Object) constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            String b3 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b3)) {
                hh4.q.setBackgroundColor(Color.parseColor(b3));
            }
            hh4.t.setImageResource(2131230820);
            Object r7 = hh4.r;
            wg6.a((Object) r7, "viewNoDeviceBinding.ftvDescription");
            r7.setText(jm4.a(getContext(), 2131886807));
            hh4.s.setOnClickListener(new c(this));
        }
        c34 c34 = new c34();
        c34.a((c34.b) new b(this));
        this.i = c34;
        RecyclerView recyclerView = a2.E;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        c34 c342 = this.i;
        if (c342 != null) {
            recyclerView.setAdapter(c342);
            this.g = new ax5<>(this, a2);
            y04 g2 = PortfolioApp.get.instance().g();
            DoNotDisturbScheduledTimeFragment doNotDisturbScheduledTimeFragment = this.j;
            if (doNotDisturbScheduledTimeFragment != null) {
                g2.a(new qx4(doNotDisturbScheduledTimeFragment)).a(this);
                ax5<ta4> ax5 = this.g;
                if (ax5 != null) {
                    ta4 a3 = ax5.a();
                    if (a3 != null) {
                        wg6.a((Object) a3, "mBinding.get()!!");
                        return a3.d();
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimeContract.View");
        }
        wg6.d("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        nx4 nx4 = this.h;
        if (nx4 != null) {
            if (nx4 != null) {
                nx4.g();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        kl4 g1 = g1();
        if (g1 != null) {
            g1.a("");
        }
        HomeAlertsFragment.super.onPause();
    }

    @DexIgnore
    public void onResume() {
        HomeAlertsFragment.super.onResume();
        nx4 nx4 = this.h;
        if (nx4 != null) {
            if (nx4 != null) {
                nx4.f();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        kl4 g1 = g1();
        if (g1 != null) {
            g1.d();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        W("alert_view");
    }

    @DexIgnore
    public void r() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.G(childFragmentManager);
        }
    }

    @DexIgnore
    public void s() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "notifyListAlarm()");
        c34 c34 = this.i;
        if (c34 != null) {
            c34.notifyDataSetChanged();
        } else {
            wg6.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void w() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.P(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void b(SpannableString spannableString) {
        Object r0;
        wg6.b(spannableString, LogBuilder.KEY_TIME);
        ax5<ta4> ax5 = this.g;
        if (ax5 != null) {
            ta4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.w) != 0) {
                r0.setText(spannableString.toString());
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.l(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(nx4 nx4) {
        wg6.b(nx4, "presenter");
        this.h = nx4;
    }

    @DexIgnore
    public void a(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        wg6.b(str, "deviceId");
        wg6.b(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.C;
        Context context = getContext();
        if (context != null) {
            wg6.a((Object) context, "context!!");
            aVar.a(context, str, arrayList, alarm);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(boolean z) {
        ConstraintLayout constraintLayout;
        ax5<ta4> ax5 = this.g;
        if (ax5 != null) {
            ta4 a2 = ax5.a();
            if (a2 != null && (constraintLayout = a2.r) != null) {
                constraintLayout.setVisibility(z ? 0 : 8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void a(SpannableString spannableString) {
        Object r0;
        wg6.b(spannableString, LogBuilder.KEY_TIME);
        ax5<ta4> ax5 = this.g;
        if (ax5 != null) {
            ta4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.v) != 0) {
                r0.setText(spannableString);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
