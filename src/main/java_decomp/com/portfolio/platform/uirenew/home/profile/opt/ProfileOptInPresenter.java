package com.portfolio.platform.uirenew.home.profile.opt;

import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.ft4;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.ul5;
import com.fossil.vl5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yl5$c$a;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileOptInPresenter extends ul5 {
    @DexIgnore
    public /* final */ String e; // = "ProfileOptInPresenter";
    @DexIgnore
    public MFUser f;
    @DexIgnore
    public /* final */ vl5 g;
    @DexIgnore
    public /* final */ UpdateUser h;
    @DexIgnore
    public /* final */ UserRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements m24.e<ft4.d, ft4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public a(ProfileOptInPresenter profileOptInPresenter, boolean z) {
            this.a = profileOptInPresenter;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.i().Q0();
            AnalyticsHelper.f.c().a(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.i().H(!this.b);
            this.a.i().Q0();
            vl5 i = this.a.i();
            int a2 = cVar.a();
            String b2 = cVar.b();
            if (b2 != null) {
                i.b(a2, b2);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<ft4.d, ft4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public b(ProfileOptInPresenter profileOptInPresenter, boolean z) {
            this.a = profileOptInPresenter;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.i().Q0();
            AnalyticsHelper.f.c().a(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.i().K(!this.b);
            this.a.i().Q0();
            vl5 i = this.a.i();
            int a2 = cVar.a();
            String b2 = cVar.b();
            if (b2 != null) {
                i.b(a2, b2);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1", f = "ProfileOptInPresenter.kt", l = {31}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ProfileOptInPresenter profileOptInPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileOptInPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ProfileOptInPresenter profileOptInPresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ProfileOptInPresenter profileOptInPresenter2 = this.this$0;
                dl6 a2 = profileOptInPresenter2.c();
                yl5$c$a yl5_c_a = new yl5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = profileOptInPresenter2;
                this.label = 1;
                obj = gk6.a(a2, yl5_c_a, this);
                if (obj == a) {
                    return a;
                }
                profileOptInPresenter = profileOptInPresenter2;
            } else if (i == 1) {
                profileOptInPresenter = (ProfileOptInPresenter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            profileOptInPresenter.f = (MFUser) obj;
            MFUser b = this.this$0.f;
            if (b != null) {
                this.this$0.i().H(b.isDiagnosticEnabled());
                this.this$0.i().K(b.isEmailOptIn());
            }
            return cd6.a;
        }
    }

    @DexIgnore
    public ProfileOptInPresenter(vl5 vl5, UpdateUser updateUser, UserRepository userRepository) {
        wg6.b(vl5, "mView");
        wg6.b(updateUser, "mUpdateUser");
        wg6.b(userRepository, "mUserRepository");
        this.g = vl5;
        this.h = updateUser;
        this.i = userRepository;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(this.e, "presenter starts: Get user information");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(this.e, "presenter stop");
    }

    @DexIgnore
    public final UserRepository h() {
        return this.i;
    }

    @DexIgnore
    public final vl5 i() {
        return this.g;
    }

    @DexIgnore
    public void j() {
        this.g.a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$b] */
    public void b(boolean z) {
        MFUser mFUser = this.f;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.e;
            local.d(str, "setSubcribeEmailData() called with: checked = " + z);
            mFUser.setEmailOptIn(z);
            this.g.f0();
            if (this.h.a(new UpdateUser.b(mFUser), new b(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.e, "mMfUser is null");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$a] */
    public void a(boolean z) {
        MFUser mFUser = this.f;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.e;
            local.d(str, "setAnonymouslySendUsageData() called with: checked = " + z);
            mFUser.setDiagnosticEnabled(z);
            this.g.f0();
            if (this.h.a(new UpdateUser.b(mFUser), new a(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.e, "mMfUser is null");
    }
}
