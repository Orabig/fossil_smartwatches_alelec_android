package com.portfolio.platform.uirenew.home.customize.hybrid.edit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.c95;
import com.fossil.j95;
import com.fossil.jb4;
import com.fossil.jm4;
import com.fossil.k95;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lx5;
import com.fossil.n35;
import com.fossil.o35;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.sq4;
import com.fossil.tq4;
import com.fossil.u35;
import com.fossil.uz5;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.xm4;
import com.fossil.y04;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeEditFragment extends BasePermissionFragment implements k95, CustomizeWidget.c, AlertDialogFragment.g, View.OnClickListener {
    @DexIgnore
    public j95 g;
    @DexIgnore
    public ax5<jb4> h;
    @DexIgnore
    public /* final */ ArrayList<Fragment> i; // = new ArrayList<>();
    @DexIgnore
    public MicroAppFragment j;
    @DexIgnore
    public MicroAppPresenter o;
    @DexIgnore
    public w04 p;
    @DexIgnore
    public HybridCustomizeViewModel q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditFragment a;

        @DexIgnore
        public b(HybridCustomizeEditFragment hybridCustomizeEditFragment) {
            this.a = hybridCustomizeEditFragment;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                wg6.a((Object) activity, "it");
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.E;
                wg6.a((Object) stringExtra, "presetId");
                wg6.a((Object) stringExtra2, "microAppPos");
                aVar.a(activity, stringExtra, stringExtra2);
            }
            jb4 a2 = this.a.j1().a();
            if (a2 != null) {
                Object r0 = a2.w;
                wg6.a((Object) r0, "binding.tvPresetName");
                r0.setEllipsize(TextUtils.TruncateAt.END);
                a2.w.setSingleLine(true);
                Object r5 = a2.w;
                wg6.a((Object) r5, "binding.tvPresetName");
                r5.setMaxLines(1);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public void onTransitionStart(Transition transition) {
            ViewPropertyAnimator duration;
            ViewPropertyAnimator duration2;
            ViewPropertyAnimator duration3;
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            jb4 a2 = this.a.j1().a();
            if (a2 != null) {
                tq4 tq4 = tq4.a;
                jb4 a3 = this.a.j1().a();
                if (a3 != null) {
                    CardView cardView = a3.q;
                    wg6.a((Object) cardView, "mBinding.get()!!.cvGroup");
                    tq4.a((View) cardView);
                    ViewPropertyAnimator animate = a2.z.animate();
                    if (!(animate == null || (duration3 = animate.setDuration(500)) == null)) {
                        duration3.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate2 = a2.y.animate();
                    if (!(animate2 == null || (duration2 = animate2.setDuration(500)) == null)) {
                        duration2.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate3 = a2.x.animate();
                    if (animate3 != null && (duration = animate3.setDuration(500)) != null) {
                        duration.alpha(1.0f);
                        return;
                    }
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditFragment a;

        @DexIgnore
        public c(HybridCustomizeEditFragment hybridCustomizeEditFragment) {
            this.a = hybridCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a("top", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.d("top", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.f(str, "top");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.e("top", str);
        }

        @DexIgnore
        public void a() {
            this.a.Z("top");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditFragment a;

        @DexIgnore
        public d(HybridCustomizeEditFragment hybridCustomizeEditFragment) {
            this.a = hybridCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a("middle", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.d("middle", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.f(str, "middle");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.e("middle", str);
        }

        @DexIgnore
        public void a() {
            this.a.Z("middle");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements u35.b {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditFragment a;

        @DexIgnore
        public e(HybridCustomizeEditFragment hybridCustomizeEditFragment) {
            this.a = hybridCustomizeEditFragment;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            wg6.b(view, "view");
            wg6.b(str, "id");
            return this.a.a("bottom", view, str);
        }

        @DexIgnore
        public void b(String str) {
            wg6.b(str, "label");
            this.a.d("bottom", str);
        }

        @DexIgnore
        public boolean c(String str) {
            wg6.b(str, "fromPos");
            this.a.f(str, "bottom");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "label");
            this.a.e("bottom", str);
        }

        @DexIgnore
        public void a() {
            this.a.Z("bottom");
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void F() {
        ImageView imageView;
        if (isActive()) {
            ax5<jb4> ax5 = this.h;
            if (ax5 != null) {
                jb4 a2 = ax5.a();
                if (a2 != null && (imageView = a2.s) != null) {
                    imageView.setImageResource(2131231013);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Z(String str) {
        wg6.b(str, "position");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals("top")) {
                            a2.D.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.C.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.B.e();
                }
                a2.D.setDragMode(false);
                a2.C.setDragMode(false);
                a2.B.setDragMode(false);
                MicroAppFragment microAppFragment = this.j;
                if (microAppFragment != null) {
                    microAppFragment.j1();
                    return;
                }
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a0(String str) {
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals("top")) {
                            a2.D.setSelectedWc(true);
                            a2.C.setSelectedWc(false);
                            a2.B.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.D.setSelectedWc(false);
                        a2.C.setSelectedWc(true);
                        a2.B.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.D.setSelectedWc(false);
                    a2.C.setSelectedWc(false);
                    a2.B.setSelectedWc(true);
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void b(String str, String str2, String str3) {
        wg6.b(str, "message");
        wg6.b(str2, "microAppId");
        wg6.b(str3, "microAppPos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558482);
            fVar.a(2131363129, str);
            fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886559));
            fVar.b(2131363190);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.l(childFragmentManager);
        }
    }

    @DexIgnore
    public final void d(String str, String str2) {
        wg6.b(str, "position");
        wg6.b(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals("top")) {
                            a2.D.d();
                        }
                    } else if (str.equals("middle")) {
                        a2.C.d();
                    }
                } else if (str.equals("bottom")) {
                    a2.B.d();
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void e(String str, String str2) {
        wg6.b(str, "position");
        wg6.b(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals("top")) {
                            a2.D.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.C.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.B.e();
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void f(String str, String str2) {
        wg6.b(str, "fromPosition");
        wg6.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 != null) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals("top")) {
                            a2.D.e();
                        }
                    } else if (str2.equals("middle")) {
                        a2.C.e();
                    }
                } else if (str2.equals("bottom")) {
                    a2.B.e();
                }
                a2.D.setDragMode(false);
                a2.C.setDragMode(false);
                a2.B.setDragMode(false);
                j95 j95 = this.g;
                if (j95 != null) {
                    j95.b(str, str2);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public String h1() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    public boolean i1() {
        j95 j95 = this.g;
        if (j95 != null) {
            j95.h();
            return false;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final ax5<jb4> j1() {
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void k1() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        this.j = getChildFragmentManager().b("MicroAppsFragment");
        if (this.j == null) {
            this.j = new MicroAppFragment();
        }
        MicroAppFragment microAppFragment = this.j;
        if (microAppFragment != null) {
            this.i.add(microAppFragment);
        }
        y04 g2 = PortfolioApp.get.instance().g();
        MicroAppFragment microAppFragment2 = this.j;
        if (microAppFragment2 != null) {
            g2.a(new c95(microAppFragment2)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppContract.View");
    }

    @DexIgnore
    public final void l1() {
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.D;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", "top");
                wg6.a((Object) putExtra, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.a(customizeWidget, "SWAP_PRESET_WATCH_APP", putExtra, new u35(new c(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget2 = a2.C;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", "middle");
                wg6.a((Object) putExtra2, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.a(customizeWidget2, "SWAP_PRESET_WATCH_APP", putExtra2, new u35(new d(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget3 = a2.B;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "bottom");
                wg6.a((Object) putExtra3, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.a(customizeWidget3, "SWAP_PRESET_WATCH_APP", putExtra3, new u35(new e(this)), (CustomizeWidget.b) null, 8, (Object) null);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void m() {
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886580);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        X(a2);
    }

    @DexIgnore
    public void n() {
        a();
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        HybridCustomizeEditFragment.super.onActivityCreated(bundle);
        HybridCustomizeEditActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = activity;
            w04 w04 = this.p;
            if (w04 != null) {
                HybridCustomizeViewModel a2 = vd.a(hybridCustomizeEditActivity, w04).a(HybridCustomizeViewModel.class);
                wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.q = a2;
                j95 j95 = this.g;
                if (j95 != null) {
                    HybridCustomizeViewModel hybridCustomizeViewModel = this.q;
                    if (hybridCustomizeViewModel != null) {
                        j95.a(hybridCustomizeViewModel);
                    } else {
                        wg6.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                wg6.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        HybridCustomizeEditFragment.super.onActivityResult(i2, i3, intent);
        if (i2 == 111 && xm4.d.a(getContext(), 1)) {
            j95 j95 = this.g;
            if (j95 != null) {
                j95.a(false);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362426) {
                j95 j95 = this.g;
                if (j95 != null) {
                    j95.a(true);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else if (id != 2131363105) {
                switch (id) {
                    case 2131363328:
                        j95 j952 = this.g;
                        if (j952 != null) {
                            j952.a("bottom");
                            return;
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    case 2131363329:
                        j95 j953 = this.g;
                        if (j953 != null) {
                            j953.a("middle");
                            return;
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    case 2131363330:
                        j95 j954 = this.g;
                        if (j954 != null) {
                            j954.a("top");
                            return;
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    default:
                        return;
                }
            } else {
                j95 j955 = this.g;
                if (j955 != null) {
                    j955.h();
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        jb4 a2 = kb.a(layoutInflater, 2131558571, viewGroup, false, e1());
        k1();
        this.h = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        HybridCustomizeEditFragment.super.onPause();
        j95 j95 = this.g;
        if (j95 != null) {
            j95.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        HybridCustomizeEditFragment.super.onResume();
        j95 j95 = this.g;
        if (j95 != null) {
            j95.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v7, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v8, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v9, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v10, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        W("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wg6.a((Object) activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            wg6.a((Object) window, "activity.window");
            window.setEnterTransition(tq4.a.a());
            Window window2 = activity.getWindow();
            wg6.a((Object) window2, "activity.window");
            window2.setSharedElementEnterTransition(tq4.a.a(PortfolioApp.get.instance()));
            Intent intent = activity.getIntent();
            wg6.a((Object) intent, "activity.intent");
            activity.setEnterSharedElementCallback(new sq4(intent, PortfolioApp.get.instance()));
            a(activity);
        }
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 != null) {
                String b2 = ThemeManager.l.a().b("nonBrandSurface");
                if (!TextUtils.isEmpty(b2)) {
                    a2.A.setBackgroundColor(Color.parseColor(b2));
                }
                String b3 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(b3)) {
                    a2.q.setBackgroundColor(Color.parseColor(b3));
                }
                a2.D.setOnClickListener(this);
                a2.C.setOnClickListener(this);
                a2.B.setOnClickListener(this);
                a2.v.setOnClickListener(this);
                a2.r.setOnClickListener(this);
                ViewPager2 viewPager2 = a2.u;
                wg6.a((Object) viewPager2, "it.rvPreset");
                viewPager2.setAdapter(new uz5(getChildFragmentManager(), this.i));
                ViewPager2 viewPager22 = a2.u;
                wg6.a((Object) viewPager22, "it.rvPreset");
                viewPager22.setUserInputEnabled(false);
                if (a2.u.getChildAt(0) != null) {
                    RecyclerView childAt = a2.u.getChildAt(0);
                    if (childAt != null) {
                        childAt.setItemViewCacheSize(2);
                    } else {
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
            }
            l1();
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void p() {
        AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558485);
        fVar.a(2131363129, jm4.a((Context) PortfolioApp.get.instance(), 2131886367));
        fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886365));
        fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886366));
        fVar.b(2131363105);
        fVar.b(2131363190);
        fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    public void q(String str) {
        wg6.b(str, "buttonsPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
        a0(str);
    }

    @DexIgnore
    public final Transition a(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        wg6.a((Object) window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new b(this));
    }

    @DexIgnore
    public void q() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            wg6.a((Object) activity, "it");
            TroubleshootingActivity.a.a(aVar, activity, PortfolioApp.get.instance().e(), false, 4, (Object) null);
        }
    }

    @DexIgnore
    public final boolean a(String str, View view, String str2) {
        wg6.b(str, "position");
        wg6.b(view, "view");
        wg6.b(str2, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 == null) {
                return true;
            }
            int hashCode = str.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && str.equals("top")) {
                        a2.D.e();
                    }
                } else if (str.equals("middle")) {
                    a2.C.e();
                }
            } else if (str.equals("bottom")) {
                a2.B.e();
            }
            a2.D.setDragMode(false);
            a2.C.setDragMode(false);
            a2.B.setDragMode(false);
            MicroAppFragment microAppFragment = this.j;
            if (microAppFragment != null) {
                microAppFragment.j1();
            }
            j95 j95 = this.g;
            if (j95 != null) {
                j95.a(str2, str);
                return true;
            }
            wg6.d("mPresenter");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e(boolean z) {
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageButton imageButton = a2.r;
                wg6.a((Object) imageButton, "it.ftvSetToWatch");
                imageButton.setEnabled(true);
                ImageButton imageButton2 = a2.r;
                wg6.a((Object) imageButton2, "it.ftvSetToWatch");
                imageButton2.setClickable(true);
                a2.r.setBackgroundResource(2131231304);
                return;
            }
            ImageButton imageButton3 = a2.r;
            wg6.a((Object) imageButton3, "it.ftvSetToWatch");
            imageButton3.setClickable(false);
            ImageButton imageButton4 = a2.r;
            wg6.a((Object) imageButton4, "it.ftvSetToWatch");
            imageButton4.setEnabled(false);
            a2.r.setBackgroundResource(2131231305);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void f(boolean z) {
        Object r0;
        ax5<jb4> ax5 = this.h;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (!(a2 == null || (r0 = a2.w) == 0)) {
                r0.setSingleLine(false);
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                if (z) {
                    activity.setResult(-1);
                } else {
                    activity.setResult(0);
                }
                activity.supportFinishAfterTransition();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.fossil.o35} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: com.fossil.o35} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: com.fossil.o35} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: com.fossil.o35} */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    public void a(n35 n35) {
        Object obj;
        Object obj2;
        wg6.b(n35, "data");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "showCurrentPreset  microApps=" + n35.a());
        ax5<jb4> ax5 = this.h;
        o35 o35 = null;
        if (ax5 != null) {
            jb4 a2 = ax5.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(n35.a());
                Object r3 = a2.w;
                wg6.a((Object) r3, "it.tvPresetName");
                r3.setText(n35.c());
                Iterator it = arrayList.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it.next();
                    if (wg6.a((Object) ((o35) obj).c(), (Object) "top")) {
                        break;
                    }
                }
                o35 o352 = (o35) obj;
                Iterator it2 = arrayList.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it2.next();
                    if (wg6.a((Object) ((o35) obj2).c(), (Object) "middle")) {
                        break;
                    }
                }
                o35 o353 = (o35) obj2;
                Iterator it3 = arrayList.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        break;
                    }
                    Object next = it3.next();
                    if (wg6.a((Object) next.c(), (Object) "bottom")) {
                        o35 = next;
                        break;
                    }
                }
                CustomizeWidget customizeWidget = a2.D;
                wg6.a((Object) customizeWidget, "it.waTop");
                FlexibleTextView flexibleTextView = a2.z;
                wg6.a((Object) flexibleTextView, "it.tvWaTop");
                a(customizeWidget, flexibleTextView, o352);
                CustomizeWidget customizeWidget2 = a2.C;
                wg6.a((Object) customizeWidget2, "it.waMiddle");
                FlexibleTextView flexibleTextView2 = a2.y;
                wg6.a((Object) flexibleTextView2, "it.tvWaMiddle");
                a(customizeWidget2, flexibleTextView2, o353);
                CustomizeWidget customizeWidget3 = a2.B;
                wg6.a((Object) customizeWidget3, "it.waBottom");
                FlexibleTextView flexibleTextView3 = a2.x;
                wg6.a((Object) flexibleTextView3, "it.tvWaBottom");
                a(customizeWidget3, flexibleTextView3, o35);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void a(CustomizeWidget customizeWidget, FlexibleTextView r3, o35 o35) {
        if (o35 != null) {
            r3.setText(o35.b());
            customizeWidget.c(o35.a());
            customizeWidget.h();
            a(customizeWidget, o35.a());
            return;
        }
        r3.setText("");
        customizeWidget.c("empty");
        customizeWidget.h();
        a(customizeWidget, "empty");
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public void a(j95 j95) {
        wg6.b(j95, "presenter");
        this.g = j95;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode != 291193711 || !str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING")) {
                    return;
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
                return;
            } else {
                if (i2 == 2131363105) {
                    f(false);
                    return;
                } else if (i2 == 2131363190) {
                    j95 j95 = this.g;
                    if (j95 != null) {
                        j95.a(true);
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            }
        } else if (!str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION")) {
            return;
        }
        if (i2 == 2131363190 && intent != null) {
            String stringExtra = intent.getStringExtra("TO_POS");
            String stringExtra2 = intent.getStringExtra("TO_ID");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
            if (wg6.a((Object) stringExtra2, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
                xm4 xm4 = xm4.d;
                Context context = getContext();
                if (context != null) {
                    xm4.b(context, stringExtra2);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                j95 j952 = this.g;
                if (j952 != null) {
                    wg6.a((Object) stringExtra, "toPos");
                    j952.a(stringExtra);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
        }
    }
}
