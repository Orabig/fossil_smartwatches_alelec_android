package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.location.Location;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc3;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mc3;
import com.fossil.nc6;
import com.fossil.qc3;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.ud;
import com.fossil.w75;
import com.fossil.wg6;
import com.fossil.xe6;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDetailViewModel extends td {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public PlacesClient a;
    @DexIgnore
    public AddressWrapper b;
    @DexIgnore
    public AddressWrapper c;
    @DexIgnore
    public List<String> d;
    @DexIgnore
    public MutableLiveData<w75.b> e; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Boolean d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public PlacesClient f;
        @DexIgnore
        public Boolean g;
        @DexIgnore
        public Boolean h;

        @DexIgnore
        public b(String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5) {
            this.a = str;
            this.b = str2;
            this.c = bool;
            this.d = bool2;
            this.e = bool3;
            this.f = placesClient;
            this.g = bool4;
            this.h = bool5;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final Boolean b() {
            return this.c;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }

        @DexIgnore
        public final PlacesClient d() {
            return this.f;
        }

        @DexIgnore
        public final Boolean e() {
            return this.g;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return wg6.a((Object) this.a, (Object) bVar.a) && wg6.a((Object) this.b, (Object) bVar.b) && wg6.a((Object) this.c, (Object) bVar.c) && wg6.a((Object) this.d, (Object) bVar.d) && wg6.a((Object) this.e, (Object) bVar.e) && wg6.a((Object) this.f, (Object) bVar.f) && wg6.a((Object) this.g, (Object) bVar.g) && wg6.a((Object) this.h, (Object) bVar.h);
        }

        @DexIgnore
        public final Boolean f() {
            return this.h;
        }

        @DexIgnore
        public final Boolean g() {
            return this.d;
        }

        @DexIgnore
        public final Boolean h() {
            return this.e;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Boolean bool = this.c;
            int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
            Boolean bool2 = this.d;
            int hashCode4 = (hashCode3 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
            Boolean bool3 = this.e;
            int hashCode5 = (hashCode4 + (bool3 != null ? bool3.hashCode() : 0)) * 31;
            PlacesClient placesClient = this.f;
            int hashCode6 = (hashCode5 + (placesClient != null ? placesClient.hashCode() : 0)) * 31;
            Boolean bool4 = this.g;
            int hashCode7 = (hashCode6 + (bool4 != null ? bool4.hashCode() : 0)) * 31;
            Boolean bool5 = this.h;
            if (bool5 != null) {
                i = bool5.hashCode();
            }
            return hashCode7 + i;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(name=" + this.a + ", address=" + this.b + ", avoidTolls=" + this.c + ", isNameEditable=" + this.d + ", isValid=" + this.e + ", placesClient=" + this.f + ", isLoading=" + this.g + ", isLocationUnavailable=" + this.h + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<TResult> implements mc3<FetchPlaceResponse> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public c(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, String str) {
            this.a = commuteTimeSettingsDetailViewModel;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
            AddressWrapper a2 = this.a.b;
            if (a2 != null) {
                wg6.a((Object) fetchPlaceResponse, "response");
                Place place = fetchPlaceResponse.getPlace();
                wg6.a((Object) place, "response.place");
                LatLng latLng = place.getLatLng();
                if (latLng != null) {
                    a2.setLat(latLng.a);
                } else {
                    wg6.a();
                    throw null;
                }
            }
            AddressWrapper a3 = this.a.b;
            if (a3 != null) {
                wg6.a((Object) fetchPlaceResponse, "response");
                Place place2 = fetchPlaceResponse.getPlace();
                wg6.a((Object) place2, "response.place");
                LatLng latLng2 = place2.getLatLng();
                if (latLng2 != null) {
                    a3.setLng(latLng2.b);
                } else {
                    wg6.a();
                    throw null;
                }
            }
            AddressWrapper a4 = this.a.b;
            if (a4 != null) {
                a4.setAddress(this.b);
            }
            CommuteTimeSettingsDetailViewModel.a(this.a, (String) null, (String) null, (Boolean) null, (Boolean) null, Boolean.valueOf(this.a.d()), (PlacesClient) null, false, false, 47, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements lc3 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel a;

        @DexIgnore
        public d(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel) {
            this.a = commuteTimeSettingsDetailViewModel;
        }

        @DexIgnore
        public final void onFailure(Exception exc) {
            wg6.b(exc, "exception");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String g = CommuteTimeSettingsDetailViewModel.f;
            local.e(g, "FetchPlaceRequest - exception=" + exc);
            CommuteTimeSettingsDetailViewModel.a(this.a, (String) null, (String) null, (Boolean) null, (Boolean) null, Boolean.valueOf(this.a.d()), (PlacesClient) null, false, true, 47, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$1", f = "CommuteTimeSettingsDetailViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeSettingsDetailViewModel;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.a = Places.createClient(PortfolioApp.get.instance());
                AddressWrapper a = this.this$0.b;
                Boolean bool = null;
                boolean z = (a != null ? a.getType() : null) == AddressWrapper.AddressType.OTHER;
                CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.this$0;
                AddressWrapper a2 = commuteTimeSettingsDetailViewModel.b;
                String name = a2 != null ? a2.getName() : null;
                Boolean a3 = hf6.a(z);
                AddressWrapper a4 = this.this$0.b;
                String address = a4 != null ? a4.getAddress() : null;
                AddressWrapper a5 = this.this$0.b;
                if (a5 != null) {
                    bool = hf6.a(a5.getAvoidTolls());
                }
                CommuteTimeSettingsDetailViewModel.a(commuteTimeSettingsDetailViewModel, name, address, bool, a3, (Boolean) null, this.this$0.a, (Boolean) null, (Boolean) null, 208, (Object) null);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = CommuteTimeSettingsDetailViewModel.class.getSimpleName();
        wg6.a((Object) simpleName, "CommuteTimeSettingsDetai\u2026el::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeSettingsDetailViewModel(an4 an4, UserRepository userRepository) {
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(userRepository, "mUserRepository");
    }

    @DexIgnore
    public final boolean c() {
        String str;
        String name;
        List<String> list = this.d;
        if (list != null) {
            for (String next : list) {
                AddressWrapper addressWrapper = this.b;
                if (addressWrapper == null || (name = addressWrapper.getName()) == null) {
                    str = null;
                } else if (name != null) {
                    str = name.toUpperCase();
                    wg6.a((Object) str, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
                if (next != null) {
                    String upperCase = next.toUpperCase();
                    wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    if (wg6.a((Object) str, (Object) upperCase)) {
                        return true;
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            return false;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final boolean d() {
        String str;
        String str2;
        String name;
        String name2;
        AddressWrapper addressWrapper = this.b;
        String str3 = null;
        if (TextUtils.isEmpty(addressWrapper != null ? addressWrapper.getAddress() : null)) {
            return false;
        }
        AddressWrapper addressWrapper2 = this.b;
        if (TextUtils.isEmpty(addressWrapper2 != null ? addressWrapper2.getName() : null)) {
            return false;
        }
        AddressWrapper addressWrapper3 = this.b;
        Boolean valueOf = addressWrapper3 != null ? Boolean.valueOf(addressWrapper3.getAvoidTolls()) : null;
        AddressWrapper addressWrapper4 = this.c;
        if (!(!wg6.a((Object) valueOf, (Object) addressWrapper4 != null ? Boolean.valueOf(addressWrapper4.getAvoidTolls()) : null))) {
            AddressWrapper addressWrapper5 = this.b;
            AddressWrapper.AddressType type = addressWrapper5 != null ? addressWrapper5.getType() : null;
            AddressWrapper addressWrapper6 = this.c;
            if (type == (addressWrapper6 != null ? addressWrapper6.getType() : null)) {
                AddressWrapper addressWrapper7 = this.b;
                if (addressWrapper7 == null || (name2 = addressWrapper7.getName()) == null) {
                    str = null;
                } else if (name2 != null) {
                    str = name2.toUpperCase();
                    wg6.a((Object) str, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
                AddressWrapper addressWrapper8 = this.c;
                if (addressWrapper8 == null || (name = addressWrapper8.getName()) == null) {
                    str2 = null;
                } else if (name != null) {
                    str2 = name.toUpperCase();
                    wg6.a((Object) str2, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
                if (!(!wg6.a((Object) str, (Object) str2))) {
                    AddressWrapper addressWrapper9 = this.b;
                    String address = addressWrapper9 != null ? addressWrapper9.getAddress() : null;
                    AddressWrapper addressWrapper10 = this.c;
                    if (addressWrapper10 != null) {
                        str3 = addressWrapper10.getAddress();
                    }
                    if (!wg6.a((Object) address, (Object) str3)) {
                        return true;
                    }
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public final void e() {
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void f() {
        this.a = null;
    }

    @DexIgnore
    public final MutableLiveData<w75.b> b() {
        return this.e;
    }

    @DexIgnore
    public final AddressWrapper a() {
        return this.b;
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper, ArrayList<String> arrayList) {
        this.c = addressWrapper;
        this.b = addressWrapper != null ? addressWrapper.clone() : null;
        this.d = arrayList;
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "name");
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setName(str);
        }
        a(this, (String) null, (String) null, (Boolean) null, (Boolean) null, Boolean.valueOf(d()), (PlacesClient) null, (Boolean) null, (Boolean) null, 239, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ void a(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, String str, String str2, AutocompleteSessionToken autocompleteSessionToken, int i, Object obj) {
        if ((i & 1) != 0) {
            str = "";
        }
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            autocompleteSessionToken = null;
        }
        commuteTimeSettingsDetailViewModel.a(str, str2, autocompleteSessionToken);
    }

    @DexIgnore
    public final void a(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        wg6.b(str, "address");
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && autocompleteSessionToken != null) {
            if (str2 != null) {
                a(str2, autocompleteSessionToken, str);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(String str, Location location) {
        wg6.b(str, "address");
        wg6.b(location, "location");
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setAddress(str);
        }
        AddressWrapper addressWrapper2 = this.b;
        if (addressWrapper2 != null) {
            addressWrapper2.setLat(location.getLatitude());
        }
        AddressWrapper addressWrapper3 = this.b;
        if (addressWrapper3 != null) {
            addressWrapper3.setLng(location.getLongitude());
        }
        a(this, (String) null, (String) null, (Boolean) null, (Boolean) null, Boolean.valueOf(d()), (PlacesClient) null, (Boolean) null, (Boolean) null, 239, (Object) null);
    }

    @DexIgnore
    public final void a(boolean z) {
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setAvoidTolls(z);
        }
        a(this, (String) null, (String) null, (Boolean) null, (Boolean) null, Boolean.valueOf(d()), (PlacesClient) null, (Boolean) null, (Boolean) null, 239, (Object) null);
    }

    @DexIgnore
    public final void a(String str, AutocompleteSessionToken autocompleteSessionToken, String str2) {
        if (this.a != null && this.b != null) {
            a(this, (String) null, (String) null, (Boolean) null, (Boolean) null, (Boolean) null, (PlacesClient) null, true, (Boolean) null, 191, (Object) null);
            ArrayList arrayList = new ArrayList();
            arrayList.add(Place.Field.ADDRESS);
            arrayList.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str, arrayList);
            wg6.a((Object) builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.a;
            if (placesClient != null) {
                qc3 fetchPlace = placesClient.fetchPlace(builder.build());
                wg6.a((Object) fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.a(new c(this, str2));
                fetchPlace.a(new d(this));
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5, int i, Object obj) {
        if ((i & 1) != 0) {
            str = null;
        }
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            bool = null;
        }
        if ((i & 8) != 0) {
            bool2 = null;
        }
        if ((i & 16) != 0) {
            bool3 = null;
        }
        if ((i & 32) != 0) {
            placesClient = null;
        }
        if ((i & 64) != 0) {
            bool4 = null;
        }
        if ((i & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0) {
            bool5 = null;
        }
        commuteTimeSettingsDetailViewModel.a(str, str2, bool, bool2, bool3, placesClient, bool4, bool5);
    }

    @DexIgnore
    public final void a(String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5) {
        this.e.a(new b(str, str2, bool, bool2, bool3, placesClient, bool4, bool5));
    }
}
