package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.b06;
import com.fossil.ba4;
import com.fossil.fe5;
import com.fossil.ge5;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.ut4;
import com.fossil.vd;
import com.fossil.wg6;
import com.fossil.yk4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewDayFragment extends BaseFragment implements ge5 {
    @DexIgnore
    public b06 f;
    @DexIgnore
    public ax5<ba4> g;
    @DexIgnore
    public fe5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewDayFragment a;

        @DexIgnore
        public b(GoalTrackingOverviewDayFragment goalTrackingOverviewDayFragment, ba4 ba4) {
            this.a = goalTrackingOverviewDayFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewDayFragment.a(this.a).a().a(1);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ b06 a(GoalTrackingOverviewDayFragment goalTrackingOverviewDayFragment) {
        b06 b06 = goalTrackingOverviewDayFragment.f;
        if (b06 != null) {
            return b06;
        }
        wg6.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    public void c(boolean z) {
        ba4 a2;
        ax5<ba4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            if (z) {
                OverviewDayGoalChart overviewDayGoalChart = a2.r;
                wg6.a((Object) overviewDayGoalChart, "binding.dayChart");
                overviewDayGoalChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                wg6.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewDayGoalChart overviewDayGoalChart2 = a2.r;
            wg6.a((Object) overviewDayGoalChart2, "binding.dayChart");
            overviewDayGoalChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            wg6.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "GoalTrackingOverviewDayFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        ba4 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        ax5<ba4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayGoalChart = a2.r) != null) {
            overviewDayGoalChart.a("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ba4 a2;
        wg6.b(layoutInflater, "inflater");
        GoalTrackingOverviewDayFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onCreateView");
        ba4 a3 = kb.a(layoutInflater, 2131558554, viewGroup, false, e1());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            b06 a4 = vd.a(activity).a(b06.class);
            wg6.a((Object) a4, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.f = a4;
            a3.s.setOnClickListener(new b(this, a3));
        }
        this.g = new ax5<>(this, a3);
        j1();
        ax5<ba4> ax5 = this.g;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        GoalTrackingOverviewDayFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onResume");
        j1();
        fe5 fe5 = this.h;
        if (fe5 != null) {
            fe5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        GoalTrackingOverviewDayFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onStop");
        fe5 fe5 = this.h;
        if (fe5 != null) {
            fe5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(ut4 ut4, ArrayList<String> arrayList) {
        ba4 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        wg6.b(ut4, "baseModel");
        wg6.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewDayFragment", "showDayDetailChart - baseModel=" + ut4);
        ax5<ba4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayGoalChart = a2.r) != null) {
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) yk4.b.a(), false, 2, (Object) null);
            }
            overviewDayGoalChart.a(ut4);
        }
    }

    @DexIgnore
    public void a(fe5 fe5) {
        wg6.b(fe5, "presenter");
        this.h = fe5;
    }
}
