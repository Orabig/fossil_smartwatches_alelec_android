package com.portfolio.platform.uirenew.home.dashboard.sleep;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ag5;
import com.fossil.ax5;
import com.fossil.b06;
import com.fossil.b94;
import com.fossil.bu4;
import com.fossil.bv4;
import com.fossil.cf;
import com.fossil.cv4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.la5;
import com.fossil.pg;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tz5;
import com.fossil.vd;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.zf5;
import com.fossil.zu4;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardSleepFragment extends BaseFragment implements ag5, bv4, bu4 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public ax5<b94> f;
    @DexIgnore
    public zf5 g;
    @DexIgnore
    public zu4 h;
    @DexIgnore
    public SleepOverviewFragment i;
    @DexIgnore
    public tz5 j;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DashboardSleepFragment.p;
        }

        @DexIgnore
        public final DashboardSleepFragment b() {
            return new DashboardSleepFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends tz5 {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardSleepFragment e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, DashboardSleepFragment dashboardSleepFragment, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = dashboardSleepFragment;
        }

        @DexIgnore
        public void a(int i) {
            DashboardSleepFragment.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = DashboardSleepFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "DashboardSleepFragment::class.java.simpleName!!");
            p = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ zf5 a(DashboardSleepFragment dashboardSleepFragment) {
        zf5 zf5 = dashboardSleepFragment.g;
        if (zf5 != null) {
            return zf5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void Q(boolean z) {
        b94 j1;
        RecyclerView recyclerView;
        View view;
        if (z) {
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
            }
            if (isVisible() && this.f != null && (j1 = j1()) != null && (recyclerView = j1.q) != null) {
                RecyclerView.ViewHolder findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0);
                if (findViewHolderForAdapterPosition == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != 0.0f) {
                    recyclerView.smoothScrollToPosition(0);
                    tz5 tz5 = this.j;
                    if (tz5 != null) {
                        tz5.a();
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        kl4 g12 = g1();
        if (g12 != null) {
            g12.a("");
        }
    }

    @DexIgnore
    public void b(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = DashboardActivityFragment.q.a();
        local.d(a2, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            SleepDetailActivity.a aVar = SleepDetailActivity.D;
            wg6.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void f() {
        tz5 tz5 = this.j;
        if (tz5 != null) {
            tz5.a();
        }
    }

    @DexIgnore
    public String h1() {
        return p;
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final b94 j1() {
        ax5<b94> ax5 = this.f;
        if (ax5 != null) {
            return ax5.a();
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        DashboardSleepFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558541, viewGroup, false, e1()));
        ax5<b94> ax5 = this.f;
        if (ax5 != null) {
            b94 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(p, "onDestroy");
        DashboardSleepFragment.super.onDestroy();
    }

    @DexIgnore
    public void onDestroyView() {
        zf5 zf5 = this.g;
        if (zf5 != null) {
            zf5.i();
            super.onDestroyView();
            d1();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        DashboardSleepFragment.super.onResume();
        zf5 zf5 = this.g;
        if (zf5 != null) {
            zf5.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStop() {
        DashboardSleepFragment.super.onStop();
        zf5 zf5 = this.g;
        if (zf5 != null) {
            zf5.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        this.i = getChildFragmentManager().b("SleepOverviewFragment");
        if (this.i == null) {
            this.i = new SleepOverviewFragment();
        }
        cv4 cv4 = new cv4();
        PortfolioApp instance = PortfolioApp.get.instance();
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        SleepOverviewFragment sleepOverviewFragment = this.i;
        if (sleepOverviewFragment != null) {
            this.h = new zu4(cv4, instance, this, childFragmentManager, sleepOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            b94 j1 = j1();
            if (!(j1 == null || (recyclerView2 = j1.q) == null)) {
                wg6.a((Object) recyclerView2, "it");
                recyclerView2.setLayoutManager(linearLayoutManager);
                zu4 zu4 = this.h;
                if (zu4 != null) {
                    recyclerView2.setAdapter(zu4);
                    LinearLayoutManager layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        this.j = new b(recyclerView2, layoutManager, this, linearLayoutManager);
                        tz5 tz5 = this.j;
                        if (tz5 != null) {
                            recyclerView2.addOnScrollListener(tz5);
                            recyclerView2.setItemViewCacheSize(0);
                            la5 la5 = new la5(linearLayoutManager.Q());
                            Drawable c = w6.c(recyclerView2.getContext(), 2131230876);
                            if (c != null) {
                                wg6.a((Object) c, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                la5.a(c);
                                recyclerView2.addItemDecoration(la5);
                                zf5 zf5 = this.g;
                                if (zf5 != null) {
                                    zf5.h();
                                } else {
                                    wg6.d("mPresenter");
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                } else {
                    wg6.d("mDashboardSleepsAdapter");
                    throw null;
                }
            }
            b94 j12 = j1();
            if (!(j12 == null || (recyclerView = j12.q) == null)) {
                wg6.a((Object) recyclerView, "recyclerView");
                pg itemAnimator = recyclerView.getItemAnimator();
                if (itemAnimator instanceof pg) {
                    itemAnimator.setSupportsChangeAnimations(false);
                }
            }
            W("sleep_view");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                b06 a2 = vd.a(activity).a(b06.class);
                wg6.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                b06 b06 = a2;
                return;
            }
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(cf<SleepSummary> cfVar) {
        zu4 zu4 = this.h;
        if (zu4 != null) {
            zu4.c(cfVar);
        } else {
            wg6.d("mDashboardSleepsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(zf5 zf5) {
        wg6.b(zf5, "presenter");
        this.g = zf5;
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        wg6.b(date, "startWeekDate");
        wg6.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }
}
