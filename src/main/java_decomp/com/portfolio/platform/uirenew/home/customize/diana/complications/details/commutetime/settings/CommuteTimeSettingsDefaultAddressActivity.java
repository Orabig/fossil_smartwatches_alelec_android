package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.e55;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.CommuteTimeSettingsDefaultAddressFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDefaultAddressActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public CommuteTimeSettingsDefaultAddressPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, Bundle bundle) {
            wg6.b(fragment, "fragment");
            wg6.b(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 111);
        }

        @DexIgnore
        public final void b(Fragment fragment, Bundle bundle) {
            wg6.b(fragment, "fragment");
            wg6.b(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 112);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        CommuteTimeSettingsDefaultAddressFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = CommuteTimeSettingsDefaultAddressFragment.q.a();
            a((Fragment) b, CommuteTimeSettingsDefaultAddressFragment.q.b(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new e55(b)).a(this);
            Bundle bundleExtra = getIntent().getBundleExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS");
            if (bundleExtra != null) {
                CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter = this.B;
                if (commuteTimeSettingsDefaultAddressPresenter != null) {
                    String string = bundleExtra.getString("AddressType");
                    if (string == null) {
                        string = "Home";
                    }
                    String string2 = bundleExtra.getString("KEY_DEFAULT_PLACE");
                    if (string2 == null) {
                        string2 = "";
                    }
                    commuteTimeSettingsDefaultAddressPresenter.a(string, string2);
                    return;
                }
                wg6.d("mCommuteTimeSettingsDefaultAddressPresenter");
                throw null;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressContract.View");
    }
}
