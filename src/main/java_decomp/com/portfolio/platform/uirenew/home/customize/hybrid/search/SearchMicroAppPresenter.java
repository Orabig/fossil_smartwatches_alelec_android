package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.aa5;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.ba5;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ea5$a$a;
import com.fossil.ea5$b$a;
import com.fossil.ea5$b$b;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchMicroAppPresenter extends aa5 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ ArrayList<MicroApp> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ba5 k;
    @DexIgnore
    public /* final */ MicroAppRepository l;
    @DexIgnore
    public /* final */ an4 m;
    @DexIgnore
    public /* final */ PortfolioApp n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$search$1", f = "SearchMicroAppPresenter.kt", l = {79}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SearchMicroAppPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(SearchMicroAppPresenter searchMicroAppPresenter, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = searchMicroAppPresenter;
            this.$query = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$query, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            List list;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                list = new ArrayList();
                if (this.$query.length() > 0) {
                    dl6 a2 = this.this$0.c();
                    ea5$a$a ea5_a_a = new ea5$a$a(this, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = list;
                    this.label = 1;
                    obj = gk6.a(a2, ea5_a_a, this);
                    if (obj == a) {
                        return a;
                    }
                }
                this.this$0.k.b((List<lc6<MicroApp, String>>) this.this$0.a((List<MicroApp>) list));
                this.this$0.h = this.$query;
                return cd6.a;
            } else if (i == 1) {
                List list2 = (List) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            list = (List) obj;
            this.this$0.k.b((List<lc6<MicroApp, String>>) this.this$0.a((List<MicroApp>) list));
            this.this$0.h = this.$query;
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1", f = "SearchMicroAppPresenter.kt", l = {40, 44}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SearchMicroAppPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(SearchMicroAppPresenter searchMicroAppPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = searchMicroAppPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 a2 = this.this$0.c();
                ea5$b$a ea5_b_a = new ea5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, ea5_b_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                List list = (List) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                this.this$0.j.clear();
                this.this$0.j.addAll((List) obj);
                this.this$0.i();
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list2 = (List) obj;
            this.this$0.i.clear();
            this.this$0.i.addAll(list2);
            dl6 a3 = this.this$0.c();
            ea5$b$b ea5_b_b = new ea5$b$b(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = list2;
            this.label = 2;
            obj = gk6.a(a3, ea5_b_b, this);
            if (obj == a) {
                return a;
            }
            this.this$0.j.clear();
            this.this$0.j.addAll((List) obj);
            this.this$0.i();
            return cd6.a;
        }
    }

    @DexIgnore
    public SearchMicroAppPresenter(ba5 ba5, MicroAppRepository microAppRepository, an4 an4, PortfolioApp portfolioApp) {
        wg6.b(ba5, "mView");
        wg6.b(microAppRepository, "mMicroAppRepository");
        wg6.b(an4, "sharedPreferencesManager");
        wg6.b(portfolioApp, "mApp");
        this.k = ba5;
        this.l = microAppRepository;
        this.m = an4;
        this.n = portfolioApp;
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public final void i() {
        if (this.j.isEmpty()) {
            this.k.b(a((List<MicroApp>) yd6.d(this.i)));
        } else {
            this.k.d(a((List<MicroApp>) yd6.d(this.j)));
        }
        if (!TextUtils.isEmpty(this.h)) {
            ba5 ba5 = this.k;
            String str = this.h;
            if (str != null) {
                ba5.b(str);
                String str2 = this.h;
                if (str2 != null) {
                    a(str2);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void j() {
        this.k.a(this);
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void h() {
        this.h = "";
        this.k.u();
        i();
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        wg6.b(str, "watchAppTop");
        wg6.b(str2, "watchAppMiddle");
        wg6.b(str3, "watchAppBottom");
        this.e = str;
        this.g = str3;
        this.f = str2;
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new a(this, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(MicroApp microApp) {
        wg6.b(microApp, "selectedMicroApp");
        List<String> r = this.m.r();
        wg6.a((Object) r, "sharedPreferencesManager.microAppSearchedIdsRecent");
        if (!r.contains(microApp.getId())) {
            r.add(0, microApp.getId());
            if (r.size() > 5) {
                r = r.subList(0, 5);
            }
            this.m.c(r);
        }
        this.k.a(microApp);
    }

    @DexIgnore
    public final List<lc6<MicroApp, String>> a(List<MicroApp> list) {
        ArrayList arrayList = new ArrayList();
        for (MicroApp next : list) {
            String id = next.getId();
            if (wg6.a((Object) id, (Object) this.e)) {
                arrayList.add(new lc6(next, "top"));
            } else if (wg6.a((Object) id, (Object) this.f)) {
                arrayList.add(new lc6(next, "middle"));
            } else if (wg6.a((Object) id, (Object) this.g)) {
                arrayList.add(new lc6(next, "bottom"));
            } else {
                arrayList.add(new lc6(next, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString("top", this.e);
            bundle.putString("middle", this.f);
            bundle.putString("bottom", this.g);
        }
        return bundle;
    }
}
