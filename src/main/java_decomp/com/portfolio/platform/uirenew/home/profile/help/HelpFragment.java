package com.portfolio.platform.uirenew.home.profile.help;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.cl5;
import com.fossil.dl5;
import com.fossil.jk3;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.ra4;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import com.zendesk.sdk.R;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HelpFragment extends BaseFragment implements dl5, View.OnClickListener, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public ax5<ra4> f;
    @DexIgnore
    public cl5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HelpFragment.i;
        }

        @DexIgnore
        public final HelpFragment b() {
            return new HelpFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HelpFragment a;

        @DexIgnore
        public b(HelpFragment helpFragment, String str) {
            this.a = helpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HelpFragment a;

        @DexIgnore
        public c(HelpFragment helpFragment, String str) {
            this.a = helpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            nh6 nh6 = nh6.a;
            Locale a2 = jm4.a();
            wg6.a((Object) a2, "LanguageHelper.getLocale()");
            Object[] objArr = {a2.getLanguage()};
            String format = String.format("https://support.fossil.com/hc/%s/categories/360000064626-Smartwatch-FAQ", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = HelpFragment.j.a();
            local.d(a3, "FAQ URL = " + format);
            this.a.Z(format);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HelpFragment a;

        @DexIgnore
        public d(HelpFragment helpFragment, String str) {
            this.a = helpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(HelpFragment.j.a(), "Repair Center URL = https://c.fossil.com/web/service_centers");
            this.a.Z("https://c.fossil.com/web/service_centers");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HelpFragment a;

        @DexIgnore
        public e(HelpFragment helpFragment, String str) {
            this.a = helpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1().i();
            this.a.j1().a("Contact Us - From app [Fossil] - [Android]");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HelpFragment a;

        @DexIgnore
        public f(HelpFragment helpFragment, String str) {
            this.a = helpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            nh6 nh6 = nh6.a;
            Locale a2 = jm4.a();
            wg6.a((Object) a2, "LanguageHelper.getLocale()");
            Object[] objArr = {a2.getLanguage()};
            String format = String.format("https://support.fossil.com/hc/%s?wearablesChat=true", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = HelpFragment.j.a();
            local.d(a3, "Chat URL = " + format);
            this.a.Z(format);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HelpFragment a;

        @DexIgnore
        public g(HelpFragment helpFragment, String str) {
            this.a = helpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(HelpFragment.j.a(), "Call Us URL = https://c.fossil.com/web/call");
            this.a.Z("https://c.fossil.com/web/call");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HelpFragment a;

        @DexIgnore
        public h(HelpFragment helpFragment, String str) {
            this.a = helpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.getActivity() != null) {
                DeleteAccountActivity.a aVar = DeleteAccountActivity.C;
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    wg6.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = HelpFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "HelpFragment::class.java.simpleName!!");
            i = simpleName;
            new String[]{"smartwatches@fossil.com"};
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public final void Z(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), i);
    }

    @DexIgnore
    public void b(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        wg6.b(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final cl5 j1() {
        cl5 cl5 = this.g;
        if (cl5 != null) {
            return cl5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void o() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 1000) {
            HelpFragment.super.onActivityResult(i2, i3, intent);
        } else if (i3 == -1) {
            cl5 cl5 = this.g;
            if (cl5 != null) {
                cl5.h();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        wg6.b(view, "v");
        if (view.getId() == 2131361851) {
            o();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        ra4 a2 = kb.a(LayoutInflater.from(getContext()), R.layout.fragment_help, (ViewGroup) null, false, e1());
        this.f = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        HelpFragment.super.onPause();
        cl5 cl5 = this.g;
        if (cl5 != null) {
            cl5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        HelpFragment.super.onResume();
        cl5 cl5 = this.g;
        if (cl5 != null) {
            cl5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        String h2 = PortfolioApp.get.instance().h();
        ax5<ra4> ax5 = this.f;
        if (ax5 != null) {
            ra4 a2 = ax5.a();
            if (a2 != null) {
                String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
                if (!TextUtils.isEmpty(b2)) {
                    int parseColor = Color.parseColor(b2);
                    a2.w.setBackgroundColor(parseColor);
                    a2.x.setBackgroundColor(parseColor);
                }
                Object r0 = a2.u;
                wg6.a((Object) r0, "binding.tvAppVersion");
                nh6 nh6 = nh6.a;
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886859);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026w_Text__AppVersionNumber)");
                Object[] objArr = {h2};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r0.setText(format);
                a2.q.setOnClickListener(new b(this, h2));
                a2.r.setOnClickListener(new c(this, h2));
                a2.s.setOnClickListener(new d(this, h2));
                a2.z.setOnClickListener(new e(this, h2));
                a2.A.setOnClickListener(new f(this, h2));
                a2.y.setOnClickListener(new g(this, h2));
                a2.v.setOnClickListener(new h(this, h2));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(cl5 cl5) {
        wg6.b(cl5, "presenter");
        jk3.a(cl5);
        wg6.a((Object) cl5, "Preconditions.checkNotNull(presenter)");
        this.g = cl5;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = PreferredUnitFragment.v.a();
        local.d(a2, "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof BaseActivity)) {
            activity = null;
        }
        BaseActivity baseActivity = (BaseActivity) activity;
        if (baseActivity != null) {
            baseActivity.a(str, i2, intent);
        }
    }
}
