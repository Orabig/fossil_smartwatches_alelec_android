package com.portfolio.platform.uirenew.home.profile.unit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ap5;
import com.fossil.ax5;
import com.fossil.bd4;
import com.fossil.bp5;
import com.fossil.jk3;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.uo5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.zh4;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PreferredUnitFragment extends BaseFragment implements bp5, View.OnClickListener, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a((qg6) null);
    @DexIgnore
    public ax5<bd4> f;
    @DexIgnore
    public ap5 g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public GradientDrawable r; // = new GradientDrawable();
    @DexIgnore
    public GradientDrawable s; // = new GradientDrawable();
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return PreferredUnitFragment.u;
        }

        @DexIgnore
        public final PreferredUnitFragment b() {
            return new PreferredUnitFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitFragment a;

        @DexIgnore
        public b(PreferredUnitFragment preferredUnitFragment) {
            this.a = preferredUnitFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitFragment a;

        @DexIgnore
        public c(PreferredUnitFragment preferredUnitFragment) {
            this.a = preferredUnitFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PreferredUnitFragment.a(this.a).b(zh4.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitFragment a;

        @DexIgnore
        public d(PreferredUnitFragment preferredUnitFragment) {
            this.a = preferredUnitFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PreferredUnitFragment.a(this.a).b(zh4.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitFragment a;

        @DexIgnore
        public e(PreferredUnitFragment preferredUnitFragment) {
            this.a = preferredUnitFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PreferredUnitFragment.a(this.a).d(zh4.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitFragment a;

        @DexIgnore
        public f(PreferredUnitFragment preferredUnitFragment) {
            this.a = preferredUnitFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PreferredUnitFragment.a(this.a).d(zh4.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitFragment a;

        @DexIgnore
        public g(PreferredUnitFragment preferredUnitFragment) {
            this.a = preferredUnitFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PreferredUnitFragment.a(this.a).a(zh4.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitFragment a;

        @DexIgnore
        public h(PreferredUnitFragment preferredUnitFragment) {
            this.a = preferredUnitFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PreferredUnitFragment.a(this.a).a(zh4.IMPERIAL);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitFragment a;

        @DexIgnore
        public i(PreferredUnitFragment preferredUnitFragment) {
            this.a = preferredUnitFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PreferredUnitFragment.a(this.a).c(zh4.METRIC);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitFragment a;

        @DexIgnore
        public j(PreferredUnitFragment preferredUnitFragment) {
            this.a = preferredUnitFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PreferredUnitFragment.a(this.a).c(zh4.IMPERIAL);
        }
    }

    /*
    static {
        String simpleName = PreferredUnitFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "PreferredUnitFragment::class.java.simpleName!!");
            u = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ap5 a(PreferredUnitFragment preferredUnitFragment) {
        ap5 ap5 = preferredUnitFragment.g;
        if (ap5 != null) {
            return ap5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void c(zh4 zh4) {
        ax5<bd4> ax5 = this.f;
        if (ax5 != null) {
            bd4 a2 = ax5.a();
            if (a2 != null) {
                View childAt = a2.t.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (zh4 != null) {
                        int i2 = uo5.a[zh4.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g b2 = a2.t.b(1);
                            if (b2 != null) {
                                b2.h();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            wg6.a((Object) childAt2, "selectedTab");
                            childAt2.setBackground(this.r);
                            View childAt3 = linearLayout.getChildAt(0);
                            wg6.a((Object) childAt3, "unSelectedTab");
                            childAt3.setBackground(this.s);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g b3 = a2.t.b(0);
                            if (b3 != null) {
                                b3.h();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            wg6.a((Object) childAt4, "selectedTab");
                            childAt4.setBackground(this.r);
                            View childAt5 = linearLayout.getChildAt(1);
                            wg6.a((Object) childAt5, "unSelectedTab");
                            childAt5.setBackground(this.s);
                            return;
                        }
                    }
                    TabLayout.g b4 = a2.t.b(0);
                    if (b4 != null) {
                        b4.h();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    wg6.a((Object) childAt6, "selectedTab");
                    childAt6.setBackground(this.r);
                    View childAt7 = linearLayout.getChildAt(1);
                    wg6.a((Object) childAt7, "unSelectedTab");
                    childAt7.setBackground(this.s);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d(zh4 zh4) {
        ax5<bd4> ax5 = this.f;
        if (ax5 != null) {
            bd4 a2 = ax5.a();
            if (a2 != null) {
                View childAt = a2.v.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (zh4 != null) {
                        int i2 = uo5.b[zh4.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g b2 = a2.v.b(1);
                            if (b2 != null) {
                                b2.h();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            wg6.a((Object) childAt2, "selectedTab");
                            childAt2.setBackground(this.r);
                            View childAt3 = linearLayout.getChildAt(0);
                            wg6.a((Object) childAt3, "unSelectedTab");
                            childAt3.setBackground(this.s);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g b3 = a2.v.b(0);
                            if (b3 != null) {
                                b3.h();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            wg6.a((Object) childAt4, "selectedTab");
                            childAt4.setBackground(this.r);
                            View childAt5 = linearLayout.getChildAt(1);
                            wg6.a((Object) childAt5, "unSelectedTab");
                            childAt5.setBackground(this.s);
                            return;
                        }
                    }
                    TabLayout.g b4 = a2.v.b(0);
                    if (b4 != null) {
                        b4.h();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    wg6.a((Object) childAt6, "selectedTab");
                    childAt6.setBackground(this.r);
                    View childAt7 = linearLayout.getChildAt(1);
                    wg6.a((Object) childAt7, "unSelectedTab");
                    childAt7.setBackground(this.s);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e(zh4 zh4) {
        ax5<bd4> ax5 = this.f;
        if (ax5 != null) {
            bd4 a2 = ax5.a();
            if (a2 != null) {
                View childAt = a2.s.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (zh4 != null) {
                        int i2 = uo5.c[zh4.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g b2 = a2.s.b(1);
                            if (b2 != null) {
                                b2.h();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            wg6.a((Object) childAt2, "selectedTab");
                            childAt2.setBackground(this.r);
                            View childAt3 = linearLayout.getChildAt(0);
                            wg6.a((Object) childAt3, "unSelectedTab");
                            childAt3.setBackground(this.s);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g b3 = a2.s.b(0);
                            if (b3 != null) {
                                b3.h();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            wg6.a((Object) childAt4, "selectedTab");
                            childAt4.setBackground(this.r);
                            View childAt5 = linearLayout.getChildAt(1);
                            wg6.a((Object) childAt5, "unSelectedTab");
                            childAt5.setBackground(this.s);
                            return;
                        }
                    }
                    TabLayout.g b4 = a2.s.b(0);
                    if (b4 != null) {
                        b4.h();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    wg6.a((Object) childAt6, "selectedTab");
                    childAt6.setBackground(this.r);
                    View childAt7 = linearLayout.getChildAt(1);
                    wg6.a((Object) childAt7, "unSelectedTab");
                    childAt7.setBackground(this.s);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void o() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void onClick(View view) {
        wg6.b(view, "v");
        if (view.getId() == 2131361851) {
            o();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v3, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        Context context = getContext();
        if (context != null) {
            Integer.valueOf(w6.a(context, 2131100404));
            Context context2 = getContext();
            if (context2 != null) {
                Integer.valueOf(w6.a(context2, 2131099834));
                this.h = Color.parseColor(ThemeManager.l.a().b("primaryButton"));
                this.i = Color.parseColor(ThemeManager.l.a().b("onPrimaryButton"));
                this.j = Color.parseColor(ThemeManager.l.a().b("primaryButtonBorder"));
                this.o = Color.parseColor(ThemeManager.l.a().b("secondaryButton"));
                this.p = Color.parseColor(ThemeManager.l.a().b("onSecondaryButton"));
                this.q = Color.parseColor(ThemeManager.l.a().b("secondaryButtonBorder"));
                bd4 a2 = kb.a(LayoutInflater.from(getContext()), 2131558595, (ViewGroup) null, false, e1());
                a2.q.setOnClickListener(new b(this));
                View childAt = a2.t.getChildAt(0);
                if (childAt != null) {
                    View childAt2 = ((LinearLayout) childAt).getChildAt(0);
                    if (childAt2 != null) {
                        childAt2.setOnClickListener(new c(this));
                    }
                    View childAt3 = a2.t.getChildAt(0);
                    if (childAt3 != null) {
                        View childAt4 = ((LinearLayout) childAt3).getChildAt(1);
                        if (childAt4 != null) {
                            childAt4.setOnClickListener(new d(this));
                        }
                        View childAt5 = a2.v.getChildAt(0);
                        if (childAt5 != null) {
                            View childAt6 = ((LinearLayout) childAt5).getChildAt(0);
                            if (childAt6 != null) {
                                childAt6.setOnClickListener(new e(this));
                            }
                            View childAt7 = a2.v.getChildAt(0);
                            if (childAt7 != null) {
                                View childAt8 = ((LinearLayout) childAt7).getChildAt(1);
                                if (childAt8 != null) {
                                    childAt8.setOnClickListener(new f(this));
                                }
                                View childAt9 = a2.s.getChildAt(0);
                                if (childAt9 != null) {
                                    View childAt10 = ((LinearLayout) childAt9).getChildAt(0);
                                    if (childAt10 != null) {
                                        childAt10.setOnClickListener(new g(this));
                                    }
                                    View childAt11 = a2.s.getChildAt(0);
                                    if (childAt11 != null) {
                                        View childAt12 = ((LinearLayout) childAt11).getChildAt(1);
                                        if (childAt12 != null) {
                                            childAt12.setOnClickListener(new h(this));
                                        }
                                        View childAt13 = a2.u.getChildAt(0);
                                        if (childAt13 != null) {
                                            View childAt14 = ((LinearLayout) childAt13).getChildAt(0);
                                            if (childAt14 != null) {
                                                childAt14.setOnClickListener(new i(this));
                                            }
                                            View childAt15 = a2.u.getChildAt(0);
                                            if (childAt15 != null) {
                                                View childAt16 = ((LinearLayout) childAt15).getChildAt(1);
                                                if (childAt16 != null) {
                                                    childAt16.setOnClickListener(new j(this));
                                                }
                                                a2.s.a(this.p, this.i);
                                                a2.u.a(this.p, this.i);
                                                a2.t.a(this.p, this.i);
                                                a2.v.a(this.p, this.i);
                                                this.f = new ax5<>(this, a2);
                                                this.r.setShape(0);
                                                this.r.setColor(this.h);
                                                this.r.setStroke(4, this.j);
                                                this.s.setShape(0);
                                                this.s.setColor(this.o);
                                                this.s.setStroke(4, this.q);
                                                wg6.a((Object) a2, "binding");
                                                return a2.d();
                                            }
                                            throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                                        }
                                        throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                                    }
                                    throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                                }
                                throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                            }
                            throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                        }
                        throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                    }
                    throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
                }
                throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        PreferredUnitFragment.super.onPause();
        ap5 ap5 = this.g;
        if (ap5 != null) {
            ap5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        PreferredUnitFragment.super.onResume();
        ap5 ap5 = this.g;
        if (ap5 != null) {
            ap5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ap5 ap5) {
        wg6.b(ap5, "presenter");
        jk3.a(ap5);
        wg6.a((Object) ap5, "checkNotNull(presenter)");
        this.g = ap5;
    }

    @DexIgnore
    public void a(zh4 zh4) {
        ax5<bd4> ax5 = this.f;
        if (ax5 != null) {
            bd4 a2 = ax5.a();
            if (a2 != null) {
                View childAt = a2.u.getChildAt(0);
                if (childAt != null) {
                    LinearLayout linearLayout = (LinearLayout) childAt;
                    if (zh4 != null) {
                        int i2 = uo5.d[zh4.ordinal()];
                        if (i2 == 1) {
                            TabLayout.g b2 = a2.u.b(1);
                            if (b2 != null) {
                                b2.h();
                            }
                            View childAt2 = linearLayout.getChildAt(1);
                            wg6.a((Object) childAt2, "selectedTab");
                            childAt2.setBackground(this.r);
                            View childAt3 = linearLayout.getChildAt(0);
                            wg6.a((Object) childAt3, "unSelectedTab");
                            childAt3.setBackground(this.s);
                            return;
                        } else if (i2 == 2) {
                            TabLayout.g b3 = a2.u.b(0);
                            if (b3 != null) {
                                b3.h();
                            }
                            View childAt4 = linearLayout.getChildAt(0);
                            wg6.a((Object) childAt4, "selectedTab");
                            childAt4.setBackground(this.r);
                            View childAt5 = linearLayout.getChildAt(1);
                            wg6.a((Object) childAt5, "unSelectedTab");
                            childAt5.setBackground(this.s);
                            return;
                        }
                    }
                    TabLayout.g b4 = a2.u.b(0);
                    if (b4 != null) {
                        b4.h();
                    }
                    View childAt6 = linearLayout.getChildAt(0);
                    wg6.a((Object) childAt6, "selectedTab");
                    childAt6.setBackground(this.r);
                    View childAt7 = linearLayout.getChildAt(1);
                    wg6.a((Object) childAt7, "unSelectedTab");
                    childAt7.setBackground(this.s);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.widget.LinearLayout");
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i2, String str) {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = u;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0) && getActivity() != null && str.hashCode() == 1008390942 && str.equals("NO_INTERNET_CONNECTION")) {
            BaseActivity activity = getActivity();
            if (activity != null) {
                activity.m();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }
}
