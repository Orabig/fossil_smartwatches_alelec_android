package com.portfolio.platform.uirenew.home.profile.password;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.fm5;
import com.fossil.hm5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileChangePasswordActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public hm5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            Intent intent = new Intent(context, ProfileChangePasswordActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        ProfileChangePasswordFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = ProfileChangePasswordFragment.y.a();
            a((Fragment) b, 2131362119);
        }
        PortfolioApp.get.instance().g().a(new fm5(b)).a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        a(false);
    }
}
