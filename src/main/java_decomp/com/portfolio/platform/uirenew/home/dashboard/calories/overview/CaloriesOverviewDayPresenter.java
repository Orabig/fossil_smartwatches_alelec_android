package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.fd5;
import com.fossil.ff6;
import com.fossil.gd5;
import com.fossil.gk6;
import com.fossil.hd5$b$a;
import com.fossil.hd5$b$b;
import com.fossil.hd5$b$c;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.sh4;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CaloriesOverviewDayPresenter extends fd5 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.p.e());
    @DexIgnore
    public Date f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public LiveData<yx5<List<ActivitySummary>>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<List<ActivitySample>>> j; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<List<WorkoutSession>>> k; // = new MutableLiveData();
    @DexIgnore
    public /* final */ gd5 l;
    @DexIgnore
    public /* final */ SummariesRepository m;
    @DexIgnore
    public /* final */ ActivitiesRepository n;
    @DexIgnore
    public /* final */ WorkoutSessionRepository o;
    @DexIgnore
    public /* final */ PortfolioApp p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$showDetailChart$1", f = "CaloriesOverviewDayPresenter.kt", l = {122, 124, 125}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewDayPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(CaloriesOverviewDayPresenter caloriesOverviewDayPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = caloriesOverviewDayPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00a7 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00a8  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00bf  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00c4  */
        public final Object invokeSuspend(Object obj) {
            lc6 lc6;
            ArrayList arrayList;
            Integer num;
            il6 il6;
            lc6 lc62;
            Object a;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                dl6 a3 = this.this$0.b();
                hd5$b$c hd5_b_c = new hd5$b$c(this, (xe6) null);
                this.L$0 = il62;
                this.label = 1;
                Object a4 = gk6.a(a3, hd5_b_c, this);
                if (a4 == a2) {
                    return a2;
                }
                il6 = il62;
                obj = a4;
            } else if (i == 1) {
                nc6.a(obj);
                il6 = (il6) this.L$0;
            } else if (i == 2) {
                arrayList = (ArrayList) this.L$2;
                lc62 = (lc6) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                Integer num2 = (Integer) obj;
                dl6 a5 = this.this$0.b();
                hd5$b$a hd5_b_a = new hd5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = lc62;
                this.L$2 = arrayList;
                this.L$3 = num2;
                this.label = 3;
                a = gk6.a(a5, hd5_b_a, this);
                if (a != a2) {
                    return a2;
                }
                num = num2;
                obj = a;
                lc6 = lc62;
                int a6 = ik4.d.a((ActivitySummary) obj, sh4.CALORIES);
                this.this$0.l.b(new BarChart.c(Math.max(num == null ? num.intValue() : 0, a6 / 16), a6, arrayList), (ArrayList) lc6.getSecond());
                return cd6.a;
            } else if (i == 3) {
                num = (Integer) this.L$3;
                arrayList = (ArrayList) this.L$2;
                lc6 = (lc6) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                int a62 = ik4.d.a((ActivitySummary) obj, sh4.CALORIES);
                this.this$0.l.b(new BarChart.c(Math.max(num == null ? num.intValue() : 0, a62 / 16), a62, arrayList), (ArrayList) lc6.getSecond());
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc63 = (lc6) obj;
            arrayList = (ArrayList) lc63.getFirst();
            dl6 a7 = this.this$0.b();
            hd5$b$b hd5_b_b = new hd5$b$b(arrayList, (xe6) null);
            this.L$0 = il6;
            this.L$1 = lc63;
            this.L$2 = arrayList;
            this.label = 2;
            Object a8 = gk6.a(a7, hd5_b_b, this);
            if (a8 == a2) {
                return a2;
            }
            Object obj2 = a8;
            lc62 = lc63;
            obj = obj2;
            Integer num22 = (Integer) obj;
            dl6 a52 = this.this$0.b();
            hd5$b$a hd5_b_a2 = new hd5$b$a(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = lc62;
            this.L$2 = arrayList;
            this.L$3 = num22;
            this.label = 3;
            a = gk6.a(a52, hd5_b_a2, this);
            if (a != a2) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<yx5<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewDayPresenter a;

        @DexIgnore
        public c(CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
            this.a = caloriesOverviewDayPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<ActivitySummary>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesOverviewDayPresenter", sb.toString());
            if (a2 != wh4.DATABASE_LOADING) {
                this.a.g = true;
                if (this.a.g && this.a.h) {
                    rm6 unused = this.a.k();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<yx5<? extends List<ActivitySample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewDayPresenter a;

        @DexIgnore
        public d(CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
            this.a = caloriesOverviewDayPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<ActivitySample>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySamples -- activitySamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesOverviewDayPresenter", sb.toString());
            if (a2 != wh4.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.g && this.a.h) {
                    rm6 unused = this.a.k();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ld<yx5<? extends List<WorkoutSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewDayPresenter a;

        @DexIgnore
        public e(CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
            this.a = caloriesOverviewDayPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<WorkoutSession>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesOverviewDayPresenter", sb.toString());
            if (a2 == wh4.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.a.l.a(false, new ArrayList());
            } else {
                this.a.l.a(true, list);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public CaloriesOverviewDayPresenter(gd5 gd5, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository, PortfolioApp portfolioApp) {
        wg6.b(gd5, "mView");
        wg6.b(summariesRepository, "mSummariesRepository");
        wg6.b(activitiesRepository, "mActivitiesRepository");
        wg6.b(workoutSessionRepository, "mWorkoutSessionRepository");
        wg6.b(portfolioApp, "mApp");
        this.l = gd5;
        this.m = summariesRepository;
        this.n = activitiesRepository;
        this.o = workoutSessionRepository;
        this.p = portfolioApp;
    }

    @DexIgnore
    public static final /* synthetic */ Date d(CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
        Date date = caloriesOverviewDayPresenter.f;
        if (date != null) {
            return date;
        }
        wg6.d("mDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                wg6.d("mDate");
                throw null;
            } else if (bk4.t(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.f;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("CaloriesOverviewDayPresenter", sb.toString());
                    return;
                }
                wg6.d("mDate");
                throw null;
            }
        }
        this.g = false;
        this.h = false;
        this.f = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.f;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("CaloriesOverviewDayPresenter", sb2.toString());
            SummariesRepository summariesRepository = this.m;
            Date date4 = this.f;
            if (date4 == null) {
                wg6.d("mDate");
                throw null;
            } else if (date4 != null) {
                this.i = summariesRepository.getSummaries(date4, date4, false);
                ActivitiesRepository activitiesRepository = this.n;
                Date date5 = this.f;
                if (date5 == null) {
                    wg6.d("mDate");
                    throw null;
                } else if (date5 != null) {
                    this.j = activitiesRepository.getActivityList(date5, date5, true);
                    WorkoutSessionRepository workoutSessionRepository = this.o;
                    Date date6 = this.f;
                    if (date6 == null) {
                        wg6.d("mDate");
                        throw null;
                    } else if (date6 != null) {
                        this.k = workoutSessionRepository.getWorkoutSessions(date6, date6, true);
                    } else {
                        wg6.d("mDate");
                        throw null;
                    }
                } else {
                    wg6.d("mDate");
                    throw null;
                }
            } else {
                wg6.d("mDate");
                throw null;
            }
        } else {
            wg6.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        this.l.a(this);
    }

    @DexIgnore
    public final rm6 k() {
        return ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayPresenter", "start");
        i();
        LiveData<yx5<List<ActivitySummary>>> liveData = this.i;
        gd5 gd5 = this.l;
        if (gd5 != null) {
            liveData.a((CaloriesOverviewDayFragment) gd5, new c(this));
            this.j.a(this.l, new d(this));
            this.k.a(this.l, new e(this));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayPresenter", "stop");
        try {
            LiveData<yx5<List<ActivitySample>>> liveData = this.j;
            gd5 gd5 = this.l;
            if (gd5 != null) {
                liveData.a((CaloriesOverviewDayFragment) gd5);
                this.i.a(this.l);
                this.k.a(this.l);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }
}
