package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import com.fossil.af6;
import com.fossil.bn4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.q55;
import com.fossil.qg6;
import com.fossil.r55;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.u55$b$a;
import com.fossil.wg6;
import com.fossil.xe6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Ringtone;
import java.util.ArrayList;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchRingPhonePresenter extends q55 {
    @DexIgnore
    public /* final */ ArrayList<Ringtone> e; // = new ArrayList<>();
    @DexIgnore
    public Ringtone f;
    @DexIgnore
    public /* final */ Gson g; // = new Gson();
    @DexIgnore
    public /* final */ r55 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$1", f = "SearchRingPhonePresenter.kt", l = {29}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SearchRingPhonePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(SearchRingPhonePresenter searchRingPhonePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = searchRingPhonePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ArrayList arrayList;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.e.isEmpty()) {
                    ArrayList b = this.this$0.e;
                    dl6 a2 = this.this$0.b();
                    u55$b$a u55_b_a = new u55$b$a((xe6) null);
                    this.L$0 = il6;
                    this.L$1 = b;
                    this.label = 1;
                    obj = gk6.a(a2, u55_b_a, this);
                    if (obj == a) {
                        return a;
                    }
                    arrayList = b;
                }
                this.this$0.j().r(this.this$0.e);
                return cd6.a;
            } else if (i == 1) {
                arrayList = (ArrayList) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList.addAll((Collection) obj);
            if (this.this$0.f == null) {
                SearchRingPhonePresenter searchRingPhonePresenter = this.this$0;
                searchRingPhonePresenter.f = (Ringtone) searchRingPhonePresenter.e.get(0);
            }
            this.this$0.j().r(this.this$0.e);
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public SearchRingPhonePresenter(r55 r55) {
        wg6.b(r55, "mView");
        this.h = r55;
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public Ringtone h() {
        Ringtone ringtone = this.f;
        if (ringtone != null) {
            return ringtone;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void i() {
        bn4.o.a().b();
    }

    @DexIgnore
    public final r55 j() {
        return this.h;
    }

    @DexIgnore
    public void k() {
        this.h.a(this);
    }

    @DexIgnore
    public void b(Ringtone ringtone) {
        wg6.b(ringtone, Constants.RINGTONE);
        this.f = ringtone;
    }

    @DexIgnore
    public void a(Ringtone ringtone) {
        wg6.b(ringtone, Constants.RINGTONE);
        bn4.o.a().a(ringtone);
        this.f = ringtone;
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, MicroAppSetting.SETTING);
        try {
            this.f = (Ringtone) this.g.a(str, Ringtone.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SearchRingPhonePresenter", "exception when parse ringtone setting " + e2);
        }
    }
}
