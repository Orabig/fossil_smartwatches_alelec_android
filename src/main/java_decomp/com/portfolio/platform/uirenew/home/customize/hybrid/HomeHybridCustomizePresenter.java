package com.portfolio.platform.uirenew.home.customize.hybrid;

import android.content.Intent;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.n35;
import com.fossil.nc6;
import com.fossil.o35;
import com.fossil.qc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rd6;
import com.fossil.rk4;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.uh4;
import com.fossil.v85;
import com.fossil.w85;
import com.fossil.wg6;
import com.fossil.x85;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.y85$b$a;
import com.fossil.y85$c$a;
import com.fossil.y85$d$a;
import com.fossil.y85$e$a;
import com.fossil.y85$g$a;
import com.fossil.y85$g$b;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeHybridCustomizePresenter extends w85 {
    @DexIgnore
    public LiveData<List<HybridPreset>> e; // = new MutableLiveData();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<HybridPreset> g; // = new ArrayList<>();
    @DexIgnore
    public HybridPreset h;
    @DexIgnore
    public MutableLiveData<String> i; // = this.m.f();
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public int k; // = 1;
    @DexIgnore
    public lc6<Boolean, ? extends List<HybridPreset>> l; // = qc6.a(false, null);
    @DexIgnore
    public /* final */ PortfolioApp m;
    @DexIgnore
    public /* final */ x85 n;
    @DexIgnore
    public /* final */ MicroAppRepository o;
    @DexIgnore
    public /* final */ HybridPresetRepository p;
    @DexIgnore
    public /* final */ SetHybridPresetToWatchUseCase q;
    @DexIgnore
    public /* final */ an4 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$createNewPreset$1", f = "HomeHybridCustomizePresenter.kt", l = {291}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HomeHybridCustomizePresenter homeHybridCustomizePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeHybridCustomizePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                Iterator it = this.this$0.g.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (hf6.a(((HybridPreset) obj2).isActive()).booleanValue()) {
                        break;
                    }
                }
                HybridPreset hybridPreset = (HybridPreset) obj2;
                if (hybridPreset != null) {
                    HybridPreset cloneFrom = HybridPreset.Companion.cloneFrom(hybridPreset);
                    dl6 b = this.this$0.c();
                    y85$b$a y85_b_a = new y85$b$a(cloneFrom, (xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = hybridPreset;
                    this.L$2 = hybridPreset;
                    this.L$3 = cloneFrom;
                    this.label = 1;
                    if (gk6.a(b, y85_b_a, this) == a) {
                        return a;
                    }
                }
                return cd6.a;
            } else if (i == 1) {
                HybridPreset hybridPreset2 = (HybridPreset) this.L$3;
                HybridPreset hybridPreset3 = (HybridPreset) this.L$2;
                HybridPreset hybridPreset4 = (HybridPreset) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.n.v();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<v85.d, v85.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset a;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset b;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter c;

        @DexIgnore
        public c(HybridPreset hybridPreset, HybridPreset hybridPreset2, HomeHybridCustomizePresenter homeHybridCustomizePresenter, String str) {
            this.a = hybridPreset;
            this.b = hybridPreset2;
            this.c = homeHybridCustomizePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetHybridPresetToWatchUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            rm6 unused = ik6.b(this.c.e(), (af6) null, (ll6) null, new y85$c$a(this, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public void a(SetHybridPresetToWatchUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            this.c.n.n();
            int b2 = bVar.b();
            if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
                List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(bVar.a());
                wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                x85 l = this.c.n;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                if (array != null) {
                    uh4[] uh4Arr = (uh4[]) array;
                    l.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.c.n.j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $nextActivePresetId$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $preset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(HybridPreset hybridPreset, xe6 xe6, HomeHybridCustomizePresenter homeHybridCustomizePresenter, String str) {
            super(2, xe6);
            this.$preset = hybridPreset;
            this.this$0 = homeHybridCustomizePresenter;
            this.$nextActivePresetId$inlined = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.$preset, xe6, this.this$0, this.$nextActivePresetId$inlined);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                y85$d$a y85_d_a = new y85$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, y85_d_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "mView.showDeleteSuccessfully " + (this.this$0.i() - 1));
            this.this$0.n.d(this.this$0.i() - 1);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$renameCurrentPreset$1", f = "HomeHybridCustomizePresenter.kt", l = {146}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(HomeHybridCustomizePresenter homeHybridCustomizePresenter, String str, String str2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeHybridCustomizePresenter;
            this.$presetId = str;
            this.$name = str2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$presetId, this.$name, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                Iterator it = this.this$0.g.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    obj2 = it.next();
                    if (hf6.a(wg6.a((Object) ((HybridPreset) obj2).getId(), (Object) this.$presetId)).booleanValue()) {
                        break;
                    }
                }
                HybridPreset hybridPreset = (HybridPreset) obj2;
                HybridPreset clone = hybridPreset != null ? hybridPreset.clone() : null;
                if (clone != null) {
                    clone.setName(this.$name);
                    dl6 b = this.this$0.c();
                    y85$e$a y85_e_a = new y85$e$a(clone, (xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = clone;
                    this.L$2 = clone;
                    this.label = 1;
                    if (gk6.a(b, y85_e_a, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                HybridPreset hybridPreset2 = (HybridPreset) this.L$2;
                HybridPreset hybridPreset3 = (HybridPreset) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements m24.e<v85.d, v85.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter a;

        @DexIgnore
        public f(HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
            this.a = homeHybridCustomizePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetHybridPresetToWatchUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.n.n();
            this.a.n.f(0);
        }

        @DexIgnore
        public void a(SetHybridPresetToWatchUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            this.a.n.n();
            int b = bVar.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(bVar.a());
                wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                x85 l = this.a.n;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                if (array != null) {
                    uh4[] uh4Arr = (uh4[]) array;
                    l.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.a.n.j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1", f = "HomeHybridCustomizePresenter.kt", l = {61}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(HomeHybridCustomizePresenter homeHybridCustomizePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeHybridCustomizePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0063  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0085  */
        public final Object invokeSuspend(Object obj) {
            x85 l;
            ArrayList arrayList;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.f.isEmpty()) {
                    ArrayList a2 = this.this$0.f;
                    dl6 b = this.this$0.c();
                    y85$g$a y85_g_a = new y85$g$a(this, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = a2;
                    this.label = 1;
                    obj = gk6.a(b, y85_g_a, this);
                    if (obj == a) {
                        return a;
                    }
                    arrayList = a2;
                }
                MutableLiveData i2 = this.this$0.i;
                l = this.this$0.n;
                if (l == null) {
                    i2.a((HomeHybridCustomizeFragment) l, new y85$g$b(this));
                    this.this$0.q.e();
                    BleCommandResultManager.d.a(CommunicateMode.SET_LINK_MAPPING);
                    return cd6.a;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            } else if (i == 1) {
                arrayList = (ArrayList) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList.addAll((Collection) obj);
            MutableLiveData i22 = this.this$0.i;
            l = this.this$0.n;
            if (l == null) {
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public HomeHybridCustomizePresenter(PortfolioApp portfolioApp, x85 x85, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, an4 an4) {
        wg6.b(portfolioApp, "mApp");
        wg6.b(x85, "mView");
        wg6.b(microAppRepository, "mMicroAppRepository");
        wg6.b(hybridPresetRepository, "mHybridPresetRepository");
        wg6.b(setHybridPresetToWatchUseCase, "mSetHybridPresetToWatchUseCase");
        wg6.b(an4, "mSharedPreferencesManager");
        this.m = portfolioApp;
        this.n = x85;
        this.o = microAppRepository;
        this.p = hybridPresetRepository;
        this.q = setHybridPresetToWatchUseCase;
        this.r = an4;
    }

    @DexIgnore
    public final boolean b(HybridPreset hybridPreset) {
        if (hybridPreset == null) {
            return true;
        }
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        for (T next : buttons) {
            if (rk4.c.c(((HybridPresetAppSetting) next).getAppId())) {
                arrayList.add(next);
            }
        }
        HybridPresetAppSetting hybridPresetAppSetting = null;
        if (!arrayList.isEmpty()) {
            Iterator it = arrayList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) it.next();
                if (!rk4.c.e(hybridPresetAppSetting2.getAppId())) {
                    hybridPresetAppSetting = hybridPresetAppSetting2;
                    break;
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "setPresetToWatch missingPermissionComp " + hybridPresetAppSetting);
        if (hybridPresetAppSetting == null) {
            return true;
        }
        this.n.t(hybridPresetAppSetting.getAppId());
        return false;
    }

    @DexIgnore
    public final void c(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new g(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        try {
            LiveData<List<HybridPreset>> liveData = this.e;
            x85 x85 = this.n;
            if (x85 != null) {
                liveData.a((HomeHybridCustomizeFragment) x85);
                this.i.a(this.n);
                this.q.g();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "Exception when remove observer.");
        }
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final int i() {
        return this.j;
    }

    @DexIgnore
    public void j() {
        this.n.a(this);
    }

    @DexIgnore
    public final void k() {
        ArrayList<HybridPreset> arrayList = this.g;
        ArrayList arrayList2 = new ArrayList(rd6.a(arrayList, 10));
        for (HybridPreset a2 : arrayList) {
            arrayList2.add(a(a2));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "showPresets - size=" + this.g.size() + " uiData " + arrayList2);
        this.n.i(arrayList2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final n35 a(HybridPreset hybridPreset) {
        String str;
        MicroApp microApp;
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        Iterator<HybridPresetAppSetting> it = buttons.iterator();
        while (true) {
            str = "";
            if (!it.hasNext()) {
                break;
            }
            HybridPresetAppSetting next = it.next();
            String component1 = next.component1();
            String component2 = next.component2();
            Iterator<T> it2 = this.f.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    microApp = null;
                    break;
                }
                microApp = it2.next();
                if (wg6.a((Object) microApp.getId(), (Object) component2)) {
                    break;
                }
            }
            MicroApp microApp2 = (MicroApp) microApp;
            if (microApp2 != null) {
                String id = microApp2.getId();
                String icon = microApp2.getIcon();
                if (icon != null) {
                    str = icon;
                }
                arrayList.add(new o35(id, str, jm4.a(PortfolioApp.get.instance(), microApp2.getNameKey(), microApp2.getName()), component1, (String) null, 16, (qg6) null));
            }
        }
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "convertPresetToHybridPresetConfigWrapper");
        String id2 = hybridPreset.getId();
        String name = hybridPreset.getName();
        if (name != null) {
            str = name;
        }
        return new n35(id2, str, arrayList, hybridPreset.isActive());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$c] */
    public final void b(String str) {
        T t;
        HybridPreset hybridPreset;
        xm4 xm4 = xm4.d;
        x85 x85 = this.n;
        if (x85 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (xm4.a(xm4, ((HomeHybridCustomizeFragment) x85).getContext(), "SET_MICRO_APP", false, false, false, 28, (Object) null)) {
            List list = (List) this.e.a();
            if (list != null) {
                Boolean.valueOf(!list.isEmpty());
            }
            Iterator<T> it = this.g.iterator();
            while (true) {
                t = null;
                if (!it.hasNext()) {
                    hybridPreset = null;
                    break;
                }
                hybridPreset = it.next();
                if (wg6.a((Object) hybridPreset.getId(), (Object) str)) {
                    break;
                }
            }
            HybridPreset hybridPreset2 = (HybridPreset) hybridPreset;
            if (hybridPreset2 != null) {
                Iterator<T> it2 = this.g.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    T next = it2.next();
                    if (((HybridPreset) next).isActive()) {
                        t = next;
                        break;
                    }
                }
                HybridPreset hybridPreset3 = (HybridPreset) t;
                this.n.m();
                FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "delete activePreset " + hybridPreset3 + " set preset " + hybridPreset2 + " as active first");
                this.q.a(new SetHybridPresetToWatchUseCase.c(hybridPreset2), new c(hybridPreset3, hybridPreset2, this, str));
            }
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (this.g.size() > i2) {
            this.j = i2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "changePresetPosition " + this.j);
            this.h = this.g.get(this.j);
            HybridPreset hybridPreset = this.h;
            if (hybridPreset != null) {
                this.n.j(hybridPreset.isActive());
                return;
            }
            return;
        }
        this.n.j(false);
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "onHomeTabChange - tab: " + i2);
        this.k = i2;
        if (this.k == 1 && this.l.getFirst().booleanValue()) {
            List list = (List) this.l.getSecond();
            if (list != null) {
                a((List<HybridPreset>) list);
            }
            this.l = qc6.a(false, null);
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        wg6.b(str, "name");
        wg6.b(str2, "presetId");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, str2, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$f] */
    public void a(boolean z) {
        HybridPreset hybridPreset;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "setPresetToWatch mCurrentPreset=" + this.h);
        Set<Integer> a2 = xm4.d.a(this.h);
        if (z) {
            this.r.q(true);
            this.r.r(true);
        }
        xm4 xm4 = xm4.d;
        x85 x85 = this.n;
        if (x85 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (xm4.a(((HomeHybridCustomizeFragment) x85).getContext(), a2) && (hybridPreset = this.h) != null) {
            this.n.m();
            this.q.a(new SetHybridPresetToWatchUseCase.c(hybridPreset), new f(this));
        }
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "nextActivePresetId");
        HybridPreset hybridPreset = this.h;
        if (hybridPreset == null) {
            return;
        }
        if (hybridPreset.isActive()) {
            b(str);
        } else {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(hybridPreset, (xe6) null, this, str), 3, (Object) null);
        }
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        if (i2 == 100 && i3 == -1) {
            this.n.e(0);
        }
        if (i2 == 111) {
            xm4 xm4 = xm4.d;
            x85 x85 = this.n;
            if (x85 == null) {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            } else if (xm4.a(((HomeHybridCustomizeFragment) x85).getContext(), 1)) {
                a(false);
            }
        }
    }

    @DexIgnore
    public final void a(List<HybridPreset> list) {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "doShowingPreset");
        this.g.clear();
        this.g.addAll(list);
        int size = this.g.size();
        int i2 = this.j;
        if (size > i2 && i2 > 0) {
            this.h = this.g.get(i2);
        }
        k();
    }
}
