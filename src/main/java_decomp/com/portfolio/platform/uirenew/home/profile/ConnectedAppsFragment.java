package com.portfolio.platform.uirenew.home.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ConnectedAppsFragmentBinding;
import com.fossil.ax5;
import com.fossil.cw4;
import com.fossil.dw4;
import com.fossil.gv1;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lx5;
import com.fossil.p74;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConnectedAppsFragment extends BaseFragment implements dw4 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public ax5<p74> f;
    @DexIgnore
    public cw4 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ConnectedAppsFragment.i;
        }

        @DexIgnore
        public final ConnectedAppsFragment b() {
            return new ConnectedAppsFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsFragment a;

        @DexIgnore
        public b(ConnectedAppsFragment connectedAppsFragment) {
            this.a = connectedAppsFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            wg6.b(compoundButton, "button");
            if (compoundButton.isPressed()) {
                ConnectedAppsFragment.a(this.a).h();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ConnectedAppsFragment a;

        @DexIgnore
        public c(ConnectedAppsFragment connectedAppsFragment) {
            this.a = connectedAppsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.getActivity() != null) {
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    activity.finish();
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    /*
    static {
        String simpleName = ConnectedAppsFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "ConnectedAppsFragment::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ cw4 a(ConnectedAppsFragment connectedAppsFragment) {
        cw4 cw4 = connectedAppsFragment.g;
        if (cw4 != null) {
            return cw4;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void B(boolean z) {
        if (isActive()) {
            ax5<p74> ax5 = this.f;
            if (ax5 != null) {
                ConnectedAppsFragmentBinding a2 = ax5.a();
                if (a2 != null) {
                    FlexibleSwitchCompat flexibleSwitchCompat = a2.q;
                    wg6.a((Object) flexibleSwitchCompat, "it.cbGooglefit");
                    flexibleSwitchCompat.setChecked(z);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return i;
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public void o(int i2) {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, "", childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1 && i3 == -1) {
            cw4 cw4 = this.g;
            if (cw4 != null) {
                cw4.i();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        ConnectedAppsFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558521, viewGroup, false, e1()));
        ax5<p74> ax5 = this.f;
        if (ax5 != null) {
            ConnectedAppsFragmentBinding a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ConnectedAppsFragment.super.onPause();
        cw4 cw4 = this.g;
        if (cw4 != null) {
            cw4.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        ConnectedAppsFragment.super.onResume();
        cw4 cw4 = this.g;
        if (cw4 != null) {
            cw4.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r2v7, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<p74> ax5 = this.f;
        if (ax5 != null) {
            ConnectedAppsFragmentBinding a2 = ax5.a();
            if (a2 != null) {
                a2.q.setOnCheckedChangeListener(new b(this));
                a2.r.setOnClickListener(new c(this));
            }
            W("connected_app_view");
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(cw4 cw4) {
        wg6.b(cw4, "presenter");
        this.g = cw4;
    }

    @DexIgnore
    public void a(gv1 gv1) {
        wg6.b(gv1, "connectionResult");
        gv1.a(getActivity(), 1);
    }
}
