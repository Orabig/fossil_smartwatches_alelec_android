package com.portfolio.platform.uirenew.home.profile.goal;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rk5;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.sh4;
import com.fossil.sk5;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.wk5$b$a;
import com.fossil.wk5$b$b;
import com.fossil.wk5$d$a;
import com.fossil.wk5$e$a;
import com.fossil.xe6;
import com.fossil.xk5;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.BaseFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileGoalEditPresenter extends rk5 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public LiveData<yx5<ActivitySettings>> e; // = new MutableLiveData();
    @DexIgnore
    public ActivitySettings f; // = new ActivitySettings(5000, 140, 30);
    @DexIgnore
    public ActivitySettings g; // = new ActivitySettings(5000, 140, 30);
    @DexIgnore
    public int h; // = 480;
    @DexIgnore
    public int i; // = 480;
    @DexIgnore
    public int j; // = 8;
    @DexIgnore
    public int k; // = 8;
    @DexIgnore
    public LiveData<yx5<Integer>> l; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<Integer>> m; // = new MutableLiveData();
    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE n; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().e());
    @DexIgnore
    public /* final */ sk5 o;
    @DexIgnore
    public /* final */ SummariesRepository p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;
    @DexIgnore
    public /* final */ GoalTrackingRepository r;
    @DexIgnore
    public /* final */ UserRepository s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$checkUserUsingDefaultGoal$1", f = "ProfileGoalEditPresenter.kt", l = {119, 124}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ProfileGoalEditPresenter profileGoalEditPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileGoalEditPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 b = this.this$0.c();
                wk5$b$a wk5_b_a = new wk5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, wk5_b_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                MFUser mFUser = (MFUser) this.L$2;
                MFUser mFUser2 = (MFUser) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser3 = (MFUser) obj;
            if (mFUser3 != null && mFUser3.isUseDefaultGoals()) {
                mFUser3.setUseDefaultGoals(false);
                dl6 b2 = this.this$0.c();
                wk5$b$b wk5_b_b = new wk5$b$b(mFUser3, (xe6) null, this);
                this.L$0 = il6;
                this.L$1 = mFUser3;
                this.L$2 = mFUser3;
                this.label = 2;
                if (gk6.a(b2, wk5_b_b, this) == a) {
                    return a;
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<yx5<? extends ActivitySettings>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ LiveData b;

        @DexIgnore
        public c(ProfileGoalEditPresenter profileGoalEditPresenter, LiveData liveData) {
            this.a = profileGoalEditPresenter;
            this.b = liveData;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<ActivitySettings> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String s = ProfileGoalEditPresenter.t;
            local.d(s, "saveActivityGoal status " + yx5.f());
            if ((yx5 != null ? yx5.f() : null) != wh4.SUCCESS || yx5.d() == null) {
                if ((yx5 != null ? yx5.f() : null) == wh4.ERROR) {
                    sk5 m = this.a.m();
                    Integer c = yx5.c();
                    if (c != null) {
                        int intValue = c.intValue();
                        String e = yx5.e();
                        if (e != null) {
                            m.d(intValue, e);
                            this.b.a(this.a.m());
                            this.a.o();
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                return;
            }
            ProfileGoalEditPresenter profileGoalEditPresenter = this.a;
            profileGoalEditPresenter.a(profileGoalEditPresenter.g);
            rm6 unused = this.a.l();
            this.b.a(this.a.m());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveGoalTrackingTarget$1", f = "ProfileGoalEditPresenter.kt", l = {58}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ProfileGoalEditPresenter profileGoalEditPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileGoalEditPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                wk5$d$a wk5_d_a = new wk5$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, wk5_d_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter$saveSleepGoal$1", f = "ProfileGoalEditPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ProfileGoalEditPresenter profileGoalEditPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileGoalEditPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                LiveData<yx5<Integer>> updateLastSleepGoal = this.this$0.q.updateLastSleepGoal(this.this$0.i);
                sk5 m = this.this$0.m();
                if (m != null) {
                    updateLastSleepGoal.a((BaseFragment) m, new wk5$e$a(this, updateLastSleepGoal));
                    return cd6.a;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ld<yx5<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter a;

        @DexIgnore
        public f(ProfileGoalEditPresenter profileGoalEditPresenter) {
            this.a = profileGoalEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<Integer> yx5) {
            Integer num = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                if (yx5 != null) {
                    num = yx5.d();
                }
                if (num != null) {
                    this.a.a(yx5.d().intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<yx5<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter a;

        @DexIgnore
        public g(ProfileGoalEditPresenter profileGoalEditPresenter) {
            this.a = profileGoalEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<Integer> yx5) {
            Integer num = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                if (yx5 != null) {
                    num = yx5.d();
                }
                if (num != null) {
                    this.a.b(yx5.d().intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ld<yx5<? extends ActivitySettings>> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileGoalEditPresenter a;

        @DexIgnore
        public h(ProfileGoalEditPresenter profileGoalEditPresenter) {
            this.a = profileGoalEditPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<ActivitySettings> yx5) {
            ActivitySettings activitySettings = null;
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                if (yx5 != null) {
                    activitySettings = yx5.d();
                }
                if (activitySettings != null) {
                    this.a.b(yx5.d());
                }
            }
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = ProfileGoalEditPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "ProfileGoalEditPresenter::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public ProfileGoalEditPresenter(sk5 sk5, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository) {
        wg6.b(sk5, "mView");
        wg6.b(summariesRepository, "mSummariesRepository");
        wg6.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        wg6.b(userRepository, "mUserRepository");
        this.o = sk5;
        this.p = summariesRepository;
        this.q = sleepSummariesRepository;
        this.r = goalTrackingRepository;
        this.s = userRepository;
    }

    @DexIgnore
    public void j() {
        this.o.j(this.h);
    }

    @DexIgnore
    public void k() {
        if (i() != FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
        } else {
            q();
        }
    }

    @DexIgnore
    public final rm6 l() {
        return ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final sk5 m() {
        return this.o;
    }

    @DexIgnore
    public final boolean n() {
        boolean z = (this.g.getCurrentStepGoal() == this.f.getCurrentStepGoal() && this.g.getCurrentCaloriesGoal() == this.f.getCurrentCaloriesGoal() && this.g.getCurrentActiveTimeGoal() == this.f.getCurrentActiveTimeGoal()) ? false : true;
        boolean z2 = this.i != this.h;
        boolean z3 = this.k != this.j;
        if (z || z2 || z3) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void o() {
        b(this.f);
        b(this.h);
        a(this.j);
    }

    @DexIgnore
    public final void p() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("XXX", "saveActivityGoal " + this.g);
        LiveData<yx5<ActivitySettings>> updateActivitySettings = this.p.updateActivitySettings(this.g);
        sk5 sk5 = this.o;
        if (sk5 != null) {
            updateActivitySettings.a((BaseFragment) sk5, new c(this, updateActivitySettings));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public final rm6 q() {
        return ik6.b(e(), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void r() {
        this.o.a(this);
    }

    @DexIgnore
    public void f() {
        this.o.z0();
        this.m = this.r.getLastGoalSetting();
        LiveData<yx5<Integer>> liveData = this.m;
        sk5 sk5 = this.o;
        if (sk5 != null) {
            liveData.a((BaseFragment) sk5, new f(this));
            this.l = this.q.getLastSleepGoal();
            LiveData<yx5<Integer>> liveData2 = this.l;
            sk5 sk52 = this.o;
            if (sk52 != null) {
                liveData2.a((BaseFragment) sk52, new g(this));
                this.e = this.p.getActivitySettings();
                LiveData<yx5<ActivitySettings>> liveData3 = this.e;
                sk5 sk53 = this.o;
                if (sk53 != null) {
                    liveData3.a((BaseFragment) sk53, new h(this));
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(t, "stop");
        LiveData<yx5<ActivitySettings>> liveData = this.e;
        sk5 sk5 = this.o;
        if (sk5 != null) {
            liveData.a((BaseFragment) sk5);
            this.l.a(this.o);
            this.m.a(this.o);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public void h() {
        if (n()) {
            this.o.E0();
        } else {
            this.o.V0();
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE i() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.n;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public final void a(ActivitySettings activitySettings) {
        String e2 = PortfolioApp.get.instance().e();
        if (!TextUtils.isEmpty(e2)) {
            PortfolioApp.get.instance().a(e2, activitySettings.getCurrentStepGoal());
        }
        this.o.V0();
    }

    @DexIgnore
    public final void b(ActivitySettings activitySettings) {
        if (!(activitySettings.getCurrentStepGoal() == this.f.getCurrentStepGoal() && activitySettings.getCurrentCaloriesGoal() == this.f.getCurrentCaloriesGoal() && activitySettings.getCurrentActiveTimeGoal() == this.f.getCurrentActiveTimeGoal())) {
            this.f = activitySettings;
            this.g = new ActivitySettings(activitySettings.getCurrentStepGoal(), activitySettings.getCurrentCaloriesGoal(), activitySettings.getCurrentActiveTimeGoal());
        }
        this.o.a(this.g);
    }

    @DexIgnore
    public final void a(int i2) {
        if (i2 != this.j) {
            this.j = i2;
            this.k = this.j;
        }
        this.o.k(this.k);
    }

    @DexIgnore
    public final void b(int i2) {
        if (i2 != this.h) {
            this.h = i2;
            this.i = this.h;
        }
        this.o.m(this.i);
    }

    @DexIgnore
    public void a(int i2, sh4 sh4) {
        wg6.b(sh4, "type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "setSettingValue value=" + i2 + " type=" + sh4);
        int i3 = xk5.a[sh4.ordinal()];
        if (i3 == 1) {
            this.g.setCurrentStepGoal(i2);
        } else if (i3 == 2) {
            this.g.setCurrentCaloriesGoal(i2);
        } else if (i3 == 3) {
            this.g.setCurrentActiveTimeGoal(i2);
        } else if (i3 == 4) {
            this.i = i2;
        } else if (i3 == 5) {
            this.k = i2;
        }
        this.o.a(i2, sh4);
    }
}
