package com.portfolio.platform.uirenew.home.details.activetime;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.bl4;
import com.fossil.cf;
import com.fossil.ch5;
import com.fossil.dh5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.nh6;
import com.fossil.p54;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.sh5;
import com.fossil.ut4;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.yk4;
import com.fossil.zg5;
import com.fossil.zh4;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveTimeDetailFragment extends BaseFragment implements dh5, View.OnClickListener {
    @DexIgnore
    public static /* final */ a y; // = new a((qg6) null);
    @DexIgnore
    public /* final */ Calendar f; // = Calendar.getInstance();
    @DexIgnore
    public sh5 g;
    @DexIgnore
    public Date h; // = new Date();
    @DexIgnore
    public ax5<p54> i;
    @DexIgnore
    public ch5 j;
    @DexIgnore
    public String o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ActiveTimeDetailFragment a(Date date) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            ActiveTimeDetailFragment activeTimeDetailFragment = new ActiveTimeDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            activeTimeDetailFragment.setArguments(bundle);
            return activeTimeDetailFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ cf b;

        @DexIgnore
        public b(ActiveTimeDetailFragment activeTimeDetailFragment, boolean z, cf cfVar, zh4 zh4) {
            this.a = z;
            this.b = cfVar;
        }

        @DexIgnore
        public boolean a(AppBarLayout appBarLayout) {
            wg6.b(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public ActiveTimeDetailFragment() {
        String b2 = ThemeManager.l.a().b("nonBrandSurface");
        this.r = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("backgroundDashboard");
        this.s = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("secondaryText");
        this.t = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("primaryText");
        this.u = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
        String b6 = ThemeManager.l.a().b("nonBrandDisableCalendarDay");
        this.v = Color.parseColor(b6 == null ? "#FFFFFF" : b6);
        String b7 = ThemeManager.l.a().b("nonBrandNonReachGoal");
        this.w = Color.parseColor(b7 == null ? "#FFFFFF" : b7);
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "ActiveTimeDetailFragment";
    }

    @DexIgnore
    public final void j1() {
        p54 a2;
        OverviewDayChart overviewDayChart;
        ax5<p54> ax5 = this.i;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayChart = a2.t) != null) {
            ch5 ch5 = this.j;
            if ((ch5 != null ? ch5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                overviewDayChart.a("dianaActiveMinutesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.a("hybridActiveMinutesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("ActiveTimeDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362542:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362543:
                    ch5 ch5 = this.j;
                    if (ch5 != null) {
                        ch5.k();
                        return;
                    }
                    return;
                case 2131362609:
                    ch5 ch52 = this.j;
                    if (ch52 != null) {
                        ch52.j();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        p54 a2;
        wg6.b(layoutInflater, "inflater");
        ActiveTimeDetailFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        p54 a3 = kb.a(layoutInflater, 2131558494, viewGroup, false, e1());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.h = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.h = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        wg6.a((Object) a3, "binding");
        a(a3);
        ch5 ch5 = this.j;
        if (ch5 != null) {
            ch5.a(this.h);
        }
        this.i = new ax5<>(this, a3);
        j1();
        ax5<p54> ax5 = this.i;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public void onDestroyView() {
        ch5 ch5 = this.j;
        if (ch5 != null) {
            ch5.i();
        }
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ActiveTimeDetailFragment.super.onPause();
        ch5 ch5 = this.j;
        if (ch5 != null) {
            ch5.g();
        }
    }

    @DexIgnore
    public void onResume() {
        ActiveTimeDetailFragment.super.onResume();
        j1();
        ch5 ch5 = this.j;
        if (ch5 != null) {
            ch5.b(this.h);
        }
        ch5 ch52 = this.j;
        if (ch52 != null) {
            ch52.f();
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        ch5 ch5 = this.j;
        if (ch5 != null) {
            ch5.a(bundle);
        }
        ActiveTimeDetailFragment.super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v2, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public final void a(p54 p54) {
        String str;
        p54.C.setOnClickListener(this);
        p54.D.setOnClickListener(this);
        p54.E.setOnClickListener(this);
        ch5 ch5 = this.j;
        if ((ch5 != null ? ch5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            str = ThemeManager.l.a().b("dianaActiveMinutesTab");
        } else {
            str = ThemeManager.l.a().b("hybridActiveMinutesTab");
        }
        this.o = str;
        this.p = ThemeManager.l.a().b("nonBrandActivityDetailBackground");
        this.q = ThemeManager.l.a().b("onDianaActiveMinutesTab");
        this.g = new sh5(sh5.c.ACTIVE_TIME, zh4.IMPERIAL, new WorkoutSessionDifference(), this.o);
        p54.G.setBackgroundColor(this.s);
        p54.r.setBackgroundColor(this.r);
        RecyclerView recyclerView = p54.J;
        wg6.a((Object) recyclerView, "it");
        recyclerView.setAdapter(this.g);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable c = w6.c(recyclerView.getContext(), 2131230877);
        if (c != null) {
            zg5 zg5 = new zg5(linearLayoutManager.Q(), false, false, 6, (qg6) null);
            wg6.a((Object) c, ResourceManager.DRAWABLE);
            zg5.a(c);
            recyclerView.addItemDecoration(zg5);
        }
    }

    @DexIgnore
    public void a(ch5 ch5) {
        wg6.b(ch5, "presenter");
        this.j = ch5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v9, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v10, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        p54 a2;
        wg6.b(date, HardwareLog.COLUMN_DATE);
        this.h = date;
        Calendar calendar = this.f;
        wg6.a((Object) calendar, "calendar");
        calendar.setTime(date);
        int i2 = this.f.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3 + " - calendar: " + this.f);
        ax5<p54> ax5 = this.i;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            a2.q.a(true, true);
            Object r1 = a2.w;
            wg6.a((Object) r1, "binding.ftvDayOfMonth");
            r1.setText(String.valueOf(this.f.get(5)));
            if (z) {
                Object r6 = a2.D;
                wg6.a((Object) r6, "binding.ivBackDate");
                r6.setVisibility(4);
            } else {
                Object r62 = a2.D;
                wg6.a((Object) r62, "binding.ivBackDate");
                r62.setVisibility(0);
            }
            if (z2 || z3) {
                Object r8 = a2.E;
                wg6.a((Object) r8, "binding.ivNextDate");
                r8.setVisibility(8);
                if (z2) {
                    Object r5 = a2.x;
                    wg6.a((Object) r5, "binding.ftvDayOfWeek");
                    r5.setText(jm4.a(getContext(), 2131886449));
                    return;
                }
                Object r52 = a2.x;
                wg6.a((Object) r52, "binding.ftvDayOfWeek");
                r52.setText(yk4.b.b(i2));
                return;
            }
            Object r7 = a2.E;
            wg6.a((Object) r7, "binding.ivNextDate");
            r7.setVisibility(0);
            Object r53 = a2.x;
            wg6.a((Object) r53, "binding.ftvDayOfWeek");
            r53.setText(yk4.b.b(i2));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v18, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v19, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v26, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v14, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v15, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v31, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v32, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v33, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v37, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v38, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v26, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v27, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v29, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v43, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v44, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v45, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v46, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v47, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v48, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v50, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v51, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v53, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v54, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v55, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v36, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v37, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v38, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v39, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v40, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v42, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r5v43, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r9v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(zh4 zh4, ActivitySummary activitySummary) {
        p54 a2;
        int i2;
        int i3;
        zh4 zh42 = zh4;
        ActivitySummary activitySummary2 = activitySummary;
        wg6.b(zh42, MFUser.DISTANCE_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeDetailFragment", "showDayDetail - distanceUnit=" + zh42 + ", activitySummary=" + activitySummary2);
        ax5<p54> ax5 = this.i;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            wg6.a((Object) a2, "binding");
            View d = a2.d();
            wg6.a((Object) d, "binding.root");
            Context context = d.getContext();
            if (activitySummary2 != null) {
                i2 = activitySummary.getActiveTime();
                i3 = activitySummary.getActiveTimeGoal();
            } else {
                i3 = 0;
                i2 = 0;
            }
            if (i2 > 0) {
                Object r9 = a2.v;
                wg6.a((Object) r9, "binding.ftvDailyValue");
                r9.setText(bl4.a.a(Integer.valueOf(i2)));
                Object r92 = a2.u;
                wg6.a((Object) r92, "binding.ftvDailyUnit");
                String a3 = jm4.a(context, 2131886400);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026s_DetailPage_Label__Mins)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    r92.setText(lowerCase);
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                Object r93 = a2.v;
                wg6.a((Object) r93, "binding.ftvDailyValue");
                r93.setText("");
                Object r94 = a2.u;
                wg6.a((Object) r94, "binding.ftvDailyUnit");
                String a4 = jm4.a(context, 2131886399);
                wg6.a((Object) a4, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                if (a4 != null) {
                    String upperCase = a4.toUpperCase();
                    wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    r94.setText(upperCase);
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            int i4 = i3 > 0 ? (i2 * 100) / i3 : -1;
            if (i2 >= i3 && i3 > 0) {
                a2.x.setTextColor(this.r);
                a2.w.setTextColor(this.r);
                a2.u.setTextColor(this.r);
                a2.v.setTextColor(this.r);
                a2.y.setTextColor(this.r);
                Object r4 = a2.E;
                wg6.a((Object) r4, "binding.ivNextDate");
                r4.setSelected(true);
                Object r42 = a2.D;
                wg6.a((Object) r42, "binding.ivBackDate");
                r42.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                wg6.a((Object) constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                Object r43 = a2.x;
                wg6.a((Object) r43, "binding.ftvDayOfWeek");
                r43.setSelected(true);
                Object r44 = a2.w;
                wg6.a((Object) r44, "binding.ftvDayOfMonth");
                r44.setSelected(true);
                View view = a2.F;
                wg6.a((Object) view, "binding.line");
                view.setSelected(true);
                Object r45 = a2.v;
                wg6.a((Object) r45, "binding.ftvDailyValue");
                r45.setSelected(true);
                Object r46 = a2.u;
                wg6.a((Object) r46, "binding.ftvDailyUnit");
                r46.setSelected(true);
                Object r47 = a2.y;
                wg6.a((Object) r47, "binding.ftvEst");
                r47.setSelected(true);
                String str = this.q;
                if (str != null) {
                    a2.x.setTextColor(Color.parseColor(str));
                    a2.w.setTextColor(Color.parseColor(str));
                    a2.v.setTextColor(Color.parseColor(str));
                    a2.u.setTextColor(Color.parseColor(str));
                    a2.y.setTextColor(Color.parseColor(str));
                    a2.F.setBackgroundColor(Color.parseColor(str));
                    a2.E.setColorFilter(Color.parseColor(str));
                    a2.D.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.o;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i2 > 0) {
                a2.w.setTextColor(this.u);
                a2.x.setTextColor(this.t);
                a2.u.setTextColor(this.w);
                a2.v.setTextColor(this.u);
                a2.y.setTextColor(this.u);
                a2.s.setBackgroundColor(Color.parseColor(this.p));
                View view2 = a2.F;
                wg6.a((Object) view2, "binding.line");
                view2.setSelected(false);
                Object r48 = a2.E;
                wg6.a((Object) r48, "binding.ivNextDate");
                r48.setSelected(false);
                Object r49 = a2.D;
                wg6.a((Object) r49, "binding.ivBackDate");
                r49.setSelected(false);
                int i5 = this.w;
                a2.F.setBackgroundColor(i5);
                a2.E.setColorFilter(i5);
                a2.D.setColorFilter(i5);
                String str3 = this.p;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.w.setTextColor(this.u);
                a2.x.setTextColor(this.t);
                a2.v.setTextColor(this.v);
                a2.u.setTextColor(this.v);
                Object r410 = a2.E;
                wg6.a((Object) r410, "binding.ivNextDate");
                r410.setSelected(false);
                Object r411 = a2.D;
                wg6.a((Object) r411, "binding.ivBackDate");
                r411.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                wg6.a((Object) constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                Object r412 = a2.x;
                wg6.a((Object) r412, "binding.ftvDayOfWeek");
                r412.setSelected(false);
                Object r413 = a2.w;
                wg6.a((Object) r413, "binding.ftvDayOfMonth");
                r413.setSelected(false);
                View view3 = a2.F;
                wg6.a((Object) view3, "binding.line");
                view3.setSelected(false);
                Object r414 = a2.v;
                wg6.a((Object) r414, "binding.ftvDailyValue");
                r414.setSelected(false);
                Object r415 = a2.u;
                wg6.a((Object) r415, "binding.ftvDailyUnit");
                r415.setSelected(false);
                Object r416 = a2.y;
                wg6.a((Object) r416, "binding.ftvEst");
                r416.setSelected(false);
                int i6 = this.w;
                a2.F.setBackgroundColor(i6);
                a2.E.setColorFilter(i6);
                a2.D.setColorFilter(i6);
                String str4 = this.p;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i4 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.H;
                wg6.a((Object) flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                Object r5 = a2.B;
                wg6.a((Object) r5, "binding.ftvProgressValue");
                r5.setText(jm4.a(context, 2131887067));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.H;
                wg6.a((Object) flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i4);
                Object r52 = a2.B;
                wg6.a((Object) r52, "binding.ftvProgressValue");
                r52.setText(i4 + "%");
            }
            Object r1 = a2.z;
            wg6.a((Object) r1, "binding.ftvGoalValue");
            nh6 nh6 = nh6.a;
            String a5 = jm4.a(context, 2131886404);
            wg6.a((Object) a5, "LanguageHelper.getString\u2026Page_Title__OfNumberMins)");
            Object[] objArr = {bl4.a.a(Integer.valueOf(i3))};
            String format = String.format(a5, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            r1.setText(format);
        }
    }

    @DexIgnore
    public void a(ut4 ut4, ArrayList<String> arrayList) {
        p54 a2;
        OverviewDayChart overviewDayChart;
        wg6.b(ut4, "baseModel");
        wg6.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeDetailFragment", "showDayDetailChart - baseModel=" + ut4);
        ax5<p54> ax5 = this.i;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayChart = a2.t) != null) {
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) yk4.b.a(), false, 2, (Object) null);
            }
            overviewDayChart.a(ut4);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(boolean z, zh4 zh4, cf<WorkoutSession> cfVar) {
        p54 a2;
        wg6.b(zh4, MFUser.DISTANCE_UNIT);
        wg6.b(cfVar, "workoutSessions");
        ax5<p54> ax5 = this.i;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.G;
                wg6.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                if (!cfVar.isEmpty()) {
                    Object r3 = a2.A;
                    wg6.a((Object) r3, "it.ftvNoWorkoutRecorded");
                    r3.setVisibility(8);
                    RecyclerView recyclerView = a2.J;
                    wg6.a((Object) recyclerView, "it.rvWorkout");
                    recyclerView.setVisibility(0);
                    sh5 sh5 = this.g;
                    if (sh5 != null) {
                        sh5.a(zh4, cfVar);
                    }
                } else {
                    Object r32 = a2.A;
                    wg6.a((Object) r32, "it.ftvNoWorkoutRecorded");
                    r32.setVisibility(0);
                    RecyclerView recyclerView2 = a2.J;
                    wg6.a((Object) recyclerView2, "it.rvWorkout");
                    recyclerView2.setVisibility(8);
                    sh5 sh52 = this.g;
                    if (sh52 != null) {
                        sh52.a(zh4, cfVar);
                    }
                }
            } else {
                LinearLayout linearLayout2 = a2.G;
                wg6.a((Object) linearLayout2, "it.llWorkout");
                linearLayout2.setVisibility(8);
            }
            AppBarLayout appBarLayout = a2.q;
            wg6.a((Object) appBarLayout, "it.appBarLayout");
            CoordinatorLayout.e layoutParams = appBarLayout.getLayoutParams();
            if (layoutParams != null) {
                CoordinatorLayout.e eVar = layoutParams;
                AppBarLayout.Behavior d = eVar.d();
                if (d == null) {
                    d = new AppBarLayout.Behavior();
                }
                d.setDragCallback(new b(this, z, cfVar, zh4));
                eVar.a(d);
                return;
            }
            throw new rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
        }
    }
}
