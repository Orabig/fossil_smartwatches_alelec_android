package com.portfolio.platform.uirenew.home;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.bu4;
import com.fossil.fb4;
import com.fossil.ij4;
import com.fossil.jj4;
import com.fossil.jm4;
import com.fossil.ju4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lc6;
import com.fossil.lh4;
import com.fossil.lx5;
import com.fossil.mj4;
import com.fossil.mj5;
import com.fossil.nh6;
import com.fossil.nj5;
import com.fossil.nw6;
import com.fossil.nz;
import com.fossil.oj5;
import com.fossil.pj5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rh6;
import com.fossil.tk4;
import com.fossil.v5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.xj4;
import com.fossil.xm4;
import com.fossil.yk4;
import com.fossil.zh4;
import com.fossil.zj4;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FossilCircleImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeProfileFragment extends BasePermissionFragment implements oj5, ju4.b, AlertDialogFragment.g, bu4 {
    @DexIgnore
    public static /* final */ a p; // = new a((qg6) null);
    @DexIgnore
    public nj5 g;
    @DexIgnore
    public ax5<fb4> h;
    @DexIgnore
    public mj4 i;
    @DexIgnore
    public ju4 j;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HomeProfileFragment a() {
            return new HomeProfileFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public b(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                AboutActivity.a aVar = AboutActivity.C;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public c(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ConnectedAppsActivity.a aVar = ConnectedAppsActivity.D;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public d(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ProfileOptInActivity.a aVar = ProfileOptInActivity.C;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public e(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ReplaceBatteryActivity.a aVar = ReplaceBatteryActivity.C;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public f(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ThemesActivity.a aVar = ThemesActivity.B;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public g(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.doCameraTask();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public h(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.v(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public i(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ProfileEditActivity.a aVar = ProfileEditActivity.B;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public j(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                wg6.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 4, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public k(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                wg6.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 4, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public l(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ProfileGoalEditActivity.a aVar = ProfileGoalEditActivity.D;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public m(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ProfileChangePasswordActivity.a aVar = ProfileChangePasswordActivity.C;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public n(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PreferredUnitActivity.a aVar = PreferredUnitActivity.C;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment a;

        @DexIgnore
        public o(HomeProfileFragment homeProfileFragment) {
            this.a = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                HelpActivity.a aVar = HelpActivity.C;
                wg6.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore
    @nw6(122)
    public final void doCameraTask() {
        if (xm4.a(xm4.d, getActivity(), "EDIT_AVATAR", false, false, false, 28, (Object) null)) {
            nj5 nj5 = this.g;
            if (nj5 != null) {
                nj5.j();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void K() {
        ju4 ju4 = this.j;
        if (ju4 != null) {
            ju4.c();
        }
    }

    @DexIgnore
    public void O() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.C;
            wg6.a((Object) activity, "it");
            aVar.b(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public void Q(boolean z) {
        if (z) {
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        kl4 g12 = g1();
        if (g12 != null) {
            g12.a("");
        }
    }

    @DexIgnore
    public void R(String str) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeProfileFragment", "user select " + str);
        WatchSettingActivity.a aVar = WatchSettingActivity.C;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            wg6.a((Object) activity, "activity!!");
            aVar.a(activity, str);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v5, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r8v6, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r8v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v8, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r8v9, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void c(ArrayList<pj5.b> arrayList) {
        wg6.b(arrayList, Constants.DEVICES);
        ju4 ju4 = this.j;
        if (ju4 != null) {
            ju4.a(arrayList);
        }
        ax5<fb4> ax5 = this.h;
        if (ax5 != null) {
            fb4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (arrayList.isEmpty()) {
                Object r8 = a2.W;
                wg6.a((Object) r8, "it.tvDevice");
                r8.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886828));
                Object r82 = a2.C;
                wg6.a((Object) r82, "it.cvPairFirstWatch");
                r82.setVisibility(0);
                Object r83 = a2.D;
                wg6.a((Object) r83, "it.fbtAddDevice");
                r83.setVisibility(8);
                return;
            }
            Object r84 = a2.W;
            wg6.a((Object) r84, "it.tvDevice");
            r84.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886929));
            Object r85 = a2.C;
            wg6.a((Object) r85, "it.cvPairFirstWatch");
            r85.setVisibility(8);
            Object r86 = a2.D;
            wg6.a((Object) r86, "it.fbtAddDevice");
            r86.setVisibility(0);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public String h1() {
        return "HomeProfileFragment";
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final void j1() {
        ax5<fb4> ax5 = this.h;
        if (ax5 != null) {
            fb4 a2 = ax5.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.A;
                wg6.a((Object) constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(0);
                v5 v5Var = new v5();
                v5Var.c(a2.z);
                v5Var.a(2131362010, 6, 0, 6);
                v5Var.a(2131362010, 7, 2131362005, 6);
                v5Var.a(2131362010, 3, 0, 3);
                v5Var.a(2131362010, 4, 2131362009, 3);
                v5Var.a(2131362005, 6, 2131362010, 7);
                v5Var.a(2131362005, 7, 0, 7);
                v5Var.a(2131362005, 3, 2131362010, 3);
                v5Var.a(2131362005, 4, 2131362010, 4);
                v5Var.a(2131362020, 6, 2131362010, 6);
                v5Var.a(2131362020, 7, 2131362010, 7);
                v5Var.a(2131362020, 3, 2131362010, 4);
                v5Var.a(2131362020, 4, 0, 4);
                v5Var.a(2131362009, 6, 2131362005, 6);
                v5Var.a(2131362009, 7, 2131362005, 7);
                v5Var.a(2131362009, 3, 2131362020, 3);
                v5Var.a(2131362009, 4, 2131362020, 4);
                v5Var.a(a2.z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void k1() {
        ax5<fb4> ax5 = this.h;
        if (ax5 != null) {
            fb4 a2 = ax5.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.A;
                wg6.a((Object) constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(8);
                v5 v5Var = new v5();
                v5Var.c(a2.z);
                v5Var.a(2131362010, 6, 0, 6);
                v5Var.a(2131362010, 7, 2131362020, 6);
                v5Var.a(2131362010, 3, 0, 3);
                v5Var.a(2131362010, 4, 2131362009, 3);
                v5Var.a(2131362020, 6, 2131362010, 7);
                v5Var.a(2131362020, 7, 0, 7);
                v5Var.a(2131362020, 3, 2131362010, 3);
                v5Var.a(2131362020, 4, 2131362010, 4);
                v5Var.a(2131362009, 6, 2131362010, 6);
                v5Var.a(2131362009, 7, 2131362010, 7);
                v5Var.a(2131362009, 3, 2131362010, 4);
                v5Var.a(2131362009, 4, 0, 4);
                v5Var.a(a2.z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        HomeProfileFragment.super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            nj5 nj5 = this.g;
            if (nj5 != null) {
                nj5.a(intent);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HomeProfileFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        fb4 a2 = kb.a(layoutInflater, 2131558569, viewGroup, false, e1());
        mj4 a3 = jj4.a((Fragment) this);
        wg6.a((Object) a3, "GlideApp.with(this)");
        this.i = a3;
        ArrayList arrayList = new ArrayList();
        mj4 mj4 = this.i;
        if (mj4 != null) {
            this.j = new ju4(arrayList, mj4, this, PortfolioApp.get.instance());
            this.h = new ax5<>(this, a2);
            ax5<fb4> ax5 = this.h;
            if (ax5 != null) {
                fb4 a4 = ax5.a();
                if (a4 != null) {
                    wg6.a((Object) a4, "mBinding.get()!!");
                    return a4.d();
                }
                wg6.a();
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mGlideRequests");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        HomeProfileFragment.super.onPause();
        nj5 nj5 = this.g;
        if (nj5 != null) {
            if (nj5 != null) {
                nj5.g();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        kl4 g1 = g1();
        if (g1 != null) {
            g1.a("");
        }
    }

    @DexIgnore
    public void onResume() {
        HomeProfileFragment.super.onResume();
        nj5 nj5 = this.g;
        if (nj5 != null) {
            if (nj5 != null) {
                nj5.f();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        kl4 g1 = g1();
        if (g1 != null) {
            g1.d();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v6, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r0v8, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r0v16, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v33, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v35, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        String b2;
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<fb4> ax5 = this.h;
        if (ax5 != null) {
            fb4 a2 = ax5.a();
            if (a2 != null) {
                String b3 = ThemeManager.l.a().b("onPrimaryButton");
                if (!TextUtils.isEmpty(b3)) {
                    int parseColor = Color.parseColor(b3);
                    Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231027);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        a2.D.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
                a2.L.setOnClickListener(new g(this));
                a2.Y.setOnClickListener(new h(this));
                a2.K.setOnClickListener(new i(this));
                a2.D.setOnClickListener(new j(this));
                a2.C.setOnClickListener(new k(this));
                a2.w.setOnClickListener(new l(this));
                a2.r.setOnClickListener(new m(this));
                a2.y.setOnClickListener(new n(this));
                a2.t.setOnClickListener(new o(this));
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnClickListener(new c(this));
                a2.u.setOnClickListener(new d(this));
                a2.v.setOnClickListener(new e(this));
                a2.x.setOnClickListener(new f(this));
                ConstraintLayout constraintLayout = a2.z;
                if (constraintLayout != null) {
                    String b4 = ThemeManager.l.a().b("nonBrandSurface");
                    if (!TextUtils.isEmpty(b4)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(b4));
                    }
                }
                ConstraintLayout constraintLayout2 = a2.B;
                if (!(constraintLayout2 == null || (b2 = ThemeManager.l.a().b("nonBrandSurface")) == null)) {
                    constraintLayout2.setBackgroundColor(Color.parseColor(b2));
                }
                nj5 nj5 = this.g;
                if (nj5 != null) {
                    if (nj5 == null) {
                        wg6.d("mPresenter");
                        throw null;
                    } else if (nj5.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                        String b5 = ThemeManager.l.a().b("dianaActiveMinutesTab");
                        String b6 = ThemeManager.l.a().b("dianaStepsTab");
                        String b7 = ThemeManager.l.a().b("dianaActiveCaloriesTab");
                        String b8 = ThemeManager.l.a().b("dianaSleepTab");
                        if (!TextUtils.isEmpty(b5)) {
                            a2.G.setColorFilter(Color.parseColor(b5));
                        }
                        if (!TextUtils.isEmpty(b8)) {
                            a2.H.setColorFilter(Color.parseColor(b8));
                        }
                        if (!TextUtils.isEmpty(b6)) {
                            a2.I.setColorFilter(Color.parseColor(b6));
                        }
                        if (!TextUtils.isEmpty(b7)) {
                            a2.J.setColorFilter(Color.parseColor(b7));
                        }
                        RecyclerView recyclerView = a2.N;
                        wg6.a((Object) recyclerView, "binding.rvDevices");
                        recyclerView.setAdapter(this.j);
                        RecyclerView recyclerView2 = a2.N;
                        wg6.a((Object) recyclerView2, "binding.rvDevices");
                        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                        RecyclerView recyclerView3 = a2.N;
                        wg6.a((Object) recyclerView3, "binding.rvDevices");
                        recyclerView3.setNestedScrollingEnabled(false);
                    }
                }
                String b9 = ThemeManager.l.a().b("hybridStepsTab");
                String b10 = ThemeManager.l.a().b("hybridActiveCaloriesTab");
                String b11 = ThemeManager.l.a().b("hybridSleepTab");
                if (!TextUtils.isEmpty(b11)) {
                    a2.H.setColorFilter(Color.parseColor(b11));
                }
                if (!TextUtils.isEmpty(b9)) {
                    a2.I.setColorFilter(Color.parseColor(b9));
                }
                if (!TextUtils.isEmpty(b10)) {
                    a2.J.setColorFilter(Color.parseColor(b10));
                }
                RecyclerView recyclerView4 = a2.N;
                wg6.a((Object) recyclerView4, "binding.rvDevices");
                recyclerView4.setAdapter(this.j);
                RecyclerView recyclerView22 = a2.N;
                wg6.a((Object) recyclerView22, "binding.rvDevices");
                recyclerView22.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView32 = a2.N;
                wg6.a((Object) recyclerView32, "binding.rvDevices");
                recyclerView32.setNestedScrollingEnabled(false);
            }
            W("profile_view");
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void z() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.E(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        if ((str.length() == 0) || getActivity() == null) {
            return;
        }
        if (str.hashCode() != -292748329 || !str.equals("CONFIRM_LOGOUT_ACCOUNT")) {
            super.a(str, i2, intent);
        } else if (i2 == 2131363190) {
            nj5 nj5 = this.g;
            if (nj5 != null) {
                nj5.i();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(nj5 nj5) {
        wg6.b(nj5, "presenter");
        this.g = nj5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v7, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v8, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r13v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r13v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r13v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r13v15, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r13v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v10, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v1, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r11v1, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(ActivityStatistic activityStatistic) {
        ax5<fb4> ax5 = this.h;
        if (ax5 != null) {
            fb4 a2 = ax5.a();
            if (a2 != null) {
                if (activityStatistic != null) {
                    ActivityStatistic.ActivityDailyBest stepsBestDay = activityStatistic.getStepsBestDay();
                    ActivityStatistic.ActivityDailyBest activeTimeBestDay = activityStatistic.getActiveTimeBestDay();
                    ActivityStatistic.CaloriesBestDay caloriesBestDay = activityStatistic.getCaloriesBestDay();
                    String a3 = jm4.a(getContext(), 2131887067);
                    if (stepsBestDay != null) {
                        Object r11 = a2.Q;
                        wg6.a((Object) r11, "it.tvAvgActivity");
                        r11.setText(tk4.a(stepsBestDay.getValue()));
                        Object r7 = a2.R;
                        wg6.a((Object) r7, "it.tvAvgActivityDate");
                        r7.setText(bk4.a(stepsBestDay.getDate()));
                    } else {
                        Object r9 = a2.Q;
                        wg6.a((Object) r9, "it.tvAvgActivity");
                        r9.setText(a3);
                        Object r72 = a2.R;
                        wg6.a((Object) r72, "it.tvAvgActivityDate");
                        r72.setText("");
                    }
                    if (activeTimeBestDay != null) {
                        Object r4 = a2.O;
                        wg6.a((Object) r4, "it.tvAvgActiveTime");
                        r4.setText(tk4.a(activeTimeBestDay.getValue()));
                        Object r42 = a2.P;
                        wg6.a((Object) r42, "it.tvAvgActiveTimeDate");
                        r42.setText(bk4.a(activeTimeBestDay.getDate()));
                    } else {
                        Object r43 = a2.O;
                        wg6.a((Object) r43, "it.tvAvgActiveTime");
                        r43.setText(a3);
                        Object r44 = a2.P;
                        wg6.a((Object) r44, "it.tvAvgActiveTimeDate");
                        r44.setText("");
                    }
                    if (caloriesBestDay != null) {
                        Object r1 = a2.S;
                        wg6.a((Object) r1, "it.tvAvgCalories");
                        r1.setText(tk4.a(rh6.a(caloriesBestDay.getValue())));
                        Object r0 = a2.T;
                        wg6.a((Object) r0, "it.tvAvgCaloriesDate");
                        r0.setText(bk4.a(caloriesBestDay.getDate()));
                    } else {
                        Object r13 = a2.S;
                        wg6.a((Object) r13, "it.tvAvgCalories");
                        r13.setText(a3);
                        Object r132 = a2.T;
                        wg6.a((Object) r132, "it.tvAvgCaloriesDate");
                        r132.setText("");
                    }
                } else {
                    String a4 = jm4.a(getContext(), 2131887067);
                    Object r12 = a2.Q;
                    wg6.a((Object) r12, "it.tvAvgActivity");
                    r12.setText(a4);
                    Object r14 = a2.S;
                    wg6.a((Object) r14, "it.tvAvgCalories");
                    r14.setText(a4);
                    Object r15 = a2.O;
                    wg6.a((Object) r15, "it.tvAvgActiveTime");
                    r15.setText(a4);
                    Object r133 = a2.R;
                    wg6.a((Object) r133, "it.tvAvgActivityDate");
                    r133.setText("");
                    Object r134 = a2.T;
                    wg6.a((Object) r134, "it.tvAvgCaloriesDate");
                    r134.setText("");
                    Object r135 = a2.P;
                    wg6.a((Object) r135, "it.tvAvgActiveTimeDate");
                    r135.setText("");
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeProfileFragment", "active serial =" + PortfolioApp.get.instance().e());
            if (DeviceIdentityUtils.isDianaDevice(PortfolioApp.get.instance().e())) {
                j1();
            } else {
                k1();
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(SleepStatistic sleepStatistic) {
        SleepStatistic.SleepDailyBest sleepTimeBestDay;
        ax5<fb4> ax5 = this.h;
        if (ax5 != null) {
            fb4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (sleepStatistic == null || (sleepTimeBestDay = sleepStatistic.getSleepTimeBestDay()) == null) {
                Object r6 = a2.U;
                wg6.a((Object) r6, "it.tvAvgSleep");
                r6.setText(jm4.a(getContext(), 2131887068));
                Object r62 = a2.V;
                wg6.a((Object) r62, "it.tvAvgSleepDate");
                r62.setText("");
                return;
            }
            Object r3 = a2.U;
            wg6.a((Object) r3, "it.tvAvgSleep");
            r3.setText(yk4.b.d(sleepTimeBestDay.getValue()));
            Object r0 = a2.V;
            wg6.a((Object) r0, "it.tvAvgSleepDate");
            r0.setText(bk4.a(sleepTimeBestDay.getDate()));
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v12, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v22, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v29, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v34, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v35, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v23, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v50, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v56, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    /* JADX WARNING: type inference failed for: r3v41, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.FossilCircleImageView] */
    /* JADX WARNING: type inference failed for: r8v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(MFUser mFUser) {
        String str;
        String str2;
        wg6.b(mFUser, "user");
        FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "updateUser");
        if (isActive()) {
            ax5<fb4> ax5 = this.h;
            if (ax5 != null) {
                fb4 a2 = ax5.a();
                if (a2 != null) {
                    Object r5 = a2.a0;
                    wg6.a((Object) r5, "it.tvUserName");
                    r5.setText(mFUser.getFirstName() + " " + mFUser.getLastName());
                    if (!TextUtils.isEmpty(mFUser.getRegisterDate())) {
                        Object h2 = bk4.h(bk4.b(mFUser.getRegisterDate()).toDate());
                        Object r8 = a2.Z;
                        wg6.a((Object) r8, "it.tvMemberSince");
                        nh6 nh6 = nh6.a;
                        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886927);
                        wg6.a((Object) a3, "LanguageHelper.getString\u2026_Text__JoinedInMonthYear)");
                        Object[] objArr = {h2};
                        String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) format, "java.lang.String.format(format, *args)");
                        r8.setText(format);
                    }
                    String profilePicture = mFUser.getProfilePicture();
                    String str3 = mFUser.getFirstName() + " " + mFUser.getLastName();
                    if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
                        mj4 mj4 = this.i;
                        if (mj4 != null) {
                            mj4.a((Object) new ij4(mFUser.getProfilePicture(), str3)).a(new nz().a(new xj4())).a(a2.L);
                            FossilCircleImageView fossilCircleImageView = a2.L;
                            wg6.a((Object) fossilCircleImageView, "it.ivUserAvatar");
                            Context context = getContext();
                            if (context != null) {
                                fossilCircleImageView.setBorderColor(w6.a(context, 2131099837));
                                FossilCircleImageView fossilCircleImageView2 = a2.L;
                                wg6.a((Object) fossilCircleImageView2, "it.ivUserAvatar");
                                fossilCircleImageView2.setBorderWidth(3);
                                Object r3 = a2.L;
                                wg6.a((Object) r3, "it.ivUserAvatar");
                                Context context2 = getContext();
                                if (context2 != null) {
                                    r3.setBackground(w6.c(context2, 2131231300));
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.d("mGlideRequests");
                            throw null;
                        }
                    } else {
                        FossilCircleImageView fossilCircleImageView3 = a2.L;
                        mj4 mj42 = this.i;
                        if (mj42 != null) {
                            fossilCircleImageView3.a(mj42, profilePicture, str3);
                            FossilCircleImageView fossilCircleImageView4 = a2.L;
                            wg6.a((Object) fossilCircleImageView4, "it.ivUserAvatar");
                            Context context3 = getContext();
                            if (context3 != null) {
                                fossilCircleImageView4.setBorderColor(w6.a(context3, R.color.transparent));
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.d("mGlideRequests");
                            throw null;
                        }
                    }
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "isUseDefaultBiometric = " + mFUser.isUseDefaultBiometric());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "weightInGrams = " + mFUser.getWeightInGrams());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "heightInCentimeters = " + mFUser.getHeightInCentimeters());
                    Object string = PortfolioApp.get.instance().getString(2131887067);
                    wg6.a(string, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    if (mFUser.getWeightUnit() == zh4.IMPERIAL) {
                        if (mFUser.getWeightInGrams() == 0 || mFUser.isUseDefaultBiometric()) {
                            nh6 nh62 = nh6.a;
                            Object[] objArr2 = new Object[2];
                            objArr2[0] = string;
                            String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886713);
                            wg6.a((Object) a4, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (a4 != null) {
                                String lowerCase = a4.toLowerCase();
                                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                                objArr2[1] = lowerCase;
                                str = String.format("%s %s", Arrays.copyOf(objArr2, objArr2.length));
                                wg6.a((Object) str, "java.lang.String.format(format, *args)");
                            } else {
                                throw new rc6("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            nh6 nh63 = nh6.a;
                            Object[] objArr3 = new Object[2];
                            objArr3[0] = tk4.a(zj4.f((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                            String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886713);
                            wg6.a((Object) a5, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (a5 != null) {
                                String lowerCase2 = a5.toLowerCase();
                                wg6.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                                objArr3[1] = lowerCase2;
                                str = String.format("%s %s", Arrays.copyOf(objArr3, objArr3.length));
                                wg6.a((Object) str, "java.lang.String.format(format, *args)");
                            } else {
                                throw new rc6("null cannot be cast to non-null type java.lang.String");
                            }
                        }
                    } else if (mFUser.getWeightInGrams() == 0 || mFUser.isUseDefaultBiometric()) {
                        nh6 nh64 = nh6.a;
                        Object[] objArr4 = new Object[2];
                        objArr4[0] = string;
                        String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886712);
                        wg6.a((Object) a6, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (a6 != null) {
                            String lowerCase3 = a6.toLowerCase();
                            wg6.a((Object) lowerCase3, "(this as java.lang.String).toLowerCase()");
                            objArr4[1] = lowerCase3;
                            str = String.format("%s %s", Arrays.copyOf(objArr4, objArr4.length));
                            wg6.a((Object) str, "java.lang.String.format(format, *args)");
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        nh6 nh65 = nh6.a;
                        Object[] objArr5 = new Object[2];
                        objArr5[0] = tk4.a(zj4.e((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                        String a7 = jm4.a((Context) PortfolioApp.get.instance(), 2131886712);
                        wg6.a((Object) a7, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (a7 != null) {
                            String lowerCase4 = a7.toLowerCase();
                            wg6.a((Object) lowerCase4, "(this as java.lang.String).toLowerCase()");
                            objArr5[1] = lowerCase4;
                            str = String.format("%s %s", Arrays.copyOf(objArr5, objArr5.length));
                            wg6.a((Object) str, "java.lang.String.format(format, *args)");
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    Object string2 = PortfolioApp.get.instance().getString(2131887067);
                    wg6.a(string2, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    if (mFUser.getHeightUnit() == zh4.IMPERIAL) {
                        if (mFUser.getHeightInCentimeters() == 0 || mFUser.isUseDefaultBiometric()) {
                            nh6 nh66 = nh6.a;
                            String a8 = jm4.a((Context) PortfolioApp.get.instance(), 2131887319);
                            wg6.a((Object) a8, "LanguageHelper.getString\u2026_height_single_character)");
                            Object[] objArr6 = {string2};
                            str2 = String.format(a8, Arrays.copyOf(objArr6, objArr6.length));
                            wg6.a((Object) str2, "java.lang.String.format(format, *args)");
                        } else {
                            lc6<Integer, Integer> b2 = zj4.b((float) mFUser.getHeightInCentimeters());
                            nh6 nh67 = nh6.a;
                            String a9 = jm4.a((Context) PortfolioApp.get.instance(), 2131887318);
                            wg6.a((Object) a9, "LanguageHelper.getString\u2026_height_double_character)");
                            Object[] objArr7 = {String.valueOf(b2.getFirst().intValue()), String.valueOf(b2.getSecond().intValue())};
                            str2 = String.format(a9, Arrays.copyOf(objArr7, objArr7.length));
                            wg6.a((Object) str2, "java.lang.String.format(format, *args)");
                        }
                    } else if (mFUser.getHeightInCentimeters() == 0 || mFUser.isUseDefaultBiometric()) {
                        nh6 nh68 = nh6.a;
                        Object[] objArr8 = new Object[2];
                        objArr8[0] = string2;
                        String a10 = jm4.a((Context) PortfolioApp.get.instance(), 2131886710);
                        wg6.a((Object) a10, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (a10 != null) {
                            String lowerCase5 = a10.toLowerCase();
                            wg6.a((Object) lowerCase5, "(this as java.lang.String).toLowerCase()");
                            objArr8[1] = lowerCase5;
                            str2 = String.format("%s %s", Arrays.copyOf(objArr8, objArr8.length));
                            wg6.a((Object) str2, "java.lang.String.format(format, *args)");
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        nh6 nh69 = nh6.a;
                        Object[] objArr9 = new Object[2];
                        objArr9[0] = String.valueOf(mFUser.getHeightInCentimeters());
                        String a11 = jm4.a((Context) PortfolioApp.get.instance(), 2131886710);
                        wg6.a((Object) a11, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (a11 != null) {
                            String lowerCase6 = a11.toLowerCase();
                            wg6.a((Object) lowerCase6, "(this as java.lang.String).toLowerCase()");
                            objArr9[1] = lowerCase6;
                            str2 = String.format("%s %s", Arrays.copyOf(objArr9, objArr9.length));
                            wg6.a((Object) str2, "java.lang.String.format(format, *args)");
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    String str4 = str + ", " + str2;
                    Object r2 = a2.X;
                    wg6.a((Object) r2, "it.tvHeightWeight");
                    r2.setText(str4);
                    if (mFUser.getAuthType() == lh4.EMAIL) {
                        RelativeLayout relativeLayout = a2.r;
                        wg6.a((Object) relativeLayout, "it.btChangePassword");
                        relativeLayout.setVisibility(0);
                        View view = a2.b0;
                        wg6.a((Object) view, "it.vChangePasswordSeparatorLine");
                        view.setVisibility(0);
                        return;
                    }
                    RelativeLayout relativeLayout2 = a2.r;
                    wg6.a((Object) relativeLayout2, "it.btChangePassword");
                    relativeLayout2.setVisibility(8);
                    View view2 = a2.b0;
                    wg6.a((Object) view2, "it.vChangePasswordSeparatorLine");
                    view2.setVisibility(8);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(int i2, String str) {
        wg6.b(str, "message");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void a(mj5 mj5) {
        wg6.b(mj5, "activityDailyBest");
        ax5<fb4> ax5 = this.h;
        if (ax5 != null) {
            fb4 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.Q;
                wg6.a((Object) r1, "it.tvAvgActivity");
                r1.setText(tk4.a((int) mj5.b()));
                Object r0 = a2.R;
                wg6.a((Object) r0, "it.tvAvgActivityDate");
                r0.setText(bk4.a(mj5.a()));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r12v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r12v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r12v3, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r12v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r12v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r12v6, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void a(boolean z, boolean z2) {
        ax5<fb4> ax5 = this.h;
        if (ax5 != null) {
            fb4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                if (z2) {
                    Object r12 = a2.F;
                    wg6.a((Object) r12, "it.ftvTitleLowBattery");
                    nh6 nh6 = nh6.a;
                    String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886602);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                    Object[] objArr = {"10%"};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    r12.setText(format);
                    Object r122 = a2.E;
                    wg6.a((Object) r122, "it.ftvDescriptionLowBattery");
                    r122.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886601));
                    Object r123 = a2.v;
                    wg6.a((Object) r123, "it.btReplaceBattery");
                    r123.setVisibility(8);
                } else {
                    Object r124 = a2.F;
                    wg6.a((Object) r124, "it.ftvTitleLowBattery");
                    nh6 nh62 = nh6.a;
                    String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886612);
                    wg6.a((Object) a4, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                    Object[] objArr2 = {"25%"};
                    String format2 = String.format(a4, Arrays.copyOf(objArr2, objArr2.length));
                    wg6.a((Object) format2, "java.lang.String.format(format, *args)");
                    r124.setText(format2);
                    Object r125 = a2.E;
                    wg6.a((Object) r125, "it.ftvDescriptionLowBattery");
                    r125.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886611));
                    Object r126 = a2.v;
                    wg6.a((Object) r126, "it.btReplaceBattery");
                    r126.setVisibility(0);
                }
                ConstraintLayout constraintLayout = a2.B;
                wg6.a((Object) constraintLayout, "it.clLowBattery");
                constraintLayout.setVisibility(0);
                return;
            }
            ConstraintLayout constraintLayout2 = a2.B;
            wg6.a((Object) constraintLayout2, "it.clLowBattery");
            constraintLayout2.setVisibility(8);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
