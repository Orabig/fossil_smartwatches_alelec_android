package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.Fragment;
import com.fossil.ca5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchMicroAppActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public SearchMicroAppPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, String str2, String str3) {
            wg6.b(fragment, "fragment");
            wg6.b(str, "watchAppTop");
            wg6.b(str2, "watchAppMiddle");
            wg6.b(str3, "watchAppBottom");
            Intent intent = new Intent(fragment.getContext(), SearchMicroAppActivity.class);
            intent.putExtra("top", str);
            intent.putExtra("middle", str2);
            intent.putExtra("bottom", str3);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 102, ActivityOptions.makeSceneTransitionAnimation(fragment.getActivity(), new Pair[0]).toBundle());
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r10v0, types: [com.portfolio.platform.ui.BaseActivity, com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        SearchMicroAppFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = SearchMicroAppFragment.o.b();
            a((Fragment) b, SearchMicroAppFragment.o.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new ca5(b)).a(this);
            Intent intent = getIntent();
            wg6.a((Object) intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                SearchMicroAppPresenter searchMicroAppPresenter = this.B;
                if (searchMicroAppPresenter != null) {
                    String string = extras.getString("top");
                    if (string == null) {
                        string = "empty";
                    }
                    String string2 = extras.getString("middle");
                    if (string2 == null) {
                        string2 = "empty";
                    }
                    String string3 = extras.getString("bottom");
                    if (string3 == null) {
                        string3 = "empty";
                    }
                    searchMicroAppPresenter.a(string, string2, string3);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                SearchMicroAppPresenter searchMicroAppPresenter2 = this.B;
                if (searchMicroAppPresenter2 != null) {
                    String string4 = bundle.getString("top");
                    if (string4 == null) {
                        string4 = "empty";
                    }
                    String string5 = bundle.getString("middle");
                    if (string5 == null) {
                        string5 = "empty";
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = "empty";
                    }
                    searchMicroAppPresenter2.a(string4, string5, string6);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppContract.View");
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        SearchMicroAppPresenter searchMicroAppPresenter = this.B;
        if (searchMicroAppPresenter != null) {
            searchMicroAppPresenter.a(bundle);
            if (bundle != null) {
                SearchMicroAppActivity.super.onSaveInstanceState(bundle);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }
}
