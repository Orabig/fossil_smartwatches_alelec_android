package com.portfolio.platform.uirenew.home.profile.battery;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.dk5;
import com.fossil.dy5;
import com.fossil.ek5;
import com.fossil.jk3;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.rd4;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReplaceBatteryFragment extends BaseFragment implements ek5 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a((qg6) null);
    @DexIgnore
    public ax5<rd4> f;
    @DexIgnore
    public HashMap g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ReplaceBatteryFragment.h;
        }

        @DexIgnore
        public final ReplaceBatteryFragment b() {
            return new ReplaceBatteryFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ReplaceBatteryFragment a;

        @DexIgnore
        public b(ReplaceBatteryFragment replaceBatteryFragment) {
            this.a = replaceBatteryFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = dy5.a(dy5.c.FEATURES, dy5.b.SHOP_BATTERY);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = ReplaceBatteryFragment.i.a();
            local.d(a3, "Purchase Battery URL = " + a2);
            ReplaceBatteryFragment replaceBatteryFragment = this.a;
            wg6.a((Object) a2, "url");
            replaceBatteryFragment.Z(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ReplaceBatteryFragment a;

        @DexIgnore
        public c(ReplaceBatteryFragment replaceBatteryFragment) {
            this.a = replaceBatteryFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = dy5.a(dy5.c.FEATURES, dy5.b.LOW_BATTERY);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = ReplaceBatteryFragment.i.a();
            local.d(a3, "Need Help URL = " + a2);
            ReplaceBatteryFragment replaceBatteryFragment = this.a;
            wg6.a((Object) a2, "url");
            replaceBatteryFragment.Z(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ReplaceBatteryFragment a;

        @DexIgnore
        public d(ReplaceBatteryFragment replaceBatteryFragment) {
            this.a = replaceBatteryFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    /*
    static {
        String simpleName = ReplaceBatteryFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "ReplaceBatteryFragment::class.java.simpleName!!");
            h = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public final void Z(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), h);
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        rd4 a2 = kb.a(LayoutInflater.from(getContext()), 2131558605, (ViewGroup) null, false, e1());
        this.f = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<rd4> ax5 = this.f;
        if (ax5 != null) {
            rd4 a2 = ax5.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(dk5 dk5) {
        wg6.b(dk5, "presenter");
        jk3.a(dk5);
        wg6.a((Object) dk5, "Preconditions.checkNotNull(presenter)");
        dk5 dk52 = dk5;
    }
}
