package com.portfolio.platform.uirenew.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.bb4;
import com.fossil.bu4;
import com.fossil.g45;
import com.fossil.j45;
import com.fossil.jm4;
import com.fossil.k45;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.m35;
import com.fossil.nh6;
import com.fossil.oj3;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.u8;
import com.fossil.wg6;
import com.fossil.xj6;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.RenamePresetDialogFragment;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeDianaCustomizeFragment extends BasePermissionFragment implements k45, View.OnClickListener, g45.d, AlertDialogFragment.g, bu4 {
    @DexIgnore
    public static /* final */ a v; // = new a((qg6) null);
    @DexIgnore
    public j45 g;
    @DexIgnore
    public ConstraintLayout h;
    @DexIgnore
    public ViewPager2 i;
    @DexIgnore
    public int j;
    @DexIgnore
    public g45 o;
    @DexIgnore
    public ax5<bb4> p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HomeDianaCustomizeFragment a() {
            return new HomeDianaCustomizeFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment a;

        @DexIgnore
        public b(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.a = homeDianaCustomizeFragment;
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
            TabLayout tabLayout;
            TabLayout.g b;
            Drawable b2;
            TabLayout tabLayout2;
            TabLayout.g b3;
            Drawable b4;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "onPageScrolled " + i + " mCurrentPosition " + this.a.j1());
            HomeDianaCustomizeFragment.super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.a.r)) {
                int parseColor = Color.parseColor(this.a.r);
                bb4 bb4 = (bb4) HomeDianaCustomizeFragment.b(this.a).a();
                if (!(bb4 == null || (tabLayout2 = bb4.w) == null || (b3 = tabLayout2.b(i)) == null || (b4 = b3.b()) == null)) {
                    b4.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.q) && this.a.j1() != i) {
                int parseColor2 = Color.parseColor(this.a.q);
                bb4 bb42 = (bb4) HomeDianaCustomizeFragment.b(this.a).a();
                if (!(bb42 == null || (tabLayout = bb42.w) == null || (b = tabLayout.b(this.a.j1())) == null || (b2 = b.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.a.r(i);
            this.a.k1().a(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RenamePresetDialogFragment.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public c(HomeDianaCustomizeFragment homeDianaCustomizeFragment, String str) {
            this.a = homeDianaCustomizeFragment;
            this.b = str;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "presetName");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
            if (!TextUtils.isEmpty(str)) {
                this.a.k1().a(str, this.b);
            }
        }

        @DexIgnore
        public void onCancel() {
            FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "showRenamePresetDialog - onCancel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements oj3.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment a;

        @DexIgnore
        public d(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.a = homeDianaCustomizeFragment;
        }

        @DexIgnore
        public final void a(TabLayout.g gVar, int i) {
            wg6.b(gVar, "tab");
            int itemCount = this.a.getItemCount();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "tabLayoutConfig dataSize=" + itemCount);
            if (!TextUtils.isEmpty(this.a.q)) {
                int parseColor = Color.parseColor(this.a.q);
                if (i == 0) {
                    gVar.b(2131230975);
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor);
                    }
                } else if (i == itemCount - 1) {
                    gVar.b(2131230819);
                    Drawable b2 = gVar.b();
                    if (b2 != null) {
                        b2.setTint(parseColor);
                    }
                } else {
                    gVar.b(2131231004);
                    Drawable b3 = gVar.b();
                    if (b3 != null) {
                        b3.setTint(parseColor);
                    }
                }
                if (!TextUtils.isEmpty(this.a.r) && i == this.a.j1()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeDianaCustomizeFragment", "tabLayoutConfig primary mCurrentPosition " + this.a.j1());
                    Drawable b4 = gVar.b();
                    if (b4 != null) {
                        b4.setTint(Color.parseColor(this.a.r));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ ax5 b(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
        ax5<bb4> ax5 = homeDianaCustomizeFragment.p;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void G0() {
        j45 j45 = this.g;
        if (j45 != null) {
            j45.a(true);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void H0() {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onAddPresetClick");
        j45 j45 = this.g;
        if (j45 != null) {
            j45.h();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void Q(boolean z) {
        if (z) {
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        kl4 g12 = g1();
        if (g12 != null) {
            g12.a("");
        }
    }

    @DexIgnore
    public void d(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showDeleteSuccessfully - position=" + i2);
        g45 g45 = this.o;
        if (g45 == null) {
            wg6.d("mAdapterDiana");
            throw null;
        } else if (g45.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                wg6.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e(int i2) {
        if (isActive()) {
            g45 g45 = this.o;
            if (g45 == null) {
                wg6.d("mAdapterDiana");
                throw null;
            } else if (g45.getItemCount() > i2) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(i2);
                } else {
                    wg6.d("rvCustomize");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void f(int i2) {
        this.t = true;
        g45 g45 = this.o;
        if (g45 == null) {
            wg6.d("mAdapterDiana");
            throw null;
        } else if (g45.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                wg6.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public int getItemCount() {
        ViewPager2 viewPager2 = this.i;
        if (viewPager2 != null) {
            RecyclerView.g adapter = viewPager2.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        wg6.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return "HomeDianaCustomizeFragment";
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public void j() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            wg6.a((Object) activity, "it");
            TroubleshootingActivity.a.a(aVar, activity, PortfolioApp.get.instance().e(), false, 4, (Object) null);
        }
    }

    @DexIgnore
    public final int j1() {
        return this.j;
    }

    @DexIgnore
    public final j45 k1() {
        j45 j45 = this.g;
        if (j45 != null) {
            return j45;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void l1() {
        ax5<bb4> ax5 = this.p;
        if (ax5 != null) {
            bb4 a2 = ax5.a();
            TabLayout tabLayout = a2 != null ? a2.w : null;
            if (tabLayout != null) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    new oj3(tabLayout, viewPager2, new d(this)).a();
                } else {
                    wg6.d("rvCustomize");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void m() {
        String string = getString(2131886580);
        wg6.a((Object) string, "getString(R.string.Desig\u2026on_Text__ApplyingToWatch)");
        X(string);
    }

    @DexIgnore
    public void n() {
        a();
    }

    @DexIgnore
    public void onClick(View view) {
        FragmentActivity activity;
        wg6.b(view, "v");
        if (view.getId() == 2131362404 && (activity = getActivity()) != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
            wg6.a((Object) activity, "it");
            PairingInstructionsActivity.a.a(aVar, activity, false, false, 6, (Object) null);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HomeDianaCustomizeFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onCreateView");
        bb4 a2 = kb.a(layoutInflater, 2131558567, viewGroup, false, e1());
        wg6.a((Object) a2, "binding");
        a(a2);
        this.p = new ax5<>(this, a2);
        ax5<bb4> ax5 = this.p;
        if (ax5 != null) {
            bb4 a3 = ax5.a();
            if (a3 != null) {
                wg6.a((Object) a3, "mBinding.get()!!");
                return a3.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        HomeDianaCustomizeFragment.super.onPause();
        j45 j45 = this.g;
        if (j45 != null) {
            if (j45 != null) {
                j45.g();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        kl4 g1 = g1();
        if (g1 != null) {
            g1.a("");
        }
    }

    @DexIgnore
    public void onResume() {
        HomeDianaCustomizeFragment.super.onResume();
        j45 j45 = this.g;
        if (j45 != null) {
            if (j45 != null) {
                j45.f();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        kl4 g1 = g1();
        if (g1 != null) {
            g1.d();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onViewCreated");
        l1();
        W("customize_view");
    }

    @DexIgnore
    public final void r(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public void v() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showCreateNewSuccessfully mCurrentPosition " + this.j);
    }

    @DexIgnore
    public void b(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2, String str, int i2) {
        List<? extends u8<View, String>> list3 = list;
        String str2 = str;
        wg6.b(list, "views");
        List<? extends u8<CustomizeWidget, String>> list4 = list2;
        wg6.b(list2, "customizeWidgetViews");
        wg6.b(str2, "complicationPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetComplicationClick preset=");
        sb.append(m35 != null ? m35.a() : null);
        sb.append(" complicationPos=");
        sb.append(str2);
        sb.append(" position=");
        sb.append(i2);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        if (m35 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                String c2 = m35.c();
                ArrayList arrayList = new ArrayList(list);
                j45 j45 = this.g;
                if (j45 != null) {
                    aVar.a(activity, c2, arrayList, list2, j45.i(), 1, str, "top");
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v12, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public final void a(bb4 bb4) {
        this.q = ThemeManager.l.a().b("disabledButton");
        this.r = ThemeManager.l.a().b("primaryText");
        this.s = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
        this.o = new g45(new ArrayList(), (DianaComplicationRingStyle) null, this);
        ConstraintLayout constraintLayout = bb4.q;
        wg6.a((Object) constraintLayout, "binding.clNoDevice");
        this.h = constraintLayout;
        bb4.t.setImageResource(2131230977);
        bb4.r.setOnClickListener(this);
        ViewPager2 viewPager2 = bb4.v;
        wg6.a((Object) viewPager2, "binding.rvPreset");
        this.i = viewPager2;
        ViewPager2 viewPager22 = this.i;
        if (viewPager22 != null) {
            if (viewPager22.getChildAt(0) != null) {
                ViewPager2 viewPager23 = this.i;
                if (viewPager23 != null) {
                    RecyclerView childAt = viewPager23.getChildAt(0);
                    if (childAt != null) {
                        childAt.setOverScrollMode(2);
                    } else {
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                } else {
                    wg6.d("rvCustomize");
                    throw null;
                }
            }
            ViewPager2 viewPager24 = this.i;
            if (viewPager24 != null) {
                g45 g45 = this.o;
                if (g45 != null) {
                    viewPager24.setAdapter(g45);
                    if (!TextUtils.isEmpty(this.s)) {
                        TabLayout tabLayout = bb4.w;
                        wg6.a((Object) tabLayout, "binding.tab");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.s)));
                    }
                    ViewPager2 viewPager25 = this.i;
                    if (viewPager25 != null) {
                        viewPager25.a(new b(this));
                        ViewPager2 viewPager26 = this.i;
                        if (viewPager26 != null) {
                            viewPager26.setCurrentItem(this.j);
                            bb4.s.setOnClickListener(this);
                            return;
                        }
                        wg6.d("rvCustomize");
                        throw null;
                    }
                    wg6.d("rvCustomize");
                    throw null;
                }
                wg6.d("mAdapterDiana");
                throw null;
            }
            wg6.d("rvCustomize");
            throw null;
        }
        wg6.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    public void b(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2) {
        wg6.b(list, "views");
        wg6.b(list2, "customizeWidgetViews");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onEditThemeClick preset=");
        sb.append(m35 != null ? m35.a() : null);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        j45 j45 = this.g;
        if (j45 != null) {
            j45.a(m35, list, list2);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void b(String str, String str2) {
        wg6.b(str, "presetName");
        wg6.b(str2, "presetId");
        if (getChildFragmentManager().b("RenamePresetDialogFragment") == null) {
            RenamePresetDialogFragment a2 = RenamePresetDialogFragment.g.a(str, new c(this, str2));
            if (isActive()) {
                a2.show(getChildFragmentManager(), "RenamePresetDialogFragment");
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void b(boolean z, boolean z2) {
        if (z) {
            ax5<bb4> ax5 = this.p;
            if (ax5 != null) {
                bb4 a2 = ax5.a();
                if (a2 != null) {
                    Object r6 = a2.x;
                    wg6.a((Object) r6, "tvTapIconToCustomize");
                    r6.setText(jm4.a(getContext(), 2131886388));
                    Object r5 = a2.x;
                    wg6.a((Object) r5, "tvTapIconToCustomize");
                    r5.setVisibility(0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        ax5<bb4> ax52 = this.p;
        if (ax52 != null) {
            bb4 a3 = ax52.a();
            if (a3 == null) {
                return;
            }
            if (!z2) {
                Object r62 = a3.x;
                wg6.a((Object) r62, "tvTapIconToCustomize");
                r62.setText(jm4.a(getContext(), 2131886375));
                Object r52 = a3.x;
                wg6.a((Object) r52, "tvTapIconToCustomize");
                r52.setVisibility(0);
                return;
            }
            Object r53 = a3.x;
            wg6.a((Object) r53, "tvTapIconToCustomize");
            r53.setVisibility(4);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void b(boolean z, String str, String str2, String str3) {
        wg6.b(str, "currentPresetName");
        wg6.b(str2, "nextPresetName");
        wg6.b(str3, "nextPresetId");
        isActive();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String string = activity.getString(2131886378);
            wg6.a((Object) string, "activity!!.getString(R.s\u2026ingAPresetIsPermanentAnd)");
            if (z) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    String string2 = activity2.getString(2131886379);
                    wg6.a((Object) string2, "activity!!.getString(R.s\u2026ingAPresetIsPermanentAnd)");
                    nh6 nh6 = nh6.a;
                    Object[] objArr = {xj6.d(str2)};
                    string = String.format(string2, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) string, "java.lang.String.format(format, *args)");
                } else {
                    wg6.a();
                    throw null;
                }
            }
            Bundle bundle = new Bundle();
            bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
            AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
            nh6 nh62 = nh6.a;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886380);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
            Object[] objArr2 = {str};
            String format = String.format(a2, Arrays.copyOf(objArr2, objArr2.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            fVar.a(2131363218, format);
            fVar.a(2131363129, string);
            fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886377));
            fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886376));
            fVar.b(2131363190);
            fVar.b(2131363105);
            fVar.a(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2) {
        wg6.b(list, "views");
        wg6.b(list2, "customizeWidgetViews");
        if (m35 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                String c2 = m35.c();
                ArrayList arrayList = new ArrayList(list);
                j45 j45 = this.g;
                if (j45 != null) {
                    aVar.a(activity, c2, arrayList, list2, j45.i(), 0, "top", "top");
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(m35 m35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2, String str, int i2) {
        List<? extends u8<View, String>> list3 = list;
        String str2 = str;
        wg6.b(list, "views");
        List<? extends u8<CustomizeWidget, String>> list4 = list2;
        wg6.b(list2, "customizeWidgetViews");
        wg6.b(str2, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetWatchClick preset=");
        sb.append(m35 != null ? m35.a() : null);
        sb.append(" watchAppPos=");
        sb.append(str2);
        sb.append(" position=");
        sb.append(i2);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        if (m35 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                String c2 = m35.c();
                ArrayList arrayList = new ArrayList(list);
                j45 j45 = this.g;
                if (j45 != null) {
                    aVar.a(activity, c2, arrayList, list2, j45.i(), 2, "top", str);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        String str2;
        wg6.b(str, "tag");
        if (str.hashCode() == -1353443012 && str.equals("DIALOG_DELETE_PRESET") && i2 == 2131363190) {
            if (intent != null) {
                str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID");
                wg6.a((Object) str2, "it.getStringExtra(NEXT_ACTIVE_PRESET_ID)");
            } else {
                str2 = "";
            }
            j45 j45 = this.g;
            if (j45 != null) {
                j45.a(str2);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(List<m35> list, DianaComplicationRingStyle dianaComplicationRingStyle) {
        wg6.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showPresets - data=" + list.size());
        g45 g45 = this.o;
        if (g45 != null) {
            g45.a(list, dianaComplicationRingStyle);
            if (!this.t && this.j == 0) {
                this.t = false;
            }
            if (!this.t) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(this.j);
                } else {
                    wg6.d("rvCustomize");
                    throw null;
                }
            }
        } else {
            wg6.d("mAdapterDiana");
            throw null;
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (z) {
            ConstraintLayout constraintLayout = this.h;
            if (constraintLayout != null) {
                constraintLayout.setVisibility(0);
            } else {
                wg6.d("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout2 = this.h;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(8);
            } else {
                wg6.d("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(j45 j45) {
        wg6.b(j45, "presenter");
        this.g = j45;
    }
}
