package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.loader.app.LoaderManager;
import com.fossil.a35;
import com.fossil.ae;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.r25;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.s25;
import com.fossil.sf6;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wg6;
import com.fossil.wx4;
import com.fossil.x25$b$a;
import com.fossil.x25$b$b;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.z24;
import com.fossil.zd;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridContactPresenter extends r25 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a((qg6) null);
    @DexIgnore
    public /* final */ List<wx4> e; // = new ArrayList();
    @DexIgnore
    public /* final */ s25 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<wx4> h;
    @DexIgnore
    public /* final */ LoaderManager i;
    @DexIgnore
    public /* final */ z24 j;
    @DexIgnore
    public /* final */ a35 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridContactPresenter.l;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1", f = "NotificationHybridContactPresenter.kt", l = {48}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridContactPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(NotificationHybridContactPresenter notificationHybridContactPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationHybridContactPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (!PortfolioApp.get.instance().w().P()) {
                    dl6 a2 = this.this$0.b();
                    x25$b$a x25_b_a = new x25$b$a((xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (gk6.a(a2, x25_b_a, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.j().isEmpty()) {
                this.this$0.j.a(this.this$0.k, null, new x25$b$b(this));
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = NotificationHybridContactPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationHybridContac\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public NotificationHybridContactPresenter(s25 s25, int i2, ArrayList<wx4> arrayList, LoaderManager loaderManager, z24 z24, a35 a35) {
        wg6.b(s25, "mView");
        wg6.b(arrayList, "mContactWrappersSelected");
        wg6.b(loaderManager, "mLoaderManager");
        wg6.b(z24, "mUseCaseHandler");
        wg6.b(a35, "mGetAllHybridContactGroups");
        this.f = s25;
        this.g = i2;
        this.h = arrayList;
        this.i = loaderManager;
        this.j = z24;
        this.k = a35;
    }

    @DexIgnore
    public void i() {
        ArrayList arrayList = new ArrayList();
        for (wx4 wx4 : this.e) {
            if (wx4.isAdded() && wx4.getCurrentHandGroup() == this.g) {
                arrayList.add(wx4);
            }
        }
        this.f.a((ArrayList<wx4>) arrayList);
    }

    @DexIgnore
    public final List<wx4> j() {
        return this.e;
    }

    @DexIgnore
    public void k() {
        this.f.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(l, "start");
        xm4 xm4 = xm4.d;
        s25 s25 = this.f;
        if (s25 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactFragment");
        } else if (xm4.a(xm4, ((NotificationHybridContactFragment) s25).getContext(), "NOTIFICATION_CONTACTS", false, false, false, 28, (Object) null)) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    public int h() {
        return this.g;
    }

    @DexIgnore
    public void a(wx4 wx4) {
        T t;
        wg6.b(wx4, "contactWrapper");
        FLogger.INSTANCE.getLocal().d(l, "reassignContact: contactWrapper=" + wx4);
        Iterator<T> it = this.e.iterator();
        while (true) {
            Integer num = null;
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            Contact contact = ((wx4) t).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = wx4.getContact();
            if (contact2 != null) {
                num = Integer.valueOf(contact2.getContactId());
            }
            if (wg6.a((Object) valueOf, (Object) num)) {
                break;
            }
        }
        wx4 wx42 = (wx4) t;
        if (wx42 != null) {
            wx42.setAdded(true);
            wx42.setCurrentHandGroup(this.g);
            this.f.F0();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public ae<Cursor> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onCreateLoader, selection = " + "has_phone_number!=0 AND mimetype=?");
        String[] strArr = {"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"};
        return new zd(PortfolioApp.get.instance(), ContactsContract.Data.CONTENT_URI, strArr, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    public void a(ae<Cursor> aeVar, Cursor cursor) {
        wg6.b(aeVar, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.f.a(cursor);
    }

    @DexIgnore
    public void a(ae<Cursor> aeVar) {
        wg6.b(aeVar, "loader");
        FLogger.INSTANCE.getLocal().d(l, ".Inside onLoaderReset");
        this.f.J();
    }
}
