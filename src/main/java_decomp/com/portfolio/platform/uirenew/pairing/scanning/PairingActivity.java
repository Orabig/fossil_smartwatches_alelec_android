package com.portfolio.platform.uirenew.pairing.scanning;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.os5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.zx5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.pairing.PairingFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ a D; // = new a((qg6) null);
    @DexIgnore
    public PairingPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return PairingActivity.C;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z) {
            wg6.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start isOnboarding=" + z);
            Intent intent = new Intent(context, PairingActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = PairingActivity.class.getSimpleName();
        wg6.a((Object) simpleName, "PairingActivity::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        PairingActivity.super.onActivityResult(i, i2, intent);
        Fragment b = getSupportFragmentManager().b(2131362119);
        if (b != null) {
            b.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.uirenew.pairing.scanning.PairingActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        PairingFragment b = getSupportFragmentManager().b(2131362119);
        Intent intent = getIntent();
        boolean z = false;
        if (intent != null) {
            z = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (b == null) {
            b = PairingFragment.o.a(z);
            a((Fragment) b, 2131362119);
        }
        PortfolioApp.get.instance().g().a(new os5(b)).a(this);
        if (bundle != null) {
            PairingPresenter pairingPresenter = this.B;
            if (pairingPresenter != null) {
                pairingPresenter.d(bundle.getBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP"));
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        PairingPresenter pairingPresenter = this.B;
        if (pairingPresenter != null) {
            bundle.putBoolean("KEY_IS_PAIR_DEVICE_FAIL_POPUP", pairingPresenter.v());
            PairingActivity.super.onSaveInstanceState(bundle);
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        s();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.uirenew.pairing.scanning.PairingActivity] */
    public final void s() {
        zx5.a.a(this, MFDeviceService.class, Constants.START_FOREGROUND_ACTION);
        zx5.a.a(this, ButtonService.class, Constants.START_FOREGROUND_ACTION);
    }
}
