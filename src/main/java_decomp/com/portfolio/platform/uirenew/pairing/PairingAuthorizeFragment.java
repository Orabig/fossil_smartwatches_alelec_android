package com.portfolio.platform.uirenew.pairing;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.d34;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.ks5;
import com.fossil.nh6;
import com.fossil.oj3;
import com.fossil.qg6;
import com.fossil.rc4;
import com.fossil.us5;
import com.fossil.wg6;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.pairing.scanning.BasePairingSubFragment;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingAuthorizeFragment extends BasePairingSubFragment {
    @DexIgnore
    public static /* final */ a w; // = new a((qg6) null);
    @DexIgnore
    public ks5 g;
    @DexIgnore
    public us5 h;
    @DexIgnore
    public ax5<rc4> i;
    @DexIgnore
    public d34 j; // = new d34();
    @DexIgnore
    public String o; // = "";
    @DexIgnore
    public List<String> p; // = new ArrayList();
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public /* final */ String s; // = ThemeManager.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String t; // = ThemeManager.l.a().b("primaryColor");
    @DexIgnore
    public /* final */ String u; // = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final PairingAuthorizeFragment a(List<String> list) {
            wg6.b(list, "instructionList");
            PairingAuthorizeFragment pairingAuthorizeFragment = new PairingAuthorizeFragment();
            pairingAuthorizeFragment.p = list;
            pairingAuthorizeFragment.q = 0;
            return pairingAuthorizeFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final PairingAuthorizeFragment a(String str) {
            wg6.b(str, "serial");
            PairingAuthorizeFragment pairingAuthorizeFragment = new PairingAuthorizeFragment();
            pairingAuthorizeFragment.o = str;
            pairingAuthorizeFragment.q = 1;
            return pairingAuthorizeFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements oj3.b {
        @DexIgnore
        public /* final */ /* synthetic */ PairingAuthorizeFragment a;

        @DexIgnore
        public b(PairingAuthorizeFragment pairingAuthorizeFragment) {
            this.a = pairingAuthorizeFragment;
        }

        @DexIgnore
        public final void a(TabLayout.g gVar, int i) {
            wg6.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.s) && !TextUtils.isEmpty(this.a.t)) {
                int parseColor = Color.parseColor(this.a.s);
                int parseColor2 = Color.parseColor(this.a.t);
                gVar.b(2131231004);
                if (i == this.a.r) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ rc4 a;
        @DexIgnore
        public /* final */ /* synthetic */ PairingAuthorizeFragment b;

        @DexIgnore
        public c(rc4 rc4, PairingAuthorizeFragment pairingAuthorizeFragment) {
            this.a = rc4;
            this.b = pairingAuthorizeFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public void b(int i) {
            Drawable b2;
            Drawable b3;
            if (i == 0) {
                Object r1 = this.a.v;
                wg6.a((Object) r1, "binding.ftvTimeout");
                r1.setVisibility(0);
            } else {
                Object r12 = this.a.v;
                wg6.a((Object) r12, "binding.ftvTimeout");
                r12.setVisibility(4);
            }
            if (!TextUtils.isEmpty(this.b.t)) {
                int parseColor = Color.parseColor(this.b.t);
                TabLayout.g b4 = this.a.w.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.s) && this.b.r != i) {
                int parseColor2 = Color.parseColor(this.b.s);
                TabLayout.g b5 = this.a.w.b(this.b.r);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.r = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingAuthorizeFragment a;

        @DexIgnore
        public d(PairingAuthorizeFragment pairingAuthorizeFragment) {
            this.a = pairingAuthorizeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            PairingAuthorizeFragment.c(this.a).h();
            this.a.i1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingAuthorizeFragment a;

        @DexIgnore
        public e(PairingAuthorizeFragment pairingAuthorizeFragment) {
            this.a = pairingAuthorizeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            Context context = this.a.getContext();
            if (context != null) {
                wg6.a((Object) context, "context!!");
                aVar.a(context, this.a.o, true);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PairingAuthorizeFragment a;

        @DexIgnore
        public f(PairingAuthorizeFragment pairingAuthorizeFragment) {
            this.a = pairingAuthorizeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = this.a.getActivity();
                    if (activity2 != null) {
                        wg6.a((Object) activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            FragmentActivity activity3 = this.a.getActivity();
                            if (activity3 != null) {
                                activity3.finish();
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ ks5 c(PairingAuthorizeFragment pairingAuthorizeFragment) {
        ks5 ks5 = pairingAuthorizeFragment.g;
        if (ks5 != null) {
            return ks5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void R(boolean z) {
        us5 us5 = this.h;
        if (us5 != null) {
            us5.a(z);
        } else {
            wg6.d("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void Z(String str) {
        wg6.b(str, "serial");
        this.o = str;
        ax5<rc4> ax5 = this.i;
        if (ax5 != null) {
            rc4 a2 = ax5.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                wg6.a((Object) constraintLayout, "it.clPairingAuthorize");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.q;
                wg6.a((Object) constraintLayout2, "it.clAuthorizeFailed");
                constraintLayout2.setVisibility(0);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        this.i = new ax5<>(this, kb.a(layoutInflater, 2131558590, viewGroup, false, e1()));
        ax5<rc4> ax5 = this.i;
        if (ax5 != null) {
            rc4 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroyView() {
        us5 us5 = this.h;
        if (us5 != null) {
            us5.a();
            super.onDestroyView();
            d1();
            return;
        }
        wg6.d("mSubPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r6v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v9, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        this.j.a(this.p);
        this.h = new us5(this);
        ax5<rc4> ax5 = this.i;
        if (ax5 != null) {
            rc4 a2 = ax5.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.y;
                wg6.a((Object) viewPager2, "binding.vpAuthorizeInstruction");
                viewPager2.setAdapter(this.j);
                if (!TextUtils.isEmpty(this.u)) {
                    TabLayout tabLayout = a2.w;
                    wg6.a((Object) tabLayout, "binding.indicator");
                    tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.u)));
                }
                ax5<rc4> ax52 = this.i;
                if (ax52 != null) {
                    rc4 a3 = ax52.a();
                    TabLayout tabLayout2 = a3 != null ? a3.w : null;
                    if (tabLayout2 != null) {
                        ax5<rc4> ax53 = this.i;
                        if (ax53 != null) {
                            rc4 a4 = ax53.a();
                            ViewPager2 viewPager22 = a4 != null ? a4.y : null;
                            if (viewPager22 != null) {
                                new oj3(tabLayout2, viewPager22, new b(this)).a();
                                a2.y.a(new c(a2, this));
                                a2.s.setOnClickListener(new d(this));
                                a2.u.setOnClickListener(new e(this));
                                a2.t.setOnClickListener(new f(this));
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.d("mBinding");
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            }
            int i2 = this.q;
            if (i2 == 0) {
                x(this.p);
            } else if (i2 == 1) {
                Z(this.o);
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void r(int i2) {
        ax5<rc4> ax5 = this.i;
        if (ax5 != null) {
            rc4 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.v;
                wg6.a((Object) r0, "it.ftvTimeout");
                nh6 nh6 = nh6.a;
                Context context = getContext();
                if (context != null) {
                    String a3 = jm4.a(context, 2131886659);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026_ExpiringInSecondSeconds)");
                    Object[] objArr = {Integer.valueOf(i2)};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    r0.setText(format);
                    return;
                }
                wg6.a();
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void x(List<String> list) {
        wg6.b(list, "instructionList");
        this.j.a(list);
        ax5<rc4> ax5 = this.i;
        if (ax5 != null) {
            rc4 a2 = ax5.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                wg6.a((Object) constraintLayout, "it.clPairingAuthorize");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.q;
                wg6.a((Object) constraintLayout2, "it.clAuthorizeFailed");
                constraintLayout2.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(ks5 ks5) {
        wg6.b(ks5, "presenter");
        this.g = ks5;
    }
}
