package com.portfolio.platform.uirenew.alarm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.l64;
import com.fossil.lx5;
import com.fossil.pv4;
import com.fossil.qg6;
import com.fossil.qv4;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmFragment extends BasePermissionFragment implements qv4, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public ax5<l64> g;
    @DexIgnore
    public pv4 h;
    @DexIgnore
    public /* final */ s i; // = new s(this);
    @DexIgnore
    public /* final */ p j; // = new p(this);
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return AlarmFragment.p;
        }

        @DexIgnore
        public final AlarmFragment b() {
            return new AlarmFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public b(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onClick(View view) {
            wg6.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.a;
            Object r1 = this.b.r;
            wg6.a((Object) r1, "binding.dayFriday");
            alarmFragment.a((TextView) r1);
            this.a.b(6, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public c(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onClick(View view) {
            wg6.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.a;
            Object r1 = this.b.t;
            wg6.a((Object) r1, "binding.daySaturday");
            alarmFragment.a((TextView) r1);
            this.a.b(7, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public d(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            pv4 b2 = AlarmFragment.b(this.a);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.b.H;
            wg6.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.G;
            wg6.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            b2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public e(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            pv4 b2 = AlarmFragment.b(this.a);
            NumberPicker numberPicker2 = this.b.F;
            wg6.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.b.G;
            wg6.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            b2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public f(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            pv4 b2 = AlarmFragment.b(this.a);
            NumberPicker numberPicker2 = this.b.F;
            wg6.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.H;
            wg6.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            b2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;

        @DexIgnore
        public g(AlarmFragment alarmFragment) {
            this.a = alarmFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;

        @DexIgnore
        public h(AlarmFragment alarmFragment) {
            this.a = alarmFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.i(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public i(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            AlarmFragment.b(this.a).a(z);
            ConstraintLayout constraintLayout = this.b.q;
            wg6.a((Object) constraintLayout, "binding.clDaysRepeat");
            constraintLayout.setVisibility(z ? 0 : 8);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;

        @DexIgnore
        public j(AlarmFragment alarmFragment) {
            this.a = alarmFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            AlarmFragment.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public k(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onClick(View view) {
            wg6.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.a;
            Object r2 = this.b.u;
            wg6.a((Object) r2, "binding.daySunday");
            alarmFragment.a((TextView) r2);
            this.a.b(1, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public l(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onClick(View view) {
            wg6.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.a;
            Object r1 = this.b.s;
            wg6.a((Object) r1, "binding.dayMonday");
            alarmFragment.a((TextView) r1);
            this.a.b(2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public m(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onClick(View view) {
            wg6.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.a;
            Object r1 = this.b.w;
            wg6.a((Object) r1, "binding.dayTuesday");
            alarmFragment.a((TextView) r1);
            this.a.b(3, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public n(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onClick(View view) {
            wg6.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.a;
            Object r1 = this.b.x;
            wg6.a((Object) r1, "binding.dayWednesday");
            alarmFragment.a((TextView) r1);
            this.a.b(4, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ l64 b;

        @DexIgnore
        public o(AlarmFragment alarmFragment, l64 l64) {
            this.a = alarmFragment;
            this.b = l64;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onClick(View view) {
            wg6.a((Object) view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.a;
            Object r1 = this.b.v;
            wg6.a((Object) r1, "binding.dayThursday");
            alarmFragment.a((TextView) r1);
            this.a.b(5, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;

        @DexIgnore
        public p(AlarmFragment alarmFragment) {
            this.a = alarmFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v12, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public void afterTextChanged(Editable editable) {
            Object r0;
            Object r02;
            l64 l64 = (l64) AlarmFragment.a(this.a).a();
            if (!(l64 == null || (r02 = l64.z) == 0)) {
                r02.requestLayout();
            }
            AlarmFragment.b(this.a).a(String.valueOf(editable));
            if (editable != null) {
                String str = editable.length() + " / 50";
                l64 l642 = (l64) AlarmFragment.a(this.a).a();
                if (l642 != null && (r0 = l642.B) != 0) {
                    r0.setText(str);
                }
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleEditText a;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;

        @DexIgnore
        public q(FlexibleEditText flexibleEditText, AlarmFragment alarmFragment) {
            this.a = flexibleEditText;
            this.b = alarmFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onFocusChange(View view, boolean z) {
            Object r4;
            int i;
            l64 l64 = (l64) AlarmFragment.a(this.b).a();
            if (l64 != null && (r4 = l64.C) != 0) {
                if (z) {
                    FlexibleEditText flexibleEditText = this.a;
                    wg6.a((Object) flexibleEditText, "it");
                    Editable text = flexibleEditText.getText();
                    if (text == null || text.length() == 0) {
                        i = 0;
                    } else {
                        FlexibleEditText flexibleEditText2 = this.a;
                        wg6.a((Object) flexibleEditText2, "it");
                        Editable text2 = flexibleEditText2.getText();
                        if (text2 != null) {
                            i = text2.length();
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    wg6.a((Object) r4, "counterTextView");
                    r4.setText(i + " / 15");
                    r4.setVisibility(0);
                    return;
                }
                wg6.a((Object) r4, "counterTextView");
                r4.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleEditText a;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;

        @DexIgnore
        public r(FlexibleEditText flexibleEditText, AlarmFragment alarmFragment) {
            this.a = flexibleEditText;
            this.b = alarmFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void onFocusChange(View view, boolean z) {
            Object r4;
            int i;
            l64 l64 = (l64) AlarmFragment.a(this.b).a();
            if (l64 != null && (r4 = l64.B) != 0) {
                if (z) {
                    FlexibleEditText flexibleEditText = this.a;
                    wg6.a((Object) flexibleEditText, "it");
                    Editable text = flexibleEditText.getText();
                    if (text == null || text.length() == 0) {
                        i = 0;
                    } else {
                        FlexibleEditText flexibleEditText2 = this.a;
                        wg6.a((Object) flexibleEditText2, "it");
                        Editable text2 = flexibleEditText2.getText();
                        if (text2 != null) {
                            i = text2.length();
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    wg6.a((Object) r4, "counterTextView");
                    r4.setText(i + " / 50");
                    r4.setVisibility(0);
                    return;
                }
                wg6.a((Object) r4, "counterTextView");
                r4.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;

        @DexIgnore
        public s(AlarmFragment alarmFragment) {
            this.a = alarmFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v12, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public void afterTextChanged(Editable editable) {
            Object r0;
            Object r02;
            l64 l64 = (l64) AlarmFragment.a(this.a).a();
            if (!(l64 == null || (r02 = l64.A) == 0)) {
                r02.requestLayout();
            }
            AlarmFragment.b(this.a).b(String.valueOf(editable));
            if (editable != null) {
                String str = editable.length() + " / 15";
                l64 l642 = (l64) AlarmFragment.a(this.a).a();
                if (l642 != null && (r0 = l642.C) != 0) {
                    r0.setText(str);
                }
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /*
    static {
        String simpleName = AlarmFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "AlarmFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ax5 a(AlarmFragment alarmFragment) {
        ax5<l64> ax5 = alarmFragment.g;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ pv4 b(AlarmFragment alarmFragment) {
        pv4 pv4 = alarmFragment.h;
        if (pv4 != null) {
            return pv4;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public String C() {
        String str;
        ax5<l64> ax5 = this.g;
        Editable editable = null;
        if (ax5 != null) {
            l64 a2 = ax5.a();
            FlexibleEditText flexibleEditText = a2 != null ? a2.z : null;
            Editable text = flexibleEditText != null ? flexibleEditText.getText() : null;
            if (!(text == null || text.length() == 0)) {
                if (flexibleEditText != null) {
                    editable = flexibleEditText.getText();
                }
                str = String.valueOf(editable);
            } else {
                str = "";
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = p;
            local.d(str2, "getMessage, value = " + str);
            return str;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void N() {
        Object r0;
        ax5<l64> ax5 = this.g;
        if (ax5 != null) {
            l64 a2 = ax5.a();
            if (a2 != null && (r0 = a2.E) != 0) {
                r0.setVisibility(0);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void R(boolean z) {
        if (!z) {
            W("add_alarm_view");
        } else {
            W("edit_alarm_view");
        }
    }

    @DexIgnore
    public void X() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.P(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void c(int i2, boolean z) {
        ax5<l64> ax5 = this.g;
        if (ax5 != null) {
            l64 a2 = ax5.a();
            if (a2 != null) {
                switch (i2) {
                    case 1:
                        Object r3 = a2.u;
                        wg6.a((Object) r3, "binding.daySunday");
                        r3.setSelected(z);
                        Object r32 = a2.u;
                        wg6.a((Object) r32, "binding.daySunday");
                        a((TextView) r32);
                        return;
                    case 2:
                        Object r33 = a2.s;
                        wg6.a((Object) r33, "binding.dayMonday");
                        r33.setSelected(z);
                        Object r34 = a2.s;
                        wg6.a((Object) r34, "binding.dayMonday");
                        a((TextView) r34);
                        return;
                    case 3:
                        Object r35 = a2.w;
                        wg6.a((Object) r35, "binding.dayTuesday");
                        r35.setSelected(z);
                        Object r36 = a2.w;
                        wg6.a((Object) r36, "binding.dayTuesday");
                        a((TextView) r36);
                        return;
                    case 4:
                        Object r37 = a2.x;
                        wg6.a((Object) r37, "binding.dayWednesday");
                        r37.setSelected(z);
                        Object r38 = a2.x;
                        wg6.a((Object) r38, "binding.dayWednesday");
                        a((TextView) r38);
                        return;
                    case 5:
                        Object r39 = a2.v;
                        wg6.a((Object) r39, "binding.dayThursday");
                        r39.setSelected(z);
                        Object r310 = a2.v;
                        wg6.a((Object) r310, "binding.dayThursday");
                        a((TextView) r310);
                        return;
                    case 6:
                        Object r311 = a2.r;
                        wg6.a((Object) r311, "binding.dayFriday");
                        r311.setSelected(z);
                        Object r312 = a2.r;
                        wg6.a((Object) r312, "binding.dayFriday");
                        a((TextView) r312);
                        return;
                    case 7:
                        Object r313 = a2.t;
                        wg6.a((Object) r313, "binding.daySaturday");
                        r313.setSelected(z);
                        Object r314 = a2.t;
                        wg6.a((Object) r314, "binding.daySaturday");
                        a((TextView) r314);
                        return;
                    default:
                        return;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void d(boolean z) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        Object r0;
        ax5<l64> ax5 = this.g;
        if (ax5 != null) {
            l64 a2 = ax5.a();
            if (!(a2 == null || (r0 = a2.y) == 0)) {
                r0.setEnabled(z);
            }
            if (z) {
                ax5<l64> ax52 = this.g;
                if (ax52 != null) {
                    l64 a3 = ax52.a();
                    if (a3 != null && (flexibleButton2 = a3.y) != null) {
                        flexibleButton2.a("flexible_button_primary");
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            ax5<l64> ax53 = this.g;
            if (ax53 != null) {
                l64 a4 = ax53.a();
                if (a4 != null && (flexibleButton = a4.y) != null) {
                    flexibleButton.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String getTitle() {
        String str;
        ax5<l64> ax5 = this.g;
        Editable editable = null;
        if (ax5 != null) {
            l64 a2 = ax5.a();
            FlexibleEditText flexibleEditText = a2 != null ? a2.A : null;
            Editable text = flexibleEditText != null ? flexibleEditText.getText() : null;
            if (!(text == null || text.length() == 0)) {
                if (flexibleEditText != null) {
                    editable = flexibleEditText.getText();
                }
                str = String.valueOf(editable);
            } else {
                str = "";
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = p;
            local.d(str2, "getTitle, value = " + str);
            return str;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return p;
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public void k(boolean z) {
        ConstraintLayout constraintLayout;
        FlexibleSwitchCompat flexibleSwitchCompat;
        ax5<l64> ax5 = this.g;
        if (ax5 != null) {
            l64 a2 = ax5.a();
            if (!(a2 == null || (flexibleSwitchCompat = a2.J) == null)) {
                flexibleSwitchCompat.setChecked(z);
            }
            ax5<l64> ax52 = this.g;
            if (ax52 != null) {
                l64 a3 = ax52.a();
                if (a3 != null && (constraintLayout = a3.q) != null) {
                    constraintLayout.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void m(String str) {
        Object r0;
        wg6.b(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local.d(str2, "showMessage, value = " + str);
        if (!xj6.a(str)) {
            ax5<l64> ax5 = this.g;
            if (ax5 != null) {
                l64 a2 = ax5.a();
                if (a2 != null && (r0 = a2.z) != 0) {
                    r0.setText(str);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v13, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r3v14, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Object r3;
        Object r32;
        wg6.b(layoutInflater, "inflater");
        AlarmFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        l64 a2 = kb.a(layoutInflater, 2131558505, viewGroup, false, e1());
        wg6.a((Object) a2, "binding");
        a(a2);
        this.g = new ax5<>(this, a2);
        ax5<l64> ax5 = this.g;
        if (ax5 != null) {
            l64 a3 = ax5.a();
            if (!(a3 == null || (r32 = a3.A) == 0)) {
                r32.addTextChangedListener(this.i);
                r32.setOnFocusChangeListener(new q(r32, this));
            }
            ax5<l64> ax52 = this.g;
            if (ax52 != null) {
                l64 a4 = ax52.a();
                if (!(a4 == null || (r3 = a4.z) == 0)) {
                    r3.addTextChangedListener(this.j);
                    r3.setOnFocusChangeListener(new r(r3, this));
                }
                ax5<l64> ax53 = this.g;
                if (ax53 != null) {
                    l64 a5 = ax53.a();
                    if (a5 != null) {
                        wg6.a((Object) a5, "mBinding.get()!!");
                        return a5.d();
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void onDestroyView() {
        ax5<l64> ax5 = this.g;
        if (ax5 != null) {
            l64 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.A;
                if (r1 != 0) {
                    r1.removeTextChangedListener(this.i);
                }
                a2.z.removeTextChangedListener(this.j);
            }
            super.onDestroyView();
            d1();
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onPause() {
        AlarmFragment.super.onPause();
        pv4 pv4 = this.h;
        if (pv4 != null) {
            pv4.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        AlarmFragment.super.onResume();
        pv4 pv4 = this.h;
        if (pv4 != null) {
            pv4.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void t(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "startFireBaseTracer(), isAddNewAlarm = " + z);
        R(z);
    }

    @DexIgnore
    public void x() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void b(int i2) {
        int i3;
        int i4 = i2 / 60;
        int i5 = i2 % 60;
        if (i4 >= 12) {
            i3 = 1;
            i4 -= 12;
        } else {
            i3 = 0;
        }
        if (i4 == 0) {
            i4 = 12;
        }
        ax5<l64> ax5 = this.g;
        if (ax5 != null) {
            l64 a2 = ax5.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.F;
                wg6.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i4);
                NumberPicker numberPicker2 = a2.H;
                wg6.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i5);
                NumberPicker numberPicker3 = a2.G;
                wg6.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i3);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(pv4 pv4) {
        wg6.b(pv4, "presenter");
        this.h = pv4;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v15, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v16, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r0v17, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r0v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v24, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(l64 l64) {
        Object r0 = l64.u;
        wg6.a((Object) r0, "binding.daySunday");
        if (!r0.isSelected()) {
            l64.u.a("flexible_tv_selected");
        } else {
            l64.u.a("flexible_tv_unselected");
        }
        Object r02 = l64.s;
        wg6.a((Object) r02, "binding.dayMonday");
        if (!r02.isSelected()) {
            l64.s.a("flexible_tv_selected");
        } else {
            l64.s.a("flexible_tv_unselected");
        }
        Object r03 = l64.w;
        wg6.a((Object) r03, "binding.dayTuesday");
        if (!r03.isSelected()) {
            l64.w.a("flexible_tv_selected");
        } else {
            l64.w.a("flexible_tv_unselected");
        }
        Object r04 = l64.x;
        wg6.a((Object) r04, "binding.dayWednesday");
        if (!r04.isSelected()) {
            l64.x.a("flexible_tv_selected");
        } else {
            l64.x.a("flexible_tv_unselected");
        }
        Object r05 = l64.v;
        wg6.a((Object) r05, "binding.dayThursday");
        if (!r05.isSelected()) {
            l64.v.a("flexible_tv_selected");
        } else {
            l64.v.a("flexible_tv_unselected");
        }
        Object r06 = l64.r;
        wg6.a((Object) r06, "binding.dayFriday");
        if (!r06.isSelected()) {
            l64.r.a("flexible_tv_selected");
        } else {
            l64.r.a("flexible_tv_unselected");
        }
        Object r07 = l64.t;
        wg6.a((Object) r07, "binding.daySaturday");
        if (!r07.isSelected()) {
            l64.t.a("flexible_tv_selected");
        } else {
            l64.t.a("flexible_tv_unselected");
        }
        l64.D.setOnClickListener(new g(this));
        l64.E.setOnClickListener(new h(this));
        l64.J.setOnCheckedChangeListener(new i(this, l64));
        l64.y.setOnClickListener(new j(this));
        l64.u.setOnClickListener(new k(this, l64));
        l64.s.setOnClickListener(new l(this, l64));
        l64.w.setOnClickListener(new m(this, l64));
        l64.x.setOnClickListener(new n(this, l64));
        l64.v.setOnClickListener(new o(this, l64));
        l64.r.setOnClickListener(new b(this, l64));
        l64.t.setOnClickListener(new c(this, l64));
        NumberPicker numberPicker = l64.F;
        wg6.a((Object) numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = l64.F;
        wg6.a((Object) numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        l64.F.setOnValueChangedListener(new d(this, l64));
        NumberPicker numberPicker3 = l64.H;
        wg6.a((Object) numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = l64.H;
        wg6.a((Object) numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        l64.H.setOnValueChangedListener(new e(this, l64));
        String[] strArr = new String[2];
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886102);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
        if (a2 != null) {
            String upperCase = a2.toUpperCase();
            wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            strArr[0] = upperCase;
            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886104);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
            if (a3 != null) {
                String upperCase2 = a3.toUpperCase();
                wg6.a((Object) upperCase2, "(this as java.lang.String).toUpperCase()");
                strArr[1] = upperCase2;
                NumberPicker numberPicker5 = l64.G;
                wg6.a((Object) numberPicker5, "binding.numberPickerThree");
                numberPicker5.setMinValue(0);
                NumberPicker numberPicker6 = l64.G;
                wg6.a((Object) numberPicker6, "binding.numberPickerThree");
                numberPicker6.setMaxValue(1);
                l64.G.setDisplayedValues(strArr);
                l64.G.setOnValueChangedListener(new f(this, l64));
                return;
            }
            throw new rc6("null cannot be cast to non-null type java.lang.String");
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final void b(int i2, boolean z) {
        pv4 pv4 = this.h;
        if (pv4 != null) {
            pv4.a(z, i2);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.l(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void a(TextView textView) {
        wg6.b(textView, "textView");
        Object r2 = (FlexibleTextView) textView;
        if (r2.isSelected()) {
            r2.a("flexible_tv_selected");
        } else {
            r2.a("flexible_tv_unselected");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public void a(String str) {
        Object r0;
        wg6.b(str, Explore.COLUMN_TITLE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local.d(str2, "showTitle, value = " + str);
        if (!xj6.a(str)) {
            ax5<l64> ax5 = this.g;
            if (ax5 != null) {
                l64 a2 = ax5.a();
                if (a2 != null && (r0 = a2.A) != 0) {
                    r0.setText(str);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(SparseIntArray sparseIntArray) {
        wg6.b(sparseIntArray, "daysRepeat");
        int size = sparseIntArray.size();
        for (int i2 = 1; i2 <= 7; i2++) {
            boolean z = false;
            if (size == 0) {
                c(i2, false);
            } else {
                if (sparseIntArray.get(i2) == i2) {
                    z = true;
                }
                c(i2, z);
            }
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        wg6.b(str, "tag");
        super.a(str, i2, intent);
        int hashCode = str.hashCode();
        if (hashCode != 1038249436) {
            if (hashCode == 1185284775 && str.equals("CONFIRM_SET_ALARM_FAILED") && i2 == 2131362220 && (activity = getActivity()) != null) {
                activity.finish();
            }
        } else if (str.equals("CONFIRM_DELETE_ALARM") && i2 == 2131363190) {
            pv4 pv4 = this.h;
            if (pv4 != null) {
                pv4.h();
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(Alarm alarm, boolean z) {
        wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "showSetAlarmComplete: alarm = " + alarm + ", isSuccess = " + z);
        if (z) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            }
        } else if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.O(childFragmentManager);
        }
    }
}
