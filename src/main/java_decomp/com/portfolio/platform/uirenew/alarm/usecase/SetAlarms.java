package com.portfolio.platform.uirenew.alarm.usecase;

import android.content.Intent;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ik6;
import com.fossil.jf6;
import com.fossil.jl4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.ui4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zv4;
import com.fossil.zv4$e$a;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAlarms extends m24<zv4.c, zv4.d, zv4.b> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public c d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ e f; // = new e();
    @DexIgnore
    public /* final */ PortfolioApp g;
    @DexIgnore
    public /* final */ AlarmsRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SetAlarms.i;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ Alarm a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public b(Alarm alarm, int i, ArrayList<Integer> arrayList) {
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            wg6.b(arrayList, "mBLEErrorCodes");
            this.a = alarm;
            this.b = i;
            this.c = arrayList;
        }

        @DexIgnore
        public final Alarm a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ List<Alarm> b;
        @DexIgnore
        public /* final */ Alarm c;

        @DexIgnore
        public c(String str, List<Alarm> list, Alarm alarm) {
            wg6.b(str, "deviceId");
            wg6.b(list, "alarms");
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = str;
            this.b = list;
            this.c = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.c;
        }

        @DexIgnore
        public final List<Alarm> b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ Alarm a;

        @DexIgnore
        public d(Alarm alarm) {
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements BleCommandResultManager.b {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(SetAlarms.j.a(), "onReceive");
            if (communicateMode == CommunicateMode.SET_LIST_ALARM && SetAlarms.this.d()) {
                SetAlarms.this.a(false);
                rm6 unused = ik6.b(SetAlarms.this.b(), (af6) null, (ll6) null, new zv4$e$a(this, intent, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms", f = "SetAlarms.kt", l = {100}, m = "run")
    public static final class f extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SetAlarms this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(SetAlarms setAlarms, xe6 xe6) {
            super(xe6);
            this.this$0 = setAlarms;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((zv4.c) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        String simpleName = SetAlarms.class.getSimpleName();
        wg6.a((Object) simpleName, "SetAlarms::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public SetAlarms(PortfolioApp portfolioApp, AlarmsRepository alarmsRepository) {
        wg6.b(portfolioApp, "mApp");
        wg6.b(alarmsRepository, "mAlarmsRepository");
        this.g = portfolioApp;
        this.h = alarmsRepository;
    }

    @DexIgnore
    public String c() {
        return i;
    }

    @DexIgnore
    public final boolean d() {
        return this.e;
    }

    @DexIgnore
    public final c e() {
        c cVar = this.d;
        if (cVar != null) {
            return cVar;
        }
        wg6.d("mRequestValues");
        throw null;
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.alarm.usecase.SetAlarms] */
    public final void a(Intent intent, Alarm alarm) {
        wg6.b(intent, "intent");
        wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "processBLEErrorCodes() - errorCode=" + intExtra);
        ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
        if (integerArrayListExtra == null) {
            integerArrayListExtra = new ArrayList<>(intExtra);
        }
        jl4 c2 = AnalyticsHelper.f.c("set_alarms");
        if (c2 != null) {
            c2.a(String.valueOf(intExtra));
        }
        AnalyticsHelper.f.e("set_alarms");
        a(new b(alarm, intExtra, integerArrayListExtra));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(zv4.c cVar, xe6<Object> xe6) {
        f fVar;
        int i2;
        Alarm alarm;
        if (xe6 instanceof f) {
            fVar = (f) xe6;
            int i3 = fVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                fVar.label = i3 - Integer.MIN_VALUE;
                Object obj = fVar.result;
                Object a2 = ff6.a();
                i2 = fVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(i, "executeUseCase");
                    jl4 b2 = AnalyticsHelper.f.b("set_alarms");
                    AnalyticsHelper.f.a("set_alarms", b2);
                    b2.d();
                    if (cVar != null) {
                        this.d = cVar;
                        Alarm a3 = cVar.a();
                        Alarm alarmById = this.h.getAlarmById(a3.getUri());
                        if (alarmById == null || alarmById.isActive() || a3.isActive()) {
                            FLogger.INSTANCE.getLocal().d(i, "alarm not found");
                            this.e = true;
                            a((c) cVar);
                            return cd6.a;
                        }
                        FLogger.INSTANCE.getLocal().d(i, "alarm found and not active");
                        AlarmsRepository alarmsRepository = this.h;
                        fVar.L$0 = this;
                        fVar.L$1 = cVar;
                        fVar.L$2 = a3;
                        fVar.L$3 = alarmById;
                        fVar.label = 1;
                        if (alarmsRepository.updateAlarm(a3, fVar) == a2) {
                            return a2;
                        }
                        alarm = a3;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    Alarm alarm2 = (Alarm) fVar.L$3;
                    alarm = (Alarm) fVar.L$2;
                    c cVar2 = (c) fVar.L$1;
                    SetAlarms setAlarms = (SetAlarms) fVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return new d(alarm);
            }
        }
        fVar = new f(this, xe6);
        Object obj2 = fVar.result;
        Object a22 = ff6.a();
        i2 = fVar.label;
        if (i2 != 0) {
        }
        return new d(alarm);
    }

    @DexIgnore
    public final void a(c cVar) {
        List<Alarm> b2 = cVar.b();
        ArrayList arrayList = new ArrayList();
        for (T next : b2) {
            if (((Alarm) next).isActive()) {
                arrayList.add(next);
            }
        }
        this.g.a(cVar.c(), (List<? extends com.misfit.frameworks.buttonservice.model.Alarm>) ui4.a(arrayList));
    }
}
