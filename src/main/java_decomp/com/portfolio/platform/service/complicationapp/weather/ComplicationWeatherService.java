package com.portfolio.platform.service.complicationapp.weather;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.bi4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hl4;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl4;
import com.fossil.jl6;
import com.fossil.kc6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.pw5;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.wp4;
import com.fossil.xe6;
import com.fossil.xh4;
import com.fossil.y24;
import com.fossil.yp4;
import com.fossil.z24;
import com.fossil.zh4;
import com.fossil.zl6;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.LogConfiguration;
import com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.usecase.GetWeather;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationWeatherService extends wp4 {
    @DexIgnore
    public /* final */ int g; // = LogConfiguration.DEBUG_FLUSH_LOG_TIME_THRESHOLD;
    @DexIgnore
    public GetWeather h;
    @DexIgnore
    public z24 i;
    @DexIgnore
    public an4 j;
    @DexIgnore
    public PortfolioApp o;
    @DexIgnore
    public CustomizeRealDataRepository p;
    @DexIgnore
    public UserRepository q;
    @DexIgnore
    public String r;
    @DexIgnore
    public WeatherSettings s;
    @DexIgnore
    public Weather t;
    @DexIgnore
    public a<String> u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.d<pw5.d, pw5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService a;

        @DexIgnore
        public b(ComplicationWeatherService complicationWeatherService) {
            this.a = complicationWeatherService;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(GetWeather.d dVar) {
            wg6.b(dVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(wp4.f.a(), "getWeather - onSuccess");
            this.a.a(dVar.a());
            if (this.a.j() != null) {
                this.a.i().s(new Gson().a(this.a.k(), WeatherSettings.class));
                ComplicationWeatherService complicationWeatherService = this.a;
                WeatherSettings k = complicationWeatherService.k();
                if (k != null) {
                    ComplicationWeatherService.a(complicationWeatherService, k, false, 2, (Object) null);
                    this.a.a();
                    return;
                }
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public void a(GetWeather.b bVar) {
            wg6.b(bVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(wp4.f.a(), "getWeather - onError");
            this.a.a(bVar.a());
            this.a.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$getWeatherBaseOnLocation$1", f = "ComplicationWeatherService.kt", l = {118}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ComplicationWeatherService complicationWeatherService, xe6 xe6) {
            super(2, xe6);
            this.this$0 = complicationWeatherService;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.k() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = wp4.f.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getWeatherBaseOnLocation - location=");
                    WeatherSettings k = this.this$0.k();
                    if (k != null) {
                        sb.append(k.getLocation());
                        local.d(a2, sb.toString());
                        WeatherSettings k2 = this.this$0.k();
                        if (k2 == null) {
                            wg6.a();
                            throw null;
                        } else if (!k2.isUseCurrentLocation()) {
                            this.this$0.l();
                        } else {
                            Calendar instance = Calendar.getInstance();
                            wg6.a((Object) instance, "Calendar.getInstance()");
                            long timeInMillis = instance.getTimeInMillis();
                            WeatherSettings k3 = this.this$0.k();
                            if (k3 == null) {
                                wg6.a();
                                throw null;
                            } else if (timeInMillis - k3.getUpdatedAt() < ((long) this.this$0.g)) {
                                ComplicationWeatherService complicationWeatherService = this.this$0;
                                WeatherSettings k4 = complicationWeatherService.k();
                                if (k4 != null) {
                                    complicationWeatherService.a(k4, true);
                                    this.this$0.a();
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                LocationSource e = this.this$0.e();
                                Object h = this.this$0.h();
                                this.L$0 = il6;
                                this.label = 1;
                                obj = e.getLocation(h, false, this);
                                if (obj == a) {
                                    return a;
                                }
                            }
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                return cd6.a;
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            LocationSource.Result result = (LocationSource.Result) obj;
            if (result.getErrorState() == LocationSource.ErrorState.SUCCESS) {
                Location location = result.getLocation();
                WeatherSettings k5 = this.this$0.k();
                if (k5 == null) {
                    wg6.a();
                    throw null;
                } else if (location != null) {
                    k5.setLatLng(new LatLng(location.getLatitude(), location.getLongitude()));
                    this.this$0.l();
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                this.this$0.a(result.getErrorState());
                this.this$0.c(result.getErrorState());
                FLogger.INSTANCE.getLocal().d(wp4.f.a(), "getWeatherBaseOnLocation - using current location but location is null, stop service.");
                this.this$0.a();
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService$showChanceOfRain$1", f = "ComplicationWeatherService.kt", l = {}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $rainProbabilityPercent;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationWeatherService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ComplicationWeatherService complicationWeatherService, int i, xe6 xe6) {
            super(2, xe6);
            this.this$0 = complicationWeatherService;
            this.$rainProbabilityPercent = i;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$rainProbabilityPercent, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.g().upsertCustomizeRealData(new CustomizeRealData("chance_of_rain", String.valueOf(this.$rainProbabilityPercent)));
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public final void c(LocationSource.ErrorState errorState) {
        a<String> aVar = this.u;
        if (aVar != null) {
            String b2 = aVar.b();
            if (b2 == null) {
                return;
            }
            if (wg6.a((Object) b2, (Object) "TEMPERATURE")) {
                jl4 c2 = AnalyticsHelper.f.c("weather");
                if (c2 != null) {
                    nh6 nh6 = nh6.a;
                    Object[] objArr = {"weather"};
                    String format = String.format("update_%s_optional_error", Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    hl4 a2 = AnalyticsHelper.f.a(format);
                    a2.a("error_code", b(errorState));
                    c2.a(a2);
                    return;
                }
                return;
            }
            jl4 c3 = AnalyticsHelper.f.c("chance-of-rain");
            if (c3 != null) {
                nh6 nh62 = nh6.a;
                Object[] objArr2 = {AnalyticsHelper.f.d("chance-of-rain")};
                String format2 = String.format("update_%s_optional_error", Arrays.copyOf(objArr2, objArr2.length));
                wg6.a((Object) format2, "java.lang.String.format(format, *args)");
                hl4 a3 = AnalyticsHelper.f.a(format2);
                a3.a("error_code", b(errorState));
                c3.a(a3);
                return;
            }
            return;
        }
        wg6.d("mWeatherTasks");
        throw null;
    }

    @DexIgnore
    public final CustomizeRealDataRepository g() {
        CustomizeRealDataRepository customizeRealDataRepository = this.p;
        if (customizeRealDataRepository != null) {
            return customizeRealDataRepository;
        }
        wg6.d("mCustomizeRealDataRepository");
        throw null;
    }

    @DexIgnore
    public final PortfolioApp h() {
        PortfolioApp portfolioApp = this.o;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        wg6.d("mPortfolioApp");
        throw null;
    }

    @DexIgnore
    public final an4 i() {
        an4 an4 = this.j;
        if (an4 != null) {
            return an4;
        }
        wg6.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final Weather j() {
        return this.t;
    }

    @DexIgnore
    public final WeatherSettings k() {
        return this.s;
    }

    @DexIgnore
    public final void l() {
        z24 z24 = this.i;
        if (z24 != null) {
            GetWeather getWeather = this.h;
            if (getWeather != null) {
                WeatherSettings weatherSettings = this.s;
                if (weatherSettings != null) {
                    LatLng latLng = weatherSettings.getLatLng();
                    WeatherSettings weatherSettings2 = this.s;
                    if (weatherSettings2 != null) {
                        z24.a(getWeather, new GetWeather.c(latLng, weatherSettings2.getTempUnit()), new b(this));
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.d("mGetWeather");
                throw null;
            }
        } else {
            wg6.d("mUseCaseHandler");
            throw null;
        }
    }

    @DexIgnore
    public final rm6 m() {
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        wg6.b(intent, "intent");
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d(wp4.f.a(), "onCreate");
        PortfolioApp.get.instance().g().a(this);
        this.r = PortfolioApp.get.instance().e();
        this.u = new a<>();
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(wp4.f.a(), "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        String string;
        wg6.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(wp4.f.a(), "onStartCommand");
        this.a = Action.MicroAppAction.SHOW_TEMPERATURE;
        super.d();
        Bundle extras = intent.getExtras();
        if (extras == null || (string = extras.getString("action")) == null) {
            return 2;
        }
        a<String> aVar = this.u;
        if (aVar == null) {
            wg6.d("mWeatherTasks");
            throw null;
        } else if (aVar.a()) {
            a<String> aVar2 = this.u;
            if (aVar2 != null) {
                aVar2.a(string);
                an4 an4 = this.j;
                if (an4 != null) {
                    String m = an4.m();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = wp4.f.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Inside SteamingAction .run - WEATHER_ACTION json ");
                    if (m != null) {
                        sb.append(m);
                        local.d(a2, sb.toString());
                        this.s = (WeatherSettings) new Gson().a(m, WeatherSettings.class);
                        m();
                        return 2;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mSharedPreferencesManager");
                throw null;
            }
            wg6.d("mWeatherTasks");
            throw null;
        } else {
            a<String> aVar3 = this.u;
            if (aVar3 != null) {
                aVar3.a(string);
                return 2;
            }
            wg6.d("mWeatherTasks");
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ HashSet<T> a; // = new HashSet<>();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final void a(T t) {
            synchronized (this.b) {
                this.a.add(t);
            }
        }

        @DexIgnore
        public final void b(T t) {
            synchronized (this.b) {
                this.a.remove(t);
            }
        }

        @DexIgnore
        public final boolean a() {
            boolean isEmpty;
            synchronized (this.b) {
                isEmpty = this.a.isEmpty();
            }
            return isEmpty;
        }

        @DexIgnore
        public final T b() {
            T next;
            synchronized (this.b) {
                Iterator<T> it = this.a.iterator();
                wg6.a((Object) it, "hashSet.iterator()");
                next = it.hasNext() ? it.next() : null;
            }
            return next;
        }
    }

    @DexIgnore
    public final void a(Weather weather) {
        this.t = weather;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x012e  */
    public final void b(WeatherSettings weatherSettings, boolean z) {
        lc6 lc6;
        int i2;
        WeatherComplicationAppInfo.TemperatureUnit temperatureUnit;
        String str;
        UserRepository userRepository = this.q;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            if (currentUser != null) {
                zh4 temperatureUnit2 = currentUser.getTemperatureUnit();
                if (temperatureUnit2 != null) {
                    int i3 = yp4.a[temperatureUnit2.ordinal()];
                    if (i3 == 1) {
                        WeatherSettings.TEMP_UNIT temp_unit = WeatherSettings.TEMP_UNIT.CELSIUS;
                        lc6 = new lc6(temp_unit, Float.valueOf(weatherSettings.getTemperatureIn(temp_unit)));
                    } else if (i3 == 2) {
                        WeatherSettings.TEMP_UNIT temp_unit2 = WeatherSettings.TEMP_UNIT.FAHRENHEIT;
                        lc6 = new lc6(temp_unit2, Float.valueOf(weatherSettings.getTemperatureIn(temp_unit2)));
                    }
                    WeatherSettings.TEMP_UNIT temp_unit3 = (WeatherSettings.TEMP_UNIT) lc6.component1();
                    float floatValue = ((Number) lc6.component2()).floatValue();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = wp4.f.a();
                    local.d(a2, "showTemperature - forecast=" + weatherSettings.getForecast() + ", temperature=" + floatValue + ", temperatureUnit=" + temp_unit3);
                    WeatherComplicationAppInfo.WeatherCondition a3 = bi4.Companion.a(weatherSettings.getForecast());
                    i2 = yp4.b[temp_unit3.ordinal()];
                    if (i2 != 1) {
                        temperatureUnit = WeatherComplicationAppInfo.TemperatureUnit.C;
                    } else if (i2 == 2) {
                        temperatureUnit = WeatherComplicationAppInfo.TemperatureUnit.F;
                    } else {
                        throw new kc6();
                    }
                    WeatherComplicationAppInfo weatherComplicationAppInfo = new WeatherComplicationAppInfo(floatValue, temperatureUnit, a3, 3600);
                    PortfolioApp instance = PortfolioApp.get.instance();
                    str = this.r;
                    if (str == null) {
                        instance.a((DeviceAppResponse) weatherComplicationAppInfo, str);
                        jl4 c2 = AnalyticsHelper.f.c("weather");
                        if (c2 != null) {
                            String str2 = this.r;
                            if (str2 != null) {
                                c2.a(str2, z, "");
                            } else {
                                wg6.d("mSerial");
                                throw null;
                            }
                        }
                        AnalyticsHelper.f.e("weather");
                        CustomizeRealData customizeRealData = new CustomizeRealData("temperature", String.valueOf(weatherSettings.getTemperatureIn(WeatherSettings.TEMP_UNIT.CELSIUS)));
                        CustomizeRealDataRepository customizeRealDataRepository = this.p;
                        if (customizeRealDataRepository != null) {
                            customizeRealDataRepository.upsertCustomizeRealData(customizeRealData);
                            return;
                        } else {
                            wg6.d("mCustomizeRealDataRepository");
                            throw null;
                        }
                    } else {
                        wg6.d("mSerial");
                        throw null;
                    }
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a4 = wp4.f.a();
                local2.e(a4, "showTemperature - temperature unit is not found, " + currentUser.getTemperatureUnit());
                lc6 = new lc6(weatherSettings.getTempUnit(), Float.valueOf(weatherSettings.getTemperature()));
                WeatherSettings.TEMP_UNIT temp_unit32 = (WeatherSettings.TEMP_UNIT) lc6.component1();
                float floatValue2 = ((Number) lc6.component2()).floatValue();
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a22 = wp4.f.a();
                local3.d(a22, "showTemperature - forecast=" + weatherSettings.getForecast() + ", temperature=" + floatValue2 + ", temperatureUnit=" + temp_unit32);
                WeatherComplicationAppInfo.WeatherCondition a32 = bi4.Companion.a(weatherSettings.getForecast());
                i2 = yp4.b[temp_unit32.ordinal()];
                if (i2 != 1) {
                }
                WeatherComplicationAppInfo weatherComplicationAppInfo2 = new WeatherComplicationAppInfo(floatValue2, temperatureUnit, a32, 3600);
                PortfolioApp instance2 = PortfolioApp.get.instance();
                str = this.r;
                if (str == null) {
                }
            } else {
                FLogger.INSTANCE.getLocal().e(wp4.f.a(), "showTemperature - do not send temperature, no user info");
            }
        } else {
            wg6.d("mUserRepository");
            throw null;
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(ComplicationWeatherService complicationWeatherService, WeatherSettings weatherSettings, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        complicationWeatherService.a(weatherSettings, z);
    }

    @DexIgnore
    public final synchronized void a(WeatherSettings weatherSettings, boolean z) {
        wg6.b(weatherSettings, "weatherSettings");
        while (true) {
            a<String> aVar = this.u;
            if (aVar == null) {
                wg6.d("mWeatherTasks");
                throw null;
            } else if (!aVar.a()) {
                a<String> aVar2 = this.u;
                if (aVar2 != null) {
                    String b2 = aVar2.b();
                    if (b2 != null) {
                        if (wg6.a((Object) b2, (Object) "TEMPERATURE")) {
                            b(weatherSettings, z);
                        } else if (wg6.a((Object) b2, (Object) "RAIN_CHANCE")) {
                            a(weatherSettings.getRainProbability(), z);
                        }
                        a<String> aVar3 = this.u;
                        if (aVar3 != null) {
                            aVar3.b(b2);
                        } else {
                            wg6.d("mWeatherTasks");
                            throw null;
                        }
                    }
                } else {
                    wg6.d("mWeatherTasks");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void a(float f, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = wp4.f.a();
        local.d(a2, "showChanceOfRain - probability=" + f);
        int i2 = (int) (f * ((float) 100));
        ChanceOfRainComplicationAppInfo chanceOfRainComplicationAppInfo = new ChanceOfRainComplicationAppInfo(i2, 3600);
        PortfolioApp instance = PortfolioApp.get.instance();
        String str = this.r;
        if (str != null) {
            instance.a((DeviceAppResponse) chanceOfRainComplicationAppInfo, str);
            jl4 c2 = AnalyticsHelper.f.c("chance-of-rain");
            if (c2 != null) {
                String str2 = this.r;
                if (str2 != null) {
                    c2.a(str2, z, "");
                } else {
                    wg6.d("mSerial");
                    throw null;
                }
            }
            AnalyticsHelper.f.e("chance-of-rain");
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new d(this, i2, (xe6) null), 3, (Object) null);
            return;
        }
        wg6.d("mSerial");
        throw null;
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "errorMessage");
        a<String> aVar = this.u;
        if (aVar != null) {
            String b2 = aVar.b();
            if (b2 == null) {
                return;
            }
            if (wg6.a((Object) b2, (Object) "TEMPERATURE")) {
                jl4 c2 = AnalyticsHelper.f.c("weather");
                if (c2 != null) {
                    nh6 nh6 = nh6.a;
                    Object[] objArr = {"weather"};
                    String format = String.format("update_%s_optional_error", Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    hl4 a2 = AnalyticsHelper.f.a(format);
                    a2.a("error_code", str);
                    c2.a(a2);
                    return;
                }
                return;
            }
            jl4 c3 = AnalyticsHelper.f.c("chance-of-rain");
            if (c3 != null) {
                nh6 nh62 = nh6.a;
                Object[] objArr2 = {AnalyticsHelper.f.d("chance-of-rain")};
                String format2 = String.format("update_%s_optional_error", Arrays.copyOf(objArr2, objArr2.length));
                wg6.a((Object) format2, "java.lang.String.format(format, *args)");
                hl4 a3 = AnalyticsHelper.f.a(format2);
                a3.a("error_code", str);
                c3.a(a3);
                return;
            }
            return;
        }
        wg6.d("mWeatherTasks");
        throw null;
    }

    @DexIgnore
    public final String b(LocationSource.ErrorState errorState) {
        int i2 = yp4.c[errorState.ordinal()];
        if (i2 == 1) {
            return xh4.LOCATION_ACCESS_DISABLED.getCode();
        }
        if (i2 == 2) {
            return xh4.BACKGROUND_LOCATION_ACCESS_DISABLED.getCode();
        }
        if (i2 != 3) {
            return xh4.UNKNOWN.getCode();
        }
        return xh4.LOCATION_SERVICE_DISABLED.getCode();
    }

    @DexIgnore
    public void b() {
        FLogger.INSTANCE.getLocal().d(wp4.f.a(), "forceStop");
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d(wp4.f.a(), "finish");
        super.a();
        stopSelf();
    }
}
