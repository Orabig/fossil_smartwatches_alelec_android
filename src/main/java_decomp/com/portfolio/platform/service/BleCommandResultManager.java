package com.portfolio.platform.service;

import android.content.Intent;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.op4;
import com.fossil.op4$e$a;
import com.fossil.qd6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleCommandResultManager {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static op4.c<CommunicateMode, op4.a> b; // = new c();
    @DexIgnore
    public static /* final */ HashMap<CommunicateMode, op4.d<op4.b>> c; // = new HashMap<>();
    @DexIgnore
    public static /* final */ BleCommandResultManager d; // = new BleCommandResultManager();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Intent a;

        @DexIgnore
        public a(CommunicateMode communicateMode, String str, Intent intent) {
            wg6.b(communicateMode, "mode");
            wg6.b(intent, "intent");
            this.a = intent;
        }

        @DexIgnore
        public final Intent a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(CommunicateMode communicateMode, Intent intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1", f = "BleCommandResultManager.kt", l = {69}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $communicateModes;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(List list, xe6 xe6) {
            super(2, xe6);
            this.$communicateModes = list;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.$communicateModes, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x011a  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x004b  */
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Iterable iterable;
            Iterator it;
            Object obj2;
            e eVar;
            CommunicateMode communicateMode;
            Object obj3;
            a aVar;
            d dVar;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                List list = this.$communicateModes;
                it = list.iterator();
                il6 = il62;
                iterable = list;
                obj2 = a;
                eVar = this;
            } else if (i == 1) {
                d dVar2 = (d) this.L$6;
                aVar = (a) this.L$5;
                it = (Iterator) this.L$2;
                iterable = (Iterable) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                obj3 = a;
                communicateMode = (CommunicateMode) this.L$4;
                eVar = this;
                BleCommandResultManager.b.a(communicateMode, aVar);
                obj2 = obj3;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!it.hasNext()) {
                Object next = it.next();
                communicateMode = (CommunicateMode) next;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String c = BleCommandResultManager.a;
                local.d(c, "retrieve mode=" + communicateMode);
                a aVar2 = (a) BleCommandResultManager.b.a(communicateMode);
                if (aVar2 != null && (dVar = (d) BleCommandResultManager.c.get(communicateMode)) != null && dVar.b() > 0) {
                    if (communicateMode == CommunicateMode.SYNC) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String c2 = BleCommandResultManager.a;
                        local2.d(c2, "SyncMode=" + aVar2.a().getIntExtra("sync_result", 3));
                    }
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String c3 = BleCommandResultManager.a;
                    local3.d(c3, "retrieve mode success=" + dVar.b());
                    cn6 c4 = zl6.c();
                    op4$e$a op4_e_a = new op4$e$a(dVar, communicateMode, aVar2, (xe6) null);
                    eVar.L$0 = il6;
                    eVar.L$1 = iterable;
                    eVar.L$2 = it;
                    eVar.L$3 = next;
                    eVar.L$4 = communicateMode;
                    eVar.L$5 = aVar2;
                    eVar.L$6 = dVar;
                    eVar.label = 1;
                    if (gk6.a(c4, op4_e_a, eVar) == obj2) {
                        return obj2;
                    }
                    obj3 = obj2;
                    aVar = aVar2;
                    BleCommandResultManager.b.a(communicateMode, aVar);
                    obj2 = obj3;
                    if (!it.hasNext()) {
                    }
                    return obj2;
                } else if (!it.hasNext()) {
                }
                if (!it.hasNext()) {
                }
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = BleCommandResultManager.class.getSimpleName();
        wg6.a((Object) simpleName, "BleCommandResultManager::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final synchronized void a(CommunicateMode communicateMode, a aVar) {
        wg6.b(communicateMode, "mode");
        wg6.b(aVar, "bleResult");
        b.b(communicateMode, aVar);
        a(communicateMode);
    }

    @DexIgnore
    public final void b(b bVar, CommunicateMode... communicateModeArr) {
        wg6.b(bVar, "observerReceiver");
        wg6.b(communicateModeArr, "communicateModes");
        b((op4.b) bVar, (List<? extends CommunicateMode>) qd6.d((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> {
        @DexIgnore
        public /* final */ List<T> a; // = new ArrayList();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final void a(T t) {
            synchronized (this.b) {
                this.a.add(t);
            }
        }

        @DexIgnore
        public final void b(T t) {
            synchronized (this.b) {
                this.a.remove(t);
            }
        }

        @DexIgnore
        public final List<T> a() {
            ArrayList arrayList;
            synchronized (this.b) {
                arrayList = new ArrayList();
                arrayList.addAll(this.a);
            }
            return arrayList;
        }

        @DexIgnore
        public final int b() {
            return this.a.size();
        }
    }

    @DexIgnore
    public final synchronized void b(op4.b bVar, List<? extends CommunicateMode> list) {
        wg6.b(bVar, "observerReceiver");
        wg6.b(list, "communicateModes");
        for (CommunicateMode communicateMode : list) {
            d dVar = c.get(communicateMode);
            if (dVar != null) {
                dVar.b(bVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T, V> {
        @DexIgnore
        public /* final */ HashMap<T, V> a; // = new HashMap<>();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final boolean a(T t, V v) {
            boolean z;
            synchronized (this.b) {
                if (wg6.a((Object) this.a.get(t), (Object) v)) {
                    this.a.remove(t);
                    z = true;
                } else {
                    z = false;
                }
            }
            return z;
        }

        @DexIgnore
        public final void b(T t, V v) {
            synchronized (this.b) {
                this.a.put(t, v);
                cd6 cd6 = cd6.a;
            }
        }

        @DexIgnore
        public String toString() {
            String hashMap = this.a.toString();
            wg6.a((Object) hashMap, "mHashMap.toString()");
            return hashMap;
        }

        @DexIgnore
        public final V a(T t) {
            return this.a.get(t);
        }
    }

    @DexIgnore
    public final void a(b bVar, CommunicateMode... communicateModeArr) {
        wg6.b(bVar, "observerReceiver");
        wg6.b(communicateModeArr, "communicateModes");
        a((op4.b) bVar, (List<? extends CommunicateMode>) qd6.d((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final void a(CommunicateMode... communicateModeArr) {
        wg6.b(communicateModeArr, "communicateModes");
        a((List<? extends CommunicateMode>) qd6.d((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final synchronized void a(List<? extends CommunicateMode> list) {
        wg6.b(list, "communicateModes");
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new e(list, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final synchronized void a(op4.b bVar, List<? extends CommunicateMode> list) {
        wg6.b(bVar, "observerReceiver");
        wg6.b(list, "communicateModes");
        for (CommunicateMode communicateMode : list) {
            d dVar = c.get(communicateMode);
            if (dVar != null) {
                dVar.a(bVar);
            } else {
                HashMap<CommunicateMode, op4.d<op4.b>> hashMap = c;
                d dVar2 = new d();
                dVar2.a(bVar);
                hashMap.put(communicateMode, dVar2);
            }
        }
    }
}
