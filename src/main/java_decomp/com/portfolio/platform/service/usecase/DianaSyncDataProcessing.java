package com.portfolio.platform.service.usecase;

import com.fossil.SyncDataExtensions;
import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hg6;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jh6;
import com.fossil.jk4;
import com.fossil.jl6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.nq4;
import com.fossil.nq4$f$a;
import com.fossil.nq4$g$a;
import com.fossil.pc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rd6;
import com.fossil.rh6;
import com.fossil.rl6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.ue6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xg6;
import com.fossil.xj6;
import com.fossil.yd6;
import com.fossil.yi4;
import com.fossil.zl6;
import com.fossil.zo4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.RestingWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaSyncDataProcessing {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ DianaSyncDataProcessing b; // = new DianaSyncDataProcessing();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ List<ActivitySample> b;
        @DexIgnore
        public /* final */ List<ActivitySummary> c;
        @DexIgnore
        public /* final */ List<MFSleepSession> d;
        @DexIgnore
        public /* final */ List<HeartRateSample> e;
        @DexIgnore
        public /* final */ List<DailyHeartRateSummary> f;
        @DexIgnore
        public /* final */ List<lc6<Long, Long>> g;
        @DexIgnore
        public /* final */ List<WorkoutSession> h;
        @DexIgnore
        public /* final */ List<GFitSample> i;
        @DexIgnore
        public /* final */ List<GFitHeartRate> j;
        @DexIgnore
        public /* final */ List<GFitWorkoutSession> k;

        @DexIgnore
        public a(long j2, List<ActivitySample> list, List<ActivitySummary> list2, List<MFSleepSession> list3, List<HeartRateSample> list4, List<DailyHeartRateSummary> list5, List<lc6<Long, Long>> list6, List<WorkoutSession> list7, List<GFitSample> list8, List<GFitHeartRate> list9, List<GFitWorkoutSession> list10) {
            wg6.b(list, "sampleRawList");
            wg6.b(list2, "summaryList");
            wg6.b(list3, "sleepSessionList");
            wg6.b(list4, "heartRateDataList");
            wg6.b(list5, "heartRateSummaryList");
            wg6.b(list6, "activeTimeList");
            wg6.b(list7, "workoutSessionList");
            wg6.b(list8, "gFitSampleList");
            wg6.b(list9, "gFitHeartRateList");
            wg6.b(list10, "gFitWorkoutSessionList");
            this.a = j2;
            this.b = list;
            this.c = list2;
            this.d = list3;
            this.e = list4;
            this.f = list5;
            this.g = list6;
            this.h = list7;
            this.i = list8;
            this.j = list9;
            this.k = list10;
        }

        @DexIgnore
        public final List<lc6<Long, Long>> a() {
            return this.g;
        }

        @DexIgnore
        public final List<GFitHeartRate> b() {
            return this.j;
        }

        @DexIgnore
        public final List<GFitSample> c() {
            return this.i;
        }

        @DexIgnore
        public final List<GFitWorkoutSession> d() {
            return this.k;
        }

        @DexIgnore
        public final List<HeartRateSample> e() {
            return this.e;
        }

        @DexIgnore
        public final List<DailyHeartRateSummary> f() {
            return this.f;
        }

        @DexIgnore
        public final long g() {
            return this.a;
        }

        @DexIgnore
        public final List<ActivitySample> h() {
            return this.b;
        }

        @DexIgnore
        public final List<MFSleepSession> i() {
            return this.d;
        }

        @DexIgnore
        public final List<ActivitySummary> j() {
            return this.c;
        }

        @DexIgnore
        public final List<WorkoutSession> k() {
            return this.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Long.valueOf(((FitnessDataWrapper) t).getStartLongTime()), Long.valueOf(((FitnessDataWrapper) t2).getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements hg6<FitnessDataWrapper, Integer> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Integer.valueOf(invoke((FitnessDataWrapper) obj));
        }

        @DexIgnore
        public final int invoke(FitnessDataWrapper fitnessDataWrapper) {
            wg6.b(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getTimezoneOffsetInSecond();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends xg6 implements hg6<FitnessDataWrapper, Long> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Long.valueOf(invoke((FitnessDataWrapper) obj));
        }

        @DexIgnore
        public final long invoke(FitnessDataWrapper fitnessDataWrapper) {
            wg6.b(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getStartLongTime();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Long.valueOf(((FitnessDataWrapper) t).getStartLongTime()), Long.valueOf(((FitnessDataWrapper) t2).getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2", f = "DianaSyncDataProcessing.kt", l = {186}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super rm6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $finalResult;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository $thirdPartyRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(a aVar, ThirdPartyRepository thirdPartyRepository, xe6 xe6) {
            super(2, xe6);
            this.$finalResult = aVar;
            this.$thirdPartyRepository = thirdPartyRepository;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.$finalResult, this.$thirdPartyRepository, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                List<GFitSample> c = this.$finalResult.c();
                ArrayList arrayList = new ArrayList();
                for (ActivitySample next : this.$finalResult.h()) {
                    DateTime component3 = next.component3();
                    next.component4();
                    double component5 = next.component5();
                    arrayList.add(new UASample(rh6.a(component5), next.component7(), next.component6(), component3.getMillis() / ((long) 1000)));
                }
                jh6 jh6 = new jh6();
                jh6.element = null;
                List<lc6<Long, Long>> a2 = this.$finalResult.a();
                ArrayList arrayList2 = new ArrayList(rd6.a(a2, 10));
                for (lc6 lc6 : a2) {
                    arrayList2.add(qd6.c((Long) lc6.getFirst(), (Long) lc6.getSecond()));
                }
                List m = yd6.m(rd6.a(arrayList2));
                if (!m.isEmpty()) {
                    jh6.element = new GFitActiveTime(m);
                }
                List<MFSleepSession> i2 = this.$finalResult.i();
                List<GFitHeartRate> b = this.$finalResult.b();
                List<GFitWorkoutSession> d = this.$finalResult.d();
                nq4$f$a nq4_f_a = r0;
                dl6 b2 = zl6.b();
                List<GFitWorkoutSession> list = d;
                nq4$f$a nq4_f_a2 = new nq4$f$a(this, c, arrayList, jh6, b, list, i2, (xe6) null);
                this.L$0 = il6;
                this.L$1 = c;
                this.L$2 = arrayList;
                this.L$3 = jh6;
                this.L$4 = m;
                this.L$5 = i2;
                this.L$6 = b;
                this.L$7 = list;
                this.label = 1;
                Object a3 = gk6.a(b2, nq4_f_a, this);
                Object obj2 = a;
                return a3 == obj2 ? obj2 : a3;
            } else if (i == 1) {
                List list2 = (List) this.L$7;
                List list3 = (List) this.L$6;
                List list4 = (List) this.L$5;
                List list5 = (List) this.L$4;
                jh6 jh62 = (jh6) this.L$3;
                List list6 = (List) this.L$2;
                List list7 = (List) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3", f = "DianaSyncDataProcessing.kt", l = {238, 244}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository $activityRepository;
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDataRepository $fitnessDataRepository;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSampleRepository $heartRateSampleRepository;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryRepository $heartRateSummaryRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository $sleepSessionsRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository $sleepSummariesRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository $summaryRepository;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionRepository $workoutSessionRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(FitnessDataRepository fitnessDataRepository, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, xe6 xe6) {
            super(2, xe6);
            this.$fitnessDataRepository = fitnessDataRepository;
            this.$activityRepository = activitiesRepository;
            this.$summaryRepository = summariesRepository;
            this.$sleepSessionsRepository = sleepSessionsRepository;
            this.$sleepSummariesRepository = sleepSummariesRepository;
            this.$heartRateSampleRepository = heartRateSampleRepository;
            this.$heartRateSummaryRepository = heartRateSummaryRepository;
            this.$workoutSessionRepository = workoutSessionRepository;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.$fitnessDataRepository, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, this.$heartRateSampleRepository, this.$heartRateSummaryRepository, this.$workoutSessionRepository, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                FitnessDataRepository fitnessDataRepository = this.$fitnessDataRepository;
                this.L$0 = il6;
                this.label = 1;
                obj = fitnessDataRepository.pushPendingFitnessData(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                List list = (List) this.L$2;
                ap4 ap4 = (ap4) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ap4 ap42 = (ap4) obj;
            if (ap42 instanceof cp4) {
                FLogger.INSTANCE.getLocal().d(DianaSyncDataProcessing.a, "push fitness data file success, get updated summary from server");
                List list2 = (List) ((cp4) ap42).a();
                if (list2 != null && (true ^ list2.isEmpty())) {
                    dl6 a2 = zl6.a();
                    nq4$g$a nq4_g_a = new nq4$g$a(this, list2, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = ap42;
                    this.L$2 = list2;
                    this.label = 2;
                    if (gk6.a(a2, nq4_g_a, this) == a) {
                        return a;
                    }
                }
            } else {
                boolean z = ap42 instanceof zo4;
            }
            return cd6.a;
        }
    }

    /*
    static {
        String name = DianaSyncDataProcessing.class.getName();
        wg6.a((Object) name, "DianaSyncDataProcessing::class.java.name");
        a = name;
    }
    */

    @DexIgnore
    public final nq4.a b(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        String str2 = str;
        List<FitnessDataWrapper> list2 = list;
        portfolioApp.a(CommunicateMode.SYNC, str2, "Calculating sleep and activity...");
        String userId = mFUser.getUserId();
        wg6.a((Object) userId, ButtonService.USER_ID);
        pc6<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> a2 = SyncDataExtensions.a(list2, str2, userId, 1000 * j);
        List<ActivitySample> first = a2.getFirst();
        List second = a2.getSecond();
        List third = a2.getThird();
        List<MFSleepSession> a3 = SyncDataExtensions.a(list2, str2);
        lc6<List<HeartRateSample>, List<DailyHeartRateSummary>> a4 = a(list2, userId);
        List first2 = a4.getFirst();
        List second2 = a4.getSecond();
        List<WorkoutSession> a5 = a(list2, str2, userId);
        List<GFitHeartRate> a6 = a(list2);
        List<GFitWorkoutSession> b2 = b(list2);
        int size = a3.size();
        double d2 = 0.0d;
        double d3 = 0.0d;
        double d4 = 0.0d;
        double d5 = 0.0d;
        int i = 0;
        int i2 = 0;
        for (ActivitySample activitySample : first) {
            d4 += activitySample.getCalories();
            d5 += activitySample.getDistance();
            d2 += activitySample.getSteps();
            i += activitySample.getActiveTime();
            Boolean t = bk4.t(activitySample.getDate());
            wg6.a((Object) t, "DateHelper.isToday(it.date)");
            if (t.booleanValue()) {
                d3 += activitySample.getSteps();
                i2 += activitySample.getActiveTime();
            }
            List<FitnessDataWrapper> list3 = list;
        }
        nh6 nh6 = nh6.a;
        Object[] objArr = {String.valueOf(size)};
        String format = String.format("Done calculating sleep: total %s sleep session(s)", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        a(str2, format);
        nh6 nh62 = nh6.a;
        Object[] objArr2 = {Double.valueOf(d2), Double.valueOf(d3), Long.valueOf(j2), Long.valueOf(userProfile.getCurrentSteps()), Double.valueOf(d4), Double.valueOf(d5), Integer.valueOf(i), Integer.valueOf(i2)};
        String format2 = String.format("After calculation: steps=%s, todayStep=%s, realTimeSteps=%s, lastRealtimeSteps=%s, Calories=%s, DistanceWrapper=%s, ActiveTime=%s, TodayActiveTime=%s", Arrays.copyOf(objArr2, objArr2.length));
        wg6.a((Object) format2, "java.lang.String.format(format, *args)");
        a(str2, format2);
        a(str2, "HeartRateWrapper data size: " + first2.size());
        FLogger.INSTANCE.getLocal().d(a, "Release=" + jk4.a());
        if (!jk4.a()) {
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Sleep sessions details: " + new Gson().a(a3));
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - SampleRaw list details: " + new Gson().a(first));
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Heart Rate sample list details: " + first2);
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Heart Rate summary list details: " + second2);
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Workout list details: " + new Gson().a(a5));
        }
        return new a(j2, first, second, a3, first2, second2, SyncDataExtensions.a(list), a5, third, a6, b2);
    }

    @DexIgnore
    public final nq4.a a(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        String str2 = str;
        List<FitnessDataWrapper> list2 = list;
        wg6.b(str2, "serial");
        wg6.b(list2, "syncData");
        wg6.b(mFUser, "user");
        wg6.b(userProfile, "userProfile");
        wg6.b(portfolioApp, "portfolioApp");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a;
        local.d(str3, ".buildSyncResult(), get all data files, synctime=" + j + ", data=" + list2);
        a aVar = new a(j2, new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
        if (list.isEmpty()) {
            a(str2, "Sync data is empty");
            return aVar;
        }
        return b(str, list, mFUser, userProfile, j, j2, portfolioApp);
    }

    @DexIgnore
    public final void a(String str, String str2) {
        PortfolioApp.get.instance().a(CommunicateMode.SYNC, str, str2);
        FLogger.INSTANCE.getLocal().d(a, str2);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str, a, str2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01a7  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01b2  */
    public final Object a(nq4.a aVar, String str, SleepSessionsRepository sleepSessionsRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataRepository fitnessDataRepository, ActivitiesRepository activitiesRepository, ThirdPartyRepository thirdPartyRepository, PortfolioApp portfolioApp, ik4 ik4, xe6<? super cd6> xe6) {
        List<HeartRateSample> e2;
        List<WorkoutSession> k;
        String str2 = str;
        String str3 = "Save sync result - size of sleepSessions=" + aVar.i().size() + ", size of sampleRaws=" + aVar.h().size() + ", realTimeSteps=" + aVar.g();
        FLogger.INSTANCE.getLocal().i(a, str3);
        a(str2, str3);
        if (xj6.a(PortfolioApp.get.instance().e()) || !xj6.b(PortfolioApp.get.instance().e(), str2, true)) {
            FLogger.INSTANCE.getLocal().e(a, "Error inside " + a + ".saveSyncResult - Sync data does not match any user's device");
            return cd6.a;
        }
        a(str2, "Saving sleep data");
        try {
            try {
                sleepSessionsRepository.insertFromDevice(aVar.i());
            } catch (Exception e3) {
                e = e3;
            }
        } catch (Exception e4) {
            e = e4;
            SleepSessionsRepository sleepSessionsRepository2 = sleepSessionsRepository;
            a(str2, "Saving sleep data. error=" + e.getMessage());
            a(str2, "Saving sleep data. OK");
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Upload sleep sessions to server");
            a(str2, "Saving activity data");
            rl6 unused = ik6.a(jl6.a(zl6.a()), (af6) null, (ll6) null, new f(aVar, thirdPartyRepository, (xe6) null), 3, (Object) null);
            ik4.a(new Date(), aVar.g());
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartbeat step by syncing");
            try {
                activitiesRepository.insertFromDevice(aVar.h());
                a(str2, "Saving activity data. OK");
            } catch (Exception e5) {
                e = e5;
            }
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update activities summary by syncing");
            summariesRepository.insertFromDevice(aVar.j());
            a(str2, "Saving activity summaries data. OK");
            e2 = aVar.e();
            if (!(!e2.isEmpty())) {
            }
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartrate summary by syncing");
            try {
                heartRateSummaryRepository.insertFromDevice(aVar.f());
                a(str2, "Saving heartrate summaries data. OK");
            } catch (Exception e6) {
                e = e6;
            }
            k = aVar.k();
            if (true ^ k.isEmpty()) {
            }
            rl6 unused2 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new g(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (xe6) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
            portfolioApp.b(str2);
            return cd6.a;
        }
        a(str2, "Saving sleep data. OK");
        FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Upload sleep sessions to server");
        a(str2, "Saving activity data");
        rl6 unused3 = ik6.a(jl6.a(zl6.a()), (af6) null, (ll6) null, new f(aVar, thirdPartyRepository, (xe6) null), 3, (Object) null);
        ik4.a(new Date(), aVar.g());
        FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartbeat step by syncing");
        try {
            activitiesRepository.insertFromDevice(aVar.h());
            a(str2, "Saving activity data. OK");
        } catch (Exception e7) {
            e = e7;
            ActivitiesRepository activitiesRepository2 = activitiesRepository;
            a(str2, "Saving activity data. error=" + e.getMessage());
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update activities summary by syncing");
            summariesRepository.insertFromDevice(aVar.j());
            a(str2, "Saving activity summaries data. OK");
            e2 = aVar.e();
            if (!(!e2.isEmpty())) {
            }
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartrate summary by syncing");
            heartRateSummaryRepository.insertFromDevice(aVar.f());
            a(str2, "Saving heartrate summaries data. OK");
            k = aVar.k();
            if (true ^ k.isEmpty()) {
            }
            rl6 unused4 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new g(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (xe6) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
            portfolioApp.b(str2);
            return cd6.a;
        }
        FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update activities summary by syncing");
        try {
            try {
                summariesRepository.insertFromDevice(aVar.j());
                a(str2, "Saving activity summaries data. OK");
            } catch (Exception e8) {
                e = e8;
            }
        } catch (Exception e9) {
            e = e9;
            SummariesRepository summariesRepository2 = summariesRepository;
            a(str2, "Saving activity summaries data. error=" + e.getMessage());
            e2 = aVar.e();
            if (!(!e2.isEmpty())) {
            }
            FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartrate summary by syncing");
            heartRateSummaryRepository.insertFromDevice(aVar.f());
            a(str2, "Saving heartrate summaries data. OK");
            k = aVar.k();
            if (true ^ k.isEmpty()) {
            }
            rl6 unused5 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new g(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (xe6) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
            portfolioApp.b(str2);
            return cd6.a;
        }
        e2 = aVar.e();
        if (!(!e2.isEmpty())) {
            heartRateSampleRepository.insertFromDevice(e2);
            a(str2, "Saving heart rate data. OK");
        } else {
            HeartRateSampleRepository heartRateSampleRepository2 = heartRateSampleRepository;
            a(str2, "No heart rate data.");
        }
        FLogger.INSTANCE.getLocal().d(a, ".saveSyncResult - Update heartrate summary by syncing");
        try {
            heartRateSummaryRepository.insertFromDevice(aVar.f());
            a(str2, "Saving heartrate summaries data. OK");
        } catch (Exception e10) {
            e = e10;
            HeartRateSummaryRepository heartRateSummaryRepository2 = heartRateSummaryRepository;
            a(str2, "Saving heartrate summaries data. error=" + e.getMessage());
            k = aVar.k();
            if (true ^ k.isEmpty()) {
            }
            rl6 unused6 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new g(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (xe6) null), 3, (Object) null);
            FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
            portfolioApp.b(str2);
            return cd6.a;
        }
        k = aVar.k();
        if (true ^ k.isEmpty()) {
            workoutSessionRepository.insertFromDevice(k);
            a(str2, "Saving workout sessions. OK");
        } else {
            WorkoutSessionRepository workoutSessionRepository2 = workoutSessionRepository;
            a(str2, "No workout sessions data.");
        }
        rl6 unused7 = ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new g(fitnessDataRepository, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, heartRateSampleRepository, heartRateSummaryRepository, workoutSessionRepository, (xe6) null), 3, (Object) null);
        FLogger.INSTANCE.getLocal().d(a, "DONE save to database, delete data file");
        portfolioApp.b(str2);
        return cd6.a;
    }

    @DexIgnore
    public final List<GFitWorkoutSession> b(List<FitnessDataWrapper> list) {
        ArrayList arrayList;
        ArrayList arrayList2;
        int i;
        ArrayList arrayList3;
        ArrayList arrayList4 = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            Iterator<T> it2 = ((FitnessDataWrapper) it.next()).getWorkouts().iterator();
            while (it2.hasNext()) {
                WorkoutSessionWrapper workoutSessionWrapper = (WorkoutSessionWrapper) it2.next();
                DateTime component2 = workoutSessionWrapper.component2();
                DateTime component3 = workoutSessionWrapper.component3();
                int component5 = workoutSessionWrapper.component5();
                int component6 = workoutSessionWrapper.component6();
                StepWrapper component7 = workoutSessionWrapper.component7();
                CalorieWrapper component8 = workoutSessionWrapper.component8();
                DistanceWrapper component9 = workoutSessionWrapper.component9();
                HeartRateWrapper component10 = workoutSessionWrapper.component10();
                long millis = component2.getMillis();
                long millis2 = component3.getMillis();
                ArrayList arrayList5 = new ArrayList();
                ArrayList arrayList6 = new ArrayList();
                ArrayList arrayList7 = new ArrayList();
                Iterator<T> it3 = it;
                ArrayList arrayList8 = new ArrayList();
                Iterator<T> it4 = it2;
                int size = component7.getValues().size();
                long j = millis2;
                if (size > 0) {
                    arrayList = arrayList4;
                    arrayList2 = arrayList8;
                    i = (int) ((((float) component5) / ((float) size)) * ((float) 1000));
                } else {
                    arrayList = arrayList4;
                    arrayList2 = arrayList8;
                    i = 0;
                }
                int i2 = 0;
                while (i2 < size) {
                    i2++;
                    arrayList5.add(new GFitWOStep(component7.getValues().get(i2).shortValue(), millis + ((long) (i2 * i)), millis + ((long) (i2 * i))));
                    component10 = component10;
                    size = size;
                }
                HeartRateWrapper heartRateWrapper = component10;
                int size2 = component8.getValues().size();
                int i3 = size2 > 0 ? (int) ((((float) component5) / ((float) size2)) * ((float) 1000)) : 0;
                int i4 = 0;
                while (i4 < size2) {
                    i4++;
                    arrayList6.add(new GFitWOCalorie(component8.getValues().get(i4).floatValue(), millis + ((long) (i4 * i3)), millis + ((long) (i4 * i3))));
                }
                int size3 = component9.getValues().size();
                int i5 = size3 > 0 ? (int) ((((float) component5) / ((float) size3)) * ((float) 1000)) : 0;
                int i6 = 0;
                while (i6 < size3) {
                    i6++;
                    arrayList7.add(new GFitWODistance((float) component9.getValues().get(i6).doubleValue(), millis + ((long) (i6 * i5)), millis + ((long) (i6 * i5))));
                }
                if (heartRateWrapper != null) {
                    int size4 = heartRateWrapper.getValues().size();
                    int i7 = size4 > 0 ? (int) ((((float) component5) / ((float) size4)) * ((float) 1000)) : 0;
                    int i8 = 0;
                    while (i8 < size4) {
                        i8++;
                        arrayList2.add(new GFitWOHeartRate((float) heartRateWrapper.getValues().get(i8).shortValue(), ((long) (i8 * i7)) + millis, ((long) (i8 * i7)) + millis));
                    }
                }
                ArrayList arrayList9 = arrayList2;
                if (millis <= 0 || j <= 0) {
                    arrayList3 = arrayList;
                    FLogger.INSTANCE.getLocal().d(a, "getGFitWorkoutSessions gFitWorkoutSession with invalid time");
                } else {
                    arrayList3 = arrayList;
                    arrayList3.add(new GFitWorkoutSession(millis, j, component6, arrayList5, arrayList6, arrayList7, arrayList9));
                }
                arrayList4 = arrayList3;
                it2 = it4;
                it = it3;
            }
        }
        return arrayList4;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v7, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v8, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v22, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v9, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v11, resolved type: com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final lc6<List<HeartRateSample>, List<DailyHeartRateSummary>> a(List<FitnessDataWrapper> list, String str) {
        Object obj;
        Iterator<T> it;
        int i;
        long j;
        int i2;
        DailyHeartRateSummary dailyHeartRateSummary;
        DailyHeartRateSummary dailyHeartRateSummary2;
        int i3;
        List<FitnessDataWrapper> list2 = list;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        if (list.isEmpty()) {
            return new lc6<>(arrayList, arrayList2);
        }
        int i4 = 0;
        yd6.a(list2, ue6.a(c.INSTANCE, d.INSTANCE));
        Calendar c2 = bk4.c(list2.get(0).getStartLongTime());
        c2.set(13, 0);
        c2.set(14, 0);
        wg6.a((Object) c2, "startTime");
        c2.setTimeZone(bk4.a(list2.get(0).getTimezoneOffsetInSecond() * 1000));
        long startLongTime = list2.get(0).getStartLongTime();
        HeartRateWrapper heartRate = list2.get(0).getHeartRate();
        long j2 = 1000;
        Calendar c3 = bk4.c(startLongTime + (((long) (heartRate != null ? heartRate.getResolutionInSecond() : 0)) * 1000));
        int i5 = c2.get(10);
        int i6 = c2.get(12);
        Date time = c2.getTime();
        wg6.a((Object) time, "startTime.time");
        long currentTimeMillis = System.currentTimeMillis();
        long currentTimeMillis2 = System.currentTimeMillis();
        DateTime dateTime = r10;
        wg6.a((Object) c3, "endTime");
        DateTime dateTime2 = new DateTime(c3.getTimeInMillis());
        DateTime dateTime3 = r8;
        DateTime dateTime4 = new DateTime(c2.getTimeInMillis());
        HeartRateSample heartRateSample = new HeartRateSample(0.0f, time, currentTimeMillis, currentTimeMillis2, dateTime, dateTime3, list2.get(0).getTimezoneOffsetInSecond(), Integer.MAX_VALUE, Integer.MIN_VALUE, str, 0, (Resting) null, 2048, (qg6) null);
        DailyHeartRateSummary dailyHeartRateSummary3 = new DailyHeartRateSummary(heartRateSample.getAverage(), new Date(heartRateSample.getStartTimeId().getMillis()), System.currentTimeMillis(), System.currentTimeMillis(), heartRateSample.getMin(), heartRateSample.getMax(), heartRateSample.getMinuteCount(), heartRateSample.getResting());
        TimeZone timeZone = TimeZone.getDefault();
        wg6.a((Object) timeZone, "TimeZone.getDefault()");
        int rawOffset = timeZone.getRawOffset() / 1000;
        jh6 jh6 = new jh6();
        Iterator<T> it2 = list.iterator();
        int i7 = i6;
        int i8 = 0;
        int i9 = 0;
        float f2 = 0.0f;
        int i10 = i5;
        DailyHeartRateSummary dailyHeartRateSummary4 = dailyHeartRateSummary3;
        HeartRateSample heartRateSample2 = heartRateSample;
        while (it2.hasNext()) {
            T next = it2.next();
            int i11 = i8 + 1;
            if (i8 >= 0) {
                FitnessDataWrapper fitnessDataWrapper = (FitnessDataWrapper) next;
                HeartRateWrapper heartRate2 = fitnessDataWrapper.getHeartRate();
                if (heartRate2 != null) {
                    int component1 = heartRate2.component1();
                    List<Short> component3 = heartRate2.component3();
                    int timezoneOffsetInSecond = fitnessDataWrapper.getTimezoneOffsetInSecond();
                    it = it2;
                    long millis = fitnessDataWrapper.getResting().isEmpty() ^ true ? fitnessDataWrapper.getResting().get(i4).getStartTime().getMillis() : 0;
                    jh6.element = fitnessDataWrapper.getResting();
                    int i12 = i10;
                    FLogger.INSTANCE.getLocal().d(a, "Resting: restingStartTimeInSecond=" + millis + ", restingValues=" + yi4.a(jh6.element));
                    String[] availableIDs = TimeZone.getAvailableIDs(rawOffset * 1000);
                    Iterator<T> it3 = component3.iterator();
                    int i13 = i12;
                    DailyHeartRateSummary dailyHeartRateSummary5 = dailyHeartRateSummary4;
                    int i14 = 0;
                    while (it3.hasNext()) {
                        T next2 = it3.next();
                        int i15 = i14 + 1;
                        if (i14 >= 0) {
                            int shortValue = ((Number) next2).shortValue();
                            DailyHeartRateSummary dailyHeartRateSummary6 = dailyHeartRateSummary5;
                            Calendar c4 = bk4.c(fitnessDataWrapper.getStartLongTime() + (((long) (i14 * component1)) * 1000));
                            Iterator<T> it4 = it3;
                            c4.set(13, 0);
                            c4.set(14, 0);
                            wg6.a((Object) c4, "currentStartTime");
                            c4.setTimeZone(TimeZone.getTimeZone(availableIDs[0]));
                            Object clone = c4.clone();
                            if (clone != null) {
                                Calendar calendar = (Calendar) clone;
                                calendar.add(13, component1);
                                cd6 cd6 = cd6.a;
                                String[] strArr = availableIDs;
                                int i16 = c4.get(10);
                                int i17 = c4.get(12);
                                if (i16 == i13 && i17 / 5 == i7 / 5 && timezoneOffsetInSecond == heartRateSample2.getTimezoneOffsetInSecond()) {
                                    if (shortValue > 0) {
                                        i3 = i9 + 1;
                                        heartRateSample2.setMinuteCount(i3);
                                    } else {
                                        i3 = i9;
                                    }
                                    f2 += (float) shortValue;
                                    if (shortValue > 0) {
                                        if (heartRateSample2.getMin() == 0) {
                                            heartRateSample2.setMin(shortValue);
                                        } else if (shortValue < heartRateSample2.getMin()) {
                                            heartRateSample2.setMin(shortValue);
                                        }
                                    }
                                    if (shortValue > 0 && shortValue > heartRateSample2.getMax()) {
                                        heartRateSample2.setMax(shortValue);
                                    }
                                    heartRateSample2.setEndTime(new DateTime((Object) calendar.getTime()));
                                    if (i3 > 0) {
                                        heartRateSample2.setAverage(f2 / ((float) i3));
                                    }
                                    for (RestingWrapper restingWrapper : jh6.element) {
                                        if (restingWrapper.getTimezoneOffsetInSecond() == heartRateSample2.getTimezoneOffsetInSecond() && heartRateSample2.getStartTime().getMillis() <= restingWrapper.getStartTime().getMillis() && restingWrapper.getStartTime().getMillis() <= heartRateSample2.getEndTime().getMillis()) {
                                            heartRateSample2.setResting(new Resting(heartRateSample2.getEndTime(), restingWrapper.getValue()));
                                        }
                                    }
                                    i9 = i3;
                                    dailyHeartRateSummary5 = dailyHeartRateSummary6;
                                    i2 = rawOffset;
                                } else {
                                    if (heartRateSample2.getAverage() > ((float) 0)) {
                                        DateTime minusMinutes = heartRateSample2.getStartTimeId().minusMinutes(heartRateSample2.getStartTimeId().getMinuteOfHour() % 5);
                                        wg6.a((Object) minusMinutes, "currentHeartRate.getStar\u2026inusMinutes(minusMinutes)");
                                        heartRateSample2.setStartTimeId(minusMinutes);
                                        arrayList.add(heartRateSample2);
                                        if (bk4.d(dailyHeartRateSummary6.getDate(), heartRateSample2.getStartTimeId().toLocalDateTime().toDate())) {
                                            dailyHeartRateSummary = b.a(dailyHeartRateSummary6, heartRateSample2);
                                        } else {
                                            DailyHeartRateSummary dailyHeartRateSummary7 = dailyHeartRateSummary6;
                                            Iterator it5 = arrayList2.iterator();
                                            while (true) {
                                                if (!it5.hasNext()) {
                                                    dailyHeartRateSummary2 = null;
                                                    break;
                                                }
                                                Object next3 = it5.next();
                                                Iterator it6 = it5;
                                                dailyHeartRateSummary2 = next3;
                                                if (bk4.d(next3.getDate(), heartRateSample2.getDate())) {
                                                    break;
                                                }
                                                it5 = it6;
                                            }
                                            DailyHeartRateSummary dailyHeartRateSummary8 = dailyHeartRateSummary2;
                                            if (dailyHeartRateSummary8 != null) {
                                                arrayList2.remove(dailyHeartRateSummary8);
                                                dailyHeartRateSummary = b.a(dailyHeartRateSummary8, heartRateSample2);
                                            } else {
                                                arrayList2.add(dailyHeartRateSummary7);
                                                float average = heartRateSample2.getAverage();
                                                Date date = heartRateSample2.getStartTimeId().toLocalDateTime().toDate();
                                                wg6.a((Object) date, "currentHeartRate.getStar\u2026oLocalDateTime().toDate()");
                                                dailyHeartRateSummary = new DailyHeartRateSummary(average, date, System.currentTimeMillis(), System.currentTimeMillis(), heartRateSample2.getMin(), heartRateSample2.getMax(), heartRateSample2.getMinuteCount(), heartRateSample2.getResting());
                                            }
                                        }
                                    } else {
                                        dailyHeartRateSummary = dailyHeartRateSummary6;
                                    }
                                    int i18 = c4.get(10);
                                    i7 = c4.get(12);
                                    float f3 = (float) shortValue;
                                    i9 = shortValue > 0 ? 1 : 0;
                                    Date time2 = c4.getTime();
                                    DailyHeartRateSummary dailyHeartRateSummary9 = dailyHeartRateSummary;
                                    wg6.a((Object) time2, "currentStartTime.time");
                                    long currentTimeMillis3 = System.currentTimeMillis();
                                    long currentTimeMillis4 = System.currentTimeMillis();
                                    int i19 = i18;
                                    DateTime dateTime5 = new DateTime(calendar.getTimeInMillis());
                                    int i20 = timezoneOffsetInSecond / DateTimeConstants.SECONDS_PER_HOUR;
                                    DateTime withZone = dateTime5.withZone(DateTimeZone.forOffsetHours(i20));
                                    DateTime dateTime6 = withZone;
                                    wg6.a((Object) withZone, "DateTime(currentEndTime.\u2026neOffsetInSecond / 3600))");
                                    i2 = rawOffset;
                                    DateTime withZone2 = new DateTime(c4.getTimeInMillis()).withZone(DateTimeZone.forOffsetHours(i20));
                                    wg6.a((Object) withZone2, "DateTime(currentStartTim\u2026neOffsetInSecond / 3600))");
                                    HeartRateSample heartRateSample3 = new HeartRateSample(f3, time2, currentTimeMillis3, currentTimeMillis4, dateTime6, withZone2, timezoneOffsetInSecond, shortValue, shortValue, str, i9, (Resting) null, 2048, (qg6) null);
                                    FLogger.INSTANCE.getLocal().d(a, "getHeartRateData startTime=" + c4.getTime() + " endTime=" + calendar.getTime());
                                    dailyHeartRateSummary5 = dailyHeartRateSummary9;
                                    i13 = i19;
                                    f2 = f3;
                                    heartRateSample2 = heartRateSample3;
                                }
                                cd6 cd62 = cd6.a;
                                i14 = i15;
                                it3 = it4;
                                rawOffset = i2;
                                availableIDs = strArr;
                            } else {
                                throw new rc6("null cannot be cast to non-null type java.util.Calendar");
                            }
                        } else {
                            qd6.c();
                            throw null;
                        }
                    }
                    i = rawOffset;
                    j = 1000;
                    cd6 cd63 = cd6.a;
                    dailyHeartRateSummary4 = dailyHeartRateSummary5;
                    i10 = i13;
                } else {
                    it = it2;
                    i = rawOffset;
                    DailyHeartRateSummary dailyHeartRateSummary10 = dailyHeartRateSummary4;
                    j = j2;
                    int i21 = i10;
                }
                it2 = it;
                i8 = i11;
                j2 = j;
                rawOffset = i;
                i4 = 0;
            } else {
                qd6.c();
                throw null;
            }
        }
        DailyHeartRateSummary dailyHeartRateSummary11 = dailyHeartRateSummary4;
        float f4 = (float) 0;
        if (heartRateSample2.getAverage() > f4) {
            DateTime minusMinutes2 = heartRateSample2.getStartTimeId().minusMinutes(heartRateSample2.getStartTimeId().getMinuteOfHour() % 5);
            wg6.a((Object) minusMinutes2, "currentHeartRate.getStar\u2026inusMinutes(minusMinutes)");
            heartRateSample2.setStartTimeId(minusMinutes2);
            arrayList.add(heartRateSample2);
            if (bk4.d(dailyHeartRateSummary11.getDate(), heartRateSample2.getStartTimeId().toLocalDateTime().toDate())) {
                arrayList2.add(a(dailyHeartRateSummary11, heartRateSample2));
            } else {
                DailyHeartRateSummary dailyHeartRateSummary12 = dailyHeartRateSummary11;
                Iterator it7 = arrayList2.iterator();
                while (true) {
                    if (!it7.hasNext()) {
                        obj = null;
                        break;
                    }
                    obj = it7.next();
                    if (bk4.d(((DailyHeartRateSummary) obj).getDate(), heartRateSample2.getDate())) {
                        break;
                    }
                }
                DailyHeartRateSummary dailyHeartRateSummary13 = (DailyHeartRateSummary) obj;
                if (dailyHeartRateSummary13 != null) {
                    arrayList2.remove(dailyHeartRateSummary13);
                    arrayList2.add(a(dailyHeartRateSummary13, heartRateSample2));
                } else {
                    arrayList2.add(dailyHeartRateSummary12);
                    float average2 = heartRateSample2.getAverage();
                    Date date2 = heartRateSample2.getStartTimeId().toLocalDateTime().toDate();
                    wg6.a((Object) date2, "currentHeartRate.getStar\u2026oLocalDateTime().toDate()");
                    arrayList2.add(new DailyHeartRateSummary(average2, date2, System.currentTimeMillis(), System.currentTimeMillis(), heartRateSample2.getMin(), heartRateSample2.getMax(), heartRateSample2.getMinuteCount(), heartRateSample2.getResting()));
                }
            }
        } else {
            DailyHeartRateSummary dailyHeartRateSummary14 = dailyHeartRateSummary11;
            if (dailyHeartRateSummary14.getAverage() > f4) {
                arrayList2.add(dailyHeartRateSummary14);
            }
        }
        FLogger.INSTANCE.getLocal().d("SyncDataExtensions", "heartrate " + arrayList + " \n summary " + arrayList2);
        return new lc6<>(arrayList, arrayList2);
    }

    @DexIgnore
    public final List<GFitHeartRate> a(List<FitnessDataWrapper> list) {
        FLogger.INSTANCE.getLocal().d(a, "getGFitHeartRates");
        ArrayList arrayList = new ArrayList();
        yd6.a(list, new b());
        for (FitnessDataWrapper fitnessDataWrapper : list) {
            int timezoneOffsetInSecond = fitnessDataWrapper.getTimezoneOffsetInSecond();
            HeartRateWrapper heartRate = fitnessDataWrapper.getHeartRate();
            if (heartRate != null) {
                int component1 = heartRate.component1();
                int i = 0;
                for (T next : heartRate.component3()) {
                    int i2 = i + 1;
                    if (i >= 0) {
                        short shortValue = ((Number) next).shortValue();
                        Calendar calendar = new DateTime(fitnessDataWrapper.getStartLongTime() + (((long) (i * component1)) * 1000), DateTimeZone.forOffsetMillis(timezoneOffsetInSecond * 1000)).toCalendar(Locale.US);
                        calendar.set(13, 0);
                        calendar.set(14, 0);
                        Object clone = calendar.clone();
                        if (clone != null) {
                            Calendar calendar2 = (Calendar) clone;
                            calendar2.add(13, component1);
                            wg6.a((Object) calendar, "currentStartTime");
                            arrayList.add(new GFitHeartRate((float) shortValue, calendar.getTimeInMillis(), calendar2.getTimeInMillis()));
                            i = i2;
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.util.Calendar");
                        }
                    } else {
                        qd6.c();
                        throw null;
                    }
                }
                continue;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final DailyHeartRateSummary a(DailyHeartRateSummary dailyHeartRateSummary, HeartRateSample heartRateSample) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "calculateHeartRateSummary - summary=" + dailyHeartRateSummary + ", sample=" + heartRateSample);
        int minuteCount = dailyHeartRateSummary.getMinuteCount() + heartRateSample.getMinuteCount();
        return new DailyHeartRateSummary(((dailyHeartRateSummary.getAverage() * ((float) dailyHeartRateSummary.getMinuteCount())) + (heartRateSample.getAverage() * ((float) heartRateSample.getMinuteCount()))) / ((float) minuteCount), dailyHeartRateSummary.getDate(), dailyHeartRateSummary.getCreatedAt(), System.currentTimeMillis(), Math.min(dailyHeartRateSummary.getMin(), heartRateSample.getMin()), Math.max(dailyHeartRateSummary.getMax(), heartRateSample.getMax()), minuteCount, heartRateSample.getResting() != null ? heartRateSample.getResting() : dailyHeartRateSummary.getResting());
    }

    @DexIgnore
    public final List<WorkoutSession> a(List<FitnessDataWrapper> list, String str, String str2) {
        FLogger.INSTANCE.getLocal().d(a, "getWorkoutData");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        yd6.a(list, new e());
        for (FitnessDataWrapper workouts : list) {
            for (WorkoutSessionWrapper workoutSessionWrapper : workouts.getWorkouts()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = a;
                local.d(str3, "getWorkoutData - value=" + workoutSessionWrapper);
                arrayList.add(new WorkoutSession(workoutSessionWrapper, str, str2));
            }
        }
        return arrayList;
    }
}
