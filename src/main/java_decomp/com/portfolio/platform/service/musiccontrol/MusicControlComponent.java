package com.portfolio.platform.service.musiccontrol;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.RemoteController;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.media.session.PlaybackState;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.KeyEvent;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.ef6;
import com.fossil.ff6;
import com.fossil.fq4;
import com.fossil.fq4$a$a;
import com.fossil.fq4$b$a;
import com.fossil.fq4$d$a;
import com.fossil.fq4$d$b;
import com.fossil.fq4$d$c;
import com.fossil.fq4$e$a;
import com.fossil.fq4$f$a;
import com.fossil.fq4$f$b;
import com.fossil.fq4$f$c;
import com.fossil.fq4$f$d;
import com.fossil.fq4$f$e;
import com.fossil.fq4$h$a;
import com.fossil.fq4$h$b;
import com.fossil.fq4$j$a;
import com.fossil.fq4$j$b;
import com.fossil.fq4$m$a;
import com.fossil.gk6;
import com.fossil.gq4;
import com.fossil.hg6;
import com.fossil.hq4;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jd;
import com.fossil.jf6;
import com.fossil.jh6;
import com.fossil.jl6;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mk6;
import com.fossil.nc6;
import com.fossil.nf6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xi4;
import com.fossil.xp6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.fossil.zp6;
import com.fossil.zw5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponseFactory;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.MediaAppDetails;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.service.FossilNotificationListenerService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MusicControlComponent {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ List<String> n; // = qd6.d("org.videolan.vlc");
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public MediaSessionManager a;
    @DexIgnore
    public b b;
    @DexIgnore
    public /* final */ xp6 c; // = zp6.a(false, 1, (Object) null);
    @DexIgnore
    public fq4.g<fq4.b> d; // = new g();
    @DexIgnore
    public /* final */ AudioManager e;
    @DexIgnore
    public MediaSessionManager.OnActiveSessionsChangedListener f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ MutableLiveData<DianaPresetRepository> h; // = new MutableLiveData<>();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ LiveData<DianaPreset> j;
    @DexIgnore
    public /* final */ ld<DianaPreset> k;
    @DexIgnore
    public /* final */ Context l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends hq4<fq4, Context> {
        @DexIgnore
        public a() {
            super(fq4$a$a.INSTANCE);
        }

        @DexIgnore
        public final String a() {
            return MusicControlComponent.m;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        /*
        static {
            new fq4$b$a((qg6) null);
        }
        */

        @DexIgnore
        public b(String str, String str2) {
            wg6.b(str, "appName");
            wg6.b(str2, "packageName");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public abstract boolean a(KeyEvent keyEvent);

        @DexIgnore
        public abstract c b();

        @DexIgnore
        public final String c() {
            return this.b;
        }

        @DexIgnore
        public abstract int d();

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof b)) {
                return false;
            }
            return wg6.a((Object) this.b, (Object) ((b) obj).b);
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ String e;

        @DexIgnore
        public c(String str, String str2, String str3, String str4, String str5) {
            wg6.b(str, "packageName");
            wg6.b(str2, "appName");
            wg6.b(str3, Explore.COLUMN_TITLE);
            wg6.b(str4, "artist");
            wg6.b(str5, "album");
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = str5;
        }

        @DexIgnore
        public final String a() {
            return this.e;
        }

        @DexIgnore
        public final String b() {
            return this.d;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }

        @DexIgnore
        public final String d() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return wg6.a((Object) this.a, (Object) cVar.a) && wg6.a((Object) this.b, (Object) cVar.b) && wg6.a((Object) this.c, (Object) cVar.c) && wg6.a((Object) this.d, (Object) cVar.d) && wg6.a((Object) this.e, (Object) cVar.e);
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.c;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.d;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.e;
            if (str5 != null) {
                i = str5.hashCode();
            }
            return hashCode4 + i;
        }

        @DexIgnore
        public String toString() {
            return "MusicMetadata[packageName=" + this.a + ", appName=" + this.b + ", title=" + this.c + ", artist=" + this.d + ", album=" + this.e + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d extends b {
        @DexIgnore
        public /* final */ MediaController.Callback c;
        @DexIgnore
        public int d;
        @DexIgnore
        public c e;
        @DexIgnore
        public /* final */ MediaController f;

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public d(MediaController mediaController, String str) {
            super(str, r0);
            wg6.b(mediaController, "mMediaController");
            wg6.b(str, "appName");
            String packageName = mediaController.getPackageName();
            wg6.a((Object) packageName, "mMediaController.packageName");
            this.f = mediaController;
            this.c = new fq4$d$a(this, str);
            this.e = new c(c(), str, "Unknown", "Unknown", "Unknown");
            this.f.registerCallback(this.c);
        }

        @DexIgnore
        public final void a(int i) {
            this.d = i;
        }

        @DexIgnore
        public c b() {
            String str;
            String str2;
            String str3;
            MediaMetadata metadata = this.f.getMetadata();
            if (metadata != null) {
                String a = xi4.a(metadata.getString("android.media.metadata.TITLE"), "Unknown");
                String a2 = xi4.a(metadata.getString("android.media.metadata.ARTIST"), "Unknown");
                str = xi4.a(metadata.getString("android.media.metadata.ALBUM"), "Unknown");
                str3 = a;
                str2 = a2;
            } else {
                str3 = "Unknown";
                str2 = str3;
                str = str2;
            }
            c cVar = new c(c(), a(), str3, str2, str);
            if (!wg6.a((Object) cVar, (Object) this.e)) {
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new fq4$d$b(this, this.e, cVar, (xe6) null), 3, (Object) null);
                this.e = cVar;
            }
            return this.e;
        }

        @DexIgnore
        public int d() {
            PlaybackState playbackState = this.f.getPlaybackState();
            Integer valueOf = playbackState != null ? Integer.valueOf(playbackState.getState()) : null;
            int i = this.d;
            if ((valueOf == null || valueOf.intValue() != i) && valueOf != null) {
                int i2 = this.d;
                this.d = valueOf.intValue();
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new fq4$d$c(this, i2, valueOf, (xe6) null), 3, (Object) null);
            }
            return this.d;
        }

        @DexIgnore
        public final c e() {
            return this.e;
        }

        @DexIgnore
        public final int f() {
            return this.d;
        }

        @DexIgnore
        public final void a(c cVar) {
            wg6.b(cVar, "<set-?>");
            this.e = cVar;
        }

        @DexIgnore
        public void a(int i, int i2, b bVar) {
            wg6.b(bVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = MusicControlComponent.o.a();
            local.d(a, "onPlaybackStateChanged(), packageName=" + c() + ", oldState=" + i + ", newState=" + i2);
        }

        @DexIgnore
        public void a(c cVar, c cVar2) {
            wg6.b(cVar, "oldMetadata");
            wg6.b(cVar2, "newMetadata");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = MusicControlComponent.o.a();
            local.d(a, "onMetadataChanged(), oldMetadata=" + cVar + ", newMetadata=" + cVar2);
        }

        @DexIgnore
        public boolean a(KeyEvent keyEvent) {
            wg6.b(keyEvent, "keyEvent");
            return this.f.dispatchMediaButtonEvent(keyEvent);
        }

        @DexIgnore
        public void a(b bVar) {
            wg6.b(bVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = MusicControlComponent.o.a();
            local.d(a, ".onSessionDestroyed, packageName=" + c());
            this.f.unregisterCallback(this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e extends b {
        @DexIgnore
        public /* final */ RemoteController.OnClientUpdateListener c;
        @DexIgnore
        public /* final */ RemoteController d;
        @DexIgnore
        public int e;
        @DexIgnore
        public c f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(Context context, String str) {
            super(str, "All apps");
            wg6.b(context, "context");
            wg6.b(str, "appName");
            this.c = new fq4$e$a(this, str);
            this.f = new c(c(), str, "Unknown", "Unknown", "Unknown");
            this.d = new RemoteController(context, this.c);
        }

        @DexIgnore
        public final int a(int i) {
            switch (i) {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    return 3;
                case 4:
                    return 4;
                case 5:
                    return 5;
                case 6:
                    return 10;
                case 7:
                    return 9;
                case 8:
                    return 6;
                case 9:
                    return 7;
                default:
                    return 0;
            }
        }

        @DexIgnore
        public final void a(c cVar) {
            wg6.b(cVar, "<set-?>");
            this.f = cVar;
        }

        @DexIgnore
        public final void b(int i) {
            this.e = i;
        }

        @DexIgnore
        public int d() {
            return this.e;
        }

        @DexIgnore
        public final c e() {
            return this.f;
        }

        @DexIgnore
        public final int f() {
            return this.e;
        }

        @DexIgnore
        public void a(int i, int i2, b bVar) {
            wg6.b(bVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = MusicControlComponent.o.a();
            local.d(a, "onPlaybackStateChanged(), packageName=" + c() + ", oldState=" + i + ", newState=" + i2);
        }

        @DexIgnore
        public c b() {
            return this.f;
        }

        @DexIgnore
        public void a(c cVar, c cVar2) {
            wg6.b(cVar, "oldMetadata");
            wg6.b(cVar2, "newMetadata");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = MusicControlComponent.o.a();
            local.d(a, "onMetadataChanged(), oldMetadata=" + cVar + ", newMetadata=" + cVar2);
        }

        @DexIgnore
        public boolean a(KeyEvent keyEvent) {
            wg6.b(keyEvent, "keyEvent");
            return this.d.sendMediaKeyEvent(keyEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f extends b {
        @DexIgnore
        public /* final */ MediaControllerCompat.a c;
        @DexIgnore
        public MediaBrowserCompat d;
        @DexIgnore
        public MediaControllerCompat e;
        @DexIgnore
        public int f;
        @DexIgnore
        public c g; // = new c("", "", "Unknown", "Unknown", "Unknown");
        @DexIgnore
        public /* final */ Context h;
        @DexIgnore
        public /* final */ MediaAppDetails i;

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public f(Context context, MediaAppDetails mediaAppDetails, String str) {
            super(str, r0);
            wg6.b(context, "context");
            wg6.b(mediaAppDetails, "mMediaAppDetails");
            wg6.b(str, "appName");
            String str2 = mediaAppDetails.packageName;
            wg6.a((Object) str2, "mMediaAppDetails.packageName");
            this.h = context;
            this.i = mediaAppDetails;
            this.c = new fq4$f$a(this, str);
            e();
        }

        @DexIgnore
        public abstract void a(f fVar);

        @DexIgnore
        public int d() {
            MediaControllerCompat mediaControllerCompat = this.e;
            int a = a(mediaControllerCompat != null ? mediaControllerCompat.b() : null);
            int i2 = this.f;
            if (a != i2) {
                this.f = a;
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new fq4$f$e(this, i2, a, (xe6) null), 3, (Object) null);
            }
            return this.f;
        }

        @DexIgnore
        public final rm6 e() {
            return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new fq4$f$b(this, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public final MediaControllerCompat.a f() {
            return this.c;
        }

        @DexIgnore
        public final c g() {
            return this.g;
        }

        @DexIgnore
        public final int h() {
            return this.f;
        }

        @DexIgnore
        public final MediaBrowserCompat i() {
            return this.d;
        }

        @DexIgnore
        public final MediaControllerCompat j() {
            return this.e;
        }

        @DexIgnore
        public final void a(MediaBrowserCompat mediaBrowserCompat) {
            this.d = mediaBrowserCompat;
        }

        @DexIgnore
        public c b() {
            String str;
            String str2;
            String str3;
            MediaControllerCompat mediaControllerCompat = this.e;
            MediaMetadataCompat a = mediaControllerCompat != null ? mediaControllerCompat.a() : null;
            if (a != null) {
                String a2 = xi4.a(a.a("android.media.metadata.TITLE"), "Unknown");
                String a3 = xi4.a(a.a("android.media.metadata.ARTIST"), "Unknown");
                str = xi4.a(a.a("android.media.metadata.ALBUM"), "Unknown");
                str3 = a2;
                str2 = a3;
            } else {
                str3 = "Unknown";
                str2 = str3;
                str = str2;
            }
            c cVar = new c(c(), a(), str3, str2, str);
            if (!wg6.a((Object) cVar, (Object) this.g)) {
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new fq4$f$d(this, this.g, cVar, (xe6) null), 3, (Object) null);
                this.g = cVar;
            }
            return this.g;
        }

        @DexIgnore
        public final void a(MediaControllerCompat mediaControllerCompat) {
            this.e = mediaControllerCompat;
        }

        @DexIgnore
        public final void a(int i2) {
            this.f = i2;
        }

        @DexIgnore
        public final void a(c cVar) {
            wg6.b(cVar, "<set-?>");
            this.g = cVar;
        }

        @DexIgnore
        public final int a(PlaybackStateCompat playbackStateCompat) {
            if (playbackStateCompat == null) {
                return 0;
            }
            switch (playbackStateCompat.getState()) {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    return 3;
                case 4:
                    return 4;
                case 5:
                    return 5;
                case 6:
                    return 6;
                case 7:
                    return 7;
                case 8:
                    return 8;
                case 9:
                    return 9;
                case 10:
                    return 10;
                case 11:
                    return 11;
                default:
                    return 0;
            }
        }

        @DexIgnore
        public void a(int i2, int i3, b bVar) {
            wg6.b(bVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = MusicControlComponent.o.a();
            local.d(a, "onPlaybackStateChanged(), packageName=" + c() + ", oldState=" + i2 + ", newState=" + i3);
        }

        @DexIgnore
        public void a(c cVar, c cVar2) {
            wg6.b(cVar, "oldMetadata");
            wg6.b(cVar2, "newMetadata");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = MusicControlComponent.o.a();
            local.d(a, "onMetadataChanged(), oldMetadata=" + cVar + ", newMetadata=" + cVar2);
        }

        @DexIgnore
        public boolean a(KeyEvent keyEvent) {
            wg6.b(keyEvent, "keyEvent");
            MediaControllerCompat mediaControllerCompat = this.e;
            if (mediaControllerCompat != null) {
                return mediaControllerCompat.a(keyEvent);
            }
            return false;
        }

        @DexIgnore
        public void a(b bVar) {
            wg6.b(bVar, "controller");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = MusicControlComponent.o.a();
            local.d(a, ".onSessionDestroyed(), packageName=" + c());
        }

        @DexIgnore
        public final /* synthetic */ Object a(Context context, MediaAppDetails mediaAppDetails, xe6<? super MediaBrowserCompat> xe6) {
            mk6 mk6 = new mk6(ef6.a(xe6), 1);
            jh6 jh6 = new jh6();
            jh6.element = null;
            jh6.element = new MediaBrowserCompat(context, mediaAppDetails.componentName, new fq4$f$c(mk6, jh6), (Bundle) null);
            ((MediaBrowserCompat) jh6.element).a();
            FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), Constants.IF_VERIFY_ENABLE_CONNECTED);
            Object e2 = mk6.e();
            if (e2 == ff6.a()) {
                nf6.c(xe6);
            }
            return e2;
        }

        @DexIgnore
        public final MediaControllerCompat b(MediaBrowserCompat mediaBrowserCompat) {
            FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), "setupMediaController");
            if (!mediaBrowserCompat.d()) {
                return null;
            }
            try {
                MediaSessionCompat.Token c2 = mediaBrowserCompat.c();
                wg6.a((Object) c2, "browser.sessionToken");
                MediaControllerCompat mediaControllerCompat = new MediaControllerCompat(this.h, c2);
                FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), "MediaControllerCompat created");
                return mediaControllerCompat;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = MusicControlComponent.o.a();
                local.e(a, "Failed to create MediaController from session token " + e2);
                return null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<X, Y> extends jd<lc6<? extends X, ? extends Y>> {
        @DexIgnore
        public h(LiveData<X> liveData, LiveData<Y> liveData2) {
            wg6.b(liveData, "trigger1");
            wg6.b(liveData2, "trigger2");
            a(liveData, new fq4$h$a(this, liveData2));
            a(liveData2, new fq4$h$b(this, liveData));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent", f = "MusicControlComponent.kt", l = {229}, m = "enableNotificationListener")
    public static final class i extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(MusicControlComponent musicControlComponent, xe6 xe6) {
            super(xe6);
            this.this$0 = musicControlComponent;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((FossilNotificationListenerService) null, (xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements MediaSessionManager.OnActiveSessionsChangedListener {
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent a;

        @DexIgnore
        public j(MusicControlComponent musicControlComponent) {
            this.a = musicControlComponent;
        }

        @DexIgnore
        public final void onActiveSessionsChanged(List<MediaController> list) {
            if (list != null && (!list.isEmpty())) {
                for (MediaController next : list) {
                    if (!(this.a.f().a(new fq4$j$a(next)) != null)) {
                        wg6.a((Object) next, "activeMediaController");
                        MusicControlComponent musicControlComponent = this.a;
                        String packageName = next.getPackageName();
                        wg6.a((Object) packageName, "activeMediaController.packageName");
                        fq4$j$b fq4_j_b = new fq4$j$b(this, next, next, musicControlComponent.a(packageName));
                        if (this.a.f().a(fq4_j_b)) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a2 = MusicControlComponent.o.a();
                            local.d(a2, ".NewNotificationMusicController is added to list controller, packageName=" + fq4_j_b.c());
                        }
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableNotificationListener$3", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
    public static final class k extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationListenerService $fossilNotificationListenerService;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(MusicControlComponent musicControlComponent, FossilNotificationListenerService fossilNotificationListenerService, xe6 xe6) {
            super(2, xe6);
            this.this$0 = musicControlComponent;
            this.$fossilNotificationListenerService = fossilNotificationListenerService;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            k kVar = new k(this.this$0, this.$fossilNotificationListenerService, xe6);
            kVar.p$ = (il6) obj;
            return kVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((k) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                MediaSessionManager.OnActiveSessionsChangedListener b = this.this$0.f;
                if (b == null) {
                    return null;
                }
                MediaSessionManager c = this.this$0.a;
                if (c != null) {
                    c.addOnActiveSessionsChangedListener(b, new ComponentName(this.$fossilNotificationListenerService, FossilNotificationListenerService.class));
                    return cd6.a;
                }
                wg6.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$findMediaBrowserAppsTask$1", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
    public static final class m extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(MusicControlComponent musicControlComponent, xe6 xe6) {
            super(2, xe6);
            this.this$0 = musicControlComponent;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            m mVar = new m(this.this$0, xe6);
            mVar.p$ = (il6) obj;
            return mVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((m) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ArrayList arrayList = new ArrayList();
                Intent intent = new Intent("android.media.browse.MediaBrowserService");
                PackageManager packageManager = PortfolioApp.get.instance().getPackageManager();
                Resources resources = PortfolioApp.get.instance().getResources();
                List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 64);
                if (queryIntentServices != null && (!queryIntentServices.isEmpty())) {
                    for (ResolveInfo next : queryIntentServices) {
                        if (!MusicControlComponent.n.contains(next.serviceInfo.packageName)) {
                            arrayList.add(new MediaAppDetails(next.serviceInfo, packageManager, resources));
                        } else {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a = MusicControlComponent.o.a();
                            local.d(a, ".findMediaBrowserAppsTask(), " + next.serviceInfo.packageName + " is in black list of using Media Browser Compat.");
                        }
                    }
                }
                for (MediaAppDetails mediaAppDetails : yd6.b(arrayList)) {
                    Context d = this.this$0.d();
                    MusicControlComponent musicControlComponent = this.this$0;
                    String str = mediaAppDetails.packageName;
                    wg6.a((Object) str, "it.packageName");
                    new fq4$m$a(mediaAppDetails, d, mediaAppDetails, musicControlComponent.a(str), this);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements ld<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent a;

        @DexIgnore
        public n(MusicControlComponent musicControlComponent) {
            this.a = musicControlComponent;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(DianaPreset dianaPreset) {
            ArrayList<DianaPresetWatchAppSetting> watchapps;
            this.a.i = false;
            if (!(dianaPreset == null || (watchapps = dianaPreset.getWatchapps()) == null)) {
                for (DianaPresetWatchAppSetting id : watchapps) {
                    if (wg6.a((Object) id.getId(), (Object) Constants.MUSIC)) {
                        this.a.i = true;
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            local.d(a2, "mCurrentPresetObserver: mIsControlMusicAssigned=" + this.a.i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ o a; // = new o();

        /* JADX WARNING: Code restructure failed: missing block: B:4:0x0013, code lost:
            r2 = r0.getActivePresetBySerialLiveData(r2);
         */
        @DexIgnore
        /* renamed from: a */
        public final LiveData<DianaPreset> apply(lc6<DianaPresetRepository, String> lc6) {
            LiveData<DianaPreset> activePresetBySerialLiveData;
            DianaPresetRepository component1 = lc6.component1();
            String component2 = lc6.component2();
            if (component2 == null) {
                component2 = "";
            }
            return (component1 == null || activePresetBySerialLiveData == null) ? zw5.k.a() : activePresetBySerialLiveData;
        }
    }

    /*
    static {
        String simpleName = MusicControlComponent.class.getSimpleName();
        wg6.a((Object) simpleName, "MusicControlComponent::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public MusicControlComponent(Context context) {
        wg6.b(context, "context");
        this.l = context;
        AudioManager audioManager = null;
        LiveData<DianaPreset> b2 = sd.b(new h(this.h, PortfolioApp.get.instance().f()), o.a);
        wg6.a((Object) b2, "Transformations.switchMa\u2026ntLiveData.create()\n    }");
        this.j = b2;
        this.k = new n(this);
        FLogger.INSTANCE.getLocal().d(m, "init");
        Object systemService = this.l.getSystemService("audio");
        this.e = systemService instanceof AudioManager ? systemService : audioManager;
        b();
        this.j.a(this.k);
    }

    @DexIgnore
    public final Context d() {
        return this.l;
    }

    @DexIgnore
    public final b e() {
        return this.b;
    }

    @DexIgnore
    public final fq4.g<fq4.b> f() {
        return this.d;
    }

    @DexIgnore
    public final xp6 g() {
        return this.c;
    }

    @DexIgnore
    public final void h() {
        MediaSessionManager mediaSessionManager;
        this.g = false;
        MediaSessionManager.OnActiveSessionsChangedListener onActiveSessionsChangedListener = this.f;
        if (onActiveSessionsChangedListener != null && (mediaSessionManager = this.a) != null) {
            mediaSessionManager.removeOnActiveSessionsChangedListener(onActiveSessionsChangedListener);
        }
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus i() {
        AudioManager audioManager = this.e;
        if (audioManager != null) {
            audioManager.adjustStreamVolume(3, -1, 1);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus j() {
        AudioManager audioManager = this.e;
        if (audioManager != null) {
            audioManager.adjustStreamVolume(3, 1, 1);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
    }

    @DexIgnore
    public final rm6 b() {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new m(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void c() {
        b bVar = this.b;
        if (bVar != null) {
            int d2 = bVar.d();
            c b2 = bVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, ".forcePushEventAndMetadataChanged active controller=" + bVar.c() + ", state=" + d2 + ", metadata=" + b2);
            a(bVar.d());
            a(bVar.b());
            return;
        }
        FLogger.INSTANCE.getLocal().d(m, ".forcePushEventAndMetadataChanged, no active controller now.");
    }

    @DexIgnore
    public final boolean b(String str) {
        wg6.b(str, "serial");
        return FossilDeviceSerialPatternUtil.isDianaDevice(str);
    }

    @DexIgnore
    public final void a(b bVar) {
        boolean z = !wg6.a((Object) this.b, (Object) bVar);
        this.b = bVar;
        if (z && bVar != null) {
            a(bVar.b());
        }
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus b(int i2) {
        b bVar = this.b;
        if (bVar != null) {
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = (!bVar.a(new KeyEvent(0, i2)) || !bVar.a(new KeyEvent(1, i2))) ? NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER : NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.NO_MUSIC_PLAYER;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> {
        @DexIgnore
        public /* final */ Set<T> a; // = new LinkedHashSet();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final boolean a(T t) {
            boolean add;
            synchronized (this.b) {
                add = !this.a.contains(t) ? this.a.add(t) : false;
            }
            return add;
        }

        @DexIgnore
        public final boolean b(T t) {
            boolean remove;
            synchronized (this.b) {
                remove = this.a.remove(t);
            }
            return remove;
        }

        @DexIgnore
        public final T a(hg6<? super T, Boolean> hg6) {
            T t;
            wg6.b(hg6, "predicate");
            synchronized (this.b) {
                Iterator<T> it = this.a.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (hg6.invoke(t).booleanValue()) {
                        break;
                    }
                }
            }
            return t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l extends e {
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(MusicControlComponent musicControlComponent, Context context, String str) {
            super(context, str);
            this.g = musicControlComponent;
        }

        @DexIgnore
        public void a(int i, int i2, b bVar) {
            wg6.b(bVar, "controller");
            super.a(i, i2, bVar);
            if (i2 == 3) {
                this.g.a(bVar);
            } else if (this.g.e() == null) {
                this.g.a(bVar);
            }
            this.g.a(i2);
        }

        @DexIgnore
        public void a(c cVar, c cVar2) {
            wg6.b(cVar, "oldMetadata");
            wg6.b(cVar2, "newMetadata");
            super.a(cVar, cVar2);
            this.g.a(b());
        }
    }

    @DexIgnore
    public final void a(DianaPresetRepository dianaPresetRepository) {
        this.h.a(dianaPresetRepository);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(FossilNotificationListenerService fossilNotificationListenerService, xe6<? super cd6> xe6) {
        i iVar;
        int i2;
        MusicControlComponent musicControlComponent;
        if (xe6 instanceof i) {
            iVar = (i) xe6;
            int i3 = iVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                iVar.label = i3 - Integer.MIN_VALUE;
                Object obj = iVar.result;
                Object a2 = ff6.a();
                i2 = iVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(m, ".enableNotificationListener()");
                    if (!this.g) {
                        if (Build.VERSION.SDK_INT >= 21) {
                            Object systemService = this.l.getSystemService("media_session");
                            if (systemService != null) {
                                this.a = (MediaSessionManager) systemService;
                                this.f = new j(this);
                                cn6 c2 = zl6.c();
                                k kVar = new k(this, fossilNotificationListenerService, (xe6) null);
                                iVar.L$0 = this;
                                iVar.L$1 = fossilNotificationListenerService;
                                iVar.label = 1;
                                if (gk6.a(c2, kVar, iVar) == a2) {
                                    return a2;
                                }
                            } else {
                                throw new rc6("null cannot be cast to non-null type android.media.session.MediaSessionManager");
                            }
                        } else {
                            l lVar = new l(this, this.l, "All Apps");
                            if (this.d.a(lVar)) {
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String str = m;
                                local.d(str, ".OldNotificationMusicController is added to list controller, packageName=" + lVar.c());
                            }
                        }
                        musicControlComponent = this;
                    }
                    return cd6.a;
                } else if (i2 == 1) {
                    FossilNotificationListenerService fossilNotificationListenerService2 = (FossilNotificationListenerService) iVar.L$1;
                    musicControlComponent = (MusicControlComponent) iVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                musicControlComponent.g = true;
                return cd6.a;
            }
        }
        iVar = new i(this, xe6);
        Object obj2 = iVar.result;
        Object a22 = ff6.a();
        i2 = iVar.label;
        if (i2 != 0) {
        }
        musicControlComponent.g = true;
        return cd6.a;
    }

    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0013, code lost:
        r3 = r0.getApplicationLabel(r3).toString();
     */
    @DexIgnore
    public final String a(String str) {
        ApplicationInfo applicationInfo;
        String obj;
        PackageManager packageManager = PortfolioApp.get.instance().getPackageManager();
        try {
            applicationInfo = packageManager.getApplicationInfo(str, 0);
        } catch (Exception unused) {
            applicationInfo = null;
        }
        return (applicationInfo == null || obj == null) ? "Unknown" : obj;
    }

    @DexIgnore
    public final void a(NotifyMusicEventResponse.MusicMediaAction musicMediaAction) {
        NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus;
        wg6.b(musicMediaAction, "action");
        switch (gq4.a[musicMediaAction.ordinal()]) {
            case 1:
                musicMediaStatus = b(126);
                if (musicMediaStatus == NotifyMusicEventResponse.MusicMediaStatus.SUCCESS) {
                    musicMediaStatus = b(85);
                    break;
                }
                break;
            case 2:
                musicMediaStatus = b(127);
                if (musicMediaStatus == NotifyMusicEventResponse.MusicMediaStatus.SUCCESS) {
                    musicMediaStatus = b(85);
                    break;
                }
                break;
            case 3:
                musicMediaStatus = b(85);
                break;
            case 4:
                musicMediaStatus = b(87);
                break;
            case 5:
                musicMediaStatus = b(88);
                break;
            case 6:
                musicMediaStatus = j();
                break;
            case 7:
                musicMediaStatus = i();
                break;
            default:
                musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
                break;
        }
        if (this.i) {
            PortfolioApp.get.instance().a((MusicResponse) MusicResponseFactory.INSTANCE.createMusicEventResponse(musicMediaAction, musicMediaStatus), PortfolioApp.get.instance().e());
        }
    }

    @DexIgnore
    public final int a() {
        AudioManager audioManager = this.e;
        if (audioManager != null) {
            return audioManager.getStreamVolume(3);
        }
        return 100;
    }

    @DexIgnore
    public final synchronized void a(int i2) {
        NotifyMusicEventResponse.MusicMediaAction musicMediaAction;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, ".notifyMusicAction(), state=" + i2);
        if (i2 == 2) {
            musicMediaAction = NotifyMusicEventResponse.MusicMediaAction.PAUSE;
        } else if (i2 != 3) {
            musicMediaAction = null;
        } else {
            musicMediaAction = NotifyMusicEventResponse.MusicMediaAction.PLAY;
        }
        if (musicMediaAction != null && this.i) {
            PortfolioApp.get.instance().a((MusicResponse) MusicResponseFactory.INSTANCE.createMusicEventResponse(musicMediaAction, NotifyMusicEventResponse.MusicMediaStatus.SUCCESS), PortfolioApp.get.instance().e());
        }
    }

    @DexIgnore
    public final synchronized void a(c cVar) {
        wg6.b(cVar, "metadata");
        String c2 = cVar.c();
        b bVar = this.b;
        if (wg6.a((Object) c2, (Object) bVar != null ? bVar.c() : null)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, ".notifyMusicTrackInfo(), metadata=" + cVar);
            if (this.i) {
                PortfolioApp.get.instance().a((MusicResponse) MusicResponseFactory.INSTANCE.createMusicTrackInfoResponse("", (byte) a(), cVar.d(), cVar.b(), cVar.a()), PortfolioApp.get.instance().e());
            }
        }
    }
}
