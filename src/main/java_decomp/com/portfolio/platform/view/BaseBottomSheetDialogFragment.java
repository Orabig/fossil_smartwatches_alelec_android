package com.portfolio.platform.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.hc;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BaseBottomSheetDialogFragment extends BottomSheetDialogFragment implements DialogInterface.OnKeyListener, View.OnKeyListener {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public int d; // = 80;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public float f; // = 0.3f;
    @DexIgnore
    public DialogInterface.OnDismissListener g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = BaseBottomSheetDialogFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "BaseBottomSheetDialogFra\u2026nt::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public void dismiss() {
        hc b2;
        FLogger.INSTANCE.getLocal().d(i, "dismiss");
        FragmentManager fragmentManager = getFragmentManager();
        if (!(fragmentManager == null || (b2 = fragmentManager.b()) == null)) {
            b2.d(this);
            if (b2 != null) {
                b2.b();
            }
        }
        BaseBottomSheetDialogFragment.super.dismiss();
    }

    @DexIgnore
    public void e1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean f1() {
        dismiss();
        return true;
    }

    @DexIgnore
    public boolean isActive() {
        return isAdded();
    }

    @DexIgnore
    public void onAttach(Context context) {
        wg6.b(context, "context");
        BaseBottomSheetDialogFragment.super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof DialogInterface.OnDismissListener)) {
            parentFragment = null;
        }
        DialogInterface.OnDismissListener onDismissListener = (DialogInterface.OnDismissListener) parentFragment;
        if (onDismissListener == null) {
            if (!(context instanceof DialogInterface.OnDismissListener)) {
                context = null;
            }
            onDismissListener = (DialogInterface.OnDismissListener) context;
        }
        this.g = onDismissListener;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        BaseBottomSheetDialogFragment.super.onCreate(bundle);
        AnalyticsHelper.f.c();
    }

    @DexIgnore
    public void onDestroy() {
        BaseBottomSheetDialogFragment.super.onDestroy();
        FLogger.INSTANCE.getLocal().d(i, "onDestroy");
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        BaseBottomSheetDialogFragment.super.onDestroyView();
        e1();
    }

    @DexIgnore
    public void onDetach() {
        BaseBottomSheetDialogFragment.super.onDetach();
        FLogger.INSTANCE.getLocal().d(i, "onDetach");
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        wg6.b(dialogInterface, "dialog");
        BaseBottomSheetDialogFragment.super.onDismiss(dialogInterface);
        FLogger.INSTANCE.getLocal().d(i, "onDismiss");
        DialogInterface.OnDismissListener onDismissListener = this.g;
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialogInterface);
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent == null || keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String simpleName = getClass().getSimpleName();
        wg6.a((Object) simpleName, "javaClass.simpleName");
        local.d(simpleName, "onKey KEYCODE_BACK");
        return f1();
    }

    @DexIgnore
    public void setupDialog(Dialog dialog, int i2) {
        Window window;
        wg6.b(dialog, "dialog");
        if (this.c) {
            Window window2 = dialog.getWindow();
            if (window2 != null) {
                window2.setDimAmount(0.0f);
            }
        } else {
            Window window3 = dialog.getWindow();
            if (window3 != null) {
                window3.setDimAmount(this.f);
            }
        }
        Window window4 = dialog.getWindow();
        if (window4 != null) {
            window4.setGravity(this.d);
        }
        Window window5 = dialog.getWindow();
        if (window5 != null) {
            window5.setLayout(-1, this.b);
        }
        Window window6 = dialog.getWindow();
        if (window6 != null) {
            window6.setBackgroundDrawableResource(R.color.transparent);
        }
        if (this.e && (window = dialog.getWindow()) != null) {
            window.setFlags(32, 32);
        }
        dialog.setOnKeyListener(this);
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        wg6.b(fragmentManager, "fragmentManager");
        FLogger.INSTANCE.getLocal().d(str != null ? str : "", "show");
        if (!isActive()) {
            hc b2 = fragmentManager.b();
            wg6.a((Object) b2, "fragmentManager.beginTransaction()");
            Fragment b3 = fragmentManager.b(str);
            if (b3 != null) {
                b2.d(b3);
            }
            b2.a((String) null);
            show(b2, str);
            fragmentManager.r();
        }
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
        if (keyEvent == null || keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String simpleName = getClass().getSimpleName();
        wg6.a((Object) simpleName, "javaClass.simpleName");
        local.d(simpleName, "onKey KEYCODE_BACK");
        return f1();
    }
}
