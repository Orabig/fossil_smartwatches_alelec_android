package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.x24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class RTLImageView extends AppCompatImageView {
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public Boolean d; // = true;

    @DexIgnore
    public RTLImageView(Context context) {
        super(context);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.RTLImageView);
            this.c = obtainStyledAttributes.getString(0);
            this.d = Boolean.valueOf(obtainStyledAttributes.getBoolean(1, true));
            obtainStyledAttributes.recycle();
        }
        a();
    }

    @DexIgnore
    public void setImageDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable.setAutoMirrored(true);
            RTLImageView.super.setImageDrawable(drawable);
        }
    }

    @DexIgnore
    public RTLImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(attributeSet);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public final void a() {
        if (this.d.booleanValue()) {
            if (TextUtils.isEmpty(this.c)) {
                this.c = "primaryButton";
            }
            String b = ThemeManager.l.a().b(this.c);
            if (!TextUtils.isEmpty(b)) {
                setColorFilter(Color.parseColor(b));
            }
        }
    }

    @DexIgnore
    public RTLImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(attributeSet);
    }
}
