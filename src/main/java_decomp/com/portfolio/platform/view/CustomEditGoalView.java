package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.my5;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.sh4;
import com.fossil.wg6;
import com.fossil.yk4;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomEditGoalView extends ConstraintLayout {
    @DexIgnore
    public static /* final */ ArrayList<String> A; // = qd6.a((T[]) new String[]{"primaryText", "goalNumber", "goalNumberDescription"});
    @DexIgnore
    public static /* final */ ArrayList<String> B; // = qd6.a((T[]) new String[]{"nonBrandDisableCalendarDay", "goalNumber", "goalNumberDescription"});
    @DexIgnore
    public AppCompatImageView u;
    @DexIgnore
    public AutoResizeTextView v;
    @DexIgnore
    public FlexibleTextView w;
    @DexIgnore
    public String x; // = "";
    @DexIgnore
    public int y;
    @DexIgnore
    public sh4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.CustomEditGoalView, android.view.ViewGroup] */
    public CustomEditGoalView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        View view = null;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        view = layoutInflater != null ? layoutInflater.inflate(2131558801, this, true) : view;
        if (view != null) {
            AppCompatImageView findViewById = view.findViewById(2131361853);
            wg6.a((Object) findViewById, "view.findViewById(R.id.aciv_icon)");
            this.u = findViewById;
            View findViewById2 = view.findViewById(2131363226);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.tv_value)");
            this.v = (AutoResizeTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362356);
            wg6.a((Object) findViewById3, "view.findViewById(R.id.ftv_goal_type)");
            this.w = (FlexibleTextView) findViewById3;
            b("custom_edit_goal_disable");
            return;
        }
        throw new rc6("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void a(String str, String str2, String str3, String str4, String str5) {
        String b = ThemeManager.l.a().b(str3);
        Typeface c = ThemeManager.l.a().c(str5);
        Typeface c2 = ThemeManager.l.a().c(str4);
        String b2 = ThemeManager.l.a().b(str2);
        if (b != null) {
            this.w.setTextColor(Color.parseColor(b));
        }
        if (c != null) {
            this.w.setTypeface(c);
        }
        if (c2 != null) {
            this.v.setTypeface(c2);
        }
        if (b2 != null) {
            this.v.setTextColor(Color.parseColor(b2));
        }
        String b3 = ThemeManager.l.a().b(str);
        if (b3 != null) {
            this.u.setBackground(c(Color.parseColor(b3)));
        }
    }

    @DexIgnore
    public final void b(String str) {
        int hashCode = str.hashCode();
        if (hashCode != -836633469) {
            if (hashCode == 1806838920 && str.equals("custom_edit_goal_enable")) {
                String str2 = this.x;
                String str3 = A.get(0);
                wg6.a((Object) str3, "STATE_ENABLED_STYLES[0]");
                String str4 = A.get(1);
                wg6.a((Object) str4, "STATE_ENABLED_STYLES[1]");
                String str5 = A.get(2);
                wg6.a((Object) str5, "STATE_ENABLED_STYLES[2]");
                a(str2, str2, str3, str4, str5);
            }
        } else if (str.equals("custom_edit_goal_disable")) {
            String str6 = B.get(0);
            wg6.a((Object) str6, "STATE_DISABLED_STYLES[0]");
            String str7 = B.get(1);
            wg6.a((Object) str7, "STATE_DISABLED_STYLES[1]");
            String str8 = B.get(2);
            wg6.a((Object) str8, "STATE_DISABLED_STYLES[2]");
            a("nonBrandSurface", "nonBrandDisableCalendarDay", str6, str7, str8);
        }
    }

    @DexIgnore
    public final Drawable c(int i) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(1);
        gradientDrawable.setColor(i);
        return gradientDrawable;
    }

    @DexIgnore
    public final sh4 getMGoalType() {
        return this.z;
    }

    @DexIgnore
    public final int getMValue() {
        return this.y;
    }

    @DexIgnore
    public final int getValue() {
        return this.y;
    }

    @DexIgnore
    public final void setMGoalType(sh4 sh4) {
        this.z = sh4;
    }

    @DexIgnore
    public final void setMValue(int i) {
        this.y = i;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.CustomEditGoalView, android.view.ViewGroup] */
    public void setSelected(boolean z2) {
        if (z2) {
            b("custom_edit_goal_enable");
        } else {
            b("custom_edit_goal_disable");
        }
        CustomEditGoalView.super.setSelected(z2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void setType(sh4 sh4) {
        wg6.b(sh4, "type");
        this.z = sh4;
        int i = my5.a[sh4.ordinal()];
        if (i == 1) {
            this.u.setImageDrawable(getContext().getDrawable(2131231077));
            this.x = "dianaActiveCaloriesTab";
            this.w.setText(getContext().getString(2131886417));
        } else if (i == 2) {
            this.u.setImageDrawable(getContext().getDrawable(2131231082));
            this.x = "dianaActiveMinutesTab";
            this.w.setText(getContext().getString(2131886425));
        } else if (i == 3) {
            this.u.setImageDrawable(getContext().getDrawable(2131231081));
            this.x = "hybridStepsTab";
            this.w.setText(getContext().getString(2131886878));
        } else if (i == 4) {
            this.u.setImageDrawable(getContext().getDrawable(2131231199));
            this.x = "dianaSleepTab";
            this.w.setText(getContext().getString(2131886534));
        } else if (i == 5) {
            this.u.setImageDrawable(getContext().getDrawable(2131231100));
            this.x = "hybridGoalTrackingTab";
            this.w.setText(getContext().getString(2131886282));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v5, types: [com.portfolio.platform.view.AutoResizeTextView, android.widget.TextView] */
    public final void setValue(int i) {
        this.y = i;
        sh4 sh4 = this.z;
        if (sh4 != null) {
            int i2 = my5.b[sh4.ordinal()];
            if (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
                this.v.setText(yk4.b.c(this.y));
            } else if (i2 == 5) {
                this.v.setText(yk4.b.d(this.y));
            }
        }
    }
}
