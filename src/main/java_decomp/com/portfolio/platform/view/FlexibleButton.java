package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatButton;
import com.fossil.jm4;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FlexibleButton extends AppCompatButton {
    @DexIgnore
    public static /* final */ ArrayList<String> h; // = qd6.a((T[]) new String[]{"success", "textButtonDisable", "success", "success"});
    @DexIgnore
    public static /* final */ ArrayList<String> i; // = qd6.a((T[]) new String[]{"onPrimaryButton", "textButtonPrimary", "primaryButton", "primaryButtonBorder"});
    @DexIgnore
    public static /* final */ ArrayList<String> j; // = qd6.a((T[]) new String[]{"onSecondaryButton", "textButtonSecondary", "secondaryButton", "secondaryButtonBorder"});
    @DexIgnore
    public static /* final */ ArrayList<String> o; // = qd6.a((T[]) new String[]{"onDisabledButton", "textButtonDisable", "disabledButton", "disabledButtonBorder"});
    @DexIgnore
    public static /* final */ ArrayList<String> p; // = qd6.a((T[]) new String[]{"nonBrandSurface", "complicationCategory", "primaryButton", "primaryButton"});
    @DexIgnore
    public static /* final */ ArrayList<String> q; // = qd6.a((T[]) new String[]{"primaryText", "checkBox", "nonBrandSurface", "disabledButtonBorder"});
    @DexIgnore
    public static /* final */ ArrayList<String> r; // = qd6.a((T[]) new String[]{"nonBrandPlaceholderText", "checkBox", "dianaInactiveTab", "dianaInactiveTab"});
    @DexIgnore
    public static /* final */ ArrayList<String> s; // = qd6.a((T[]) new String[]{"onNavigationRightApplyButton", "textButtonSecondary", "navigationRightApplyButton", "navigationRightApplyButton"});
    @DexIgnore
    public static /* final */ ArrayList<String> t; // = qd6.a((T[]) new String[]{"onNavigationRightAppliedButton", "textButtonPrimary", "navigationRightAppliedButton", "navigationRightAppliedButton"});
    @DexIgnore
    public /* final */ String c; // = "FlexibleButton";
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleButton(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public final void a(AttributeSet attributeSet) {
        CharSequence text = getText();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.FlexibleButton);
            String string = obtainStyledAttributes.getString(2);
            String str = "";
            if (string == null) {
                string = str;
            }
            this.d = string;
            String string2 = obtainStyledAttributes.getString(3);
            if (string2 == null) {
                string2 = str;
            }
            this.e = string2;
            String string3 = obtainStyledAttributes.getString(0);
            if (string3 == null) {
                string3 = str;
            }
            this.f = string3;
            String string4 = obtainStyledAttributes.getString(1);
            if (string4 != null) {
                str = string4;
            }
            this.g = str;
            obtainStyledAttributes.recycle();
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087}, 0, 0);
            int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
            if (resourceId != -1) {
                text = a(resourceId);
            }
            obtainStyledAttributes2.recycle();
        }
        if (!TextUtils.isEmpty(text)) {
            setText(text);
        }
        if (!TextUtils.isEmpty(this.d) || !TextUtils.isEmpty(this.e) || TextUtils.isEmpty(this.f) || TextUtils.isEmpty(this.g)) {
            a(this.d, this.e, this.f, this.g);
        }
        setElevation(0.0f);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, Constants.STATE);
        switch (str.hashCode()) {
            case -1362481035:
                if (str.equals("flexible_button_disabled")) {
                    String str2 = o.get(0);
                    wg6.a((Object) str2, "DISABLED_BUTTON_STYLES[0]");
                    String str3 = o.get(1);
                    wg6.a((Object) str3, "DISABLED_BUTTON_STYLES[1]");
                    String str4 = o.get(2);
                    wg6.a((Object) str4, "DISABLED_BUTTON_STYLES[2]");
                    String str5 = o.get(3);
                    wg6.a((Object) str5, "DISABLED_BUTTON_STYLES[3]");
                    a(str2, str3, str4, str5);
                    return;
                }
                return;
            case -607282431:
                if (str.equals("flexible_button_right_applied")) {
                    String str6 = t.get(0);
                    wg6.a((Object) str6, "RIGHT_APPLIED_BUTTON_STYLES[0]");
                    String str7 = t.get(1);
                    wg6.a((Object) str7, "RIGHT_APPLIED_BUTTON_STYLES[1]");
                    String str8 = t.get(2);
                    wg6.a((Object) str8, "RIGHT_APPLIED_BUTTON_STYLES[2]");
                    String str9 = t.get(3);
                    wg6.a((Object) str9, "RIGHT_APPLIED_BUTTON_STYLES[3]");
                    a(str6, str7, str8, str9);
                    return;
                }
                return;
            case -582367622:
                if (str.equals("flexible_button_nonBrandSurface")) {
                    String str10 = p.get(0);
                    wg6.a((Object) str10, "nonBrandSurface_BUTTON_STYLES[0]");
                    String str11 = p.get(1);
                    wg6.a((Object) str11, "nonBrandSurface_BUTTON_STYLES[1]");
                    String str12 = p.get(2);
                    wg6.a((Object) str12, "nonBrandSurface_BUTTON_STYLES[2]");
                    String str13 = p.get(3);
                    wg6.a((Object) str13, "nonBrandSurface_BUTTON_STYLES[3]");
                    a(str10, str11, str12, str13);
                    return;
                }
                return;
            case -152730143:
                if (str.equals("flexible_button_search")) {
                    String str14 = r.get(0);
                    wg6.a((Object) str14, "SEARCH_BUTTON_STYLES[0]");
                    String str15 = r.get(1);
                    wg6.a((Object) str15, "SEARCH_BUTTON_STYLES[1]");
                    String str16 = r.get(2);
                    wg6.a((Object) str16, "SEARCH_BUTTON_STYLES[2]");
                    String str17 = r.get(3);
                    wg6.a((Object) str17, "SEARCH_BUTTON_STYLES[3]");
                    a(str14, str15, str16, str17);
                    return;
                }
                return;
            case 85932699:
                if (str.equals("flexible_button_secondary")) {
                    String str18 = j.get(0);
                    wg6.a((Object) str18, "SECONDARY_BUTTON_STYLES[0]");
                    String str19 = j.get(1);
                    wg6.a((Object) str19, "SECONDARY_BUTTON_STYLES[1]");
                    String str20 = j.get(2);
                    wg6.a((Object) str20, "SECONDARY_BUTTON_STYLES[2]");
                    String str21 = j.get(3);
                    wg6.a((Object) str21, "SECONDARY_BUTTON_STYLES[3]");
                    a(str18, str19, str20, str21);
                    return;
                }
                return;
            case 324320304:
                if (str.equals("flexible_button_connected")) {
                    String str22 = h.get(0);
                    wg6.a((Object) str22, "CONNECTED_BUTTON_STYLES[0]");
                    String str23 = h.get(1);
                    wg6.a((Object) str23, "CONNECTED_BUTTON_STYLES[1]");
                    String str24 = h.get(2);
                    wg6.a((Object) str24, "CONNECTED_BUTTON_STYLES[2]");
                    String str25 = h.get(3);
                    wg6.a((Object) str25, "CONNECTED_BUTTON_STYLES[3]");
                    a(str22, str23, str24, str25);
                    return;
                }
                return;
            case 625065714:
                if (str.equals("flexible_button_right_apply")) {
                    String str26 = s.get(0);
                    wg6.a((Object) str26, "RIGHT_APPLY_BUTTON_STYLES[0]");
                    String str27 = s.get(1);
                    wg6.a((Object) str27, "RIGHT_APPLY_BUTTON_STYLES[1]");
                    String str28 = s.get(2);
                    wg6.a((Object) str28, "RIGHT_APPLY_BUTTON_STYLES[2]");
                    String str29 = s.get(3);
                    wg6.a((Object) str29, "RIGHT_APPLY_BUTTON_STYLES[3]");
                    a(str26, str27, str28, str29);
                    return;
                }
                return;
            case 1076923322:
                if (str.equals("flexible_button_checkbox_typeface")) {
                    String str30 = q.get(0);
                    wg6.a((Object) str30, "CHECKBOX_BUTTON_STYLES[0]");
                    String str31 = q.get(1);
                    wg6.a((Object) str31, "CHECKBOX_BUTTON_STYLES[1]");
                    String str32 = q.get(2);
                    wg6.a((Object) str32, "CHECKBOX_BUTTON_STYLES[2]");
                    String str33 = q.get(3);
                    wg6.a((Object) str33, "CHECKBOX_BUTTON_STYLES[3]");
                    a(str30, str31, str32, str33);
                    return;
                }
                return;
            case 1572205801:
                if (str.equals("flexible_button_primary")) {
                    String str34 = i.get(0);
                    wg6.a((Object) str34, "PRIMARY_BUTTON_STYLES[0]");
                    String str35 = i.get(1);
                    wg6.a((Object) str35, "PRIMARY_BUTTON_STYLES[1]");
                    String str36 = i.get(2);
                    wg6.a((Object) str36, "PRIMARY_BUTTON_STYLES[2]");
                    String str37 = i.get(3);
                    wg6.a((Object) str37, "PRIMARY_BUTTON_STYLES[3]");
                    a(str34, str35, str36, str37);
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public final void a(String str, String str2, String str3, String str4) {
        wg6.b(str, "textColorStyle");
        wg6.b(str2, "textFontStyle");
        wg6.b(str3, "backgroundColorStyle");
        wg6.b(str4, "borderColorStyle");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = this.c;
        local.d(str5, "setStyle textColorStyle=" + str + " textFontStyle=" + str2 + ' ' + "backgroundColorStyle=" + str3 + " borderColorStyle=" + str4);
        String b = ThemeManager.l.a().b(str);
        Typeface c2 = ThemeManager.l.a().c(str2);
        String b2 = ThemeManager.l.a().b(str3);
        String b3 = ThemeManager.l.a().b(str4);
        if (b != null) {
            setTextColor(Color.parseColor(b));
        }
        if (c2 != null) {
            setTypeface(c2);
        }
        if (b2 != null) {
            setBackgroundColor(Color.parseColor(b2));
        }
        if (b3 != null) {
            LayerDrawable layerDrawable = (LayerDrawable) w6.c(getContext(), 2131230866);
            GradientDrawable gradientDrawable = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131362942) : null);
            if (!(b2 == null || gradientDrawable == null)) {
                gradientDrawable.setColor(Color.parseColor(b2));
            }
            if (gradientDrawable != null) {
                gradientDrawable.setStroke(4, Color.parseColor(b3));
            }
            setBackground(layerDrawable);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(int i2) {
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), i2);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return a2;
    }
}
