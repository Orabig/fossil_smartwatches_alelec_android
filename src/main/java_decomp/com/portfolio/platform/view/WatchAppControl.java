package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.jj4;
import com.fossil.qc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppControl extends ConstraintLayout {
    @DexIgnore
    public Drawable A;
    @DexIgnore
    public String B;
    @DexIgnore
    public String C;
    @DexIgnore
    public int D;
    @DexIgnore
    public String E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public /* final */ ColorDrawable K;
    @DexIgnore
    public ImageView u;
    @DexIgnore
    public TextView v;
    @DexIgnore
    public TextView w;
    @DexIgnore
    public /* final */ int x; // = Color.parseColor("#FFFF00");
    @DexIgnore
    public Drawable y;
    @DexIgnore
    public Drawable z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.view.WatchAppControl, android.view.ViewGroup] */
    public WatchAppControl(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int i = this.x;
        this.D = i;
        this.F = i;
        this.G = i;
        this.H = i;
        this.K = new ColorDrawable(w6.a(getContext(), R.color.transparent));
        TypedArray typedArray = null;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        View inflate = layoutInflater != null ? layoutInflater.inflate(2131558807, this, true) : null;
        if (inflate != null) {
            View findViewById = inflate.findViewById(2131362602);
            wg6.a((Object) findViewById, "view.findViewById(R.id.iv_icon)");
            this.u = (ImageView) findViewById;
            View findViewById2 = inflate.findViewById(2131363148);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.tv_end)");
            this.v = (TextView) findViewById2;
            View findViewById3 = inflate.findViewById(2131363101);
            wg6.a((Object) findViewById3, "view.findViewById(R.id.tv_bottom)");
            this.w = (TextView) findViewById3;
            typedArray = context != null ? context.obtainStyledAttributes(attributeSet, x24.WatchAppControl) : typedArray;
            if (typedArray != null) {
                this.y = typedArray.getDrawable(8);
                this.z = typedArray.getDrawable(9);
                this.A = typedArray.getDrawable(10);
                this.C = typedArray.getString(5);
                this.D = typedArray.getColor(6, this.x);
                typedArray.getDimension(7, -1.0f);
                this.E = typedArray.getString(0);
                this.F = typedArray.getColor(1, this.x);
                typedArray.getDimension(2, -1.0f);
                this.G = typedArray.getColor(3, this.x);
                this.H = typedArray.getColor(4, this.x);
                this.I = typedArray.getBoolean(12, false);
                this.J = typedArray.getBoolean(11, false);
                setRemoveMode(this.J);
                typedArray.recycle();
                return;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    private final void setIconSrc(Drawable drawable) {
        this.A = drawable;
        if (drawable != null) {
            this.u.setImageDrawable(this.A);
            this.u.setImageTintList(ColorStateList.valueOf(this.H));
        }
    }

    @DexIgnore
    private final void setRemoveMode(boolean z2) {
        this.J = z2;
        if (this.J) {
            this.u.setVisibility(8);
            this.v.setVisibility(8);
            this.w.setVisibility(8);
            return;
        }
        d();
    }

    @DexIgnore
    public final void d() {
        if (!TextUtils.isEmpty(this.B)) {
            qc6.a(jj4.a((View) this.u).a(this.B).b(), this.u);
        } else {
            Drawable drawable = this.A;
            if (drawable != null) {
                this.u.setImageDrawable(drawable);
                this.u.setImageTintList(ColorStateList.valueOf(this.H));
            }
        }
        if (!TextUtils.isEmpty(this.C)) {
            this.v.setText(this.C);
            this.v.setVisibility(0);
        } else {
            this.v.setVisibility(8);
        }
        if (!TextUtils.isEmpty(this.E)) {
            this.w.setText(this.E);
            this.w.setVisibility(0);
        } else {
            this.w.setVisibility(8);
        }
        if (this.I) {
            ImageView imageView = this.u;
            Drawable drawable2 = this.z;
            if (drawable2 == null) {
                drawable2 = this.y;
            }
            if (drawable2 == null) {
                drawable2 = this.K;
            }
            imageView.setBackground(drawable2);
            int i = this.H;
            int i2 = this.x;
            if (i != i2) {
                this.v.setTextColor(i);
            } else {
                int i3 = this.G;
                if (i3 != i2) {
                    this.v.setTextColor(i3);
                } else {
                    int i4 = this.D;
                    if (i4 != i2) {
                        this.v.setTextColor(i4);
                    }
                }
            }
            int i5 = this.H;
            int i6 = this.x;
            if (i5 != i6) {
                this.w.setTextColor(i5);
                return;
            }
            int i7 = this.G;
            if (i7 != i6) {
                this.w.setTextColor(i7);
                return;
            }
            int i8 = this.F;
            if (i8 != i6) {
                this.w.setTextColor(i8);
                return;
            }
            return;
        }
        ImageView imageView2 = this.u;
        Drawable drawable3 = this.y;
        if (drawable3 == null) {
            drawable3 = this.K;
        }
        imageView2.setBackground(drawable3);
        int i9 = this.G;
        int i10 = this.x;
        if (i9 != i10) {
            this.v.setTextColor(i9);
        } else {
            int i11 = this.D;
            if (i11 != i10) {
                this.v.setTextColor(i11);
            }
        }
        int i12 = this.G;
        int i13 = this.x;
        if (i12 != i13) {
            this.w.setTextColor(i12);
            return;
        }
        int i14 = this.F;
        if (i14 != i13) {
            this.w.setTextColor(i14);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.WatchAppControl, android.view.ViewGroup] */
    public final void setDragMode(boolean z2) {
        if (z2) {
            setAlpha(0.0f);
        } else {
            setAlpha(1.0f);
        }
    }

    @DexIgnore
    public final void setSelectedWc(boolean z2) {
        this.I = z2;
        setRemoveMode(this.J);
    }
}
