package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Scroller;
import androidx.appcompat.widget.AppCompatEditText;
import com.fossil.x24;
import com.j256.ormlite.logger.Logger;
import com.portfolio.platform.manager.ThemeManager;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NumberPicker extends LinearLayout {
    @DexIgnore
    public /* final */ Drawable A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public /* final */ Scroller E;
    @DexIgnore
    public /* final */ Scroller F;
    @DexIgnore
    public int G;
    @DexIgnore
    public e H;
    @DexIgnore
    public d I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public VelocityTracker L;
    @DexIgnore
    public int M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public /* final */ int Q;
    @DexIgnore
    public /* final */ boolean R;
    @DexIgnore
    public /* final */ Drawable S;
    @DexIgnore
    public /* final */ int T;
    @DexIgnore
    public int U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public boolean W;
    @DexIgnore
    public String a;
    @DexIgnore
    public int a0;
    @DexIgnore
    public String b;
    @DexIgnore
    public int b0;
    @DexIgnore
    public /* final */ ImageButton c;
    @DexIgnore
    public int c0;
    @DexIgnore
    public /* final */ ImageButton d;
    @DexIgnore
    public boolean d0;
    @DexIgnore
    public /* final */ AppCompatEditText e;
    @DexIgnore
    public boolean e0;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public i f0;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ h g0;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public int h0;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public String[] r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public g v;
    @DexIgnore
    public f w;
    @DexIgnore
    public /* final */ SparseArray<String> x;
    @DexIgnore
    public /* final */ int[] y;
    @DexIgnore
    public /* final */ Paint z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class CustomEditText extends AppCompatEditText {
        @DexIgnore
        public CustomEditText(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.NumberPicker$CustomEditText, android.widget.EditText] */
        public void onEditorAction(int i) {
            NumberPicker.super.onEditorAction(i);
            if (i == 6) {
                clearFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            NumberPicker.this.b();
            NumberPicker.this.e.clearFocus();
            if (view.getId() == 2131362745) {
                NumberPicker.this.a(true);
            } else {
                NumberPicker.this.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnLongClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            NumberPicker.this.b();
            NumberPicker.this.e.clearFocus();
            if (view.getId() == 2131362745) {
                NumberPicker.this.a(true, 0);
            } else {
                NumberPicker.this.a(false, 0);
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            NumberPicker.this.j();
            NumberPicker.this.V = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public boolean a;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public void run() {
            NumberPicker.this.a(this.a);
            NumberPicker.this.postDelayed(this, 300);
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        String format(int i);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(NumberPicker numberPicker, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i {
        @DexIgnore
        public c a;

        @DexIgnore
        public i(NumberPicker numberPicker) {
            this.a = new c();
        }

        @DexIgnore
        public boolean a(int i, int i2, Bundle bundle) {
            c cVar = this.a;
            return cVar != null && cVar.performAction(i, i2, bundle);
        }

        @DexIgnore
        public void a(int i, int i2) {
            c cVar = this.a;
            if (cVar != null) {
                cVar.a(i, i2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j implements f {
        @DexIgnore
        public /* final */ StringBuilder a; // = new StringBuilder();
        @DexIgnore
        public char b;
        @DexIgnore
        public transient Formatter c;
        @DexIgnore
        public /* final */ Object[] d; // = new Object[1];

        @DexIgnore
        public j() {
            b(Locale.getDefault());
        }

        @DexIgnore
        public static char c(Locale locale) {
            return new DecimalFormatSymbols(locale).getZeroDigit();
        }

        @DexIgnore
        public final Formatter a(Locale locale) {
            return new Formatter(this.a, locale);
        }

        @DexIgnore
        public final void b(Locale locale) {
            this.c = a(locale);
            this.b = c(locale);
        }

        @DexIgnore
        public String format(int i) {
            Locale locale = Locale.getDefault();
            if (this.b != c(locale)) {
                b(locale);
            }
            this.d[0] = Integer.valueOf(i);
            StringBuilder sb = this.a;
            sb.delete(0, sb.length());
            this.c.format("%02d", this.d);
            return this.c.toString();
        }
    }

    /*
    static {
        new j();
    }
    */

    @DexIgnore
    public NumberPicker(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    private i getSupportAccessibilityNodeProvider() {
        return new i(this);
    }

    @DexIgnore
    public static int resolveSizeAndState(int i2, int i3, int i4) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode != 0 && mode == 1073741824) {
                i2 = size;
            }
        } else if (size < i2) {
            i2 = 16777216 | size;
        }
        return i2 | (-16777216 & i4);
    }

    @DexIgnore
    public final boolean a(Scroller scroller) {
        scroller.forceFinished(true);
        int finalY = scroller.getFinalY() - scroller.getCurrY();
        int i2 = this.C - ((this.D + finalY) % this.B);
        if (i2 == 0) {
            return false;
        }
        int abs = Math.abs(i2);
        int i3 = this.B;
        if (abs > i3 / 2) {
            i2 = i2 > 0 ? i2 - i3 : i2 + i3;
        }
        scrollBy(0, finalY + i2);
        return true;
    }

    @DexIgnore
    public void b() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive(this.e)) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.R) {
                this.e.setVisibility(4);
            }
        }
    }

    @DexIgnore
    public final void c() {
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(((getBottom() - getTop()) - this.p) / 2);
    }

    @DexIgnore
    public void computeScroll() {
        Scroller scroller = this.E;
        if (scroller.isFinished()) {
            scroller = this.F;
            if (scroller.isFinished()) {
                return;
            }
        }
        scroller.computeScrollOffset();
        int currY = scroller.getCurrY();
        if (this.G == 0) {
            this.G = scroller.getStartY();
        }
        scrollBy(0, currY - this.G);
        this.G = currY;
        if (scroller.isFinished()) {
            b(scroller);
        } else {
            invalidate();
        }
    }

    @DexIgnore
    public final void d() {
        e();
        int[] iArr = this.y;
        this.q = (int) ((((float) ((getBottom() - getTop()) - (iArr.length * this.p))) / ((float) iArr.length)) + 0.5f);
        this.B = this.p + this.q;
        this.C = (this.e.getBaseline() + this.e.getTop()) - (this.B * 1);
        this.D = this.C;
        l();
    }

    @DexIgnore
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        int i2;
        if (!this.R) {
            return super.dispatchHoverEvent(motionEvent);
        }
        if (!((AccessibilityManager) getContext().getSystemService("accessibility")).isEnabled()) {
            return false;
        }
        int y2 = (int) motionEvent.getY();
        if (y2 < this.a0) {
            i2 = 3;
        } else {
            i2 = y2 > this.b0 ? 1 : 2;
        }
        int action = motionEvent.getAction() & 255;
        i supportAccessibilityNodeProvider = getSupportAccessibilityNodeProvider();
        if (action == 7) {
            int i3 = this.c0;
            if (i3 == i2 || i3 == -1) {
                return false;
            }
            supportAccessibilityNodeProvider.a(i3, 256);
            supportAccessibilityNodeProvider.a(i2, Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            this.c0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, (Bundle) null);
            return false;
        } else if (action == 9) {
            supportAccessibilityNodeProvider.a(i2, Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            this.c0 = i2;
            supportAccessibilityNodeProvider.a(i2, 64, (Bundle) null);
            return false;
        } else if (action != 10) {
            return false;
        } else {
            supportAccessibilityNodeProvider.a(i2, 256);
            this.c0 = -1;
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0044, code lost:
        requestFocus();
        r5.h0 = r0;
        g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        if (r5.E.isFinished() == false) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        if (r0 != 20) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0056, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0058, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005c, code lost:
        return true;
     */
    @DexIgnore
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyCode == 19 || keyCode == 20) {
            if (this.R) {
                int action = keyEvent.getAction();
                if (action != 0) {
                    if (action == 1 && this.h0 == keyCode) {
                        this.h0 = -1;
                        return true;
                    }
                } else if (!this.P) {
                }
            }
        } else if (keyCode == 23 || keyCode == 66) {
            g();
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @DexIgnore
    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            g();
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    @DexIgnore
    public final void e() {
        this.x.clear();
        int[] iArr = this.y;
        int i2 = this.u;
        for (int i3 = 0; i3 < this.y.length; i3++) {
            int i4 = (i3 - 1) + i2;
            if (this.P) {
                i4 = d(i4);
            }
            iArr[i3] = i4;
            a(iArr[i3]);
        }
    }

    @DexIgnore
    public final void f(int i2) {
        if (this.U != i2) {
            this.U = i2;
        }
    }

    @DexIgnore
    public final void g() {
        e eVar = this.H;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
        d dVar = this.I;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
        this.g0.a();
    }

    @DexIgnore
    public AccessibilityNodeProvider getAccessibilityNodeProvider() {
        if (!this.R) {
            return super.getAccessibilityNodeProvider();
        }
        if (this.f0 == null) {
            this.f0 = new i(this);
        }
        return this.f0.a;
    }

    @DexIgnore
    public float getBottomFadingEdgeStrength() {
        return 0.9f;
    }

    @DexIgnore
    public int getMaxValue() {
        return this.t;
    }

    @DexIgnore
    public int getMinValue() {
        return this.s;
    }

    @DexIgnore
    public int getSolidColor() {
        return this.Q;
    }

    @DexIgnore
    public float getTopFadingEdgeStrength() {
        return 0.9f;
    }

    @DexIgnore
    public int getValue() {
        return this.u;
    }

    @DexIgnore
    public boolean getWrapSelectorWheel() {
        return this.P;
    }

    @DexIgnore
    public final void h() {
        d dVar = this.I;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
    }

    @DexIgnore
    public final void i() {
        e eVar = this.H;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
    }

    @DexIgnore
    public void j() {
    }

    @DexIgnore
    public final void k() {
        int i2;
        if (this.o) {
            String[] strArr = this.r;
            int i3 = 0;
            if (strArr == null) {
                float f2 = 0.0f;
                for (int i4 = 0; i4 <= 9; i4++) {
                    float measureText = this.z.measureText(g(i4));
                    if (measureText > f2) {
                        f2 = measureText;
                    }
                }
                for (int i5 = this.t; i5 > 0; i5 /= 10) {
                    i3++;
                }
                i2 = (int) (((float) i3) * f2);
            } else {
                int length = strArr.length;
                int i6 = 0;
                while (i3 < length) {
                    float measureText2 = this.z.measureText(strArr[i3]);
                    if (measureText2 > ((float) i6)) {
                        i6 = (int) measureText2;
                    }
                    i3++;
                }
                i2 = i6;
            }
            int paddingLeft = i2 + this.e.getPaddingLeft() + this.e.getPaddingRight();
            if (this.j != paddingLeft) {
                int i7 = this.i;
                if (paddingLeft > i7) {
                    this.j = paddingLeft;
                } else {
                    this.j = i7;
                }
                invalidate();
            }
        }
    }

    @DexIgnore
    public final boolean l() {
        String[] strArr = this.r;
        String c2 = strArr == null ? c(this.u) : strArr[this.u - this.s];
        if (TextUtils.isEmpty(c2) || c2.equals(this.e.getText().toString())) {
            return false;
        }
        this.e.setText(c2);
        return true;
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        g();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.R) {
            super.onDraw(canvas);
            return;
        }
        float right = (float) ((getRight() - getLeft()) / 2);
        float f2 = (float) this.D;
        Drawable drawable = this.A;
        if (drawable != null && this.U == 0) {
            if (this.e0) {
                drawable.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.A.setBounds(0, 0, getRight(), this.a0);
                this.A.draw(canvas);
            }
            if (this.d0) {
                this.A.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.A.setBounds(0, this.b0, getRight(), getBottom());
                this.A.draw(canvas);
            }
        }
        int[] iArr = this.y;
        float f3 = f2;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            String str = this.x.get(iArr[i2]);
            if (i2 != 1 || this.e.getVisibility() != 0) {
                a(this.z, (float) getWidth(), str);
                canvas.drawText(str, right, f3, this.z);
            }
            f3 += (float) this.B;
        }
        Drawable drawable2 = this.S;
        if (drawable2 != null) {
            int i3 = this.a0;
            drawable2.setBounds(0, i3, getRight(), this.T + i3);
            this.S.draw(canvas);
            int i4 = this.b0;
            this.S.setBounds(0, i4 - this.T, getRight(), i4);
            this.S.draw(canvas);
        }
    }

    @DexIgnore
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(NumberPicker.class.getName());
        accessibilityEvent.setScrollable(true);
        accessibilityEvent.setScrollY((this.s + this.u) * this.B);
        accessibilityEvent.setMaxScrollY((this.t - this.s) * this.B);
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.R || !isEnabled() || (motionEvent.getAction() & 255) != 0) {
            return false;
        }
        g();
        this.e.setVisibility(4);
        float y2 = motionEvent.getY();
        this.J = y2;
        this.K = y2;
        this.V = false;
        this.W = false;
        float f2 = this.J;
        if (f2 < ((float) this.a0)) {
            if (this.U == 0) {
                this.g0.a(2);
            }
        } else if (f2 > ((float) this.b0) && this.U == 0) {
            this.g0.a(1);
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        if (!this.E.isFinished()) {
            this.E.forceFinished(true);
            this.F.forceFinished(true);
            f(0);
        } else if (!this.F.isFinished()) {
            this.E.forceFinished(true);
            this.F.forceFinished(true);
        } else {
            float f3 = this.J;
            if (f3 < ((float) this.a0)) {
                b();
                a(false, (long) ViewConfiguration.getLongPressTimeout());
            } else if (f3 > ((float) this.b0)) {
                b();
                a(true, (long) ViewConfiguration.getLongPressTimeout());
            } else {
                this.W = true;
                f();
            }
        }
        return true;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (!this.R) {
            super.onLayout(z2, i2, i3, i4, i5);
            return;
        }
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int measuredWidth2 = this.e.getMeasuredWidth();
        int measuredHeight2 = this.e.getMeasuredHeight();
        int i6 = (measuredWidth - measuredWidth2) / 2;
        int i7 = (measuredHeight - measuredHeight2) / 2;
        this.e.layout(i6, i7, measuredWidth2 + i6, measuredHeight2 + i7);
        if (z2) {
            d();
            c();
            int height = getHeight();
            int i8 = this.f;
            int i9 = this.T;
            this.a0 = ((height - i8) / 2) - i9;
            this.b0 = this.a0 + (i9 * 2) + i8;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (!this.R) {
            super.onMeasure(i2, i3);
            return;
        }
        super.onMeasure(a(i2, this.j), a(i3, this.h));
        setMeasuredDimension(a(this.i, getMeasuredWidth(), i2), a(this.g, getMeasuredHeight(), i3));
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled() || !this.R) {
            return false;
        }
        if (this.L == null) {
            this.L = VelocityTracker.obtain();
        }
        this.L.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 1) {
            h();
            i();
            this.g0.a();
            VelocityTracker velocityTracker = this.L;
            velocityTracker.computeCurrentVelocity(1000, (float) this.O);
            int yVelocity = (int) velocityTracker.getYVelocity();
            if (Math.abs(yVelocity) > this.N) {
                b(yVelocity);
                f(2);
            } else {
                int y2 = (int) motionEvent.getY();
                if (((int) Math.abs(((float) y2) - this.J)) > this.M) {
                    a();
                } else if (this.W) {
                    this.W = false;
                    j();
                } else {
                    int i2 = (y2 / this.B) - 1;
                    if (i2 > 0) {
                        a(true);
                        this.g0.b(1);
                    } else if (i2 < 0) {
                        a(false);
                        this.g0.b(2);
                    }
                }
                f(0);
            }
            this.L.recycle();
            this.L = null;
        } else if (action == 2 && !this.V) {
            float y3 = motionEvent.getY();
            if (this.U == 1) {
                scrollBy(0, (int) (y3 - this.K));
                invalidate();
            } else if (((int) Math.abs(y3 - this.J)) > this.M) {
                g();
                f(1);
            }
            this.K = y3;
        }
        return true;
    }

    @DexIgnore
    public void scrollBy(int i2, int i3) {
        int[] iArr = this.y;
        if (!this.P && i3 > 0 && iArr[1] <= this.s) {
            this.D = this.C;
        } else if (this.P || i3 >= 0 || iArr[1] < this.t) {
            this.D += i3;
            while (true) {
                int i4 = this.D;
                if (i4 - this.C <= this.q) {
                    break;
                }
                this.D = i4 - this.B;
                a(iArr);
                a(iArr[1], true);
                if (!this.P && iArr[1] <= this.s) {
                    this.D = this.C;
                }
            }
            while (true) {
                int i5 = this.D;
                if (i5 - this.C < (-this.q)) {
                    this.D = i5 + this.B;
                    b(iArr);
                    a(iArr[1], true);
                    if (!this.P && iArr[1] >= this.t) {
                        this.D = this.C;
                    }
                } else {
                    return;
                }
            }
        } else {
            this.D = this.C;
        }
    }

    @DexIgnore
    public void setDisplayedValues(String[] strArr) {
        if (!Arrays.equals(this.r, strArr)) {
            this.r = strArr;
            if (this.r != null) {
                this.e.setRawInputType(524289);
            } else {
                this.e.setRawInputType(2);
            }
            l();
            e();
            k();
        }
    }

    @DexIgnore
    public void setEnabled(boolean z2) {
        ImageButton imageButton;
        ImageButton imageButton2;
        super.setEnabled(z2);
        if (!this.R && (imageButton2 = this.c) != null) {
            imageButton2.setEnabled(z2);
        }
        if (!this.R && (imageButton = this.d) != null) {
            imageButton.setEnabled(z2);
        }
        this.e.setEnabled(z2);
    }

    @DexIgnore
    public void setMaxValue(int i2) {
        if (this.t != i2) {
            if (i2 >= 0) {
                this.t = i2;
                int i3 = this.t;
                if (i3 < this.u) {
                    this.u = i3;
                }
                setWrapSelectorWheel(this.t - this.s > this.y.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("maxValue must be >= 0");
        }
    }

    @DexIgnore
    public void setMinValue(int i2) {
        if (this.s != i2) {
            if (i2 >= 0) {
                this.s = i2;
                int i3 = this.s;
                if (i3 > this.u) {
                    this.u = i3;
                }
                setWrapSelectorWheel(this.t - this.s > this.y.length);
                e();
                l();
                k();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("minValue must be >= 0");
        }
    }

    @DexIgnore
    public void setOnValueChangedListener(g gVar) {
        this.v = gVar;
    }

    @DexIgnore
    public void setValue(int i2) {
        a(i2, false);
    }

    @DexIgnore
    public void setWrapSelectorWheel(boolean z2) {
        boolean z3 = this.t - this.s >= this.y.length;
        if ((!z2 || z3) && z2 != this.P) {
            this.P = z2;
        }
    }

    @DexIgnore
    public NumberPicker(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969649);
    }

    @DexIgnore
    public NumberPicker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        Typeface c2;
        String b2;
        int i3;
        int i4;
        this.x = new SparseArray<>();
        this.y = new int[3];
        this.C = Integer.MIN_VALUE;
        boolean z2 = false;
        this.U = 0;
        this.h0 = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.NumberPicker, i2, 0);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        this.R = resourceId != 0;
        obtainStyledAttributes.getColor(11, 0);
        this.Q = obtainStyledAttributes.getColor(15, 0);
        this.S = obtainStyledAttributes.getDrawable(12);
        this.a = obtainStyledAttributes.getString(7);
        this.b = obtainStyledAttributes.getString(8);
        this.T = obtainStyledAttributes.getDimensionPixelSize(13, (int) TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics()));
        this.f = obtainStyledAttributes.getDimensionPixelSize(14, (int) TypedValue.applyDimension(1, 48.0f, getResources().getDisplayMetrics()));
        this.g = obtainStyledAttributes.getDimensionPixelSize(3, -1);
        this.h = obtainStyledAttributes.getDimensionPixelSize(1, -1);
        int i5 = this.g;
        if (i5 == -1 || (i4 = this.h) == -1 || i5 <= i4) {
            this.i = obtainStyledAttributes.getDimensionPixelSize(4, -1);
            this.j = obtainStyledAttributes.getDimensionPixelSize(2, -1);
            int i6 = this.i;
            if (i6 == -1 || (i3 = this.j) == -1 || i6 <= i3) {
                this.o = this.j == -1 ? true : z2;
                this.A = obtainStyledAttributes.getDrawable(17);
                obtainStyledAttributes.recycle();
                this.g0 = new h();
                setWillNotDraw(!this.R);
                LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
                if (layoutInflater != null) {
                    layoutInflater.inflate(resourceId, this, true);
                }
                a aVar = new a();
                b bVar = new b();
                if (!this.R) {
                    this.c = (ImageButton) findViewById(2131362745);
                    this.c.setOnClickListener(aVar);
                    this.c.setOnLongClickListener(bVar);
                } else {
                    this.c = null;
                }
                if (!this.R) {
                    this.d = (ImageButton) findViewById(2131362744);
                    this.d.setOnClickListener(aVar);
                    this.d.setOnLongClickListener(bVar);
                } else {
                    this.d = null;
                }
                this.e = findViewById(2131362746);
                if (!TextUtils.isEmpty(this.a) && (b2 = ThemeManager.l.a().b(this.a)) != null) {
                    this.e.setTextColor(Color.parseColor(b2));
                }
                if (!TextUtils.isEmpty(this.b) && (c2 = ThemeManager.l.a().c(this.b)) != null) {
                    this.e.setTypeface(c2);
                }
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                this.M = viewConfiguration.getScaledTouchSlop();
                this.N = viewConfiguration.getScaledMinimumFlingVelocity();
                this.O = viewConfiguration.getScaledMaximumFlingVelocity() / 8;
                this.p = (int) this.e.getTextSize();
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                paint.setTextAlign(Paint.Align.CENTER);
                paint.setTextSize((float) this.p);
                paint.setTypeface(this.e.getTypeface());
                paint.setColor(this.e.getTextColors().getColorForState(LinearLayout.ENABLED_STATE_SET, -1));
                this.z = paint;
                this.E = new Scroller(getContext(), (Interpolator) null, true);
                this.F = new Scroller(getContext(), new DecelerateInterpolator(2.5f));
                l();
                if (getImportantForAccessibility() == 0) {
                    setImportantForAccessibility(1);
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("minWidth > maxWidth");
        }
        throw new IllegalArgumentException("minHeight > maxHeight");
    }

    @DexIgnore
    public String c(int i2) {
        f fVar = this.w;
        return fVar != null ? fVar.format(i2) : g(i2);
    }

    @DexIgnore
    public final void f() {
        d dVar = this.I;
        if (dVar == null) {
            this.I = new d();
        } else {
            removeCallbacks(dVar);
        }
        postDelayed(this.I, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Runnable {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;

        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void a() {
            this.b = 0;
            this.a = 0;
            NumberPicker.this.removeCallbacks(this);
            NumberPicker numberPicker = NumberPicker.this;
            if (numberPicker.d0) {
                numberPicker.d0 = false;
                numberPicker.invalidate(0, numberPicker.b0, numberPicker.getRight(), NumberPicker.this.getBottom());
            }
        }

        @DexIgnore
        public void b(int i) {
            a();
            this.b = 2;
            this.a = i;
            NumberPicker.this.post(this);
        }

        @DexIgnore
        public void run() {
            int i = this.b;
            if (i == 1) {
                int i2 = this.a;
                if (i2 == 1) {
                    NumberPicker numberPicker = NumberPicker.this;
                    numberPicker.d0 = true;
                    numberPicker.invalidate(0, numberPicker.b0, numberPicker.getRight(), NumberPicker.this.getBottom());
                } else if (i2 == 2) {
                    NumberPicker numberPicker2 = NumberPicker.this;
                    numberPicker2.e0 = true;
                    numberPicker2.invalidate(0, 0, numberPicker2.getRight(), NumberPicker.this.a0);
                }
            } else if (i == 2) {
                int i3 = this.a;
                if (i3 == 1) {
                    NumberPicker numberPicker3 = NumberPicker.this;
                    if (!numberPicker3.d0) {
                        numberPicker3.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPicker numberPicker4 = NumberPicker.this;
                    numberPicker4.d0 = !numberPicker4.d0;
                    numberPicker4.invalidate(0, numberPicker4.b0, numberPicker4.getRight(), NumberPicker.this.getBottom());
                } else if (i3 == 2) {
                    NumberPicker numberPicker5 = NumberPicker.this;
                    if (!numberPicker5.e0) {
                        numberPicker5.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPicker numberPicker6 = NumberPicker.this;
                    numberPicker6.e0 = !numberPicker6.e0;
                    numberPicker6.invalidate(0, 0, numberPicker6.getRight(), NumberPicker.this.a0);
                }
            }
        }

        @DexIgnore
        public void a(int i) {
            a();
            this.b = 1;
            this.a = i;
            NumberPicker.this.postDelayed(this, (long) ViewConfiguration.getTapTimeout());
        }
    }

    @DexIgnore
    public static String g(int i2) {
        return String.format(Locale.getDefault(), "%d", new Object[]{Integer.valueOf(i2)});
    }

    @DexIgnore
    public final void b(Scroller scroller) {
        if (Objects.equals(scroller, this.E)) {
            if (!a()) {
                l();
            }
            f(0);
        } else if (this.U != 1) {
            l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends AccessibilityNodeProvider {
        @DexIgnore
        public /* final */ Rect a; // = new Rect();
        @DexIgnore
        public /* final */ int[] b; // = new int[2];
        @DexIgnore
        public int c; // = Integer.MIN_VALUE;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(int i, int i2) {
            if (i != 1) {
                if (i == 2) {
                    a(i2);
                } else if (i == 3 && d()) {
                    a(i, i2, b());
                }
            } else if (e()) {
                a(i, i2, c());
            }
        }

        @DexIgnore
        public final String b() {
            NumberPicker numberPicker = NumberPicker.this;
            int i = numberPicker.u - 1;
            if (numberPicker.P) {
                i = numberPicker.d(i);
            }
            NumberPicker numberPicker2 = NumberPicker.this;
            int i2 = numberPicker2.s;
            if (i < i2) {
                return null;
            }
            String[] strArr = numberPicker2.r;
            return strArr == null ? numberPicker2.c(i) : strArr[i - i2];
        }

        @DexIgnore
        public final String c() {
            NumberPicker numberPicker = NumberPicker.this;
            int i = numberPicker.u + 1;
            if (numberPicker.P) {
                i = numberPicker.d(i);
            }
            NumberPicker numberPicker2 = NumberPicker.this;
            if (i > numberPicker2.t) {
                return null;
            }
            String[] strArr = numberPicker2.r;
            return strArr == null ? numberPicker2.c(i) : strArr[i - numberPicker2.s];
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            if (i == -1) {
                return a(NumberPicker.this.getScrollX(), NumberPicker.this.getScrollY(), NumberPicker.this.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft()), NumberPicker.this.getScrollY() + (NumberPicker.this.getBottom() - NumberPicker.this.getTop()));
            }
            if (i == 1) {
                String c2 = c();
                int scrollX = NumberPicker.this.getScrollX();
                NumberPicker numberPicker = NumberPicker.this;
                return a(1, c2, scrollX, numberPicker.b0 - numberPicker.T, numberPicker.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft()), NumberPicker.this.getScrollY() + (NumberPicker.this.getBottom() - NumberPicker.this.getTop()));
            } else if (i == 2) {
                return a();
            } else {
                if (i != 3) {
                    return super.createAccessibilityNodeInfo(i);
                }
                String b2 = b();
                int scrollX2 = NumberPicker.this.getScrollX();
                int scrollY = NumberPicker.this.getScrollY();
                int scrollX3 = NumberPicker.this.getScrollX() + (NumberPicker.this.getRight() - NumberPicker.this.getLeft());
                NumberPicker numberPicker2 = NumberPicker.this;
                return a(3, b2, scrollX2, scrollY, scrollX3, numberPicker2.a0 + numberPicker2.T);
            }
        }

        @DexIgnore
        public final boolean d() {
            return NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue();
        }

        @DexIgnore
        public final boolean e() {
            return NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue();
        }

        @DexIgnore
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            if (TextUtils.isEmpty(str)) {
                return Collections.emptyList();
            }
            String lowerCase = str.toLowerCase();
            ArrayList arrayList = new ArrayList();
            if (i == -1) {
                a(lowerCase, 3, (List<AccessibilityNodeInfo>) arrayList);
                a(lowerCase, 2, (List<AccessibilityNodeInfo>) arrayList);
                a(lowerCase, 1, (List<AccessibilityNodeInfo>) arrayList);
                return arrayList;
            } else if (i != 1 && i != 2 && i != 3) {
                return super.findAccessibilityNodeInfosByText(str, i);
            } else {
                a(lowerCase, i, (List<AccessibilityNodeInfo>) arrayList);
                return arrayList;
            }
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            if (i != -1) {
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128 || this.c != i) {
                                        return false;
                                    }
                                    this.c = Integer.MIN_VALUE;
                                    a(i, 65536);
                                    NumberPicker numberPicker = NumberPicker.this;
                                    numberPicker.invalidate(0, 0, numberPicker.getRight(), NumberPicker.this.a0);
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPicker numberPicker2 = NumberPicker.this;
                                    numberPicker2.invalidate(0, 0, numberPicker2.getRight(), NumberPicker.this.a0);
                                    return true;
                                }
                            } else if (!NumberPicker.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPicker.this.a(false);
                                a(i, 1);
                                return true;
                            }
                        }
                    } else if (i2 != 1) {
                        if (i2 != 2) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128) {
                                        return NumberPicker.this.e.performAccessibilityAction(i2, bundle);
                                    }
                                    if (this.c != i) {
                                        return false;
                                    }
                                    this.c = Integer.MIN_VALUE;
                                    a(i, 65536);
                                    NumberPicker.this.e.invalidate();
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    a(i, 32768);
                                    NumberPicker.this.e.invalidate();
                                    return true;
                                }
                            } else if (!NumberPicker.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPicker.this.j();
                                return true;
                            }
                        } else if (!NumberPicker.this.isEnabled() || !NumberPicker.this.e.isFocused()) {
                            return false;
                        } else {
                            NumberPicker.this.e.clearFocus();
                            return true;
                        }
                    } else if (!NumberPicker.this.isEnabled() || NumberPicker.this.e.isFocused()) {
                        return false;
                    } else {
                        return NumberPicker.this.e.requestFocus();
                    }
                } else if (i2 != 16) {
                    if (i2 != 64) {
                        if (i2 != 128 || this.c != i) {
                            return false;
                        }
                        this.c = Integer.MIN_VALUE;
                        a(i, 65536);
                        NumberPicker numberPicker3 = NumberPicker.this;
                        numberPicker3.invalidate(0, numberPicker3.b0, numberPicker3.getRight(), NumberPicker.this.getBottom());
                        return true;
                    } else if (this.c == i) {
                        return false;
                    } else {
                        this.c = i;
                        a(i, 32768);
                        NumberPicker numberPicker4 = NumberPicker.this;
                        numberPicker4.invalidate(0, numberPicker4.b0, numberPicker4.getRight(), NumberPicker.this.getBottom());
                        return true;
                    }
                } else if (!NumberPicker.this.isEnabled()) {
                    return false;
                } else {
                    NumberPicker.this.a(true);
                    a(i, 1);
                    return true;
                }
            } else if (i2 != 64) {
                if (i2 != 128) {
                    if (i2 != 4096) {
                        if (i2 == 8192) {
                            if (!NumberPicker.this.isEnabled() || (!NumberPicker.this.getWrapSelectorWheel() && NumberPicker.this.getValue() <= NumberPicker.this.getMinValue())) {
                                return false;
                            }
                            NumberPicker.this.a(false);
                            return true;
                        }
                    } else if (!NumberPicker.this.isEnabled() || (!NumberPicker.this.getWrapSelectorWheel() && NumberPicker.this.getValue() >= NumberPicker.this.getMaxValue())) {
                        return false;
                    } else {
                        NumberPicker.this.a(true);
                        return true;
                    }
                } else if (this.c != i) {
                    return false;
                } else {
                    this.c = Integer.MIN_VALUE;
                    NumberPicker.this.performAccessibilityAction(Logger.DEFAULT_FULL_MESSAGE_LENGTH, (Bundle) null);
                    return true;
                }
            } else if (this.c == i) {
                return false;
            } else {
                this.c = i;
                NumberPicker.this.performAccessibilityAction(64, (Bundle) null);
                return true;
            }
            return super.performAction(i, i2, bundle);
        }

        @DexIgnore
        public final void a(int i) {
            if (((AccessibilityManager) NumberPicker.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
                NumberPicker.this.e.onInitializeAccessibilityEvent(obtain);
                NumberPicker.this.e.onPopulateAccessibilityEvent(obtain);
                obtain.setSource(NumberPicker.this, 2);
                NumberPicker numberPicker = NumberPicker.this;
                numberPicker.requestSendAccessibilityEvent(numberPicker, obtain);
            }
        }

        @DexIgnore
        public final void a(int i, int i2, String str) {
            if (((AccessibilityManager) NumberPicker.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
                obtain.setClassName(Button.class.getName());
                obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
                obtain.getText().add(str);
                obtain.setEnabled(NumberPicker.this.isEnabled());
                obtain.setSource(NumberPicker.this, i);
                NumberPicker numberPicker = NumberPicker.this;
                numberPicker.requestSendAccessibilityEvent(numberPicker, obtain);
            }
        }

        @DexIgnore
        public final void a(String str, int i, List<AccessibilityNodeInfo> list) {
            if (i == 1) {
                String c2 = c();
                if (!TextUtils.isEmpty(c2) && c2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(1));
                }
            } else if (i == 2) {
                Editable text = NumberPicker.this.e.getText();
                if (TextUtils.isEmpty(text) || !text.toString().toLowerCase().contains(str)) {
                    Editable text2 = NumberPicker.this.e.getText();
                    if (!TextUtils.isEmpty(text2) && text2.toString().toLowerCase().contains(str)) {
                        list.add(createAccessibilityNodeInfo(2));
                        return;
                    }
                    return;
                }
                list.add(createAccessibilityNodeInfo(2));
            } else if (i == 3) {
                String b2 = b();
                if (!TextUtils.isEmpty(b2) && b2.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(3));
                }
            }
        }

        @DexIgnore
        public final AccessibilityNodeInfo a() {
            AccessibilityNodeInfo createAccessibilityNodeInfo = NumberPicker.this.e.createAccessibilityNodeInfo();
            createAccessibilityNodeInfo.setSource(NumberPicker.this, 2);
            if (this.c != 2) {
                createAccessibilityNodeInfo.addAction(64);
            }
            if (this.c == 2) {
                createAccessibilityNodeInfo.addAction(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            }
            return createAccessibilityNodeInfo;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, String str, int i2, int i3, int i4, int i5) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(Button.class.getName());
            obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
            obtain.setSource(NumberPicker.this, i);
            obtain.setParent(NumberPicker.this);
            obtain.setText(str);
            obtain.setClickable(true);
            obtain.setLongClickable(true);
            obtain.setEnabled(NumberPicker.this.isEnabled());
            Rect rect = this.a;
            rect.set(i2, i3, i4, i5);
            obtain.setBoundsInParent(rect);
            int[] iArr = this.b;
            NumberPicker.this.getLocationOnScreen(iArr);
            rect.offset(iArr[0], iArr[1]);
            obtain.setBoundsInScreen(rect);
            if (this.c != i) {
                obtain.addAction(64);
            }
            if (this.c == i) {
                obtain.addAction(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            }
            if (NumberPicker.this.isEnabled()) {
                obtain.addAction(16);
            }
            return obtain;
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, int i2, int i3, int i4) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(NumberPicker.class.getName());
            obtain.setPackageName(NumberPicker.this.getContext().getPackageName());
            obtain.setSource(NumberPicker.this);
            if (d()) {
                obtain.addChild(NumberPicker.this, 3);
            }
            obtain.addChild(NumberPicker.this, 2);
            if (e()) {
                obtain.addChild(NumberPicker.this, 1);
            }
            obtain.setParent((View) NumberPicker.this.getParentForAccessibility());
            obtain.setEnabled(NumberPicker.this.isEnabled());
            obtain.setScrollable(true);
            if (this.c != -1) {
                obtain.addAction(64);
            }
            if (this.c == -1) {
                obtain.addAction(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
            }
            if (NumberPicker.this.isEnabled()) {
                if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() < NumberPicker.this.getMaxValue()) {
                    obtain.addAction(4096);
                }
                if (NumberPicker.this.getWrapSelectorWheel() || NumberPicker.this.getValue() > NumberPicker.this.getMinValue()) {
                    obtain.addAction(8192);
                }
            }
            return obtain;
        }
    }

    @DexIgnore
    public final int a(int i2, int i3) {
        if (i3 == -1) {
            return i2;
        }
        int size = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE) {
            return View.MeasureSpec.makeMeasureSpec(Math.min(size, i3), 1073741824);
        }
        if (mode == 0) {
            return View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
        if (mode == 1073741824) {
            return i2;
        }
        throw new IllegalArgumentException("Unknown measure mode: " + mode);
    }

    @DexIgnore
    public final void e(int i2) {
        g gVar = this.v;
        if (gVar != null) {
            gVar.a(this, i2, this.u);
        }
    }

    @DexIgnore
    public final int a(int i2, int i3, int i4) {
        return i2 != -1 ? resolveSizeAndState(Math.max(i2, i3), i4, 0) : i3;
    }

    @DexIgnore
    public final void b(int i2) {
        this.G = 0;
        if (i2 > 0) {
            this.E.fling(0, 0, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        } else {
            this.E.fling(0, Integer.MAX_VALUE, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        }
        invalidate();
    }

    @DexIgnore
    public int d(int i2) {
        int i3 = this.t;
        if (i2 > i3) {
            int i4 = this.s;
            return (i4 + ((i2 - i3) % (i3 - i4))) - 1;
        }
        int i5 = this.s;
        return i2 < i5 ? (i3 - ((i5 - i2) % (i3 - i5))) + 1 : i2;
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        int i3;
        if (this.u != i2) {
            if (this.P) {
                i3 = d(i2);
            } else {
                i3 = Math.min(Math.max(i2, this.s), this.t);
            }
            int i4 = this.u;
            this.u = i3;
            l();
            if (z2) {
                e(i4);
            }
            e();
            invalidate();
        }
    }

    @DexIgnore
    public final void b(int[] iArr) {
        System.arraycopy(iArr, 1, iArr, 0, iArr.length - 1);
        int i2 = iArr[iArr.length - 2] + 1;
        if (this.P && i2 > this.t) {
            i2 = this.s;
        }
        iArr[iArr.length - 1] = i2;
        a(i2);
    }

    @DexIgnore
    public void a(boolean z2) {
        if (this.R) {
            this.e.setVisibility(4);
            if (!a(this.E)) {
                a(this.F);
            }
            this.G = 0;
            if (z2) {
                this.E.startScroll(0, 0, 0, -this.B, 300);
            } else {
                this.E.startScroll(0, 0, 0, this.B, 300);
            }
            invalidate();
        } else if (z2) {
            a(this.u + 1, true);
        } else {
            a(this.u - 1, true);
        }
    }

    @DexIgnore
    public final void a(int[] iArr) {
        System.arraycopy(iArr, 0, iArr, 1, iArr.length - 1);
        int i2 = iArr[1] - 1;
        if (this.P && i2 < this.s) {
            i2 = this.t;
        }
        iArr[0] = i2;
        a(i2);
    }

    @DexIgnore
    public final void a(int i2) {
        String str;
        SparseArray<String> sparseArray = this.x;
        if (sparseArray.get(i2) == null) {
            int i3 = this.s;
            if (i2 < i3 || i2 > this.t) {
                str = "";
            } else {
                String[] strArr = this.r;
                str = strArr != null ? strArr[i2 - i3] : c(i2);
            }
            sparseArray.put(i2, str);
        }
    }

    @DexIgnore
    public void a(boolean z2, long j2) {
        e eVar = this.H;
        if (eVar == null) {
            this.H = new e();
        } else {
            removeCallbacks(eVar);
        }
        this.H.a(z2);
        postDelayed(this.H, j2);
    }

    @DexIgnore
    public final boolean a() {
        int i2 = this.C - this.D;
        if (i2 == 0) {
            return false;
        }
        this.G = 0;
        int abs = Math.abs(i2);
        int i3 = this.B;
        if (abs > i3 / 2) {
            if (i2 > 0) {
                i3 = -i3;
            }
            i2 += i3;
        }
        this.F.startScroll(0, 0, 0, i2, 800);
        invalidate();
        return true;
    }

    @DexIgnore
    public final void a(Paint paint, float f2, String str) {
        paint.setTextSize((float) this.p);
        Rect rect = new Rect();
        paint.getTextBounds(str, 0, str.length(), rect);
        if (((float) rect.width()) >= f2) {
            paint.setTextSize((paint.getTextSize() * f2) / ((float) rect.width()));
        }
    }
}
