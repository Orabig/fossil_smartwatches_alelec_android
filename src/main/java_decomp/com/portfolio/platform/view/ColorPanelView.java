package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;
import com.fossil.py5;
import com.fossil.x24;
import com.fossil.x9;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ColorPanelView extends View {
    @DexIgnore
    public Paint a;
    @DexIgnore
    public Paint b;
    @DexIgnore
    public Paint c;
    @DexIgnore
    public Paint d;
    @DexIgnore
    public Rect e;
    @DexIgnore
    public Rect f;
    @DexIgnore
    public RectF g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;

    @DexIgnore
    public ColorPanelView(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.ColorPanelView);
        this.p = obtainStyledAttributes.getInt(1, 1);
        this.h = obtainStyledAttributes.getBoolean(2, false);
        if (!this.h || this.p == 1) {
            this.j = obtainStyledAttributes.getColor(0, -9539986);
            obtainStyledAttributes.recycle();
            if (this.j == -9539986) {
                TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(new TypedValue().data, new int[]{16842808});
                this.j = obtainStyledAttributes2.getColor(0, this.j);
                obtainStyledAttributes2.recycle();
            }
            this.i = py5.a(context, 1.0f);
            this.a = new Paint();
            this.a.setAntiAlias(true);
            this.b = new Paint();
            this.b.setAntiAlias(true);
            if (this.h) {
                this.d = new Paint();
            }
            if (this.p == 1) {
                Bitmap bitmap = ((BitmapDrawable) context.getResources().getDrawable(2131230974)).getBitmap();
                this.c = new Paint();
                this.c.setAntiAlias(true);
                Shader.TileMode tileMode = Shader.TileMode.REPEAT;
                this.c.setShader(new BitmapShader(bitmap, tileMode, tileMode));
                return;
            }
            return;
        }
        throw new IllegalStateException("Color preview is only available in circle mode");
    }

    @DexIgnore
    public final void b() {
        Rect rect = this.e;
        int i2 = rect.left;
        int i3 = this.i;
        this.f = new Rect(i2 + i3, rect.top + i3, rect.right - i3, rect.bottom - i3);
    }

    @DexIgnore
    public void c() {
        int[] iArr = new int[2];
        Rect rect = new Rect();
        getLocationOnScreen(iArr);
        getWindowVisibleDisplayFrame(rect);
        Context context = getContext();
        int width = getWidth();
        int height = getHeight();
        int i2 = iArr[1] + (height / 2);
        int i3 = iArr[0] + (width / 2);
        if (x9.o(this) == 0) {
            i3 = context.getResources().getDisplayMetrics().widthPixels - i3;
        }
        StringBuilder sb = new StringBuilder("#");
        if (Color.alpha(this.o) != 255) {
            sb.append(Integer.toHexString(this.o).toUpperCase(Locale.ENGLISH));
        } else {
            sb.append(String.format("%06X", new Object[]{Integer.valueOf(16777215 & this.o)}).toUpperCase(Locale.ENGLISH));
        }
        Toast makeText = Toast.makeText(context, sb.toString(), 0);
        if (i2 < rect.height()) {
            makeText.setGravity(8388661, i3, (iArr[1] + height) - rect.top);
        } else {
            makeText.setGravity(81, 0, height);
        }
        makeText.show();
    }

    @DexIgnore
    public int getBorderColor() {
        return this.j;
    }

    @DexIgnore
    public int getColor() {
        return this.o;
    }

    @DexIgnore
    public int getShape() {
        return this.p;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        this.a.setColor(this.j);
        this.b.setColor(this.o);
        int i2 = this.p;
        if (i2 == 0) {
            if (this.i > 0) {
                canvas.drawRect(this.e, this.a);
            }
            canvas.drawRect(this.f, this.b);
        } else if (i2 == 1) {
            int measuredWidth = getMeasuredWidth() / 2;
            if (this.i > 0) {
                canvas.drawCircle((float) (getMeasuredWidth() / 2), (float) (getMeasuredHeight() / 2), (float) measuredWidth, this.a);
            }
            if (Color.alpha(this.o) < 255) {
                canvas.drawCircle((float) (getMeasuredWidth() / 2), (float) (getMeasuredHeight() / 2), (float) (measuredWidth - this.i), this.c);
            }
            if (this.h) {
                canvas.drawArc(this.g, 90.0f, 180.0f, true, this.d);
                canvas.drawArc(this.g, 270.0f, 180.0f, true, this.b);
                return;
            }
            canvas.drawCircle((float) (getMeasuredWidth() / 2), (float) (getMeasuredHeight() / 2), (float) (measuredWidth - this.i), this.b);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int i4 = this.p;
        if (i4 == 0) {
            setMeasuredDimension(View.MeasureSpec.getSize(i2), View.MeasureSpec.getSize(i3));
        } else if (i4 == 1) {
            super.onMeasure(i2, i2);
            setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
        } else {
            super.onMeasure(i2, i3);
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.o = bundle.getInt("color");
            parcelable = bundle.getParcelable("instanceState");
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt("color", this.o);
        return bundle;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (this.p == 0 || this.h) {
            this.e = new Rect();
            this.e.left = getPaddingLeft();
            this.e.right = i2 - getPaddingRight();
            this.e.top = getPaddingTop();
            this.e.bottom = i3 - getPaddingBottom();
            if (this.h) {
                a();
            } else {
                b();
            }
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        this.j = i2;
        invalidate();
    }

    @DexIgnore
    public void setColor(int i2) {
        this.o = i2;
        invalidate();
    }

    @DexIgnore
    public void setOriginalColor(int i2) {
        Paint paint = this.d;
        if (paint != null) {
            paint.setColor(i2);
        }
    }

    @DexIgnore
    public void setShape(int i2) {
        this.p = i2;
        invalidate();
    }

    @DexIgnore
    public ColorPanelView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ColorPanelView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.g = new RectF();
        this.j = -9539986;
        this.o = -16777216;
        a(context, attributeSet);
    }

    @DexIgnore
    public final void a() {
        Rect rect = this.e;
        int i2 = rect.left;
        int i3 = this.i;
        this.g = new RectF((float) (i2 + i3), (float) (rect.top + i3), (float) (rect.right - i3), (float) (rect.bottom - i3));
    }
}
