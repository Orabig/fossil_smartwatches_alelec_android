package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import com.fossil.d7;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityHorizontalBar extends BaseFitnessChart {
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public /* final */ Paint C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ Paint F;
    @DexIgnore
    public /* final */ Paint G;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public int g;
    @DexIgnore
    public String h;
    @DexIgnore
    public int i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public Typeface w;
    @DexIgnore
    public RectF x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes;
        wg6.b(context, "context");
        this.h = "";
        this.j = 5.0f;
        this.o = 8.0f;
        this.p = 8.0f;
        this.q = 1.0f;
        this.r = 8;
        this.s = -1;
        this.t = 10;
        this.v = true;
        this.x = new RectF();
        this.C = new Paint(1);
        this.D = new Paint(1);
        this.E = new Paint(1);
        this.F = new Paint(1);
        this.G = new Paint(1);
        if (!(attributeSet == null || (obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, x24.ActivityHorizontalBar, 0, 0)) == null)) {
            this.g = obtainStyledAttributes.getColor(0, 0);
            this.i = obtainStyledAttributes.getColor(2, 0);
            this.j = (float) obtainStyledAttributes.getDimensionPixelSize(4, 5);
            this.o = (float) obtainStyledAttributes.getDimensionPixelSize(5, 8);
            this.p = (float) obtainStyledAttributes.getDimensionPixelSize(3, 8);
            this.q = obtainStyledAttributes.getFloat(6, 1.0f);
            this.s = obtainStyledAttributes.getResourceId(7, -1);
            this.r = obtainStyledAttributes.getDimensionPixelSize(8, 8);
            String string = obtainStyledAttributes.getString(9);
            this.h = string == null ? "" : string;
            this.u = obtainStyledAttributes.getColor(10, 0);
            this.t = obtainStyledAttributes.getDimensionPixelSize(11, 10);
            try {
                this.w = d7.a(getContext(), obtainStyledAttributes.getResourceId(1, -1));
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ActivityHorizontalBar", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        c();
    }

    @DexIgnore
    public final RectF a(RectF rectF, float f2) {
        float f3 = rectF.left + f2;
        float f4 = rectF.top + f2;
        float f5 = rectF.right - f2;
        float f6 = rectF.bottom - f2;
        float f7 = (float) 0;
        if (f5 <= f7 || f5 < f3) {
            f5 = f3 + f2;
        }
        if (f6 <= f7 || f6 < f4) {
            f6 = f4 + f2;
        }
        return new RectF(f3, f4, f5, f6);
    }

    @DexIgnore
    public final void c() {
        this.G.setAntiAlias(true);
        this.G.setStyle(Paint.Style.STROKE);
        this.C.setColor(this.i);
        this.C.setAntiAlias(true);
        this.C.setStyle(Paint.Style.FILL);
        this.E.setAlpha((int) (this.q * ((float) 255)));
        this.E.setColorFilter(new PorterDuffColorFilter(this.i, PorterDuff.Mode.SRC_IN));
        this.E.setAntiAlias(true);
        this.F.setColor(-1);
        this.F.setAntiAlias(true);
        this.F.setStyle(Paint.Style.FILL);
        this.D.setColor(this.u);
        this.D.setAntiAlias(true);
        this.D.setStyle(Paint.Style.FILL);
        this.D.setTextSize((float) this.t);
        Typeface typeface = this.w;
        if (typeface != null) {
            this.D.setTypeface(typeface);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return this.s;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.r;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityHorizontalBar", "onDraw - mValue=" + this.e + ", mGoal=" + this.f + ", mMax=" + this.d);
        this.y = ((float) getWidth()) - this.p;
        if (canvas != null) {
            canvas.drawColor(this.g);
            RectF rectF = this.x;
            float f2 = this.d;
            float f3 = (float) 0;
            float f4 = 0.0f;
            float f5 = f2 > f3 ? (this.y * this.e) / f2 : 0.0f;
            float f6 = this.o;
            float f7 = (float) 2;
            rectF.set(0.0f, 0.0f, f5 + (f6 * f7), f6 * ((float) 3));
            float f8 = this.e;
            if (f8 > f3) {
                if (f8 >= this.f) {
                    this.C.setAlpha((int) 15.299999999999999d);
                    RectF rectF2 = this.x;
                    float f9 = (float) 4;
                    float f10 = this.j;
                    canvas.drawRoundRect(rectF2, f9 * f10, f9 * f10, this.C);
                    this.C.setAlpha((int) 40.800000000000004d);
                    RectF a2 = a(this.x, this.o / f7);
                    float f11 = this.j;
                    canvas.drawRoundRect(a2, f11 * 2.0f, f11 * 2.0f, this.C);
                }
                this.C.setAlpha(255);
                RectF a3 = a(this.x, this.o);
                float f12 = this.j;
                canvas.drawRoundRect(a3, f12, f12, this.C);
            }
            float height = ((float) getHeight()) / 2.0f;
            float f13 = this.d;
            if (f13 > f3) {
                f4 = (this.y * this.f) / f13;
            }
            float f14 = this.o;
            this.z = f4 + f14;
            if (this.e <= this.f) {
                canvas.drawBitmap(getStartBitmap(), this.z - ((float) getStartBitmap().getWidth()), height - ((float) (getStartBitmap().getHeight() / 2)), this.E);
            } else {
                canvas.drawCircle(this.z - (((float) this.r) / 2.0f), height, f14 / 4.0f, this.F);
            }
            if (this.v) {
                canvas.drawText(this.h, ((float) getWidth()) - this.A, height + (this.B / f7), this.D);
            }
        }
    }

    @DexIgnore
    public void onGlobalLayout() {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 0) {
                size = (int) (this.o * ((float) 3));
            } else if (mode != 1073741824) {
                size = 0;
            }
        }
        setMeasuredDimension(getWidth(), size);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context) {
        this(context, (AttributeSet) null, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wg6.b(context, "context");
    }
}
