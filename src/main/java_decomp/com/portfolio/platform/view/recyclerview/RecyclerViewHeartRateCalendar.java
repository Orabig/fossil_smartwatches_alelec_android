package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bk4;
import com.fossil.f7;
import com.fossil.jm4;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.sz5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.yd6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewHeartRateCalendar extends ConstraintLayout implements View.OnClickListener {
    @DexIgnore
    public TextView A;
    @DexIgnore
    public ConstraintLayout B;
    @DexIgnore
    public String C;
    @DexIgnore
    public String D;
    @DexIgnore
    public String E;
    @DexIgnore
    public String F;
    @DexIgnore
    public String G;
    @DexIgnore
    public String H;
    @DexIgnore
    public String I;
    @DexIgnore
    public Calendar J;
    @DexIgnore
    public int K;
    @DexIgnore
    public GridLayoutManager u;
    @DexIgnore
    public sz5 v;
    @DexIgnore
    public c w;
    @DexIgnore
    public d x;
    @DexIgnore
    public View y;
    @DexIgnore
    public View z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, Calendar calendar);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Calendar calendar);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void next();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends GridLayoutManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewHeartRateCalendar e;

        @DexIgnore
        public e(RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar) {
            this.e = recyclerViewHeartRateCalendar;
        }

        @DexIgnore
        public int a(int i) {
            sz5 mAdapter$app_fossilRelease = this.e.getMAdapter$app_fossilRelease();
            if (mAdapter$app_fossilRelease != null) {
                int itemViewType = mAdapter$app_fossilRelease.getItemViewType(i);
                return (itemViewType == 0 || itemViewType == 1) ? 1 : -1;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewHeartRateCalendar a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView b;

        @DexIgnore
        public f(RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar, RecyclerView recyclerView) {
            this.a = recyclerViewHeartRateCalendar;
            this.b = recyclerView;
        }

        @DexIgnore
        public void onGlobalLayout() {
            sz5 mAdapter$app_fossilRelease = this.a.getMAdapter$app_fossilRelease();
            if (mAdapter$app_fossilRelease != null) {
                RecyclerView recyclerView = this.b;
                wg6.a((Object) recyclerView, "recyclerView");
                mAdapter$app_fossilRelease.b(recyclerView.getMeasuredWidth() / 7);
                RecyclerView recyclerView2 = this.b;
                wg6.a((Object) recyclerView2, "recyclerView");
                recyclerView2.setAdapter(this.a.getMAdapter$app_fossilRelease());
                RecyclerView recyclerView3 = this.b;
                wg6.a((Object) recyclerView3, "recyclerView");
                recyclerView3.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    /*
    static {
        new a((qg6) null);
        Color.parseColor("#FFFF00");
    }
    */

    @DexIgnore
    public RecyclerViewHeartRateCalendar(Context context) {
        this(context, (AttributeSet) null, 0, 6, (qg6) null);
    }

    @DexIgnore
    public RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (qg6) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        this.C = "";
        this.D = "";
        this.E = "";
        this.F = "";
        this.G = "";
        this.H = "";
        this.I = "";
        this.J = Calendar.getInstance();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.RecyclerViewHeartRateCalendar);
            try {
                String string = obtainStyledAttributes.getString(4);
                if (string == null) {
                    string = "onDianaHeartRateTab";
                }
                this.C = string;
                String string2 = obtainStyledAttributes.getString(5);
                if (string2 == null) {
                    string2 = "nonBrandDisableCalendarDay";
                }
                this.D = string2;
                String string3 = obtainStyledAttributes.getString(6);
                if (string3 == null) {
                    string3 = "primaryText";
                }
                this.E = string3;
                String string4 = obtainStyledAttributes.getString(1);
                if (string4 == null) {
                    string4 = "averageRestingHeartRate";
                }
                this.F = string4;
                String string5 = obtainStyledAttributes.getString(0);
                if (string5 == null) {
                    string5 = "aboveAverageRestingHeartRate";
                }
                this.G = string5;
                String string6 = obtainStyledAttributes.getString(3);
                if (string6 == null) {
                    string6 = "maxHeartRate";
                }
                this.H = string6;
                String string7 = obtainStyledAttributes.getString(2);
                if (string7 == null) {
                    string7 = "nonBrandSurface";
                }
                this.I = string7;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("RecyclerViewHeartRateCalendar", "RecyclerViewHeartRateCalendar - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a(context);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.view.View$OnClickListener, com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar, android.view.ViewGroup] */
    public final void a(Context context) {
        Context context2 = context;
        View inflate = View.inflate(context2, 2131558800, this);
        this.B = inflate.findViewById(2131362088);
        this.A = (TextView) inflate.findViewById(2131362706);
        this.y = inflate.findViewById(2131362737);
        this.z = inflate.findViewById(2131362817);
        RecyclerView findViewById = inflate.findViewById(2131362157);
        this.v = new sz5(context2);
        int parseColor = Color.parseColor(ThemeManager.l.a().b(this.G));
        int parseColor2 = Color.parseColor(ThemeManager.l.a().b(this.F));
        int parseColor3 = Color.parseColor(ThemeManager.l.a().b(this.C));
        int parseColor4 = Color.parseColor(ThemeManager.l.a().b(this.D));
        int c2 = f7.c(Color.parseColor(ThemeManager.l.a().b(this.H)), 20);
        int parseColor5 = Color.parseColor(ThemeManager.l.a().b(this.E));
        sz5 sz5 = this.v;
        if (sz5 != null) {
            sz5.a(parseColor3, parseColor4, parseColor5, parseColor2, parseColor, c2);
            this.u = new RecyclerViewHeartRateCalendar$init$Anon1(context, context, 7, 0, true);
            String b2 = ThemeManager.l.a().b(this.I);
            if (!TextUtils.isEmpty(b2)) {
                ConstraintLayout constraintLayout = this.B;
                if (constraintLayout != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(b2));
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                ConstraintLayout constraintLayout2 = this.B;
                if (constraintLayout2 != null) {
                    constraintLayout2.setBackgroundColor(w6.a(context2, 2131099970));
                } else {
                    wg6.a();
                    throw null;
                }
            }
            GridLayoutManager gridLayoutManager = this.u;
            if (gridLayoutManager != null) {
                gridLayoutManager.a(new e(this));
                wg6.a((Object) findViewById, "recyclerView");
                findViewById.setLayoutManager(this.u);
                findViewById.setItemAnimator((RecyclerView.j) null);
                findViewById.getViewTreeObserver().addOnGlobalLayoutListener(new f(this, findViewById));
                View view = this.y;
                if (view != null) {
                    view.setOnClickListener(this);
                    View view2 = this.z;
                    if (view2 != null) {
                        view2.setOnClickListener(this);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void c(int i) {
        d dVar = this.x;
        if (dVar == null) {
            return;
        }
        if (i != 2131362737) {
            if (i == 2131362817) {
                if (dVar != null) {
                    dVar.a();
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else if (dVar != null) {
            dVar.next();
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void d() {
        sz5 sz5 = this.v;
        Calendar calendar = null;
        Calendar d2 = sz5 != null ? sz5.d() : null;
        sz5 sz52 = this.v;
        if (sz52 != null) {
            calendar = sz52.p();
        }
        c cVar = this.w;
        if (cVar != null) {
            Calendar calendar2 = this.J;
            wg6.a((Object) calendar2, "chosenCalendar");
            cVar.a(calendar2);
        }
        a(calendar, d2);
    }

    @DexIgnore
    public final sz5 getMAdapter$app_fossilRelease() {
        return this.v;
    }

    @DexIgnore
    public void onClick(View view) {
        wg6.b(view, "view");
        setEnableButtonNextAndPrevMonth(false);
        int id = view.getId();
        if (id == 2131362737) {
            this.K++;
        } else if (id == 2131362817) {
            this.K--;
        }
        sz5 sz5 = this.v;
        if (sz5 != null) {
            this.J = bk4.a(this.K, sz5.d());
        }
        d();
        c(view.getId());
    }

    @DexIgnore
    public final void setData(Map<Long, Integer> map) {
        int i;
        wg6.b(map, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("RecyclerViewHeartRateCalendar", "setData dataSize=" + map.size());
        Collection<Integer> values = map.values();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = values.iterator();
        while (true) {
            boolean z2 = true;
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (((Number) next).intValue() <= 0) {
                z2 = false;
            }
            if (z2) {
                arrayList.add(next);
            }
        }
        Integer num = (Integer) yd6.h(arrayList);
        int intValue = num != null ? num.intValue() : 0;
        Collection<Integer> values2 = map.values();
        ArrayList arrayList2 = new ArrayList();
        for (T next2 : values2) {
            if (((Number) next2).intValue() > 0) {
                arrayList2.add(next2);
            }
        }
        Integer num2 = (Integer) yd6.g(arrayList2);
        int intValue2 = num2 != null ? num2.intValue() : 100;
        if (intValue != intValue2) {
            i = intValue;
        } else if (intValue2 == 0) {
            intValue2 = 100;
        }
        sz5 sz5 = this.v;
        if (sz5 != null) {
            Calendar calendar = this.J;
            wg6.a((Object) calendar, "chosenCalendar");
            sz5.a(map, i, intValue2, calendar);
        }
        sz5 sz52 = this.v;
        if (sz52 != null) {
            sz52.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final void setEnableButtonNextAndPrevMonth(boolean z2) {
        View view = this.y;
        if (view != null) {
            view.setEnabled(z2);
        }
        View view2 = this.z;
        if (view2 != null) {
            view2.setEnabled(z2);
        }
    }

    @DexIgnore
    public final void setEndDate(Calendar calendar) {
        wg6.b(calendar, "endDate");
        this.J = bk4.a(this.K, calendar);
        sz5 sz5 = this.v;
        if (sz5 != null) {
            sz5.a(calendar);
            sz5 sz52 = this.v;
            if (sz52 != null) {
                sz52.notifyDataSetChanged();
                sz5 sz53 = this.v;
                if (sz53 != null) {
                    a(sz53.p(), calendar);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setMAdapter$app_fossilRelease(sz5 sz5) {
        this.v = sz5;
    }

    @DexIgnore
    public final void setOnCalendarItemClickListener(b bVar) {
        wg6.b(bVar, "listener");
        sz5 sz5 = this.v;
        if (sz5 != null) {
            sz5.a(bVar);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setOnCalendarMonthChanged(c cVar) {
        wg6.b(cVar, "listener");
        this.w = cVar;
    }

    @DexIgnore
    public final void setTintColor(int i) {
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RecyclerViewHeartRateCalendar(Context context, AttributeSet attributeSet, int i, int i2, qg6 qg6) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2, Calendar calendar3) {
        wg6.b(calendar, "currentCalendar");
        wg6.b(calendar2, "startCalendar");
        wg6.b(calendar3, "endCalendar");
        sz5 sz5 = this.v;
        if (sz5 != null) {
            sz5.c(calendar2);
            sz5.a(calendar3);
            sz5.b(calendar);
            sz5.notifyDataSetChanged();
            a(calendar2, calendar3);
        }
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2) {
        if (calendar != null && calendar2 != null) {
            int i = this.J.get(2);
            int i2 = this.J.get(1);
            View view = this.z;
            if (view != null) {
                int i3 = 8;
                view.setVisibility((i == calendar.get(2) && i2 == calendar.get(1)) ? 8 : 0);
                View view2 = this.y;
                if (view2 != null) {
                    if (!(i == calendar2.get(2) && i2 == calendar2.get(1))) {
                        i3 = 0;
                    }
                    view2.setVisibility(i3);
                    TextView textView = this.A;
                    if (textView != null) {
                        nh6 nh6 = nh6.a;
                        Calendar calendar3 = this.J;
                        wg6.a((Object) calendar3, "chosenCalendar");
                        Object[] objArr = {a(calendar3), Integer.valueOf(i2)};
                        String format = String.format("%s %s", Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) format, "java.lang.String.format(format, *args)");
                        int length = format.length() - 1;
                        int i4 = 0;
                        boolean z2 = false;
                        while (i4 <= length) {
                            boolean z3 = format.charAt(!z2 ? i4 : length) <= ' ';
                            if (!z2) {
                                if (!z3) {
                                    z2 = true;
                                } else {
                                    i4++;
                                }
                            } else if (!z3) {
                                break;
                            } else {
                                length--;
                            }
                        }
                        textView.setText(format.subSequence(i4, length + 1).toString());
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v18, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v21, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v24, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v27, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v30, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v33, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v36, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v39, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(Calendar calendar) {
        switch (calendar.get(2)) {
            case 0:
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886621);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a2;
            case 1:
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886620);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026hs_Month_Title__February)");
                return a3;
            case 2:
                String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886624);
                wg6.a((Object) a4, "LanguageHelper.getString\u2026onths_Month_Title__March)");
                return a4;
            case 3:
                String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886617);
                wg6.a((Object) a5, "LanguageHelper.getString\u2026onths_Month_Title__April)");
                return a5;
            case 4:
                String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886625);
                wg6.a((Object) a6, "LanguageHelper.getString\u2026_Months_Month_Title__May)");
                return a6;
            case 5:
                String a7 = jm4.a((Context) PortfolioApp.get.instance(), 2131886623);
                wg6.a((Object) a7, "LanguageHelper.getString\u2026Months_Month_Title__June)");
                return a7;
            case 6:
                String a8 = jm4.a((Context) PortfolioApp.get.instance(), 2131886622);
                wg6.a((Object) a8, "LanguageHelper.getString\u2026Months_Month_Title__July)");
                return a8;
            case 7:
                String a9 = jm4.a((Context) PortfolioApp.get.instance(), 2131886618);
                wg6.a((Object) a9, "LanguageHelper.getString\u2026nths_Month_Title__August)");
                return a9;
            case 8:
                String a10 = jm4.a((Context) PortfolioApp.get.instance(), 2131886628);
                wg6.a((Object) a10, "LanguageHelper.getString\u2026s_Month_Title__September)");
                return a10;
            case 9:
                String a11 = jm4.a((Context) PortfolioApp.get.instance(), 2131886627);
                wg6.a((Object) a11, "LanguageHelper.getString\u2026ths_Month_Title__October)");
                return a11;
            case 10:
                String a12 = jm4.a((Context) PortfolioApp.get.instance(), 2131886626);
                wg6.a((Object) a12, "LanguageHelper.getString\u2026hs_Month_Title__November)");
                return a12;
            case 11:
                String a13 = jm4.a((Context) PortfolioApp.get.instance(), 2131886619);
                wg6.a((Object) a13, "LanguageHelper.getString\u2026hs_Month_Title__December)");
                return a13;
            default:
                String a14 = jm4.a((Context) PortfolioApp.get.instance(), 2131886621);
                wg6.a((Object) a14, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a14;
        }
    }
}
