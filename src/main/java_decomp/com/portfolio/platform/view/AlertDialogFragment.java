package com.portfolio.platform.view;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.ft;
import com.fossil.hc;
import com.fossil.iy5;
import com.fossil.nz;
import com.fossil.u8;
import com.fossil.w6;
import com.fossil.wq;
import com.fossil.wy5;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.DashbarData;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.NumberPickerLarge;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UseSparseArrays"})
public class AlertDialogFragment extends DialogFragment implements DialogInterface.OnKeyListener {
    @DexIgnore
    public g A;
    @DexIgnore
    public h B;
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public ArrayList<Integer> f;
    @DexIgnore
    public HashMap<Integer, Integer> g;
    @DexIgnore
    public HashMap<Integer, Bitmap> h;
    @DexIgnore
    public HashMap<Integer, String> i;
    @DexIgnore
    public HashMap<Integer, String> j;
    @DexIgnore
    public DashbarData o;
    @DexIgnore
    public HashMap<Integer, SpannableString> p;
    @DexIgnore
    public ArrayList<Integer> q;
    @DexIgnore
    public ArrayList<Integer> r;
    @DexIgnore
    public ArrayList<Integer> s;
    @DexIgnore
    public ArrayList<Integer> t;
    @DexIgnore
    public HashMap<Integer, List<Serializable>> u;
    @DexIgnore
    public ArrayList<u8<Integer, Integer>> v;
    @DexIgnore
    public HashMap<Integer, Boolean> w;
    @DexIgnore
    public HashMap<Integer, Integer> x;
    @DexIgnore
    public HashMap<Integer, Integer> y;
    @DexIgnore
    public Intent z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;

        @DexIgnore
        public a(View view) {
            this.a = view;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wq.a(AlertDialogFragment.this).a(str2).a(new nz().a(ft.c)).a((ImageView) this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public b(View view, int i) {
            this.a = view;
            this.b = i;
        }

        @DexIgnore
        public void onClick(View view) {
            AlertDialogFragment.this.dismiss();
            AlertDialogFragment alertDialogFragment = AlertDialogFragment.this;
            HashMap<Integer, Boolean> hashMap = alertDialogFragment.w;
            if (hashMap != null) {
                alertDialogFragment.z.putExtra("EXTRA_SWITCH_RESULTS", hashMap);
            }
            AlertDialogFragment alertDialogFragment2 = AlertDialogFragment.this;
            HashMap<Integer, Integer> hashMap2 = alertDialogFragment2.x;
            if (hashMap2 != null) {
                alertDialogFragment2.z.putExtra("EXTRA_NUMBER_PICKER_RESULTS", hashMap2);
            }
            AlertDialogFragment alertDialogFragment3 = AlertDialogFragment.this;
            HashMap<Integer, Integer> hashMap3 = alertDialogFragment3.y;
            if (hashMap3 != null) {
                alertDialogFragment3.z.putExtra("EXTRA_RADIO_GROUPS_RESULTS", hashMap3);
            }
            if (AlertDialogFragment.this.t != null) {
                HashMap hashMap4 = new HashMap();
                Iterator<Integer> it = AlertDialogFragment.this.t.iterator();
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    hashMap4.put(Integer.valueOf(intValue), ((FlexibleEditText) this.a.findViewById(intValue)).getText().toString());
                }
                AlertDialogFragment.this.z.putExtra("EXTRA_EDIT_TEXT_RESULTS", hashMap4);
            }
            AlertDialogFragment alertDialogFragment4 = AlertDialogFragment.this;
            g gVar = alertDialogFragment4.A;
            if (gVar != null) {
                gVar.a(alertDialogFragment4.a, this.b, alertDialogFragment4.z);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public c(View view, int i) {
            this.a = view;
            this.b = i;
        }

        @DexIgnore
        public void onClick(View view) {
            AlertDialogFragment alertDialogFragment = AlertDialogFragment.this;
            HashMap<Integer, Boolean> hashMap = alertDialogFragment.w;
            if (hashMap != null) {
                alertDialogFragment.z.putExtra("EXTRA_SWITCH_RESULTS", hashMap);
            }
            AlertDialogFragment alertDialogFragment2 = AlertDialogFragment.this;
            HashMap<Integer, Integer> hashMap2 = alertDialogFragment2.x;
            if (hashMap2 != null) {
                alertDialogFragment2.z.putExtra("EXTRA_NUMBER_PICKER_RESULTS", hashMap2);
            }
            AlertDialogFragment alertDialogFragment3 = AlertDialogFragment.this;
            HashMap<Integer, Integer> hashMap3 = alertDialogFragment3.y;
            if (hashMap3 != null) {
                alertDialogFragment3.z.putExtra("EXTRA_RADIO_GROUPS_RESULTS", hashMap3);
            }
            if (AlertDialogFragment.this.t != null) {
                HashMap hashMap4 = new HashMap();
                Iterator<Integer> it = AlertDialogFragment.this.t.iterator();
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    hashMap4.put(Integer.valueOf(intValue), ((FlexibleEditText) this.a.findViewById(intValue)).getText().toString());
                }
                AlertDialogFragment.this.z.putExtra("EXTRA_EDIT_TEXT_RESULTS", hashMap4);
            }
            AlertDialogFragment alertDialogFragment4 = AlertDialogFragment.this;
            g gVar = alertDialogFragment4.A;
            if (gVar != null) {
                gVar.a(alertDialogFragment4.a, this.b, alertDialogFragment4.z);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            AlertDialogFragment alertDialogFragment = AlertDialogFragment.this;
            if (alertDialogFragment.w == null) {
                alertDialogFragment.w = new HashMap<>();
            }
            AlertDialogFragment.this.w.put(Integer.valueOf(compoundButton.getId()), Boolean.valueOf(z));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements NumberPickerLarge.h {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(NumberPickerLarge numberPickerLarge, int i, int i2) {
            AlertDialogFragment alertDialogFragment = AlertDialogFragment.this;
            if (alertDialogFragment.x == null) {
                alertDialogFragment.x = new HashMap<>();
            }
            AlertDialogFragment.this.x.put(Integer.valueOf(numberPickerLarge.getId()), Integer.valueOf(i2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d; // = true;
        @DexIgnore
        public /* final */ ArrayList<Integer> e; // = new ArrayList<>();
        @DexIgnore
        public /* final */ HashMap<Integer, Integer> f; // = new HashMap<>();
        @DexIgnore
        public /* final */ HashMap<Integer, Bitmap> g; // = new HashMap<>();
        @DexIgnore
        public /* final */ HashMap<Integer, String> h; // = new HashMap<>();
        @DexIgnore
        public /* final */ HashMap<Integer, String> i; // = new HashMap<>();
        @DexIgnore
        public DashbarData j;
        @DexIgnore
        public /* final */ HashMap<Integer, SpannableString> k; // = new HashMap<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> l; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> m; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> n; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> o; // = new ArrayList<>();
        @DexIgnore
        public /* final */ HashMap<Integer, List<Serializable>> p; // = new HashMap<>();
        @DexIgnore
        public /* final */ ArrayList<u8<Integer, Integer>> q; // = new ArrayList<>();

        @DexIgnore
        public f(int i2) {
            this.a = i2;
        }

        @DexIgnore
        public f a(boolean z) {
            this.d = z;
            return this;
        }

        @DexIgnore
        public f b(boolean z) {
            this.c = z;
            return this;
        }

        @DexIgnore
        public f c(int i2) {
            this.b = i2;
            return this;
        }

        @DexIgnore
        public f a(int i2, String str) {
            this.i.put(Integer.valueOf(i2), str);
            return this;
        }

        @DexIgnore
        public f b(int i2) {
            this.n.add(Integer.valueOf(i2));
            return this;
        }

        @DexIgnore
        public f a(DashbarData dashbarData) {
            this.j = dashbarData;
            return this;
        }

        @DexIgnore
        public f a(int i2) {
            this.l.add(Integer.valueOf(i2));
            return this;
        }

        @DexIgnore
        public f a(int i2, int i3, int i4, int i5) {
            a(i2, i3, i4, i5, (NumberPickerLarge.f) null, (String[]) null);
            return this;
        }

        @DexIgnore
        public f a(int i2, int i3, int i4, int i5, NumberPickerLarge.f fVar, String[] strArr) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(Integer.valueOf(i3));
            arrayList.add(Integer.valueOf(i4));
            arrayList.add(Integer.valueOf(i5));
            if (fVar != null) {
                arrayList.add(fVar);
            }
            if (strArr != null) {
                arrayList.add(strArr);
            }
            this.p.put(Integer.valueOf(i2), arrayList);
            return this;
        }

        @DexIgnore
        public AlertDialogFragment a(String str) {
            return a(str, (Bundle) null);
        }

        @DexIgnore
        public AlertDialogFragment a(String str, Bundle bundle) {
            String str2 = str;
            return AlertDialogFragment.a(str, bundle, this.a, this.b, this.c, this.e, this.f, this.g, this.h, this.i, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.d, this.j);
        }

        @DexIgnore
        public AlertDialogFragment a(FragmentManager fragmentManager, String str) {
            return a(fragmentManager, str, 1, 2131951629);
        }

        @DexIgnore
        public AlertDialogFragment a(FragmentManager fragmentManager, String str, int i2, int i3) {
            AlertDialogFragment a2 = a(str);
            a2.setStyle(i2, i3);
            a2.show(fragmentManager, str);
            return a2;
        }

        @DexIgnore
        public AlertDialogFragment a(FragmentManager fragmentManager, String str, Bundle bundle) {
            return a(fragmentManager, str, bundle, 1, 2131951629);
        }

        @DexIgnore
        public AlertDialogFragment a(FragmentManager fragmentManager, String str, Bundle bundle, int i2, int i3) {
            AlertDialogFragment a2 = a(str, bundle);
            a2.setStyle(i2, i3);
            a2.show(fragmentManager, str);
            return a2;
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(String str, int i, Intent intent);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        void T(String str);
    }

    /*
    static {
        Class<AlertDialogFragment> cls = AlertDialogFragment.class;
    }
    */

    @DexIgnore
    public static AlertDialogFragment a(String str, Bundle bundle, int i2, int i3, boolean z2, ArrayList<Integer> arrayList, HashMap<Integer, Integer> hashMap, HashMap<Integer, Bitmap> hashMap2, HashMap<Integer, String> hashMap3, HashMap<Integer, String> hashMap4, HashMap<Integer, SpannableString> hashMap5, ArrayList<Integer> arrayList2, ArrayList<Integer> arrayList3, ArrayList<Integer> arrayList4, ArrayList<Integer> arrayList5, HashMap<Integer, List<Serializable>> hashMap6, ArrayList<u8<Integer, Integer>> arrayList6, boolean z3, DashbarData dashbarData) {
        AlertDialogFragment alertDialogFragment = new AlertDialogFragment();
        Bundle bundle2 = new Bundle();
        String str2 = str;
        bundle2.putString("ARGUMENTS_TAG", str);
        Bundle bundle3 = bundle;
        bundle2.putBundle("ARGUMENTS_BUNDLE", bundle);
        int i4 = i2;
        bundle2.putInt("ARGUMENTS_LAYOUT_ID", i2);
        int i5 = i3;
        bundle2.putInt("ARGUMENTS_STATUS_BAR_COLOR_ID", i3);
        boolean z4 = z2;
        bundle2.putBoolean("ARGUMENTS_STATUS_BAR_DARK_ICON", z2);
        ArrayList<Integer> arrayList7 = arrayList;
        bundle2.putSerializable("ARGUMENTS_STATUS_BAR_FLAGS", arrayList);
        HashMap<Integer, Integer> hashMap7 = hashMap;
        bundle2.putSerializable("ARGUMENTS_IMAGE_VIEWS", hashMap);
        HashMap<Integer, Bitmap> hashMap8 = hashMap2;
        bundle2.putSerializable("ARGUMENTS_BLUR_IMAGE_VIEWS", hashMap2);
        HashMap<Integer, String> hashMap9 = hashMap3;
        bundle2.putSerializable("ARGUMENTS_DEVICE_IMAGE_VIEWS", hashMap3);
        HashMap<Integer, String> hashMap10 = hashMap4;
        bundle2.putSerializable("ARGUMENTS_TEXT_VIEWS", hashMap4);
        HashMap<Integer, SpannableString> hashMap11 = hashMap5;
        bundle2.putSerializable("ARGUMENTS_TEXT_VIEWS_SPANNABLE", hashMap5);
        bundle2.putSerializable("ARGUMENTS_DISMISS_VIEWS", arrayList4);
        ArrayList<Integer> arrayList8 = arrayList2;
        bundle2.putSerializable("ARGUMENTS_ACTION_VIEWS", arrayList2);
        bundle2.putSerializable("ARGUMENTS_SWITCH_VIEWS", arrayList3);
        bundle2.putSerializable("ARGUMENTS_EDIT_TEXT_VIEWS", arrayList5);
        bundle2.putSerializable("ARGUMENTS_NUMBER_PICKERS", hashMap6);
        bundle2.putSerializable("ARGUMENTS_RADIO_GROUPS", arrayList6);
        bundle2.putBoolean("ARGUMENTS_ALLOW_BACK_PRESS", z3);
        bundle2.putParcelable("ARGUMENTS_DASH_BAR_DATA", dashbarData);
        alertDialogFragment.setArguments(bundle2);
        return alertDialogFragment;
    }

    @DexIgnore
    public boolean d1() {
        dismiss();
        return true;
    }

    @DexIgnore
    public void dismiss() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            hc b2 = fragmentManager.b();
            b2.d(this);
            b2.b();
        }
    }

    @DexIgnore
    public void onAttach(Context context) {
        AlertDialogFragment.super.onAttach(context);
        g parentFragment = getParentFragment();
        if (parentFragment != null) {
            if (parentFragment instanceof g) {
                this.A = parentFragment;
            }
            if (parentFragment instanceof h) {
                this.B = (h) parentFragment;
            }
        }
        if (this.A == null && (context instanceof g)) {
            this.A = (g) context;
        }
        if (this.B == null && (context instanceof h)) {
            this.B = (h) context;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        AlertDialogFragment.super.onCreate(bundle);
        setRetainInstance(true);
        this.z = new Intent();
        Bundle arguments = getArguments();
        this.a = arguments.getString("ARGUMENTS_TAG");
        Bundle bundle2 = arguments.getBundle("ARGUMENTS_BUNDLE");
        if (bundle2 != null) {
            this.z.putExtras(bundle2);
        }
        this.b = arguments.getInt("ARGUMENTS_LAYOUT_ID");
        this.c = arguments.getInt("ARGUMENTS_STATUS_BAR_COLOR_ID");
        this.d = arguments.getBoolean("ARGUMENTS_STATUS_BAR_DARK_ICON");
        this.f = (ArrayList) arguments.getSerializable("ARGUMENTS_STATUS_BAR_FLAGS");
        this.g = (HashMap) arguments.getSerializable("ARGUMENTS_IMAGE_VIEWS");
        this.h = (HashMap) arguments.getSerializable("ARGUMENTS_BLUR_IMAGE_VIEWS");
        this.i = (HashMap) arguments.getSerializable("ARGUMENTS_DEVICE_IMAGE_VIEWS");
        this.j = (HashMap) arguments.getSerializable("ARGUMENTS_TEXT_VIEWS");
        this.p = (HashMap) arguments.getSerializable("ARGUMENTS_TEXT_VIEWS_SPANNABLE");
        this.q = (ArrayList) arguments.getSerializable("ARGUMENTS_ACTION_VIEWS");
        this.r = (ArrayList) arguments.getSerializable("ARGUMENTS_DISMISS_VIEWS");
        this.s = (ArrayList) arguments.getSerializable("ARGUMENTS_SWITCH_VIEWS");
        this.t = (ArrayList) arguments.getSerializable("ARGUMENTS_EDIT_TEXT_VIEWS");
        this.u = (HashMap) arguments.getSerializable("ARGUMENTS_NUMBER_PICKERS");
        this.v = (ArrayList) arguments.getSerializable("ARGUMENTS_RADIO_GROUPS");
        this.e = arguments.getBoolean("ARGUMENTS_ALLOW_BACK_PRESS", true);
        this.o = (DashbarData) arguments.getParcelable("ARGUMENTS_DASH_BAR_DATA");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Window window;
        if (Build.VERSION.SDK_INT >= 21 && (window = getDialog().getWindow()) != null) {
            if (this.c != 0) {
                window.addFlags(Integer.MIN_VALUE);
                window.setStatusBarColor(w6.a(PortfolioApp.T, this.c));
            }
            ArrayList<Integer> arrayList = this.f;
            if (arrayList != null && !arrayList.isEmpty()) {
                Iterator<Integer> it = this.f.iterator();
                while (it.hasNext()) {
                    window.addFlags(it.next().intValue());
                }
            }
            if (Build.VERSION.SDK_INT >= 23 && this.d) {
                window.getDecorView().setSystemUiVisibility(8192);
            }
        }
        View inflate = layoutInflater.inflate(this.b, viewGroup);
        View findViewById = inflate.findViewById(2131362851);
        if (findViewById != null) {
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                findViewById.setBackgroundColor(Color.parseColor(b2));
            }
        }
        return inflate;
    }

    @DexIgnore
    public void onDetach() {
        AlertDialogFragment.super.onDetach();
        this.A = null;
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        FLogger.INSTANCE.getLocal().d("AlertDialogFragment", "onDismiss");
        h hVar = this.B;
        if (hVar != null) {
            hVar.T(getTag());
        }
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
        if (!this.e) {
            return true;
        }
        if (keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d("AlertDialogFragment", "onKey KEYCODE_BACK");
        return d1();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        AlertDialogFragment.super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(2131362851);
        if (findViewById != null) {
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                findViewById.setBackgroundColor(Color.parseColor(b2));
            }
        }
        HashMap<Integer, Integer> hashMap = this.g;
        if (hashMap != null) {
            for (Map.Entry next : hashMap.entrySet()) {
                View findViewById2 = view.findViewById(((Integer) next.getKey()).intValue());
                if (findViewById2 == null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("AlertDialogFragment", "Set ImageViews - view is null on mTag = " + this.a);
                } else {
                    ((ImageView) findViewById2).setImageResource(((Integer) next.getValue()).intValue());
                }
            }
        }
        HashMap<Integer, Bitmap> hashMap2 = this.h;
        if (hashMap2 != null) {
            for (Map.Entry next2 : hashMap2.entrySet()) {
                View findViewById3 = view.findViewById(((Integer) next2.getKey()).intValue());
                if (findViewById3 == null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("AlertDialogFragment", "Set mBlurImageViews - view is null on mTag = " + this.a);
                } else {
                    wy5.b a2 = wy5.a(getContext());
                    a2.a(25);
                    a2.b(2);
                    a2.a((Bitmap) next2.getValue()).a((ImageView) findViewById3);
                }
            }
        }
        HashMap<Integer, String> hashMap3 = this.i;
        if (hashMap3 != null) {
            for (Map.Entry next3 : hashMap3.entrySet()) {
                View findViewById4 = view.findViewById(((Integer) next3.getKey()).intValue());
                if (findViewById4 == null) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.e("AlertDialogFragment", "Set mDeviceImageViews - view is null on mTag = " + this.a);
                } else {
                    String str = (String) next3.getValue();
                    CloudImageHelper.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.DeviceType.TYPE_LARGE).setPlaceHolder((ImageView) findViewById4, DeviceHelper.o.b(str, DeviceHelper.ImageStyle.LARGE)).setImageCallback(new a(findViewById4)).download();
                }
            }
        }
        HashMap<Integer, String> hashMap4 = this.j;
        if (hashMap4 != null) {
            for (Map.Entry next4 : hashMap4.entrySet()) {
                View findViewById5 = view.findViewById(((Integer) next4.getKey()).intValue());
                if (findViewById5 == null) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    local4.e("AlertDialogFragment", "Set TextViews - view is null on mTag = " + this.a);
                } else {
                    ((TextView) findViewById5).setText((CharSequence) next4.getValue());
                }
            }
        }
        DashbarData dashbarData = this.o;
        if (dashbarData != null) {
            DashBar dashBar = (DashBar) view.findViewById(dashbarData.getViewId());
            if (dashBar == null) {
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                local5.e("AlertDialogFragment", "Set DashBar - view is null on mTag = " + this.a);
            } else {
                dashBar.setProgress(this.o.getStartProgress());
                ObjectAnimator ofInt = ObjectAnimator.ofInt(dashBar, "progress", new int[]{this.o.getStartProgress(), this.o.getEndProgress()});
                ofInt.setDuration(500);
                ofInt.start();
            }
        }
        HashMap<Integer, SpannableString> hashMap5 = this.p;
        if (hashMap5 != null) {
            for (Map.Entry next5 : hashMap5.entrySet()) {
                View findViewById6 = view.findViewById(((Integer) next5.getKey()).intValue());
                if (findViewById6 == null) {
                    ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                    local6.e("AlertDialogFragment", "Set TextViews - view is null on mTag = " + this.a);
                } else {
                    TextView textView = (TextView) findViewById6;
                    textView.setText((CharSequence) next5.getValue());
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    textView.setHighlightColor(0);
                }
            }
        }
        ArrayList<Integer> arrayList = this.r;
        if (arrayList != null && !arrayList.isEmpty()) {
            Iterator<Integer> it = this.r.iterator();
            while (it.hasNext()) {
                int intValue = it.next().intValue();
                if (view.findViewById(intValue) == null) {
                    ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                    local7.e("AlertDialogFragment", "Set action - view is null on mTag = " + this.a);
                } else {
                    view.findViewById(intValue).setOnClickListener(new b(view, intValue));
                }
            }
        }
        ArrayList<Integer> arrayList2 = this.q;
        if (arrayList2 != null) {
            Iterator<Integer> it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                int intValue2 = it2.next().intValue();
                if (view.findViewById(intValue2) == null) {
                    ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
                    local8.e("AlertDialogFragment", "Set action - view is null on mTag = " + this.a);
                } else {
                    view.findViewById(intValue2).setOnClickListener(new c(view, intValue2));
                }
            }
        }
        ArrayList<Integer> arrayList3 = this.s;
        if (arrayList3 != null) {
            Iterator<Integer> it3 = arrayList3.iterator();
            while (it3.hasNext()) {
                int intValue3 = it3.next().intValue();
                SwitchCompat findViewById7 = view.findViewById(intValue3);
                if (findViewById7 == null) {
                    ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
                    local9.e("AlertDialogFragment", "Set SwitchViews - view is null on mTag = " + this.a);
                } else {
                    if (this.w == null) {
                        this.w = new HashMap<>();
                    }
                    this.w.put(Integer.valueOf(intValue3), false);
                    findViewById7.setOnCheckedChangeListener(new d());
                }
            }
        }
        ArrayList<Integer> arrayList4 = this.t;
        if (arrayList4 != null) {
            Iterator<Integer> it4 = arrayList4.iterator();
            while (it4.hasNext()) {
                if (((FlexibleEditText) view.findViewById(it4.next().intValue())) == null) {
                    ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
                    local10.e("AlertDialogFragment", "Set EditTextViews - view is null on mTag = " + this.a);
                }
            }
        }
        HashMap<Integer, List<Serializable>> hashMap6 = this.u;
        if (hashMap6 != null) {
            for (Map.Entry next6 : hashMap6.entrySet()) {
                View findViewById8 = view.findViewById(((Integer) next6.getKey()).intValue());
                if (findViewById8 == null) {
                    ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
                    local11.e("AlertDialogFragment", "Set NumberPickers - view is null on mTag = " + this.a);
                } else {
                    NumberPickerLarge numberPickerLarge = (NumberPickerLarge) findViewById8;
                    numberPickerLarge.setOnValueChangedListener(new e());
                    List list = (List) next6.getValue();
                    int intValue4 = ((Integer) list.get(2)).intValue();
                    numberPickerLarge.setMinValue(((Integer) list.get(0)).intValue());
                    numberPickerLarge.setMaxValue(((Integer) list.get(1)).intValue());
                    numberPickerLarge.setValue(intValue4);
                    if (list.size() > 3) {
                        numberPickerLarge.setFormatter((NumberPickerLarge.f) list.get(3));
                    }
                    if (list.size() > 4) {
                        numberPickerLarge.setDisplayedValues((String[]) list.get(4));
                    }
                    if (this.x == null) {
                        this.x = new HashMap<>();
                    }
                    this.x.put(next6.getKey(), Integer.valueOf(intValue4));
                }
            }
        }
        ArrayList<u8<Integer, Integer>> arrayList5 = this.v;
        if (arrayList5 != null) {
            Iterator<u8<Integer, Integer>> it5 = arrayList5.iterator();
            while (it5.hasNext()) {
                u8 u8Var = (u8) it5.next();
                Integer num = (Integer) u8Var.a;
                Integer num2 = (Integer) u8Var.b;
                if (!(num == null || num2 == null)) {
                    RadioGroup radioGroup = (RadioGroup) view.findViewById(num.intValue());
                    if (radioGroup == null) {
                        ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
                        local12.e("AlertDialogFragment", "Set RadioGroup - radioGroup is null on mTag = " + this.a);
                    } else {
                        RadioButton radioButton = (RadioButton) view.findViewById(num2.intValue());
                        if (radioButton != null) {
                            radioButton.setChecked(true);
                        } else {
                            ILocalFLogger local13 = FLogger.INSTANCE.getLocal();
                            local13.e("AlertDialogFragment", "Set RadioGroup - defaultRadioButtonChecked is null on mTag = " + this.a + ", do not set check default");
                        }
                        if (this.y == null) {
                            this.y = new HashMap<>();
                        }
                        this.y.put(num, num2);
                        radioGroup.setOnCheckedChangeListener(new iy5(this, num));
                    }
                }
            }
        }
    }

    @DexIgnore
    public void setupDialog(Dialog dialog, int i2) {
        dialog.requestWindowFeature(1);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
            dialog.getWindow().setLayout(-1, -1);
            dialog.getWindow().getDecorView().setSystemUiVisibility(3328);
            dialog.setOnKeyListener(this);
        }
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlertDialogFragment", "show - tag: " + str);
        hc b2 = fragmentManager.b();
        Fragment b3 = fragmentManager.b(str);
        if (b3 != null) {
            b2.d(b3);
        }
        b2.a((String) null);
        b2.a(this, str);
        b2.b();
    }

    @DexIgnore
    public /* synthetic */ void a(Integer num, RadioGroup radioGroup, int i2) {
        if (this.y == null) {
            this.y = new HashMap<>();
        }
        this.y.put(num, Integer.valueOf(i2));
    }
}
