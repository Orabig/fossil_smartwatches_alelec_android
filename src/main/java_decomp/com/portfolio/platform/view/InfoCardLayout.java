package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import com.fossil.wg6;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InfoCardLayout extends FrameLayout {
    @DexIgnore
    public BottomSheetBehavior<FrameLayout> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ InfoCardLayout a;

        @DexIgnore
        public a(InfoCardLayout infoCardLayout) {
            this.a = infoCardLayout;
        }

        @DexIgnore
        public void onGlobalLayout() {
            this.a.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            InfoCardLayout infoCardLayout = this.a;
            BottomSheetBehavior b = BottomSheetBehavior.b(infoCardLayout);
            wg6.a((Object) b, "BottomSheetBehavior.from(this@InfoCardLayout)");
            infoCardLayout.setMBottomSheetBehavior$app_fossilRelease(b);
            this.a.getMBottomSheetBehavior$app_fossilRelease().b(false);
            this.a.getMBottomSheetBehavior$app_fossilRelease().c(0);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InfoCardLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        getViewTreeObserver().addOnGlobalLayoutListener(new a(this));
    }

    @DexIgnore
    public final BottomSheetBehavior<FrameLayout> getMBottomSheetBehavior$app_fossilRelease() {
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.a;
        if (bottomSheetBehavior != null) {
            return bottomSheetBehavior;
        }
        wg6.d("mBottomSheetBehavior");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        wg6.b(motionEvent, Constants.EVENT);
        return true;
    }

    @DexIgnore
    public final void setMBottomSheetBehavior$app_fossilRelease(BottomSheetBehavior<FrameLayout> bottomSheetBehavior) {
        wg6.b(bottomSheetBehavior, "<set-?>");
        this.a = bottomSheetBehavior;
    }
}
