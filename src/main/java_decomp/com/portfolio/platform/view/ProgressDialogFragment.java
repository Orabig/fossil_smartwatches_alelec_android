package com.portfolio.platform.view;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.DialogFragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProgressDialogFragment extends DialogFragment {
    @DexIgnore
    public static /* final */ a c; // = new a((qg6) null);
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public HashMap b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ProgressDialogFragment a(String str) {
            wg6.b(str, Explore.COLUMN_TITLE);
            ProgressDialogFragment progressDialogFragment = new ProgressDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString("DIALOG_TITLE", str);
            progressDialogFragment.setArguments(bundle);
            return progressDialogFragment;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.b;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        ProgressDialogFragment.super.onCreate(bundle);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProgressDialogFragment", "create with arguments " + getArguments());
        Bundle arguments = getArguments();
        if (arguments != null) {
            String string = arguments.getString("DIALOG_TITLE");
            if (string == null) {
                string = "";
            }
            this.a = string;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        return layoutInflater.inflate(2131558572, viewGroup, false);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        ProgressDialogFragment.super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void onViewCreated(View view, Bundle bundle) {
        Window window;
        wg6.b(view, "view");
        ProgressDialogFragment.super.onViewCreated(view, bundle);
        Object r6 = (FlexibleTextView) view.findViewById(2131363218);
        ViewGroup viewGroup = (ViewGroup) view.findViewById(2131362851);
        String b2 = ThemeManager.l.a().b("nonBrandSurface");
        String b3 = ThemeManager.l.a().b("primaryText");
        FlexibleProgressBar flexibleProgressBar = (FlexibleProgressBar) view.findViewById(2131362822);
        if (b3 != null) {
            wg6.a((Object) flexibleProgressBar, "progressBar");
            flexibleProgressBar.getIndeterminateDrawable().setTint(Color.parseColor(b3));
        }
        if (b2 != null) {
            viewGroup.setBackgroundColor(Color.parseColor(b2));
        }
        if (TextUtils.isEmpty(this.a)) {
            wg6.a((Object) r6, "mTvTitle");
            r6.setVisibility(8);
        } else {
            wg6.a((Object) r6, "mTvTitle");
            r6.setVisibility(0);
            r6.setText(this.a);
        }
        Dialog dialog = getDialog();
        if (dialog != null && (window = dialog.getWindow()) != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
        }
    }
}
