package com.portfolio.platform.data;

import android.content.Context;
import com.fossil.jm4;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.NumberPickerLarge;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileFormatter implements NumberPickerLarge.f, RulerValuePicker.a, Serializable {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ int UNIT_DEFAULT; // = -1;
    @DexIgnore
    public static /* final */ int UNIT_DOT; // = 2;
    @DexIgnore
    public static /* final */ int UNIT_FEET; // = 0;
    @DexIgnore
    public static /* final */ int UNIT_FEET_INCHES; // = 3;
    @DexIgnore
    public static /* final */ int UNIT_INCHES; // = 1;
    @DexIgnore
    public static /* final */ int UNIT_WEIGHT; // = 4;
    @DexIgnore
    public /* final */ int mUnit;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public ProfileFormatter(int i) {
        this.mUnit = i;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public String format(int i) {
        int i2 = this.mUnit;
        if (i2 == 0) {
            nh6 nh6 = nh6.a;
            Locale locale = Locale.US;
            wg6.a((Object) locale, "Locale.US");
            String a = jm4.a((Context) PortfolioApp.get.instance(), 2131887145);
            wg6.a((Object) a, "LanguageHelper.getString\u2026ce, R.string.feet_format)");
            Object[] objArr = {Integer.valueOf(i)};
            String format = String.format(locale, a, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
            return format;
        } else if (i2 == 1) {
            nh6 nh62 = nh6.a;
            Locale locale2 = Locale.US;
            wg6.a((Object) locale2, "Locale.US");
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131887174);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026, R.string.inches_format)");
            Object[] objArr2 = {Integer.valueOf(i)};
            String format2 = String.format(locale2, a2, Arrays.copyOf(objArr2, objArr2.length));
            wg6.a((Object) format2, "java.lang.String.format(locale, format, *args)");
            return format2;
        } else if (i2 == 2) {
            nh6 nh63 = nh6.a;
            Locale locale3 = Locale.US;
            wg6.a((Object) locale3, "Locale.US");
            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131887133);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026nce, R.string.dot_format)");
            Object[] objArr3 = {Integer.valueOf(i)};
            String format3 = String.format(locale3, a3, Arrays.copyOf(objArr3, objArr3.length));
            wg6.a((Object) format3, "java.lang.String.format(locale, format, *args)");
            return format3;
        } else if (i2 == 3) {
            nh6 nh64 = nh6.a;
            Locale locale4 = Locale.US;
            wg6.a((Object) locale4, "Locale.US");
            String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131887146);
            wg6.a((Object) a4, "LanguageHelper.getString\u2026tring.feet_inches_format)");
            Object[] objArr4 = {Integer.valueOf(i / 12), Integer.valueOf(i % 12)};
            String format4 = String.format(locale4, a4, Arrays.copyOf(objArr4, objArr4.length));
            wg6.a((Object) format4, "java.lang.String.format(locale, format, *args)");
            return format4;
        } else if (i2 != 4) {
            nh6 nh65 = nh6.a;
            Locale locale5 = Locale.US;
            wg6.a((Object) locale5, "Locale.US");
            String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131887224);
            wg6.a((Object) a5, "LanguageHelper.getString\u2026, R.string.normal_format)");
            Object[] objArr5 = {Integer.valueOf(i)};
            String format5 = String.format(locale5, a5, Arrays.copyOf(objArr5, objArr5.length));
            wg6.a((Object) format5, "java.lang.String.format(locale, format, *args)");
            return format5;
        } else {
            nh6 nh66 = nh6.a;
            Locale locale6 = Locale.US;
            wg6.a((Object) locale6, "Locale.US");
            String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131887360);
            wg6.a((Object) a6, "LanguageHelper.getString\u2026, R.string.weight_format)");
            Object[] objArr6 = {Integer.valueOf(i / 10), Integer.valueOf(i % 10)};
            String format6 = String.format(locale6, a6, Arrays.copyOf(objArr6, objArr6.length));
            wg6.a((Object) format6, "java.lang.String.format(locale, format, *args)");
            return format6;
        }
    }

    @DexIgnore
    public final int getMUnit() {
        return this.mUnit;
    }
}
