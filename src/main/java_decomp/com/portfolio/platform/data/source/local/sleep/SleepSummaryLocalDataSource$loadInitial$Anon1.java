package com.portfolio.platform.data.source.local.sleep;

import com.fossil.rm6;
import com.fossil.vk4;
import com.fossil.wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryLocalDataSource$loadInitial$Anon1 implements vk4.b {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

    @DexIgnore
    public SleepSummaryLocalDataSource$loadInitial$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.this$0 = sleepSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(vk4.b.a aVar) {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = this.this$0;
        sleepSummaryLocalDataSource.calculateStartDate(sleepSummaryLocalDataSource.mCreatedDate);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource2 = this.this$0;
        vk4.d dVar = vk4.d.INITIAL;
        Date mStartDate = sleepSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        wg6.a((Object) aVar, "helperCallback");
        rm6 unused = sleepSummaryLocalDataSource2.loadData(dVar, mStartDate, mEndDate, aVar);
    }
}
