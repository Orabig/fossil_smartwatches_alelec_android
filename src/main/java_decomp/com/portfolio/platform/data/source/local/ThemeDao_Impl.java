package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.t44;
import com.fossil.vh;
import com.portfolio.platform.data.model.Theme;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDao_Impl extends ThemeDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<Theme> __deletionAdapterOfTheme;
    @DexIgnore
    public /* final */ hh<Theme> __insertionAdapterOfTheme;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearThemeTable;
    @DexIgnore
    public /* final */ vh __preparedStmtOfResetAllCurrentTheme;
    @DexIgnore
    public /* final */ vh __preparedStmtOfSetCurrentThemeId;
    @DexIgnore
    public /* final */ t44 __themeConverter; // = new t44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<Theme> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `theme` (`isCurrentTheme`,`type`,`id`,`name`,`styles`) VALUES (?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, Theme theme) {
            miVar.a(1, theme.isCurrentTheme() ? 1 : 0);
            if (theme.getType() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, theme.getType());
            }
            if (theme.getId() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, theme.getId());
            }
            if (theme.getName() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, theme.getName());
            }
            String a = ThemeDao_Impl.this.__themeConverter.a(theme.getStyles());
            if (a == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<Theme> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `theme` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, Theme theme) {
            if (theme.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, theme.getId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE theme SET isCurrentTheme=0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends vh {
        @DexIgnore
        public Anon4(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE theme SET isCurrentTheme=1 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends vh {
        @DexIgnore
        public Anon5(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM theme";
        }
    }

    @DexIgnore
    public ThemeDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfTheme = new Anon1(ohVar);
        this.__deletionAdapterOfTheme = new Anon2(ohVar);
        this.__preparedStmtOfResetAllCurrentTheme = new Anon3(ohVar);
        this.__preparedStmtOfSetCurrentThemeId = new Anon4(ohVar);
        this.__preparedStmtOfClearThemeTable = new Anon5(ohVar);
    }

    @DexIgnore
    public void clearThemeTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearThemeTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearThemeTable.release(acquire);
        }
    }

    @DexIgnore
    public void deleteTheme(Theme theme) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfTheme.handle(theme);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public Theme getCurrentTheme() {
        boolean z = false;
        rh b = rh.b("SELECT * FROM theme WHERE isCurrentTheme=1", 0);
        this.__db.assertNotSuspendingTransaction();
        Theme theme = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "isCurrentTheme");
            int b3 = ai.b(a, "type");
            int b4 = ai.b(a, "id");
            int b5 = ai.b(a, "name");
            int b6 = ai.b(a, "styles");
            if (a.moveToFirst()) {
                Theme theme2 = new Theme(a.getString(b4), a.getString(b5), this.__themeConverter.a(a.getString(b6)));
                if (a.getInt(b2) != 0) {
                    z = true;
                }
                theme2.setCurrentTheme(z);
                theme2.setType(a.getString(b3));
                theme = theme2;
            }
            return theme;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public String getCurrentThemeId() {
        rh b = rh.b("SELECT id FROM theme WHERE isCurrentTheme=1", 0);
        this.__db.assertNotSuspendingTransaction();
        String str = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            if (a.moveToFirst()) {
                str = a.getString(0);
            }
            return str;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Theme> getListTheme() {
        rh b = rh.b("SELECT * FROM theme", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "isCurrentTheme");
            int b3 = ai.b(a, "type");
            int b4 = ai.b(a, "id");
            int b5 = ai.b(a, "name");
            int b6 = ai.b(a, "styles");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                Theme theme = new Theme(a.getString(b4), a.getString(b5), this.__themeConverter.a(a.getString(b6)));
                theme.setCurrentTheme(a.getInt(b2) != 0);
                theme.setType(a.getString(b3));
                arrayList.add(theme);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<String> getListThemeId() {
        rh b = rh.b("SELECT id FROM theme", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(a.getString(0));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<String> getListThemeIdByType(String str) {
        rh b = rh.b("SELECT id FROM theme WHERE type=?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(a.getString(0));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public String getNameById(String str) {
        rh b = rh.b("SELECT name FROM theme WHERE id=?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        String str2 = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            if (a.moveToFirst()) {
                str2 = a.getString(0);
            }
            return str2;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public Theme getThemeById(String str) {
        boolean z = true;
        rh b = rh.b("SELECT * FROM theme WHERE id=?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Theme theme = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "isCurrentTheme");
            int b3 = ai.b(a, "type");
            int b4 = ai.b(a, "id");
            int b5 = ai.b(a, "name");
            int b6 = ai.b(a, "styles");
            if (a.moveToFirst()) {
                Theme theme2 = new Theme(a.getString(b4), a.getString(b5), this.__themeConverter.a(a.getString(b6)));
                if (a.getInt(b2) == 0) {
                    z = false;
                }
                theme2.setCurrentTheme(z);
                theme2.setType(a.getString(b3));
                theme = theme2;
            }
            return theme;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertListTheme(List<Theme> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfTheme.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void resetAllCurrentTheme() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfResetAllCurrentTheme.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfResetAllCurrentTheme.release(acquire);
        }
    }

    @DexIgnore
    public void setCurrentThemeId(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfSetCurrentThemeId.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfSetCurrentThemeId.release(acquire);
        }
    }

    @DexIgnore
    public void upsertTheme(Theme theme) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfTheme.insert(theme);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
