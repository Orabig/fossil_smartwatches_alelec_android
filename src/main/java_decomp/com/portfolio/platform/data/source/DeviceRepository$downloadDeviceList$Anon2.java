package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$2", f = "DeviceRepository.kt", l = {}, m = "invokeSuspend")
public final class DeviceRepository$downloadDeviceList$Anon2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ap4 $response;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRepository$downloadDeviceList$Anon2(DeviceRepository deviceRepository, ap4 ap4, xe6 xe6) {
        super(2, xe6);
        this.this$0 = deviceRepository;
        this.$response = ap4;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        DeviceRepository$downloadDeviceList$Anon2 deviceRepository$downloadDeviceList$Anon2 = new DeviceRepository$downloadDeviceList$Anon2(this.this$0, this.$response, xe6);
        deviceRepository$downloadDeviceList$Anon2.p$ = (il6) obj;
        return deviceRepository$downloadDeviceList$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DeviceRepository$downloadDeviceList$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            ApiResponse apiResponse = (ApiResponse) ((cp4) this.$response).a();
            List<Device> list = apiResponse != null ? apiResponse.get_items() : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = DeviceRepository.Companion.getTAG();
            local.d(tag, "downloadDeviceList() - isFromCache = false " + list);
            this.this$0.removeStealDevice(list);
            if (list != null) {
                for (Device device : list) {
                    if (DeviceHelper.o.e(device.getDeviceId())) {
                        Device deviceByDeviceId = this.this$0.mDeviceDao.getDeviceByDeviceId(device.getDeviceId());
                        if (deviceByDeviceId != null) {
                            if (device.getBatteryLevel() <= 0) {
                                device.setBatteryLevel(deviceByDeviceId.getBatteryLevel());
                            }
                            device.setVibrationStrength(deviceByDeviceId.getVibrationStrength());
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String tag2 = DeviceRepository.Companion.getTAG();
                        local2.d(tag2, "update device: " + device);
                        this.this$0.mDeviceDao.addOrUpdateDevice(device);
                    } else {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String tag3 = DeviceRepository.Companion.getTAG();
                        local3.d(tag3, "Ignoring legacy device=" + device.getDeviceId());
                    }
                }
                return cd6.a;
            }
            wg6.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
