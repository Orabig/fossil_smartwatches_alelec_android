package com.portfolio.platform.data.source.remote;

import android.util.SparseArray;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.EmailAddress;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationRemoteDataSource implements NotificationsDataSource {
    @DexIgnore
    public void clearAllNotificationSetting() {
    }

    @DexIgnore
    public void clearAllPhoneFavoritesContacts() {
    }

    @DexIgnore
    public List<AppFilter> getAllAppFilterByHour(int i, int i2) {
        return null;
    }

    @DexIgnore
    public List<AppFilter> getAllAppFilterByVibration(int i) {
        return null;
    }

    @DexIgnore
    public List<AppFilter> getAllAppFilters(int i) {
        return new ArrayList();
    }

    @DexIgnore
    public List<ContactGroup> getAllContactGroups(int i) {
        return null;
    }

    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> getAllNotificationsByHour(String str, int i) {
        return null;
    }

    @DexIgnore
    public AppFilter getAppFilterByType(String str, int i) {
        return null;
    }

    @DexIgnore
    public Contact getContactById(int i) {
        return null;
    }

    @DexIgnore
    public List<Integer> getContactGroupId(List<Integer> list) {
        return null;
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingEmail(String str, int i) {
        return null;
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i) {
        return null;
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingSms(String str, int i) {
        return null;
    }

    @DexIgnore
    public List<Integer> getLocalContactId() {
        return null;
    }

    @DexIgnore
    public void removeAllAppFilters() {
    }

    @DexIgnore
    public void removeAppFilter(AppFilter appFilter) {
    }

    @DexIgnore
    public void removeContact(Contact contact) {
    }

    @DexIgnore
    public void removeContactGroup(ContactGroup contactGroup) {
    }

    @DexIgnore
    public void removeContactGroupList(List<ContactGroup> list) {
    }

    @DexIgnore
    public void removeListAppFilter(List<AppFilter> list) {
    }

    @DexIgnore
    public void removeListContact(List<Contact> list) {
    }

    @DexIgnore
    public void removeLocalRedundantContact(List<Integer> list) {
    }

    @DexIgnore
    public void removeLocalRedundantContactGroup(List<Integer> list) {
    }

    @DexIgnore
    public void removePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
    }

    @DexIgnore
    public void removePhoneFavoritesContact(String str) {
    }

    @DexIgnore
    public void removePhoneNumber(List<Integer> list) {
    }

    @DexIgnore
    public void removePhoneNumberByContactGroupId(int i) {
    }

    @DexIgnore
    public void removeRedundantContact() {
    }

    @DexIgnore
    public void saveAppFilter(AppFilter appFilter) {
    }

    @DexIgnore
    public void saveContact(Contact contact) {
    }

    @DexIgnore
    public void saveContactGroup(ContactGroup contactGroup) {
    }

    @DexIgnore
    public void saveContactGroupList(List<ContactGroup> list) {
    }

    @DexIgnore
    public void saveEmailAddress(EmailAddress emailAddress) {
    }

    @DexIgnore
    public void saveListAppFilters(List<AppFilter> list) {
    }

    @DexIgnore
    public void saveListContact(List<Contact> list) {
    }

    @DexIgnore
    public void saveListPhoneNumber(List<PhoneNumber> list) {
    }

    @DexIgnore
    public void savePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
    }

    @DexIgnore
    public void savePhoneNumber(PhoneNumber phoneNumber) {
    }
}
