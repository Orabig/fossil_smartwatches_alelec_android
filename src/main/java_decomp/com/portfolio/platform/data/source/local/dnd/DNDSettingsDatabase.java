package com.portfolio.platform.data.source.local.dnd;

import com.fossil.oh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DNDSettingsDatabase extends oh {
    @DexIgnore
    public abstract DNDScheduledTimeDao getDNDScheduledTimeDao();
}
