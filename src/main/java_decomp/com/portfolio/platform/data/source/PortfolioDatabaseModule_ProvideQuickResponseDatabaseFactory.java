package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory implements Factory<QuickResponseDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static QuickResponseDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideQuickResponseDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static QuickResponseDatabase proxyProvideQuickResponseDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        QuickResponseDatabase provideQuickResponseDatabase = portfolioDatabaseModule.provideQuickResponseDatabase(portfolioApp);
        z76.a(provideQuickResponseDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideQuickResponseDatabase;
    }

    @DexIgnore
    public QuickResponseDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
