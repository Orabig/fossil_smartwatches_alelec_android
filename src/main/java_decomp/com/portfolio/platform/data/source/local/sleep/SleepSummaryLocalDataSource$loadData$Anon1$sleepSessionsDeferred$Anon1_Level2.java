package com.portfolio.platform.data.source.local.sleep;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.ku3;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1", f = "SleepSummaryLocalDataSource.kt", l = {176}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super ap4<ku3>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ lc6 $downloadingRange;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource$loadData$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2(SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1, lc6 lc6, xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSummaryLocalDataSource$loadData$Anon1;
        this.$downloadingRange = lc6;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 = new SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2(this.this$0, this.$downloadingRange, xe6);
        sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2.p$ = (il6) obj;
        return sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            this.L$0 = this.p$;
            this.label = 1;
            obj = SleepSessionsRepository.fetchSleepSessions$default(this.this$0.this$0.mSleepSessionsRepository, (Date) this.$downloadingRange.getFirst(), (Date) this.$downloadingRange.getSecond(), 0, 0, this, 12, (Object) null);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il6 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
