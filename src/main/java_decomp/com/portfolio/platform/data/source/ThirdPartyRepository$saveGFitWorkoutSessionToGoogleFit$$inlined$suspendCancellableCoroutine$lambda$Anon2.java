package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.hh6;
import com.fossil.lc3;
import com.fossil.lk6;
import com.fossil.mc6;
import com.fossil.wg6;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 implements lc3 {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ lk6 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ hh6 $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ GoogleSignInAccount $googleSignInAccount$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    public ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(GoogleSignInAccount googleSignInAccount, hh6 hh6, int i, lk6 lk6, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$googleSignInAccount$inlined = googleSignInAccount;
        this.$countSizeOfList$inlined = hh6;
        this.$sizeOfGFitWorkoutSessionList$inlined = i;
        this.$continuation$inlined = lk6;
        this.this$0 = thirdPartyRepository;
        this.$gFitWorkoutSessionList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    public final void onFailure(Exception exc) {
        wg6.b(exc, "it");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("There was a problem inserting the session: ");
        exc.printStackTrace();
        sb.append(cd6.a);
        local.e(ThirdPartyRepository.TAG, sb.toString());
        hh6 hh6 = this.$countSizeOfList$inlined;
        hh6.element++;
        if (hh6.element >= this.$sizeOfGFitWorkoutSessionList$inlined && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitWorkoutSessionToGoogleFit");
            lk6 lk6 = this.$continuation$inlined;
            mc6.a aVar = mc6.Companion;
            lk6.resumeWith(mc6.m1constructorimpl((Object) null));
        }
    }
}
