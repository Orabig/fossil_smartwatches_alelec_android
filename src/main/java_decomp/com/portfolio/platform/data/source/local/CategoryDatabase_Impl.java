package com.portfolio.platform.data.source.local;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryDatabase_Impl extends CategoryDatabase {
    @DexIgnore
    public volatile CategoryDao _categoryDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `category` (`id` TEXT NOT NULL, `englishName` TEXT NOT NULL, `name` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `priority` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'deca3c3756d2b01ba1167f57e573ed39')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `category`");
            if (CategoryDatabase_Impl.this.mCallbacks != null) {
                int size = CategoryDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) CategoryDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (CategoryDatabase_Impl.this.mCallbacks != null) {
                int size = CategoryDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) CategoryDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = CategoryDatabase_Impl.this.mDatabase = iiVar;
            CategoryDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (CategoryDatabase_Impl.this.mCallbacks != null) {
                int size = CategoryDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) CategoryDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(6);
            hashMap.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap.put("englishName", new fi.a("englishName", "TEXT", true, 0, (String) null, 1));
            hashMap.put("name", new fi.a("name", "TEXT", true, 0, (String) null, 1));
            hashMap.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            hashMap.put("createdAt", new fi.a("createdAt", "TEXT", true, 0, (String) null, 1));
            hashMap.put("priority", new fi.a("priority", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi("category", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "category");
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "category(com.portfolio.platform.data.model.Category).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public CategoryDao categoryDao() {
        CategoryDao categoryDao;
        if (this._categoryDao != null) {
            return this._categoryDao;
        }
        synchronized (this) {
            if (this._categoryDao == null) {
                this._categoryDao = new CategoryDao_Impl(this);
            }
            categoryDao = this._categoryDao;
        }
        return categoryDao;
    }

    @DexIgnore
    public void clearAllTables() {
        CategoryDatabase_Impl.super.assertNotMainThread();
        ii a = CategoryDatabase_Impl.super.getOpenHelper().a();
        try {
            CategoryDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `category`");
            CategoryDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            CategoryDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"category"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(2), "deca3c3756d2b01ba1167f57e573ed39", "e8ca7dfeb46b68975e9271e7b97e5f11");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }
}
