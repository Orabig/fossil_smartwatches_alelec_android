package com.portfolio.platform.data.source;

import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jh6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3 implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3(GoalTrackingRepository goalTrackingRepository) {
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    public void onFail(int i) {
        FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "pushPendingGoalTrackingDataList onFetchFailed");
    }

    @DexIgnore
    public void onSuccess(List<GoalTrackingData> list) {
        wg6.b(list, "goalTrackingList");
        jh6 jh6 = new jh6();
        jh6.element = list.get(0).getDate();
        jh6 jh62 = new jh6();
        jh62.element = list.get(0).getDate();
        for (GoalTrackingData next : list) {
            if (next.getDate().getTime() < ((Date) jh6.element).getTime()) {
                jh6.element = next.getDate();
            }
            if (next.getDate().getTime() > ((Date) jh62.element).getTime()) {
                jh62.element = next.getDate();
            }
        }
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3$onSuccess$Anon1_Level2(this, jh6, jh62, (xe6) null), 3, (Object) null);
    }
}
