package com.portfolio.platform.data.source.local;

import com.portfolio.platform.data.model.LocalFile;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class FileDao {
    @DexIgnore
    public abstract void clearLocalFileTable();

    @DexIgnore
    public abstract void deleteLocalFile(LocalFile localFile);

    @DexIgnore
    public abstract List<LocalFile> getListLocalFile();

    @DexIgnore
    public abstract List<LocalFile> getListPendingFile();

    @DexIgnore
    public abstract LocalFile getLocalFileByRemoteUrl(String str);

    @DexIgnore
    public abstract void insertListLocalFile(List<LocalFile> list);

    @DexIgnore
    public abstract void upsertLocalFile(LocalFile localFile);
}
