package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideUserRemoteDataSourceFactory implements Factory<UserDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiServiceV2Provider;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> authApiGuestServiceProvider;
    @DexIgnore
    public /* final */ Provider<AuthApiUserService> authApiUserServiceProvider;
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideUserRemoteDataSourceFactory(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider, Provider<AuthApiGuestService> provider2, Provider<AuthApiUserService> provider3) {
        this.module = repositoriesModule;
        this.apiServiceV2Provider = provider;
        this.authApiGuestServiceProvider = provider2;
        this.authApiUserServiceProvider = provider3;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideUserRemoteDataSourceFactory create(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider, Provider<AuthApiGuestService> provider2, Provider<AuthApiUserService> provider3) {
        return new RepositoriesModule_ProvideUserRemoteDataSourceFactory(repositoriesModule, provider, provider2, provider3);
    }

    @DexIgnore
    public static UserDataSource provideInstance(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider, Provider<AuthApiGuestService> provider2, Provider<AuthApiUserService> provider3) {
        return proxyProvideUserRemoteDataSource(repositoriesModule, provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public static UserDataSource proxyProvideUserRemoteDataSource(RepositoriesModule repositoriesModule, ApiServiceV2 apiServiceV2, AuthApiGuestService authApiGuestService, AuthApiUserService authApiUserService) {
        UserDataSource provideUserRemoteDataSource = repositoriesModule.provideUserRemoteDataSource(apiServiceV2, authApiGuestService, authApiUserService);
        z76.a(provideUserRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideUserRemoteDataSource;
    }

    @DexIgnore
    public UserDataSource get() {
        return provideInstance(this.module, this.apiServiceV2Provider, this.authApiGuestServiceProvider, this.authApiUserServiceProvider);
    }
}
