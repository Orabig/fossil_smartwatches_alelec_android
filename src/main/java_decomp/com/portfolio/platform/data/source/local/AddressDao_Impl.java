package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressDao_Impl implements AddressDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<AddressOfWeather> __insertionAdapterOfAddressOfWeather;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<AddressOfWeather> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `addressOfWeather` (`id`,`lat`,`lng`,`address`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, AddressOfWeather addressOfWeather) {
            miVar.a(1, (long) addressOfWeather.getId());
            miVar.a(2, addressOfWeather.getLat());
            miVar.a(3, addressOfWeather.getLng());
            if (addressOfWeather.getAddress() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, addressOfWeather.getAddress());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM addressOfWeather";
        }
    }

    @DexIgnore
    public AddressDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfAddressOfWeather = new Anon1(ohVar);
        this.__preparedStmtOfClearData = new Anon2(ohVar);
    }

    @DexIgnore
    public void clearData() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearData.release(acquire);
        }
    }

    @DexIgnore
    public List<AddressOfWeather> getAllSavedAddress() {
        rh b = rh.b("SELECT * FROM addressOfWeather", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, Constants.LAT);
            int b4 = ai.b(a, "lng");
            int b5 = ai.b(a, "address");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                AddressOfWeather addressOfWeather = new AddressOfWeather(a.getDouble(b3), a.getDouble(b4), a.getString(b5));
                addressOfWeather.setId(a.getInt(b2));
                arrayList.add(addressOfWeather);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void saveAddress(AddressOfWeather addressOfWeather) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfAddressOfWeather.insert(addressOfWeather);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
