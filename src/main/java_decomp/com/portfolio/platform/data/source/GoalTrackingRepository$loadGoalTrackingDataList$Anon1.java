package com.portfolio.platform.data.source;

import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.xe6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository", f = "GoalTrackingRepository.kt", l = {394, 404}, m = "loadGoalTrackingDataList")
public final class GoalTrackingRepository$loadGoalTrackingDataList$Anon1 extends jf6 {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$loadGoalTrackingDataList$Anon1(GoalTrackingRepository goalTrackingRepository, xe6 xe6) {
        super(xe6);
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.loadGoalTrackingDataList((Date) null, (Date) null, 0, 0, this);
    }
}
