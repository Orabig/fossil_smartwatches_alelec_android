package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataRepository_Factory implements Factory<FitnessDataRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;
    @DexIgnore
    public /* final */ Provider<FitnessDataDao> mFitnessDataDaoProvider;

    @DexIgnore
    public FitnessDataRepository_Factory(Provider<FitnessDataDao> provider, Provider<ApiServiceV2> provider2) {
        this.mFitnessDataDaoProvider = provider;
        this.mApiServiceV2Provider = provider2;
    }

    @DexIgnore
    public static FitnessDataRepository_Factory create(Provider<FitnessDataDao> provider, Provider<ApiServiceV2> provider2) {
        return new FitnessDataRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static FitnessDataRepository newFitnessDataRepository(FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        return new FitnessDataRepository(fitnessDataDao, apiServiceV2);
    }

    @DexIgnore
    public static FitnessDataRepository provideInstance(Provider<FitnessDataDao> provider, Provider<ApiServiceV2> provider2) {
        return new FitnessDataRepository(provider.get(), provider2.get());
    }

    @DexIgnore
    public FitnessDataRepository get() {
        return provideInstance(this.mFitnessDataDaoProvider, this.mApiServiceV2Provider);
    }
}
