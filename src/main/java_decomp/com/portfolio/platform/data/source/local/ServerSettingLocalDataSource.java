package com.portfolio.platform.data.source.local;

import com.fossil.wg6;
import com.fossil.zm4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingLocalDataSource implements ServerSettingDataSource {
    @DexIgnore
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
        zm4.p.a().l().addOrUpdateServerSetting(serverSetting);
    }

    @DexIgnore
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
        zm4.p.a().l().a(list);
    }

    @DexIgnore
    public void clearData() {
        zm4.p.a().l().a();
    }

    @DexIgnore
    public ServerSetting getServerSettingByKey(String str) {
        wg6.b(str, "key");
        return zm4.p.a().l().getServerSettingByKey(str);
    }

    @DexIgnore
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        wg6.b(onGetServerSettingList, Constants.CALLBACK);
    }
}
