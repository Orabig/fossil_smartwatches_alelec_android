package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidePresetDaoFactory implements Factory<HybridPresetDao> {
    @DexIgnore
    public /* final */ Provider<HybridCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidePresetDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<HybridCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidePresetDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<HybridCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidePresetDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static HybridPresetDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<HybridCustomizeDatabase> provider) {
        return proxyProvidePresetDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static HybridPresetDao proxyProvidePresetDao(PortfolioDatabaseModule portfolioDatabaseModule, HybridCustomizeDatabase hybridCustomizeDatabase) {
        HybridPresetDao providePresetDao = portfolioDatabaseModule.providePresetDao(hybridCustomizeDatabase);
        z76.a(providePresetDao, "Cannot return null from a non-@Nullable @Provides method");
        return providePresetDao;
    }

    @DexIgnore
    public HybridPresetDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
