package com.portfolio.platform.data.source.local;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.hf6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zm4;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserLocalDataSource extends UserDataSource {
    @DexIgnore
    public void clearAllUser() {
        zm4.p.a().n().f();
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, xe6<? super Integer> xe6) {
        zm4.p.a().n().c(mFUser);
        return hf6.a((int) MFNetworkReturnCode.RESPONSE_OK);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return zm4.p.a().n().b();
    }

    @DexIgnore
    public void insertUser(MFUser mFUser) {
        wg6.b(mFUser, "user");
        zm4.p.a().n().b(mFUser);
    }

    @DexIgnore
    public Object updateUser(MFUser mFUser, boolean z, xe6<? super ap4<MFUser>> xe6) {
        zm4.p.a().n().a(mFUser);
        return new cp4(getCurrentUser(), false, 2, (qg6) null);
    }
}
