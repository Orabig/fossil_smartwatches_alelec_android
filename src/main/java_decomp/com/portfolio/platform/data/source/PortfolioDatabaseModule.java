package com.portfolio.platform.data.source;

import android.util.Log;
import com.fossil.an4;
import com.fossil.ik4;
import com.fossil.ji;
import com.fossil.nh;
import com.fossil.oh;
import com.fossil.wg6;
import com.fossil.xh;
import com.fossil.zm4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.local.AddressDao;
import com.portfolio.platform.data.source.local.AddressDatabase;
import com.portfolio.platform.data.source.local.AlarmsLocalDataSource;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.data.source.local.FileDatabase;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import com.portfolio.platform.data.source.local.alarm.AlarmDao;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule {
    @DexIgnore
    public final synchronized ActivitySampleDao provideActivitySampleDao(FitnessDatabase fitnessDatabase) {
        wg6.b(fitnessDatabase, "db");
        StringBuilder sb = new StringBuilder();
        sb.append("provideActivitySampleDao: DBNAME=");
        ji openHelper = fitnessDatabase.getOpenHelper();
        wg6.a((Object) openHelper, "db.openHelper");
        sb.append(openHelper.getDatabaseName());
        Log.d("PortfolioDatabaseModule", sb.toString());
        return fitnessDatabase.activitySampleDao();
    }

    @DexIgnore
    public final synchronized ActivitySummaryDao provideActivitySummaryDao(FitnessDatabase fitnessDatabase) {
        wg6.b(fitnessDatabase, "db");
        StringBuilder sb = new StringBuilder();
        sb.append("provideActivitySummaryDao: DBNAME=");
        ji openHelper = fitnessDatabase.getOpenHelper();
        wg6.a((Object) openHelper, "db.openHelper");
        sb.append(openHelper.getDatabaseName());
        Log.d("PortfolioDatabaseModule", sb.toString());
        return fitnessDatabase.activitySummaryDao();
    }

    @DexIgnore
    public final AddressDao provideAddressDao(AddressDatabase addressDatabase) {
        wg6.b(addressDatabase, "db");
        return addressDatabase.addressDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final synchronized AddressDatabase provideAddressDatabase(PortfolioApp r3) {
        AddressDatabase b;
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, AddressDatabase.class, "address.db");
        a.d();
        b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final AlarmsLocalDataSource provideAlarmsLocalDataSource(AlarmDao alarmDao) {
        wg6.b(alarmDao, "alarmDao");
        return new AlarmsLocalDataSource(alarmDao);
    }

    @DexIgnore
    public final AlarmsRemoteDataSource provideAlarmsRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "apiService");
        return new AlarmsRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public final CategoryDao provideCategoryDao(CategoryDatabase categoryDatabase) {
        wg6.b(categoryDatabase, "db");
        return categoryDatabase.categoryDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final synchronized CategoryDatabase provideCategoryDatabase(PortfolioApp r3) {
        CategoryDatabase b;
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, CategoryDatabase.class, "category.db");
        a.d();
        b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final ComplicationDao provideComplicationDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        wg6.b(dianaCustomizeDatabase, "db");
        return dianaCustomizeDatabase.getComplicationDao();
    }

    @DexIgnore
    public final ComplicationLastSettingDao provideComplicationSettingDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        wg6.b(dianaCustomizeDatabase, "db");
        return dianaCustomizeDatabase.getComplicationSettingDao();
    }

    @DexIgnore
    public final CustomizeRealDataDao provideCustomizeRealDataDao(CustomizeRealDataDatabase customizeRealDataDatabase) {
        wg6.b(customizeRealDataDatabase, "db");
        return customizeRealDataDatabase.realDataDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final CustomizeRealDataDatabase provideCustomizeRealDataDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, CustomizeRealDataDatabase.class, "customizeRealData.db");
        a.d();
        CustomizeRealDataDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final DNDSettingsDatabase provideDNDSettingsDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, DNDSettingsDatabase.class, "dndSettings.db");
        a.d();
        DNDSettingsDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final DeviceDao provideDeviceDao(DeviceDatabase deviceDatabase) {
        wg6.b(deviceDatabase, "db");
        return deviceDatabase.deviceDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final DeviceDatabase provideDeviceDatabase(PortfolioApp r4) {
        wg6.b(r4, "app");
        oh.a a = nh.a(r4, DeviceDatabase.class, "devices.db");
        a.a(new xh[]{DeviceDatabase.Companion.getMIGRATION_FROM_1_TO_2(), DeviceDatabase.Companion.getMIGRATION_FROM_2_TO_3()});
        a.d();
        DeviceDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final DianaCustomizeDatabase provideDianaCustomizeDatabase(PortfolioApp r4) {
        wg6.b(r4, "app");
        oh.a a = nh.a(r4, DianaCustomizeDatabase.class, "dianaCustomize.db");
        a.a(new xh[]{DianaCustomizeDatabase.Companion.getMIGRATION_FROM_13_TO_14()});
        a.d();
        a.c();
        DianaCustomizeDatabase b = a.b();
        wg6.a((Object) b, "Room\n                .da\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final DianaPresetDao provideDianaPresetDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        wg6.b(dianaCustomizeDatabase, "db");
        return dianaCustomizeDatabase.getPresetDao();
    }

    @DexIgnore
    public final FileDao provideFileDao(FileDatabase fileDatabase) {
        wg6.b(fileDatabase, "db");
        return fileDatabase.fileDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final FileDatabase provideFileDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, FileDatabase.class, "file.db");
        a.d();
        FileDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final synchronized FitnessDataDao provideFitnessDataDao(FitnessDatabase fitnessDatabase) {
        wg6.b(fitnessDatabase, "db");
        return fitnessDatabase.getFitnessDataDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final synchronized FitnessDatabase provideFitnessDatabase(PortfolioApp r5) {
        String str;
        FitnessDatabase b;
        wg6.b(r5, "app");
        Log.d("PortfolioDatabaseModule", "provideFitnessDatabase");
        MFUser b2 = zm4.p.a().n().b();
        if (b2 == null || (str = b2.getUserId()) == null) {
            str = "Anonymous";
        }
        Log.d("PortfolioDatabaseModule", "provideFitnessDatabase userId " + str);
        oh.a a = nh.a(r5, FitnessDatabase.class, str + "_" + "fitness.db");
        a.a(new xh[]{FitnessDatabase.Companion.getMIGRATION_FROM_4_TO_21()});
        a.d();
        a.c();
        b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder<Fit\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final ik4 provideFitnessHelper(an4 an4, ActivitySummaryDao activitySummaryDao) {
        wg6.b(an4, "sharedPreferencesManager");
        wg6.b(activitySummaryDao, "activitySummaryDao");
        return new ik4(an4, activitySummaryDao);
    }

    @DexIgnore
    public final GoalTrackingDao provideGoalTrackingDao(GoalTrackingDatabase goalTrackingDatabase) {
        wg6.b(goalTrackingDatabase, "db");
        return goalTrackingDatabase.getGoalTrackingDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final GoalTrackingDatabase provideGoalTrackingDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, GoalTrackingDatabase.class, "goalTracking.db");
        a.d();
        GoalTrackingDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final synchronized HeartRateDailySummaryDao provideHeartRateDailySummaryDao(FitnessDatabase fitnessDatabase) {
        wg6.b(fitnessDatabase, "db");
        return fitnessDatabase.getHeartRateDailySummaryDao();
    }

    @DexIgnore
    public final synchronized HeartRateSampleDao provideHeartRateDao(FitnessDatabase fitnessDatabase) {
        wg6.b(fitnessDatabase, "db");
        return fitnessDatabase.getHeartRateDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final HybridCustomizeDatabase provideHybridCustomizeDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, HybridCustomizeDatabase.class, "hybridCustomize.db");
        a.d();
        HybridCustomizeDatabase b = a.b();
        wg6.a((Object) b, "Room\n                .da\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final InAppNotificationDao provideInAppNotificationDao(InAppNotificationDatabase inAppNotificationDatabase) {
        wg6.b(inAppNotificationDatabase, "db");
        return inAppNotificationDatabase.inAppNotificationDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final InAppNotificationDatabase provideInAppNotificationDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, InAppNotificationDatabase.class, "inAppNotification.db");
        a.d();
        InAppNotificationDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final MicroAppDao provideMicroAppDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        wg6.b(hybridCustomizeDatabase, "db");
        return hybridCustomizeDatabase.microAppDao();
    }

    @DexIgnore
    public final MicroAppLastSettingDao provideMicroAppLastSettingDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        wg6.b(hybridCustomizeDatabase, "db");
        return hybridCustomizeDatabase.microAppLastSettingDao();
    }

    @DexIgnore
    public final NotificationSettingsDao provideNotificationSettingsDao(NotificationSettingsDatabase notificationSettingsDatabase) {
        wg6.b(notificationSettingsDatabase, "db");
        return notificationSettingsDatabase.getNotificationSettingsDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final NotificationSettingsDatabase provideNotificationSettingsDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, NotificationSettingsDatabase.class, "notificationSettings.db");
        a.d();
        NotificationSettingsDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final HybridPresetDao providePresetDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        wg6.b(hybridCustomizeDatabase, "db");
        return hybridCustomizeDatabase.presetDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final QuickResponseDatabase provideQuickResponseDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3.getApplicationContext(), QuickResponseDatabase.class, "quickResponse.db");
        a.d();
        QuickResponseDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final QuickResponseMessageDao provideQuickResponseMessageDao(QuickResponseDatabase quickResponseDatabase) {
        wg6.b(quickResponseDatabase, "db");
        return quickResponseDatabase.quickResponseMessageDao();
    }

    @DexIgnore
    public final QuickResponseSenderDao provideQuickResponseSenderDao(QuickResponseDatabase quickResponseDatabase) {
        wg6.b(quickResponseDatabase, "db");
        return quickResponseDatabase.quickResponseSenderDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final RemindersSettingsDatabase provideRemindersSettingsDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, RemindersSettingsDatabase.class, "remindersSettings.db");
        a.d();
        RemindersSettingsDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final synchronized SampleRawDao provideSampleRawDao(FitnessDatabase fitnessDatabase) {
        wg6.b(fitnessDatabase, "db");
        StringBuilder sb = new StringBuilder();
        sb.append("provideSampleRawDao: DBNAME=");
        ji openHelper = fitnessDatabase.getOpenHelper();
        wg6.a((Object) openHelper, "db.openHelper");
        sb.append(openHelper.getDatabaseName());
        Log.d("PortfolioDatabaseModule", sb.toString());
        return fitnessDatabase.sampleRawDao();
    }

    @DexIgnore
    public final SkuDao provideSkuDao(DeviceDatabase deviceDatabase) {
        wg6.b(deviceDatabase, "db");
        return deviceDatabase.skuDao();
    }

    @DexIgnore
    public final synchronized SleepDao provideSleepDao(SleepDatabase sleepDatabase) {
        wg6.b(sleepDatabase, "db");
        StringBuilder sb = new StringBuilder();
        sb.append("provideFitnessDao: DBNAME=");
        ji openHelper = sleepDatabase.getOpenHelper();
        wg6.a((Object) openHelper, "db.openHelper");
        sb.append(openHelper.getDatabaseName());
        Log.d("PortfolioDatabaseModule", sb.toString());
        return sleepDatabase.sleepDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final synchronized SleepDatabase provideSleepDatabase(PortfolioApp r5) {
        String str;
        SleepDatabase b;
        wg6.b(r5, "app");
        Log.d("PortfolioDatabaseModule", "provideSleepDatabase");
        MFUser b2 = zm4.p.a().n().b();
        if (b2 == null || (str = b2.getUserId()) == null) {
            str = "Anonymous";
        }
        Log.d("PortfolioDatabaseModule", "provideSleepDatabase: userID " + str);
        oh.a a = nh.a(r5, SleepDatabase.class, str + "_" + "sleep.db");
        a.a(new xh[]{SleepDatabase.Companion.getMIGRATION_FROM_3_TO_9()});
        a.d();
        b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder<Sle\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final ThemeDao provideThemeDao(ThemeDatabase themeDatabase) {
        wg6.b(themeDatabase, "db");
        return themeDatabase.themeDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final ThemeDatabase provideThemeDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, ThemeDatabase.class, "theme.db");
        a.d();
        ThemeDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final ThirdPartyDatabase provideThirdPartyDatabase(PortfolioApp r3) {
        wg6.b(r3, "app");
        oh.a a = nh.a(r3, ThirdPartyDatabase.class, "thirdParty.db");
        a.d();
        ThirdPartyDatabase b = a.b();
        wg6.a((Object) b, "Room.databaseBuilder(app\u2026\n                .build()");
        return b;
    }

    @DexIgnore
    public final WatchAppDao provideWatchAppDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        wg6.b(dianaCustomizeDatabase, "db");
        return dianaCustomizeDatabase.getWatchAppDao();
    }

    @DexIgnore
    public final WatchAppLastSettingDao provideWatchAppSettingDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        wg6.b(dianaCustomizeDatabase, "db");
        return dianaCustomizeDatabase.getWatchAppSettingDao();
    }

    @DexIgnore
    public final synchronized WorkoutDao provideWorkoutDao(FitnessDatabase fitnessDatabase) {
        wg6.b(fitnessDatabase, "db");
        return fitnessDatabase.getWorkoutDao();
    }

    @DexIgnore
    public final AlarmDao providesAlarmDao(AlarmDatabase alarmDatabase) {
        wg6.b(alarmDatabase, "db");
        return alarmDatabase.alarmDao();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final AlarmDatabase providesAlarmDatabase(PortfolioApp r6) {
        String str;
        wg6.b(r6, "app");
        MFUser b = zm4.p.a().n().b();
        if (b == null || (str = b.getUserId()) == null) {
            str = "Anonymous";
        }
        oh.a a = nh.a(r6, AlarmDatabase.class, str + "_" + "alarm.db");
        a.a(new xh[]{AlarmDatabase.Companion.migrating3Or4To5(str, 3), AlarmDatabase.Companion.migrating3Or4To5(str, 4), AlarmDatabase.Companion.getMIGRATION_FROM_5_TO_6()});
        a.d();
        AlarmDatabase b2 = a.b();
        wg6.a((Object) b2, "Room.databaseBuilder(app\u2026\n                .build()");
        return b2;
    }

    @DexIgnore
    public final RingStyleDao providesRingStyleDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        wg6.b(dianaCustomizeDatabase, "db");
        return dianaCustomizeDatabase.getRingStyleDao();
    }

    @DexIgnore
    public final WatchFaceDao providesWatchFaceDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        wg6.b(dianaCustomizeDatabase, "db");
        return dianaCustomizeDatabase.getWatchFaceDao();
    }
}
