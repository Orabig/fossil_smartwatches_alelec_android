package com.portfolio.platform.data.source;

import com.fossil.wg6;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppLastSettingRepository {
    @DexIgnore
    public /* final */ WatchAppLastSettingDao mWatchAppLastSettingDao;

    @DexIgnore
    public WatchAppLastSettingRepository(WatchAppLastSettingDao watchAppLastSettingDao) {
        wg6.b(watchAppLastSettingDao, "mWatchAppLastSettingDao");
        this.mWatchAppLastSettingDao = watchAppLastSettingDao;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mWatchAppLastSettingDao.cleanUp();
    }

    @DexIgnore
    public final WatchAppLastSetting getWatchAppLastSetting(String str) {
        wg6.b(str, "id");
        return this.mWatchAppLastSettingDao.getWatchAppLastSetting(str);
    }

    @DexIgnore
    public final void upsertWatchAppLastSetting(WatchAppLastSetting watchAppLastSetting) {
        wg6.b(watchAppLastSetting, "WatchAppLastSetting");
        this.mWatchAppLastSettingDao.upsertWatchAppLastSetting(watchAppLastSetting);
    }
}
