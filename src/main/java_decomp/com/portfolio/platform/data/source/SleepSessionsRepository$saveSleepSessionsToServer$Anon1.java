package com.portfolio.platform.data.source;

import android.text.TextUtils;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepSession;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.remote.UpsertApiResponse;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.SleepSessionsRepository$saveSleepSessionsToServer$1", f = "SleepSessionsRepository.kt", l = {282}, m = "invokeSuspend")
public final class SleepSessionsRepository$saveSleepSessionsToServer$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $allSleepSessionList;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository.PushPendingSleepSessionsCallback $pushPendingSleepSessionsCallback;
    @DexIgnore
    public /* final */ /* synthetic */ String $userId;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public int I$1;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$saveSleepSessionsToServer$Anon1(SleepSessionsRepository sleepSessionsRepository, List list, String str, SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback, xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSessionsRepository;
        this.$allSleepSessionList = list;
        this.$userId = str;
        this.$pushPendingSleepSessionsCallback = pushPendingSleepSessionsCallback;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        SleepSessionsRepository$saveSleepSessionsToServer$Anon1 sleepSessionsRepository$saveSleepSessionsToServer$Anon1 = new SleepSessionsRepository$saveSleepSessionsToServer$Anon1(this.this$0, this.$allSleepSessionList, this.$userId, this.$pushPendingSleepSessionsCallback, xe6);
        sleepSessionsRepository$saveSleepSessionsToServer$Anon1.p$ = (il6) obj;
        return sleepSessionsRepository$saveSleepSessionsToServer$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSessionsRepository$saveSleepSessionsToServer$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x01b8, code lost:
        if (r13.intValue() != 409000) goto L_0x01ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x01ce, code lost:
        if (r13.intValue() != 409001) goto L_0x01d0;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x023f  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x005a  */
    public final Object invokeSuspend(Object obj) {
        List list;
        Object obj2;
        Object obj3;
        il6 il6;
        int i;
        List list2;
        int i2;
        SleepSessionsRepository$saveSleepSessionsToServer$Anon1 sleepSessionsRepository$saveSleepSessionsToServer$Anon1;
        ap4 ap4;
        SleepSessionsRepository$saveSleepSessionsToServer$Anon1 sleepSessionsRepository$saveSleepSessionsToServer$Anon12;
        Object obj4;
        ArrayList arrayList;
        Object a = ff6.a();
        int i3 = this.label;
        int i4 = 0;
        boolean z = true;
        if (i3 == 0) {
            nc6.a(obj);
            il6 il62 = this.p$;
            FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.Companion.getTAG$app_fossilRelease(), "saveSleepSessionsToServer");
            il6 = il62;
            list2 = new ArrayList();
            i = 0;
            obj4 = a;
            sleepSessionsRepository$saveSleepSessionsToServer$Anon12 = this;
        } else if (i3 == 1) {
            i2 = this.I$1;
            list2 = (List) this.L$1;
            i = this.I$0;
            il6 = (il6) this.L$0;
            nc6.a(obj);
            obj2 = obj;
            obj3 = a;
            list = (List) this.L$2;
            sleepSessionsRepository$saveSleepSessionsToServer$Anon1 = this;
            ap4 = (ap4) obj2;
            i += 100;
            if (ap4 instanceof cp4) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "saveSleepSessionsToServer success, bravo!!! startIndex=" + i + " endIndex=" + i2);
                Object a2 = ((cp4) ap4).a();
                if (a2 != null) {
                    List<MFSleepSession> list3 = (List) a2;
                    for (MFSleepSession pinType : list3) {
                        pinType.setPinType(i4);
                    }
                    sleepSessionsRepository$saveSleepSessionsToServer$Anon1.this$0.mSleepDao.upsertSleepSessionList(list3);
                    list2.addAll(list3);
                    if (i >= sleepSessionsRepository$saveSleepSessionsToServer$Anon1.$allSleepSessionList.size()) {
                        SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback = sleepSessionsRepository$saveSleepSessionsToServer$Anon1.$pushPendingSleepSessionsCallback;
                        if (pushPendingSleepSessionsCallback != null) {
                            pushPendingSleepSessionsCallback.onSuccess(list2);
                        }
                        return cd6.a;
                    }
                }
                wg6.a();
                throw null;
            } else if (ap4 instanceof zo4) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease2 = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("saveSleepSessionsToServer failed, errorCode=");
                zo4 zo4 = (zo4) ap4;
                sb.append(zo4.a());
                sb.append(' ');
                sb.append("startIndex=");
                sb.append(i);
                sb.append(" endIndex=");
                sb.append(i2);
                local2.d(tAG$app_fossilRelease2, sb.toString());
                if (zo4.a() == 422) {
                    arrayList = new ArrayList();
                    if (!TextUtils.isEmpty(zo4.b())) {
                        try {
                        } catch (Exception e) {
                            e = e;
                        }
                        Object a3 = new Gson().a(((zo4) ap4).b(), new SleepSessionsRepository$saveSleepSessionsToServer$Anon1$type$Anon1_Level2().getType());
                        wg6.a(a3, "Gson().fromJson(repoResponse.errorItems, type)");
                        List list4 = ((UpsertApiResponse) a3).get_items();
                        if (list4.isEmpty() ^ z) {
                            int size = list4.size();
                            int i5 = 0;
                            while (i5 < size) {
                                Integer code = ((SleepSession) list4.get(i5)).getCode();
                                if (code == null) {
                                }
                                Integer code2 = ((SleepSession) list4.get(i5)).getCode();
                                if (code2 == null) {
                                }
                                if (((SleepSession) list4.get(i5)).getRealEndTime() != null) {
                                    MFSleepSession mFSleepSession = (MFSleepSession) list.get(i5);
                                    try {
                                        mFSleepSession.setPinType(0);
                                        arrayList.add(mFSleepSession);
                                    } catch (Exception e2) {
                                        e = e2;
                                    }
                                    i5++;
                                } else {
                                    i5++;
                                }
                            }
                        }
                        sleepSessionsRepository$saveSleepSessionsToServer$Anon1.this$0.mSleepDao.upsertSleepSessionList(arrayList);
                        list2.addAll(list);
                        if (i >= sleepSessionsRepository$saveSleepSessionsToServer$Anon1.$allSleepSessionList.size()) {
                            SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback2 = sleepSessionsRepository$saveSleepSessionsToServer$Anon1.$pushPendingSleepSessionsCallback;
                            if (pushPendingSleepSessionsCallback2 != null) {
                                pushPendingSleepSessionsCallback2.onSuccess(list2);
                            }
                            return cd6.a;
                        }
                        if (i >= sleepSessionsRepository$saveSleepSessionsToServer$Anon1.$allSleepSessionList.size()) {
                            SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback3 = sleepSessionsRepository$saveSleepSessionsToServer$Anon1.$pushPendingSleepSessionsCallback;
                            if (pushPendingSleepSessionsCallback3 != null) {
                                pushPendingSleepSessionsCallback3.onFail(zo4.a());
                            }
                            return cd6.a;
                        }
                        sleepSessionsRepository$saveSleepSessionsToServer$Anon12 = sleepSessionsRepository$saveSleepSessionsToServer$Anon1;
                        obj4 = obj3;
                        i4 = 0;
                        z = true;
                    }
                }
                if (i >= sleepSessionsRepository$saveSleepSessionsToServer$Anon1.$allSleepSessionList.size()) {
                }
                sleepSessionsRepository$saveSleepSessionsToServer$Anon12 = sleepSessionsRepository$saveSleepSessionsToServer$Anon1;
                obj4 = obj3;
                i4 = 0;
                z = true;
            }
            sleepSessionsRepository$saveSleepSessionsToServer$Anon12 = sleepSessionsRepository$saveSleepSessionsToServer$Anon1;
            obj4 = obj3;
            i4 = 0;
            z = true;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (i < sleepSessionsRepository$saveSleepSessionsToServer$Anon12.$allSleepSessionList.size()) {
            i2 = i + 100;
            if (i2 > sleepSessionsRepository$saveSleepSessionsToServer$Anon12.$allSleepSessionList.size()) {
                i2 = sleepSessionsRepository$saveSleepSessionsToServer$Anon12.$allSleepSessionList.size();
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease3 = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
            local3.d(tAG$app_fossilRelease3, "saveSleepSessionsToServer startIndex=" + i + " endIndex=" + i2);
            List subList = sleepSessionsRepository$saveSleepSessionsToServer$Anon12.$allSleepSessionList.subList(i, i2);
            SleepSessionsRepository sleepSessionsRepository = sleepSessionsRepository$saveSleepSessionsToServer$Anon12.this$0;
            String str = sleepSessionsRepository$saveSleepSessionsToServer$Anon12.$userId;
            sleepSessionsRepository$saveSleepSessionsToServer$Anon12.L$0 = il6;
            sleepSessionsRepository$saveSleepSessionsToServer$Anon12.I$0 = i;
            sleepSessionsRepository$saveSleepSessionsToServer$Anon12.L$1 = list2;
            sleepSessionsRepository$saveSleepSessionsToServer$Anon12.I$1 = i2;
            sleepSessionsRepository$saveSleepSessionsToServer$Anon12.L$2 = subList;
            sleepSessionsRepository$saveSleepSessionsToServer$Anon12.label = z ? 1 : 0;
            obj2 = sleepSessionsRepository.insertSleepSessionList(str, subList, sleepSessionsRepository$saveSleepSessionsToServer$Anon12);
            if (obj2 == obj4) {
                return obj4;
            }
            list = subList;
            obj3 = obj4;
            sleepSessionsRepository$saveSleepSessionsToServer$Anon1 = sleepSessionsRepository$saveSleepSessionsToServer$Anon12;
            ap4 = (ap4) obj2;
            i += 100;
            if (ap4 instanceof cp4) {
            }
            sleepSessionsRepository$saveSleepSessionsToServer$Anon12 = sleepSessionsRepository$saveSleepSessionsToServer$Anon1;
            obj4 = obj3;
            i4 = 0;
            z = true;
            if (i < sleepSessionsRepository$saveSleepSessionsToServer$Anon12.$allSleepSessionList.size()) {
            }
            return obj4;
        }
        return cd6.a;
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String tAG$app_fossilRelease4 = SleepSessionsRepository.Companion.getTAG$app_fossilRelease();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("saveSleepSessionsToServer ex=");
        e.printStackTrace();
        sb2.append(cd6.a);
        local4.e(tAG$app_fossilRelease4, sb2.toString());
        sleepSessionsRepository$saveSleepSessionsToServer$Anon1.this$0.mSleepDao.upsertSleepSessionList(arrayList);
        list2.addAll(list);
        if (i >= sleepSessionsRepository$saveSleepSessionsToServer$Anon1.$allSleepSessionList.size()) {
        }
        if (i >= sleepSessionsRepository$saveSleepSessionsToServer$Anon1.$allSleepSessionList.size()) {
        }
        sleepSessionsRepository$saveSleepSessionsToServer$Anon12 = sleepSessionsRepository$saveSleepSessionsToServer$Anon1;
        obj4 = obj3;
        i4 = 0;
        z = true;
        if (i < sleepSessionsRepository$saveSleepSessionsToServer$Anon12.$allSleepSessionList.size()) {
        }
        return cd6.a;
    }
}
