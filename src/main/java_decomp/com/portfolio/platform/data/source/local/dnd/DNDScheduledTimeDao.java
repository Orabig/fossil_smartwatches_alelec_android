package com.portfolio.platform.data.source.local.dnd;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DNDScheduledTimeDao {
    @DexIgnore
    void delete();

    @DexIgnore
    DNDScheduledTimeModel getDNDScheduledTimeModelWithFieldScheduledTimeType(int i);

    @DexIgnore
    LiveData<DNDScheduledTimeModel> getDNDScheduledTimeWithFieldScheduledTimeType(int i);

    @DexIgnore
    LiveData<List<DNDScheduledTimeModel>> getListDNDScheduledTime();

    @DexIgnore
    List<DNDScheduledTimeModel> getListDNDScheduledTimeModel();

    @DexIgnore
    void insertDNDScheduledTime(DNDScheduledTimeModel dNDScheduledTimeModel);

    @DexIgnore
    void insertListDNDScheduledTime(List<DNDScheduledTimeModel> list);
}
