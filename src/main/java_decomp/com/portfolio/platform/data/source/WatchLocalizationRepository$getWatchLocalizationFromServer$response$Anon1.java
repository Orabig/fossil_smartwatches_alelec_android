package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$response$1", f = "WatchLocalizationRepository.kt", l = {25}, m = "invokeSuspend")
public final class WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1 extends sf6 implements hg6<xe6<? super rx6<ApiResponse<WatchLocalization>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $locale;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1(WatchLocalizationRepository watchLocalizationRepository, String str, xe6 xe6) {
        super(1, xe6);
        this.this$0 = watchLocalizationRepository;
        this.$locale = str;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1(this.this$0, this.$locale, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 access$getApi$p = this.this$0.api;
            String str = this.$locale;
            this.label = 1;
            obj = access$getApi$p.getWatchLocalizationData(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
