package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealDataDao_Impl implements CustomizeRealDataDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<CustomizeRealData> __insertionAdapterOfCustomizeRealData;
    @DexIgnore
    public /* final */ vh __preparedStmtOfCleanUp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<CustomizeRealData> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `customizeRealData` (`id`,`value`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, CustomizeRealData customizeRealData) {
            if (customizeRealData.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, customizeRealData.getId());
            }
            if (customizeRealData.getValue() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, customizeRealData.getValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM customizeRealData";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<CustomizeRealData>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<CustomizeRealData> call() throws Exception {
            Cursor a = bi.a(CustomizeRealDataDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, ServerSetting.VALUE);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new CustomizeRealData(a.getString(b), a.getString(b2)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public CustomizeRealDataDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfCustomizeRealData = new Anon1(ohVar);
        this.__preparedStmtOfCleanUp = new Anon2(ohVar);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public LiveData<List<CustomizeRealData>> getAllRealDataAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"customizeRealData"}, false, new Anon3(rh.b("SELECT * FROM customizeRealData", 0)));
    }

    @DexIgnore
    public List<CustomizeRealData> getAllRealDataRaw() {
        rh b = rh.b("SELECT * FROM customizeRealData", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, ServerSetting.VALUE);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new CustomizeRealData(a.getString(b2), a.getString(b3)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public CustomizeRealData getRealData(String str) {
        rh b = rh.b("SELECT * FROM customizeRealData WHERE id=?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        CustomizeRealData customizeRealData = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, ServerSetting.VALUE);
            if (a.moveToFirst()) {
                customizeRealData = new CustomizeRealData(a.getString(b2), a.getString(b3));
            }
            return customizeRealData;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertRealData(CustomizeRealData customizeRealData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCustomizeRealData.insert(customizeRealData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
