package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.lh;
import com.fossil.ll6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.wk4;
import com.fossil.xe6;
import com.fossil.ye;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionLocalDataSource extends ye<Long, WorkoutSession> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ FitnessDataDao fitnessDataDao;
    @DexIgnore
    public /* final */ vk4.a listener;
    @DexIgnore
    public vk4 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = wk4.a(this.mHelper);
    @DexIgnore
    public /* final */ lh.c mObserver;
    @DexIgnore
    public int mOffset;
    @DexIgnore
    public /* final */ WorkoutDao workoutDao;
    @DexIgnore
    public /* final */ FitnessDatabase workoutDatabase;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends lh.c {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = workoutSessionLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wg6.b(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return WorkoutSessionLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = WorkoutSessionLocalDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "WorkoutSessionLocalDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public WorkoutSessionLocalDataSource(WorkoutSessionRepository workoutSessionRepository2, FitnessDataDao fitnessDataDao2, WorkoutDao workoutDao2, FitnessDatabase fitnessDatabase, Date date, u04 u04, vk4.a aVar) {
        wg6.b(workoutSessionRepository2, "workoutSessionRepository");
        wg6.b(fitnessDataDao2, "fitnessDataDao");
        wg6.b(workoutDao2, "workoutDao");
        wg6.b(fitnessDatabase, "workoutDatabase");
        wg6.b(date, "currentDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.fitnessDataDao = fitnessDataDao2;
        this.workoutDao = workoutDao2;
        this.workoutDatabase = fitnessDatabase;
        this.currentDate = date;
        this.listener = aVar;
        this.mHelper = new vk4(u04.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "workout_session", new String[0]);
        this.workoutDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<WorkoutSession> getDataInDatabase(int i) {
        return this.workoutDao.getWorkoutSessionsInDateDesc(this.currentDate, i);
    }

    @DexIgnore
    private final rm6 loadData(vk4.b.a aVar, int i) {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new WorkoutSessionLocalDataSource$loadData$Anon1(this, i, aVar, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ rm6 loadData$default(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, vk4.b.a aVar, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return workoutSessionLocalDataSource.loadData(aVar, i);
    }

    @DexIgnore
    public final vk4 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.workoutDatabase.getInvalidationTracker().b();
        return WorkoutSessionLocalDataSource.super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(ye.f<Long> fVar, ye.a<WorkoutSession> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - params = " + ((Long) fVar.a) + " & currentDate = " + this.currentDate);
        WorkoutDao workoutDao2 = this.workoutDao;
        Date date = this.currentDate;
        Object obj = fVar.a;
        wg6.a(obj, "params.key");
        aVar.a(workoutDao2.getWorkoutSessionsInDateAfterDesc(date, ((Number) obj).longValue(), fVar.b));
        this.mHelper.a(vk4.d.AFTER, (vk4.b) new WorkoutSessionLocalDataSource$loadAfter$Anon1(this));
    }

    @DexIgnore
    public void loadBefore(ye.f<Long> fVar, ye.a<WorkoutSession> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(ye.e<Long> eVar, ye.c<WorkoutSession> cVar) {
        wg6.b(eVar, "params");
        wg6.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - currentDate = " + this.currentDate);
        cVar.a(getDataInDatabase(eVar.a));
        this.mHelper.a(vk4.d.INITIAL, (vk4.b) new WorkoutSessionLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.workoutDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(vk4 vk4) {
        wg6.b(vk4, "<set-?>");
        this.mHelper = vk4;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wg6.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public Long getKey(WorkoutSession workoutSession) {
        wg6.b(workoutSession, "item");
        return Long.valueOf(workoutSession.getCreatedAt());
    }
}
