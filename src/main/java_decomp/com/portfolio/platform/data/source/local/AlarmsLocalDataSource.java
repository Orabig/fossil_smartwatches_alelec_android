package com.portfolio.platform.data.source.local;

import com.fossil.rd6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsLocalDataSource {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ AlarmDao alarmDao;

    @DexIgnore
    public AlarmsLocalDataSource(AlarmDao alarmDao2) {
        wg6.b(alarmDao2, "alarmDao");
        this.alarmDao = alarmDao2;
        String simpleName = AlarmsLocalDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "AlarmsLocalDataSource::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    public final void cleanUp() {
        this.alarmDao.cleanUp();
    }

    @DexIgnore
    public final void deleteAlarm(Alarm alarm) {
        wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        int removeAlarm = this.alarmDao.removeAlarm(alarm.getUri());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "deleteAlarm - uri: " + alarm.getUri() + " - row: " + removeAlarm);
    }

    @DexIgnore
    public final Alarm findNexActiveAlarm() {
        Calendar instance = Calendar.getInstance();
        return this.alarmDao.getInComingActiveAlarm((instance.get(11) * 60) + instance.get(12));
    }

    @DexIgnore
    public final List<Alarm> getActiveAlarms() {
        List<Alarm> activeAlarms = this.alarmDao.getActiveAlarms();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "getActiveAlarms - activeAlarms: " + activeAlarms);
        return activeAlarms;
    }

    @DexIgnore
    public final Alarm getAlarmById(String str) {
        wg6.b(str, "uri");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.d(str2, "getAlarmById - uri=" + str);
        return this.alarmDao.getAlarmWithUri(str);
    }

    @DexIgnore
    public final List<Alarm> getAllAlarmIgnoreDeletePinType() {
        List<Alarm> alarmsIgnoreDeleted = this.alarmDao.getAlarmsIgnoreDeleted();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("getAllAlarmIgnoreDeletePinType - alarms: ");
        ArrayList arrayList = new ArrayList(rd6.a(alarmsIgnoreDeleted, 10));
        for (Alarm uri : alarmsIgnoreDeleted) {
            arrayList.add(uri.getUri());
        }
        sb.append(arrayList);
        local.d(str, sb.toString());
        return alarmsIgnoreDeleted;
    }

    @DexIgnore
    public final List<Alarm> getAllPendingAlarm() {
        List<Alarm> pendingAlarms = this.alarmDao.getPendingAlarms();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("getAllPendingAlarm - alarms: ");
        ArrayList arrayList = new ArrayList(rd6.a(pendingAlarms, 10));
        for (Alarm uri : pendingAlarms) {
            arrayList.add(uri.getUri());
        }
        sb.append(arrayList);
        local.d(str, sb.toString());
        return pendingAlarms;
    }

    @DexIgnore
    public final List<Alarm> getInComingActiveAlarms() {
        return this.alarmDao.getInComingActiveAlarms();
    }

    @DexIgnore
    public final void insertAlarms(List<Alarm> list) {
        wg6.b(list, "alarms");
        Long[] insertAlarms = this.alarmDao.insertAlarms(list);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("upsertAlarm  - id: ");
        ArrayList arrayList = new ArrayList(rd6.a(list, 10));
        for (Alarm id : list) {
            arrayList.add(id.getId());
        }
        sb.append(arrayList);
        sb.append(" - row: ");
        sb.append(insertAlarms);
        local.d(str, sb.toString());
    }

    @DexIgnore
    public final void upsertAlarm(Alarm alarm) {
        wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        long insertAlarm = this.alarmDao.insertAlarm(alarm);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "upsertAlarm  - id: " + alarm.getId() + " - row: " + insertAlarm);
    }
}
