package com.portfolio.platform.data.source;

import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.remote.ApiResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository$loadSummaries$summaries$Anon1 extends TypeToken<ApiResponse<DailyHeartRateSummary>> {
}
