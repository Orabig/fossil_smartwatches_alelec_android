package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AlarmsDataSource {
    @DexIgnore
    void cleanUp();

    @DexIgnore
    Object deleteAlarm(Alarm alarm, xe6<? super ap4<Alarm>> xe6);

    @DexIgnore
    Object findAlarm(String str, xe6<? super Alarm> xe6);

    @DexIgnore
    Object getActiveAlarms(xe6<? super List<Alarm>> xe6);

    @DexIgnore
    Object getAlarms(xe6<? super ap4<List<Alarm>>> xe6);

    @DexIgnore
    Object getAllAlarmIgnorePinType(xe6<? super List<Alarm>> xe6);

    @DexIgnore
    Object setAlarm(Alarm alarm, xe6<? super ap4<Alarm>> xe6);
}
