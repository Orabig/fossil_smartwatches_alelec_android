package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.rm6;
import com.fossil.vk4;
import com.fossil.wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryLocalDataSource$loadInitial$Anon1 implements vk4.b {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource$loadInitial$Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.this$0 = goalTrackingSummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(vk4.b.a aVar) {
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = this.this$0;
        goalTrackingSummaryLocalDataSource.calculateStartDate(goalTrackingSummaryLocalDataSource.mCreatedDate);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource2 = this.this$0;
        vk4.d dVar = vk4.d.INITIAL;
        Date mStartDate = goalTrackingSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        wg6.a((Object) aVar, "helperCallback");
        rm6 unused = goalTrackingSummaryLocalDataSource2.loadData(dVar, mStartDate, mEndDate, aVar);
    }
}
