package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.LocalFile;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDao_Impl extends FileDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<LocalFile> __deletionAdapterOfLocalFile;
    @DexIgnore
    public /* final */ hh<LocalFile> __insertionAdapterOfLocalFile;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearLocalFileTable;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<LocalFile> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `localfile` (`pinType`,`fileName`,`localUri`,`remoteUrl`,`checksum`) VALUES (?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, LocalFile localFile) {
            miVar.a(1, (long) localFile.getPinType());
            if (localFile.getFileName() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, localFile.getFileName());
            }
            if (localFile.getLocalUri() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, localFile.getLocalUri());
            }
            if (localFile.getRemoteUrl() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, localFile.getRemoteUrl());
            }
            if (localFile.getChecksum() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, localFile.getChecksum());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<LocalFile> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `localfile` WHERE `remoteUrl` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, LocalFile localFile) {
            if (localFile.getRemoteUrl() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, localFile.getRemoteUrl());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM localfile";
        }
    }

    @DexIgnore
    public FileDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfLocalFile = new Anon1(ohVar);
        this.__deletionAdapterOfLocalFile = new Anon2(ohVar);
        this.__preparedStmtOfClearLocalFileTable = new Anon3(ohVar);
    }

    @DexIgnore
    public void clearLocalFileTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearLocalFileTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearLocalFileTable.release(acquire);
        }
    }

    @DexIgnore
    public void deleteLocalFile(LocalFile localFile) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfLocalFile.handle(localFile);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<LocalFile> getListLocalFile() {
        rh b = rh.b("SELECT * FROM localfile", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "fileName");
            int b4 = ai.b(a, "localUri");
            int b5 = ai.b(a, "remoteUrl");
            int b6 = ai.b(a, "checksum");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                LocalFile localFile = new LocalFile(a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6));
                localFile.setPinType(a.getInt(b2));
                arrayList.add(localFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<LocalFile> getListPendingFile() {
        rh b = rh.b("SELECT * FROM localfile WHERE pinType  <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "fileName");
            int b4 = ai.b(a, "localUri");
            int b5 = ai.b(a, "remoteUrl");
            int b6 = ai.b(a, "checksum");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                LocalFile localFile = new LocalFile(a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6));
                localFile.setPinType(a.getInt(b2));
                arrayList.add(localFile);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LocalFile getLocalFileByRemoteUrl(String str) {
        rh b = rh.b("SELECT * FROM localfile WHERE remoteUrl=?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        LocalFile localFile = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "fileName");
            int b4 = ai.b(a, "localUri");
            int b5 = ai.b(a, "remoteUrl");
            int b6 = ai.b(a, "checksum");
            if (a.moveToFirst()) {
                LocalFile localFile2 = new LocalFile(a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6));
                localFile2.setPinType(a.getInt(b2));
                localFile = localFile2;
            }
            return localFile;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertListLocalFile(List<LocalFile> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLocalFile.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertLocalFile(LocalFile localFile) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLocalFile.insert(localFile);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
