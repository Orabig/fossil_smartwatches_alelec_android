package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetRemoteDataSource_Factory implements Factory<HybridPresetRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public HybridPresetRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static HybridPresetRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new HybridPresetRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static HybridPresetRemoteDataSource newHybridPresetRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new HybridPresetRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public static HybridPresetRemoteDataSource provideInstance(Provider<ApiServiceV2> provider) {
        return new HybridPresetRemoteDataSource(provider.get());
    }

    @DexIgnore
    public HybridPresetRemoteDataSource get() {
        return provideInstance(this.mApiServiceV2Provider);
    }
}
