package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory implements Factory<ServerSettingDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiServiceProvider;
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        this.module = repositoriesModule;
        this.apiServiceProvider = provider;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory create(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return new RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory(repositoriesModule, provider);
    }

    @DexIgnore
    public static ServerSettingDataSource provideInstance(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return proxyProvideServerSettingRemoteDataSource(repositoriesModule, provider.get());
    }

    @DexIgnore
    public static ServerSettingDataSource proxyProvideServerSettingRemoteDataSource(RepositoriesModule repositoriesModule, ApiServiceV2 apiServiceV2) {
        ServerSettingDataSource provideServerSettingRemoteDataSource = repositoriesModule.provideServerSettingRemoteDataSource(apiServiceV2);
        z76.a(provideServerSettingRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideServerSettingRemoteDataSource;
    }

    @DexIgnore
    public ServerSettingDataSource get() {
        return provideInstance(this.module, this.apiServiceProvider);
    }
}
