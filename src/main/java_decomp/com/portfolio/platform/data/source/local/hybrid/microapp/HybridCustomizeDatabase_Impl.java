package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.os.Build;
import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeDatabase_Impl extends HybridCustomizeDatabase {
    @DexIgnore
    public volatile HybridPresetDao _hybridPresetDao;
    @DexIgnore
    public volatile MicroAppDao _microAppDao;
    @DexIgnore
    public volatile MicroAppLastSettingDao _microAppLastSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `microApp` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `categories` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `icon` TEXT, PRIMARY KEY(`id`, `serialNumber`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `microAppSetting` (`id` TEXT NOT NULL, `appId` TEXT NOT NULL, `setting` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`appId`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `microAppVariant` (`id` TEXT NOT NULL, `appId` TEXT NOT NULL, `name` TEXT NOT NULL, `description` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `majorNumber` INTEGER NOT NULL, `minorNumber` INTEGER NOT NULL, `serialNumber` TEXT NOT NULL, PRIMARY KEY(`appId`, `serialNumber`, `name`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `microAppLastSetting` (`appId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`appId`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `declarationFile` (`appId` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `variantName` TEXT NOT NULL, `id` TEXT NOT NULL, `description` TEXT, `content` TEXT, PRIMARY KEY(`appId`, `serialNumber`, `variantName`, `id`), FOREIGN KEY(`appId`, `serialNumber`, `variantName`) REFERENCES `microAppVariant`(`appId`, `serialNumber`, `name`) ON UPDATE CASCADE ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED)");
            iiVar.b("CREATE TABLE IF NOT EXISTS `hybridPreset` (`pinType` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `id` TEXT NOT NULL, `name` TEXT, `serialNumber` TEXT NOT NULL, `buttons` TEXT NOT NULL, `isActive` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `hybridRecommendPreset` (`id` TEXT NOT NULL, `name` TEXT, `serialNumber` TEXT NOT NULL, `buttons` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1cd16a2fbeeede74e77b1faa1351cfe8')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `microApp`");
            iiVar.b("DROP TABLE IF EXISTS `microAppSetting`");
            iiVar.b("DROP TABLE IF EXISTS `microAppVariant`");
            iiVar.b("DROP TABLE IF EXISTS `microAppLastSetting`");
            iiVar.b("DROP TABLE IF EXISTS `declarationFile`");
            iiVar.b("DROP TABLE IF EXISTS `hybridPreset`");
            iiVar.b("DROP TABLE IF EXISTS `hybridRecommendPreset`");
            if (HybridCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = HybridCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) HybridCustomizeDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (HybridCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = HybridCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) HybridCustomizeDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = HybridCustomizeDatabase_Impl.this.mDatabase = iiVar;
            iiVar.b("PRAGMA foreign_keys = ON");
            HybridCustomizeDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (HybridCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = HybridCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) HybridCustomizeDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            ii iiVar2 = iiVar;
            HashMap hashMap = new HashMap(8);
            hashMap.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap.put("name", new fi.a("name", "TEXT", true, 0, (String) null, 1));
            hashMap.put("nameKey", new fi.a("nameKey", "TEXT", true, 0, (String) null, 1));
            hashMap.put("serialNumber", new fi.a("serialNumber", "TEXT", true, 2, (String) null, 1));
            hashMap.put(HelpRequest.INCLUDE_CATEGORIES, new fi.a(HelpRequest.INCLUDE_CATEGORIES, "TEXT", true, 0, (String) null, 1));
            hashMap.put("description", new fi.a("description", "TEXT", true, 0, (String) null, 1));
            hashMap.put("descriptionKey", new fi.a("descriptionKey", "TEXT", true, 0, (String) null, 1));
            hashMap.put(MicroApp.COLUMN_ICON, new fi.a(MicroApp.COLUMN_ICON, "TEXT", false, 0, (String) null, 1));
            fi fiVar = new fi("microApp", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar2, "microApp");
            if (!fiVar.equals(a)) {
                return new qh.b(false, "microApp(com.portfolio.platform.data.model.room.microapp.MicroApp).\n Expected:\n" + fiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(6);
            fi.a aVar = r10;
            fi.a aVar2 = new fi.a("id", "TEXT", true, 0, (String) null, 1);
            hashMap2.put("id", aVar);
            hashMap2.put("appId", new fi.a("appId", "TEXT", true, 1, (String) null, 1));
            hashMap2.put(MicroAppSetting.SETTING, new fi.a(MicroAppSetting.SETTING, "TEXT", false, 0, (String) null, 1));
            hashMap2.put("createdAt", new fi.a("createdAt", "TEXT", true, 0, (String) null, 1));
            hashMap2.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            hashMap2.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            fi fiVar2 = new fi(MicroAppSetting.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar2, MicroAppSetting.TABLE_NAME);
            if (!fiVar2.equals(a2)) {
                return new qh.b(false, "microAppSetting(com.portfolio.platform.data.model.room.microapp.MicroAppSetting).\n Expected:\n" + fiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(9);
            hashMap3.put("id", new fi.a("id", "TEXT", true, 0, (String) null, 1));
            hashMap3.put("appId", new fi.a("appId", "TEXT", true, 1, (String) null, 1));
            hashMap3.put("name", new fi.a("name", "TEXT", true, 3, (String) null, 1));
            hashMap3.put("description", new fi.a("description", "TEXT", true, 0, (String) null, 1));
            hashMap3.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap3.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            hashMap3.put(MicroAppVariant.COLUMN_MAJOR_NUMBER, new fi.a(MicroAppVariant.COLUMN_MAJOR_NUMBER, "INTEGER", true, 0, (String) null, 1));
            hashMap3.put(MicroAppVariant.COLUMN_MINOR_NUMBER, new fi.a(MicroAppVariant.COLUMN_MINOR_NUMBER, "INTEGER", true, 0, (String) null, 1));
            hashMap3.put("serialNumber", new fi.a("serialNumber", "TEXT", true, 2, (String) null, 1));
            fi fiVar3 = new fi("microAppVariant", hashMap3, new HashSet(0), new HashSet(0));
            fi a3 = fi.a(iiVar2, "microAppVariant");
            if (!fiVar3.equals(a3)) {
                return new qh.b(false, "microAppVariant(com.portfolio.platform.data.model.room.microapp.MicroAppVariant).\n Expected:\n" + fiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(3);
            hashMap4.put("appId", new fi.a("appId", "TEXT", true, 1, (String) null, 1));
            hashMap4.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            hashMap4.put(MicroAppSetting.SETTING, new fi.a(MicroAppSetting.SETTING, "TEXT", true, 0, (String) null, 1));
            fi fiVar4 = new fi("microAppLastSetting", hashMap4, new HashSet(0), new HashSet(0));
            fi a4 = fi.a(iiVar2, "microAppLastSetting");
            if (!fiVar4.equals(a4)) {
                return new qh.b(false, "microAppLastSetting(com.portfolio.platform.data.model.microapp.MicroAppLastSetting).\n Expected:\n" + fiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(6);
            hashMap5.put("appId", new fi.a("appId", "TEXT", true, 1, (String) null, 1));
            hashMap5.put("serialNumber", new fi.a("serialNumber", "TEXT", true, 2, (String) null, 1));
            hashMap5.put("variantName", new fi.a("variantName", "TEXT", true, 3, (String) null, 1));
            hashMap5.put("id", new fi.a("id", "TEXT", true, 4, (String) null, 1));
            hashMap5.put("description", new fi.a("description", "TEXT", false, 0, (String) null, 1));
            hashMap5.put("content", new fi.a("content", "TEXT", false, 0, (String) null, 1));
            HashSet hashSet = new HashSet(1);
            hashSet.add(new fi.b("microAppVariant", "NO ACTION", "CASCADE", Arrays.asList(new String[]{"appId", "serialNumber", "variantName"}), Arrays.asList(new String[]{"appId", "serialNumber", "name"})));
            fi fiVar5 = new fi("declarationFile", hashMap5, hashSet, new HashSet(0));
            fi a5 = fi.a(iiVar2, "declarationFile");
            if (!fiVar5.equals(a5)) {
                return new qh.b(false, "declarationFile(com.portfolio.platform.data.model.room.microapp.DeclarationFile).\n Expected:\n" + fiVar5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(8);
            hashMap6.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            hashMap6.put("createdAt", new fi.a("createdAt", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("updatedAt", new fi.a("updatedAt", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap6.put("name", new fi.a("name", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("serialNumber", new fi.a("serialNumber", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("buttons", new fi.a("buttons", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("isActive", new fi.a("isActive", "INTEGER", true, 0, (String) null, 1));
            fi fiVar6 = new fi("hybridPreset", hashMap6, new HashSet(0), new HashSet(0));
            fi a6 = fi.a(iiVar2, "hybridPreset");
            if (!fiVar6.equals(a6)) {
                return new qh.b(false, "hybridPreset(com.portfolio.platform.data.model.room.microapp.HybridPreset).\n Expected:\n" + fiVar6 + "\n Found:\n" + a6);
            }
            HashMap hashMap7 = new HashMap(7);
            hashMap7.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap7.put("name", new fi.a("name", "TEXT", false, 0, (String) null, 1));
            hashMap7.put("serialNumber", new fi.a("serialNumber", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("buttons", new fi.a("buttons", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("isDefault", new fi.a("isDefault", "INTEGER", true, 0, (String) null, 1));
            hashMap7.put("createdAt", new fi.a("createdAt", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            fi fiVar7 = new fi("hybridRecommendPreset", hashMap7, new HashSet(0), new HashSet(0));
            fi a7 = fi.a(iiVar2, "hybridRecommendPreset");
            if (fiVar7.equals(a7)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "hybridRecommendPreset(com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset).\n Expected:\n" + fiVar7 + "\n Found:\n" + a7);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        HybridCustomizeDatabase_Impl.super.assertNotMainThread();
        ii a = HybridCustomizeDatabase_Impl.super.getOpenHelper().a();
        boolean z = Build.VERSION.SDK_INT >= 21;
        if (!z) {
            try {
                a.b("PRAGMA foreign_keys = FALSE");
            } catch (Throwable th) {
                HybridCustomizeDatabase_Impl.super.endTransaction();
                if (!z) {
                    a.b("PRAGMA foreign_keys = TRUE");
                }
                a.d("PRAGMA wal_checkpoint(FULL)").close();
                if (!a.A()) {
                    a.b("VACUUM");
                }
                throw th;
            }
        }
        HybridCustomizeDatabase_Impl.super.beginTransaction();
        if (z) {
            a.b("PRAGMA defer_foreign_keys = TRUE");
        }
        a.b("DELETE FROM `microApp`");
        a.b("DELETE FROM `microAppSetting`");
        a.b("DELETE FROM `microAppVariant`");
        a.b("DELETE FROM `microAppLastSetting`");
        a.b("DELETE FROM `declarationFile`");
        a.b("DELETE FROM `hybridPreset`");
        a.b("DELETE FROM `hybridRecommendPreset`");
        HybridCustomizeDatabase_Impl.super.setTransactionSuccessful();
        HybridCustomizeDatabase_Impl.super.endTransaction();
        if (!z) {
            a.b("PRAGMA foreign_keys = TRUE");
        }
        a.d("PRAGMA wal_checkpoint(FULL)").close();
        if (!a.A()) {
            a.b("VACUUM");
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"microApp", MicroAppSetting.TABLE_NAME, "microAppVariant", "microAppLastSetting", "declarationFile", "hybridPreset", "hybridRecommendPreset"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(10), "1cd16a2fbeeede74e77b1faa1351cfe8", "c7fc8c005bafc0b2f864a0c6cb79cc7b");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public MicroAppDao microAppDao() {
        MicroAppDao microAppDao;
        if (this._microAppDao != null) {
            return this._microAppDao;
        }
        synchronized (this) {
            if (this._microAppDao == null) {
                this._microAppDao = new MicroAppDao_Impl(this);
            }
            microAppDao = this._microAppDao;
        }
        return microAppDao;
    }

    @DexIgnore
    public MicroAppLastSettingDao microAppLastSettingDao() {
        MicroAppLastSettingDao microAppLastSettingDao;
        if (this._microAppLastSettingDao != null) {
            return this._microAppLastSettingDao;
        }
        synchronized (this) {
            if (this._microAppLastSettingDao == null) {
                this._microAppLastSettingDao = new MicroAppLastSettingDao_Impl(this);
            }
            microAppLastSettingDao = this._microAppLastSettingDao;
        }
        return microAppLastSettingDao;
    }

    @DexIgnore
    public HybridPresetDao presetDao() {
        HybridPresetDao hybridPresetDao;
        if (this._hybridPresetDao != null) {
            return this._hybridPresetDao;
        }
        synchronized (this) {
            if (this._hybridPresetDao == null) {
                this._hybridPresetDao = new HybridPresetDao_Impl(this);
            }
            hybridPresetDao = this._hybridPresetDao;
        }
        return hybridPresetDao;
    }
}
