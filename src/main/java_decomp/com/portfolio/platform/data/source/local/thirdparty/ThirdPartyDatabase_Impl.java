package com.portfolio.platform.data.source.local.thirdparty;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyDatabase_Impl extends ThirdPartyDatabase {
    @DexIgnore
    public volatile GFitActiveTimeDao _gFitActiveTimeDao;
    @DexIgnore
    public volatile GFitHeartRateDao _gFitHeartRateDao;
    @DexIgnore
    public volatile GFitSampleDao _gFitSampleDao;
    @DexIgnore
    public volatile GFitSleepDao _gFitSleepDao;
    @DexIgnore
    public volatile GFitWorkoutSessionDao _gFitWorkoutSessionDao;
    @DexIgnore
    public volatile UASampleDao _uASampleDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `gFitSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS `gFitActiveTime` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `activeTimes` TEXT NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS `gFitHeartRate` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `value` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS `gFitWorkoutSession` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL, `workoutType` INTEGER NOT NULL, `steps` TEXT NOT NULL, `calories` TEXT NOT NULL, `distances` TEXT NOT NULL, `heartRates` TEXT NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS `uaSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `time` INTEGER NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS `gFitSleep` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `sleepMins` INTEGER NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '4a002011cd11b3072c8efe0a76ce12f3')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `gFitSample`");
            iiVar.b("DROP TABLE IF EXISTS `gFitActiveTime`");
            iiVar.b("DROP TABLE IF EXISTS `gFitHeartRate`");
            iiVar.b("DROP TABLE IF EXISTS `gFitWorkoutSession`");
            iiVar.b("DROP TABLE IF EXISTS `uaSample`");
            iiVar.b("DROP TABLE IF EXISTS `gFitSleep`");
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = ThirdPartyDatabase_Impl.this.mDatabase = iiVar;
            ThirdPartyDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            ii iiVar2 = iiVar;
            HashMap hashMap = new HashMap(6);
            hashMap.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap.put("step", new fi.a("step", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("distance", new fi.a("distance", "REAL", true, 0, (String) null, 1));
            hashMap.put("calorie", new fi.a("calorie", "REAL", true, 0, (String) null, 1));
            hashMap.put("startTime", new fi.a("startTime", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("endTime", new fi.a("endTime", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi("gFitSample", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar2, "gFitSample");
            if (!fiVar.equals(a)) {
                return new qh.b(false, "gFitSample(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample).\n Expected:\n" + fiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap2.put("activeTimes", new fi.a("activeTimes", "TEXT", true, 0, (String) null, 1));
            fi fiVar2 = new fi("gFitActiveTime", hashMap2, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar2, "gFitActiveTime");
            if (!fiVar2.equals(a2)) {
                return new qh.b(false, "gFitActiveTime(com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime).\n Expected:\n" + fiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(4);
            fi.a aVar = r11;
            fi.a aVar2 = new fi.a("id", "INTEGER", true, 1, (String) null, 1);
            hashMap3.put("id", aVar);
            hashMap3.put(ServerSetting.VALUE, new fi.a(ServerSetting.VALUE, "REAL", true, 0, (String) null, 1));
            hashMap3.put("startTime", new fi.a("startTime", "INTEGER", true, 0, (String) null, 1));
            hashMap3.put("endTime", new fi.a("endTime", "INTEGER", true, 0, (String) null, 1));
            fi fiVar3 = new fi("gFitHeartRate", hashMap3, new HashSet(0), new HashSet(0));
            fi a3 = fi.a(iiVar2, "gFitHeartRate");
            if (!fiVar3.equals(a3)) {
                return new qh.b(false, "gFitHeartRate(com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate).\n Expected:\n" + fiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(8);
            hashMap4.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap4.put("startTime", new fi.a("startTime", "INTEGER", true, 0, (String) null, 1));
            hashMap4.put("endTime", new fi.a("endTime", "INTEGER", true, 0, (String) null, 1));
            hashMap4.put("workoutType", new fi.a("workoutType", "INTEGER", true, 0, (String) null, 1));
            hashMap4.put("steps", new fi.a("steps", "TEXT", true, 0, (String) null, 1));
            hashMap4.put(Constants.CALORIES, new fi.a(Constants.CALORIES, "TEXT", true, 0, (String) null, 1));
            hashMap4.put("distances", new fi.a("distances", "TEXT", true, 0, (String) null, 1));
            hashMap4.put("heartRates", new fi.a("heartRates", "TEXT", true, 0, (String) null, 1));
            fi fiVar4 = new fi("gFitWorkoutSession", hashMap4, new HashSet(0), new HashSet(0));
            fi a4 = fi.a(iiVar2, "gFitWorkoutSession");
            if (!fiVar4.equals(a4)) {
                return new qh.b(false, "gFitWorkoutSession(com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession).\n Expected:\n" + fiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(5);
            hashMap5.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap5.put("step", new fi.a("step", "INTEGER", true, 0, (String) null, 1));
            hashMap5.put("distance", new fi.a("distance", "REAL", true, 0, (String) null, 1));
            hashMap5.put("calorie", new fi.a("calorie", "REAL", true, 0, (String) null, 1));
            hashMap5.put(LogBuilder.KEY_TIME, new fi.a(LogBuilder.KEY_TIME, "INTEGER", true, 0, (String) null, 1));
            fi fiVar5 = new fi("uaSample", hashMap5, new HashSet(0), new HashSet(0));
            fi a5 = fi.a(iiVar2, "uaSample");
            if (!fiVar5.equals(a5)) {
                return new qh.b(false, "uaSample(com.portfolio.platform.data.model.thirdparty.ua.UASample).\n Expected:\n" + fiVar5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(4);
            hashMap6.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap6.put("sleepMins", new fi.a("sleepMins", "INTEGER", true, 0, (String) null, 1));
            hashMap6.put("startTime", new fi.a("startTime", "INTEGER", true, 0, (String) null, 1));
            hashMap6.put("endTime", new fi.a("endTime", "INTEGER", true, 0, (String) null, 1));
            fi fiVar6 = new fi("gFitSleep", hashMap6, new HashSet(0), new HashSet(0));
            fi a6 = fi.a(iiVar2, "gFitSleep");
            if (fiVar6.equals(a6)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "gFitSleep(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep).\n Expected:\n" + fiVar6 + "\n Found:\n" + a6);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        ThirdPartyDatabase_Impl.super.assertNotMainThread();
        ii a = ThirdPartyDatabase_Impl.super.getOpenHelper().a();
        try {
            ThirdPartyDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `gFitSample`");
            a.b("DELETE FROM `gFitActiveTime`");
            a.b("DELETE FROM `gFitHeartRate`");
            a.b("DELETE FROM `gFitWorkoutSession`");
            a.b("DELETE FROM `uaSample`");
            a.b("DELETE FROM `gFitSleep`");
            ThirdPartyDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            ThirdPartyDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"gFitSample", "gFitActiveTime", "gFitHeartRate", "gFitWorkoutSession", "uaSample", "gFitSleep"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(2), "4a002011cd11b3072c8efe0a76ce12f3", "fd54d50c044f828885f0e40cfeeed303");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public GFitActiveTimeDao getGFitActiveTimeDao() {
        GFitActiveTimeDao gFitActiveTimeDao;
        if (this._gFitActiveTimeDao != null) {
            return this._gFitActiveTimeDao;
        }
        synchronized (this) {
            if (this._gFitActiveTimeDao == null) {
                this._gFitActiveTimeDao = new GFitActiveTimeDao_Impl(this);
            }
            gFitActiveTimeDao = this._gFitActiveTimeDao;
        }
        return gFitActiveTimeDao;
    }

    @DexIgnore
    public GFitHeartRateDao getGFitHeartRateDao() {
        GFitHeartRateDao gFitHeartRateDao;
        if (this._gFitHeartRateDao != null) {
            return this._gFitHeartRateDao;
        }
        synchronized (this) {
            if (this._gFitHeartRateDao == null) {
                this._gFitHeartRateDao = new GFitHeartRateDao_Impl(this);
            }
            gFitHeartRateDao = this._gFitHeartRateDao;
        }
        return gFitHeartRateDao;
    }

    @DexIgnore
    public GFitSampleDao getGFitSampleDao() {
        GFitSampleDao gFitSampleDao;
        if (this._gFitSampleDao != null) {
            return this._gFitSampleDao;
        }
        synchronized (this) {
            if (this._gFitSampleDao == null) {
                this._gFitSampleDao = new GFitSampleDao_Impl(this);
            }
            gFitSampleDao = this._gFitSampleDao;
        }
        return gFitSampleDao;
    }

    @DexIgnore
    public GFitSleepDao getGFitSleepDao() {
        GFitSleepDao gFitSleepDao;
        if (this._gFitSleepDao != null) {
            return this._gFitSleepDao;
        }
        synchronized (this) {
            if (this._gFitSleepDao == null) {
                this._gFitSleepDao = new GFitSleepDao_Impl(this);
            }
            gFitSleepDao = this._gFitSleepDao;
        }
        return gFitSleepDao;
    }

    @DexIgnore
    public GFitWorkoutSessionDao getGFitWorkoutSessionDao() {
        GFitWorkoutSessionDao gFitWorkoutSessionDao;
        if (this._gFitWorkoutSessionDao != null) {
            return this._gFitWorkoutSessionDao;
        }
        synchronized (this) {
            if (this._gFitWorkoutSessionDao == null) {
                this._gFitWorkoutSessionDao = new GFitWorkoutSessionDao_Impl(this);
            }
            gFitWorkoutSessionDao = this._gFitWorkoutSessionDao;
        }
        return gFitWorkoutSessionDao;
    }

    @DexIgnore
    public UASampleDao getUASampleDao() {
        UASampleDao uASampleDao;
        if (this._uASampleDao != null) {
            return this._uASampleDao;
        }
        synchronized (this) {
            if (this._uASampleDao == null) {
                this._uASampleDao = new UASampleDao_Impl(this);
            }
            uASampleDao = this._uASampleDao;
        }
        return uASampleDao;
    }
}
