package com.portfolio.platform.data.source;

import com.fossil.wg6;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppLastSettingRepository {
    @DexIgnore
    public /* final */ MicroAppLastSettingDao mMicroAppLastSettingDao;

    @DexIgnore
    public MicroAppLastSettingRepository(MicroAppLastSettingDao microAppLastSettingDao) {
        wg6.b(microAppLastSettingDao, "mMicroAppLastSettingDao");
        this.mMicroAppLastSettingDao = microAppLastSettingDao;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mMicroAppLastSettingDao.cleanUp();
    }

    @DexIgnore
    public final MicroAppLastSetting getMicroAppLastSetting(String str) {
        wg6.b(str, "id");
        return this.mMicroAppLastSettingDao.getMicroAppLastSetting(str);
    }

    @DexIgnore
    public final void upsertMicroAppLastSetting(MicroAppLastSetting microAppLastSetting) {
        wg6.b(microAppLastSetting, "MicroAppLastSetting");
        this.mMicroAppLastSettingDao.upsertMicroAppLastSetting(microAppLastSetting);
    }

    @DexIgnore
    public final void upsertMicroAppLastSettingList(List<MicroAppLastSetting> list) {
        wg6.b(list, "microAppLastSettingList");
        this.mMicroAppLastSettingDao.upsertMicroAppLastSettingList(list);
    }
}
