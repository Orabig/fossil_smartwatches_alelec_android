package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppRemoteDataSource_Factory implements Factory<MicroAppRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public MicroAppRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static MicroAppRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new MicroAppRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static MicroAppRemoteDataSource newMicroAppRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new MicroAppRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public static MicroAppRemoteDataSource provideInstance(Provider<ApiServiceV2> provider) {
        return new MicroAppRemoteDataSource(provider.get());
    }

    @DexIgnore
    public MicroAppRemoteDataSource get() {
        return provideInstance(this.mApiServiceV2Provider);
    }
}
