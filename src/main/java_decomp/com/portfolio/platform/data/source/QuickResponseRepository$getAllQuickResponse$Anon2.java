package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.QuickResponseRepository$getAllQuickResponse$2", f = "QuickResponseRepository.kt", l = {33}, m = "invokeSuspend")
public final class QuickResponseRepository$getAllQuickResponse$Anon2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $responses;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ QuickResponseRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public QuickResponseRepository$getAllQuickResponse$Anon2(QuickResponseRepository quickResponseRepository, List list, xe6 xe6) {
        super(2, xe6);
        this.this$0 = quickResponseRepository;
        this.$responses = list;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        QuickResponseRepository$getAllQuickResponse$Anon2 quickResponseRepository$getAllQuickResponse$Anon2 = new QuickResponseRepository$getAllQuickResponse$Anon2(this.this$0, this.$responses, xe6);
        quickResponseRepository$getAllQuickResponse$Anon2.p$ = (il6) obj;
        return quickResponseRepository$getAllQuickResponse$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((QuickResponseRepository$getAllQuickResponse$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            QuickResponseRepository quickResponseRepository = this.this$0;
            List list = this.$responses;
            this.L$0 = il6;
            this.label = 1;
            if (quickResponseRepository.insertQRs(list, this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
