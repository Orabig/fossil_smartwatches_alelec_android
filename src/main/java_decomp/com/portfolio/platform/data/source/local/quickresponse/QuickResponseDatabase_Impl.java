package com.portfolio.platform.data.source.local.quickresponse;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseDatabase_Impl extends QuickResponseDatabase {
    @DexIgnore
    public volatile QuickResponseMessageDao _quickResponseMessageDao;
    @DexIgnore
    public volatile QuickResponseSenderDao _quickResponseSenderDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `quickResponseMessage` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `response` TEXT NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS `quickResponseSender` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `content` TEXT NOT NULL)");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'e3a41f0570eccd0330bf7f76279c817f')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `quickResponseMessage`");
            iiVar.b("DROP TABLE IF EXISTS `quickResponseSender`");
            if (QuickResponseDatabase_Impl.this.mCallbacks != null) {
                int size = QuickResponseDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) QuickResponseDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (QuickResponseDatabase_Impl.this.mCallbacks != null) {
                int size = QuickResponseDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) QuickResponseDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = QuickResponseDatabase_Impl.this.mDatabase = iiVar;
            QuickResponseDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (QuickResponseDatabase_Impl.this.mCallbacks != null) {
                int size = QuickResponseDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) QuickResponseDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            ii iiVar2 = iiVar;
            HashMap hashMap = new HashMap(2);
            hashMap.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap.put("response", new fi.a("response", "TEXT", true, 0, (String) null, 1));
            fi fiVar = new fi("quickResponseMessage", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar2, "quickResponseMessage");
            if (!fiVar.equals(a)) {
                return new qh.b(false, "quickResponseMessage(com.portfolio.platform.data.model.QuickResponseMessage).\n Expected:\n" + fiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap2.put("content", new fi.a("content", "TEXT", true, 0, (String) null, 1));
            fi fiVar2 = new fi("quickResponseSender", hashMap2, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar2, "quickResponseSender");
            if (fiVar2.equals(a2)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "quickResponseSender(com.portfolio.platform.data.model.QuickResponseSender).\n Expected:\n" + fiVar2 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        QuickResponseDatabase_Impl.super.assertNotMainThread();
        ii a = QuickResponseDatabase_Impl.super.getOpenHelper().a();
        try {
            QuickResponseDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `quickResponseMessage`");
            a.b("DELETE FROM `quickResponseSender`");
            QuickResponseDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            QuickResponseDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"quickResponseMessage", "quickResponseSender"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "e3a41f0570eccd0330bf7f76279c817f", "eb9908832be312d2d3b3e7c35c7e6720");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public QuickResponseMessageDao quickResponseMessageDao() {
        QuickResponseMessageDao quickResponseMessageDao;
        if (this._quickResponseMessageDao != null) {
            return this._quickResponseMessageDao;
        }
        synchronized (this) {
            if (this._quickResponseMessageDao == null) {
                this._quickResponseMessageDao = new QuickResponseMessageDao_Impl(this);
            }
            quickResponseMessageDao = this._quickResponseMessageDao;
        }
        return quickResponseMessageDao;
    }

    @DexIgnore
    public QuickResponseSenderDao quickResponseSenderDao() {
        QuickResponseSenderDao quickResponseSenderDao;
        if (this._quickResponseSenderDao != null) {
            return this._quickResponseSenderDao;
        }
        synchronized (this) {
            if (this._quickResponseSenderDao == null) {
                this._quickResponseSenderDao = new QuickResponseSenderDao_Impl(this);
            }
            quickResponseSenderDao = this._quickResponseSenderDao;
        }
        return quickResponseSenderDao;
    }
}
