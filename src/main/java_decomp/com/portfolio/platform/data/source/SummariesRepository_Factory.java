package com.portfolio.platform.data.source;

import com.fossil.ik4;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository_Factory implements Factory<SummariesRepository> {
    @DexIgnore
    public /* final */ Provider<ActivitySummaryDao> mActivitySummaryDaoProvider;
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;
    @DexIgnore
    public /* final */ Provider<FitnessDataDao> mFitnessDataDaoProvider;
    @DexIgnore
    public /* final */ Provider<FitnessDatabase> mFitnessDatabaseProvider;
    @DexIgnore
    public /* final */ Provider<ik4> mFitnessHelperProvider;

    @DexIgnore
    public SummariesRepository_Factory(Provider<ApiServiceV2> provider, Provider<ActivitySummaryDao> provider2, Provider<FitnessDataDao> provider3, Provider<FitnessDatabase> provider4, Provider<ik4> provider5) {
        this.mApiServiceV2Provider = provider;
        this.mActivitySummaryDaoProvider = provider2;
        this.mFitnessDataDaoProvider = provider3;
        this.mFitnessDatabaseProvider = provider4;
        this.mFitnessHelperProvider = provider5;
    }

    @DexIgnore
    public static SummariesRepository_Factory create(Provider<ApiServiceV2> provider, Provider<ActivitySummaryDao> provider2, Provider<FitnessDataDao> provider3, Provider<FitnessDatabase> provider4, Provider<ik4> provider5) {
        return new SummariesRepository_Factory(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static SummariesRepository newSummariesRepository(ApiServiceV2 apiServiceV2, ActivitySummaryDao activitySummaryDao, FitnessDataDao fitnessDataDao, FitnessDatabase fitnessDatabase, ik4 ik4) {
        return new SummariesRepository(apiServiceV2, activitySummaryDao, fitnessDataDao, fitnessDatabase, ik4);
    }

    @DexIgnore
    public static SummariesRepository provideInstance(Provider<ApiServiceV2> provider, Provider<ActivitySummaryDao> provider2, Provider<FitnessDataDao> provider3, Provider<FitnessDatabase> provider4, Provider<ik4> provider5) {
        return new SummariesRepository(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get());
    }

    @DexIgnore
    public SummariesRepository get() {
        return provideInstance(this.mApiServiceV2Provider, this.mActivitySummaryDaoProvider, this.mFitnessDataDaoProvider, this.mFitnessDatabaseProvider, this.mFitnessHelperProvider);
    }
}
