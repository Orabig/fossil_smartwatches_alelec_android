package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.fossil.x34;
import com.fossil.xe;
import com.fossil.yh;
import com.fossil.z34;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDao_Impl extends GoalTrackingDao {
    @DexIgnore
    public /* final */ x34 __dateShortStringConverter; // = new x34();
    @DexIgnore
    public /* final */ z34 __dateTimeISOStringConverter; // = new z34();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<GoalSetting> __insertionAdapterOfGoalSetting;
    @DexIgnore
    public /* final */ hh<GoalTrackingData> __insertionAdapterOfGoalTrackingData;
    @DexIgnore
    public /* final */ hh<GoalTrackingSummary> __insertionAdapterOfGoalTrackingSummary;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllGoalTrackingData;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllGoalTrackingSummaries;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteGoalSetting;
    @DexIgnore
    public /* final */ vh __preparedStmtOfRemoveDeletedGoalTrackingData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<GoalSetting> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalSetting` (`id`,`currentTarget`) VALUES (?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, GoalSetting goalSetting) {
            miVar.a(1, (long) goalSetting.getId());
            miVar.a(2, (long) goalSetting.getCurrentTarget());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon10(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor a = bi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "pinType");
                int b2 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b3 = ai.b(a, "totalTracked");
                int b4 = ai.b(a, "goalTarget");
                int b5 = ai.b(a, "createdAt");
                int b6 = ai.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getInt(b3), a.getInt(b4), a.getLong(b5), a.getLong(b6));
                    goalTrackingSummary.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements Callable<GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon11(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public GoalTrackingSummary call() throws Exception {
            GoalTrackingSummary goalTrackingSummary = null;
            Cursor a = bi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "pinType");
                int b2 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b3 = ai.b(a, "totalTracked");
                int b4 = ai.b(a, "goalTarget");
                int b5 = ai.b(a, "createdAt");
                int b6 = ai.b(a, "updatedAt");
                if (a.moveToFirst()) {
                    goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getInt(b3), a.getInt(b4), a.getLong(b5), a.getLong(b6));
                    goalTrackingSummary.setPinType(a.getInt(b));
                }
                return goalTrackingSummary;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon12(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor a = bi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "pinType");
                int b2 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b3 = ai.b(a, "totalTracked");
                int b4 = ai.b(a, "goalTarget");
                int b5 = ai.b(a, "createdAt");
                int b6 = ai.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b2)), a.getInt(b3), a.getInt(b4), a.getLong(b5), a.getLong(b6));
                    goalTrackingSummary.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon13(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<GoalTrackingData> call() throws Exception {
            Cursor a = bi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "pinType");
                int b2 = ai.b(a, "id");
                int b3 = ai.b(a, "trackedAt");
                int b4 = ai.b(a, "timezoneOffsetInSecond");
                int b5 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b6 = ai.b(a, "createdAt");
                int b7 = ai.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(a.getString(b2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b3)), a.getInt(b4), GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b5)), a.getLong(b6), a.getLong(b7));
                    goalTrackingData.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon14(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<GoalTrackingData> call() throws Exception {
            Cursor a = bi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "pinType");
                int b2 = ai.b(a, "id");
                int b3 = ai.b(a, "trackedAt");
                int b4 = ai.b(a, "timezoneOffsetInSecond");
                int b5 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b6 = ai.b(a, "createdAt");
                int b7 = ai.b(a, "updatedAt");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(a.getString(b2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b3)), a.getInt(b4), GoalTrackingDao_Impl.this.__dateShortStringConverter.a(a.getString(b5)), a.getLong(b6), a.getLong(b7));
                    goalTrackingData.setPinType(a.getInt(b));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends hh<GoalTrackingSummary> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingDay` (`pinType`,`date`,`totalTracked`,`goalTarget`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, GoalTrackingSummary goalTrackingSummary) {
            miVar.a(1, (long) goalTrackingSummary.getPinType());
            String a = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingSummary.getDate());
            if (a == null) {
                miVar.a(2);
            } else {
                miVar.a(2, a);
            }
            miVar.a(3, (long) goalTrackingSummary.getTotalTracked());
            miVar.a(4, (long) goalTrackingSummary.getGoalTarget());
            miVar.a(5, goalTrackingSummary.getCreatedAt());
            miVar.a(6, goalTrackingSummary.getUpdatedAt());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends hh<GoalTrackingData> {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingRaw` (`pinType`,`id`,`trackedAt`,`timezoneOffsetInSecond`,`date`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, GoalTrackingData goalTrackingData) {
            miVar.a(1, (long) goalTrackingData.getPinType());
            if (goalTrackingData.getId() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, goalTrackingData.getId());
            }
            String a = GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(goalTrackingData.getTrackedAt());
            if (a == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a);
            }
            miVar.a(4, (long) goalTrackingData.getTimezoneOffsetInSecond());
            String a2 = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingData.getDate());
            if (a2 == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a2);
            }
            miVar.a(6, goalTrackingData.getCreatedAt());
            miVar.a(7, goalTrackingData.getUpdatedAt());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends vh {
        @DexIgnore
        public Anon4(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM goalSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends vh {
        @DexIgnore
        public Anon5(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM goalTrackingDay";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends vh {
        @DexIgnore
        public Anon6(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw WHERE  id == ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends vh {
        @DexIgnore
        public Anon7(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon8(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public Integer call() throws Exception {
            Integer num = null;
            Cursor a = bi.a(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                if (a.moveToFirst()) {
                    if (!a.isNull(0)) {
                        num = Integer.valueOf(a.getInt(0));
                    }
                }
                return num;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends xe.b<Integer, GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 extends yh<GoalTrackingSummary> {
            @DexIgnore
            public Anon1_Level2(oh ohVar, rh rhVar, boolean z, String... strArr) {
                super(ohVar, rhVar, z, strArr);
            }

            @DexIgnore
            public List<GoalTrackingSummary> convertRows(Cursor cursor) {
                Cursor cursor2 = cursor;
                int b = ai.b(cursor2, "pinType");
                int b2 = ai.b(cursor2, HardwareLog.COLUMN_DATE);
                int b3 = ai.b(cursor2, "totalTracked");
                int b4 = ai.b(cursor2, "goalTarget");
                int b5 = ai.b(cursor2, "createdAt");
                int b6 = ai.b(cursor2, "updatedAt");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.a(cursor2.getString(b2)), cursor2.getInt(b3), cursor2.getInt(b4), cursor2.getLong(b5), cursor2.getLong(b6));
                    goalTrackingSummary.setPinType(cursor2.getInt(b));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon9(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public yh<GoalTrackingSummary> create() {
            return new Anon1_Level2(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, "goalTrackingDay");
        }
    }

    @DexIgnore
    public GoalTrackingDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfGoalSetting = new Anon1(ohVar);
        this.__insertionAdapterOfGoalTrackingSummary = new Anon2(ohVar);
        this.__insertionAdapterOfGoalTrackingData = new Anon3(ohVar);
        this.__preparedStmtOfDeleteGoalSetting = new Anon4(ohVar);
        this.__preparedStmtOfDeleteAllGoalTrackingSummaries = new Anon5(ohVar);
        this.__preparedStmtOfRemoveDeletedGoalTrackingData = new Anon6(ohVar);
        this.__preparedStmtOfDeleteAllGoalTrackingData = new Anon7(ohVar);
    }

    @DexIgnore
    public void deleteAllGoalTrackingData() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllGoalTrackingData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    public void deleteAllGoalTrackingSummaries() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllGoalTrackingSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingSummaries.release(acquire);
        }
    }

    @DexIgnore
    public void deleteGoalSetting() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteGoalSetting.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteGoalSetting.release(acquire);
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getGoalTrackingDataInDate(Date date) {
        rh b = rh.b("SELECT * FROM goalTrackingRaw WHERE date== ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "pinType");
            int b3 = ai.b(a2, "id");
            int b4 = ai.b(a2, "trackedAt");
            int b5 = ai.b(a2, "timezoneOffsetInSecond");
            int b6 = ai.b(a2, HardwareLog.COLUMN_DATE);
            int b7 = ai.b(a2, "createdAt");
            int b8 = ai.b(a2, "updatedAt");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a2.getString(b3), this.__dateTimeISOStringConverter.a(a2.getString(b4)), a2.getInt(b5), this.__dateShortStringConverter.a(a2.getString(b6)), a2.getLong(b7), a2.getLong(b8));
                goalTrackingData.setPinType(a2.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getGoalTrackingDataList(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a3, "pinType");
            int b3 = ai.b(a3, "id");
            int b4 = ai.b(a3, "trackedAt");
            int b5 = ai.b(a3, "timezoneOffsetInSecond");
            int b6 = ai.b(a3, HardwareLog.COLUMN_DATE);
            int b7 = ai.b(a3, "createdAt");
            int b8 = ai.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a3.getString(b3), this.__dateTimeISOStringConverter.a(a3.getString(b4)), a3.getInt(b5), this.__dateShortStringConverter.a(a3.getString(b6)), a3.getLong(b7), a3.getLong(b8));
                goalTrackingData.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getGoalTrackingDataListAfterInDate(Date date, DateTime dateTime, long j, int i) {
        rh b = rh.b("SELECT * FROM goalTrackingRaw WHERE date == ? AND updatedAt < ? AND trackedAt < ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 4);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        b.a(2, j);
        String a2 = this.__dateTimeISOStringConverter.a(dateTime);
        if (a2 == null) {
            b.a(3);
        } else {
            b.a(3, a2);
        }
        b.a(4, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a3, "pinType");
            int b3 = ai.b(a3, "id");
            int b4 = ai.b(a3, "trackedAt");
            int b5 = ai.b(a3, "timezoneOffsetInSecond");
            int b6 = ai.b(a3, HardwareLog.COLUMN_DATE);
            int b7 = ai.b(a3, "createdAt");
            int b8 = ai.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a3.getString(b3), this.__dateTimeISOStringConverter.a(a3.getString(b4)), a3.getInt(b5), this.__dateShortStringConverter.a(a3.getString(b6)), a3.getLong(b7), a3.getLong(b8));
                goalTrackingData.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getGoalTrackingDataListInitInDate(Date date, int i) {
        rh b = rh.b("SELECT * FROM goalTrackingRaw WHERE date == ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        b.a(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "pinType");
            int b3 = ai.b(a2, "id");
            int b4 = ai.b(a2, "trackedAt");
            int b5 = ai.b(a2, "timezoneOffsetInSecond");
            int b6 = ai.b(a2, HardwareLog.COLUMN_DATE);
            int b7 = ai.b(a2, "createdAt");
            int b8 = ai.b(a2, "updatedAt");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a2.getString(b3), this.__dateTimeISOStringConverter.a(a2.getString(b4)), a2.getInt(b5), this.__dateShortStringConverter.a(a2.getString(b6)), a2.getLong(b7), a2.getLong(b8));
                goalTrackingData.setPinType(a2.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<GoalTrackingData>> getGoalTrackingDataListLiveData(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingRaw"}, false, new Anon13(b));
    }

    @DexIgnore
    public List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a3, "pinType");
            int b3 = ai.b(a3, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a3, "totalTracked");
            int b5 = ai.b(a3, "goalTarget");
            int b6 = ai.b(a3, "createdAt");
            int b7 = ai.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a3.getString(b3)), a3.getInt(b4), a3.getInt(b5), a3.getLong(b6), a3.getLong(b7));
                goalTrackingSummary.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummariesLiveData(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingDay"}, false, new Anon12(b));
    }

    @DexIgnore
    public GoalTrackingSummary getGoalTrackingSummary(Date date) {
        rh b = rh.b("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        GoalTrackingSummary goalTrackingSummary = null;
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "pinType");
            int b3 = ai.b(a2, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a2, "totalTracked");
            int b5 = ai.b(a2, "goalTarget");
            int b6 = ai.b(a2, "createdAt");
            int b7 = ai.b(a2, "updatedAt");
            if (a2.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a2.getString(b3)), a2.getInt(b4), a2.getInt(b5), a2.getLong(b6), a2.getLong(b7));
                goalTrackingSummary.setPinType(a2.getInt(b2));
            }
            return goalTrackingSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public List<GoalTrackingSummary> getGoalTrackingSummaryList() {
        rh b = rh.b("SELECT * FROM goalTrackingDay", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a, "totalTracked");
            int b5 = ai.b(a, "goalTarget");
            int b6 = ai.b(a, "createdAt");
            int b7 = ai.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a.getString(b3)), a.getInt(b4), a.getInt(b5), a.getLong(b6), a.getLong(b7));
                goalTrackingSummary.setPinType(a.getInt(b2));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummaryListLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingDay"}, false, new Anon10(rh.b("SELECT * FROM goalTrackingDay", 0)));
    }

    @DexIgnore
    public LiveData<GoalTrackingSummary> getGoalTrackingSummaryLiveData(Date date) {
        rh b = rh.b("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingDay"}, false, new Anon11(b));
    }

    @DexIgnore
    public GoalTrackingDao.TotalSummary getGoalTrackingValueAndTarget(Date date, Date date2) {
        rh b = rh.b("SELECT SUM(totalTracked) as total_values, SUM(goalTarget) as total_targets FROM goalTrackingDay\n        WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        GoalTrackingDao.TotalSummary totalSummary = null;
        Cursor a3 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a3, "total_values");
            int b3 = ai.b(a3, "total_targets");
            if (a3.moveToFirst()) {
                totalSummary = new GoalTrackingDao.TotalSummary();
                totalSummary.setValues(a3.getInt(b2));
                totalSummary.setTargets(a3.getInt(b3));
            }
            return totalSummary;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public Integer getLastGoalSetting() {
        rh b = rh.b("SELECT currentTarget FROM goalSetting LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Integer num = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            if (a.moveToFirst()) {
                if (!a.isNull(0)) {
                    num = Integer.valueOf(a.getInt(0));
                }
            }
            return num;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<Integer> getLastGoalSettingLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"goalSetting"}, false, new Anon8(rh.b("SELECT currentTarget FROM goalSetting LIMIT 1", 0)));
    }

    @DexIgnore
    public GoalTrackingSummary getNearestGoalTrackingFromDate(Date date) {
        rh b = rh.b("SELECT * FROM goalTrackingDay WHERE date <= ? ORDER BY date DESC LIMIT 1", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        GoalTrackingSummary goalTrackingSummary = null;
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "pinType");
            int b3 = ai.b(a2, HardwareLog.COLUMN_DATE);
            int b4 = ai.b(a2, "totalTracked");
            int b5 = ai.b(a2, "goalTarget");
            int b6 = ai.b(a2, "createdAt");
            int b7 = ai.b(a2, "updatedAt");
            if (a2.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.a(a2.getString(b3)), a2.getInt(b4), a2.getInt(b5), a2.getLong(b6), a2.getLong(b7));
                goalTrackingSummary.setPinType(a2.getInt(b2));
            }
            return goalTrackingSummary;
        } finally {
            a2.close();
            b.c();
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getPendingGoalTrackingDataList(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a3 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a3, "pinType");
            int b3 = ai.b(a3, "id");
            int b4 = ai.b(a3, "trackedAt");
            int b5 = ai.b(a3, "timezoneOffsetInSecond");
            int b6 = ai.b(a3, HardwareLog.COLUMN_DATE);
            int b7 = ai.b(a3, "createdAt");
            int b8 = ai.b(a3, "updatedAt");
            ArrayList arrayList = new ArrayList(a3.getCount());
            while (a3.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a3.getString(b3), this.__dateTimeISOStringConverter.a(a3.getString(b4)), a3.getInt(b5), this.__dateShortStringConverter.a(a3.getString(b6)), a3.getLong(b7), a3.getLong(b8));
                goalTrackingData.setPinType(a3.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a3.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<GoalTrackingData>> getPendingGoalTrackingDataListLiveData(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"goalTrackingRaw"}, false, new Anon14(b));
    }

    @DexIgnore
    public xe.b<Integer, GoalTrackingSummary> getSummariesDataSource() {
        return new Anon9(rh.b("SELECT * FROM goalTrackingDay ORDER BY date DESC", 0));
    }

    @DexIgnore
    public void removeDeletedGoalTrackingData(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfRemoveDeletedGoalTrackingData.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeletedGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    public void upsertGoalSettings(GoalSetting goalSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalSetting.insert(goalSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertGoalTrackingData(GoalTrackingData goalTrackingData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(goalTrackingData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertGoalTrackingDataList(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertGoalTrackingSummaries(List<GoalTrackingSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertGoalTrackingSummary(GoalTrackingSummary goalTrackingSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert(goalTrackingSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertListGoalTrackingData(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GoalTrackingData> getPendingGoalTrackingDataList() {
        rh b = rh.b("SELECT * FROM goalTrackingRaw WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "id");
            int b4 = ai.b(a, "trackedAt");
            int b5 = ai.b(a, "timezoneOffsetInSecond");
            int b6 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b7 = ai.b(a, "createdAt");
            int b8 = ai.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(a.getString(b3), this.__dateTimeISOStringConverter.a(a.getString(b4)), a.getInt(b5), this.__dateShortStringConverter.a(a.getString(b6)), a.getLong(b7), a.getLong(b8));
                goalTrackingData.setPinType(a.getInt(b2));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }
}
