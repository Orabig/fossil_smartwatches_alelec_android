package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitHeartRateDao_Impl implements GFitHeartRateDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<GFitHeartRate> __deletionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ hh<GFitHeartRate> __insertionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<GFitHeartRate> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitHeartRate` (`id`,`value`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, GFitHeartRate gFitHeartRate) {
            miVar.a(1, (long) gFitHeartRate.getId());
            miVar.a(2, (double) gFitHeartRate.getValue());
            miVar.a(3, gFitHeartRate.getStartTime());
            miVar.a(4, gFitHeartRate.getEndTime());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<GFitHeartRate> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitHeartRate` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, GFitHeartRate gFitHeartRate) {
            miVar.a(1, (long) gFitHeartRate.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM gFitHeartRate";
        }
    }

    @DexIgnore
    public GFitHeartRateDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfGFitHeartRate = new Anon1(ohVar);
        this.__deletionAdapterOfGFitHeartRate = new Anon2(ohVar);
        this.__preparedStmtOfClearAll = new Anon3(ohVar);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitHeartRate.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitHeartRate> getAllGFitHeartRate() {
        rh b = rh.b("SELECT * FROM gFitHeartRate", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, ServerSetting.VALUE);
            int b4 = ai.b(a, "startTime");
            int b5 = ai.b(a, "endTime");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitHeartRate gFitHeartRate = new GFitHeartRate(a.getFloat(b3), a.getLong(b4), a.getLong(b5));
                gFitHeartRate.setId(a.getInt(b2));
                arrayList.add(gFitHeartRate);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitHeartRate(GFitHeartRate gFitHeartRate) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert(gFitHeartRate);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
