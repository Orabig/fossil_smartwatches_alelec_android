package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getActivitySettings$Anon1 extends tx5<ActivitySettings, ActivitySettings> {
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    public SummariesRepository$getActivitySettings$Anon1(SummariesRepository summariesRepository) {
        this.this$0 = summariesRepository;
    }

    @DexIgnore
    public Object createCall(xe6<? super rx6<ActivitySettings>> xe6) {
        return this.this$0.mApiServiceV2.getActivitySetting(xe6);
    }

    @DexIgnore
    public LiveData<ActivitySettings> loadFromDb() {
        return this.this$0.mActivitySummaryDao.getActivitySettingLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getActivitySettings - onFetchFailed");
    }

    @DexIgnore
    public boolean shouldFetch(ActivitySettings activitySettings) {
        return true;
    }

    @DexIgnore
    public void saveCallResult(ActivitySettings activitySettings) {
        wg6.b(activitySettings, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getActivitySettings - saveCallResult -- item=" + activitySettings);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new SummariesRepository$getActivitySettings$Anon1$saveCallResult$Anon1_Level2(this, activitySettings, (xe6) null), 3, (Object) null);
    }
}
