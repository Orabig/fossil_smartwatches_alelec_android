package com.portfolio.platform.data.source;

import com.fossil.wg6;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationLastSettingRepository {
    @DexIgnore
    public /* final */ ComplicationLastSettingDao mComplicationLastSettingDao;

    @DexIgnore
    public ComplicationLastSettingRepository(ComplicationLastSettingDao complicationLastSettingDao) {
        wg6.b(complicationLastSettingDao, "mComplicationLastSettingDao");
        this.mComplicationLastSettingDao = complicationLastSettingDao;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mComplicationLastSettingDao.cleanUp();
    }

    @DexIgnore
    public final ComplicationLastSetting getComplicationLastSetting(String str) {
        wg6.b(str, "id");
        return this.mComplicationLastSettingDao.getComplicationLastSetting(str);
    }

    @DexIgnore
    public final void upsertComplicationLastSetting(ComplicationLastSetting complicationLastSetting) {
        wg6.b(complicationLastSetting, "ComplicationLastSetting");
        this.mComplicationLastSettingDao.upsertComplicationLastSetting(complicationLastSetting);
    }
}
