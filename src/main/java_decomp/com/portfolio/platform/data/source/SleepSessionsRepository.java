package com.portfolio.platform.data.source;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.du3;
import com.fossil.ff6;
import com.fossil.fu3;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.fossil.zl6;
import com.fossil.zm4;
import com.fossil.zo4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.SleepSession;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import com.portfolio.platform.helper.GsonISOConvertDateTime;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.response.sleep.SleepSessionParse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionsRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ FitnessDataDao mFitnessDataDao;
    @DexIgnore
    public /* final */ SleepDao mSleepDao;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return SleepSessionsRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingSleepSessionsCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<MFSleepSession> list);
    }

    /*
    static {
        String simpleName = SleepSessionsRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "SleepSessionsRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public SleepSessionsRepository(SleepDao sleepDao, ApiServiceV2 apiServiceV2, SleepDatabase sleepDatabase, FitnessDataDao fitnessDataDao) {
        wg6.b(sleepDao, "mSleepDao");
        wg6.b(apiServiceV2, "mApiService");
        wg6.b(sleepDatabase, "mSleepDatabase");
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        this.mSleepDao = sleepDao;
        this.mApiService = apiServiceV2;
        this.mSleepDatabase = sleepDatabase;
        this.mFitnessDataDao = fitnessDataDao;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchSleepSessions$default(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, int i, int i2, xe6 xe6, int i3, Object obj) {
        return sleepSessionsRepository.fetchSleepSessions(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, xe6);
    }

    @DexIgnore
    private final boolean isExistsSleepSession(MFSleepSession mFSleepSession) {
        List<MFSleepSession> sleepSessions = this.mSleepDao.getSleepSessions(mFSleepSession.getDay().getTime());
        long startTime = (long) mFSleepSession.getStartTime();
        long endTime = (long) mFSleepSession.getEndTime();
        for (MFSleepSession mFSleepSession2 : sleepSessions) {
            long startTime2 = (long) mFSleepSession2.getStartTime();
            long endTime2 = (long) mFSleepSession2.getEndTime();
            if (startTime2 <= startTime && endTime2 >= startTime) {
                return true;
            }
            if (startTime2 <= endTime && endTime2 >= endTime) {
                return true;
            }
            if (startTime <= startTime2 && endTime2 <= endTime) {
                return true;
            }
            if (startTime <= endTime2 && endTime >= endTime2) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    private final rm6 saveSleepSessionsToServer(String str, List<MFSleepSession> list, PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback) {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new SleepSessionsRepository$saveSleepSessionsToServer$Anon1(this, list, str, pushPendingSleepSessionsCallback, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        this.mSleepDao.deleteAllSleepSessions();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final Object downloadRecommendedGoals(int i, int i2, int i3, String str, xe6<? super ap4<SleepRecommendedGoal>> xe6) {
        SleepSessionsRepository$downloadRecommendedGoals$Anon1 sleepSessionsRepository$downloadRecommendedGoals$Anon1;
        int i4;
        ap4 ap4;
        xe6<? super ap4<SleepRecommendedGoal>> xe62 = xe6;
        if (xe62 instanceof SleepSessionsRepository$downloadRecommendedGoals$Anon1) {
            sleepSessionsRepository$downloadRecommendedGoals$Anon1 = (SleepSessionsRepository$downloadRecommendedGoals$Anon1) xe62;
            int i5 = sleepSessionsRepository$downloadRecommendedGoals$Anon1.label;
            if ((i5 & Integer.MIN_VALUE) != 0) {
                sleepSessionsRepository$downloadRecommendedGoals$Anon1.label = i5 - Integer.MIN_VALUE;
                SleepSessionsRepository$downloadRecommendedGoals$Anon1 sleepSessionsRepository$downloadRecommendedGoals$Anon12 = sleepSessionsRepository$downloadRecommendedGoals$Anon1;
                Object obj = sleepSessionsRepository$downloadRecommendedGoals$Anon12.result;
                Object a = ff6.a();
                i4 = sleepSessionsRepository$downloadRecommendedGoals$Anon12.label;
                if (i4 != 0) {
                    nc6.a(obj);
                    SleepSessionsRepository$downloadRecommendedGoals$response$Anon1 sleepSessionsRepository$downloadRecommendedGoals$response$Anon1 = new SleepSessionsRepository$downloadRecommendedGoals$response$Anon1(this, i, i2, i3, str, (xe6) null);
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.L$0 = this;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$0 = i;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$1 = i2;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$2 = i3;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.L$1 = str;
                    sleepSessionsRepository$downloadRecommendedGoals$Anon12.label = 1;
                    obj = ResponseKt.a(sleepSessionsRepository$downloadRecommendedGoals$response$Anon1, sleepSessionsRepository$downloadRecommendedGoals$Anon12);
                    if (obj == a) {
                        return a;
                    }
                } else if (i4 == 1) {
                    String str2 = (String) sleepSessionsRepository$downloadRecommendedGoals$Anon12.L$1;
                    int i6 = sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$2;
                    int i7 = sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$1;
                    int i8 = sleepSessionsRepository$downloadRecommendedGoals$Anon12.I$0;
                    SleepSessionsRepository sleepSessionsRepository = (SleepSessionsRepository) sleepSessionsRepository$downloadRecommendedGoals$Anon12.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    Object a2 = ((cp4) ap4).a();
                    if (a2 != null) {
                        return new cp4((SleepRecommendedGoal) a2, false, 2, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null);
                } else {
                    throw new kc6();
                }
            }
        }
        sleepSessionsRepository$downloadRecommendedGoals$Anon1 = new SleepSessionsRepository$downloadRecommendedGoals$Anon1(this, xe62);
        SleepSessionsRepository$downloadRecommendedGoals$Anon1 sleepSessionsRepository$downloadRecommendedGoals$Anon122 = sleepSessionsRepository$downloadRecommendedGoals$Anon1;
        Object obj2 = sleepSessionsRepository$downloadRecommendedGoals$Anon122.result;
        Object a3 = ff6.a();
        i4 = sleepSessionsRepository$downloadRecommendedGoals$Anon122.label;
        if (i4 != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002d  */
    public final Object fetchSleepSessions(Date date, Date date2, int i, int i2, xe6<? super ap4<ku3>> xe6) {
        SleepSessionsRepository$fetchSleepSessions$Anon1 sleepSessionsRepository$fetchSleepSessions$Anon1;
        int i3;
        ap4 ap4;
        ap4 ap42;
        Object obj;
        int i4;
        Date date3;
        SleepSessionsRepository sleepSessionsRepository;
        int i5;
        String message;
        List<SleepSessionParse> list;
        Date date4 = date;
        Date date5 = date2;
        xe6<? super ap4<ku3>> xe62 = xe6;
        if (xe62 instanceof SleepSessionsRepository$fetchSleepSessions$Anon1) {
            sleepSessionsRepository$fetchSleepSessions$Anon1 = (SleepSessionsRepository$fetchSleepSessions$Anon1) xe62;
            int i6 = sleepSessionsRepository$fetchSleepSessions$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                sleepSessionsRepository$fetchSleepSessions$Anon1.label = i6 - Integer.MIN_VALUE;
                SleepSessionsRepository$fetchSleepSessions$Anon1 sleepSessionsRepository$fetchSleepSessions$Anon12 = sleepSessionsRepository$fetchSleepSessions$Anon1;
                Object obj2 = sleepSessionsRepository$fetchSleepSessions$Anon12.result;
                Object a = ff6.a();
                i3 = sleepSessionsRepository$fetchSleepSessions$Anon12.label;
                if (i3 != 0) {
                    nc6.a(obj2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "fetchSleepSessions: start = " + date4 + ", end = " + date5);
                    SleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1 sleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1 = new SleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1(this, date, date2, i, i2, (xe6) null);
                    sleepSessionsRepository$fetchSleepSessions$Anon12.L$0 = this;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.L$1 = date4;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.L$2 = date5;
                    i5 = i;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.I$0 = i5;
                    int i7 = i2;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.I$1 = i7;
                    sleepSessionsRepository$fetchSleepSessions$Anon12.label = 1;
                    Object a2 = ResponseKt.a(sleepSessionsRepository$fetchSleepSessions$repoResponse$Anon1, sleepSessionsRepository$fetchSleepSessions$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj2 = a2;
                    date3 = date5;
                    sleepSessionsRepository = this;
                } else if (i3 == 1) {
                    int i8 = sleepSessionsRepository$fetchSleepSessions$Anon12.I$1;
                    i5 = sleepSessionsRepository$fetchSleepSessions$Anon12.I$0;
                    nc6.a(obj2);
                    i4 = i8;
                    date3 = (Date) sleepSessionsRepository$fetchSleepSessions$Anon12.L$2;
                    date4 = (Date) sleepSessionsRepository$fetchSleepSessions$Anon12.L$1;
                    sleepSessionsRepository = (SleepSessionsRepository) sleepSessionsRepository$fetchSleepSessions$Anon12.L$0;
                } else if (i3 == 2) {
                    ApiResponse apiResponse = (ApiResponse) sleepSessionsRepository$fetchSleepSessions$Anon12.L$5;
                    ArrayList arrayList = (ArrayList) sleepSessionsRepository$fetchSleepSessions$Anon12.L$4;
                    ap42 = (ap4) sleepSessionsRepository$fetchSleepSessions$Anon12.L$3;
                    int i9 = sleepSessionsRepository$fetchSleepSessions$Anon12.I$1;
                    int i10 = sleepSessionsRepository$fetchSleepSessions$Anon12.I$0;
                    Date date6 = (Date) sleepSessionsRepository$fetchSleepSessions$Anon12.L$2;
                    Date date7 = (Date) sleepSessionsRepository$fetchSleepSessions$Anon12.L$1;
                    SleepSessionsRepository sleepSessionsRepository2 = (SleepSessionsRepository) sleepSessionsRepository$fetchSleepSessions$Anon12.L$0;
                    try {
                        nc6.a(obj2);
                        obj = obj2;
                        return (ap4) obj;
                    } catch (Exception e) {
                        e = e;
                        ap4 = ap42;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj2;
                String str2 = null;
                if (!(ap4 instanceof cp4)) {
                    if (((cp4) ap4).a() != null) {
                        ArrayList arrayList2 = new ArrayList();
                        try {
                            du3 du3 = new du3();
                            du3.a(DateTime.class, new GsonConvertDateTime());
                            du3.a(Date.class, new GsonConverterShortDate());
                            ApiResponse apiResponse2 = (ApiResponse) du3.a().a(((ku3) ((cp4) ap4).a()).toString(), new SleepSessionsRepository$fetchSleepSessions$responseDate$Anon1().getType());
                            if (!(apiResponse2 == null || (list = apiResponse2.get_items()) == null)) {
                                for (SleepSessionParse mfSleepSessionBySleepSessionParse : list) {
                                    arrayList2.add(mfSleepSessionBySleepSessionParse.getMfSleepSessionBySleepSessionParse());
                                }
                            }
                            if (!((cp4) ap4).b()) {
                                sleepSessionsRepository.insert$app_fossilRelease(arrayList2);
                            }
                            if ((apiResponse2 != null ? apiResponse2.get_range() : null) == null) {
                                return ap4;
                            }
                            Range range = apiResponse2.get_range();
                            if (range == null) {
                                wg6.a();
                                throw null;
                            } else if (!range.isHasNext()) {
                                return ap4;
                            } else {
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$0 = sleepSessionsRepository;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$1 = date4;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$2 = date3;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.I$0 = i5;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.I$1 = i4;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$3 = ap4;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$4 = arrayList2;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.L$5 = apiResponse2;
                                sleepSessionsRepository$fetchSleepSessions$Anon12.label = 2;
                                obj = sleepSessionsRepository.fetchSleepSessions(date4, date3, i5 + i4, i4, sleepSessionsRepository$fetchSleepSessions$Anon12);
                                if (obj == a) {
                                    return a;
                                }
                                ap42 = ap4;
                                return (ap4) obj;
                            }
                        } catch (Exception e2) {
                            e = e2;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str3 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("fetchSleepSessions exception=");
                            e.printStackTrace();
                            sb.append(cd6.a);
                            local2.d(str3, sb.toString());
                            return ap4;
                        }
                    }
                    return ap4;
                }
                if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("fetchSleepSessions Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            str2 = c2.getUserMessage();
                        }
                    } else {
                        str2 = message;
                    }
                    if (str2 == null) {
                        str2 = "";
                    }
                    sb2.append(str2);
                    local3.d(str4, sb2.toString());
                }
                return ap4;
            }
        }
        sleepSessionsRepository$fetchSleepSessions$Anon1 = new SleepSessionsRepository$fetchSleepSessions$Anon1(this, xe62);
        SleepSessionsRepository$fetchSleepSessions$Anon1 sleepSessionsRepository$fetchSleepSessions$Anon122 = sleepSessionsRepository$fetchSleepSessions$Anon1;
        Object obj22 = sleepSessionsRepository$fetchSleepSessions$Anon122.result;
        Object a3 = ff6.a();
        i3 = sleepSessionsRepository$fetchSleepSessions$Anon122.label;
        if (i3 != 0) {
        }
        ap4 = (ap4) obj22;
        String str22 = null;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final List<MFSleepSession> getPendingSleepSessions(Date date, Date date2) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        SleepDao sleepDao = this.mSleepDao;
        Date o = bk4.o(date);
        wg6.a((Object) o, "DateHelper.getStartOfDay(startDate)");
        Date j = bk4.j(date2);
        wg6.a((Object) j, "DateHelper.getEndOfDay(endDate)");
        return sleepDao.getPendingSleepSessions(o, j);
    }

    @DexIgnore
    public final LiveData<yx5<List<MFSleepSession>>> getSleepSessionList(Date date, Date date2, boolean z) {
        wg6.b(date, "start");
        wg6.b(date2, "end");
        Date o = bk4.o(date);
        Date j = bk4.j(date2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSleepSessionList: start = " + o + ", end = " + j);
        LiveData<yx5<List<MFSleepSession>>> b = sd.b(this.mFitnessDataDao.getFitnessDataLiveData(date, date2), new SleepSessionsRepository$getSleepSessionList$Anon1(this, o, j, z));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final void insert$app_fossilRelease(List<MFSleepSession> list) {
        wg6.b(list, "sleepSessionList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insert: sleepSessionList = " + list);
        for (MFSleepSession next : list) {
            if (next.getDate() >= 0) {
                MFSleepSession sleepSession = this.mSleepDao.getSleepSession((long) next.getRealEndTime());
                if ((sleepSession != null ? sleepSession.getUpdatedAt() : null) == null) {
                    next.setEditedSleepMinutes(Integer.valueOf(next.getRealSleepMinutes()));
                    next.setEditedStartTime(Integer.valueOf(next.getRealStartTime()));
                    next.setEditedEndTime(Integer.valueOf(next.getRealEndTime()));
                    next.setEditedSleepStateDistInMinute(next.getRealSleepStateDistInMinute());
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local2.d(str2, "Insert sleep session - date=" + bk4.e(new Date(next.getDate())) + ", sleepDistribution=" + next.getEditedSleepStateDistInMinute());
                    this.mSleepDao.upsertSleepSession(next);
                } else if (sleepSession.getUpdatedAt().getMillis() < next.getUpdatedAt().getMillis()) {
                    next.setEditedSleepMinutes(Integer.valueOf(next.getRealSleepMinutes()));
                    next.setEditedStartTime(Integer.valueOf(next.getRealStartTime()));
                    next.setEditedEndTime(Integer.valueOf(next.getRealEndTime()));
                    next.setEditedSleepStateDistInMinute(next.getRealSleepStateDistInMinute());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local3.d(str3, "Edit sleep session - date=" + bk4.e(new Date(next.getDate())) + ", sleepDistribution=" + next.getEditedSleepStateDistInMinute());
                    this.mSleepDao.upsertSleepSession(next);
                }
            }
        }
    }

    @DexIgnore
    public final void insertFromDevice(List<MFSleepSession> list) {
        wg6.b(list, "sleepSessionList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: sleepSessionList = " + list);
        for (MFSleepSession next : list) {
            if (isExistsSleepSession(next)) {
                FLogger.INSTANCE.getLocal().d(TAG, ".saveSyncResult - Sleep session already existed");
            } else if (this.mSleepDao.getSleepSession((long) next.getRealEndTime()) == null) {
                FLogger.INSTANCE.getLocal().d(TAG, ".saveSyncResult - Saving sleep session to local database");
                this.mSleepDao.addSleepSession(next);
            } else {
                FLogger.INSTANCE.getLocal().d(TAG, ".saveSyncResult - Sleep session already existed");
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v17, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v5, resolved type: java.util.List<com.portfolio.platform.data.model.room.sleep.MFSleepSession>} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final /* synthetic */ Object insertSleepSessionList(String str, List<MFSleepSession> list, xe6<? super ap4<List<MFSleepSession>>> xe6) {
        SleepSessionsRepository$insertSleepSessionList$Anon1 sleepSessionsRepository$insertSleepSessionList$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof SleepSessionsRepository$insertSleepSessionList$Anon1) {
            sleepSessionsRepository$insertSleepSessionList$Anon1 = (SleepSessionsRepository$insertSleepSessionList$Anon1) xe6;
            int i2 = sleepSessionsRepository$insertSleepSessionList$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                sleepSessionsRepository$insertSleepSessionList$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = sleepSessionsRepository$insertSleepSessionList$Anon1.result;
                Object a = ff6.a();
                i = sleepSessionsRepository$insertSleepSessionList$Anon1.label;
                String str2 = null;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local.d(str3, "insertSleepSessionList sleepSessionList=" + list);
                    fu3 fu3 = new fu3();
                    du3 du3 = new du3();
                    du3.a(Date.class, new GsonISOConvertDateTime());
                    Gson a2 = du3.a();
                    for (MFSleepSession sleepSession : list) {
                        try {
                            fu3.a(a2.b(new SleepSession(str, sleepSession), SleepSession.class));
                        } catch (Exception e) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str4 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("insertSleepSessionList exception=");
                            e.printStackTrace();
                            sb.append(cd6.a);
                            local2.e(str4, sb.toString());
                        }
                    }
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str5 = TAG;
                    local3.d(str5, "insertSleepSessionList jsonArray=" + fu3);
                    ku3 ku3 = new ku3();
                    ku3.a(CloudLogWriter.ITEMS_PARAM, fu3);
                    SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1 sleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1 = new SleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1(this, ku3, (xe6) null);
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$0 = this;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$1 = str;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$2 = list;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$3 = fu3;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$4 = a2;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.L$5 = ku3;
                    sleepSessionsRepository$insertSleepSessionList$Anon1.label = 1;
                    obj = ResponseKt.a(sleepSessionsRepository$insertSleepSessionList$repoResponse$Anon1, sleepSessionsRepository$insertSleepSessionList$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) sleepSessionsRepository$insertSleepSessionList$Anon1.L$5;
                    Gson gson = (Gson) sleepSessionsRepository$insertSleepSessionList$Anon1.L$4;
                    fu3 fu32 = (fu3) sleepSessionsRepository$insertSleepSessionList$Anon1.L$3;
                    list = sleepSessionsRepository$insertSleepSessionList$Anon1.L$2;
                    String str6 = (String) sleepSessionsRepository$insertSleepSessionList$Anon1.L$1;
                    SleepSessionsRepository sleepSessionsRepository = (SleepSessionsRepository) sleepSessionsRepository$insertSleepSessionList$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str7 = TAG;
                    local4.d(str7, "insertSleepSession onResponse: response = " + ap4);
                    return new cp4(list, false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str8 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("insertSleepSession Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    sb2.append(" message=");
                    ServerError c = zo4.c();
                    if (c != null) {
                        str2 = c.getMessage();
                    }
                    sb2.append(str2);
                    local5.e(str8, sb2.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), zo4.b());
                } else {
                    throw new kc6();
                }
            }
        }
        sleepSessionsRepository$insertSleepSessionList$Anon1 = new SleepSessionsRepository$insertSleepSessionList$Anon1(this, xe6);
        Object obj2 = sleepSessionsRepository$insertSleepSessionList$Anon1.result;
        Object a3 = ff6.a();
        i = sleepSessionsRepository$insertSleepSessionList$Anon1.label;
        String str22 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final void pushPendingSleepSessions(PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "pushPendingActivities fitnessDb=" + this.mSleepDatabase);
        List<MFSleepSession> pendingSleepSessions = this.mSleepDao.getPendingSleepSessions();
        MFUser b = zm4.p.a().n().b();
        String userId = b != null ? b.getUserId() : null;
        if (!(!pendingSleepSessions.isEmpty()) || TextUtils.isEmpty(userId)) {
            if (pushPendingSleepSessionsCallback != null) {
                pushPendingSleepSessionsCallback.onFail(MFNetworkReturnCode.NOT_FOUND);
            }
        } else if (userId != null) {
            saveSleepSessionsToServer(userId, pendingSleepSessions, pushPendingSleepSessionsCallback);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void upsertRecommendedGoals(SleepRecommendedGoal sleepRecommendedGoal) {
        wg6.b(sleepRecommendedGoal, "recommendedGoal");
        this.mSleepDao.upsertSleepRecommendedGoal(sleepRecommendedGoal);
    }
}
