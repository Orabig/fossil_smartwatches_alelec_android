package com.portfolio.platform.data.source.remote;

import com.fossil.jy6;
import com.fossil.ku3;
import com.fossil.rx6;
import com.fossil.uy6;
import com.fossil.xe6;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AuthApiUserService {
    @DexIgnore
    @uy6("rpc/auth/password/change")
    Call<ku3> changePassword(@jy6 ku3 ku3);

    @DexIgnore
    @uy6("rpc/auth/logout")
    Object logout(@jy6 ku3 ku3, xe6<? super rx6<Void>> xe6);
}
