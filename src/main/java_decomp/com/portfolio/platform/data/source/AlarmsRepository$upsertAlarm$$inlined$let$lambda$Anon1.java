package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ xe6 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $it;
    @DexIgnore
    public /* final */ /* synthetic */ List $serverAlarm$inlined;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1(List list, xe6 xe6, AlarmsRepository alarmsRepository, xe6 xe62, List list2) {
        super(2, xe6);
        this.$it = list;
        this.this$0 = alarmsRepository;
        this.$continuation$inlined = xe62;
        this.$serverAlarm$inlined = list2;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 = new AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1(this.$it, xe6, this.this$0, this.$continuation$inlined, this.$serverAlarm$inlined);
        alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1.p$ = (il6) obj;
        return alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.mAlarmsLocalDataSource.insertAlarms(this.$it);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
