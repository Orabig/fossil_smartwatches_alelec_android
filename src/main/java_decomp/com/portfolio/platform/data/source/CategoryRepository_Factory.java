package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryRepository_Factory implements Factory<CategoryRepository> {
    @DexIgnore
    public /* final */ Provider<CategoryDao> mCategoryDaoProvider;
    @DexIgnore
    public /* final */ Provider<CategoryRemoteDataSource> mCategoryRemoteDataSourceProvider;

    @DexIgnore
    public CategoryRepository_Factory(Provider<CategoryDao> provider, Provider<CategoryRemoteDataSource> provider2) {
        this.mCategoryDaoProvider = provider;
        this.mCategoryRemoteDataSourceProvider = provider2;
    }

    @DexIgnore
    public static CategoryRepository_Factory create(Provider<CategoryDao> provider, Provider<CategoryRemoteDataSource> provider2) {
        return new CategoryRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static CategoryRepository newCategoryRepository(CategoryDao categoryDao, CategoryRemoteDataSource categoryRemoteDataSource) {
        return new CategoryRepository(categoryDao, categoryRemoteDataSource);
    }

    @DexIgnore
    public static CategoryRepository provideInstance(Provider<CategoryDao> provider, Provider<CategoryRemoteDataSource> provider2) {
        return new CategoryRepository(provider.get(), provider2.get());
    }

    @DexIgnore
    public CategoryRepository get() {
        return provideInstance(this.mCategoryDaoProvider, this.mCategoryRemoteDataSourceProvider);
    }
}
