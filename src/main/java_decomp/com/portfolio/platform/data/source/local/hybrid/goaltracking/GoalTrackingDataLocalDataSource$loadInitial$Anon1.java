package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.rm6;
import com.fossil.vk4;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataLocalDataSource$loadInitial$Anon1 implements vk4.b {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingDataLocalDataSource$loadInitial$Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.this$0 = goalTrackingDataLocalDataSource;
    }

    @DexIgnore
    public final void run(vk4.b.a aVar) {
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.this$0;
        vk4.d dVar = vk4.d.INITIAL;
        wg6.a((Object) aVar, "helperCallback");
        rm6 unused = goalTrackingDataLocalDataSource.loadData(dVar, aVar, this.this$0.mOffset);
    }
}
