package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionsRepository_Factory implements Factory<SleepSessionsRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<FitnessDataDao> mFitnessDataDaoProvider;
    @DexIgnore
    public /* final */ Provider<SleepDao> mSleepDaoProvider;
    @DexIgnore
    public /* final */ Provider<SleepDatabase> mSleepDatabaseProvider;

    @DexIgnore
    public SleepSessionsRepository_Factory(Provider<SleepDao> provider, Provider<ApiServiceV2> provider2, Provider<SleepDatabase> provider3, Provider<FitnessDataDao> provider4) {
        this.mSleepDaoProvider = provider;
        this.mApiServiceProvider = provider2;
        this.mSleepDatabaseProvider = provider3;
        this.mFitnessDataDaoProvider = provider4;
    }

    @DexIgnore
    public static SleepSessionsRepository_Factory create(Provider<SleepDao> provider, Provider<ApiServiceV2> provider2, Provider<SleepDatabase> provider3, Provider<FitnessDataDao> provider4) {
        return new SleepSessionsRepository_Factory(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static SleepSessionsRepository newSleepSessionsRepository(SleepDao sleepDao, ApiServiceV2 apiServiceV2, SleepDatabase sleepDatabase, FitnessDataDao fitnessDataDao) {
        return new SleepSessionsRepository(sleepDao, apiServiceV2, sleepDatabase, fitnessDataDao);
    }

    @DexIgnore
    public static SleepSessionsRepository provideInstance(Provider<SleepDao> provider, Provider<ApiServiceV2> provider2, Provider<SleepDatabase> provider3, Provider<FitnessDataDao> provider4) {
        return new SleepSessionsRepository(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public SleepSessionsRepository get() {
        return provideInstance(this.mSleepDaoProvider, this.mApiServiceProvider, this.mSleepDatabaseProvider, this.mFitnessDataDaoProvider);
    }
}
