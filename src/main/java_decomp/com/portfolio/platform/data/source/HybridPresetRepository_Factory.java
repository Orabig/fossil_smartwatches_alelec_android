package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetRepository_Factory implements Factory<HybridPresetRepository> {
    @DexIgnore
    public /* final */ Provider<HybridPresetDao> mHybridPresetDaoProvider;
    @DexIgnore
    public /* final */ Provider<HybridPresetRemoteDataSource> mHybridPresetRemoteDataSourceProvider;

    @DexIgnore
    public HybridPresetRepository_Factory(Provider<HybridPresetDao> provider, Provider<HybridPresetRemoteDataSource> provider2) {
        this.mHybridPresetDaoProvider = provider;
        this.mHybridPresetRemoteDataSourceProvider = provider2;
    }

    @DexIgnore
    public static HybridPresetRepository_Factory create(Provider<HybridPresetDao> provider, Provider<HybridPresetRemoteDataSource> provider2) {
        return new HybridPresetRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static HybridPresetRepository newHybridPresetRepository(HybridPresetDao hybridPresetDao, HybridPresetRemoteDataSource hybridPresetRemoteDataSource) {
        return new HybridPresetRepository(hybridPresetDao, hybridPresetRemoteDataSource);
    }

    @DexIgnore
    public static HybridPresetRepository provideInstance(Provider<HybridPresetDao> provider, Provider<HybridPresetRemoteDataSource> provider2) {
        return new HybridPresetRepository(provider.get(), provider2.get());
    }

    @DexIgnore
    public HybridPresetRepository get() {
        return provideInstance(this.mHybridPresetDaoProvider, this.mHybridPresetRemoteDataSourceProvider);
    }
}
