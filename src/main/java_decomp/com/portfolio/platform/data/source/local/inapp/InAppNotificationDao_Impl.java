package com.portfolio.platform.data.source.local.inapp;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationDao_Impl extends InAppNotificationDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<InAppNotification> __deletionAdapterOfInAppNotification;
    @DexIgnore
    public /* final */ hh<InAppNotification> __insertionAdapterOfInAppNotification;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<InAppNotification> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `inAppNotification` (`id`,`title`,`content`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, inAppNotification.getId());
            }
            if (inAppNotification.getTitle() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, inAppNotification.getTitle());
            }
            if (inAppNotification.getContent() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, inAppNotification.getContent());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<InAppNotification> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `inAppNotification` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, inAppNotification.getId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<InAppNotification>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<InAppNotification> call() throws Exception {
            Cursor a = bi.a(InAppNotificationDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, Explore.COLUMN_TITLE);
                int b3 = ai.b(a, "content");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new InAppNotification(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public InAppNotificationDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfInAppNotification = new Anon1(ohVar);
        this.__deletionAdapterOfInAppNotification = new Anon2(ohVar);
    }

    @DexIgnore
    public void delete(InAppNotification inAppNotification) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfInAppNotification.handle(inAppNotification);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public LiveData<List<InAppNotification>> getAllInAppNotification() {
        return this.__db.getInvalidationTracker().a(new String[]{"inAppNotification"}, false, new Anon3(rh.b("SELECT * FROM inAppNotification", 0)));
    }

    @DexIgnore
    public void insertListInAppNotification(List<InAppNotification> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInAppNotification.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
