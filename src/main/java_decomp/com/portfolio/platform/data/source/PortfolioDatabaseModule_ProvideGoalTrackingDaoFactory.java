package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory implements Factory<GoalTrackingDao> {
    @DexIgnore
    public /* final */ Provider<GoalTrackingDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<GoalTrackingDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<GoalTrackingDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static GoalTrackingDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<GoalTrackingDatabase> provider) {
        return proxyProvideGoalTrackingDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static GoalTrackingDao proxyProvideGoalTrackingDao(PortfolioDatabaseModule portfolioDatabaseModule, GoalTrackingDatabase goalTrackingDatabase) {
        GoalTrackingDao provideGoalTrackingDao = portfolioDatabaseModule.provideGoalTrackingDao(goalTrackingDatabase);
        z76.a(provideGoalTrackingDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideGoalTrackingDao;
    }

    @DexIgnore
    public GoalTrackingDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
