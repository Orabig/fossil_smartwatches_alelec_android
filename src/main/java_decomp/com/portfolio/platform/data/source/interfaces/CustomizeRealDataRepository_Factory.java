package com.portfolio.platform.data.source.interfaces;

import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealDataRepository_Factory implements Factory<CustomizeRealDataRepository> {
    @DexIgnore
    public /* final */ Provider<CustomizeRealDataDao> mCustomizeRealDataProvider;

    @DexIgnore
    public CustomizeRealDataRepository_Factory(Provider<CustomizeRealDataDao> provider) {
        this.mCustomizeRealDataProvider = provider;
    }

    @DexIgnore
    public static CustomizeRealDataRepository_Factory create(Provider<CustomizeRealDataDao> provider) {
        return new CustomizeRealDataRepository_Factory(provider);
    }

    @DexIgnore
    public static CustomizeRealDataRepository newCustomizeRealDataRepository(CustomizeRealDataDao customizeRealDataDao) {
        return new CustomizeRealDataRepository(customizeRealDataDao);
    }

    @DexIgnore
    public static CustomizeRealDataRepository provideInstance(Provider<CustomizeRealDataDao> provider) {
        return new CustomizeRealDataRepository(provider.get());
    }

    @DexIgnore
    public CustomizeRealDataRepository get() {
        return provideInstance(this.mCustomizeRealDataProvider);
    }
}
