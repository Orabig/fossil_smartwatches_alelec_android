package com.portfolio.platform.data;

import android.location.Location;
import com.fossil.lk6;
import com.fossil.mc6;
import com.fossil.wg6;
import com.fossil.wv2;
import com.fossil.yv2;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends yv2 {
    @DexIgnore
    public /* final */ /* synthetic */ lk6 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ wv2 $this_requestLocationUpdates$inlined;

    @DexIgnore
    public LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(lk6 lk6, wv2 wv2, LocationRequest locationRequest) {
        this.$continuation = lk6;
        this.$this_requestLocationUpdates$inlined = wv2;
        this.$locationRequest$inlined = locationRequest;
    }

    @DexIgnore
    public void onLocationResult(LocationResult locationResult) {
        wg6.b(locationResult, "locationResult");
        LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1.super.onLocationResult(locationResult);
        Location p = locationResult.p();
        if (p != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "onLocationResult lastLocation=" + p);
        } else {
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "onLocationResult lastLocation is null");
        }
        this.$this_requestLocationUpdates$inlined.a(this);
        if (this.$continuation.isActive()) {
            lk6 lk6 = this.$continuation;
            mc6.a aVar = mc6.Companion;
            lk6.resumeWith(mc6.m1constructorimpl(p));
        }
    }
}
