package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.zm4;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryLocalDataSource extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryLocalDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wg6.b(str, "<set-?>");
            MicroAppGalleryLocalDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryLocalDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "MicroAppGalleryLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public void deleteListMicroApp(String str) {
        wg6.b(str, "serial");
        String str2 = TAG;
        MFLogger.d(str2, "deleteListMicroApp serial=" + str);
        zm4.p.a().d().deleteMicroAppList(str);
    }

    @DexIgnore
    public void getMicroApp(String str, String str2, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        wg6.b(str, "deviceSerial");
        wg6.b(str2, "microAppId");
        String str3 = TAG;
        MFLogger.d(str3, "getMicroApp deviceSerial=" + str + " microAppId=" + str2);
        MicroApp microApp = zm4.p.a().d().getMicroApp(str, str2);
        if (microApp != null) {
            MFLogger.d(TAG, "getMicroApp onSuccess");
            if (getMicroAppCallback != null) {
                getMicroAppCallback.onSuccess(microApp);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getMicroApp onFail");
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onFail();
        }
    }

    @DexIgnore
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        wg6.b(str, "deviceSerial");
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppGallery deviceSerial=" + str);
        List<MicroApp> listMicroApp = zm4.p.a().d().getListMicroApp(str);
        if (listMicroApp == null || !(!listMicroApp.isEmpty())) {
            MFLogger.d(TAG, "getMicroAppGallery onFail");
            if (getMicroAppGalleryCallback != null) {
                getMicroAppGalleryCallback.onFail();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getMicroAppGallery onSuccess");
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onSuccess(listMicroApp);
        }
    }

    @DexIgnore
    public void updateListMicroApp(List<? extends MicroApp> list) {
        if (list != null && (!list.isEmpty())) {
            String str = TAG;
            MFLogger.d(str, "updateListMicroApp microAppListSize=" + list.size());
            zm4.p.a().d().updateListMicroApp(list);
        }
    }
}
