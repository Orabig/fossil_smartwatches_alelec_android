package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.hp4;
import com.fossil.wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepository$downloadAllVariants$Anon1 implements MicroAppVariantDataSource.GetVariantListRemoteCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantDataSource.GetVariantListCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ int $major;
    @DexIgnore
    public /* final */ /* synthetic */ int $minor;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository this$0;

    @DexIgnore
    public MicroAppVariantRepository$downloadAllVariants$Anon1(MicroAppVariantRepository microAppVariantRepository, String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        this.this$0 = microAppVariantRepository;
        this.$serialNumber = str;
        this.$major = i;
        this.$minor = i2;
        this.$callback = getVariantListCallback;
    }

    @DexIgnore
    public void onFail(int i) {
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants remote serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onFail");
        MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback = this.$callback;
        if (getVariantListCallback != null) {
            getVariantListCallback.onFail(i);
        }
    }

    @DexIgnore
    public void onSuccess(ArrayList<hp4> arrayList) {
        wg6.b(arrayList, "variantParserList");
        String tag = MicroAppVariantRepository.Companion.getTAG();
        MFLogger.d(tag, "getAllMicroAppVariants remote serialNumber=" + this.$serialNumber + " major=" + this.$major + " minor=" + this.$minor + " onSuccess");
        this.this$0.mAppExecutors.a().execute(new MicroAppVariantRepository$downloadAllVariants$Anon1$onSuccess$Anon1_Level2(this, arrayList));
    }

    @DexIgnore
    public void onSuccess(List<MicroAppVariant> list) {
        wg6.b(list, "variantList");
    }
}
