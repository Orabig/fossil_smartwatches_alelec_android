package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.dx6;
import com.fossil.ep4;
import com.fossil.fu3;
import com.fossil.ku3;
import com.fossil.rx6;
import com.fossil.u04;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class PresetRemoteDataSource extends PresetDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "PresetRemoteDataSource";
    @DexIgnore
    public /* final */ ShortcutApiService mApiService;
    @DexIgnore
    public /* final */ u04 mAppExecutors;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements dx6<ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetSavedPresetListCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ List val$savedPresets;

        @DexIgnore
        public Anon1(List list, PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
            this.val$savedPresets = list;
            this.val$callback = getSavedPresetListCallback;
        }

        @DexIgnore
        public void onFailure(Call<ku3> call, Throwable th) {
            MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure");
            if (!this.val$savedPresets.isEmpty()) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure presetList not null");
                PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
                if (getSavedPresetListCallback != null) {
                    getSavedPresetListCallback.onSuccess(this.val$savedPresets);
                    return;
                }
                return;
            }
            MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure presetList is null");
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback2 = this.val$callback;
            if (getSavedPresetListCallback2 != null) {
                getSavedPresetListCallback2.onFail();
            }
        }

        @DexIgnore
        public void onResponse(Call<ku3> call, rx6<ku3> rx6) {
            if (rx6.d()) {
                ep4 ep4 = new ep4();
                ep4.a((ku3) rx6.a());
                this.val$savedPresets.addAll(ep4.b());
                Range a = ep4.a();
                if (a != null && a.isHasNext()) {
                    MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onSuccess hasNext=true");
                    PresetRemoteDataSource.this.getUserPresets(a.getOffset() + a.getLimit() + 1, a.getLimit(), this);
                } else if (this.val$callback != null) {
                    MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onSuccess hasNext=false");
                    if (!this.val$savedPresets.isEmpty()) {
                        this.val$callback.onSuccess(this.val$savedPresets);
                    } else {
                        this.val$callback.onFail();
                    }
                }
            } else {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful");
                if (!this.val$savedPresets.isEmpty()) {
                    MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful presetList not null");
                    PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
                    if (getSavedPresetListCallback != null) {
                        getSavedPresetListCallback.onSuccess(this.val$savedPresets);
                        return;
                    }
                    return;
                }
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful presetList is null");
                PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback2 = this.val$callback;
                if (getSavedPresetListCallback2 != null) {
                    getSavedPresetListCallback2.onFail();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 implements PresetDataSource.AddOrUpdateSavedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.AddOrUpdateSavedPresetCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ SavedPreset val$savedPreset;

        @DexIgnore
        public Anon2(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
            this.val$savedPreset = savedPreset;
            this.val$callback = addOrUpdateSavedPresetCallback;
        }

        @DexIgnore
        public void onFail() {
            String str = PresetRemoteDataSource.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset onFail mappingSetId=" + this.val$savedPreset.getId() + " mappingSetName=" + this.val$savedPreset.getName());
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onFail();
            }
        }

        @DexIgnore
        public void onSuccess(List<SavedPreset> list) {
            String str = PresetRemoteDataSource.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset onSuccess mappingSetId=" + this.val$savedPreset.getId() + " mappingSetName=" + this.val$savedPreset.getName());
            if (!list.isEmpty()) {
                PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
                if (addOrUpdateSavedPresetCallback != null) {
                    addOrUpdateSavedPresetCallback.onSuccess(list.get(0));
                    return;
                }
                return;
            }
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback2 = this.val$callback;
            if (addOrUpdateSavedPresetCallback2 != null) {
                addOrUpdateSavedPresetCallback2.onFail();
            }
        }
    }

    @DexIgnore
    public PresetRemoteDataSource(ShortcutApiService shortcutApiService, u04 u04) {
        this.mApiService = shortcutApiService;
        this.mAppExecutors = u04;
    }

    @DexIgnore
    private ku3 createListItemJsonObject(fu3 fu3) {
        ku3 ku3 = new ku3();
        ku3.a(CloudLogWriter.ITEMS_PARAM, fu3);
        return ku3;
    }

    @DexIgnore
    public void addOrUpdateActivePreset(ActivePreset activePreset, PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateActivePreset serialNumber=" + activePreset.getSerialNumber() + " originalId=" + activePreset.getOriginalId());
        fu3 fu3 = new fu3();
        fu3.a(activePreset.getJsonObject());
        ku3 createListItemJsonObject = createListItemJsonObject(fu3);
        String str2 = TAG;
        MFLogger.d(str2, "initJsonData - json: " + createListItemJsonObject);
    }

    @DexIgnore
    public void addOrUpdateSavedPreset(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset mappingSetId=" + savedPreset.getId() + " mappingSetName=" + savedPreset.getName());
        ArrayList arrayList = new ArrayList();
        arrayList.add(savedPreset);
        addOrUpdateSavedPresetList(arrayList, new Anon2(savedPreset, addOrUpdateSavedPresetCallback));
    }

    @DexIgnore
    public void addOrUpdateSavedPresetList(List<SavedPreset> list, PresetDataSource.AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset presetListSize=" + list.size());
        fu3 fu3 = new fu3();
        for (SavedPreset jsonObject : list) {
            fu3.a(jsonObject.getJsonObject());
        }
        ku3 createListItemJsonObject = createListItemJsonObject(fu3);
        String str2 = TAG;
        MFLogger.d(str2, "initJsonData - json: " + createListItemJsonObject);
    }

    @DexIgnore
    public void clearData() {
    }

    @DexIgnore
    public void deleteSavedPresetList(List<SavedPreset> list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str = TAG;
        MFLogger.d(str, "deleteSavedPresetList presetListSize=" + list.size());
        fu3 fu3 = new fu3();
        try {
            for (SavedPreset id : list) {
                ku3 ku3 = new ku3();
                ku3.a("id", id.getId());
                fu3.a(ku3);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        createListItemJsonObject(fu3);
    }

    @DexIgnore
    public void downloadActivePresetList(PresetDataSource.GetActivePresetListCallback getActivePresetListCallback) {
    }

    @DexIgnore
    public void getDefaultPreset(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getDefaultPreset deviceSerial=" + str);
        new ArrayList();
    }

    @DexIgnore
    public void getRecommendedPresets(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getRecommendedPresets deviceSerial=" + str);
        new ArrayList();
    }

    @DexIgnore
    public void getSavedPresetList(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
        MFLogger.d(TAG, "getSavedPresetList");
        getUserPresets(0, 100, new Anon1(new ArrayList(), getSavedPresetListCallback));
    }

    @DexIgnore
    public void getUserPresets(int i, int i2, dx6<ku3> dx6) {
    }
}
