package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.fu3;
import com.fossil.ku3;
import com.fossil.vu3;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "savedPreset")
public class SavedPreset implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_BUTTONS; // = "buttons";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createAt";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<SavedPreset> CREATOR; // = new Anon1();
    @DexIgnore
    public static /* final */ String TAG; // = SavedPreset.class.getSimpleName();
    @DexIgnore
    public List<ButtonMapping> buttonMappingList;
    @DexIgnore
    @vu3("buttons")
    @DatabaseField(columnName = "buttons")
    public String buttons;
    @DexIgnore
    @vu3("createAt")
    @DatabaseField(columnName = "createAt")
    public long createAt;
    @DexIgnore
    @vu3("id")
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @vu3("name")
    @DatabaseField(columnName = "name")
    public String name;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @vu3("updatedAt")
    @DatabaseField(columnName = "updatedAt")
    public long updateAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<SavedPreset> {
        @DexIgnore
        public SavedPreset createFromParcel(Parcel parcel) {
            return new SavedPreset(parcel);
        }

        @DexIgnore
        public SavedPreset[] newArray(int i) {
            return new SavedPreset[i];
        }
    }

    @DexIgnore
    public enum MappingSetType {
        RECOMMENDED("recommended"),
        DEFAULT("default"),
        USER_SAVED("saved"),
        USER_NOT_SAVED("not saved");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public MappingSetType(String str) {
            this.value = str;
        }

        @DexIgnore
        public static MappingSetType fromString(String str) {
            for (MappingSetType mappingSetType : values()) {
                if (mappingSetType.value.equals(str)) {
                    return mappingSetType;
                }
            }
            return USER_SAVED;
        }

        @DexIgnore
        public String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public SavedPreset() {
        this.buttonMappingList = new ArrayList();
        this.id = UUID.randomUUID().toString();
        this.id = UUID.randomUUID().toString();
        this.pinType = 0;
    }

    @DexIgnore
    private List<ButtonMapping> getListMappingFromJson() {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(this.buttons);
            if (jSONArray.length() > 0) {
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    ButtonMapping buttonMapping = new ButtonMapping("", "");
                    if (jSONObject.has("button")) {
                        buttonMapping.setButton(jSONObject.getString("button"));
                    }
                    if (jSONObject.has("appId")) {
                        buttonMapping.setMicroAppId(jSONObject.getString("appId"));
                    }
                    arrayList.add(buttonMapping);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    @DexIgnore
    private void write(JsonWriter jsonWriter, List<ButtonMapping> list) throws IOException {
        jsonWriter.m();
        for (ButtonMapping next : list) {
            jsonWriter.n();
            jsonWriter.e("button").h(next.getButton());
            jsonWriter.e("appId").h(next.getMicroAppId());
            jsonWriter.p();
        }
        jsonWriter.o();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public List<ButtonMapping> getButtonMappingList() {
        if (this.buttonMappingList.isEmpty() && !TextUtils.isEmpty(this.buttons)) {
            this.buttonMappingList = new ArrayList(getListMappingFromJson());
        }
        return this.buttonMappingList;
    }

    @DexIgnore
    public String getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public ku3 getJsonObject() {
        ku3 ku3 = new ku3();
        try {
            ku3.a("createAt", Long.valueOf(this.createAt));
            ku3.a("updatedAt", Long.valueOf(this.updateAt));
            ku3.a("id", this.id);
            ku3.a("name", this.name);
            ku3.a("buttons", (JsonElement) new Gson().a(this.buttons, fu3.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        MFLogger.d("SavedPreset", "initJsonData - json: " + ku3);
        return ku3;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public void setButtonMappingList(List<ButtonMapping> list) throws IOException {
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = new JsonWriter(stringWriter);
        write(jsonWriter, list);
        this.buttons = stringWriter.toString();
        this.buttonMappingList.clear();
        this.buttonMappingList.addAll(list);
        try {
            jsonWriter.close();
            stringWriter.close();
        } catch (Exception e) {
            String str = TAG;
            MFLogger.d(str, "Exception when close write e=" + e);
        }
    }

    @DexIgnore
    public void setButtons(String str) {
        this.buttons = str;
        getButtonMappingList();
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j / 1000;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j / 1000;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.buttonMappingList);
        parcel.writeString(this.name);
        parcel.writeString(this.buttons);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
        parcel.writeString(this.id);
        parcel.writeInt(this.pinType);
    }

    @DexIgnore
    public SavedPreset(SavedPreset savedPreset) {
        this.buttonMappingList = new ArrayList();
        this.id = UUID.randomUUID().toString();
        this.id = UUID.randomUUID().toString();
        this.name = savedPreset.name;
        this.createAt = savedPreset.createAt;
        this.updateAt = savedPreset.updateAt;
        this.pinType = 0;
        setButtons(savedPreset.buttons);
    }

    @DexIgnore
    public SavedPreset(Parcel parcel) {
        this.buttonMappingList = new ArrayList();
        this.id = UUID.randomUUID().toString();
        parcel.readTypedList(this.buttonMappingList, ButtonMapping.CREATOR);
        this.name = parcel.readString();
        this.buttons = parcel.readString();
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
        this.id = parcel.readString();
        this.pinType = parcel.readInt();
    }
}
