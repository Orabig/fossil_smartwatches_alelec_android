package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.u04;
import com.fossil.wg6;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepositoryModule {
    @DexIgnore
    @Local
    public final MicroAppVariantDataSource provideMicroAppVariantLocalDataSource$app_fossilRelease() {
        return new MicroAppVariantLocalDataSource();
    }

    @DexIgnore
    @Remote
    public final MicroAppVariantDataSource provideMicroAppVariantRemoteDataSource$app_fossilRelease(ShortcutApiService shortcutApiService, u04 u04) {
        wg6.b(shortcutApiService, "shortcutApiService");
        wg6.b(u04, "appExecutors");
        return new MicroAppVariantRemoteDataSource(shortcutApiService, u04);
    }
}
