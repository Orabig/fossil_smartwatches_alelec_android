package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2 implements MicroAppGalleryDataSource.GetMicroAppGalleryCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryRepository$getMicroApp$Anon1 this$0;

    @DexIgnore
    public MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2(MicroAppGalleryRepository$getMicroApp$Anon1 microAppGalleryRepository$getMicroApp$Anon1) {
        this.this$0 = microAppGalleryRepository$getMicroApp$Anon1;
    }

    @DexIgnore
    public void onFail() {
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroApp microAppId=" + this.this$0.$microAppId + " remote onFail");
        MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback = this.this$0.$callback;
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onFail();
        }
    }

    @DexIgnore
    public void onSuccess(List<? extends MicroApp> list) {
        MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback;
        wg6.b(list, "microAppList");
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroApp microAppId=" + this.this$0.$microAppId + " remote onSuccess");
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ((MicroApp) list.get(i)).setPlatform(this.this$0.$deviceSerial);
            if (wg6.a((Object) ((MicroApp) list.get(i)).getAppId(), (Object) this.this$0.$microAppId) && (getMicroAppCallback = this.this$0.$callback) != null) {
                getMicroAppCallback.onSuccess((MicroApp) list.get(i));
            }
        }
        this.this$0.this$0.mAppExecutors.a().execute(new MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2$onSuccess$Anon1_Level3(this, list));
    }
}
