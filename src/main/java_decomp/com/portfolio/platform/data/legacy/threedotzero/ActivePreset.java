package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.fu3;
import com.fossil.ku3;
import com.fossil.vu3;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "activePreset")
public class ActivePreset implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_BUTTONS; // = "buttons";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createAt";
    @DexIgnore
    public static /* final */ String COLUMN_ORIGINAL_ID; // = "originalId";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_SERIAL; // = "serialNumber";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<ActivePreset> CREATOR; // = new Anon1();
    @DexIgnore
    public static /* final */ String TAG; // = ActivePreset.class.getSimpleName();
    @DexIgnore
    public List<ButtonMapping> buttonMappingList;
    @DexIgnore
    @vu3("buttons")
    @DatabaseField(columnName = "buttons")
    public String buttons;
    @DexIgnore
    @vu3("createAt")
    @DatabaseField(columnName = "createAt")
    public long createAt;
    @DexIgnore
    @vu3("originalId")
    @DatabaseField(columnName = "originalId")
    public String originalId;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @vu3("serialNumber")
    @DatabaseField(columnName = "serialNumber", id = true)
    public String serialNumber;
    @DexIgnore
    @vu3("updatedAt")
    @DatabaseField(columnName = "updatedAt")
    public long updateAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<ActivePreset> {
        @DexIgnore
        public ActivePreset createFromParcel(Parcel parcel) {
            return new ActivePreset(parcel);
        }

        @DexIgnore
        public ActivePreset[] newArray(int i) {
            return new ActivePreset[i];
        }
    }

    @DexIgnore
    public ActivePreset() {
        this.buttonMappingList = new ArrayList();
        this.pinType = 0;
        this.originalId = "";
    }

    @DexIgnore
    private List<ButtonMapping> getListMappingFromJson() {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(this.buttons);
            if (jSONArray.length() > 0) {
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    ButtonMapping buttonMapping = new ButtonMapping("", "");
                    if (jSONObject.has("button")) {
                        buttonMapping.setButton(jSONObject.getString("button"));
                    }
                    if (jSONObject.has("appId")) {
                        buttonMapping.setMicroAppId(jSONObject.getString("appId"));
                    }
                    arrayList.add(buttonMapping);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    @DexIgnore
    private void write(JsonWriter jsonWriter, List<ButtonMapping> list) throws IOException {
        jsonWriter.m();
        for (ButtonMapping next : list) {
            jsonWriter.n();
            jsonWriter.e("button").h(next.getButton());
            jsonWriter.e("appId").h(next.getMicroAppId());
            jsonWriter.p();
        }
        jsonWriter.o();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public List<ButtonMapping> getButtonMappingList() {
        if (this.buttonMappingList.isEmpty() && !TextUtils.isEmpty(this.buttons)) {
            this.buttonMappingList = new ArrayList(getListMappingFromJson());
        }
        return this.buttonMappingList;
    }

    @DexIgnore
    public String getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public long getCreateAt() {
        return this.createAt;
    }

    @DexIgnore
    public ku3 getJsonObject() {
        ku3 ku3 = new ku3();
        try {
            ku3.a("createAt", Long.valueOf(this.createAt));
            ku3.a("updatedAt", Long.valueOf(this.updateAt));
            ku3.a(COLUMN_ORIGINAL_ID, this.originalId == null ? "" : this.originalId);
            ku3.a("serialNumber", this.serialNumber);
            ku3.a("buttons", (JsonElement) new Gson().a(this.buttons, fu3.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        MFLogger.d("SavedPreset", "initJsonData - json: " + ku3);
        return ku3;
    }

    @DexIgnore
    public String getOriginalId() {
        return this.originalId;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public long getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public void setButtonMappingList(List<ButtonMapping> list) throws IOException {
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = new JsonWriter(stringWriter);
        write(jsonWriter, list);
        this.buttons = stringWriter.toString();
        this.buttonMappingList.clear();
        this.buttonMappingList.addAll(list);
        try {
            jsonWriter.close();
            stringWriter.close();
        } catch (Exception e) {
            String str = TAG;
            MFLogger.d(str, "setButtonMappingList error when close writer e=" + e);
        }
    }

    @DexIgnore
    public void setButtons(String str) {
        this.buttons = str;
        if (!TextUtils.isEmpty(str)) {
            this.buttonMappingList = new ArrayList(getListMappingFromJson());
        }
    }

    @DexIgnore
    public void setCreateAt(long j) {
        this.createAt = j / 1000;
    }

    @DexIgnore
    public void setOriginalId(String str) {
        this.originalId = str;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setSerialNumber(String str) {
        this.serialNumber = str;
    }

    @DexIgnore
    public void setUpdateAt(long j) {
        this.updateAt = j / 1000;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.buttonMappingList);
        parcel.writeString(this.buttons);
        parcel.writeLong(this.createAt);
        parcel.writeLong(this.updateAt);
        parcel.writeString(this.originalId);
        parcel.writeInt(this.pinType);
    }

    @DexIgnore
    public ActivePreset(ActivePreset activePreset) {
        this.buttonMappingList = new ArrayList();
        this.createAt = activePreset.createAt;
        this.updateAt = activePreset.updateAt;
        this.originalId = activePreset.originalId;
        this.serialNumber = activePreset.serialNumber;
        this.originalId = activePreset.originalId;
        setButtons(activePreset.buttons);
        this.pinType = 0;
    }

    @DexIgnore
    public ActivePreset(Parcel parcel) {
        this.buttonMappingList = new ArrayList();
        parcel.readTypedList(this.buttonMappingList, ButtonMapping.CREATOR);
        this.buttons = parcel.readString();
        this.createAt = parcel.readLong();
        this.updateAt = parcel.readLong();
        this.originalId = parcel.readString();
        this.pinType = parcel.readInt();
    }
}
