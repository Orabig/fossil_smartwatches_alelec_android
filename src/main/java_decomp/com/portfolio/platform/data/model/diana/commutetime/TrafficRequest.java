package com.portfolio.platform.data.model.diana.commutetime;

import com.fossil.qd6;
import com.fossil.vu3;
import com.fossil.wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TrafficRequest {
    @DexIgnore
    @vu3("avoid")
    public List<String> avoid;
    @DexIgnore
    @vu3("destination")
    public Address destination;
    @DexIgnore
    @vu3("mode")
    public String mode; // = "driving";
    @DexIgnore
    @vu3("origin")
    public Address origin;

    @DexIgnore
    public TrafficRequest(boolean z, Address address, Address address2) {
        wg6.b(address, "destination");
        wg6.b(address2, "origin");
        this.destination = address;
        this.origin = address2;
        this.avoid = z ? qd6.a((T[]) new String[]{"tolls"}) : null;
        this.mode = "driving";
    }

    @DexIgnore
    public final List<String> getAvoid() {
        return this.avoid;
    }

    @DexIgnore
    public final Address getDestination() {
        return this.destination;
    }

    @DexIgnore
    public final String getMode() {
        return this.mode;
    }

    @DexIgnore
    public final Address getOrigin() {
        return this.origin;
    }

    @DexIgnore
    public final void setAvoid(List<String> list) {
        this.avoid = list;
    }

    @DexIgnore
    public final void setDestination(Address address) {
        wg6.b(address, "<set-?>");
        this.destination = address;
    }

    @DexIgnore
    public final void setMode(String str) {
        wg6.b(str, "<set-?>");
        this.mode = str;
    }

    @DexIgnore
    public final void setOrigin(Address address) {
        wg6.b(address, "<set-?>");
        this.origin = address;
    }
}
