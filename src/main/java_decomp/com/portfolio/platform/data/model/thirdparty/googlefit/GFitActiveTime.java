package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitActiveTime {
    @DexIgnore
    public List<Long> activeTimes;
    @DexIgnore
    public int id;

    @DexIgnore
    public GFitActiveTime() {
        this((List) null, 1, (qg6) null);
    }

    @DexIgnore
    public GFitActiveTime(List<Long> list) {
        wg6.b(list, "activeTimes");
        this.activeTimes = list;
    }

    @DexIgnore
    public static /* synthetic */ GFitActiveTime copy$default(GFitActiveTime gFitActiveTime, List<Long> list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = gFitActiveTime.activeTimes;
        }
        return gFitActiveTime.copy(list);
    }

    @DexIgnore
    public final List<Long> component1() {
        return this.activeTimes;
    }

    @DexIgnore
    public final GFitActiveTime copy(List<Long> list) {
        wg6.b(list, "activeTimes");
        return new GFitActiveTime(list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof GFitActiveTime) && wg6.a((Object) this.activeTimes, (Object) ((GFitActiveTime) obj).activeTimes);
        }
        return true;
    }

    @DexIgnore
    public final List<Long> getActiveTimes() {
        return this.activeTimes;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public int hashCode() {
        List<Long> list = this.activeTimes;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setActiveTimes(List<Long> list) {
        wg6.b(list, "<set-?>");
        this.activeTimes = list;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public String toString() {
        return "GFitActiveTime(activeTimes=" + this.activeTimes + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GFitActiveTime(List list, int i, qg6 qg6) {
        this((i & 1) != 0 ? qd6.a() : list);
    }
}
