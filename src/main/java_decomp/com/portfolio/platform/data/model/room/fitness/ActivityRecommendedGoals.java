package com.portfolio.platform.data.model.room.fitness;

import com.fossil.d;
import com.fossil.pj4;
import com.fossil.vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityRecommendedGoals {
    @DexIgnore
    @pj4
    public int id;
    @DexIgnore
    @vu3("recommendedGoalActiveTime")
    public int recommendedActiveTimeGoal;
    @DexIgnore
    @vu3("recommendedGoalCalories")
    public int recommendedCaloriesGoal;
    @DexIgnore
    @vu3("recommendedGoalSteps")
    public int recommendedStepsGoal;

    @DexIgnore
    public ActivityRecommendedGoals(int i, int i2, int i3) {
        this.recommendedStepsGoal = i;
        this.recommendedCaloriesGoal = i2;
        this.recommendedActiveTimeGoal = i3;
    }

    @DexIgnore
    public static /* synthetic */ ActivityRecommendedGoals copy$default(ActivityRecommendedGoals activityRecommendedGoals, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = activityRecommendedGoals.recommendedStepsGoal;
        }
        if ((i4 & 2) != 0) {
            i2 = activityRecommendedGoals.recommendedCaloriesGoal;
        }
        if ((i4 & 4) != 0) {
            i3 = activityRecommendedGoals.recommendedActiveTimeGoal;
        }
        return activityRecommendedGoals.copy(i, i2, i3);
    }

    @DexIgnore
    public final int component1() {
        return this.recommendedStepsGoal;
    }

    @DexIgnore
    public final int component2() {
        return this.recommendedCaloriesGoal;
    }

    @DexIgnore
    public final int component3() {
        return this.recommendedActiveTimeGoal;
    }

    @DexIgnore
    public final ActivityRecommendedGoals copy(int i, int i2, int i3) {
        return new ActivityRecommendedGoals(i, i2, i3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivityRecommendedGoals)) {
            return false;
        }
        ActivityRecommendedGoals activityRecommendedGoals = (ActivityRecommendedGoals) obj;
        return this.recommendedStepsGoal == activityRecommendedGoals.recommendedStepsGoal && this.recommendedCaloriesGoal == activityRecommendedGoals.recommendedCaloriesGoal && this.recommendedActiveTimeGoal == activityRecommendedGoals.recommendedActiveTimeGoal;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final int getRecommendedActiveTimeGoal() {
        return this.recommendedActiveTimeGoal;
    }

    @DexIgnore
    public final int getRecommendedCaloriesGoal() {
        return this.recommendedCaloriesGoal;
    }

    @DexIgnore
    public final int getRecommendedStepsGoal() {
        return this.recommendedStepsGoal;
    }

    @DexIgnore
    public int hashCode() {
        return (((d.a(this.recommendedStepsGoal) * 31) + d.a(this.recommendedCaloriesGoal)) * 31) + d.a(this.recommendedActiveTimeGoal);
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setRecommendedActiveTimeGoal(int i) {
        this.recommendedActiveTimeGoal = i;
    }

    @DexIgnore
    public final void setRecommendedCaloriesGoal(int i) {
        this.recommendedCaloriesGoal = i;
    }

    @DexIgnore
    public final void setRecommendedStepsGoal(int i) {
        this.recommendedStepsGoal = i;
    }

    @DexIgnore
    public String toString() {
        return "ActivityRecommendedGoals(recommendedStepsGoal=" + this.recommendedStepsGoal + ", recommendedCaloriesGoal=" + this.recommendedCaloriesGoal + ", recommendedActiveTimeGoal=" + this.recommendedActiveTimeGoal + ")";
    }
}
