package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutStateChangeWrapper {
    @DexIgnore
    public int indexInSecond;
    @DexIgnore
    public int state;

    @DexIgnore
    public WorkoutStateChangeWrapper(int i, int i2) {
        this.state = i;
        this.indexInSecond = i2;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutStateChangeWrapper copy$default(WorkoutStateChangeWrapper workoutStateChangeWrapper, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = workoutStateChangeWrapper.state;
        }
        if ((i3 & 2) != 0) {
            i2 = workoutStateChangeWrapper.indexInSecond;
        }
        return workoutStateChangeWrapper.copy(i, i2);
    }

    @DexIgnore
    public final int component1() {
        return this.state;
    }

    @DexIgnore
    public final int component2() {
        return this.indexInSecond;
    }

    @DexIgnore
    public final WorkoutStateChangeWrapper copy(int i, int i2) {
        return new WorkoutStateChangeWrapper(i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkoutStateChangeWrapper)) {
            return false;
        }
        WorkoutStateChangeWrapper workoutStateChangeWrapper = (WorkoutStateChangeWrapper) obj;
        return this.state == workoutStateChangeWrapper.state && this.indexInSecond == workoutStateChangeWrapper.indexInSecond;
    }

    @DexIgnore
    public final int getIndexInSecond() {
        return this.indexInSecond;
    }

    @DexIgnore
    public final int getState() {
        return this.state;
    }

    @DexIgnore
    public int hashCode() {
        return (d.a(this.state) * 31) + d.a(this.indexInSecond);
    }

    @DexIgnore
    public final void setIndexInSecond(int i) {
        this.indexInSecond = i;
    }

    @DexIgnore
    public final void setState(int i) {
        this.state = i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutStateChangeWrapper(state=" + this.state + ", indexInSecond=" + this.indexInSecond + ")";
    }
}
