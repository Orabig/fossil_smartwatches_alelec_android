package com.portfolio.platform.data.model.diana.preset;

import android.graphics.drawable.Drawable;
import com.fossil.ai4;
import com.fossil.qg6;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceWrapper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public Drawable background;
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public Drawable bottomComplication;
    @DexIgnore
    public MetaData bottomMetaData;
    @DexIgnore
    public Drawable combination;
    @DexIgnore
    public String id;
    @DexIgnore
    public boolean isRemoveIconVisible;
    @DexIgnore
    public Drawable leftComplication;
    @DexIgnore
    public MetaData leftMetaData;
    @DexIgnore
    public String name;
    @DexIgnore
    public Drawable rightComplication;
    @DexIgnore
    public MetaData rightMetaData;
    @DexIgnore
    public Drawable topComplication;
    @DexIgnore
    public MetaData topMetaData;
    @DexIgnore
    public ai4 type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final WatchFaceWrapper createBackgroundWrapper(String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig, ai4 ai4) {
            wg6.b(str, "id");
            wg6.b(str2, "name");
            wg6.b(backgroundConfig, "backgroundConfig");
            wg6.b(ai4, "type");
            return new WatchFaceWrapper(str, drawable, drawable2, drawable3, drawable4, drawable5, drawable6, metaData, metaData2, metaData3, metaData4, str2, backgroundConfig, ai4, false, 16384, (qg6) null);
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class MetaData {
        @DexIgnore
        public Integer selectedBackgroundColor;
        @DexIgnore
        public Integer selectedForegroundColor;
        @DexIgnore
        public Integer unselectedBackgroundColor;
        @DexIgnore
        public Integer unselectedForegroundColor;

        @DexIgnore
        public MetaData(Integer num, Integer num2, Integer num3, Integer num4) {
            this.selectedForegroundColor = num;
            this.selectedBackgroundColor = num2;
            this.unselectedForegroundColor = num3;
            this.unselectedBackgroundColor = num4;
        }

        @DexIgnore
        public static /* synthetic */ MetaData copy$default(MetaData metaData, Integer num, Integer num2, Integer num3, Integer num4, int i, Object obj) {
            if ((i & 1) != 0) {
                num = metaData.selectedForegroundColor;
            }
            if ((i & 2) != 0) {
                num2 = metaData.selectedBackgroundColor;
            }
            if ((i & 4) != 0) {
                num3 = metaData.unselectedForegroundColor;
            }
            if ((i & 8) != 0) {
                num4 = metaData.unselectedBackgroundColor;
            }
            return metaData.copy(num, num2, num3, num4);
        }

        @DexIgnore
        public final Integer component1() {
            return this.selectedForegroundColor;
        }

        @DexIgnore
        public final Integer component2() {
            return this.selectedBackgroundColor;
        }

        @DexIgnore
        public final Integer component3() {
            return this.unselectedForegroundColor;
        }

        @DexIgnore
        public final Integer component4() {
            return this.unselectedBackgroundColor;
        }

        @DexIgnore
        public final MetaData copy(Integer num, Integer num2, Integer num3, Integer num4) {
            return new MetaData(num, num2, num3, num4);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MetaData)) {
                return false;
            }
            MetaData metaData = (MetaData) obj;
            return wg6.a((Object) this.selectedForegroundColor, (Object) metaData.selectedForegroundColor) && wg6.a((Object) this.selectedBackgroundColor, (Object) metaData.selectedBackgroundColor) && wg6.a((Object) this.unselectedForegroundColor, (Object) metaData.unselectedForegroundColor) && wg6.a((Object) this.unselectedBackgroundColor, (Object) metaData.unselectedBackgroundColor);
        }

        @DexIgnore
        public final Integer getSelectedBackgroundColor() {
            return this.selectedBackgroundColor;
        }

        @DexIgnore
        public final Integer getSelectedForegroundColor() {
            return this.selectedForegroundColor;
        }

        @DexIgnore
        public final Integer getUnselectedBackgroundColor() {
            return this.unselectedBackgroundColor;
        }

        @DexIgnore
        public final Integer getUnselectedForegroundColor() {
            return this.unselectedForegroundColor;
        }

        @DexIgnore
        public int hashCode() {
            Integer num = this.selectedForegroundColor;
            int i = 0;
            int hashCode = (num != null ? num.hashCode() : 0) * 31;
            Integer num2 = this.selectedBackgroundColor;
            int hashCode2 = (hashCode + (num2 != null ? num2.hashCode() : 0)) * 31;
            Integer num3 = this.unselectedForegroundColor;
            int hashCode3 = (hashCode2 + (num3 != null ? num3.hashCode() : 0)) * 31;
            Integer num4 = this.unselectedBackgroundColor;
            if (num4 != null) {
                i = num4.hashCode();
            }
            return hashCode3 + i;
        }

        @DexIgnore
        public final void setSelectedBackgroundColor(Integer num) {
            this.selectedBackgroundColor = num;
        }

        @DexIgnore
        public final void setSelectedForegroundColor(Integer num) {
            this.selectedForegroundColor = num;
        }

        @DexIgnore
        public final void setUnselectedBackgroundColor(Integer num) {
            this.unselectedBackgroundColor = num;
        }

        @DexIgnore
        public final void setUnselectedForegroundColor(Integer num) {
            this.unselectedForegroundColor = num;
        }

        @DexIgnore
        public String toString() {
            return "MetaData(selectedForegroundColor=" + this.selectedForegroundColor + ", selectedBackgroundColor=" + this.selectedBackgroundColor + ", unselectedForegroundColor=" + this.unselectedForegroundColor + ", unselectedBackgroundColor=" + this.unselectedBackgroundColor + ")";
        }
    }

    @DexIgnore
    public WatchFaceWrapper(String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig2, ai4 ai4, boolean z) {
        String str3 = str2;
        BackgroundConfig backgroundConfig3 = backgroundConfig2;
        ai4 ai42 = ai4;
        wg6.b(str, "id");
        wg6.b(str3, "name");
        wg6.b(backgroundConfig3, "backgroundConfig");
        wg6.b(ai42, "type");
        this.id = str;
        this.combination = drawable;
        this.background = drawable2;
        this.topComplication = drawable3;
        this.rightComplication = drawable4;
        this.bottomComplication = drawable5;
        this.leftComplication = drawable6;
        this.topMetaData = metaData;
        this.rightMetaData = metaData2;
        this.bottomMetaData = metaData3;
        this.leftMetaData = metaData4;
        this.name = str3;
        this.backgroundConfig = backgroundConfig3;
        this.type = ai42;
        this.isRemoveIconVisible = z;
    }

    @DexIgnore
    public static /* synthetic */ WatchFaceWrapper copy$default(WatchFaceWrapper watchFaceWrapper, String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig2, ai4 ai4, boolean z, int i, Object obj) {
        WatchFaceWrapper watchFaceWrapper2 = watchFaceWrapper;
        int i2 = i;
        return watchFaceWrapper.copy((i2 & 1) != 0 ? watchFaceWrapper2.id : str, (i2 & 2) != 0 ? watchFaceWrapper2.combination : drawable, (i2 & 4) != 0 ? watchFaceWrapper2.background : drawable2, (i2 & 8) != 0 ? watchFaceWrapper2.topComplication : drawable3, (i2 & 16) != 0 ? watchFaceWrapper2.rightComplication : drawable4, (i2 & 32) != 0 ? watchFaceWrapper2.bottomComplication : drawable5, (i2 & 64) != 0 ? watchFaceWrapper2.leftComplication : drawable6, (i2 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? watchFaceWrapper2.topMetaData : metaData, (i2 & 256) != 0 ? watchFaceWrapper2.rightMetaData : metaData2, (i2 & 512) != 0 ? watchFaceWrapper2.bottomMetaData : metaData3, (i2 & 1024) != 0 ? watchFaceWrapper2.leftMetaData : metaData4, (i2 & 2048) != 0 ? watchFaceWrapper2.name : str2, (i2 & 4096) != 0 ? watchFaceWrapper2.backgroundConfig : backgroundConfig2, (i2 & 8192) != 0 ? watchFaceWrapper2.type : ai4, (i2 & 16384) != 0 ? watchFaceWrapper2.isRemoveIconVisible : z);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final MetaData component10() {
        return this.bottomMetaData;
    }

    @DexIgnore
    public final MetaData component11() {
        return this.leftMetaData;
    }

    @DexIgnore
    public final String component12() {
        return this.name;
    }

    @DexIgnore
    public final BackgroundConfig component13() {
        return this.backgroundConfig;
    }

    @DexIgnore
    public final ai4 component14() {
        return this.type;
    }

    @DexIgnore
    public final boolean component15() {
        return this.isRemoveIconVisible;
    }

    @DexIgnore
    public final Drawable component2() {
        return this.combination;
    }

    @DexIgnore
    public final Drawable component3() {
        return this.background;
    }

    @DexIgnore
    public final Drawable component4() {
        return this.topComplication;
    }

    @DexIgnore
    public final Drawable component5() {
        return this.rightComplication;
    }

    @DexIgnore
    public final Drawable component6() {
        return this.bottomComplication;
    }

    @DexIgnore
    public final Drawable component7() {
        return this.leftComplication;
    }

    @DexIgnore
    public final MetaData component8() {
        return this.topMetaData;
    }

    @DexIgnore
    public final MetaData component9() {
        return this.rightMetaData;
    }

    @DexIgnore
    public final WatchFaceWrapper copy(String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig2, ai4 ai4, boolean z) {
        String str3 = str;
        wg6.b(str3, "id");
        String str4 = str2;
        wg6.b(str4, "name");
        BackgroundConfig backgroundConfig3 = backgroundConfig2;
        wg6.b(backgroundConfig3, "backgroundConfig");
        ai4 ai42 = ai4;
        wg6.b(ai42, "type");
        return new WatchFaceWrapper(str3, drawable, drawable2, drawable3, drawable4, drawable5, drawable6, metaData, metaData2, metaData3, metaData4, str4, backgroundConfig3, ai42, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WatchFaceWrapper)) {
            return false;
        }
        WatchFaceWrapper watchFaceWrapper = (WatchFaceWrapper) obj;
        return wg6.a((Object) this.id, (Object) watchFaceWrapper.id) && wg6.a((Object) this.combination, (Object) watchFaceWrapper.combination) && wg6.a((Object) this.background, (Object) watchFaceWrapper.background) && wg6.a((Object) this.topComplication, (Object) watchFaceWrapper.topComplication) && wg6.a((Object) this.rightComplication, (Object) watchFaceWrapper.rightComplication) && wg6.a((Object) this.bottomComplication, (Object) watchFaceWrapper.bottomComplication) && wg6.a((Object) this.leftComplication, (Object) watchFaceWrapper.leftComplication) && wg6.a((Object) this.topMetaData, (Object) watchFaceWrapper.topMetaData) && wg6.a((Object) this.rightMetaData, (Object) watchFaceWrapper.rightMetaData) && wg6.a((Object) this.bottomMetaData, (Object) watchFaceWrapper.bottomMetaData) && wg6.a((Object) this.leftMetaData, (Object) watchFaceWrapper.leftMetaData) && wg6.a((Object) this.name, (Object) watchFaceWrapper.name) && wg6.a((Object) this.backgroundConfig, (Object) watchFaceWrapper.backgroundConfig) && wg6.a((Object) this.type, (Object) watchFaceWrapper.type) && this.isRemoveIconVisible == watchFaceWrapper.isRemoveIconVisible;
    }

    @DexIgnore
    public final Drawable getBackground() {
        return this.background;
    }

    @DexIgnore
    public final BackgroundConfig getBackgroundConfig() {
        return this.backgroundConfig;
    }

    @DexIgnore
    public final Drawable getBottomComplication() {
        return this.bottomComplication;
    }

    @DexIgnore
    public final MetaData getBottomMetaData() {
        return this.bottomMetaData;
    }

    @DexIgnore
    public final Drawable getCombination() {
        return this.combination;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final Drawable getLeftComplication() {
        return this.leftComplication;
    }

    @DexIgnore
    public final MetaData getLeftMetaData() {
        return this.leftMetaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final Drawable getRightComplication() {
        return this.rightComplication;
    }

    @DexIgnore
    public final MetaData getRightMetaData() {
        return this.rightMetaData;
    }

    @DexIgnore
    public final Drawable getTopComplication() {
        return this.topComplication;
    }

    @DexIgnore
    public final MetaData getTopMetaData() {
        return this.topMetaData;
    }

    @DexIgnore
    public final ai4 getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Drawable drawable = this.combination;
        int hashCode2 = (hashCode + (drawable != null ? drawable.hashCode() : 0)) * 31;
        Drawable drawable2 = this.background;
        int hashCode3 = (hashCode2 + (drawable2 != null ? drawable2.hashCode() : 0)) * 31;
        Drawable drawable3 = this.topComplication;
        int hashCode4 = (hashCode3 + (drawable3 != null ? drawable3.hashCode() : 0)) * 31;
        Drawable drawable4 = this.rightComplication;
        int hashCode5 = (hashCode4 + (drawable4 != null ? drawable4.hashCode() : 0)) * 31;
        Drawable drawable5 = this.bottomComplication;
        int hashCode6 = (hashCode5 + (drawable5 != null ? drawable5.hashCode() : 0)) * 31;
        Drawable drawable6 = this.leftComplication;
        int hashCode7 = (hashCode6 + (drawable6 != null ? drawable6.hashCode() : 0)) * 31;
        MetaData metaData = this.topMetaData;
        int hashCode8 = (hashCode7 + (metaData != null ? metaData.hashCode() : 0)) * 31;
        MetaData metaData2 = this.rightMetaData;
        int hashCode9 = (hashCode8 + (metaData2 != null ? metaData2.hashCode() : 0)) * 31;
        MetaData metaData3 = this.bottomMetaData;
        int hashCode10 = (hashCode9 + (metaData3 != null ? metaData3.hashCode() : 0)) * 31;
        MetaData metaData4 = this.leftMetaData;
        int hashCode11 = (hashCode10 + (metaData4 != null ? metaData4.hashCode() : 0)) * 31;
        String str2 = this.name;
        int hashCode12 = (hashCode11 + (str2 != null ? str2.hashCode() : 0)) * 31;
        BackgroundConfig backgroundConfig2 = this.backgroundConfig;
        int hashCode13 = (hashCode12 + (backgroundConfig2 != null ? backgroundConfig2.hashCode() : 0)) * 31;
        ai4 ai4 = this.type;
        if (ai4 != null) {
            i = ai4.hashCode();
        }
        int i2 = (hashCode13 + i) * 31;
        boolean z = this.isRemoveIconVisible;
        if (z) {
            z = true;
        }
        return i2 + (z ? 1 : 0);
    }

    @DexIgnore
    public final boolean isRemoveIconVisible() {
        return this.isRemoveIconVisible;
    }

    @DexIgnore
    public final void setBackground(Drawable drawable) {
        this.background = drawable;
    }

    @DexIgnore
    public final void setBackgroundConfig(BackgroundConfig backgroundConfig2) {
        wg6.b(backgroundConfig2, "<set-?>");
        this.backgroundConfig = backgroundConfig2;
    }

    @DexIgnore
    public final void setBottomComplication(Drawable drawable) {
        this.bottomComplication = drawable;
    }

    @DexIgnore
    public final void setBottomMetaData(MetaData metaData) {
        this.bottomMetaData = metaData;
    }

    @DexIgnore
    public final void setCombination(Drawable drawable) {
        this.combination = drawable;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setLeftComplication(Drawable drawable) {
        this.leftComplication = drawable;
    }

    @DexIgnore
    public final void setLeftMetaData(MetaData metaData) {
        this.leftMetaData = metaData;
    }

    @DexIgnore
    public final void setName(String str) {
        wg6.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setRemoveIconVisible(boolean z) {
        this.isRemoveIconVisible = z;
    }

    @DexIgnore
    public final void setRightComplication(Drawable drawable) {
        this.rightComplication = drawable;
    }

    @DexIgnore
    public final void setRightMetaData(MetaData metaData) {
        this.rightMetaData = metaData;
    }

    @DexIgnore
    public final void setTopComplication(Drawable drawable) {
        this.topComplication = drawable;
    }

    @DexIgnore
    public final void setTopMetaData(MetaData metaData) {
        this.topMetaData = metaData;
    }

    @DexIgnore
    public final void setType(ai4 ai4) {
        wg6.b(ai4, "<set-?>");
        this.type = ai4;
    }

    @DexIgnore
    public String toString() {
        return "WatchFaceWrapper(id=" + this.id + ", combination=" + this.combination + ", background=" + this.background + ", topComplication=" + this.topComplication + ", rightComplication=" + this.rightComplication + ", bottomComplication=" + this.bottomComplication + ", leftComplication=" + this.leftComplication + ", topMetaData=" + this.topMetaData + ", rightMetaData=" + this.rightMetaData + ", bottomMetaData=" + this.bottomMetaData + ", leftMetaData=" + this.leftMetaData + ", name=" + this.name + ", backgroundConfig=" + this.backgroundConfig + ", type=" + this.type + ", isRemoveIconVisible=" + this.isRemoveIconVisible + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchFaceWrapper(String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig2, ai4 ai4, boolean z, int i, qg6 qg6) {
        this(str, drawable, drawable2, drawable3, drawable4, drawable5, drawable6, metaData, metaData2, metaData3, metaData4, str2, backgroundConfig2, ai4, (i & 16384) != 0 ? false : z);
    }
}
