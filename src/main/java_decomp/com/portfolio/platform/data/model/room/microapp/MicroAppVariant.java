package com.portfolio.platform.data.model.room.microapp;

import com.fossil.d;
import com.fossil.vu3;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import java.util.ArrayList;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariant {
    @DexIgnore
    @vu3("appId")
    public String appId;
    @DexIgnore
    @vu3("createdAt")
    public Date createdAt;
    @DexIgnore
    @vu3("declarationFiles")
    public ArrayList<DeclarationFile> declarationFileList; // = new ArrayList<>();
    @DexIgnore
    @vu3("description")
    public String description;
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    @vu3("majorNumber")
    public int majorNumber;
    @DexIgnore
    @vu3("minorNumber")
    public int minorNumber;
    @DexIgnore
    @vu3("name")
    public String name;
    @DexIgnore
    @vu3("serialNumber")
    public String serialNumber;
    @DexIgnore
    @vu3("updatedAt")
    public Date updatedAt;

    @DexIgnore
    public MicroAppVariant(String str, String str2, String str3, String str4, Date date, Date date2, int i, int i2, String str5) {
        wg6.b(str, "id");
        wg6.b(str2, "appId");
        wg6.b(str3, "name");
        wg6.b(str4, "description");
        wg6.b(date, "createdAt");
        wg6.b(date2, "updatedAt");
        wg6.b(str5, "serialNumber");
        this.id = str;
        this.appId = str2;
        this.name = str3;
        this.description = str4;
        this.createdAt = date;
        this.updatedAt = date2;
        this.majorNumber = i;
        this.minorNumber = i2;
        this.serialNumber = str5;
    }

    @DexIgnore
    public static /* synthetic */ MicroAppVariant copy$default(MicroAppVariant microAppVariant, String str, String str2, String str3, String str4, Date date, Date date2, int i, int i2, String str5, int i3, Object obj) {
        MicroAppVariant microAppVariant2 = microAppVariant;
        int i4 = i3;
        return microAppVariant.copy((i4 & 1) != 0 ? microAppVariant2.id : str, (i4 & 2) != 0 ? microAppVariant2.appId : str2, (i4 & 4) != 0 ? microAppVariant2.name : str3, (i4 & 8) != 0 ? microAppVariant2.description : str4, (i4 & 16) != 0 ? microAppVariant2.createdAt : date, (i4 & 32) != 0 ? microAppVariant2.updatedAt : date2, (i4 & 64) != 0 ? microAppVariant2.majorNumber : i, (i4 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? microAppVariant2.minorNumber : i2, (i4 & 256) != 0 ? microAppVariant2.serialNumber : str5);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.appId;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final String component4() {
        return this.description;
    }

    @DexIgnore
    public final Date component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date component6() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component7() {
        return this.majorNumber;
    }

    @DexIgnore
    public final int component8() {
        return this.minorNumber;
    }

    @DexIgnore
    public final String component9() {
        return this.serialNumber;
    }

    @DexIgnore
    public final MicroAppVariant copy(String str, String str2, String str3, String str4, Date date, Date date2, int i, int i2, String str5) {
        wg6.b(str, "id");
        wg6.b(str2, "appId");
        wg6.b(str3, "name");
        wg6.b(str4, "description");
        Date date3 = date;
        wg6.b(date3, "createdAt");
        Date date4 = date2;
        wg6.b(date4, "updatedAt");
        String str6 = str5;
        wg6.b(str6, "serialNumber");
        return new MicroAppVariant(str, str2, str3, str4, date3, date4, i, i2, str6);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MicroAppVariant)) {
            return false;
        }
        MicroAppVariant microAppVariant = (MicroAppVariant) obj;
        return wg6.a((Object) this.id, (Object) microAppVariant.id) && wg6.a((Object) this.appId, (Object) microAppVariant.appId) && wg6.a((Object) this.name, (Object) microAppVariant.name) && wg6.a((Object) this.description, (Object) microAppVariant.description) && wg6.a((Object) this.createdAt, (Object) microAppVariant.createdAt) && wg6.a((Object) this.updatedAt, (Object) microAppVariant.updatedAt) && this.majorNumber == microAppVariant.majorNumber && this.minorNumber == microAppVariant.minorNumber && wg6.a((Object) this.serialNumber, (Object) microAppVariant.serialNumber);
    }

    @DexIgnore
    public final String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public final Date getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final ArrayList<DeclarationFile> getDeclarationFileList() {
        return this.declarationFileList;
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getMajorNumber() {
        return this.majorNumber;
    }

    @DexIgnore
    public final int getMinorNumber() {
        return this.minorNumber;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final Date getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.appId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.description;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Date date = this.createdAt;
        int hashCode5 = (hashCode4 + (date != null ? date.hashCode() : 0)) * 31;
        Date date2 = this.updatedAt;
        int hashCode6 = (((((hashCode5 + (date2 != null ? date2.hashCode() : 0)) * 31) + d.a(this.majorNumber)) * 31) + d.a(this.minorNumber)) * 31;
        String str5 = this.serialNumber;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode6 + i;
    }

    @DexIgnore
    public final void setAppId(String str) {
        wg6.b(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setCreatedAt(Date date) {
        wg6.b(date, "<set-?>");
        this.createdAt = date;
    }

    @DexIgnore
    public final void setDeclarationFileList(ArrayList<DeclarationFile> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.declarationFileList = arrayList;
    }

    @DexIgnore
    public final void setDescription(String str) {
        wg6.b(str, "<set-?>");
        this.description = str;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMajorNumber(int i) {
        this.majorNumber = i;
    }

    @DexIgnore
    public final void setMinorNumber(int i) {
        this.minorNumber = i;
    }

    @DexIgnore
    public final void setName(String str) {
        wg6.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        wg6.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(Date date) {
        wg6.b(date, "<set-?>");
        this.updatedAt = date;
    }

    @DexIgnore
    public String toString() {
        return "MicroAppVariant(id=" + this.id + ", appId=" + this.appId + ", name=" + this.name + ", description=" + this.description + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", majorNumber=" + this.majorNumber + ", minorNumber=" + this.minorNumber + ", serialNumber=" + this.serialNumber + ")";
    }
}
