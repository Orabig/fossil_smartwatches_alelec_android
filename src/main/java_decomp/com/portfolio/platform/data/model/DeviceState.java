package com.portfolio.platform.data.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum DeviceState {
    Connected,
    Disconnected,
    RecoveryModeStart,
    RecoveryModeEnd,
    Shutdown,
    FirmwareUpdate,
    NoChange
}
