package com.portfolio.platform.data.model;

import com.fossil.qg6;
import com.fossil.vu3;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "SERVER_SETTING")
public final class ServerSetting {
    @DexIgnore
    public static /* final */ String CREATE_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String KEY; // = "key";
    @DexIgnore
    public static /* final */ String OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "SERVER_SETTING";
    @DexIgnore
    public static /* final */ String UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String VALUE; // = "value";
    @DexIgnore
    @vu3("createdAt")
    @DatabaseField(columnName = "createdAt")
    public String createdAt;
    @DexIgnore
    @vu3("key")
    @DatabaseField(columnName = "key", id = true)
    public String key;
    @DexIgnore
    @vu3("id")
    @DatabaseField(columnName = "objectId")
    public String objectId;
    @DexIgnore
    @vu3("updatedAt")
    @DatabaseField(columnName = "updatedAt")
    public String updateAt;
    @DexIgnore
    @vu3("value")
    @DatabaseField(columnName = "value")
    public String value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getKey() {
        return this.key;
    }

    @DexIgnore
    public final String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public final String getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setKey(String str) {
        this.key = str;
    }

    @DexIgnore
    public final void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public final void setUpdateAt(String str) {
        this.updateAt = str;
    }

    @DexIgnore
    public final void setValue(String str) {
        this.value = str;
    }

    @DexIgnore
    public String toString() {
        return "key: " + this.key + " - value: " + this.value;
    }
}
