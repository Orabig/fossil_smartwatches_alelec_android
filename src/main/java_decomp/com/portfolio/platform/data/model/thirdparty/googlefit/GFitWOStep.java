package com.portfolio.platform.data.model.thirdparty.googlefit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWOStep {
    @DexIgnore
    public long endTime;
    @DexIgnore
    public long startTime;
    @DexIgnore
    public int step;

    @DexIgnore
    public GFitWOStep(int i, long j, long j2) {
        this.step = i;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final int getStep() {
        return this.step;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public final void setStep(int i) {
        this.step = i;
    }
}
