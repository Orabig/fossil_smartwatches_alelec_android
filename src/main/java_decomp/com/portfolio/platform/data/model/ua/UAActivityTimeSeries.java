package com.portfolio.platform.data.model.ua;

import com.fossil.fu3;
import com.fossil.ie6;
import com.fossil.ku3;
import com.fossil.nu3;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xj6;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UAActivityTimeSeries {
    @DexIgnore
    public Map<Long, Double> caloriesMap;
    @DexIgnore
    public Map<Long, Double> distanceMap;
    @DexIgnore
    public ku3 extraJsonObject;
    @DexIgnore
    public String mExternalId;
    @DexIgnore
    public Map<Long, Integer> stepsMap;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Builder {
        @DexIgnore
        public Map<Long, Double> caloriesMap;
        @DexIgnore
        public Map<Long, Double> distanceMap;
        @DexIgnore
        public String mExternalId;
        @DexIgnore
        public Map<Long, Integer> stepsMap;

        @DexIgnore
        public final void addCalories(long j, double d) {
            if (this.caloriesMap == null) {
                this.caloriesMap = new HashMap();
            }
            Map<Long, Double> map = this.caloriesMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addDistance(long j, double d) {
            if (this.distanceMap == null) {
                this.distanceMap = new HashMap();
            }
            Map<Long, Double> map = this.distanceMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addSteps(long j, int i) {
            if (this.stepsMap == null) {
                this.stepsMap = new HashMap();
            }
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Integer.valueOf(i));
                return;
            }
            throw new rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
        }

        @DexIgnore
        public final UAActivityTimeSeries build() {
            if (this.mExternalId == null) {
                this.mExternalId = UUID.randomUUID().toString();
            }
            return new UAActivityTimeSeries(this);
        }

        @DexIgnore
        public final Map<Long, Double> getCaloriesMap() {
            return this.caloriesMap;
        }

        @DexIgnore
        public final Map<Long, Double> getDistanceMap() {
            return this.distanceMap;
        }

        @DexIgnore
        public final String getMExternalId() {
            return this.mExternalId;
        }

        @DexIgnore
        public final Map<Long, Integer> getStepsMap() {
            return this.stepsMap;
        }

        @DexIgnore
        public final void setCaloriesMap(Map<Long, Double> map) {
            this.caloriesMap = map;
        }

        @DexIgnore
        public final void setDistanceMap(Map<Long, Double> map) {
            this.distanceMap = map;
        }

        @DexIgnore
        public final void setExternalId(String str) {
            wg6.b(str, Constants.PROFILE_KEY_EXTERNALID);
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setMExternalId(String str) {
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setStepsMap(Map<Long, Integer> map) {
            this.stepsMap = map;
        }
    }

    @DexIgnore
    public UAActivityTimeSeries(String str, Map<Long, Integer> map, Map<Long, Double> map2, Map<Long, Double> map3) {
        wg6.b(str, "mExternalId");
        this.mExternalId = str;
        this.stepsMap = map;
        this.distanceMap = map2;
        this.caloriesMap = map3;
    }

    @DexIgnore
    public final ku3 getExtraJsonObject() {
        return this.extraJsonObject;
    }

    @DexIgnore
    public final ku3 getJsonData() {
        ku3 ku3 = new ku3();
        ku3.a("external_id", new nu3(this.mExternalId));
        ku3 ku32 = new ku3();
        if (this.stepsMap != null) {
            ku3 ku33 = new ku3();
            ku33.a("interval", new nu3(100));
            Gson gson = new Gson();
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                ku33.a("values", (JsonElement) gson.a(xj6.a(xj6.a(ie6.a((HashMap) map).toString(), "(", "[", false), ")", "]", false), fu3.class));
                ku32.a("steps", ku33);
            } else {
                throw new rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
            }
        }
        if (this.distanceMap != null) {
            ku3 ku34 = new ku3();
            ku34.a("interval", new nu3(100));
            Gson gson2 = new Gson();
            Map<Long, Double> map2 = this.distanceMap;
            if (map2 != null) {
                ku34.a("values", (JsonElement) gson2.a(xj6.a(xj6.a(ie6.a((HashMap) map2).toString(), "(", "[", false), ")", "]", false), fu3.class));
                ku32.a("distance", ku34);
            } else {
                throw new rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        if (this.caloriesMap != null) {
            ku3 ku35 = new ku3();
            ku35.a("interval", new nu3(100));
            Gson gson3 = new Gson();
            Map<Long, Double> map3 = this.caloriesMap;
            if (map3 != null) {
                ku35.a("values", (JsonElement) gson3.a(xj6.a(xj6.a(ie6.a((HashMap) map3).toString(), "(", "[", false), ")", "]", false), fu3.class));
                ku32.a("energy_expended", ku35);
            } else {
                throw new rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        ku3.a("time_series", ku32);
        if (this.extraJsonObject != null) {
            fu3 fu3 = new fu3();
            fu3.a(this.extraJsonObject);
            ku3 ku36 = new ku3();
            ku36.a("data_source", fu3);
            ku3.a("_links", ku36);
        }
        return ku3;
    }

    @DexIgnore
    public final void setExtraJsonObject(ku3 ku3) {
        this.extraJsonObject = ku3;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public UAActivityTimeSeries(Builder builder) {
        this(r0, builder.getStepsMap(), builder.getDistanceMap(), builder.getCaloriesMap());
        wg6.b(builder, "builder");
        String mExternalId2 = builder.getMExternalId();
        if (mExternalId2 != null) {
        } else {
            wg6.a();
            throw null;
        }
    }
}
