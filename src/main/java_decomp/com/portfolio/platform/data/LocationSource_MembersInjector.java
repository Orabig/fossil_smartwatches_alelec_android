package com.portfolio.platform.data;

import com.portfolio.platform.data.source.local.AddressDao;
import dagger.MembersInjector;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource_MembersInjector implements MembersInjector<LocationSource> {
    @DexIgnore
    public /* final */ Provider<AddressDao> addressDaoProvider;

    @DexIgnore
    public LocationSource_MembersInjector(Provider<AddressDao> provider) {
        this.addressDaoProvider = provider;
    }

    @DexIgnore
    public static MembersInjector<LocationSource> create(Provider<AddressDao> provider) {
        return new LocationSource_MembersInjector(provider);
    }

    @DexIgnore
    public static void injectAddressDao(LocationSource locationSource, AddressDao addressDao) {
        locationSource.addressDao = addressDao;
    }

    @DexIgnore
    public void injectMembers(LocationSource locationSource) {
        injectAddressDao(locationSource, this.addressDaoProvider.get());
    }
}
