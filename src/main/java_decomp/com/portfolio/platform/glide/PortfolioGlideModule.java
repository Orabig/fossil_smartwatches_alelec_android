package com.portfolio.platform.glide;

import android.content.Context;
import com.fossil.dr;
import com.fossil.fj4;
import com.fossil.gj4;
import com.fossil.hj4;
import com.fossil.ij4;
import com.fossil.nj4;
import com.fossil.oj4;
import com.fossil.uy;
import com.fossil.wg6;
import com.fossil.wq;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioGlideModule extends uy {
    @DexIgnore
    public void a(Context context, wq wqVar, dr drVar) {
        wg6.b(context, "context");
        wg6.b(wqVar, "glide");
        wg6.b(drVar, "registry");
        PortfolioGlideModule.super.a(context, wqVar, drVar);
        drVar.a(gj4.class, InputStream.class, new fj4.b());
        drVar.a(ij4.class, InputStream.class, new hj4.b());
        drVar.a(oj4.class, InputStream.class, new nj4.b());
    }

    @DexIgnore
    public boolean a() {
        return false;
    }
}
