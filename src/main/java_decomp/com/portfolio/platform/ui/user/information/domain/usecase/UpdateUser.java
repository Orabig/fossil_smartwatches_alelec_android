package com.portfolio.platform.ui.user.information.domain.usecase;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ft4;
import com.fossil.jf6;
import com.fossil.kc6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateUser extends m24<ft4.b, ft4.d, ft4.c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public b(MFUser mFUser) {
            wg6.b(mFUser, "mfUser");
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public d(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser", f = "UpdateUser.kt", l = {22}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateUser this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(UpdateUser updateUser, xe6 xe6) {
            super(xe6);
            this.this$0 = updateUser;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((ft4.b) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = UpdateUser.class.getSimpleName();
        wg6.a((Object) simpleName, "UpdateUser::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public UpdateUser(UserRepository userRepository) {
        wg6.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object a(ft4.b bVar, xe6<Object> xe6) {
        e eVar;
        int i;
        ap4 ap4;
        String str;
        String userMessage;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(e, "running UseCase");
                    if (bVar == null) {
                        return new c(600, "");
                    }
                    UserRepository userRepository = this.d;
                    MFUser a3 = bVar.a();
                    eVar.L$0 = this;
                    eVar.L$1 = bVar;
                    eVar.label = 1;
                    obj = userRepository.updateUser(a3, true, eVar);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    b bVar2 = (b) eVar.L$1;
                    UpdateUser updateUser = (UpdateUser) eVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    PortfolioApp instance = PortfolioApp.get.instance();
                    UserProfile j = PortfolioApp.get.instance().j();
                    if (j != null) {
                        instance.a(j);
                        return new d((MFUser) ((cp4) ap4).a());
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    int a4 = zo4.a();
                    ServerError c2 = zo4.c();
                    if (c2 == null || (userMessage = c2.getUserMessage()) == null) {
                        ServerError c3 = zo4.c();
                        str = c3 != null ? c3.getMessage() : null;
                    } else {
                        str = userMessage;
                    }
                    if (str == null) {
                        str = "";
                    }
                    return new c(a4, str);
                } else {
                    throw new kc6();
                }
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
