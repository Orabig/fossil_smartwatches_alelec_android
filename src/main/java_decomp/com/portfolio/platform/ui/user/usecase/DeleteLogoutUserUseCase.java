package com.portfolio.platform.ui.user.usecase;

import android.app.Activity;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ht4;
import com.fossil.ht4$f$a;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.in4;
import com.fossil.jf6;
import com.fossil.kk4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.lp4;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.qx5;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.fossil.zm4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeleteLogoutUserUseCase extends m24<ht4.b, ht4.d, ht4.c> {
    @DexIgnore
    public static /* final */ String K;
    @DexIgnore
    public static /* final */ a L; // = new a((qg6) null);
    @DexIgnore
    public /* final */ FitnessDataRepository A;
    @DexIgnore
    public /* final */ HeartRateSampleRepository B;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository C;
    @DexIgnore
    public /* final */ WorkoutSessionRepository D;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase E;
    @DexIgnore
    public /* final */ ThirdPartyDatabase F;
    @DexIgnore
    public /* final */ RingStyleRepository G;
    @DexIgnore
    public /* final */ FileRepository H;
    @DexIgnore
    public /* final */ QuickResponseRepository I;
    @DexIgnore
    public /* final */ WatchFaceRepository J;
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ AlarmsRepository e;
    @DexIgnore
    public /* final */ an4 f;
    @DexIgnore
    public /* final */ HybridPresetRepository g;
    @DexIgnore
    public /* final */ ActivitiesRepository h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ MicroAppSettingRepository j;
    @DexIgnore
    public /* final */ NotificationsRepository k;
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ SleepSessionsRepository m;
    @DexIgnore
    public /* final */ GoalTrackingRepository n;
    @DexIgnore
    public /* final */ in4 o;
    @DexIgnore
    public /* final */ kk4 p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;
    @DexIgnore
    public /* final */ DianaPresetRepository r;
    @DexIgnore
    public /* final */ WatchAppRepository s;
    @DexIgnore
    public /* final */ ComplicationRepository t;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;
    @DexIgnore
    public /* final */ MicroAppRepository w;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository x;
    @DexIgnore
    public /* final */ ComplicationLastSettingRepository y;
    @DexIgnore
    public /* final */ WatchAppLastSettingRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DeleteLogoutUserUseCase.K;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ WeakReference<Activity> b;

        @DexIgnore
        public b(int i, WeakReference<Activity> weakReference) {
            this.a = i;
            this.b = weakReference;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final WeakReference<Activity> b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1", f = "DeleteLogoutUserUseCase.kt", l = {171, 197}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteLogoutUserUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(DeleteLogoutUserUseCase deleteLogoutUserUseCase, xe6 xe6) {
            super(2, xe6);
            this.this$0 = deleteLogoutUserUseCase;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r9v7, types: [com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase, com.portfolio.platform.CoroutineUseCase] */
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                FLogger.INSTANCE.getRemote().flush();
                try {
                    qx5.b.f(ButtonService.DEVICE_SECRET_KEY);
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = DeleteLogoutUserUseCase.L.a();
                    local.e(a2, "exception when remove alias in keystore " + e);
                }
                this.this$0.f.a(false);
                this.this$0.k.clearAllNotificationSetting();
                this.this$0.m.cleanUp();
                this.this$0.q.cleanUp();
                this.this$0.A.cleanUp();
                this.this$0.f.X();
                this.this$0.d.clearAllUser();
                PortfolioApp.get.instance().V();
                DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (deleteLogoutUserUseCase.a((xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                this.this$0.J.cleanUp();
                ThirdPartyDatabase z = this.this$0.F;
                z.getGFitSampleDao().clearAll();
                z.getGFitActiveTimeDao().clearAll();
                z.getGFitWorkoutSessionDao().clearAll();
                z.getUASampleDao().clearAll();
                this.this$0.d();
                this.this$0.a(new d());
                this.this$0.l.cleanUp();
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.x.cleanUp();
            this.this$0.y.cleanUp();
            this.this$0.z.cleanUp();
            this.this$0.g.cleanUp();
            this.this$0.w.cleanUp();
            this.this$0.h.cleanUp();
            this.this$0.i.cleanUp();
            this.this$0.n.cleanUp();
            this.this$0.r.cleanUp();
            this.this$0.s.cleanUp();
            this.this$0.t.cleanUp();
            this.this$0.j.clearData();
            this.this$0.e.cleanUp();
            zm4.p.a().o();
            lp4.g.a();
            PortfolioApp.get.instance().c();
            PortfolioApp.get.instance().H();
            this.this$0.u.getNotificationSettingsDao().delete();
            this.this$0.v.getDNDScheduledTimeDao().delete();
            this.this$0.E.getInactivityNudgeTimeDao().delete();
            this.this$0.E.getRemindTimeDao().delete();
            this.this$0.B.cleanUp();
            this.this$0.C.cleanUp();
            this.this$0.D.cleanUp();
            this.this$0.G.cleanUp();
            FileRepository h = this.this$0.H;
            this.L$0 = il6;
            this.label = 2;
            if (h.cleanUp(this) == a) {
                return a;
            }
            this.this$0.J.cleanUp();
            ThirdPartyDatabase z2 = this.this$0.F;
            z2.getGFitSampleDao().clearAll();
            z2.getGFitActiveTimeDao().clearAll();
            z2.getGFitWorkoutSessionDao().clearAll();
            z2.getUASampleDao().clearAll();
            this.this$0.d();
            this.this$0.a(new d());
            this.this$0.l.cleanUp();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$resetQuickResponseMessage$1", f = "DeleteLogoutUserUseCase.kt", l = {131}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteLogoutUserUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DeleteLogoutUserUseCase deleteLogoutUserUseCase, xe6 xe6) {
            super(2, xe6);
            this.this$0 = deleteLogoutUserUseCase;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                ht4$f$a ht4_f_a = new ht4$f$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, ht4_f_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f.b(hf6.a(false));
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase", f = "DeleteLogoutUserUseCase.kt", l = {83, 102}, m = "run")
    public static final class g extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteLogoutUserUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(DeleteLogoutUserUseCase deleteLogoutUserUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = deleteLogoutUserUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((ht4.b) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        String simpleName = DeleteLogoutUserUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "DeleteLogoutUserUseCase::class.java.simpleName");
        K = simpleName;
    }
    */

    @DexIgnore
    public DeleteLogoutUserUseCase(UserRepository userRepository, AlarmsRepository alarmsRepository, an4 an4, HybridPresetRepository hybridPresetRepository, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, MicroAppSettingRepository microAppSettingRepository, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, SleepSessionsRepository sleepSessionsRepository, GoalTrackingRepository goalTrackingRepository, in4 in4, kk4 kk4, SleepSummariesRepository sleepSummariesRepository, DianaPresetRepository dianaPresetRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, NotificationSettingsDatabase notificationSettingsDatabase, DNDSettingsDatabase dNDSettingsDatabase, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, FitnessDataRepository fitnessDataRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, RemindersSettingsDatabase remindersSettingsDatabase, ThirdPartyDatabase thirdPartyDatabase, RingStyleRepository ringStyleRepository, FileRepository fileRepository, QuickResponseRepository quickResponseRepository, WatchFaceRepository watchFaceRepository, PortfolioApp portfolioApp) {
        UserRepository userRepository2 = userRepository;
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        an4 an42 = an4;
        HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        ActivitiesRepository activitiesRepository2 = activitiesRepository;
        SummariesRepository summariesRepository2 = summariesRepository;
        MicroAppSettingRepository microAppSettingRepository2 = microAppSettingRepository;
        NotificationsRepository notificationsRepository2 = notificationsRepository;
        DeviceRepository deviceRepository2 = deviceRepository;
        SleepSessionsRepository sleepSessionsRepository2 = sleepSessionsRepository;
        GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        in4 in42 = in4;
        kk4 kk42 = kk4;
        SleepSummariesRepository sleepSummariesRepository2 = sleepSummariesRepository;
        WatchAppRepository watchAppRepository2 = watchAppRepository;
        wg6.b(userRepository2, "mUserRepository");
        wg6.b(alarmsRepository2, "mAlarmRepository");
        wg6.b(an42, "mSharedPreferences");
        wg6.b(hybridPresetRepository2, "mPresetRepository");
        wg6.b(activitiesRepository2, "mActivitiesRepository");
        wg6.b(summariesRepository2, "mSummariesRepository");
        wg6.b(microAppSettingRepository2, "mMicroAppSettingRepository");
        wg6.b(notificationsRepository2, "mNotificationRepository");
        wg6.b(deviceRepository2, "mDeviceRepository");
        wg6.b(sleepSessionsRepository2, "mSleepSessionsRepository");
        wg6.b(goalTrackingRepository2, "mGoalTrackingRepository");
        wg6.b(in42, "mLoginGoogleManager");
        wg6.b(kk42, "mGoogleFitHelper");
        wg6.b(sleepSummariesRepository2, "mSleepSummariesRepository");
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        wg6.b(watchAppRepository, "mWatchAppRepository");
        wg6.b(complicationRepository, "mComplicationRepository");
        wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        wg6.b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        wg6.b(microAppRepository, "mMicroAppRepository");
        wg6.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        wg6.b(complicationLastSettingRepository, "mComplicationLastSettingRepository");
        wg6.b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        wg6.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        wg6.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        wg6.b(workoutSessionRepository, "mWorkoutSessionRepository");
        wg6.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        wg6.b(thirdPartyDatabase, "mThirdPartyDatabase");
        wg6.b(ringStyleRepository, "mRingStyleRepository");
        wg6.b(fileRepository, "mFileRepository");
        wg6.b(quickResponseRepository, "mQuickResponseRepository");
        wg6.b(watchFaceRepository, "mWatchFaceRepository");
        wg6.b(portfolioApp, "mApp");
        this.d = userRepository2;
        this.e = alarmsRepository2;
        this.f = an42;
        this.g = hybridPresetRepository2;
        this.h = activitiesRepository2;
        this.i = summariesRepository2;
        this.j = microAppSettingRepository2;
        this.k = notificationsRepository2;
        this.l = deviceRepository2;
        this.m = sleepSessionsRepository2;
        this.n = goalTrackingRepository2;
        this.o = in42;
        this.p = kk42;
        this.q = sleepSummariesRepository2;
        this.r = dianaPresetRepository;
        this.s = watchAppRepository;
        this.t = complicationRepository;
        this.u = notificationSettingsDatabase;
        this.v = dNDSettingsDatabase;
        this.w = microAppRepository;
        this.x = microAppLastSettingRepository;
        this.y = complicationLastSettingRepository;
        this.z = watchAppLastSettingRepository;
        this.A = fitnessDataRepository;
        this.B = heartRateSampleRepository;
        this.C = heartRateSummaryRepository;
        this.D = workoutSessionRepository;
        this.E = remindersSettingsDatabase;
        this.F = thirdPartyDatabase;
        this.G = ringStyleRepository;
        this.H = fileRepository;
        this.I = quickResponseRepository;
        this.J = watchFaceRepository;
    }

    @DexIgnore
    public String c() {
        return K;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final void d() {
        rm6 unused = ik6.b(b(), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r0v13 */
    /* JADX WARNING: type inference failed for: r0v14 */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0119  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    public Object a(b bVar, xe6<Object> xe6) {
        g gVar;
        int i2;
        Object r0;
        int intValue;
        Object r02;
        int intValue2;
        if (xe6 instanceof g) {
            gVar = (g) xe6;
            int i3 = gVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                gVar.label = i3 - Integer.MIN_VALUE;
                Object obj = gVar.result;
                Object a2 = ff6.a();
                i2 = gVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = K;
                    StringBuilder sb = new StringBuilder();
                    sb.append("running UseCase with mode=");
                    sb.append(bVar != null ? hf6.a(bVar.a()) : null);
                    local.d(str, sb.toString());
                    if (bVar != null && bVar.a() == 0) {
                        MFUser currentUser = this.d.getCurrentUser();
                        if (currentUser != null) {
                            UserRepository userRepository = this.d;
                            gVar.L$0 = this;
                            gVar.L$1 = bVar;
                            gVar.L$2 = currentUser;
                            gVar.label = 1;
                            obj = userRepository.deleteUser(currentUser, gVar);
                            if (obj == a2) {
                                return a2;
                            }
                            r02 = this;
                        }
                        return new Object();
                    } else if (bVar == null || bVar.a() != 1) {
                        if (bVar != null && bVar.a() == 2) {
                            if (this.d.getCurrentUser() != null) {
                                a((b) bVar);
                            } else {
                                a(new c(600));
                            }
                        }
                        return new Object();
                    } else {
                        UserRepository userRepository2 = this.d;
                        gVar.L$0 = this;
                        gVar.L$1 = bVar;
                        gVar.label = 2;
                        obj = userRepository2.logoutUser(gVar);
                        if (obj == a2) {
                            return a2;
                        }
                        r0 = this;
                        intValue = ((Number) obj).intValue();
                        if (intValue != 200) {
                        }
                        return new Object();
                    }
                } else if (i2 == 1) {
                    MFUser mFUser = (MFUser) gVar.L$2;
                    bVar = (b) gVar.L$1;
                    nc6.a(obj);
                    r02 = (DeleteLogoutUserUseCase) gVar.L$0;
                } else if (i2 == 2) {
                    bVar = (b) gVar.L$1;
                    nc6.a(obj);
                    r0 = (DeleteLogoutUserUseCase) gVar.L$0;
                    intValue = ((Number) obj).intValue();
                    if (intValue != 200) {
                        r0.a(bVar);
                    } else {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = K;
                        local2.d(str2, "Inside .run failed with http code=" + intValue);
                        if (intValue == 401 || intValue == 404) {
                            r0.a(bVar);
                        } else {
                            r0.a(new c(intValue));
                        }
                    }
                    return new Object();
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                intValue2 = ((Number) obj).intValue();
                if (intValue2 != 200) {
                    AnalyticsHelper.f.c().b("remove_user", Constants.RESULT, "");
                    r02.a(bVar);
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = K;
                    local3.d(str3, "Inside .run failed with http code=" + intValue2);
                    AnalyticsHelper.f.c().b("remove_user", Constants.RESULT, String.valueOf(intValue2));
                    if (intValue2 == 401 || intValue2 == 404) {
                        r02.a(bVar);
                    } else {
                        r02.a(new c(intValue2));
                    }
                }
                return new Object();
            }
        }
        gVar = new g(this, xe6);
        Object obj2 = gVar.result;
        Object a22 = ff6.a();
        i2 = gVar.label;
        if (i2 != 0) {
        }
        intValue2 = ((Number) obj2).intValue();
        if (intValue2 != 200) {
        }
        return new Object();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final void a(b bVar) {
        this.o.a(bVar.b());
        try {
            IButtonConnectivity b2 = PortfolioApp.get.b();
            if (b2 != null) {
                b2.deviceUnlink(PortfolioApp.get.instance().e());
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        rm6 unused = ik6.b(b(), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final /* synthetic */ Object a(xe6<? super cd6> xe6) {
        if (this.p.e()) {
            this.p.h();
        }
        return cd6.a;
    }
}
