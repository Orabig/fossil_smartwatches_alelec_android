package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.util.AttributeSet;
import com.fossil.jm4;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.tk4;
import com.fossil.wg6;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewSleepWeekChart extends OverviewWeekChart {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public OverviewSleepWeekChart(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public float a(float f) {
        return ((f - (getMBarWidth() * ((float) getMNumberBar()))) - ((float) 20)) / ((float) (getMNumberBar() - 1));
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public String a(int i) {
        nh6 nh6 = nh6.a;
        String a2 = jm4.a(getContext(), 2131886434);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026eep7days_Label__NumberHr)");
        Object[] objArr = {tk4.a(((float) i) / ((float) 60), 1).toString()};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepWeekChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }
}
