package com.portfolio.platform.ui.view.chart.base;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseChart extends ViewGroup {
    @DexIgnore
    public static /* final */ b t; // = new b((qg6) null);
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public c o;
    @DexIgnore
    public e p;
    @DexIgnore
    public a q;
    @DexIgnore
    public d r;
    @DexIgnore
    public /* final */ String s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BaseChart baseChart, Context context) {
            super(context);
            wg6.b(context, "context");
            this.a = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            wg6.b(canvas, "canvas");
            super.onDraw(canvas);
            this.a.a(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.a.a(i, i2, i3, i4);
        }

        @DexIgnore
        public boolean performClick() {
            return super.performClick();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final boolean a(PointF pointF, RectF rectF) {
            wg6.b(pointF, "point");
            wg6.b(rectF, "rect");
            float f = pointF.x;
            if (f <= rectF.right && f >= rectF.left) {
                float f2 = pointF.y;
                return f2 <= rectF.bottom && f2 >= rectF.top;
            }
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(BaseChart baseChart, Context context) {
            super(context);
            wg6.b(context, "context");
            this.a = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            wg6.b(canvas, "canvas");
            super.onDraw(canvas);
            this.a.b(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.a.setMGraphWidth(i);
            this.a.setMGraphHeight(i2);
            this.a.c(i, i2, i3, i4);
        }

        @DexIgnore
        public boolean performClick() {
            return super.performClick();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(BaseChart baseChart, Context context) {
            super(context);
            wg6.b(context, "context");
            this.a = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            wg6.b(canvas, "canvas");
            super.onDraw(canvas);
            this.a.c(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.a.b(i, i2, i3, i4);
        }

        @DexIgnore
        public boolean onTouchEvent(MotionEvent motionEvent) {
            wg6.b(motionEvent, Constants.EVENT);
            return this.a.a(motionEvent);
        }

        @DexIgnore
        public boolean performClick() {
            return super.performClick();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(BaseChart baseChart, Context context) {
            super(context);
            wg6.b(context, "context");
            this.a = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            wg6.b(canvas, "canvas");
            super.onDraw(canvas);
            this.a.d(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.a.setMLegendWidth(i);
            this.a.d(i, i2, i3, i4);
        }
    }

    @DexIgnore
    public BaseChart(Context context) {
        super(context);
        String simpleName = getClass().getSimpleName();
        this.s = simpleName == null ? "BaseChart" : simpleName;
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d(this.s, "initGraph");
        Context context = getContext();
        wg6.a((Object) context, "context");
        this.o = new c(this, context);
        c cVar = this.o;
        if (cVar != null) {
            addView(cVar);
            Context context2 = getContext();
            wg6.a((Object) context2, "context");
            this.p = new e(this, context2);
            e eVar = this.p;
            if (eVar != null) {
                addView(eVar);
                Context context3 = getContext();
                wg6.a((Object) context3, "context");
                this.q = new a(this, context3);
                a aVar = this.q;
                if (aVar != null) {
                    addView(aVar);
                    Context context4 = getContext();
                    wg6.a((Object) context4, "context");
                    this.r = new d(this, context4);
                    d dVar = this.r;
                    if (dVar != null) {
                        addView(dVar);
                    } else {
                        wg6.d("mGraphOverlay");
                        throw null;
                    }
                } else {
                    wg6.d("mBackground");
                    throw null;
                }
            } else {
                wg6.d("mLegend");
                throw null;
            }
        } else {
            wg6.d("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void a(Canvas canvas) {
        wg6.b(canvas, "canvas");
    }

    @DexIgnore
    public final void b() {
        c cVar = this.o;
        if (cVar != null) {
            cVar.invalidate();
            e eVar = this.p;
            if (eVar != null) {
                eVar.invalidate();
                a aVar = this.q;
                if (aVar != null) {
                    aVar.invalidate();
                    d dVar = this.r;
                    if (dVar != null) {
                        dVar.invalidate();
                    } else {
                        wg6.d("mGraphOverlay");
                        throw null;
                    }
                } else {
                    wg6.d("mBackground");
                    throw null;
                }
            } else {
                wg6.d("mLegend");
                throw null;
            }
        } else {
            wg6.d("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void b(Canvas canvas) {
        wg6.b(canvas, "canvas");
    }

    @DexIgnore
    public final void c() {
        b();
    }

    @DexIgnore
    public void c(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void c(Canvas canvas) {
        wg6.b(canvas, "canvas");
    }

    @DexIgnore
    public void d(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void d(Canvas canvas) {
        wg6.b(canvas, "canvas");
    }

    @DexIgnore
    public final a getMBackground() {
        a aVar = this.q;
        if (aVar != null) {
            return aVar;
        }
        wg6.d("mBackground");
        throw null;
    }

    @DexIgnore
    public final int getMBottomPadding() {
        return this.f;
    }

    @DexIgnore
    public final c getMGraph() {
        c cVar = this.o;
        if (cVar != null) {
            return cVar;
        }
        wg6.d("mGraph");
        throw null;
    }

    @DexIgnore
    public final int getMGraphHeight() {
        return this.h;
    }

    @DexIgnore
    public final d getMGraphOverlay() {
        d dVar = this.r;
        if (dVar != null) {
            return dVar;
        }
        wg6.d("mGraphOverlay");
        throw null;
    }

    @DexIgnore
    public final int getMGraphWidth() {
        return this.g;
    }

    @DexIgnore
    public final int getMHeight() {
        return this.a;
    }

    @DexIgnore
    public final int getMLeftPadding() {
        return this.c;
    }

    @DexIgnore
    public final e getMLegend() {
        e eVar = this.p;
        if (eVar != null) {
            return eVar;
        }
        wg6.d("mLegend");
        throw null;
    }

    @DexIgnore
    public final int getMLegendHeight() {
        return this.j;
    }

    @DexIgnore
    public final int getMLegendWidth() {
        return this.i;
    }

    @DexIgnore
    public final int getMRightPadding() {
        return this.e;
    }

    @DexIgnore
    public final int getMTopPadding() {
        return this.d;
    }

    @DexIgnore
    public final int getMWidth() {
        return this.b;
    }

    @DexIgnore
    public final String getTAG() {
        return this.s;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.b = i2;
        this.a = i3;
        this.c = getPaddingLeft();
        this.d = getPaddingTop();
        this.e = getPaddingRight();
        this.f = getPaddingBottom();
        c cVar = this.o;
        if (cVar != null) {
            cVar.layout(this.c, this.d, i2 - this.e, (i3 - this.j) - this.f);
            e eVar = this.p;
            if (eVar != null) {
                int i6 = this.c;
                int i7 = this.f;
                eVar.layout(i6, (i3 - this.j) - i7, i2 - this.e, i3 - i7);
                a aVar = this.q;
                if (aVar != null) {
                    aVar.layout(this.c, this.d, i2 - this.e, i3 - this.f);
                    d dVar = this.r;
                    if (dVar != null) {
                        dVar.layout(this.c, this.d, i2 - this.e, (i3 - this.j) - this.f);
                    } else {
                        wg6.d("mGraphOverlay");
                        throw null;
                    }
                } else {
                    wg6.d("mBackground");
                    throw null;
                }
            } else {
                wg6.d("mLegend");
                throw null;
            }
        } else {
            wg6.d("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public final void setMBackground(a aVar) {
        wg6.b(aVar, "<set-?>");
        this.q = aVar;
    }

    @DexIgnore
    public final void setMBottomPadding(int i2) {
        this.f = i2;
    }

    @DexIgnore
    public final void setMGraph(c cVar) {
        wg6.b(cVar, "<set-?>");
        this.o = cVar;
    }

    @DexIgnore
    public final void setMGraphHeight(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public final void setMGraphOverlay(d dVar) {
        wg6.b(dVar, "<set-?>");
        this.r = dVar;
    }

    @DexIgnore
    public final void setMGraphWidth(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public final void setMHeight(int i2) {
        this.a = i2;
    }

    @DexIgnore
    public final void setMLeftPadding(int i2) {
        this.c = i2;
    }

    @DexIgnore
    public final void setMLegend(e eVar) {
        wg6.b(eVar, "<set-?>");
        this.p = eVar;
    }

    @DexIgnore
    public final void setMLegendHeight(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void setMLegendWidth(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public final void setMRightPadding(int i2) {
        this.e = i2;
    }

    @DexIgnore
    public final void setMTopPadding(int i2) {
        this.d = i2;
    }

    @DexIgnore
    public final void setMWidth(int i2) {
        this.b = i2;
    }

    @DexIgnore
    public BaseChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        String simpleName = getClass().getSimpleName();
        this.s = simpleName == null ? "BaseChart" : simpleName;
    }

    @DexIgnore
    public BaseChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        String simpleName = getClass().getSimpleName();
        this.s = simpleName == null ? "BaseChart" : simpleName;
    }

    @DexIgnore
    public BaseChart(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        String simpleName = getClass().getSimpleName();
        this.s = simpleName == null ? "BaseChart" : simpleName;
    }

    @DexIgnore
    public boolean a(MotionEvent motionEvent) {
        wg6.b(motionEvent, Constants.EVENT);
        return super.onTouchEvent(motionEvent);
    }
}
