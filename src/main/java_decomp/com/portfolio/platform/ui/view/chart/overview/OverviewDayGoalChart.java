package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.fossil.ue6;
import com.fossil.wg6;
import com.fossil.yd6;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewDayGoalChart extends OverviewDayChart {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(((BarChart.b) t).c(), ((BarChart.b) t2).c());
        }
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public void e(Canvas canvas) {
        Object obj;
        wg6.b(canvas, "canvas");
        getMGraphPaint().setColor(getMDefaultColor());
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            ArrayList<BarChart.b> arrayList = it.next().c().get(0);
            wg6.a((Object) arrayList, "item.mListOfBarPoints[0]");
            Iterator it2 = yd6.a(arrayList, new a()).iterator();
            if (!it2.hasNext()) {
                obj = null;
            } else {
                obj = it2.next();
                if (it2.hasNext()) {
                    int e = ((BarChart.b) obj).e();
                    do {
                        Object next = it2.next();
                        int e2 = ((BarChart.b) next).e();
                        if (e < e2) {
                            obj = next;
                            e = e2;
                        }
                    } while (it2.hasNext());
                }
            }
            BarChart.b bVar = (BarChart.b) obj;
            if (bVar != null) {
                canvas.drawRoundRect(bVar.a(), getMBarRadius(), getMBarRadius(), getMGraphPaint());
            }
        }
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewDayGoalChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }
}
