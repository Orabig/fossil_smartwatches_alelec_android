package com.portfolio.platform.ui.device.locate.map.usecase;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.fu3;
import com.fossil.hf6;
import com.fossil.hg6;
import com.fossil.is4;
import com.fossil.jf6;
import com.fossil.jm4;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.response.ResponseKt;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetAddress extends m24<is4.b, is4.d, is4.c> {
    @DexIgnore
    public /* final */ GoogleApiService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ double a;
        @DexIgnore
        public /* final */ double b;

        @DexIgnore
        public b(double d, double d2) {
            this.a = d;
            this.b = d2;
        }

        @DexIgnore
        public final double a() {
            return this.a;
        }

        @DexIgnore
        public final double b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            wg6.b(str, "address");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress", f = "GetAddress.kt", l = {20}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetAddress this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(GetAddress getAddress, xe6 xe6) {
            super(xe6);
            this.this$0 = getAddress;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((is4.b) null, (xe6<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetAddress$run$response$1", f = "GetAddress.kt", l = {20}, m = "invokeSuspend")
    public static final class f extends sf6 implements hg6<xe6<? super rx6<ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ GetAddress this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(GetAddress getAddress, b bVar, xe6 xe6) {
            super(1, xe6);
            this.this$0 = getAddress;
            this.$requestValues = bVar;
        }

        @DexIgnore
        public final xe6<cd6> create(xe6<?> xe6) {
            wg6.b(xe6, "completion");
            return new f(this.this$0, this.$requestValues, xe6);
        }

        @DexIgnore
        public final Object invoke(Object obj) {
            return ((f) create((xe6) obj)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                GoogleApiService a2 = this.this$0.d;
                StringBuilder sb = new StringBuilder();
                b bVar = this.$requestValues;
                Double d = null;
                sb.append(bVar != null ? hf6.a(bVar.a()) : null);
                sb.append(',');
                b bVar2 = this.$requestValues;
                if (bVar2 != null) {
                    d = hf6.a(bVar2.b());
                }
                sb.append(d);
                String sb2 = sb.toString();
                Locale a3 = jm4.a();
                wg6.a((Object) a3, "LanguageHelper.getLocale()");
                String language = a3.getLanguage();
                wg6.a((Object) language, "LanguageHelper.getLocale().language");
                this.label = 1;
                obj = a2.getAddress(sb2, language, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public GetAddress(GoogleApiService googleApiService) {
        wg6.b(googleApiService, "mGoogleApiService");
        this.d = googleApiService;
    }

    @DexIgnore
    public String c() {
        return "GetAddress";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object a(is4.b bVar, xe6<Object> xe6) {
        e eVar;
        int i;
        ap4 ap4;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                JsonElement jsonElement = null;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d("GetAddress", "executeUseCase");
                    f fVar = new f(this, bVar, (xe6) null);
                    eVar.L$0 = this;
                    eVar.L$1 = bVar;
                    eVar.label = 1;
                    obj = ResponseKt.a(fVar, eVar);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    b bVar2 = (b) eVar.L$1;
                    GetAddress getAddress = (GetAddress) eVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ku3 ku3 = (ku3) ((cp4) ap4).a();
                    fu3 b2 = ku3 != null ? ku3.b("results") : null;
                    if (b2 == null || b2.size() <= 0) {
                        return new c();
                    }
                    ku3 ku32 = b2.get(0);
                    if (ku32 != null) {
                        jsonElement = ku32.a("formatted_address");
                    }
                    if (jsonElement == null) {
                        return new c();
                    }
                    String f2 = jsonElement.f();
                    wg6.a((Object) f2, "value.asString");
                    return new d(f2);
                } else if (ap4 instanceof zo4) {
                    return new c();
                } else {
                    throw new kc6();
                }
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        JsonElement jsonElement2 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
