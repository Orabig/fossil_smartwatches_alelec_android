package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import android.os.Bundle;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.cp4;
import com.fossil.ed6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jk4;
import com.fossil.jl6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.ws5;
import com.fossil.xe6;
import com.fossil.xs5;
import com.fossil.yr4;
import com.fossil.yr4$i$a;
import com.fossil.yr4$i$b;
import com.fossil.ys5;
import com.fossil.zl6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SwitchActiveDeviceUseCase extends m24<yr4.b, yr4.d, yr4.c> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((qg6) null);
    @DexIgnore
    public b d;
    @DexIgnore
    public Device e;
    @DexIgnore
    public c f;
    @DexIgnore
    public /* final */ BleCommandResultManager.b g; // = new i(this);
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ cj4 j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ an4 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SwitchActiveDeviceUseCase.m;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(String str, int i) {
            wg6.b(str, "newActiveSerial");
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(int i, ArrayList<Integer> arrayList, String str) {
            this.a = i;
            this.b = arrayList;
            this.c = str;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ Device a;

        @DexIgnore
        public d(Device device) {
            this.a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1", f = "SwitchActiveDeviceUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, xe6 xe6) {
            super(2, xe6);
            this.this$0 = switchActiveDeviceUseCase;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r5v9, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = SwitchActiveDeviceUseCase.n.a();
                StringBuilder sb = new StringBuilder();
                sb.append("doSwitchDevice serial ");
                b f = this.this$0.f();
                if (f != null) {
                    sb.append(f.b());
                    local.d(a, sb.toString());
                    PortfolioApp instance = PortfolioApp.get.instance();
                    b f2 = this.this$0.f();
                    if (f2 != null) {
                        if (!instance.r(f2.b())) {
                            this.this$0.i();
                            this.this$0.a(new c(116, (ArrayList<Integer>) null, ""));
                        }
                        return cd6.a;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1", f = "SwitchActiveDeviceUseCase.kt", l = {180}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $newActiveDeviceSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = switchActiveDeviceUseCase;
            this.$newActiveDeviceSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, this.$newActiveDeviceSerial, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r10v11, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r10v14, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r10v16, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        public final Object invokeSuspend(Object obj) {
            String str;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = SwitchActiveDeviceUseCase.n.a();
                local.d(a2, "could not erase new data file " + this.$newActiveDeviceSerial + ", force switch to it");
                String e = this.this$0.k.e();
                SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$0;
                switchActiveDeviceUseCase.a(switchActiveDeviceUseCase.i.getDeviceBySerial(this.$newActiveDeviceSerial));
                boolean e2 = PortfolioApp.get.instance().e(this.$newActiveDeviceSerial);
                if (e2) {
                    SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.this$0;
                    String str2 = this.$newActiveDeviceSerial;
                    this.L$0 = il6;
                    this.L$1 = e;
                    this.Z$0 = e2;
                    this.label = 1;
                    obj = switchActiveDeviceUseCase2.a(str2, (MisfitDeviceProfile) null, this);
                    if (obj == a) {
                        return a;
                    }
                    str = e;
                } else {
                    this.this$0.a(new c(116, (ArrayList<Integer>) null, ""));
                    return cd6.a;
                }
            } else if (i == 1) {
                str = (String) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((Boolean) ((lc6) obj).component1()).booleanValue()) {
                this.this$0.b(this.$newActiveDeviceSerial);
                Object r10 = this.this$0;
                r10.a(new d(r10.e()));
            } else {
                PortfolioApp.get.instance().e(str);
                c g = this.this$0.g();
                if (g == null || this.this$0.a(g) == null) {
                    this.this$0.a(new c(116, (ArrayList<Integer>) null, ""));
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements m24.e<ys5, ws5> {
        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ys5 ys5) {
            wg6.b(ys5, "responseValue");
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "getDeviceSetting success");
        }

        @DexIgnore
        public void a(ws5 ws5) {
            wg6.b(ws5, "errorValue");
            FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "getDeviceSetting fail");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$2", f = "SwitchActiveDeviceUseCase.kt", l = {232, 239}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super lc6<? extends Boolean, ? extends Integer>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SwitchActiveDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(SwitchActiveDeviceUseCase switchActiveDeviceUseCase, MisfitDeviceProfile misfitDeviceProfile, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = switchActiveDeviceUseCase;
            this.$currentDeviceProfile = misfitDeviceProfile;
            this.$serial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, this.$currentDeviceProfile, this.$serial, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00bd, code lost:
            if (r22 == null) goto L_0x00c0;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object obj3;
            il6 il6;
            Device device;
            Object a = ff6.a();
            int i = this.label;
            String str = null;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.this$0;
                MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
                if (misfitDeviceProfile != null) {
                    int batteryLevel = misfitDeviceProfile.getBatteryLevel();
                    if (batteryLevel < 0 || 100 < batteryLevel) {
                        batteryLevel = batteryLevel < 0 ? 0 : 100;
                    }
                    int b = jk4.b(misfitDeviceProfile.getVibrationStrength().getVibrationStrengthLevel());
                    Device deviceBySerial = this.this$0.i.getDeviceBySerial(this.$serial);
                    if (deviceBySerial != null) {
                        deviceBySerial.setBatteryLevel(batteryLevel);
                        deviceBySerial.setFirmwareRevision(misfitDeviceProfile.getFirmwareVersion());
                        deviceBySerial.setMacAddress(misfitDeviceProfile.getAddress());
                        deviceBySerial.setMajor(misfitDeviceProfile.getMicroAppMajorVersion());
                        deviceBySerial.setMinor(misfitDeviceProfile.getMicroAppMinorVersion());
                        if (deviceBySerial != null) {
                            device = deviceBySerial;
                        }
                    }
                    device = new Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), batteryLevel, hf6.a(b), false, 64, (qg6) null);
                    cd6 cd6 = cd6.a;
                }
                device = this.this$0.i.getDeviceBySerial(this.$serial);
                switchActiveDeviceUseCase.a(device);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = SwitchActiveDeviceUseCase.n.a();
                local.d(a2, "doSwitchDevice device " + this.this$0.e());
                if (this.this$0.e() == null) {
                    this.this$0.a(new c(116, qd6.a((T[]) new Integer[]{hf6.a(-1)}), "No device data"));
                    return new lc6(hf6.a(false), hf6.a(-1));
                }
                DeviceRepository b2 = this.this$0.i;
                Device e = this.this$0.e();
                if (e != null) {
                    this.L$0 = il6;
                    this.label = 1;
                    obj3 = b2.forceLinkDevice(e, this);
                    if (obj3 == a) {
                        return a;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
                obj3 = obj;
            } else if (i == 2) {
                MFUser mFUser = (MFUser) this.L$3;
                MFUser mFUser2 = (MFUser) this.L$2;
                ap4 ap4 = (ap4) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                obj2 = obj;
                ap4 ap42 = (ap4) obj2;
                return new lc6(hf6.a(true), hf6.a(-1));
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ap4 ap43 = (ap4) obj3;
            if (ap43 instanceof cp4) {
                FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "doSwitchDevice success");
                MFUser currentUser = this.this$0.h.getCurrentUser();
                if (currentUser != null) {
                    Device e2 = this.this$0.e();
                    if (e2 != null) {
                        currentUser.setActiveDeviceId(e2.getDeviceId());
                        UserRepository d = this.this$0.h;
                        this.L$0 = il6;
                        this.L$1 = ap43;
                        this.L$2 = currentUser;
                        this.L$3 = currentUser;
                        this.label = 2;
                        obj2 = d.updateUser(currentUser, false, this);
                        if (obj2 == a) {
                            return a;
                        }
                        ap4 ap422 = (ap4) obj2;
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                return new lc6(hf6.a(true), hf6.a(-1));
            } else if (!(ap43 instanceof zo4)) {
                return new lc6(hf6.a(false), hf6.a(-1));
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = SwitchActiveDeviceUseCase.n.a();
                StringBuilder sb = new StringBuilder();
                sb.append("doSwitchDevice fail ");
                zo4 zo4 = (zo4) ap43;
                sb.append(zo4.a());
                local2.d(a3, sb.toString());
                SwitchActiveDeviceUseCase switchActiveDeviceUseCase2 = this.this$0;
                ArrayList a4 = qd6.a((T[]) new Integer[]{hf6.a(zo4.a())});
                ServerError c = zo4.c();
                if (c != null) {
                    str = c.getMessage();
                }
                switchActiveDeviceUseCase2.a(new c(114, a4, str));
                PortfolioApp.get.instance().a(this.$serial, false, zo4.a());
                return new lc6(hf6.a(false), hf6.a(zo4.a()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ SwitchActiveDeviceUseCase a;

        @DexIgnore
        public i(SwitchActiveDeviceUseCase switchActiveDeviceUseCase) {
            this.a = switchActiveDeviceUseCase;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r11v14, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r11v17, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r12v4, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r11v19, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r11v30, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            if (communicateMode == CommunicateMode.SWITCH_DEVICE) {
                b f = this.a.f();
                if (wg6.a((Object) stringExtra, (Object) f != null ? f.b() : null)) {
                    boolean z = true;
                    if (intExtra == ServiceActionResult.ASK_FOR_LINK_SERVER.ordinal()) {
                        if (intent.getExtras() == null) {
                            z = false;
                        }
                        if (!ed6.a || z) {
                            Bundle extras = intent.getExtras();
                            if (extras != null) {
                                rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new yr4$i$a(this, stringExtra, (MisfitDeviceProfile) extras.getParcelable("device"), (xe6) null), 3, (Object) null);
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            throw new AssertionError("Assertion failed");
                        }
                    } else if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                        this.a.i();
                        FLogger.INSTANCE.getLocal().d(SwitchActiveDeviceUseCase.n.a(), "Switch device  success");
                        if (intent.getExtras() == null) {
                            z = false;
                        }
                        if (!ed6.a || z) {
                            Bundle extras2 = intent.getExtras();
                            if (extras2 != null) {
                                MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras2.getParcelable("device");
                                if (misfitDeviceProfile != null) {
                                    if (misfitDeviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
                                        this.a.l.a(misfitDeviceProfile.getHeartRateMode());
                                    }
                                    SwitchActiveDeviceUseCase switchActiveDeviceUseCase = this.a;
                                    wg6.a((Object) stringExtra, "serial");
                                    switchActiveDeviceUseCase.b(stringExtra);
                                    rm6 unused2 = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new yr4$i$b(this, misfitDeviceProfile, stringExtra, (xe6) null), 3, (Object) null);
                                    PortfolioApp.get.instance().k(stringExtra);
                                    Object r11 = this.a;
                                    r11.a(new d(r11.e()));
                                    return;
                                }
                                wg6.a();
                                throw null;
                            }
                            wg6.a();
                            throw null;
                        }
                        throw new AssertionError("Assertion failed");
                    } else if (intExtra == ServiceActionResult.FAILED.ordinal()) {
                        this.a.i();
                        int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                        ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                        if (integerArrayListExtra == null) {
                            integerArrayListExtra = new ArrayList<>();
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = SwitchActiveDeviceUseCase.n.a();
                        local.d(a2, "stop current workout fail due to " + intExtra2);
                        if (intExtra2 != 1101) {
                            if (intExtra2 == 1928) {
                                c g = this.a.g();
                                if (g == null || this.a.a(g) == null) {
                                    this.a.a(new c(116, (ArrayList<Integer>) null, ""));
                                    return;
                                }
                                return;
                            } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                                this.a.a(new c(117, (ArrayList<Integer>) null, ""));
                                return;
                            }
                        }
                        this.a.a(new c(113, integerArrayListExtra, ""));
                    }
                }
            }
        }
    }

    /*
    static {
        String simpleName = SwitchActiveDeviceUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "SwitchActiveDeviceUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public SwitchActiveDeviceUseCase(UserRepository userRepository, DeviceRepository deviceRepository, cj4 cj4, PortfolioApp portfolioApp, an4 an4) {
        wg6.b(userRepository, "mUserRepository");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(cj4, "mDeviceSettingFactory");
        wg6.b(portfolioApp, "mApp");
        wg6.b(an4, "mSharePrefs");
        this.h = userRepository;
        this.i = deviceRepository;
        this.j = cj4;
        this.k = portfolioApp;
        this.l = an4;
    }

    @DexIgnore
    public final Device e() {
        return this.e;
    }

    @DexIgnore
    public final b f() {
        return this.d;
    }

    @DexIgnore
    public final c g() {
        return this.f;
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d(m, "registerReceiver ");
        BleCommandResultManager.d.b(this.g, CommunicateMode.SWITCH_DEVICE);
        BleCommandResultManager.d.a(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d(m, "unregisterReceiver ");
        BleCommandResultManager.d.b(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$g] */
    public final void b(String str) {
        wg6.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(m, "getDeviceSetting start");
        this.j.a(str).a(new xs5(str), new g());
    }

    @DexIgnore
    public String c() {
        return m;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final rm6 d() {
        return ik6.b(b(), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(Device device) {
        this.e = device;
    }

    @DexIgnore
    public final void a(c cVar) {
        this.f = cVar;
    }

    @DexIgnore
    public Object a(yr4.b bVar, xe6<Object> xe6) {
        if (bVar == null) {
            return new c(600, (ArrayList<Integer>) null, "");
        }
        this.d = bVar;
        String e2 = this.k.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "run with mode " + bVar.a() + " currentActive " + e2);
        if (bVar.a() != 4) {
            d();
        } else {
            a(bVar.b());
        }
        return new Object();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final rm6 a(String str) {
        return ik6.b(b(), (af6) null, (ll6) null, new f(this, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str, MisfitDeviceProfile misfitDeviceProfile, xe6<? super lc6<Boolean, Integer>> xe6) {
        return gk6.a(zl6.b(), new h(this, misfitDeviceProfile, str, (xe6) null), xe6);
    }
}
