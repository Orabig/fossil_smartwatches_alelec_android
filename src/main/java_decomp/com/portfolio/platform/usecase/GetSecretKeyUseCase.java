package com.portfolio.platform.usecase;

import android.text.TextUtils;
import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.mw5;
import com.fossil.n24;
import com.fossil.nc6;
import com.fossil.ow5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetSecretKeyUseCase extends m24<ow5.b, ow5.d, ow5.c> {
    @DexIgnore
    public /* final */ mw5 d;
    @DexIgnore
    public /* final */ DeviceRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            wg6.b(str, "deviceId");
            wg6.b(str2, ButtonService.USER_ID);
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(int i, String str) {
            wg6.b(str, "errorMesagge");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.usecase.GetSecretKeyUseCase", f = "GetSecretKeyUseCase.kt", l = {22, 28}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetSecretKeyUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(GetSecretKeyUseCase getSecretKeyUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = getSecretKeyUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((ow5.b) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public GetSecretKeyUseCase(mw5 mw5, DeviceRepository deviceRepository) {
        wg6.b(mw5, "mEncryptValueKeyStoreUseCase");
        wg6.b(deviceRepository, "mDeviceRepository");
        this.d = mw5;
        this.e = deviceRepository;
    }

    @DexIgnore
    public String c() {
        return "GetSecretKeyUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: java.lang.String} */
    /* JADX WARNING: type inference failed for: r2v3, types: [java.lang.Object, com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.usecase.GetSecretKeyUseCase] */
    /* JADX WARNING: type inference failed for: r6v4, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.mw5] */
    /* JADX WARNING: type inference failed for: r2v5 */
    /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.usecase.GetSecretKeyUseCase] */
    /* JADX WARNING: type inference failed for: r2v12 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object a(b bVar, xe6<Object> xe6) {
        e eVar;
        int i;
        CoroutineUseCase coroutineUseCase;
        String str;
        CoroutineUseCase coroutineUseCase2;
        Object r2;
        ap4 ap4;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    DeviceRepository deviceRepository = this.e;
                    if (bVar != null) {
                        String a3 = bVar.a();
                        eVar.L$0 = this;
                        eVar.L$1 = bVar;
                        eVar.label = 1;
                        obj = deviceRepository.getDeviceSecretKey(a3, eVar);
                        if (obj == a2) {
                            return a2;
                        }
                        r2 = this;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i == 1) {
                    bVar = (b) eVar.L$1;
                    nc6.a(obj);
                    r2 = (GetSecretKeyUseCase) eVar.L$0;
                } else if (i == 2) {
                    String str2 = (String) eVar.L$4;
                    str = (String) eVar.L$3;
                    ap4 ap42 = (ap4) eVar.L$2;
                    b bVar2 = (b) eVar.L$1;
                    nc6.a(obj);
                    coroutineUseCase2 = (GetSecretKeyUseCase) eVar.L$0;
                    PortfolioApp.get.instance().e(str, PortfolioApp.get.instance().e());
                    coroutineUseCase = coroutineUseCase2;
                    coroutineUseCase.a(new d());
                    return new Object();
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("success to get server secret key ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append((String) cp4.a());
                    local.d("GetSecretKeyUseCase", sb.toString());
                    coroutineUseCase = r2;
                    if (!TextUtils.isEmpty((CharSequence) cp4.a())) {
                        Object a4 = cp4.a();
                        if (a4 != null) {
                            String str3 = (String) a4;
                            String str4 = bVar.b() + ':' + bVar.a();
                            Object r6 = r2.d;
                            mw5.b bVar3 = new mw5.b(str4, str3);
                            eVar.L$0 = r2;
                            eVar.L$1 = bVar;
                            eVar.L$2 = ap4;
                            eVar.L$3 = str3;
                            eVar.L$4 = str4;
                            eVar.label = 2;
                            if (n24.a(r6, bVar3, eVar) == a2) {
                                return a2;
                            }
                            str = str3;
                            coroutineUseCase2 = r2;
                            PortfolioApp.get.instance().e(str, PortfolioApp.get.instance().e());
                            coroutineUseCase = coroutineUseCase2;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    coroutineUseCase.a(new d());
                    return new Object();
                }
                if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("fail to get server secret key ");
                    zo4 zo4 = (zo4) ap4;
                    sb2.append(zo4.a());
                    local2.d("GetSecretKeyUseCase", sb2.toString());
                    r2.a(new c(zo4.a(), ""));
                }
                return new Object();
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
