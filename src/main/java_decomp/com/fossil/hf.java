package com.fossil;

import com.fossil.cf;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hf<T> extends cf<T> {
    @DexIgnore
    public /* final */ boolean u;
    @DexIgnore
    public /* final */ Object v;
    @DexIgnore
    public /* final */ xe<?, T> w;

    @DexIgnore
    public hf(cf<T> cfVar) {
        super(cfVar.e.r(), cfVar.a, cfVar.b, (cf.e) null, cfVar.d);
        this.w = cfVar.d();
        this.u = cfVar.g();
        this.f = cfVar.f;
        this.v = cfVar.e();
    }

    @DexIgnore
    public void a(cf<T> cfVar, cf.g gVar) {
    }

    @DexIgnore
    public xe<?, T> d() {
        return this.w;
    }

    @DexIgnore
    public Object e() {
        return this.v;
    }

    @DexIgnore
    public void e(int i) {
    }

    @DexIgnore
    public boolean g() {
        return this.u;
    }

    @DexIgnore
    public boolean h() {
        return true;
    }

    @DexIgnore
    public boolean i() {
        return true;
    }
}
