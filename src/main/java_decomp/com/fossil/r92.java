package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r92 {
    @DexIgnore
    public SharedPreferences a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public /* final */ Map<String, Object> c;

    @DexIgnore
    public r92(Context context) {
        this(context, new z92());
    }

    @DexIgnore
    public final boolean a() {
        return this.a.getAll().isEmpty();
    }

    @DexIgnore
    public final synchronized void b() {
        this.c.clear();
        z92.a(this.b);
        this.a.edit().clear().commit();
    }

    @DexIgnore
    public r92(Context context, z92 z92) {
        this.c = new p4();
        this.b = context;
        this.a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        File file = new File(w6.c(this.b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !a()) {
                    Log.i("InstanceID/Store", "App restored, clearing state");
                    k92.a(this.b, this);
                }
            } catch (IOException e) {
                if (Log.isLoggable("InstanceID/Store", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("InstanceID/Store", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    @DexIgnore
    public final synchronized void a(String str) {
        SharedPreferences.Editor edit = this.a.edit();
        for (String next : this.a.getAll().keySet()) {
            if (next.startsWith(str)) {
                edit.remove(next);
            }
        }
        edit.commit();
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (this) {
            this.c.remove(str);
        }
        z92.a(this.b, str);
        a(String.valueOf(str).concat("|"));
    }
}
