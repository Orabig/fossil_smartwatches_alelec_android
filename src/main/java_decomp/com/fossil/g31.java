package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g31 extends vh {
    @DexIgnore
    public g31(w81 w81, oh ohVar) {
        super(ohVar);
    }

    @DexIgnore
    public String createQuery() {
        return "delete from DeviceFile where deviceMacAddress = ? and fileType = ? and fileIndex = ? and isCompleted = 0";
    }
}
