package com.fossil;

import android.content.Context;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cw<T> implements bs<T> {
    @DexIgnore
    public static /* final */ bs<?> b; // = new cw();

    @DexIgnore
    public static <T> cw<T> a() {
        return (cw) b;
    }

    @DexIgnore
    public rt<T> a(Context context, rt<T> rtVar, int i, int i2) {
        return rtVar;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
    }
}
