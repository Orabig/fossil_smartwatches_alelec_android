package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hw3 extends lw3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore
    public hw3() {
        a();
    }

    @DexIgnore
    public hw3 a() {
        this.a = "";
        this.b = "";
        return this;
    }

    @DexIgnore
    public hw3 a(iw3 iw3) throws IOException {
        while (true) {
            int j = iw3.j();
            if (j == 0) {
                return this;
            }
            if (j == 18) {
                this.a = iw3.i();
            } else if (j == 26) {
                this.b = iw3.i();
            } else if (j == 50) {
                iw3.i();
            } else if (!nw3.b(iw3, j)) {
                return this;
            }
        }
    }
}
