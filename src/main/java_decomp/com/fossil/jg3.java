package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jg3 {
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public boolean g; // = true;

    @DexIgnore
    public jg3(View view) {
        this.a = view;
    }

    @DexIgnore
    public void a() {
        View view = this.a;
        x9.e(view, this.d - (view.getTop() - this.b));
        View view2 = this.a;
        x9.d(view2, this.e - (view2.getLeft() - this.c));
    }

    @DexIgnore
    public boolean b(int i) {
        if (!this.f || this.d == i) {
            return false;
        }
        this.d = i;
        a();
        return true;
    }

    @DexIgnore
    public int c() {
        return this.e;
    }

    @DexIgnore
    public int d() {
        return this.d;
    }

    @DexIgnore
    public boolean e() {
        return this.g;
    }

    @DexIgnore
    public boolean f() {
        return this.f;
    }

    @DexIgnore
    public void g() {
        this.b = this.a.getTop();
        this.c = this.a.getLeft();
    }

    @DexIgnore
    public boolean a(int i) {
        if (!this.g || this.e == i) {
            return false;
        }
        this.e = i;
        a();
        return true;
    }

    @DexIgnore
    public int b() {
        return this.b;
    }

    @DexIgnore
    public void b(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public void a(boolean z) {
        this.g = z;
    }
}
