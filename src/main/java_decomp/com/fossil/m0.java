package com.fossil;

import androidx.appcompat.view.ActionMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface m0 {
    @DexIgnore
    void onSupportActionModeFinished(ActionMode actionMode);

    @DexIgnore
    void onSupportActionModeStarted(ActionMode actionMode);

    @DexIgnore
    ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback);
}
