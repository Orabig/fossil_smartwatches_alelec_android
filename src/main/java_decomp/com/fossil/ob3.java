package com.fossil;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ob3 extends e22 implements ew1 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ob3> CREATOR; // = new nb3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public Intent c;

    @DexIgnore
    public ob3(int i, int i2, Intent intent) {
        this.a = i;
        this.b = i2;
        this.c = intent;
    }

    @DexIgnore
    public final Status o() {
        if (this.b == 0) {
            return Status.e;
        }
        return Status.i;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, this.b);
        g22.a(parcel, 3, (Parcelable) this.c, i, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public ob3() {
        this(0, (Intent) null);
    }

    @DexIgnore
    public ob3(int i, Intent intent) {
        this(2, 0, (Intent) null);
    }
}
