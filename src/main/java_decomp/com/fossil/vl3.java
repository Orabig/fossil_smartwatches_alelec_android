package com.fossil;

import com.fossil.zl3;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vl3<E> extends AbstractCollection<E> implements Serializable {
    @DexIgnore
    @Deprecated
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public zl3<E> asList() {
        int size = size();
        if (size == 0) {
            return zl3.of();
        }
        if (size != 1) {
            return new nn3(this, toArray());
        }
        return zl3.of(iterator().next());
    }

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean contains(Object obj);

    @DexIgnore
    public int copyIntoArray(Object[] objArr, int i) {
        Iterator it = iterator();
        while (it.hasNext()) {
            objArr[i] = it.next();
            i++;
        }
        return i;
    }

    @DexIgnore
    public abstract boolean isPartialView();

    @DexIgnore
    public abstract jo3<E> iterator();

    @DexIgnore
    @Deprecated
    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final Object[] toArray() {
        int size = size();
        if (size == 0) {
            return in3.a;
        }
        Object[] objArr = new Object[size];
        copyIntoArray(objArr, 0);
        return objArr;
    }

    @DexIgnore
    public Object writeReplace() {
        return new zl3.d(toArray());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<E> extends b<E> {
        @DexIgnore
        public Object[] a;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public a(int i) {
            bl3.a(i, "initialCapacity");
            this.a = new Object[i];
        }

        @DexIgnore
        public final void a(int i) {
            Object[] objArr = this.a;
            if (objArr.length < i) {
                this.a = in3.a((T[]) objArr, b.a(objArr.length, i));
            }
        }

        @DexIgnore
        public a<E> a(E e) {
            jk3.a(e);
            a(this.b + 1);
            Object[] objArr = this.a;
            int i = this.b;
            this.b = i + 1;
            objArr[i] = e;
            return this;
        }

        @DexIgnore
        public b<E> a(E... eArr) {
            in3.a(eArr);
            a(this.b + eArr.length);
            System.arraycopy(eArr, 0, this.a, this.b, eArr.length);
            this.b += eArr.length;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<E> {
        @DexIgnore
        public static int a(int i, int i2) {
            if (i2 >= 0) {
                int i3 = i + (i >> 1) + 1;
                if (i3 < i2) {
                    i3 = Integer.highestOneBit(i2 - 1) << 1;
                }
                if (i3 < 0) {
                    return Integer.MAX_VALUE;
                }
                return i3;
            }
            throw new AssertionError("cannot store more than MAX_VALUE elements");
        }

        @DexIgnore
        public abstract b<E> a(E e);

        @DexIgnore
        public b<E> a(E... eArr) {
            for (E a : eArr) {
                a(a);
            }
            return this;
        }

        @DexIgnore
        public b<E> a(Iterator<? extends E> it) {
            while (it.hasNext()) {
                a(it.next());
            }
            return this;
        }
    }

    @DexIgnore
    public final <T> T[] toArray(T[] tArr) {
        jk3.a(tArr);
        int size = size();
        if (tArr.length < size) {
            tArr = in3.c(tArr, size);
        } else if (tArr.length > size) {
            tArr[size] = null;
        }
        copyIntoArray(tArr, 0);
        return tArr;
    }
}
