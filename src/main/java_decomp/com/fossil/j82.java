package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j82 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<j82> CREATOR; // = new i82();
    @DexIgnore
    public /* final */ nd2 a;

    @DexIgnore
    public j82(IBinder iBinder) {
        this.a = pd2.a(iBinder);
    }

    @DexIgnore
    public final String toString() {
        return String.format("DisableFitRequest", new Object[0]);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a.asBinder(), false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public j82(nd2 nd2) {
        this.a = nd2;
    }
}
