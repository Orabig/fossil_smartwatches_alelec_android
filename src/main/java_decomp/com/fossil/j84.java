package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j84 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ OverviewSleepDayChart u;
    @DexIgnore
    public /* final */ View v;
    @DexIgnore
    public /* final */ View w;
    @DexIgnore
    public /* final */ View x;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j84(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, FlexibleButton flexibleButton, ImageView imageView, ImageView imageView2, ImageView imageView3, OverviewSleepDayChart overviewSleepDayChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = imageView;
        this.s = imageView2;
        this.t = imageView3;
        this.u = overviewSleepDayChart;
        this.v = view2;
        this.w = view3;
        this.x = view4;
    }
}
