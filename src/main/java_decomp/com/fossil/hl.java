package com.fossil;

import androidx.viewpager2.widget.ViewPager2;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl extends ViewPager2.i {
    @DexIgnore
    public /* final */ List<ViewPager2.i> a;

    @DexIgnore
    public hl(int i) {
        this.a = new ArrayList(i);
    }

    @DexIgnore
    public void a(ViewPager2.i iVar) {
        this.a.add(iVar);
    }

    @DexIgnore
    public void b(int i) {
        try {
            for (ViewPager2.i b : this.a) {
                b.b(i);
            }
        } catch (ConcurrentModificationException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public void a(int i, float f, int i2) {
        try {
            for (ViewPager2.i a2 : this.a) {
                a2.a(i, f, i2);
            }
        } catch (ConcurrentModificationException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public void a(int i) {
        try {
            for (ViewPager2.i a2 : this.a) {
                a2.a(i);
            }
        } catch (ConcurrentModificationException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public final void a(ConcurrentModificationException concurrentModificationException) {
        throw new IllegalStateException("Adding and removing callbacks during dispatch to callbacks is not supported", concurrentModificationException);
    }
}
