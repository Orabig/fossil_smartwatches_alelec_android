package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v53<V> extends FutureTask<V> implements Comparable<v53> {
    @DexIgnore
    public /* final */ long a; // = u53.l.getAndIncrement();
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ u53 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v53(u53 u53, Callable<V> callable, boolean z, String str) {
        super(callable);
        this.d = u53;
        w12.a(str);
        this.c = str;
        this.b = z;
        if (this.a == RecyclerView.FOREVER_NS) {
            u53.b().t().a("Tasks index overflow");
        }
    }

    @DexIgnore
    public final /* synthetic */ int compareTo(Object obj) {
        v53 v53 = (v53) obj;
        boolean z = this.b;
        if (z != v53.b) {
            return z ? -1 : 1;
        }
        long j = this.a;
        long j2 = v53.a;
        if (j < j2) {
            return -1;
        }
        if (j > j2) {
            return 1;
        }
        this.d.b().u().a("Two tasks share the same index. index", Long.valueOf(this.a));
        return 0;
    }

    @DexIgnore
    public final void setException(Throwable th) {
        this.d.b().t().a(this.c, th);
        super.setException(th);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v53(u53 u53, Runnable runnable, boolean z, String str) {
        super(runnable, (Object) null);
        this.d = u53;
        w12.a(str);
        this.c = str;
        this.b = false;
        if (this.a == RecyclerView.FOREVER_NS) {
            u53.b().t().a("Tasks index overflow");
        }
    }
}
