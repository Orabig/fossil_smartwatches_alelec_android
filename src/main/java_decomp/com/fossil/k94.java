package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k94 extends j94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public long v;

    /*
    static {
        x.put(2131362119, 1);
        x.put(2131362035, 2);
        x.put(2131363218, 3);
        x.put(2131363105, 4);
        x.put(2131363194, 5);
        x.put(2131362878, 6);
        x.put(2131362000, 7);
    }
    */

    @DexIgnore
    public k94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 8, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[7], objArr[0], objArr[2], objArr[1], objArr[6], objArr[4], objArr[5], objArr[3]);
        this.v = -1;
        this.r.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
