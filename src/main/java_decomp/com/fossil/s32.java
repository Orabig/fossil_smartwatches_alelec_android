package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.x52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s32 extends cb2 implements q32 {
    @DexIgnore
    public s32(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    @DexIgnore
    public final x52 zzb() throws RemoteException {
        Parcel a = a(1, zza());
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final int zzc() throws RemoteException {
        Parcel a = a(2, zza());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
