package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataPoint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u72 implements Parcelable.Creator<DataPoint> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v4, types: [java.lang.Object[]] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        f72 f72 = null;
        l72[] l72Arr = null;
        f72 f722 = null;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    f72 = f22.a(parcel2, a, f72.CREATOR);
                    break;
                case 3:
                    j = f22.s(parcel2, a);
                    break;
                case 4:
                    j2 = f22.s(parcel2, a);
                    break;
                case 5:
                    l72Arr = f22.b(parcel2, a, l72.CREATOR);
                    break;
                case 6:
                    f722 = f22.a(parcel2, a, f72.CREATOR);
                    break;
                case 7:
                    j3 = f22.s(parcel2, a);
                    break;
                case 8:
                    j4 = f22.s(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new DataPoint(f72, j, j2, l72Arr, f722, j3, j4);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new DataPoint[i];
    }
}
