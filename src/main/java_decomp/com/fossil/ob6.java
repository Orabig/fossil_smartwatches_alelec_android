package com.fossil;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ob6 implements xb6 {
    @DexIgnore
    public /* final */ bc6 a;
    @DexIgnore
    public /* final */ ac6 b;
    @DexIgnore
    public /* final */ b96 c;
    @DexIgnore
    public /* final */ lb6 d;
    @DexIgnore
    public /* final */ cc6 e;
    @DexIgnore
    public /* final */ i86 f;
    @DexIgnore
    public /* final */ cb6 g; // = new db6(this.f);
    @DexIgnore
    public /* final */ c96 h;

    @DexIgnore
    public ob6(i86 i86, bc6 bc6, b96 b96, ac6 ac6, lb6 lb6, cc6 cc6, c96 c96) {
        this.f = i86;
        this.a = bc6;
        this.c = b96;
        this.b = ac6;
        this.d = lb6;
        this.e = cc6;
        this.h = c96;
    }

    @DexIgnore
    public yb6 a() {
        return a(wb6.USE_CACHE);
    }

    @DexIgnore
    public final yb6 b(wb6 wb6) {
        yb6 yb6 = null;
        try {
            if (wb6.SKIP_CACHE_LOOKUP.equals(wb6)) {
                return null;
            }
            JSONObject a2 = this.d.a();
            if (a2 != null) {
                yb6 a3 = this.b.a(this.c, a2);
                if (a3 != null) {
                    a(a2, "Loaded cached settings: ");
                    long a4 = this.c.a();
                    if (!wb6.IGNORE_CACHE_EXPIRATION.equals(wb6)) {
                        if (a3.a(a4)) {
                            c86.g().d("Fabric", "Cached settings have expired.");
                            return null;
                        }
                    }
                    try {
                        c86.g().d("Fabric", "Returning cached settings.");
                        return a3;
                    } catch (Exception e2) {
                        e = e2;
                        yb6 = a3;
                        c86.g().e("Fabric", "Failed to get cached settings", e);
                        return yb6;
                    }
                } else {
                    c86.g().e("Fabric", "Failed to transform cached settings data.", (Throwable) null);
                    return null;
                }
            } else {
                c86.g().d("Fabric", "No cached settings data found.");
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            c86.g().e("Fabric", "Failed to get cached settings", e);
            return yb6;
        }
    }

    @DexIgnore
    public String c() {
        return z86.a(z86.n(this.f.d()));
    }

    @DexIgnore
    public String d() {
        return this.g.get().getString("existing_instance_identifier", "");
    }

    @DexIgnore
    public yb6 a(wb6 wb6) {
        JSONObject a2;
        yb6 yb6 = null;
        if (!this.h.a()) {
            c86.g().d("Fabric", "Not fetching settings, because data collection is disabled by Firebase.");
            return null;
        }
        try {
            if (!c86.h() && !b()) {
                yb6 = b(wb6);
            }
            if (yb6 == null && (a2 = this.e.a(this.a)) != null) {
                yb6 = this.b.a(this.c, a2);
                this.d.a(yb6.f, a2);
                a(a2, "Loaded settings: ");
                a(c());
            }
            if (yb6 == null) {
                return b(wb6.IGNORE_CACHE_EXPIRATION);
            }
            return yb6;
        } catch (Exception e2) {
            c86.g().e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", e2);
            return null;
        }
    }

    @DexIgnore
    public final void a(JSONObject jSONObject, String str) throws JSONException {
        l86 g2 = c86.g();
        g2.d("Fabric", str + jSONObject.toString());
    }

    @DexIgnore
    public boolean b() {
        return !d().equals(c());
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public boolean a(String str) {
        SharedPreferences.Editor edit = this.g.edit();
        edit.putString("existing_instance_identifier", str);
        return this.g.a(edit);
    }
}
