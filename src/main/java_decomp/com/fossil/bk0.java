package com.fossil;

import android.bluetooth.BluetoothDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bk0 extends xg6 implements hg6<BluetoothDevice, String> {
    @DexIgnore
    public static /* final */ bk0 a; // = new bk0();

    @DexIgnore
    public bk0() {
        super(1);
    }

    @DexIgnore
    public Object invoke(Object obj) {
        String address = ((BluetoothDevice) obj).getAddress();
        wg6.a(address, "it.address");
        return address;
    }
}
