package com.fossil;

import com.portfolio.platform.helper.AnalyticsHelper;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d14 implements Factory<sj4> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public d14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static d14 a(b14 b14) {
        return new d14(b14);
    }

    @DexIgnore
    public static AnalyticsHelper b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static AnalyticsHelper c(b14 b14) {
        AnalyticsHelper a2 = b14.a();
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public AnalyticsHelper get() {
        return b(this.a);
    }
}
