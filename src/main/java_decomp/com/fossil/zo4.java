package com.fossil;

import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo4<T> extends ap4<T> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ ServerError b;
    @DexIgnore
    public /* final */ Throwable c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public zo4(int i, ServerError serverError, Throwable th, String str) {
        super((qg6) null);
        this.a = i;
        this.b = serverError;
        this.c = th;
        this.d = str;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final ServerError c() {
        return this.b;
    }

    @DexIgnore
    public final Throwable d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof zo4)) {
            return false;
        }
        zo4 zo4 = (zo4) obj;
        return this.a == zo4.a && wg6.a((Object) this.b, (Object) zo4.b) && wg6.a((Object) this.c, (Object) zo4.c) && wg6.a((Object) this.d, (Object) zo4.d);
    }

    @DexIgnore
    public int hashCode() {
        int a2 = d.a(this.a) * 31;
        ServerError serverError = this.b;
        int i = 0;
        int hashCode = (a2 + (serverError != null ? serverError.hashCode() : 0)) * 31;
        Throwable th = this.c;
        int hashCode2 = (hashCode + (th != null ? th.hashCode() : 0)) * 31;
        String str = this.d;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        return "Failure(code=" + this.a + ", serverError=" + this.b + ", throwable=" + this.c + ", errorItems=" + this.d + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ zo4(int i, ServerError serverError, Throwable th, String str, int i2, qg6 qg6) {
        this(i, serverError, (i2 & 4) != 0 ? null : th, (i2 & 8) != 0 ? null : str);
    }
}
