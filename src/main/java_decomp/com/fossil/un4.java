package com.fossil;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class un4 extends AppFilterProviderImpl {
    @DexIgnore
    public static /* final */ String a; // = "un4";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements UpgradeCommand {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void execute(SQLiteDatabase sQLiteDatabase) {
            FLogger.INSTANCE.getLocal().d(un4.a, " ---- UPGRADE DB APPFILTER, table APPFILTER");
            sQLiteDatabase.execSQL("ALTER TABLE appfilter ADD COLUMN deviceFamily int");
            FLogger.INSTANCE.getLocal().d(un4.a, " ---- UPGRADE DB APPFILTER, table APPFILTER SUCCESS");
            StringBuilder sb = new StringBuilder();
            String e = PortfolioApp.T.e();
            if (!TextUtils.isEmpty(e)) {
                sb.append("UPDATE ");
                sb.append("appfilter");
                sb.append(" SET deviceFamily = ");
                sb.append(DeviceIdentityUtils.getDeviceFamily(e).ordinal());
                sQLiteDatabase.execSQL(sb.toString());
                return;
            }
            un4.this.removeAllAppFilters();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements UpgradeCommand {
        @DexIgnore
        public b(un4 un4) {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:32:0x016f A[Catch:{ Exception -> 0x0218 }] */
        public void execute(SQLiteDatabase sQLiteDatabase) {
            String str;
            String str2;
            String str3;
            String str4;
            int i;
            SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
            try {
                FLogger.INSTANCE.getLocal().d(un4.a, "Inside .doInBackground upgrade appfilter");
                Cursor query = sQLiteDatabase.query(true, "appfilter", new String[]{"id", "type", "color", "name", "haptic", "timestamp", "enabled", "deviceFamily"}, (String) null, (String[]) null, (String) null, (String) null, (String) null, (String) null);
                ArrayList<AppFilter> arrayList = new ArrayList<>();
                int i2 = -1;
                String e = PortfolioApp.T.e();
                if (!TextUtils.isEmpty(e)) {
                    i2 = DeviceHelper.o.a(e).getValue();
                }
                String str5 = "deviceFamily";
                String str6 = "enabled";
                String str7 = "name";
                String str8 = "type";
                if (query != null && i2 > 0) {
                    if (DeviceHelper.o.g(e)) {
                        query.moveToFirst();
                        while (!query.isAfterLast()) {
                            String string = query.getString(query.getColumnIndex(str8));
                            String string2 = query.getString(query.getColumnIndex("color"));
                            String string3 = query.getString(query.getColumnIndex(str7));
                            String string4 = query.getString(query.getColumnIndex("haptic"));
                            String str9 = str8;
                            int i3 = query.getInt(query.getColumnIndex("timestamp"));
                            String str10 = str7;
                            int i4 = query.getInt(query.getColumnIndex(str6));
                            String str11 = str6;
                            int i5 = query.getInt(query.getColumnIndex(str5));
                            String str12 = str5;
                            int i6 = query.getInt(query.getColumnIndex("id"));
                            if (i5 == i2) {
                                i = i2;
                                AppFilter appFilter = new AppFilter();
                                appFilter.setType(string);
                                appFilter.setColor(string2);
                                appFilter.setName(string3);
                                appFilter.setHaptic(string4);
                                appFilter.setTimestamp((long) i3);
                                boolean z = true;
                                if (i4 != 1) {
                                    z = false;
                                }
                                appFilter.setEnabled(z);
                                appFilter.setDeviceFamily(i5);
                                appFilter.setDbRowId(i6);
                                wn4 a = zm4.p.a().f().a(string, MFDeviceFamily.fromInt(appFilter.getDeviceFamily()).toString());
                                if (a != null) {
                                    appFilter.setHour(a.d());
                                    appFilter.setVibrationOnly(a.f());
                                }
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String str13 = un4.a;
                                local.d(str13, "Add appfiler=" + appFilter);
                                if (!appFilter.isVibrationOnly()) {
                                    arrayList.add(appFilter);
                                }
                            } else {
                                i = i2;
                                FLogger.INSTANCE.getLocal().d(un4.a, "Skip this app filter");
                            }
                            query.moveToNext();
                            str8 = str9;
                            str7 = str10;
                            str6 = str11;
                            str5 = str12;
                            i2 = i;
                        }
                        str = str5;
                        str2 = str6;
                        str3 = str7;
                        str4 = str8;
                        query.close();
                        sQLiteDatabase2.execSQL("CREATE TABLE appfilter_copy (id INTEGER PRIMARY KEY AUTOINCREMENT, type VARCHAR, color VARCHAR, haptic VARCHAR, timestamp BIGINT, enabled INTEGER, name VARCHAR, isVibrationOnly INTEGER, deviceFamily INTEGER, hour INTEGER);");
                        if (!arrayList.isEmpty()) {
                            for (AppFilter appFilter2 : arrayList) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("color", appFilter2.getColor());
                                contentValues.put("haptic", appFilter2.getHaptic());
                                contentValues.put("timestamp", Long.valueOf(appFilter2.getTimestamp()));
                                String str14 = str3;
                                contentValues.put(str14, appFilter2.getName());
                                String str15 = str2;
                                contentValues.put(str15, Boolean.valueOf(appFilter2.isEnabled()));
                                String str16 = str4;
                                contentValues.put(str16, appFilter2.getType());
                                contentValues.put("id", Integer.valueOf(appFilter2.getDbRowId()));
                                contentValues.put("isVibrationOnly", Boolean.valueOf(appFilter2.isVibrationOnly()));
                                String str17 = str;
                                contentValues.put(str17, Integer.valueOf(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()));
                                contentValues.put("hour", Integer.valueOf(appFilter2.getHour()));
                                sQLiteDatabase2.insert("appfilter_copy", (String) null, contentValues);
                                str3 = str14;
                                str2 = str15;
                                str4 = str16;
                                str = str17;
                            }
                        }
                        sQLiteDatabase2.execSQL("DROP TABLE appfilter;");
                        sQLiteDatabase2.execSQL("ALTER TABLE appfilter_copy RENAME TO appfilter;");
                        FLogger.INSTANCE.getLocal().d(un4.a, "Migration complete");
                    }
                }
                str = str5;
                str2 = str6;
                str3 = str7;
                str4 = str8;
                sQLiteDatabase2.execSQL("CREATE TABLE appfilter_copy (id INTEGER PRIMARY KEY AUTOINCREMENT, type VARCHAR, color VARCHAR, haptic VARCHAR, timestamp BIGINT, enabled INTEGER, name VARCHAR, isVibrationOnly INTEGER, deviceFamily INTEGER, hour INTEGER);");
                if (!arrayList.isEmpty()) {
                }
                sQLiteDatabase2.execSQL("DROP TABLE appfilter;");
                sQLiteDatabase2.execSQL("ALTER TABLE appfilter_copy RENAME TO appfilter;");
                FLogger.INSTANCE.getLocal().d(un4.a, "Migration complete");
            } catch (Exception e2) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str18 = un4.a;
                local2.e(str18, "Error inside " + un4.a + ".upgrade - e=" + e2);
            }
        }
    }

    @DexIgnore
    public un4(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @SuppressLint({"UseSparseArrays"})
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        Map<Integer, UpgradeCommand> dbUpgrades = un4.super.getDbUpgrades();
        if (dbUpgrades == null) {
            dbUpgrades = new HashMap<>();
        }
        dbUpgrades.put(2, new a());
        dbUpgrades.put(4, new b(this));
        return dbUpgrades;
    }
}
