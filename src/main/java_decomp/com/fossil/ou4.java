package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ou4 extends df<GoalTrackingData, a> {
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public b e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ /* synthetic */ ou4 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ou4$a$a")
        /* renamed from: com.fossil.ou4$a$a  reason: collision with other inner class name */
        public static final class C0033a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0033a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b a2;
                if (this.a.d.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1) {
                    a aVar = this.a;
                    GoalTrackingData a3 = ou4.a(aVar.d, aVar.getAdapterPosition());
                    if (a3 != null && (a2 = this.a.d.e) != null) {
                        wg6.a((Object) a3, "it1");
                        a2.a(a3);
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ou4 ou4, View view) {
            super(view);
            wg6.b(view, "view");
            this.d = ou4;
            this.a = (FlexibleTextView) view.findViewById(2131362440);
            this.b = (FlexibleTextView) view.findViewById(2131362398);
            this.c = (FlexibleTextView) view.findViewById(2131362322);
            this.c.setOnClickListener(new C0033a(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r9v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void a(GoalTrackingData goalTrackingData) {
            String str;
            wg6.b(goalTrackingData, "item");
            if (this.d.d == goalTrackingData.getTimezoneOffsetInSecond()) {
                str = "";
            } else if (goalTrackingData.getTimezoneOffsetInSecond() >= 0) {
                str = '+' + tk4.a(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
            } else {
                str = tk4.a(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
                wg6.a((Object) str, "NumberHelper.decimalForm\u2026ffsetInSecond / 3600F, 1)");
            }
            Object r1 = this.a;
            wg6.a((Object) r1, "mTvTime");
            nh6 nh6 = nh6.a;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131887302);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026ce, R.string.s_time_zone)");
            Object[] objArr = {bk4.a(goalTrackingData.getTrackedAt().getMillis(), goalTrackingData.getTimezoneOffsetInSecond()), str};
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            r1.setText(format);
            Object r9 = this.b;
            wg6.a((Object) r9, "mTvNoTime");
            r9.setVisibility(8);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(GoalTrackingData goalTrackingData);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ou4(b bVar, r24 r24) {
        super(r24);
        wg6.b(r24, "goalTrackingDataDiff");
        this.e = bVar;
        TimeZone timeZone = TimeZone.getDefault();
        wg6.a((Object) timeZone, "TimeZone.getDefault()");
        this.d = bk4.a(timeZone.getID(), true);
    }

    @DexIgnore
    public static final /* synthetic */ GoalTrackingData a(ou4 ou4, int i) {
        return (GoalTrackingData) ou4.getItem(i);
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558679, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026           parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        GoalTrackingData goalTrackingData = (GoalTrackingData) getItem(i);
        if (goalTrackingData != null) {
            aVar.a(goalTrackingData);
        }
    }
}
