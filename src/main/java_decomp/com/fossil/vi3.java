package com.fossil;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Shader;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vi3 {
    @DexIgnore
    public static /* final */ int[] h; // = new int[3];
    @DexIgnore
    public static /* final */ float[] i; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f};
    @DexIgnore
    public static /* final */ int[] j; // = new int[4];
    @DexIgnore
    public static /* final */ float[] k; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f};
    @DexIgnore
    public /* final */ Paint a;
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ Paint c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public /* final */ Path g;

    @DexIgnore
    public vi3() {
        this(-16777216);
    }

    @DexIgnore
    public void a(int i2) {
        this.d = f7.c(i2, 68);
        this.e = f7.c(i2, 20);
        this.f = f7.c(i2, 0);
    }

    @DexIgnore
    public vi3(int i2) {
        this.g = new Path();
        a(i2);
        this.b = new Paint(4);
        this.b.setStyle(Paint.Style.FILL);
        this.a = new Paint();
        this.a.setColor(this.d);
        this.c = new Paint(this.b);
    }

    @DexIgnore
    public void a(Canvas canvas, Matrix matrix, RectF rectF, int i2) {
        rectF.bottom += (float) i2;
        rectF.offset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) (-i2));
        int[] iArr = h;
        iArr[0] = this.f;
        iArr[1] = this.e;
        iArr[2] = this.d;
        Paint paint = this.c;
        float f2 = rectF.left;
        paint.setShader(new LinearGradient(f2, rectF.top, f2, rectF.bottom, iArr, i, Shader.TileMode.CLAMP));
        canvas.save();
        canvas.concat(matrix);
        canvas.drawRect(rectF, this.c);
        canvas.restore();
    }

    @DexIgnore
    public void a(Canvas canvas, Matrix matrix, RectF rectF, int i2, float f2, float f3) {
        RectF rectF2 = rectF;
        int i3 = i2;
        float f4 = f3;
        boolean z = f4 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        Path path = this.g;
        if (z) {
            int[] iArr = j;
            iArr[0] = 0;
            iArr[1] = this.f;
            iArr[2] = this.e;
            iArr[3] = this.d;
            float f5 = f2;
        } else {
            path.rewind();
            path.moveTo(rectF.centerX(), rectF.centerY());
            path.arcTo(rectF2, f2, f4);
            path.close();
            float f6 = (float) (-i3);
            rectF2.inset(f6, f6);
            int[] iArr2 = j;
            iArr2[0] = 0;
            iArr2[1] = this.d;
            iArr2[2] = this.e;
            iArr2[3] = this.f;
        }
        float width = 1.0f - (((float) i3) / (rectF.width() / 2.0f));
        float[] fArr = k;
        fArr[1] = width;
        fArr[2] = ((1.0f - width) / 2.0f) + width;
        this.b.setShader(new RadialGradient(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, j, k, Shader.TileMode.CLAMP));
        canvas.save();
        canvas.concat(matrix);
        if (!z) {
            canvas.clipPath(path, Region.Op.DIFFERENCE);
        } else {
            Canvas canvas2 = canvas;
        }
        canvas.drawArc(rectF, f2, f3, true, this.b);
        canvas.restore();
    }

    @DexIgnore
    public Paint a() {
        return this.a;
    }
}
