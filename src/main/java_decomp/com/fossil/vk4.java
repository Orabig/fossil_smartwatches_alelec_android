package com.fossil;

import android.util.Log;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vk4 {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ c[] c; // = {new c(this, d.INITIAL), new c(this, d.BEFORE), new c(this, d.AFTER)};
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<a> d; // = new CopyOnWriteArrayList<>();

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(g gVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c {
        @DexIgnore
        public e a;
        @DexIgnore
        public b b;
        @DexIgnore
        public Throwable c;
        @DexIgnore
        public f d; // = f.SUCCESS;

        @DexIgnore
        public c(vk4 vk4, d dVar) {
        }
    }

    @DexIgnore
    public enum d {
        INITIAL,
        BEFORE,
        AFTER
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ b a;
        @DexIgnore
        public /* final */ vk4 b;
        @DexIgnore
        public /* final */ d c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void run() {
                e eVar = e.this;
                eVar.b.a(eVar.c, eVar.a);
            }
        }

        @DexIgnore
        public e(b bVar, vk4 vk4, d dVar) {
            this.a = bVar;
            this.b = vk4;
            this.c = dVar;
        }

        @DexIgnore
        public void a(Executor executor) {
            executor.execute(new a());
        }

        @DexIgnore
        public void run() {
            this.a.run(new b.a(this, this.b));
        }
    }

    @DexIgnore
    public enum f {
        RUNNING,
        SUCCESS,
        FAILED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g {
        @DexIgnore
        public /* final */ f a;
        @DexIgnore
        public /* final */ f b;
        @DexIgnore
        public /* final */ f c;
        @DexIgnore
        public /* final */ Throwable[] d;

        @DexIgnore
        public g(f fVar, f fVar2, f fVar3, Throwable[] thArr) {
            this.a = fVar;
            this.b = fVar2;
            this.c = fVar3;
            this.d = thArr;
        }

        @DexIgnore
        public boolean a() {
            f fVar = this.a;
            f fVar2 = f.FAILED;
            return fVar == fVar2 || this.b == fVar2 || this.c == fVar2;
        }

        @DexIgnore
        public boolean b() {
            f fVar = this.a;
            f fVar2 = f.RUNNING;
            return fVar == fVar2 || this.b == fVar2 || this.c == fVar2;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || g.class != obj.getClass()) {
                return false;
            }
            g gVar = (g) obj;
            if (this.a == gVar.a && this.b == gVar.b && this.c == gVar.c) {
                return Arrays.equals(this.d, gVar.d);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + Arrays.hashCode(this.d);
        }

        @DexIgnore
        public String toString() {
            return "StatusReport{initial=" + this.a + ", before=" + this.b + ", after=" + this.c + ", mErrors=" + Arrays.toString(this.d) + '}';
        }

        @DexIgnore
        public Throwable a(d dVar) {
            return this.d[dVar.ordinal()];
        }
    }

    @DexIgnore
    public vk4(Executor executor) {
        this.b = executor;
    }

    @DexIgnore
    public boolean a(a aVar) {
        return this.d.add(aVar);
    }

    @DexIgnore
    public boolean b(a aVar) {
        return this.d.remove(aVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        if (r4 == null) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        new com.fossil.vk4.e(r7, r5, r6).run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        return true;
     */
    @DexIgnore
    public boolean a(d dVar, b bVar) {
        boolean z = !this.d.isEmpty();
        synchronized (this.a) {
            c cVar = this.c[dVar.ordinal()];
            if (cVar.b != null) {
                return false;
            }
            cVar.b = bVar;
            cVar.d = f.RUNNING;
            g gVar = null;
            cVar.a = null;
            cVar.c = null;
            if (z) {
                gVar = a();
            }
        }
    }

    @DexIgnore
    public boolean b() {
        int i;
        FLogger.INSTANCE.getLocal().d("PagingRequestHelper", "retryAllFailed");
        e[] eVarArr = new e[d.values().length];
        synchronized (this.a) {
            for (int i2 = 0; i2 < d.values().length; i2++) {
                eVarArr[i2] = this.c[i2].a;
                this.c[i2].a = null;
            }
        }
        boolean z = false;
        for (e eVar : eVarArr) {
            if (eVar != null) {
                eVar.a(this.b);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    @FunctionalInterface
    public interface b {
        @DexIgnore
        void run(a aVar);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a {
            @DexIgnore
            public /* final */ AtomicBoolean a; // = new AtomicBoolean();
            @DexIgnore
            public /* final */ e b;
            @DexIgnore
            public /* final */ vk4 c;

            @DexIgnore
            public a(e eVar, vk4 vk4) {
                this.b = eVar;
                this.c = vk4;
            }

            @DexIgnore
            public final void a() {
                Log.d("PagingRequestHelper", "recordSuccess");
                if (this.a.compareAndSet(false, true)) {
                    this.c.a(this.b, (Throwable) null);
                    return;
                }
                throw new IllegalStateException("already called recordSuccess or recordFailure");
            }

            @DexIgnore
            public final void a(Throwable th) {
                Log.d("PagingRequestHelper", "recordFailure");
                if (th == null) {
                    throw new IllegalArgumentException("You must provide a throwable describing the error to record the failure");
                } else if (this.a.compareAndSet(false, true)) {
                    this.c.a(this.b, th);
                } else {
                    throw new IllegalStateException("already called recordSuccess or recordFailure");
                }
            }
        }
    }

    @DexIgnore
    public final g a() {
        c[] cVarArr = this.c;
        return new g(a(d.INITIAL), a(d.BEFORE), a(d.AFTER), new Throwable[]{cVarArr[0].c, cVarArr[1].c, cVarArr[2].c});
    }

    @DexIgnore
    public final f a(d dVar) {
        return this.c[dVar.ordinal()].d;
    }

    @DexIgnore
    public void a(e eVar, Throwable th) {
        g gVar;
        boolean z = th == null;
        boolean isEmpty = true ^ this.d.isEmpty();
        synchronized (this.a) {
            c cVar = this.c[eVar.c.ordinal()];
            gVar = null;
            cVar.b = null;
            cVar.c = th;
            if (z) {
                cVar.a = null;
                cVar.d = f.SUCCESS;
            } else {
                cVar.a = eVar;
                cVar.d = f.FAILED;
            }
            if (isEmpty) {
                gVar = a();
            }
        }
        if (gVar != null) {
            a(gVar);
        }
    }

    @DexIgnore
    public final void a(g gVar) {
        Iterator<a> it = this.d.iterator();
        while (it.hasNext()) {
            it.next().a(gVar);
        }
    }
}
