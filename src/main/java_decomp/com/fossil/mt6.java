package com.fossil;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mt6 implements Serializable, Comparable<mt6> {
    @DexIgnore
    public static /* final */ mt6 EMPTY; // = of(new byte[0]);
    @DexIgnore
    public static /* final */ char[] HEX_DIGITS; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public /* final */ byte[] data;
    @DexIgnore
    public transient int hashCode;
    @DexIgnore
    public transient String utf8;

    @DexIgnore
    public mt6(byte[] bArr) {
        this.data = bArr;
    }

    @DexIgnore
    public static int codePointIndexToCharIndex(String str, int i) {
        int length = str.length();
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            if (i3 == i) {
                return i2;
            }
            int codePointAt = str.codePointAt(i2);
            if ((Character.isISOControl(codePointAt) && codePointAt != 10 && codePointAt != 13) || codePointAt == 65533) {
                return -1;
            }
            i3++;
            i2 += Character.charCount(codePointAt);
        }
        return str.length();
    }

    @DexIgnore
    public static mt6 decodeBase64(String str) {
        if (str != null) {
            byte[] a = it6.a(str);
            if (a != null) {
                return new mt6(a);
            }
            return null;
        }
        throw new IllegalArgumentException("base64 == null");
    }

    @DexIgnore
    public static mt6 decodeHex(String str) {
        if (str == null) {
            throw new IllegalArgumentException("hex == null");
        } else if (str.length() % 2 == 0) {
            byte[] bArr = new byte[(str.length() / 2)];
            for (int i = 0; i < bArr.length; i++) {
                int i2 = i * 2;
                bArr[i] = (byte) ((a(str.charAt(i2)) << 4) + a(str.charAt(i2 + 1)));
            }
            return of(bArr);
        } else {
            throw new IllegalArgumentException("Unexpected hex string: " + str);
        }
    }

    @DexIgnore
    public static mt6 encodeString(String str, Charset charset) {
        if (str == null) {
            throw new IllegalArgumentException("s == null");
        } else if (charset != null) {
            return new mt6(str.getBytes(charset));
        } else {
            throw new IllegalArgumentException("charset == null");
        }
    }

    @DexIgnore
    public static mt6 encodeUtf8(String str) {
        if (str != null) {
            mt6 mt6 = new mt6(str.getBytes(bu6.a));
            mt6.utf8 = str;
            return mt6;
        }
        throw new IllegalArgumentException("s == null");
    }

    @DexIgnore
    public static mt6 of(byte... bArr) {
        if (bArr != null) {
            return new mt6((byte[]) bArr.clone());
        }
        throw new IllegalArgumentException("data == null");
    }

    @DexIgnore
    public static mt6 read(InputStream inputStream, int i) throws IOException {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (i >= 0) {
            byte[] bArr = new byte[i];
            int i2 = 0;
            while (i2 < i) {
                int read = inputStream.read(bArr, i2, i - i2);
                if (read != -1) {
                    i2 += read;
                } else {
                    throw new EOFException();
                }
            }
            return new mt6(bArr);
        } else {
            throw new IllegalArgumentException("byteCount < 0: " + i);
        }
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws IOException {
        mt6 read = read(objectInputStream, objectInputStream.readInt());
        try {
            Field declaredField = mt6.class.getDeclaredField("data");
            declaredField.setAccessible(true);
            declaredField.set(this, read.data);
        } catch (NoSuchFieldException unused) {
            throw new AssertionError();
        } catch (IllegalAccessException unused2) {
            throw new AssertionError();
        }
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(this.data.length);
        objectOutputStream.write(this.data);
    }

    @DexIgnore
    public final mt6 a(String str) {
        try {
            return of(MessageDigest.getInstance(str).digest(this.data));
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public ByteBuffer asByteBuffer() {
        return ByteBuffer.wrap(this.data).asReadOnlyBuffer();
    }

    @DexIgnore
    public String base64() {
        return it6.a(this.data);
    }

    @DexIgnore
    public String base64Url() {
        return it6.b(this.data);
    }

    @DexIgnore
    public final boolean endsWith(mt6 mt6) {
        return rangeEquals(size() - mt6.size(), mt6, 0, mt6.size());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof mt6) {
            mt6 mt6 = (mt6) obj;
            int size = mt6.size();
            byte[] bArr = this.data;
            if (size != bArr.length || !mt6.rangeEquals(0, bArr, 0, bArr.length)) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public byte getByte(int i) {
        return this.data[i];
    }

    @DexIgnore
    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int hashCode2 = Arrays.hashCode(this.data);
        this.hashCode = hashCode2;
        return hashCode2;
    }

    @DexIgnore
    public String hex() {
        byte[] bArr = this.data;
        char[] cArr = new char[(bArr.length * 2)];
        int i = 0;
        for (byte b : bArr) {
            int i2 = i + 1;
            char[] cArr2 = HEX_DIGITS;
            cArr[i] = cArr2[(b >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = cArr2[b & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY];
        }
        return new String(cArr);
    }

    @DexIgnore
    public mt6 hmacSha1(mt6 mt6) {
        return a("HmacSHA1", mt6);
    }

    @DexIgnore
    public mt6 hmacSha256(mt6 mt6) {
        return a("HmacSHA256", mt6);
    }

    @DexIgnore
    public mt6 hmacSha512(mt6 mt6) {
        return a("HmacSHA512", mt6);
    }

    @DexIgnore
    public final int indexOf(mt6 mt6) {
        return indexOf(mt6.internalArray(), 0);
    }

    @DexIgnore
    public byte[] internalArray() {
        return this.data;
    }

    @DexIgnore
    public final int lastIndexOf(mt6 mt6) {
        return lastIndexOf(mt6.internalArray(), size());
    }

    @DexIgnore
    public mt6 md5() {
        return a("MD5");
    }

    @DexIgnore
    public boolean rangeEquals(int i, mt6 mt6, int i2, int i3) {
        return mt6.rangeEquals(i2, this.data, i, i3);
    }

    @DexIgnore
    public mt6 sha1() {
        return a("SHA-1");
    }

    @DexIgnore
    public mt6 sha256() {
        return a("SHA-256");
    }

    @DexIgnore
    public mt6 sha512() {
        return a("SHA-512");
    }

    @DexIgnore
    public int size() {
        return this.data.length;
    }

    @DexIgnore
    public final boolean startsWith(mt6 mt6) {
        return rangeEquals(0, mt6, 0, mt6.size());
    }

    @DexIgnore
    public String string(Charset charset) {
        if (charset != null) {
            return new String(this.data, charset);
        }
        throw new IllegalArgumentException("charset == null");
    }

    @DexIgnore
    public mt6 substring(int i) {
        return substring(i, this.data.length);
    }

    @DexIgnore
    public mt6 toAsciiLowercase() {
        int i = 0;
        while (true) {
            byte[] bArr = this.data;
            if (i >= bArr.length) {
                return this;
            }
            byte b = bArr[i];
            if (b < 65 || b > 90) {
                i++;
            } else {
                byte[] bArr2 = (byte[]) bArr.clone();
                bArr2[i] = (byte) (b + 32);
                for (int i2 = i + 1; i2 < bArr2.length; i2++) {
                    byte b2 = bArr2[i2];
                    if (b2 >= 65 && b2 <= 90) {
                        bArr2[i2] = (byte) (b2 + 32);
                    }
                }
                return new mt6(bArr2);
            }
        }
    }

    @DexIgnore
    public mt6 toAsciiUppercase() {
        int i = 0;
        while (true) {
            byte[] bArr = this.data;
            if (i >= bArr.length) {
                return this;
            }
            byte b = bArr[i];
            if (b < 97 || b > 122) {
                i++;
            } else {
                byte[] bArr2 = (byte[]) bArr.clone();
                bArr2[i] = (byte) (b - 32);
                for (int i2 = i + 1; i2 < bArr2.length; i2++) {
                    byte b2 = bArr2[i2];
                    if (b2 >= 97 && b2 <= 122) {
                        bArr2[i2] = (byte) (b2 - 32);
                    }
                }
                return new mt6(bArr2);
            }
        }
    }

    @DexIgnore
    public byte[] toByteArray() {
        return (byte[]) this.data.clone();
    }

    @DexIgnore
    public String toString() {
        if (this.data.length == 0) {
            return "[size=0]";
        }
        String utf82 = utf8();
        int codePointIndexToCharIndex = codePointIndexToCharIndex(utf82, 64);
        if (codePointIndexToCharIndex != -1) {
            String replace = utf82.substring(0, codePointIndexToCharIndex).replace("\\", "\\\\").replace("\n", "\\n").replace("\r", "\\r");
            if (codePointIndexToCharIndex < utf82.length()) {
                return "[size=" + this.data.length + " text=" + replace + "\u2026]";
            }
            return "[text=" + replace + "]";
        } else if (this.data.length <= 64) {
            return "[hex=" + hex() + "]";
        } else {
            return "[size=" + this.data.length + " hex=" + substring(0, 64).hex() + "\u2026]";
        }
    }

    @DexIgnore
    public String utf8() {
        String str = this.utf8;
        if (str != null) {
            return str;
        }
        String str2 = new String(this.data, bu6.a);
        this.utf8 = str2;
        return str2;
    }

    @DexIgnore
    public void write(OutputStream outputStream) throws IOException {
        if (outputStream != null) {
            outputStream.write(this.data);
            return;
        }
        throw new IllegalArgumentException("out == null");
    }

    @DexIgnore
    public int compareTo(mt6 mt6) {
        int size = size();
        int size2 = mt6.size();
        int min = Math.min(size, size2);
        int i = 0;
        while (i < min) {
            byte b = getByte(i) & 255;
            byte b2 = mt6.getByte(i) & 255;
            if (b == b2) {
                i++;
            } else if (b < b2) {
                return -1;
            } else {
                return 1;
            }
        }
        if (size == size2) {
            return 0;
        }
        if (size < size2) {
            return -1;
        }
        return 1;
    }

    @DexIgnore
    public final boolean endsWith(byte[] bArr) {
        return rangeEquals(size() - bArr.length, bArr, 0, bArr.length);
    }

    @DexIgnore
    public final int indexOf(mt6 mt6, int i) {
        return indexOf(mt6.internalArray(), i);
    }

    @DexIgnore
    public final int lastIndexOf(mt6 mt6, int i) {
        return lastIndexOf(mt6.internalArray(), i);
    }

    @DexIgnore
    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        if (i >= 0) {
            byte[] bArr2 = this.data;
            return i <= bArr2.length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && bu6.a(bArr2, i, bArr, i2, i3);
        }
    }

    @DexIgnore
    public final boolean startsWith(byte[] bArr) {
        return rangeEquals(0, bArr, 0, bArr.length);
    }

    @DexIgnore
    public mt6 substring(int i, int i2) {
        if (i >= 0) {
            byte[] bArr = this.data;
            if (i2 <= bArr.length) {
                int i3 = i2 - i;
                if (i3 < 0) {
                    throw new IllegalArgumentException("endIndex < beginIndex");
                } else if (i == 0 && i2 == bArr.length) {
                    return this;
                } else {
                    byte[] bArr2 = new byte[i3];
                    System.arraycopy(this.data, i, bArr2, 0, i3);
                    return new mt6(bArr2);
                }
            } else {
                throw new IllegalArgumentException("endIndex > length(" + this.data.length + ")");
            }
        } else {
            throw new IllegalArgumentException("beginIndex < 0");
        }
    }

    @DexIgnore
    public static mt6 of(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            bu6.a((long) bArr.length, (long) i, (long) i2);
            byte[] bArr2 = new byte[i2];
            System.arraycopy(bArr, i, bArr2, 0, i2);
            return new mt6(bArr2);
        }
        throw new IllegalArgumentException("data == null");
    }

    @DexIgnore
    public final mt6 a(String str, mt6 mt6) {
        try {
            Mac instance = Mac.getInstance(str);
            instance.init(new SecretKeySpec(mt6.toByteArray(), str));
            return of(instance.doFinal(this.data));
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        } catch (InvalidKeyException e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    public final int indexOf(byte[] bArr) {
        return indexOf(bArr, 0);
    }

    @DexIgnore
    public final int lastIndexOf(byte[] bArr) {
        return lastIndexOf(bArr, size());
    }

    @DexIgnore
    public void write(jt6 jt6) {
        byte[] bArr = this.data;
        jt6.write(bArr, 0, bArr.length);
    }

    @DexIgnore
    public int indexOf(byte[] bArr, int i) {
        int length = this.data.length - bArr.length;
        for (int max = Math.max(i, 0); max <= length; max++) {
            if (bu6.a(this.data, max, bArr, 0, bArr.length)) {
                return max;
            }
        }
        return -1;
    }

    @DexIgnore
    public int lastIndexOf(byte[] bArr, int i) {
        for (int min = Math.min(i, this.data.length - bArr.length); min >= 0; min--) {
            if (bu6.a(this.data, min, bArr, 0, bArr.length)) {
                return min;
            }
        }
        return -1;
    }

    @DexIgnore
    public static int a(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        char c2 = 'a';
        if (c < 'a' || c > 'f') {
            c2 = 'A';
            if (c < 'A' || c > 'F') {
                throw new IllegalArgumentException("Unexpected hex digit: " + c);
            }
        }
        return (c - c2) + 10;
    }

    @DexIgnore
    public static mt6 of(ByteBuffer byteBuffer) {
        if (byteBuffer != null) {
            byte[] bArr = new byte[byteBuffer.remaining()];
            byteBuffer.get(bArr);
            return new mt6(bArr);
        }
        throw new IllegalArgumentException("data == null");
    }
}
