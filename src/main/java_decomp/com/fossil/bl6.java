package com.fossil;

import com.fossil.bl6;
import java.lang.Throwable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bl6<T extends Throwable & bl6<T>> {
    @DexIgnore
    T createCopy();
}
