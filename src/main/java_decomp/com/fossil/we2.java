package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.rv1;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we2 extends qe2<cd2> {
    @DexIgnore
    public static /* final */ je2 E; // = je2.FIT_CONFIG;
    @DexIgnore
    public static /* final */ rv1.g<we2> F; // = new rv1.g<>();
    @DexIgnore
    public static /* final */ rv1<rv1.d.C0044d> G; // = new rv1<>("Fitness.CONFIG_API", new ye2(), F);

    /*
    static {
        new rv1("Fitness.CONFIG_CLIENT", new af2(), F);
    }
    */

    @DexIgnore
    public we2(Context context, Looper looper, e12 e12, wv1.b bVar, wv1.c cVar) {
        super(context, looper, E, bVar, cVar, e12);
    }

    @DexIgnore
    public final String A() {
        return "com.google.android.gms.fitness.ConfigApi";
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitConfigApi");
        if (queryLocalInterface instanceof cd2) {
            return (cd2) queryLocalInterface;
        }
        return new bd2(iBinder);
    }

    @DexIgnore
    public final int j() {
        return nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.internal.IGoogleFitConfigApi";
    }
}
