package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uj6 extends tj6 {
    @DexIgnore
    public static final <T> void a(Appendable appendable, T t, hg6<? super T, ? extends CharSequence> hg6) {
        wg6.b(appendable, "$this$appendElement");
        if (hg6 != null) {
            appendable.append((CharSequence) hg6.invoke(t));
            return;
        }
        if (t != null ? t instanceof CharSequence : true) {
            appendable.append((CharSequence) t);
        } else if (t instanceof Character) {
            appendable.append(((Character) t).charValue());
        } else {
            appendable.append(String.valueOf(t));
        }
    }
}
