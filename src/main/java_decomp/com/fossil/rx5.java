package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rx5 {
    @DexIgnore
    public static rx5 a;

    @DexIgnore
    public static SharedPreferences w(Context context) {
        return context.getSharedPreferences(Constants.FRAMEWORKS_SHAREPREFS, 0);
    }

    @DexIgnore
    public String a(Context context) {
        return a(context, "com.misfit.frameworks.profile.createdAt");
    }

    @DexIgnore
    public String b(Context context) {
        return a(context, "com.misfit.frameworks.profile.integrations");
    }

    @DexIgnore
    public String c(Context context) {
        return a(context, "com.misfit.frameworks.profile.registration");
    }

    @DexIgnore
    public String d(Context context) {
        return a(context, "com.misfit.frameworks.profile.registerDate");
    }

    @DexIgnore
    public String e(Context context) {
        return a(context, "com.misfit.frameworks.profile.updateAt");
    }

    @DexIgnore
    public String f(Context context) {
        return a(context, "com.misfit.frameworks.profile.accessToken");
    }

    @DexIgnore
    public String g(Context context) {
        return a(context, "com.misfit.frameworks.profile.authType");
    }

    @DexIgnore
    public String h(Context context) {
        return a(context, "com.misfit.frameworks.profile.birthday");
    }

    @DexIgnore
    public String i(Context context) {
        return a(context, "com.misfit.frameworks.profile.email");
    }

    @DexIgnore
    public String j(Context context) {
        return a(context, "com.misfit.frameworks.profile.firstname");
    }

    @DexIgnore
    public rh4 k(Context context) {
        return rh4.Companion.a(a(context, "com.misfit.frameworks.profile.gender"));
    }

    @DexIgnore
    public String l(Context context) {
        return a(context, "com.misfit.frameworks.profile.height");
    }

    @DexIgnore
    public String m(Context context) {
        return a(context, "com.misfit.frameworks.profile.userId");
    }

    @DexIgnore
    public String n(Context context) {
        return a(context, "com.misfit.frameworks.profile.lastname");
    }

    @DexIgnore
    public String o(Context context) {
        return a(context, "com.misfit.frameworks.profile.profilePic");
    }

    @DexIgnore
    public String p(Context context) {
        return a(context, "com.misfit.frameworks.profile.units.distance");
    }

    @DexIgnore
    public String q(Context context) {
        return a(context, "com.misfit.frameworks.profile.units.height");
    }

    @DexIgnore
    public String r(Context context) {
        return a(context, "com.misfit.frameworks.profile.units.weight");
    }

    @DexIgnore
    public String s(Context context) {
        return a(context, "com.misfit.frameworks.profile.weight");
    }

    @DexIgnore
    public boolean t(Context context) {
        String a2 = a(context, "com.misfit.frameworks.onboarding.all");
        if (!TextUtils.isEmpty(a2)) {
            return Boolean.valueOf(a2).booleanValue();
        }
        return false;
    }

    @DexIgnore
    public boolean u(Context context) {
        return a(context, "com.misfit.frameworks.profile.diagnosticEnable", true);
    }

    @DexIgnore
    public boolean v(Context context) {
        return Boolean.valueOf(a(context, "com.misfit.frameworks.profile.emailOptIn")).booleanValue();
    }

    @DexIgnore
    public static synchronized rx5 a() {
        rx5 rx5;
        synchronized (rx5.class) {
            if (a == null) {
                a = new rx5();
            }
            rx5 = a;
        }
        return rx5;
    }

    @DexIgnore
    public final String a(Context context, String str) {
        SharedPreferences w = w(context);
        if (w != null) {
            return w.getString(str, "");
        }
        return "";
    }

    @DexIgnore
    public final boolean a(Context context, String str, boolean z) {
        SharedPreferences w = w(context);
        return w != null && w.getBoolean(str, z);
    }
}
