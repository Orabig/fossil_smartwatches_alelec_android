package com.fossil;

import android.annotation.TargetApi;
import android.os.Build;
import com.fossil.wearables.fsl.countdown.CountDown;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c20 implements ia6<a20> {
    @DexIgnore
    /* renamed from: b */
    public byte[] a(a20 a20) throws IOException {
        return a(a20).toString().getBytes("UTF-8");
    }

    @DexIgnore
    @TargetApi(9)
    public JSONObject a(a20 a20) throws IOException {
        try {
            JSONObject jSONObject = new JSONObject();
            b20 b20 = a20.a;
            jSONObject.put("appBundleId", b20.a);
            jSONObject.put("executionId", b20.b);
            jSONObject.put("installationId", b20.c);
            jSONObject.put("limitAdTrackingEnabled", b20.d);
            jSONObject.put("betaDeviceToken", b20.e);
            jSONObject.put("buildId", b20.f);
            jSONObject.put("osVersion", b20.g);
            jSONObject.put(CountDown.COLUMN_DEVICE_MODEL, b20.h);
            jSONObject.put("appVersionCode", b20.i);
            jSONObject.put("appVersionName", b20.j);
            jSONObject.put("timestamp", a20.b);
            jSONObject.put("type", a20.c.toString());
            if (a20.d != null) {
                jSONObject.put("details", new JSONObject(a20.d));
            }
            jSONObject.put("customType", a20.e);
            if (a20.f != null) {
                jSONObject.put("customAttributes", new JSONObject(a20.f));
            }
            jSONObject.put("predefinedType", a20.g);
            if (a20.h != null) {
                jSONObject.put("predefinedAttributes", new JSONObject(a20.h));
            }
            return jSONObject;
        } catch (JSONException e) {
            if (Build.VERSION.SDK_INT >= 9) {
                throw new IOException(e.getMessage(), e);
            }
            throw new IOException(e.getMessage());
        }
    }
}
