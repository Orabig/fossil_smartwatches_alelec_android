package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq0 extends xg6 implements hg6<km1, Boolean> {
    @DexIgnore
    public static /* final */ fq0 a; // = new fq0();

    @DexIgnore
    public fq0() {
        super(1);
    }

    @DexIgnore
    public Object invoke(Object obj) {
        km1 km1 = (km1) obj;
        sk1 sk1 = km1.b;
        return Boolean.valueOf(sk1 == sk1.INTERRUPTED || sk1 == sk1.CONNECTION_DROPPED || sk1 == sk1.BLUETOOTH_OFF || km1.c.d.c.a == x11.GATT_NULL);
    }
}
