package com.fossil;

import android.content.Context;
import com.fossil.nu4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.uirenew.permission.AllowNotificationServiceFragment;
import com.portfolio.platform.uirenew.permission.PermissionFragment;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt5 extends ft5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ List<nu4.c> e; // = new ArrayList();
    @DexIgnore
    public /* final */ gt5 f;
    @DexIgnore
    public /* final */ ArrayList<Integer> g;
    @DexIgnore
    public /* final */ an4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = kt5.class.getSimpleName();
        wg6.a((Object) simpleName, "PermissionPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public kt5(gt5 gt5, ArrayList<Integer> arrayList, an4 an4) {
        wg6.b(gt5, "mView");
        wg6.b(arrayList, "mListPerms");
        wg6.b(an4, "mSharedPreferencesManager");
        this.f = gt5;
        this.g = arrayList;
        this.h = an4;
    }

    @DexIgnore
    public void f() {
        Context context;
        FLogger.INSTANCE.getLocal().d(i, "start");
        gt5 gt5 = this.f;
        if (gt5 instanceof PermissionFragment) {
            context = ((PermissionFragment) gt5).getContext();
        } else if (gt5 != null) {
            context = ((AllowNotificationServiceFragment) gt5).getContext();
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.AllowNotificationServiceFragment");
        }
        if (this.e.isEmpty()) {
            for (Number intValue : this.g) {
                int intValue2 = intValue.intValue();
                pc6<String, String, String> b = xm4.d.b(intValue2);
                this.e.add(new nu4.c(intValue2, b.component1(), b.component2(), xm4.d.a(intValue2), b.component3(), xm4.d.a(context, intValue2)));
            }
        } else {
            for (nu4.c cVar : this.e) {
                cVar.a(xm4.d.a(context, cVar.d()));
            }
        }
        List<nu4.c> list = this.e;
        int i2 = 0;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (nu4.c b2 : list) {
                if (b2.b() && (i2 = i2 + 1) < 0) {
                    qd6.b();
                    throw null;
                }
            }
        }
        if (i2 == this.e.size()) {
            this.f.close();
        } else {
            this.f.t(this.e);
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "stop");
    }

    @DexIgnore
    public void h() {
        this.h.q(false);
        this.f.close();
    }

    @DexIgnore
    public void i() {
        this.h.r(false);
        this.f.close();
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }
}
