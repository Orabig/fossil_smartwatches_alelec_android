package com.fossil;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rh implements li, ki {
    @DexIgnore
    public static /* final */ TreeMap<Integer, rh> i; // = new TreeMap<>();
    @DexIgnore
    public volatile String a;
    @DexIgnore
    public /* final */ long[] b;
    @DexIgnore
    public /* final */ double[] c;
    @DexIgnore
    public /* final */ String[] d;
    @DexIgnore
    public /* final */ byte[][] e;
    @DexIgnore
    public /* final */ int[] f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ki {
        @DexIgnore
        public /* final */ /* synthetic */ rh a;

        @DexIgnore
        public a(rh rhVar) {
            this.a = rhVar;
        }

        @DexIgnore
        public void a(int i) {
            this.a.a(i);
        }

        @DexIgnore
        public void close() {
        }

        @DexIgnore
        public void a(int i, long j) {
            this.a.a(i, j);
        }

        @DexIgnore
        public void a(int i, double d) {
            this.a.a(i, d);
        }

        @DexIgnore
        public void a(int i, String str) {
            this.a.a(i, str);
        }

        @DexIgnore
        public void a(int i, byte[] bArr) {
            this.a.a(i, bArr);
        }
    }

    @DexIgnore
    public rh(int i2) {
        this.g = i2;
        int i3 = i2 + 1;
        this.f = new int[i3];
        this.b = new long[i3];
        this.c = new double[i3];
        this.d = new String[i3];
        this.e = new byte[i3][];
    }

    @DexIgnore
    public static rh a(li liVar) {
        rh b2 = b(liVar.b(), liVar.a());
        liVar.a(new a(b2));
        return b2;
    }

    @DexIgnore
    public static rh b(String str, int i2) {
        synchronized (i) {
            Map.Entry<Integer, rh> ceilingEntry = i.ceilingEntry(Integer.valueOf(i2));
            if (ceilingEntry != null) {
                i.remove(ceilingEntry.getKey());
                rh value = ceilingEntry.getValue();
                value.a(str, i2);
                return value;
            }
            rh rhVar = new rh(i2);
            rhVar.a(str, i2);
            return rhVar;
        }
    }

    @DexIgnore
    public static void d() {
        if (i.size() > 15) {
            int size = i.size() - 10;
            Iterator<Integer> it = i.descendingKeySet().iterator();
            while (true) {
                int i2 = size - 1;
                if (size > 0) {
                    it.next();
                    it.remove();
                    size = i2;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public void c() {
        synchronized (i) {
            i.put(Integer.valueOf(this.g), this);
            d();
        }
    }

    @DexIgnore
    public void close() {
    }

    @DexIgnore
    public void a(String str, int i2) {
        this.a = str;
        this.h = i2;
    }

    @DexIgnore
    public int a() {
        return this.h;
    }

    @DexIgnore
    public void a(ki kiVar) {
        for (int i2 = 1; i2 <= this.h; i2++) {
            int i3 = this.f[i2];
            if (i3 == 1) {
                kiVar.a(i2);
            } else if (i3 == 2) {
                kiVar.a(i2, this.b[i2]);
            } else if (i3 == 3) {
                kiVar.a(i2, this.c[i2]);
            } else if (i3 == 4) {
                kiVar.a(i2, this.d[i2]);
            } else if (i3 == 5) {
                kiVar.a(i2, this.e[i2]);
            }
        }
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public void a(int i2) {
        this.f[i2] = 1;
    }

    @DexIgnore
    public void a(int i2, long j) {
        this.f[i2] = 2;
        this.b[i2] = j;
    }

    @DexIgnore
    public void a(int i2, double d2) {
        this.f[i2] = 3;
        this.c[i2] = d2;
    }

    @DexIgnore
    public void a(int i2, String str) {
        this.f[i2] = 4;
        this.d[i2] = str;
    }

    @DexIgnore
    public void a(int i2, byte[] bArr) {
        this.f[i2] = 5;
        this.e[i2] = bArr;
    }

    @DexIgnore
    public void a(rh rhVar) {
        int a2 = rhVar.a() + 1;
        System.arraycopy(rhVar.f, 0, this.f, 0, a2);
        System.arraycopy(rhVar.b, 0, this.b, 0, a2);
        System.arraycopy(rhVar.d, 0, this.d, 0, a2);
        System.arraycopy(rhVar.e, 0, this.e, 0, a2);
        System.arraycopy(rhVar.c, 0, this.c, 0, a2);
    }
}
