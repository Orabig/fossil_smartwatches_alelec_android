package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp6 extends hp6 {
    @DexIgnore
    public static /* final */ dl6 f;
    @DexIgnore
    public static /* final */ gp6 g;

    /*
    static {
        gp6 gp6 = new gp6();
        g = gp6;
        f = gp6.b(xo6.a("kotlinx.coroutines.io.parallelism", ci6.a(64, vo6.a()), 0, 0, 12, (Object) null));
    }
    */

    @DexIgnore
    public gp6() {
        super(0, 0, (String) null, 7, (qg6) null);
    }

    @DexIgnore
    public void close() {
        throw new UnsupportedOperationException("DefaultDispatcher cannot be closed");
    }

    @DexIgnore
    public final dl6 p() {
        return f;
    }

    @DexIgnore
    public String toString() {
        return "DefaultDispatcher";
    }
}
