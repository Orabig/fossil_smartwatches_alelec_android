package com.fossil;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mg6 implements ei6, Serializable {
    @DexIgnore
    public static /* final */ Object NO_RECEIVER; // = a.a;
    @DexIgnore
    public /* final */ Object receiver;
    @DexIgnore
    public transient ei6 reflected;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Serializable {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        private Object readResolve() throws ObjectStreamException {
            return a;
        }
    }

    @DexIgnore
    public mg6() {
        this(NO_RECEIVER);
    }

    @DexIgnore
    public Object call(Object... objArr) {
        return getReflected().call(objArr);
    }

    @DexIgnore
    public Object callBy(Map map) {
        return getReflected().callBy(map);
    }

    @DexIgnore
    public ei6 compute() {
        ei6 ei6 = this.reflected;
        if (ei6 != null) {
            return ei6;
        }
        ei6 computeReflected = computeReflected();
        this.reflected = computeReflected;
        return computeReflected;
    }

    @DexIgnore
    public abstract ei6 computeReflected();

    @DexIgnore
    public List<Annotation> getAnnotations() {
        return getReflected().getAnnotations();
    }

    @DexIgnore
    public Object getBoundReceiver() {
        return this.receiver;
    }

    @DexIgnore
    public String getName() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    public hi6 getOwner() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    public List<Object> getParameters() {
        return getReflected().getParameters();
    }

    @DexIgnore
    public ei6 getReflected() {
        ei6 compute = compute();
        if (compute != this) {
            return compute;
        }
        throw new fg6();
    }

    @DexIgnore
    public oi6 getReturnType() {
        return getReflected().getReturnType();
    }

    @DexIgnore
    public String getSignature() {
        throw new AbstractMethodError();
    }

    @DexIgnore
    public List<Object> getTypeParameters() {
        return getReflected().getTypeParameters();
    }

    @DexIgnore
    public pi6 getVisibility() {
        return getReflected().getVisibility();
    }

    @DexIgnore
    public boolean isAbstract() {
        return getReflected().isAbstract();
    }

    @DexIgnore
    public boolean isFinal() {
        return getReflected().isFinal();
    }

    @DexIgnore
    public boolean isOpen() {
        return getReflected().isOpen();
    }

    @DexIgnore
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }

    @DexIgnore
    public mg6(Object obj) {
        this.receiver = obj;
    }
}
