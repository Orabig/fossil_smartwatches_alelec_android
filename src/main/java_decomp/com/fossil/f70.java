package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum f70 {
    KM(0),
    MILE(1);
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final f70 a(int i) {
            for (f70 f70 : f70.values()) {
                if (f70.a() == i) {
                    return f70;
                }
            }
            return null;
        }
    }

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public f70(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }
}
