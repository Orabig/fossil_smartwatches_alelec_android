package com.fossil;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y73 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ Uri b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ v73 e;

    @DexIgnore
    public y73(v73 v73, boolean z, Uri uri, String str, String str2) {
        this.e = v73;
        this.a = z;
        this.b = uri;
        this.c = str;
        this.d = str2;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.a, this.b, this.c, this.d);
    }
}
