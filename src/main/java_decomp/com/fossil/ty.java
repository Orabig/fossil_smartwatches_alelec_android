package com.fossil;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ty implements ny {
    @DexIgnore
    public /* final */ Set<yz<?>> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public void a(yz<?> yzVar) {
        this.a.add(yzVar);
    }

    @DexIgnore
    public void b(yz<?> yzVar) {
        this.a.remove(yzVar);
    }

    @DexIgnore
    public void c() {
        for (T c : r00.a(this.a)) {
            c.c();
        }
    }

    @DexIgnore
    public void e() {
        this.a.clear();
    }

    @DexIgnore
    public List<yz<?>> f() {
        return r00.a(this.a);
    }

    @DexIgnore
    public void a() {
        for (T a2 : r00.a(this.a)) {
            a2.a();
        }
    }

    @DexIgnore
    public void b() {
        for (T b : r00.a(this.a)) {
            b.b();
        }
    }
}
