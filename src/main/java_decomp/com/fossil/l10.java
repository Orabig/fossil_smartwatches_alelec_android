package com.fossil;

import android.content.Context;
import com.fossil.a20;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l10 implements z10 {
    @DexIgnore
    public /* final */ i86 a;
    @DexIgnore
    public /* final */ va6 b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ w10 d;
    @DexIgnore
    public /* final */ ScheduledExecutorService e;
    @DexIgnore
    public /* final */ AtomicReference<ScheduledFuture<?>> f; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ b20 g;
    @DexIgnore
    public /* final */ o10 h;
    @DexIgnore
    public na6 i;
    @DexIgnore
    public x86 j; // = new x86();
    @DexIgnore
    public m10 k; // = new r10();
    @DexIgnore
    public boolean l; // = true;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public volatile int n; // = -1;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;

    @DexIgnore
    public l10(i86 i86, Context context, ScheduledExecutorService scheduledExecutorService, w10 w10, va6 va6, b20 b20, o10 o10) {
        this.a = i86;
        this.c = context;
        this.e = scheduledExecutorService;
        this.d = w10;
        this.b = va6;
        this.g = b20;
        this.h = o10;
    }

    @DexIgnore
    public void a(fb6 fb6, String str) {
        String str2;
        String str3;
        this.i = g10.a(new x10(this.a, str, fb6.a, this.b, this.j.d(this.c)));
        this.d.a(fb6);
        this.o = fb6.e;
        this.p = fb6.f;
        l86 g2 = c86.g();
        StringBuilder sb = new StringBuilder();
        sb.append("Firebase analytics forwarding ");
        String str4 = "enabled";
        sb.append(this.o ? str4 : "disabled");
        g2.d("Answers", sb.toString());
        l86 g3 = c86.g();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Firebase analytics including purchase events ");
        if (this.p) {
            str2 = str4;
        } else {
            str2 = "disabled";
        }
        sb2.append(str2);
        g3.d("Answers", sb2.toString());
        this.l = fb6.g;
        l86 g4 = c86.g();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Custom event tracking ");
        if (this.l) {
            str3 = str4;
        } else {
            str3 = "disabled";
        }
        sb3.append(str3);
        g4.d("Answers", sb3.toString());
        this.m = fb6.h;
        l86 g5 = c86.g();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("Predefined event tracking ");
        if (!this.m) {
            str4 = "disabled";
        }
        sb4.append(str4);
        g5.d("Answers", sb4.toString());
        if (fb6.j > 1) {
            c86.g().d("Answers", "Event sampling enabled");
            this.k = new v10(fb6.j);
        }
        this.n = fb6.b;
        a(0, (long) this.n);
    }

    @DexIgnore
    public boolean b() {
        try {
            return this.d.g();
        } catch (IOException e2) {
            z86.a(this.c, "Failed to roll file over.", e2);
            return false;
        }
    }

    @DexIgnore
    public void c() {
        if (this.f.get() != null) {
            z86.c(this.c, "Cancelling time-based rollover because no events are currently being generated.");
            this.f.get().cancel(false);
            this.f.set((Object) null);
        }
    }

    @DexIgnore
    public void d() {
        this.d.a();
    }

    @DexIgnore
    public void e() {
        if (this.n != -1) {
            a((long) this.n, (long) this.n);
        }
    }

    @DexIgnore
    public void a(a20.b bVar) {
        a20 a2 = bVar.a(this.g);
        if (!this.l && a20.c.CUSTOM.equals(a2.c)) {
            l86 g2 = c86.g();
            g2.d("Answers", "Custom events tracking disabled - skipping event: " + a2);
        } else if (!this.m && a20.c.PREDEFINED.equals(a2.c)) {
            l86 g3 = c86.g();
            g3.d("Answers", "Predefined events tracking disabled - skipping event: " + a2);
        } else if (this.k.a(a2)) {
            l86 g4 = c86.g();
            g4.d("Answers", "Skipping filtered event: " + a2);
        } else {
            try {
                this.d.a(a2);
            } catch (IOException e2) {
                l86 g5 = c86.g();
                g5.e("Answers", "Failed to write event: " + a2, e2);
            }
            e();
            boolean z = a20.c.CUSTOM.equals(a2.c) || a20.c.PREDEFINED.equals(a2.c);
            boolean equals = "purchase".equals(a2.g);
            if (this.o && z) {
                if (!equals || this.p) {
                    try {
                        this.h.a(a2);
                    } catch (Exception e3) {
                        l86 g6 = c86.g();
                        g6.e("Answers", "Failed to map event to Firebase: " + a2, e3);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a() {
        if (this.i == null) {
            z86.c(this.c, "skipping files send because we don't yet know the target endpoint");
            return;
        }
        z86.c(this.c, "Sending all files");
        List d2 = this.d.d();
        int i2 = 0;
        while (true) {
            try {
                if (d2.size() <= 0) {
                    break;
                }
                z86.c(this.c, String.format(Locale.US, "attempt to send batch of %d files", new Object[]{Integer.valueOf(d2.size())}));
                boolean a2 = this.i.a(d2);
                if (a2) {
                    i2 += d2.size();
                    this.d.a(d2);
                }
                if (!a2) {
                    break;
                }
                d2 = this.d.d();
            } catch (Exception e2) {
                Context context = this.c;
                z86.a(context, "Failed to send batch of analytics files to server: " + e2.getMessage(), e2);
            }
        }
        if (i2 == 0) {
            this.d.b();
        }
    }

    @DexIgnore
    public void a(long j2, long j3) {
        if (this.f.get() == null) {
            qa6 qa6 = new qa6(this.c, this);
            Context context = this.c;
            z86.c(context, "Scheduling time based file roll over every " + j3 + " seconds");
            try {
                this.f.set(this.e.scheduleAtFixedRate(qa6, j2, j3, TimeUnit.SECONDS));
            } catch (RejectedExecutionException e2) {
                z86.a(this.c, "Failed to schedule time based file roll over", e2);
            }
        }
    }
}
