package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class li4 extends ii4 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public li4(String str, boolean z) {
        this.b = str;
        this.a = z;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public boolean b() {
        return this.a;
    }
}
