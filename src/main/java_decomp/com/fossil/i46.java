package com.fossil;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i46 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        if (n36.s() && q36.r != null) {
            if (n36.p()) {
                o46.b(q36.r).a((v36) new u36(q36.r, q36.a(q36.r, false, (r36) null), 2, th, thread, (r36) null), (x56) null, false, true);
                q36.m.b((Object) "MTA has caught the following uncaught exception:");
                q36.m.b(th);
            }
            q36.f(q36.r);
            if (q36.n != null) {
                q36.m.a((Object) "Call the original uncaught exception handler.");
                if (!(q36.n instanceof i46)) {
                    q36.n.uncaughtException(thread, th);
                }
            }
        }
    }
}
