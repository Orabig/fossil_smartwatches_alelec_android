package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mv2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<mv2> CREATOR; // = new pv2();
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ Bundle g;

    @DexIgnore
    public mv2(long j, long j2, boolean z, String str, String str2, String str3, Bundle bundle) {
        this.a = j;
        this.b = j2;
        this.c = z;
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = bundle;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, this.b);
        g22.a(parcel, 3, this.c);
        g22.a(parcel, 4, this.d, false);
        g22.a(parcel, 5, this.e, false);
        g22.a(parcel, 6, this.f, false);
        g22.a(parcel, 7, this.g, false);
        g22.a(parcel, a2);
    }
}
