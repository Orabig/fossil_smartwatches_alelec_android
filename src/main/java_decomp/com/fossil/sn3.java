package com.fossil;

import com.fossil.im3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn3<E> extends im3.b<E> {
    @DexIgnore
    public static /* final */ sn3<Object> EMPTY; // = new sn3<>(in3.a, 0, (Object[]) null, 0);
    @DexIgnore
    public /* final */ transient Object[] b;
    @DexIgnore
    public /* final */ transient int c;
    @DexIgnore
    public /* final */ transient int d;
    @DexIgnore
    public /* final */ transient Object[] table;

    @DexIgnore
    public sn3(Object[] objArr, int i, Object[] objArr2, int i2) {
        this.b = objArr;
        this.table = objArr2;
        this.c = i2;
        this.d = i;
    }

    @DexIgnore
    public boolean contains(Object obj) {
        Object[] objArr = this.table;
        if (obj == null || objArr == null) {
            return false;
        }
        int a = sl3.a(obj);
        while (true) {
            int i = a & this.c;
            Object obj2 = objArr[i];
            if (obj2 == null) {
                return false;
            }
            if (obj2.equals(obj)) {
                return true;
            }
            a = i + 1;
        }
    }

    @DexIgnore
    public int copyIntoArray(Object[] objArr, int i) {
        Object[] objArr2 = this.b;
        System.arraycopy(objArr2, 0, objArr, i, objArr2.length);
        return i + this.b.length;
    }

    @DexIgnore
    public zl3<E> createAsList() {
        return this.table == null ? zl3.of() : new nn3(this, this.b);
    }

    @DexIgnore
    public E get(int i) {
        return this.b[i];
    }

    @DexIgnore
    public int hashCode() {
        return this.d;
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.b.length;
    }
}
