package com.fossil;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContextView;
import com.fossil.q1;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f1 extends ActionMode implements q1.a {
    @DexIgnore
    public Context c;
    @DexIgnore
    public ActionBarContextView d;
    @DexIgnore
    public ActionMode.Callback e;
    @DexIgnore
    public WeakReference<View> f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public q1 h;

    @DexIgnore
    public f1(Context context, ActionBarContextView actionBarContextView, ActionMode.Callback callback, boolean z) {
        this.c = context;
        this.d = actionBarContextView;
        this.e = callback;
        q1 q1Var = new q1(actionBarContextView.getContext());
        q1Var.c(1);
        this.h = q1Var;
        this.h.a((q1.a) this);
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.d.setSubtitle(charSequence);
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.d.setTitle(charSequence);
    }

    @DexIgnore
    public Menu c() {
        return this.h;
    }

    @DexIgnore
    public MenuInflater d() {
        return new h1(this.d.getContext());
    }

    @DexIgnore
    public CharSequence e() {
        return this.d.getSubtitle();
    }

    @DexIgnore
    public CharSequence g() {
        return this.d.getTitle();
    }

    @DexIgnore
    public void i() {
        this.e.b(this, this.h);
    }

    @DexIgnore
    public boolean j() {
        return this.d.c();
    }

    @DexIgnore
    public void a(int i) {
        a((CharSequence) this.c.getString(i));
    }

    @DexIgnore
    public void b(int i) {
        b((CharSequence) this.c.getString(i));
    }

    @DexIgnore
    public void a(boolean z) {
        super.a(z);
        this.d.setTitleOptional(z);
    }

    @DexIgnore
    public View b() {
        WeakReference<View> weakReference = this.f;
        if (weakReference != null) {
            return (View) weakReference.get();
        }
        return null;
    }

    @DexIgnore
    public void a(View view) {
        this.d.setCustomView(view);
        this.f = view != null ? new WeakReference<>(view) : null;
    }

    @DexIgnore
    public void a() {
        if (!this.g) {
            this.g = true;
            this.d.sendAccessibilityEvent(32);
            this.e.a(this);
        }
    }

    @DexIgnore
    public boolean a(q1 q1Var, MenuItem menuItem) {
        return this.e.a((ActionMode) this, menuItem);
    }

    @DexIgnore
    public void a(q1 q1Var) {
        i();
        this.d.e();
    }
}
