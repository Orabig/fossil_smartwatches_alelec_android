package com.fossil;

import com.fossil.xw3;
import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jx3<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public List<jx3<K, V>.c> b;
    @DexIgnore
    public Map<K, V> c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public volatile jx3<K, V>.e e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends jx3<FieldDescriptorType, Object> {
        @DexIgnore
        public a(int i) {
            super(i, (a) null);
        }

        @DexIgnore
        public void h() {
            if (!g()) {
                for (int i = 0; i < c(); i++) {
                    Map.Entry a = a(i);
                    if (((xw3.b) a.getKey()).q()) {
                        a.setValue(Collections.unmodifiableList((List) a.getValue()));
                    }
                }
                for (Map.Entry entry : e()) {
                    if (((xw3.b) entry.getKey()).q()) {
                        entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                    }
                }
            }
            jx3.super.h();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object put(Object obj, Object obj2) {
            return jx3.super.put((xw3.b) obj, obj2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ Iterator<Object> a; // = new a();
        @DexIgnore
        public static /* final */ Iterable<Object> b; // = new C0026b();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Iterator<Object> {
            @DexIgnore
            public boolean hasNext() {
                return false;
            }

            @DexIgnore
            public Object next() {
                throw new NoSuchElementException();
            }

            @DexIgnore
            public void remove() {
                throw new UnsupportedOperationException();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jx3$b$b")
        /* renamed from: com.fossil.jx3$b$b  reason: collision with other inner class name */
        public static class C0026b implements Iterable<Object> {
            @DexIgnore
            public Iterator<Object> iterator() {
                return b.a;
            }
        }

        @DexIgnore
        public static <T> Iterable<T> b() {
            return b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Map.Entry<K, V>, Comparable<jx3<K, V>.c> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public V b;

        @DexIgnore
        public c(jx3 jx3, Map.Entry<K, V> entry) {
            this((Comparable) entry.getKey(), entry.getValue());
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(jx3<K, V>.c cVar) {
            return getKey().compareTo(cVar.getKey());
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            if (!a(this.a, entry.getKey()) || !a(this.b, entry.getValue())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public V getValue() {
            return this.b;
        }

        @DexIgnore
        public int hashCode() {
            K k = this.a;
            int i = 0;
            int hashCode = k == null ? 0 : k.hashCode();
            V v = this.b;
            if (v != null) {
                i = v.hashCode();
            }
            return hashCode ^ i;
        }

        @DexIgnore
        public V setValue(V v) {
            jx3.this.a();
            V v2 = this.b;
            this.b = v;
            return v2;
        }

        @DexIgnore
        public String toString() {
            return this.a + "=" + this.b;
        }

        @DexIgnore
        public c(K k, V v) {
            this.a = k;
            this.b = v;
        }

        @DexIgnore
        public final boolean a(Object obj, Object obj2) {
            if (obj == null) {
                return obj2 == null;
            }
            return obj.equals(obj2);
        }

        @DexIgnore
        public K getKey() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Iterator<Map.Entry<K, V>> {
        @DexIgnore
        public int a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public Iterator<Map.Entry<K, V>> c;

        @DexIgnore
        public d() {
            this.a = -1;
        }

        @DexIgnore
        public final Iterator<Map.Entry<K, V>> a() {
            if (this.c == null) {
                this.c = jx3.this.c.entrySet().iterator();
            }
            return this.c;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.a + 1 < jx3.this.b.size() || a().hasNext()) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public void remove() {
            if (this.b) {
                this.b = false;
                jx3.this.a();
                if (this.a < jx3.this.b.size()) {
                    jx3 jx3 = jx3.this;
                    int i = this.a;
                    this.a = i - 1;
                    Object unused = jx3.b(i);
                    return;
                }
                a().remove();
                return;
            }
            throw new IllegalStateException("remove() was called before next()");
        }

        @DexIgnore
        public Map.Entry<K, V> next() {
            this.b = true;
            int i = this.a + 1;
            this.a = i;
            if (i < jx3.this.b.size()) {
                return (Map.Entry) jx3.this.b.get(this.a);
            }
            return (Map.Entry) a().next();
        }

        @DexIgnore
        public /* synthetic */ d(jx3 jx3, a aVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends AbstractSet<Map.Entry<K, V>> {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        /* renamed from: a */
        public boolean add(Map.Entry<K, V> entry) {
            if (contains(entry)) {
                return false;
            }
            jx3.this.put((Comparable) entry.getKey(), entry.getValue());
            return true;
        }

        @DexIgnore
        public void clear() {
            jx3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            Object obj2 = jx3.this.get(entry.getKey());
            Object value = entry.getValue();
            return obj2 == value || (obj2 != null && obj2.equals(value));
        }

        @DexIgnore
        public Iterator<Map.Entry<K, V>> iterator() {
            return new d(jx3.this, (a) null);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            Map.Entry entry = (Map.Entry) obj;
            if (!contains(entry)) {
                return false;
            }
            jx3.this.remove(entry.getKey());
            return true;
        }

        @DexIgnore
        public int size() {
            return jx3.this.size();
        }

        @DexIgnore
        public /* synthetic */ e(jx3 jx3, a aVar) {
            this();
        }
    }

    @DexIgnore
    public /* synthetic */ jx3(int i, a aVar) {
        this(i);
    }

    @DexIgnore
    public void clear() {
        a();
        if (!this.b.isEmpty()) {
            this.b.clear();
        }
        if (!this.c.isEmpty()) {
            this.c.clear();
        }
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.c.containsKey(comparable);
    }

    @DexIgnore
    public int d() {
        return this.c.size();
    }

    @DexIgnore
    public Iterable<Map.Entry<K, V>> e() {
        if (this.c.isEmpty()) {
            return b.b();
        }
        return this.c.entrySet();
    }

    @DexIgnore
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.e == null) {
            this.e = new e(this, (a) null);
        }
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof jx3)) {
            return super.equals(obj);
        }
        jx3 jx3 = (jx3) obj;
        int size = size();
        if (size != jx3.size()) {
            return false;
        }
        int c2 = c();
        if (c2 != jx3.c()) {
            return entrySet().equals(jx3.entrySet());
        }
        for (int i = 0; i < c2; i++) {
            if (!a(i).equals(jx3.a(i))) {
                return false;
            }
        }
        if (c2 != size) {
            return this.c.equals(jx3.c);
        }
        return true;
    }

    @DexIgnore
    public final SortedMap<K, V> f() {
        a();
        if (this.c.isEmpty() && !(this.c instanceof TreeMap)) {
            this.c = new TreeMap();
        }
        return (SortedMap) this.c;
    }

    @DexIgnore
    public boolean g() {
        return this.d;
    }

    @DexIgnore
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return this.b.get(a2).getValue();
        }
        return this.c.get(comparable);
    }

    @DexIgnore
    public void h() {
        Map<K, V> map;
        if (!this.d) {
            if (this.c.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.c);
            }
            this.c = map;
            this.d = true;
        }
    }

    @DexIgnore
    public int hashCode() {
        int c2 = c();
        int i = 0;
        for (int i2 = 0; i2 < c2; i2++) {
            i += this.b.get(i2).hashCode();
        }
        return d() > 0 ? i + this.c.hashCode() : i;
    }

    @DexIgnore
    public V remove(Object obj) {
        a();
        Comparable comparable = (Comparable) obj;
        int a2 = a(comparable);
        if (a2 >= 0) {
            return b(a2);
        }
        if (this.c.isEmpty()) {
            return null;
        }
        return this.c.remove(comparable);
    }

    @DexIgnore
    public int size() {
        return this.b.size() + this.c.size();
    }

    @DexIgnore
    public jx3(int i) {
        this.a = i;
        this.b = Collections.emptyList();
        this.c = Collections.emptyMap();
    }

    @DexIgnore
    public static <FieldDescriptorType extends xw3.b<FieldDescriptorType>> jx3<FieldDescriptorType, Object> c(int i) {
        return new a(i);
    }

    @DexIgnore
    public final V b(int i) {
        a();
        V value = this.b.remove(i).getValue();
        if (!this.c.isEmpty()) {
            Iterator it = f().entrySet().iterator();
            this.b.add(new c(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    @DexIgnore
    public Map.Entry<K, V> a(int i) {
        return this.b.get(i);
    }

    @DexIgnore
    public int c() {
        return this.b.size();
    }

    @DexIgnore
    /* renamed from: a */
    public V put(K k, V v) {
        a();
        int a2 = a(k);
        if (a2 >= 0) {
            return this.b.get(a2).setValue(v);
        }
        b();
        int i = -(a2 + 1);
        if (i >= this.a) {
            return f().put(k, v);
        }
        int size = this.b.size();
        int i2 = this.a;
        if (size == i2) {
            c remove = this.b.remove(i2 - 1);
            f().put(remove.getKey(), remove.getValue());
        }
        this.b.add(i, new c(k, v));
        return null;
    }

    @DexIgnore
    public final void b() {
        a();
        if (this.b.isEmpty() && !(this.b instanceof ArrayList)) {
            this.b = new ArrayList(this.a);
        }
    }

    @DexIgnore
    public final int a(K k) {
        int size = this.b.size() - 1;
        if (size >= 0) {
            int compareTo = k.compareTo(this.b.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = k.compareTo(this.b.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }

    @DexIgnore
    public final void a() {
        if (this.d) {
            throw new UnsupportedOperationException();
        }
    }
}
