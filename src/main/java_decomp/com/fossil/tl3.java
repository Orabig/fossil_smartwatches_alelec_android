package com.fossil;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tl3<E> extends zl3<E> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ vl3<?> collection;

        @DexIgnore
        public a(vl3<?> vl3) {
            this.collection = vl3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.collection.asList();
        }
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return delegateCollection().contains(obj);
    }

    @DexIgnore
    public abstract vl3<E> delegateCollection();

    @DexIgnore
    public boolean isEmpty() {
        return delegateCollection().isEmpty();
    }

    @DexIgnore
    public boolean isPartialView() {
        return delegateCollection().isPartialView();
    }

    @DexIgnore
    public int size() {
        return delegateCollection().size();
    }

    @DexIgnore
    public Object writeReplace() {
        return new a(delegateCollection());
    }
}
