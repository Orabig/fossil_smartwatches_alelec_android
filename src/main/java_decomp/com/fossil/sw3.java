package com.fossil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sw3 implements Iterable<Byte>, Serializable {
    @DexIgnore
    public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
    @DexIgnore
    public static /* final */ int CONCATENATE_BY_COPY_SIZE; // = 128;
    @DexIgnore
    public static /* final */ sw3 EMPTY; // = new h(zw3.b);
    @DexIgnore
    public static /* final */ int MAX_READ_FROM_CHUNK_SIZE; // = 8192;
    @DexIgnore
    public static /* final */ int MIN_READ_FROM_CHUNK_SIZE; // = 256;
    @DexIgnore
    public static /* final */ d a;
    @DexIgnore
    public int hash; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements e {
        @DexIgnore
        public int a; // = 0;
        @DexIgnore
        public /* final */ int b; // = sw3.this.size();

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public byte a() {
            try {
                sw3 sw3 = sw3.this;
                int i = this.a;
                this.a = i + 1;
                return sw3.byteAt(i);
            } catch (IndexOutOfBoundsException e) {
                throw new NoSuchElementException(e.getMessage());
            }
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < this.b;
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public Byte next() {
            return Byte.valueOf(a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements d {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public byte[] a(byte[] bArr, int i, int i2) {
            return Arrays.copyOfRange(bArr, i, i2 + i);
        }

        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends h {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;
        @DexIgnore
        public /* final */ int bytesLength;
        @DexIgnore
        public /* final */ int bytesOffset;

        @DexIgnore
        public c(byte[] bArr, int i, int i2) {
            super(bArr);
            sw3.checkRange(i, i + i2, bArr.length);
            this.bytesOffset = i;
            this.bytesLength = i2;
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException {
            throw new InvalidObjectException("BoundedByteStream instances are not to be serialized directly");
        }

        @DexIgnore
        public byte byteAt(int i) {
            sw3.checkIndex(i, size());
            return this.bytes[this.bytesOffset + i];
        }

        @DexIgnore
        public void copyToInternal(byte[] bArr, int i, int i2, int i3) {
            System.arraycopy(this.bytes, getOffsetIntoBytes() + i, bArr, i2, i3);
        }

        @DexIgnore
        public int getOffsetIntoBytes() {
            return this.bytesOffset;
        }

        @DexIgnore
        public int size() {
            return this.bytesLength;
        }

        @DexIgnore
        public Object writeReplace() {
            return sw3.wrap(toByteArray());
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        byte[] a(byte[] bArr, int i, int i2);
    }

    @DexIgnore
    public interface e extends Iterator<Byte> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f {
        @DexIgnore
        public /* final */ byte[] a;

        @DexIgnore
        public /* synthetic */ f(int i, a aVar) {
            this(i);
        }

        @DexIgnore
        public f(int i) {
            this.a = new byte[i];
            uw3.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class g extends sw3 {
        @DexIgnore
        public abstract boolean equalsRange(sw3 sw3, int i, int i2);

        @DexIgnore
        public final int getTreeDepth() {
            return 0;
        }

        @DexIgnore
        public final boolean isBalanced() {
            return true;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Iterator iterator() {
            return sw3.super.iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends g {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;
        @DexIgnore
        public /* final */ byte[] bytes;

        @DexIgnore
        public h(byte[] bArr) {
            this.bytes = bArr;
        }

        @DexIgnore
        public final ByteBuffer asReadOnlyByteBuffer() {
            return ByteBuffer.wrap(this.bytes, getOffsetIntoBytes(), size()).asReadOnlyBuffer();
        }

        @DexIgnore
        public final List<ByteBuffer> asReadOnlyByteBufferList() {
            return Collections.singletonList(asReadOnlyByteBuffer());
        }

        @DexIgnore
        public byte byteAt(int i) {
            return this.bytes[i];
        }

        @DexIgnore
        public final void copyTo(ByteBuffer byteBuffer) {
            byteBuffer.put(this.bytes, getOffsetIntoBytes(), size());
        }

        @DexIgnore
        public void copyToInternal(byte[] bArr, int i, int i2, int i3) {
            System.arraycopy(this.bytes, i, bArr, i2, i3);
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof sw3) || size() != ((sw3) obj).size()) {
                return false;
            }
            if (size() == 0) {
                return true;
            }
            if (!(obj instanceof h)) {
                return obj.equals(this);
            }
            h hVar = (h) obj;
            int peekCachedHashCode = peekCachedHashCode();
            int peekCachedHashCode2 = hVar.peekCachedHashCode();
            if (peekCachedHashCode == 0 || peekCachedHashCode2 == 0 || peekCachedHashCode == peekCachedHashCode2) {
                return equalsRange(hVar, 0, size());
            }
            return false;
        }

        @DexIgnore
        public final boolean equalsRange(sw3 sw3, int i, int i2) {
            if (i2 <= sw3.size()) {
                int i3 = i + i2;
                if (i3 > sw3.size()) {
                    throw new IllegalArgumentException("Ran off end of other: " + i + ", " + i2 + ", " + sw3.size());
                } else if (!(sw3 instanceof h)) {
                    return sw3.substring(i, i3).equals(substring(0, i2));
                } else {
                    h hVar = (h) sw3;
                    byte[] bArr = this.bytes;
                    byte[] bArr2 = hVar.bytes;
                    int offsetIntoBytes = getOffsetIntoBytes() + i2;
                    int offsetIntoBytes2 = getOffsetIntoBytes();
                    int offsetIntoBytes3 = hVar.getOffsetIntoBytes() + i;
                    while (offsetIntoBytes2 < offsetIntoBytes) {
                        if (bArr[offsetIntoBytes2] != bArr2[offsetIntoBytes3]) {
                            return false;
                        }
                        offsetIntoBytes2++;
                        offsetIntoBytes3++;
                    }
                    return true;
                }
            } else {
                throw new IllegalArgumentException("Length too large: " + i2 + size());
            }
        }

        @DexIgnore
        public int getOffsetIntoBytes() {
            return 0;
        }

        @DexIgnore
        public final boolean isValidUtf8() {
            int offsetIntoBytes = getOffsetIntoBytes();
            return ox3.c(this.bytes, offsetIntoBytes, size() + offsetIntoBytes);
        }

        @DexIgnore
        public final tw3 newCodedInput() {
            return tw3.a(this.bytes, getOffsetIntoBytes(), size(), true);
        }

        @DexIgnore
        public final InputStream newInput() {
            return new ByteArrayInputStream(this.bytes, getOffsetIntoBytes(), size());
        }

        @DexIgnore
        public final int partialHash(int i, int i2, int i3) {
            return zw3.a(i, this.bytes, getOffsetIntoBytes() + i2, i3);
        }

        @DexIgnore
        public final int partialIsValidUtf8(int i, int i2, int i3) {
            int offsetIntoBytes = getOffsetIntoBytes() + i2;
            return ox3.a(i, this.bytes, offsetIntoBytes, i3 + offsetIntoBytes);
        }

        @DexIgnore
        public int size() {
            return this.bytes.length;
        }

        @DexIgnore
        public final sw3 substring(int i, int i2) {
            int checkRange = sw3.checkRange(i, i2, size());
            if (checkRange == 0) {
                return sw3.EMPTY;
            }
            return new c(this.bytes, getOffsetIntoBytes() + i, checkRange);
        }

        @DexIgnore
        public final String toStringInternal(Charset charset) {
            return new String(this.bytes, getOffsetIntoBytes(), size(), charset);
        }

        @DexIgnore
        public final void writeTo(OutputStream outputStream) throws IOException {
            outputStream.write(toByteArray());
        }

        @DexIgnore
        public final void writeToInternal(OutputStream outputStream, int i, int i2) throws IOException {
            outputStream.write(this.bytes, getOffsetIntoBytes() + i, i2);
        }

        @DexIgnore
        public final void writeTo(rw3 rw3) throws IOException {
            rw3.a(this.bytes, getOffsetIntoBytes(), size());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements d {
        @DexIgnore
        public j() {
        }

        @DexIgnore
        public byte[] a(byte[] bArr, int i, int i2) {
            byte[] bArr2 = new byte[i2];
            System.arraycopy(bArr, i, bArr2, 0, i2);
            return bArr2;
        }

        @DexIgnore
        public /* synthetic */ j(a aVar) {
            this();
        }
    }

    /*
    static {
        Class<sw3> cls = sw3.class;
        boolean z = true;
        try {
            Class.forName("android.content.Context");
        } catch (ClassNotFoundException unused) {
            z = false;
        }
        a = z ? new j((a) null) : new b((a) null);
    }
    */

    @DexIgnore
    public static sw3 a(InputStream inputStream, int i2) throws IOException {
        byte[] bArr = new byte[i2];
        int i3 = 0;
        while (i3 < i2) {
            int read = inputStream.read(bArr, i3, i2 - i3);
            if (read == -1) {
                break;
            }
            i3 += read;
        }
        if (i3 == 0) {
            return null;
        }
        return copyFrom(bArr, 0, i3);
    }

    @DexIgnore
    public static void checkIndex(int i2, int i3) {
        if (((i3 - (i2 + 1)) | i2) >= 0) {
            return;
        }
        if (i2 < 0) {
            throw new ArrayIndexOutOfBoundsException("Index < 0: " + i2);
        }
        throw new ArrayIndexOutOfBoundsException("Index > length: " + i2 + ", " + i3);
    }

    @DexIgnore
    public static int checkRange(int i2, int i3, int i4) {
        int i5 = i3 - i2;
        if ((i2 | i3 | i5 | (i4 - i3)) >= 0) {
            return i5;
        }
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Beginning index: " + i2 + " < 0");
        } else if (i3 < i2) {
            throw new IndexOutOfBoundsException("Beginning index larger than ending index: " + i2 + ", " + i3);
        } else {
            throw new IndexOutOfBoundsException("End index: " + i3 + " >= " + i4);
        }
    }

    @DexIgnore
    public static sw3 copyFrom(byte[] bArr, int i2, int i3) {
        return new h(a.a(bArr, i2, i3));
    }

    @DexIgnore
    public static sw3 copyFromUtf8(String str) {
        return new h(str.getBytes(zw3.a));
    }

    @DexIgnore
    public static f newCodedBuilder(int i2) {
        return new f(i2, (a) null);
    }

    @DexIgnore
    public static i newOutput(int i2) {
        return new i(i2);
    }

    @DexIgnore
    public static sw3 readFrom(InputStream inputStream) throws IOException {
        return readFrom(inputStream, 256, 8192);
    }

    @DexIgnore
    public static sw3 wrap(byte[] bArr) {
        return new h(bArr);
    }

    @DexIgnore
    public abstract ByteBuffer asReadOnlyByteBuffer();

    @DexIgnore
    public abstract List<ByteBuffer> asReadOnlyByteBufferList();

    @DexIgnore
    public abstract byte byteAt(int i2);

    @DexIgnore
    public final sw3 concat(sw3 sw3) {
        if (Integer.MAX_VALUE - size() >= sw3.size()) {
            return ix3.concatenate(this, sw3);
        }
        throw new IllegalArgumentException("ByteString would be too long: " + size() + "+" + sw3.size());
    }

    @DexIgnore
    public abstract void copyTo(ByteBuffer byteBuffer);

    @DexIgnore
    public void copyTo(byte[] bArr, int i2) {
        copyTo(bArr, 0, i2, size());
    }

    @DexIgnore
    public abstract void copyToInternal(byte[] bArr, int i2, int i3, int i4);

    @DexIgnore
    public final boolean endsWith(sw3 sw3) {
        return size() >= sw3.size() && substring(size() - sw3.size()).equals(sw3);
    }

    @DexIgnore
    public abstract boolean equals(Object obj);

    @DexIgnore
    public abstract int getTreeDepth();

    @DexIgnore
    public final int hashCode() {
        int i2 = this.hash;
        if (i2 == 0) {
            int size = size();
            i2 = partialHash(size, 0, size);
            if (i2 == 0) {
                i2 = 1;
            }
            this.hash = i2;
        }
        return i2;
    }

    @DexIgnore
    public abstract boolean isBalanced();

    @DexIgnore
    public final boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    public abstract boolean isValidUtf8();

    @DexIgnore
    public abstract tw3 newCodedInput();

    @DexIgnore
    public abstract InputStream newInput();

    @DexIgnore
    public abstract int partialHash(int i2, int i3, int i4);

    @DexIgnore
    public abstract int partialIsValidUtf8(int i2, int i3, int i4);

    @DexIgnore
    public final int peekCachedHashCode() {
        return this.hash;
    }

    @DexIgnore
    public abstract int size();

    @DexIgnore
    public final boolean startsWith(sw3 sw3) {
        if (size() < sw3.size() || !substring(0, sw3.size()).equals(sw3)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final sw3 substring(int i2) {
        return substring(i2, size());
    }

    @DexIgnore
    public abstract sw3 substring(int i2, int i3);

    @DexIgnore
    public final byte[] toByteArray() {
        int size = size();
        if (size == 0) {
            return zw3.b;
        }
        byte[] bArr = new byte[size];
        copyToInternal(bArr, 0, 0, size);
        return bArr;
    }

    @DexIgnore
    public final String toString(String str) throws UnsupportedEncodingException {
        try {
            return toString(Charset.forName(str));
        } catch (UnsupportedCharsetException e2) {
            UnsupportedEncodingException unsupportedEncodingException = new UnsupportedEncodingException(str);
            unsupportedEncodingException.initCause(e2);
            throw unsupportedEncodingException;
        }
    }

    @DexIgnore
    public abstract String toStringInternal(Charset charset);

    @DexIgnore
    public final String toStringUtf8() {
        return toString(zw3.a);
    }

    @DexIgnore
    public abstract void writeTo(rw3 rw3) throws IOException;

    @DexIgnore
    public abstract void writeTo(OutputStream outputStream) throws IOException;

    @DexIgnore
    public final void writeTo(OutputStream outputStream, int i2, int i3) throws IOException {
        checkRange(i2, i2 + i3, size());
        if (i3 > 0) {
            writeToInternal(outputStream, i2, i3);
        }
    }

    @DexIgnore
    public abstract void writeToInternal(OutputStream outputStream, int i2, int i3) throws IOException;

    @DexIgnore
    public static sw3 copyFrom(byte[] bArr) {
        return copyFrom(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static i newOutput() {
        return new i(128);
    }

    @DexIgnore
    public static sw3 readFrom(InputStream inputStream, int i2) throws IOException {
        return readFrom(inputStream, i2, i2);
    }

    @DexIgnore
    public static sw3 wrap(byte[] bArr, int i2, int i3) {
        return new c(bArr, i2, i3);
    }

    @DexIgnore
    public final void copyTo(byte[] bArr, int i2, int i3, int i4) {
        checkRange(i2, i2 + i4, size());
        checkRange(i3, i3 + i4, bArr.length);
        if (i4 > 0) {
            copyToInternal(bArr, i2, i3, i4);
        }
    }

    @DexIgnore
    public final e iterator() {
        return new a();
    }

    @DexIgnore
    public static sw3 copyFrom(ByteBuffer byteBuffer, int i2) {
        byte[] bArr = new byte[i2];
        byteBuffer.get(bArr);
        return new h(bArr);
    }

    @DexIgnore
    public static sw3 readFrom(InputStream inputStream, int i2, int i3) throws IOException {
        ArrayList arrayList = new ArrayList();
        while (true) {
            sw3 a2 = a(inputStream, i2);
            if (a2 == null) {
                return copyFrom((Iterable<sw3>) arrayList);
            }
            arrayList.add(a2);
            i2 = Math.min(i2 * 2, i3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends OutputStream {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<sw3> b;
        @DexIgnore
        public int c;
        @DexIgnore
        public byte[] d;
        @DexIgnore
        public int e;

        @DexIgnore
        public i(int i) {
            if (i >= 0) {
                this.a = i;
                this.b = new ArrayList<>();
                this.d = new byte[i];
                return;
            }
            throw new IllegalArgumentException("Buffer size < 0");
        }

        @DexIgnore
        public final void b(int i) {
            this.b.add(new h(this.d));
            this.c += this.d.length;
            this.d = new byte[Math.max(this.a, Math.max(i, this.c >>> 1))];
            this.e = 0;
        }

        @DexIgnore
        public synchronized int k() {
            return this.c + this.e;
        }

        @DexIgnore
        public String toString() {
            return String.format("<ByteString.Output@%s size=%d>", new Object[]{Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(k())});
        }

        @DexIgnore
        public synchronized void write(int i) {
            if (this.e == this.d.length) {
                b(1);
            }
            byte[] bArr = this.d;
            int i2 = this.e;
            this.e = i2 + 1;
            bArr[i2] = (byte) i;
        }

        @DexIgnore
        public synchronized void write(byte[] bArr, int i, int i2) {
            if (i2 <= this.d.length - this.e) {
                System.arraycopy(bArr, i, this.d, this.e, i2);
                this.e += i2;
            } else {
                int length = this.d.length - this.e;
                System.arraycopy(bArr, i, this.d, this.e, length);
                int i3 = i2 - length;
                b(i3);
                System.arraycopy(bArr, i + length, this.d, 0, i3);
                this.e = i3;
            }
        }
    }

    @DexIgnore
    public static sw3 a(Iterator<sw3> it, int i2) {
        if (i2 == 1) {
            return it.next();
        }
        int i3 = i2 >>> 1;
        return a(it, i3).concat(a(it, i2 - i3));
    }

    @DexIgnore
    public final String toString(Charset charset) {
        return size() == 0 ? "" : toStringInternal(charset);
    }

    @DexIgnore
    public static sw3 copyFrom(ByteBuffer byteBuffer) {
        return copyFrom(byteBuffer, byteBuffer.remaining());
    }

    @DexIgnore
    public final String toString() {
        return String.format("<ByteString@%s size=%d>", new Object[]{Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(size())});
    }

    @DexIgnore
    public static sw3 copyFrom(String str, String str2) throws UnsupportedEncodingException {
        return new h(str.getBytes(str2));
    }

    @DexIgnore
    public static sw3 copyFrom(String str, Charset charset) {
        return new h(str.getBytes(charset));
    }

    @DexIgnore
    public static sw3 copyFrom(Iterable<sw3> iterable) {
        int i2;
        if (!(iterable instanceof Collection)) {
            i2 = 0;
            Iterator<sw3> it = iterable.iterator();
            while (it.hasNext()) {
                it.next();
                i2++;
            }
        } else {
            i2 = ((Collection) iterable).size();
        }
        if (i2 == 0) {
            return EMPTY;
        }
        return a(iterable.iterator(), i2);
    }
}
