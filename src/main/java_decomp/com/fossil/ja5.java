package com.fossil;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ja5 extends td {
    @DexIgnore
    public MutableLiveData<b> a; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public b(Integer num, boolean z) {
            this.a = num;
            this.b = z;
        }

        @DexIgnore
        public final Integer a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return wg6.a((Object) this.a, (Object) bVar.a) && this.b == bVar.b;
        }

        @DexIgnore
        public int hashCode() {
            Integer num = this.a;
            int hashCode = (num != null ? num.hashCode() : 0) * 31;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            return hashCode + (z ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(tutorialResource=" + this.a + ", showLoading=" + this.b + ")";
        }
    }

    /*
    static {
        new a((qg6) null);
        wg6.a((Object) ja5.class.getSimpleName(), "CustomizeTutorialViewModel::class.java.simpleName");
    }
    */

    @DexIgnore
    public final MutableLiveData<b> a() {
        return this.a;
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "watchAppId");
        a(this, dl4.c.c(str), false, 2, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ void a(ja5 ja5, Integer num, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            num = null;
        }
        if ((i & 2) != 0) {
            z = false;
        }
        ja5.a(num, z);
    }

    @DexIgnore
    public final void a(Integer num, boolean z) {
        this.a.a(new b(num, z));
    }
}
