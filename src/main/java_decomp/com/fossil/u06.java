package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface u06 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements u06 {
        @DexIgnore
        public int a() {
            return 0;
        }

        @DexIgnore
        public Bitmap a(String str) {
            return null;
        }

        @DexIgnore
        public void a(String str, Bitmap bitmap) {
        }

        @DexIgnore
        public int size() {
            return 0;
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    int a();

    @DexIgnore
    Bitmap a(String str);

    @DexIgnore
    void a(String str, Bitmap bitmap);

    @DexIgnore
    int size();
}
