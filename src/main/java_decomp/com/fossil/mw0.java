package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum mw0 {
    BOND_NONE(10),
    BONDING(11),
    BONDED(12);
    
    @DexIgnore
    public static /* final */ tu0 e; // = null;

    /*
    static {
        e = new tu0((qg6) null);
    }
    */

    @DexIgnore
    public mw0(int i) {
    }
}
