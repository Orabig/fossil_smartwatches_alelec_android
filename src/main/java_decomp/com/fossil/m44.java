package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ArrayList<DianaPresetWatchAppSetting>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<DianaPresetWatchAppSetting>> {
    }

    @DexIgnore
    public final String a(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        wg6.b(arrayList, "configurationList");
        if (arrayList.isEmpty()) {
            return "";
        }
        return new Gson().a(arrayList, new a().getType());
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> a(String str) {
        wg6.b(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object a2 = new Gson().a(str, new b().getType());
        wg6.a(a2, "Gson().fromJson(data, type)");
        return (ArrayList) a2;
    }
}
