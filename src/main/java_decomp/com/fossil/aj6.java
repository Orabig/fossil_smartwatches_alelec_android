package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aj6 extends zi6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterable<T>, ph6 {
        @DexIgnore
        public /* final */ /* synthetic */ ti6 a;

        @DexIgnore
        public a(ti6 ti6) {
            this.a = ti6;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return this.a.iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<T, Boolean> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(1);
        }

        @DexIgnore
        public final boolean invoke(T t) {
            return t == null;
        }
    }

    @DexIgnore
    public static final <T> ti6<T> a(ti6<? extends T> ti6, hg6<? super T, Boolean> hg6) {
        wg6.b(ti6, "$this$filter");
        wg6.b(hg6, "predicate");
        return new ri6(ti6, true, hg6);
    }

    @DexIgnore
    public static final <T> ti6<T> b(ti6<? extends T> ti6, hg6<? super T, Boolean> hg6) {
        wg6.b(ti6, "$this$filterNot");
        wg6.b(hg6, "predicate");
        return new ri6(ti6, false, hg6);
    }

    @DexIgnore
    public static final <T> ti6<T> c(ti6<? extends T> ti6) {
        wg6.b(ti6, "$this$filterNotNull");
        ti6<T> b2 = b(ti6, b.INSTANCE);
        if (b2 != null) {
            return b2;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.sequences.Sequence<T>");
    }

    @DexIgnore
    public static final <T, R> ti6<R> d(ti6<? extends T> ti6, hg6<? super T, ? extends R> hg6) {
        wg6.b(ti6, "$this$mapNotNull");
        wg6.b(hg6, "transform");
        return c(new bj6(ti6, hg6));
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> T e(ti6<? extends T> ti6) {
        wg6.b(ti6, "$this$min");
        Iterator<? extends T> it = ti6.iterator();
        if (!it.hasNext()) {
            return null;
        }
        T t = (Comparable) it.next();
        while (it.hasNext()) {
            T t2 = (Comparable) it.next();
            if (t.compareTo(t2) > 0) {
                t = t2;
            }
        }
        return t;
    }

    @DexIgnore
    public static final <T> List<T> f(ti6<? extends T> ti6) {
        wg6.b(ti6, "$this$toList");
        return qd6.b(g(ti6));
    }

    @DexIgnore
    public static final <T> List<T> g(ti6<? extends T> ti6) {
        wg6.b(ti6, "$this$toMutableList");
        ArrayList arrayList = new ArrayList();
        a(ti6, arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C a(ti6<? extends T> ti6, C c) {
        wg6.b(ti6, "$this$toCollection");
        wg6.b(c, "destination");
        for (Object add : ti6) {
            c.add(add);
        }
        return c;
    }

    @DexIgnore
    public static final <T> Iterable<T> b(ti6<? extends T> ti6) {
        wg6.b(ti6, "$this$asIterable");
        return new a(ti6);
    }

    @DexIgnore
    public static final <T, R> ti6<R> c(ti6<? extends T> ti6, hg6<? super T, ? extends R> hg6) {
        wg6.b(ti6, "$this$map");
        wg6.b(hg6, "transform");
        return new bj6(ti6, hg6);
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> T d(ti6<? extends T> ti6) {
        wg6.b(ti6, "$this$max");
        Iterator<? extends T> it = ti6.iterator();
        if (!it.hasNext()) {
            return null;
        }
        T t = (Comparable) it.next();
        while (it.hasNext()) {
            T t2 = (Comparable) it.next();
            if (t.compareTo(t2) < 0) {
                t = t2;
            }
        }
        return t;
    }

    @DexIgnore
    public static final <T, A extends Appendable> A a(ti6<? extends T> ti6, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, hg6<? super T, ? extends CharSequence> hg6) {
        wg6.b(ti6, "$this$joinTo");
        wg6.b(a2, "buffer");
        wg6.b(charSequence, "separator");
        wg6.b(charSequence2, "prefix");
        wg6.b(charSequence3, "postfix");
        wg6.b(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (Object next : ti6) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            uj6.a(a2, next, hg6);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    @DexIgnore
    public static /* synthetic */ String a(ti6 ti6, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, hg6 hg6, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = (i2 & 2) != 0 ? charSequence5 : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence7 = charSequence4;
        if ((i2 & 32) != 0) {
            hg6 = null;
        }
        return a(ti6, charSequence, charSequence6, charSequence5, i3, charSequence7, hg6);
    }

    @DexIgnore
    public static final <T> String a(ti6<? extends T> ti6, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, hg6<? super T, ? extends CharSequence> hg6) {
        wg6.b(ti6, "$this$joinToString");
        wg6.b(charSequence, "separator");
        wg6.b(charSequence2, "prefix");
        wg6.b(charSequence3, "postfix");
        wg6.b(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        a(ti6, sb, charSequence, charSequence2, charSequence3, i, charSequence4, hg6);
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }
}
