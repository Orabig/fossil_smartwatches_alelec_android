package com.fossil;

import android.bluetooth.BluetoothDevice;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class an1 extends p40 {
    @DexIgnore
    public /* final */ BluetoothDevice a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public an1(BluetoothDevice bluetoothDevice, int i) {
        this.a = bluetoothDevice;
        this.b = i;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.DEVICE, (Object) this.a), bm0.REQUEST_ID, (Object) Integer.valueOf(this.b));
    }
}
