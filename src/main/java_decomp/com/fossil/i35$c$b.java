package com.fossil;

import android.text.TextUtils;
import com.fossil.a35;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i35$c$b implements y24.d<a35.d, a35.b> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationHybridEveryonePresenter.c a;

    @DexIgnore
    public i35$c$b(NotificationHybridEveryonePresenter.c cVar) {
        this.a = cVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(a35.d dVar) {
        wg6.b(dVar, "successResponse");
        FLogger.INSTANCE.getLocal().d(NotificationHybridEveryonePresenter.l.a(), "GetAllContactGroup onSuccess");
        for (ContactGroup contactGroup : dVar.a()) {
            for (Contact contact : contactGroup.getContacts()) {
                wg6.a((Object) contact, "contact");
                if (contact.getContactId() == -100 || contact.getContactId() == -200) {
                    wx4 wx4 = new wx4(contact, (String) null, 2, (qg6) null);
                    wx4.setAdded(true);
                    Contact contact2 = wx4.getContact();
                    if (contact2 != null) {
                        contact2.setDbRowId(contact.getDbRowId());
                        contact2.setUseSms(contact.isUseSms());
                        contact2.setUseCall(contact.isUseCall());
                    }
                    wx4.setCurrentHandGroup(contactGroup.getHour());
                    List phoneNumbers = contact.getPhoneNumbers();
                    wg6.a((Object) phoneNumbers, "contact.phoneNumbers");
                    if (!phoneNumbers.isEmpty()) {
                        Object obj = contact.getPhoneNumbers().get(0);
                        wg6.a(obj, "contact.phoneNumbers[0]");
                        String number = ((PhoneNumber) obj).getNumber();
                        if (!TextUtils.isEmpty(number)) {
                            wx4.setHasPhoneNumber(true);
                            wx4.setPhoneNumber(number);
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a2 = NotificationHybridEveryonePresenter.l.a();
                            local.d(a2, " filter selected contact, phoneNumber=" + number);
                        }
                    }
                    Iterator it = this.a.this$0.h.iterator();
                    int i = 0;
                    while (true) {
                        if (!it.hasNext()) {
                            i = -1;
                            break;
                        }
                        Contact contact3 = ((wx4) it.next()).getContact();
                        if (contact3 != null && contact3.getContactId() == contact.getContactId()) {
                            break;
                        }
                        i++;
                    }
                    if (i != -1) {
                        wx4.setCurrentHandGroup(this.a.this$0.g);
                        this.a.this$0.h.remove(i);
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = NotificationHybridEveryonePresenter.l.a();
                    local2.d(a3, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                    this.a.this$0.m().add(wx4);
                }
            }
        }
        if (!this.a.this$0.h.isEmpty()) {
            for (wx4 add : this.a.this$0.h) {
                this.a.this$0.m().add(add);
            }
        }
        this.a.this$0.f.b(this.a.this$0.m(), this.a.this$0.g);
    }

    @DexIgnore
    public void a(a35.b bVar) {
        wg6.b(bVar, "errorResponse");
        FLogger.INSTANCE.getLocal().d(NotificationHybridEveryonePresenter.l.a(), "GetAllContactGroup onError");
    }
}
