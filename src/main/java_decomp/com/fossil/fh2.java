package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fh2 implements Parcelable.Creator<eh2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        ch2 ch2 = null;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        int i = 1;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i = f22.q(parcel, a);
            } else if (a2 == 2) {
                ch2 = (ch2) f22.a(parcel, a, ch2.CREATOR);
            } else if (a2 == 3) {
                iBinder = f22.p(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                iBinder2 = f22.p(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new eh2(i, ch2, iBinder, iBinder2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new eh2[i];
    }
}
