package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.splash.SplashPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1$1", f = "SplashPresenter.kt", l = {63, 64}, m = "invokeSuspend")
public final class lu5$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SplashPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lu5$b$a(SplashPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        lu5$b$a lu5_b_a = new lu5$b$a(this.this$0, xe6);
        lu5_b_a.p$ = (il6) obj;
        return lu5_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((lu5$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            ThemeRepository f = this.this$0.this$0.j;
            this.L$0 = il6;
            this.label = 1;
            if (f.initializeLocalTheme(this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ThemeManager a2 = ThemeManager.l.a();
        this.L$0 = il6;
        this.label = 2;
        if (a2.a((xe6<? super cd6>) this) == a) {
            return a;
        }
        return cd6.a;
    }
}
