package com.fossil;

import java.io.IOException;
import java.nio.CharBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oo3 {
    @DexIgnore
    public static CharBuffer a() {
        return CharBuffer.allocate(2048);
    }

    @DexIgnore
    public static <T> T a(Readable readable, to3<T> to3) throws IOException {
        String a;
        jk3.a(readable);
        jk3.a(to3);
        uo3 uo3 = new uo3(readable);
        do {
            a = uo3.a();
            if (a == null || !to3.a(a)) {
            }
            a = uo3.a();
            break;
        } while (!to3.a(a));
        return to3.getResult();
    }
}
