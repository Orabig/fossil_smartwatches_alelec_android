package com.fossil;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zy4 implements Factory<yy4> {
    @DexIgnore
    public static NotificationContactsPresenter a(uy4 uy4, z24 z24, mz4 mz4, pz4 pz4, LoaderManager loaderManager) {
        return new NotificationContactsPresenter(uy4, z24, mz4, pz4, loaderManager);
    }
}
