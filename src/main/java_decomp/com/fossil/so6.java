package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so6 {
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0013, code lost:
        r1 = r1.d;
     */
    @DexIgnore
    public static final Throwable a(ck6<?> ck6, Throwable th) {
        xe6<T> xe6;
        wg6.b(ck6, "$this$tryRecover");
        wg6.b(th, "exception");
        if (!(ck6 instanceof ro6)) {
            ck6 = null;
        }
        ro6 ro6 = (ro6) ck6;
        return (ro6 == null || xe6 == null) ? th : to6.a(th, (xe6<?>) xe6);
    }
}
