package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq0 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ hm1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bq0(hm1 hm1) {
        super(1);
        this.a = hm1;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        hm1 hm1 = this.a;
        long j = ((e11) obj).J;
        hm1.H = 0;
        hm1.G = 0;
        hm1.I = 0;
        hm1.F = j;
        long j2 = hm1.F;
        int i = (j2 > hm1.C ? 1 : (j2 == hm1.C ? 0 : -1));
        if (i > 0 || j2 <= 0) {
            hm1.F = 0;
            hm1.m();
        } else if (i == 0) {
            hm1.n();
        } else {
            hm1.G = j2;
            hm1.o();
        }
        return cd6.a;
    }
}
