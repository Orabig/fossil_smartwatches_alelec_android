package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ru6 extends nu6 {
    @DexIgnore
    public String option;

    @DexIgnore
    public ru6(String str) {
        super(str);
    }

    @DexIgnore
    public String getOption() {
        return this.option;
    }

    @DexIgnore
    public ru6(String str, String str2) {
        this(str);
        this.option = str2;
    }
}
