package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en6 implements am6, qk6 {
    @DexIgnore
    public static /* final */ en6 a; // = new en6();

    @DexIgnore
    public boolean a(Throwable th) {
        wg6.b(th, "cause");
        return false;
    }

    @DexIgnore
    public void dispose() {
    }

    @DexIgnore
    public String toString() {
        return "NonDisposableHandle";
    }
}
