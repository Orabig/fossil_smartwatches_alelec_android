package com.fossil;

import com.fossil.rm6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xm6<J extends rm6> extends zk6 implements am6, mm6 {
    @DexIgnore
    public /* final */ J d;

    @DexIgnore
    public xm6(J j) {
        wg6.b(j, "job");
        this.d = j;
    }

    @DexIgnore
    public dn6 a() {
        return null;
    }

    @DexIgnore
    public void dispose() {
        J j = this.d;
        if (j != null) {
            ((ym6) j).b((xm6<?>) this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.JobSupport");
    }

    @DexIgnore
    public boolean isActive() {
        return true;
    }
}
