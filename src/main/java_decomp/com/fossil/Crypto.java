package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Base64;
import java.security.KeyPair;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Crypto {
    @DexIgnore
    public static /* final */ b91 a; // = b91.CBC_PKCS5_PADDING;
    @DexIgnore
    public static /* final */ byte[] b; // = new byte[16];
    @DexIgnore
    public static byte[] c;
    @DexIgnore
    public static byte[] d;
    @DexIgnore
    @TargetApi(23)
    public static byte[] e;
    @DexIgnore
    public static /* final */ Crypto instance;

    /*
    static {
        SharedPreferences a2;
        Crypto crypto = new Crypto();
        instance = crypto;
        if (c == null && (a2 = cw0.a(lp0.TEXT_ENCRYPTION_PREFERENCE)) != null) {
            String string = a2.getString("text_encryption_secret_key", (String) null);
            if (string == null) {
                string = a2.getString("text_encryption_key", (String) null);
            }
            if (string != null) {
                c = Base64.decode(string, 0);
            }
        }
        crypto.a();
        crypto.b();
    }
    */

    @DexIgnore
    public final void a() {
        SharedPreferences a2;
        if (d == null) {
            KeyPair a3 = rl1.b.a("a");
            byte[] bArr = null;
            if (a3 == null) {
                Context a4 = gk0.f.a();
                a3 = a4 != null ? rl1.b.a(a4, "a") : null;
            }
            if (a3 != null && (a2 = cw0.a(lp0.TEXT_ENCRYPTION_PREFERENCE)) != null) {
                String string = a2.getString("b", (String) null);
                if (string != null) {
                    bArr = Base64.decode(string, 0);
                }
                if (bArr == null) {
                    SecureRandom secureRandom = new SecureRandom();
                    KeyGenerator instance2 = KeyGenerator.getInstance("AES");
                    instance2.init(256, secureRandom);
                    SecretKey generateKey = instance2.generateKey();
                    wg6.a(generateKey, "keyGenerator.generateKey()");
                    byte[] encoded = generateKey.getEncoded();
                    Cipher instance3 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    instance3.init(1, a3.getPublic());
                    bArr = instance3.doFinal(encoded);
                    a2.edit().putString("b", Base64.encodeToString(bArr, 0)).apply();
                }
                Cipher instance4 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                instance4.init(2, a3.getPrivate());
                d = instance4.doFinal(bArr);
            }
        }
    }

    @DexIgnore
    public final void b() {
        byte[] bArr;
        byte[] bArr2;
        if (Build.VERSION.SDK_INT >= 23 && e == null) {
            SecretKey b2 = rl1.b.b("c");
            SharedPreferences a2 = cw0.a(lp0.TEXT_ENCRYPTION_PREFERENCE);
            if (a2 != null) {
                byte[] bArr3 = null;
                String string = a2.getString("d", (String) null);
                byte[] decode = string != null ? Base64.decode(string, 0) : null;
                String string2 = a2.getString("e", (String) null);
                if (string2 != null) {
                    bArr3 = Base64.decode(string2, 0);
                }
                if (decode == null || bArr3 == null) {
                    SecureRandom secureRandom = new SecureRandom();
                    KeyGenerator instance2 = KeyGenerator.getInstance("AES");
                    instance2.init(256, secureRandom);
                    SecretKey generateKey = instance2.generateKey();
                    wg6.a(generateKey, "keyGenerator.generateKey()");
                    byte[] encoded = generateKey.getEncoded();
                    Cipher instance3 = Cipher.getInstance(b91.GCM_NO_PADDING.a);
                    instance3.init(1, b2);
                    bArr2 = instance3.doFinal(encoded);
                    wg6.a(instance3, "cipher");
                    bArr = instance3.getIV();
                    a2.edit().putString("d", Base64.encodeToString(bArr2, 0)).putString("e", Base64.encodeToString(bArr, 0)).apply();
                } else {
                    byte[] bArr4 = decode;
                    bArr = bArr3;
                    bArr2 = bArr4;
                }
                Cipher instance4 = Cipher.getInstance(b91.GCM_NO_PADDING.a);
                instance4.init(2, b2, new GCMParameterSpec(128, bArr));
                e = instance4.doFinal(bArr2);
            }
        }
    }

    @DexIgnore
    public final String decryptAES_CBC(String str) {
        for (byte[] bArr : nd6.e(new byte[][]{e, d, c})) {
            try {
                xa1 xa1 = xa1.a;
                b91 b91 = a;
                byte[] bArr2 = b;
                byte[] decode = Base64.decode(str, 2);
                wg6.a(decode, "Base64.decode(encryptedText, Base64.NO_WRAP)");
                return new String(xa1.a(b91, bArr, bArr2, decode), mi0.A.f());
            } catch (Exception unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public final String encryptAES_CBC(String str) {
        byte[] bArr;
        if (Build.VERSION.SDK_INT >= 23) {
            b();
            bArr = e;
        } else {
            a();
            bArr = d;
        }
        if (bArr != null) {
            try {
                b91 b91 = a;
                byte[] bArr2 = b;
                byte[] bytes = str.getBytes(mi0.A.f());
                wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
                e71 e71 = e71.ENCRYPT;
                SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
                Cipher instance2 = Cipher.getInstance(b91.a);
                instance2.init(e71.a, secretKeySpec, new IvParameterSpec(bArr2));
                byte[] doFinal = instance2.doFinal(bytes);
                wg6.a(doFinal, "cipher.doFinal(data)");
                return Base64.encodeToString(doFinal, 2);
            } catch (Exception unused) {
            }
        }
        return null;
    }
}
