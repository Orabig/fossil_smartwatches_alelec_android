package com.fossil;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j34 extends RecyclerView.g<b> {
    @DexIgnore
    public ArrayList<Style> a; // = new ArrayList<>();
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public /* final */ ArrayList<Theme> c;
    @DexIgnore
    public a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void S(String str);

        @DexIgnore
        void U(String str);

        @DexIgnore
        void V(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ AppCompatCheckBox a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ j34 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a c = this.a.d.d;
                if (c != null) {
                    c.U(((Theme) this.a.d.c.get(this.a.getAdapterPosition())).getId());
                }
                j34 j34 = this.a.d;
                j34.b = ((Theme) j34.c.get(this.a.getAdapterPosition())).getId();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.j34$b$b")
        /* renamed from: com.fossil.j34$b$b  reason: collision with other inner class name */
        public static final class C0016b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0016b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a c = this.a.d.d;
                if (c != null) {
                    c.S(((Theme) this.a.d.c.get(this.a.getAdapterPosition())).getId());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public c(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a c = this.a.d.d;
                if (c != null) {
                    c.V(((Theme) this.a.d.c.get(this.a.getAdapterPosition())).getId());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(j34 j34, View view) {
            super(view);
            wg6.b(view, "view");
            this.d = j34;
            this.a = view.findViewById(2131361974);
            this.b = (ImageView) view.findViewById(2131362570);
            this.c = (ImageView) view.findViewById(2131362565);
            this.a.setOnClickListener(new a(this));
            this.b.setOnClickListener(new C0016b(this));
            this.c.setOnClickListener(new c(this));
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(Theme theme) {
            wg6.b(theme, "theme");
            if (wg6.a((Object) theme.getType(), (Object) "fixed")) {
                ImageView imageView = this.b;
                wg6.a((Object) imageView, "ivEdit");
                imageView.setVisibility(4);
                ImageView imageView2 = this.c;
                wg6.a((Object) imageView2, "ivDelete");
                imageView2.setVisibility(4);
            }
            AppCompatCheckBox appCompatCheckBox = this.a;
            wg6.a((Object) appCompatCheckBox, "cbCurrentTheme");
            appCompatCheckBox.setChecked(wg6.a((Object) this.d.b, (Object) theme.getId()));
            AppCompatCheckBox appCompatCheckBox2 = this.a;
            wg6.a((Object) appCompatCheckBox2, "cbCurrentTheme");
            appCompatCheckBox2.setText(theme.getName());
            String a2 = ThemeManager.l.a().a("primaryText", this.d.a);
            String a3 = ThemeManager.l.a().a("nonBrandSurface", this.d.a);
            Typeface b2 = ThemeManager.l.a().b("headline2", this.d.a);
            if (a2 != null) {
                this.a.setTextColor(Color.parseColor(a2));
                oa.a(this.a, new ColorStateList(new int[][]{new int[]{-16842910}, new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(a2), Color.parseColor(a3), Color.parseColor(a3)}));
                this.c.setColorFilter(Color.parseColor(a2));
                this.b.setColorFilter(Color.parseColor(a2));
            }
            if (b2 != null) {
                AppCompatCheckBox appCompatCheckBox3 = this.a;
                wg6.a((Object) appCompatCheckBox3, "cbCurrentTheme");
                appCompatCheckBox3.setTypeface(b2);
            }
        }
    }

    @DexIgnore
    public j34(ArrayList<Theme> arrayList, a aVar) {
        wg6.b(arrayList, "mData");
        this.c = arrayList;
        this.d = aVar;
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558687, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026tem_theme, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public final void a(ArrayList<Theme> arrayList) {
        wg6.b(arrayList, "ids");
        this.c.clear();
        this.c.addAll(arrayList);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(List<Style> list, String str) {
        wg6.b(list, "allStyles");
        wg6.b(str, "selectedThemeId");
        this.a.clear();
        this.a.addAll(list);
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wg6.b(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            Theme theme = this.c.get(i);
            wg6.a((Object) theme, "mData[position]");
            bVar.a(theme);
        }
    }
}
