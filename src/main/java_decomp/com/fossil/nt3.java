package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt3 implements Closeable {
    @DexIgnore
    public /* final */ URL a;
    @DexIgnore
    public qc3<Bitmap> b;
    @DexIgnore
    public volatile InputStream c;

    @DexIgnore
    public nt3(URL url) {
        this.a = url;
    }

    @DexIgnore
    public static nt3 e(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return new nt3(new URL(str));
        } catch (MalformedURLException unused) {
            String valueOf = String.valueOf(str);
            Log.w("FirebaseMessaging", valueOf.length() != 0 ? "Not downloading image, bad URL: ".concat(valueOf) : new String("Not downloading image, bad URL: "));
            return null;
        }
    }

    @DexIgnore
    public final void a(Executor executor) {
        this.b = tc3.a(executor, new mt3(this));
    }

    @DexIgnore
    public final void close() {
        pb2.a(this.c);
    }

    @DexIgnore
    public final qc3<Bitmap> k() {
        qc3<Bitmap> qc3 = this.b;
        w12.a(qc3);
        return qc3;
    }

    @DexIgnore
    public final Bitmap l() throws IOException {
        String valueOf = String.valueOf(this.a);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
        sb.append("Starting download of: ");
        sb.append(valueOf);
        Log.i("FirebaseMessaging", sb.toString());
        byte[] m = m();
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(m, 0, m.length);
        if (decodeByteArray != null) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                String valueOf2 = String.valueOf(this.a);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 31);
                sb2.append("Successfully downloaded image: ");
                sb2.append(valueOf2);
                Log.d("FirebaseMessaging", sb2.toString());
            }
            return decodeByteArray;
        }
        String valueOf3 = String.valueOf(this.a);
        StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf3).length() + 24);
        sb3.append("Failed to decode image: ");
        sb3.append(valueOf3);
        throw new IOException(sb3.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0068, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0069, code lost:
        if (r0 != null) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0070, code lost:
        com.fossil.rb2.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0073, code lost:
        throw r2;
     */
    @DexIgnore
    public final byte[] m() throws IOException {
        URLConnection openConnection = this.a.openConnection();
        if (openConnection.getContentLength() <= 1048576) {
            InputStream inputStream = openConnection.getInputStream();
            this.c = inputStream;
            byte[] a2 = ob2.a(ob2.a(inputStream, 1048577));
            if (inputStream != null) {
                inputStream.close();
            }
            if (Log.isLoggable("FirebaseMessaging", 2)) {
                int length = a2.length;
                String valueOf = String.valueOf(this.a);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 34);
                sb.append("Downloaded ");
                sb.append(length);
                sb.append(" bytes from ");
                sb.append(valueOf);
                Log.v("FirebaseMessaging", sb.toString());
            }
            if (a2.length <= 1048576) {
                return a2;
            }
            throw new IOException("Image exceeds max size of 1048576");
        }
        throw new IOException("Content-Length exceeds max size of 1048576");
    }
}
