package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.Arrays;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zz5 extends LinearLayout implements CustomSwipeRefreshLayout.c {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public ImageView b;
    @DexIgnore
    public FlexibleTextView c;
    @DexIgnore
    public FlexibleTextView d;
    @DexIgnore
    public FlexibleTextView e;
    @DexIgnore
    public FlexibleTextView f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ String i; // = ThemeManager.l.a().b("nonBrandSurface");
    @DexIgnore
    public fr j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ zz5 a;

        @DexIgnore
        public b(zz5 zz5) {
            this.a = zz5;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            wg6.b(str, "serial");
            wg6.b(str2, "filePath");
            er a2 = this.a.getMRequestManager$app_fossilRelease().a(str2);
            ImageView ivDevice$app_fossilRelease = this.a.getIvDevice$app_fossilRelease();
            if (ivDevice$app_fossilRelease != null) {
                a2.a(ivDevice$app_fossilRelease);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zz5(Context context) {
        super(context);
        wg6.b(context, "context");
        fr d2 = wq.d(PortfolioApp.get.instance());
        wg6.a((Object) d2, "Glide.with(PortfolioApp.instance)");
        this.j = d2;
        setWillNotDraw(false);
        a();
    }

    @DexIgnore
    public final void a() {
        View inflate = LayoutInflater.from(getContext()).inflate(2131558804, (ViewGroup) null);
        if (inflate != null) {
            this.a = (ViewGroup) inflate;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            ViewGroup viewGroup = this.a;
            if (viewGroup != null) {
                this.c = (FlexibleTextView) viewGroup.findViewById(2131362276);
                this.d = (FlexibleTextView) viewGroup.findViewById(2131362284);
                this.e = (FlexibleTextView) viewGroup.findViewById(2131362279);
                this.f = (FlexibleTextView) viewGroup.findViewById(2131362281);
                this.b = (ImageView) viewGroup.findViewById(2131362529);
                String str = this.i;
                if (str != null) {
                    viewGroup.setBackgroundColor(Color.parseColor(str));
                }
                addView(viewGroup, layoutParams);
                return;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type android.view.ViewGroup");
    }

    @DexIgnore
    public final ImageView getIvDevice$app_fossilRelease() {
        return this.b;
    }

    @DexIgnore
    public final fr getMRequestManager$app_fossilRelease() {
        return this.j;
    }

    @DexIgnore
    public final void setIvDevice$app_fossilRelease(ImageView imageView) {
        this.b = imageView;
    }

    @DexIgnore
    public final void setMRequestManager$app_fossilRelease(fr frVar) {
        wg6.b(frVar, "<set-?>");
        this.j = frVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r2v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void a(CustomSwipeRefreshLayout.e eVar, CustomSwipeRefreshLayout.e eVar2) {
        Object r2;
        Object r22;
        wg6.b(eVar, "currentState");
        wg6.b(eVar2, "lastState");
        int a2 = eVar.a();
        int a3 = eVar2.a();
        if (a2 != a3) {
            if (a2 == 0) {
                Object r23 = this.d;
                if (r23 != 0) {
                    r23.setText(jm4.a(getContext(), 2131886575));
                }
            } else if (a2 != 1) {
                if (a2 == 2) {
                    Object r24 = this.d;
                    if (r24 != 0) {
                        r24.setText(jm4.a(getContext(), 2131886578));
                    }
                } else if (a2 == 3 && (r22 = this.d) != 0) {
                    r22.setText(jm4.a(getContext(), 2131886549));
                }
            } else if (!(a3 == 1 || (r2 = this.d) == 0)) {
                r2.setText(jm4.a(getContext(), 2131886576));
            }
            a(this.g, this.h);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r12v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r11v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void a(String str, String str2) {
        String str3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeadViewCard", "updateStaticInfo - deviceSerial=" + str);
        this.g = str;
        this.h = str2;
        Object instance = PortfolioApp.get.instance();
        if (TextUtils.isEmpty(str)) {
            Object r11 = this.f;
            if (r11 != 0) {
                r11.setVisibility(0);
                return;
            }
            return;
        }
        Object r1 = this.f;
        if (r1 != 0) {
            r1.setVisibility(8);
        }
        Object r12 = this.c;
        if (r12 != 0) {
            r12.setText(str2);
        }
        long g2 = new an4(instance).g(instance.e());
        if (g2 <= 0) {
            str3 = jm4.a((Context) instance, 2131887136);
            wg6.a((Object) str3, "LanguageHelper.getString\u2026 R.string.empty_hour_min)");
        } else {
            Calendar instance2 = Calendar.getInstance();
            wg6.a((Object) instance2, "calendar");
            instance2.setTimeInMillis(g2);
            Boolean t = bk4.t(instance2.getTime());
            wg6.a((Object) t, "DateHelper.isToday(calendar.time)");
            if (t.booleanValue()) {
                nh6 nh6 = nh6.a;
                String a2 = jm4.a((Context) instance, 2131886478);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026ay_Title__TodayMonthDate)");
                Object[] objArr = {bk4.g(instance2.getTime())};
                str3 = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) str3, "java.lang.String.format(format, *args)");
            } else {
                Calendar instance3 = Calendar.getInstance();
                wg6.a((Object) instance3, "Calendar.getInstance()");
                long timeInMillis = (instance3.getTimeInMillis() - g2) / 3600000;
                long j2 = (long) 24;
                if (timeInMillis < j2) {
                    nh6 nh62 = nh6.a;
                    String a3 = jm4.a((Context) instance, 2131887300);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    Object[] objArr2 = {String.valueOf(timeInMillis)};
                    str3 = String.format(a3, Arrays.copyOf(objArr2, objArr2.length));
                    wg6.a((Object) str3, "java.lang.String.format(format, *args)");
                } else {
                    nh6 nh63 = nh6.a;
                    String a4 = jm4.a((Context) instance, 2131887300);
                    wg6.a((Object) a4, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    Object[] objArr3 = {String.valueOf(timeInMillis / j2)};
                    str3 = String.format(a4, Arrays.copyOf(objArr3, objArr3.length));
                    wg6.a((Object) str3, "java.lang.String.format(format, *args)");
                }
            }
        }
        Object r4 = this.e;
        if (r4 != 0) {
            nh6 nh64 = nh6.a;
            String a5 = jm4.a((Context) instance, 2131886932);
            wg6.a((Object) a5, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
            Object[] objArr4 = {str3};
            String format = String.format(a5, Arrays.copyOf(objArr4, objArr4.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            r4.setText(format);
        }
        Object r122 = this.e;
        if (r122 != 0) {
            r122.setSelected(true);
        }
        CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
        if (str != null) {
            CloudImageHelper.ItemImage type = with.setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView = this.b;
            if (imageView != null) {
                type.setPlaceHolder(imageView, DeviceHelper.o.b(str, DeviceHelper.ImageStyle.SMALL)).setImageCallback(new b(this)).download();
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
