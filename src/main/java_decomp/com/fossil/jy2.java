package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jy2 extends jh2 implements xx2 {
    @DexIgnore
    public jy2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IUiSettingsDelegate");
    }

    @DexIgnore
    public final void d(boolean z) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, z);
        b(18, zza);
    }
}
