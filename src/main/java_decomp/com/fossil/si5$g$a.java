package com.fossil;

import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class si5$g$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter.g this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Long.valueOf(((HeartRateSample) t).getStartTimeId().getMillis()), Long.valueOf(((HeartRateSample) t2).getStartTimeId().getMillis()));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public si5$g$a(HeartRateDetailPresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        si5$g$a si5_g_a = new si5$g$a(this.this$0, xe6);
        si5_g_a.p$ = (il6) obj;
        return si5_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((si5$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            List f = this.this$0.this$0.k;
            if (f == null) {
                return null;
            }
            if (f.size() > 1) {
                ud6.a(f, new a());
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
