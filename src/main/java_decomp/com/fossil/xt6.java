package com.fossil;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xt6 extends mt6 {
    @DexIgnore
    public /* final */ transient int[] directory;
    @DexIgnore
    public /* final */ transient byte[][] segments;

    @DexIgnore
    public xt6(jt6 jt6, int i) {
        super((byte[]) null);
        bu6.a(jt6.b, 0, (long) i);
        int i2 = 0;
        vt6 vt6 = jt6.a;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            int i5 = vt6.c;
            int i6 = vt6.b;
            if (i5 != i6) {
                i3 += i5 - i6;
                i4++;
                vt6 = vt6.f;
            } else {
                throw new AssertionError("s.limit == s.pos");
            }
        }
        this.segments = new byte[i4][];
        this.directory = new int[(i4 * 2)];
        vt6 vt62 = jt6.a;
        int i7 = 0;
        while (i2 < i) {
            this.segments[i7] = vt62.a;
            i2 += vt62.c - vt62.b;
            if (i2 > i) {
                i2 = i;
            }
            int[] iArr = this.directory;
            iArr[i7] = i2;
            iArr[this.segments.length + i7] = vt62.b;
            vt62.d = true;
            i7++;
            vt62 = vt62.f;
        }
    }

    @DexIgnore
    private Object writeReplace() {
        return a();
    }

    @DexIgnore
    public final int a(int i) {
        int binarySearch = Arrays.binarySearch(this.directory, 0, this.segments.length, i + 1);
        return binarySearch >= 0 ? binarySearch : ~binarySearch;
    }

    @DexIgnore
    public ByteBuffer asByteBuffer() {
        return ByteBuffer.wrap(toByteArray()).asReadOnlyBuffer();
    }

    @DexIgnore
    public String base64() {
        return a().base64();
    }

    @DexIgnore
    public String base64Url() {
        return a().base64Url();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof mt6) {
            mt6 mt6 = (mt6) obj;
            if (mt6.size() != size() || !rangeEquals(0, mt6, 0, size())) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public byte getByte(int i) {
        int i2;
        bu6.a((long) this.directory[this.segments.length - 1], (long) i, 1);
        int a = a(i);
        if (a == 0) {
            i2 = 0;
        } else {
            i2 = this.directory[a - 1];
        }
        int[] iArr = this.directory;
        byte[][] bArr = this.segments;
        return bArr[a][(i - i2) + iArr[bArr.length + a]];
    }

    @DexIgnore
    public int hashCode() {
        int i = this.hashCode;
        if (i != 0) {
            return i;
        }
        int length = this.segments.length;
        int i2 = 0;
        int i3 = 1;
        int i4 = 0;
        while (i2 < length) {
            byte[] bArr = this.segments[i2];
            int[] iArr = this.directory;
            int i5 = iArr[length + i2];
            int i6 = iArr[i2];
            int i7 = (i6 - i4) + i5;
            while (i5 < i7) {
                i3 = (i3 * 31) + bArr[i5];
                i5++;
            }
            i2++;
            i4 = i6;
        }
        this.hashCode = i3;
        return i3;
    }

    @DexIgnore
    public String hex() {
        return a().hex();
    }

    @DexIgnore
    public mt6 hmacSha1(mt6 mt6) {
        return a().hmacSha1(mt6);
    }

    @DexIgnore
    public mt6 hmacSha256(mt6 mt6) {
        return a().hmacSha256(mt6);
    }

    @DexIgnore
    public int indexOf(byte[] bArr, int i) {
        return a().indexOf(bArr, i);
    }

    @DexIgnore
    public byte[] internalArray() {
        return toByteArray();
    }

    @DexIgnore
    public int lastIndexOf(byte[] bArr, int i) {
        return a().lastIndexOf(bArr, i);
    }

    @DexIgnore
    public mt6 md5() {
        return a().md5();
    }

    @DexIgnore
    public boolean rangeEquals(int i, mt6 mt6, int i2, int i3) {
        int i4;
        if (i < 0 || i > size() - i3) {
            return false;
        }
        int a = a(i);
        while (i3 > 0) {
            if (a == 0) {
                i4 = 0;
            } else {
                i4 = this.directory[a - 1];
            }
            int min = Math.min(i3, ((this.directory[a] - i4) + i4) - i);
            int[] iArr = this.directory;
            byte[][] bArr = this.segments;
            if (!mt6.rangeEquals(i2, bArr[a], (i - i4) + iArr[bArr.length + a], min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            a++;
        }
        return true;
    }

    @DexIgnore
    public mt6 sha1() {
        return a().sha1();
    }

    @DexIgnore
    public mt6 sha256() {
        return a().sha256();
    }

    @DexIgnore
    public int size() {
        return this.directory[this.segments.length - 1];
    }

    @DexIgnore
    public String string(Charset charset) {
        return a().string(charset);
    }

    @DexIgnore
    public mt6 substring(int i) {
        return a().substring(i);
    }

    @DexIgnore
    public mt6 toAsciiLowercase() {
        return a().toAsciiLowercase();
    }

    @DexIgnore
    public mt6 toAsciiUppercase() {
        return a().toAsciiUppercase();
    }

    @DexIgnore
    public byte[] toByteArray() {
        int[] iArr = this.directory;
        byte[][] bArr = this.segments;
        byte[] bArr2 = new byte[iArr[bArr.length - 1]];
        int length = bArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int[] iArr2 = this.directory;
            int i3 = iArr2[length + i];
            int i4 = iArr2[i];
            System.arraycopy(this.segments[i], i3, bArr2, i2, i4 - i2);
            i++;
            i2 = i4;
        }
        return bArr2;
    }

    @DexIgnore
    public String toString() {
        return a().toString();
    }

    @DexIgnore
    public String utf8() {
        return a().utf8();
    }

    @DexIgnore
    public void write(OutputStream outputStream) throws IOException {
        if (outputStream != null) {
            int length = this.segments.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                int[] iArr = this.directory;
                int i3 = iArr[length + i];
                int i4 = iArr[i];
                outputStream.write(this.segments[i], i3, i4 - i2);
                i++;
                i2 = i4;
            }
            return;
        }
        throw new IllegalArgumentException("out == null");
    }

    @DexIgnore
    public final mt6 a() {
        return new mt6(toByteArray());
    }

    @DexIgnore
    public mt6 substring(int i, int i2) {
        return a().substring(i, i2);
    }

    @DexIgnore
    public void write(jt6 jt6) {
        int length = this.segments.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            int[] iArr = this.directory;
            int i3 = iArr[length + i];
            int i4 = iArr[i];
            vt6 vt6 = new vt6(this.segments[i], i3, (i3 + i4) - i2, true, false);
            vt6 vt62 = jt6.a;
            if (vt62 == null) {
                vt6.g = vt6;
                vt6.f = vt6;
                jt6.a = vt6;
            } else {
                vt62.g.a(vt6);
            }
            i++;
            i2 = i4;
        }
        jt6.b += (long) i2;
    }

    @DexIgnore
    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        int i4;
        if (i < 0 || i > size() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int a = a(i);
        while (i3 > 0) {
            if (a == 0) {
                i4 = 0;
            } else {
                i4 = this.directory[a - 1];
            }
            int min = Math.min(i3, ((this.directory[a] - i4) + i4) - i);
            int[] iArr = this.directory;
            byte[][] bArr2 = this.segments;
            if (!bu6.a(bArr2[a], (i - i4) + iArr[bArr2.length + a], bArr, i2, min)) {
                return false;
            }
            i += min;
            i2 += min;
            i3 -= min;
            a++;
        }
        return true;
    }
}
