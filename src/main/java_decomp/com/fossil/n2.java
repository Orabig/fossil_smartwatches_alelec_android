package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n2 extends m2 {
    @DexIgnore
    public /* final */ SeekBar d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public ColorStateList f; // = null;
    @DexIgnore
    public PorterDuff.Mode g; // = null;
    @DexIgnore
    public boolean h; // = false;
    @DexIgnore
    public boolean i; // = false;

    @DexIgnore
    public n2(SeekBar seekBar) {
        super(seekBar);
        this.d = seekBar;
    }

    @DexIgnore
    public void a(AttributeSet attributeSet, int i2) {
        super.a(attributeSet, i2);
        i3 a = i3.a(this.d.getContext(), attributeSet, j0.AppCompatSeekBar, i2, 0);
        Drawable c = a.c(j0.AppCompatSeekBar_android_thumb);
        if (c != null) {
            this.d.setThumb(c);
        }
        b(a.b(j0.AppCompatSeekBar_tickMark));
        if (a.g(j0.AppCompatSeekBar_tickMarkTintMode)) {
            this.g = s2.a(a.d(j0.AppCompatSeekBar_tickMarkTintMode, -1), this.g);
            this.i = true;
        }
        if (a.g(j0.AppCompatSeekBar_tickMarkTint)) {
            this.f = a.a(j0.AppCompatSeekBar_tickMarkTint);
            this.h = true;
        }
        a.a();
        c();
    }

    @DexIgnore
    public void b(Drawable drawable) {
        Drawable drawable2 = this.e;
        if (drawable2 != null) {
            drawable2.setCallback((Drawable.Callback) null);
        }
        this.e = drawable;
        if (drawable != null) {
            drawable.setCallback(this.d);
            o7.a(drawable, x9.o(this.d));
            if (drawable.isStateful()) {
                drawable.setState(this.d.getDrawableState());
            }
            c();
        }
        this.d.invalidate();
    }

    @DexIgnore
    public final void c() {
        if (this.e == null) {
            return;
        }
        if (this.h || this.i) {
            this.e = o7.i(this.e.mutate());
            if (this.h) {
                o7.a(this.e, this.f);
            }
            if (this.i) {
                o7.a(this.e, this.g);
            }
            if (this.e.isStateful()) {
                this.e.setState(this.d.getDrawableState());
            }
        }
    }

    @DexIgnore
    public void d() {
        Drawable drawable = this.e;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.d.getDrawableState())) {
            this.d.invalidateDrawable(drawable);
        }
    }

    @DexIgnore
    public void e() {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    @DexIgnore
    public void a(Canvas canvas) {
        if (this.e != null) {
            int max = this.d.getMax();
            int i2 = 1;
            if (max > 1) {
                int intrinsicWidth = this.e.getIntrinsicWidth();
                int intrinsicHeight = this.e.getIntrinsicHeight();
                int i3 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
                if (intrinsicHeight >= 0) {
                    i2 = intrinsicHeight / 2;
                }
                this.e.setBounds(-i3, -i2, i3, i2);
                float width = ((float) ((this.d.getWidth() - this.d.getPaddingLeft()) - this.d.getPaddingRight())) / ((float) max);
                int save = canvas.save();
                canvas.translate((float) this.d.getPaddingLeft(), (float) (this.d.getHeight() / 2));
                for (int i4 = 0; i4 <= max; i4++) {
                    this.e.draw(canvas);
                    canvas.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                canvas.restoreToCount(save);
            }
        }
    }
}
