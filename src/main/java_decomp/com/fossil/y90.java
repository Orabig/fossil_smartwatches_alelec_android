package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y90 extends x90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ ce0 d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<y90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new y90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new y90[i];
        }
    }

    @DexIgnore
    public y90(byte b, int i, ce0 ce0, String str) {
        super(e90.IFTTT_APP, b, i);
        this.d = ce0;
        this.e = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(y90.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            y90 y90 = (y90) obj;
            if (this.d != y90.d) {
                return false;
            }
            String str = this.e;
            String str2 = y90.e;
            if (str != null) {
                return !str.contentEquals(str2);
            }
            throw new rc6("null cannot be cast to non-null type java.lang.String");
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.IFTTTWatchAppRequest");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.d.hashCode();
        return this.e.hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
        if (parcel != null) {
            parcel.writeString(this.e);
        }
    }

    @DexIgnore
    public /* synthetic */ y90(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.d = ce0.values()[parcel.readInt()];
        String readString = parcel.readString();
        if (readString != null) {
            this.e = readString;
        } else {
            wg6.a();
            throw null;
        }
    }
}
