package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zf0 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        r40 r40;
        String str = null;
        String action = intent != null ? intent.getAction() : null;
        if (action != null && action.hashCode() == -343131398 && action.equals("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED")) {
            ii1 ii1 = (ii1) intent.getParcelableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE");
            q40.d dVar = (q40.d) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE");
            q40.d dVar2 = (q40.d) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE");
            oa1 oa1 = oa1.a;
            kj0 kj0 = kj0.i;
            Object[] objArr = new Object[3];
            if (!(ii1 == null || (r40 = ii1.r) == null)) {
                str = r40.getMacAddress();
            }
            objArr[0] = str;
            objArr[1] = dVar;
            objArr[2] = dVar2;
            if (ii1 != null && dVar != null && dVar2 != null) {
                kj0.i.a(ii1, dVar2);
            }
        }
    }
}
