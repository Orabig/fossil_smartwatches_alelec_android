package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class od0 extends pd0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public static /* final */ short c; // = 255;
    @DexIgnore
    public /* final */ short b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<od0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new od0(cw0.b(parcel.readByte()));
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new od0[i];
        }
    }

    /*
    static {
        lg6 lg6 = lg6.a;
    }
    */

    @DexIgnore
    public od0(short s) throws IllegalArgumentException {
        this.b = s;
        short s2 = c;
        short s3 = this.b;
        if (!(s3 >= 0 && s2 >= s3)) {
            StringBuilder b2 = ze0.b("goalId(");
            b2.append(this.b);
            b2.append(") is out of range ");
            b2.append("[0, ");
            throw new IllegalArgumentException(ze0.a(b2, c, "]."));
        }
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(new JSONObject(), bm0.GOAL_ID, (Object) Short.valueOf(this.b));
    }

    @DexIgnore
    public int c() {
        return 1;
    }

    @DexIgnore
    public byte[] d() {
        ByteBuffer allocate = ByteBuffer.allocate(3);
        wg6.a(allocate, "ByteBuffer.allocate(GOAL\u2026ACKING_TYPE_FRAME_LENGTH)");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        allocate.put(xl0.GOAL_TRACKING.a);
        allocate.put((byte) 1);
        allocate.put((byte) this.b);
        byte[] array = allocate.array();
        wg6.a(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final short getGoalId() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) this.b);
    }
}
