package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fq1 {

    @DexIgnore
    public enum a {
        OK,
        TRANSIENT_ERROR,
        FATAL_ERROR
    }

    @DexIgnore
    public static fq1 a(long j) {
        return new aq1(a.OK, j);
    }

    @DexIgnore
    public static fq1 c() {
        return new aq1(a.FATAL_ERROR, -1);
    }

    @DexIgnore
    public static fq1 d() {
        return new aq1(a.TRANSIENT_ERROR, -1);
    }

    @DexIgnore
    public abstract long a();

    @DexIgnore
    public abstract a b();
}
