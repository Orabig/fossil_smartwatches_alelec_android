package com.fossil;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qi6<T> implements ti6<T> {
    @DexIgnore
    public /* final */ AtomicReference<ti6<T>> a;

    @DexIgnore
    public qi6(ti6<? extends T> ti6) {
        wg6.b(ti6, "sequence");
        this.a = new AtomicReference<>(ti6);
    }

    @DexIgnore
    public Iterator<T> iterator() {
        ti6 andSet = this.a.getAndSet((Object) null);
        if (andSet != null) {
            return andSet.iterator();
        }
        throw new IllegalStateException("This sequence can be consumed only once.");
    }
}
