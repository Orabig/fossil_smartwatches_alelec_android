package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt6 {
    @DexIgnore
    public static vt6 a;
    @DexIgnore
    public static long b;

    @DexIgnore
    public static vt6 a() {
        synchronized (wt6.class) {
            if (a == null) {
                return new vt6();
            }
            vt6 vt6 = a;
            a = vt6.f;
            vt6.f = null;
            b -= 8192;
            return vt6;
        }
    }

    @DexIgnore
    public static void a(vt6 vt6) {
        if (vt6.f != null || vt6.g != null) {
            throw new IllegalArgumentException();
        } else if (!vt6.d) {
            synchronized (wt6.class) {
                if (b + 8192 <= 65536) {
                    b += 8192;
                    vt6.f = a;
                    vt6.c = 0;
                    vt6.b = 0;
                    a = vt6;
                }
            }
        }
    }
}
