package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sb4 extends rb4 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131362442, 1);
        A.put(2131363266, 2);
        A.put(2131362305, 3);
        A.put(2131362549, 4);
        A.put(2131363267, 5);
        A.put(2131362304, 6);
        A.put(2131362548, 7);
        A.put(2131363268, 8);
        A.put(2131362391, 9);
        A.put(2131362607, 10);
        A.put(2131363269, 11);
        A.put(2131362644, 12);
    }
    */

    @DexIgnore
    public sb4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 13, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public sb4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[6], objArr[3], objArr[9], objArr[1], objArr[7], objArr[4], objArr[10], objArr[12], objArr[2], objArr[5], objArr[8], objArr[11]);
        this.y = -1;
        this.x = objArr[0];
        this.x.setTag((Object) null);
        a(view);
        f();
    }
}
