package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.DeviceHelper;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ek4 implements MembersInjector<dk4> {
    @DexIgnore
    public static void a(DeviceHelper deviceHelper, an4 an4) {
        deviceHelper.a = an4;
    }

    @DexIgnore
    public static void a(DeviceHelper deviceHelper, DeviceRepository deviceRepository) {
        deviceHelper.b = deviceRepository;
    }
}
