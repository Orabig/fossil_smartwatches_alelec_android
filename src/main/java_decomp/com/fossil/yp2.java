package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yp2 extends wp2<zp2, zp2> {
    @DexIgnore
    public static void a(Object obj, zp2 zp2) {
        ((fn2) obj).zzb = zp2;
    }

    @DexIgnore
    public final void b(Object obj) {
        ((fn2) obj).zzb.a();
    }

    @DexIgnore
    public final /* synthetic */ int c(Object obj) {
        return ((zp2) obj).b();
    }

    @DexIgnore
    public final /* synthetic */ int d(Object obj) {
        return ((zp2) obj).c();
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, tq2 tq2) throws IOException {
        ((zp2) obj).b(tq2);
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, tq2 tq2) throws IOException {
        ((zp2) obj).a(tq2);
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj, Object obj2) {
        zp2 zp2 = (zp2) obj;
        zp2 zp22 = (zp2) obj2;
        if (zp22.equals(zp2.d())) {
            return zp2;
        }
        return zp2.a(zp2, zp22);
    }

    @DexIgnore
    public final /* synthetic */ Object a(Object obj) {
        return ((fn2) obj).zzb;
    }

    @DexIgnore
    public final /* bridge */ /* synthetic */ void a(Object obj, Object obj2) {
        a(obj, (zp2) obj2);
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, Object obj2) {
        a(obj, (zp2) obj2);
    }

    @DexIgnore
    public final /* synthetic */ Object a() {
        return zp2.e();
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, long j) {
        ((zp2) obj).a(i << 3, (Object) Long.valueOf(j));
    }
}
