package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w25 implements Factory<s25> {
    @DexIgnore
    public static s25 a(t25 t25) {
        s25 d = t25.d();
        z76.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
