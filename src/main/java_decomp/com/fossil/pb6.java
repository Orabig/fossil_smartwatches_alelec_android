package com.fossil;

import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.model.Explore;
import org.joda.time.DateTimeConstants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pb6 implements ac6 {
    @DexIgnore
    public yb6 a(b96 b96, JSONObject jSONObject) throws JSONException {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", DateTimeConstants.SECONDS_PER_HOUR);
        return new yb6(a(b96, (long) optInt2, jSONObject), b(jSONObject.getJSONObject("app")), g(jSONObject.getJSONObject(Constants.SESSION)), f(jSONObject.getJSONObject("prompt")), d(jSONObject.getJSONObject("features")), a(jSONObject.getJSONObject("analytics")), c(jSONObject.getJSONObject("beta")), optInt, optInt2);
    }

    @DexIgnore
    public final ib6 b(JSONObject jSONObject) throws JSONException {
        return new ib6(jSONObject.getString("identifier"), jSONObject.getString("status"), jSONObject.getString("url"), jSONObject.getString("reports_url"), jSONObject.getString("ndk_reports_url"), jSONObject.optBoolean("update_required", false), (!jSONObject.has(MicroApp.COLUMN_ICON) || !jSONObject.getJSONObject(MicroApp.COLUMN_ICON).has("hash")) ? null : e(jSONObject.getJSONObject(MicroApp.COLUMN_ICON)));
    }

    @DexIgnore
    public final kb6 c(JSONObject jSONObject) throws JSONException {
        return new kb6(jSONObject.optString("update_endpoint", zb6.a), jSONObject.optInt("update_suspend_duration", DateTimeConstants.SECONDS_PER_HOUR));
    }

    @DexIgnore
    public final rb6 d(JSONObject jSONObject) {
        return new rb6(jSONObject.optBoolean("prompt_enabled", false), jSONObject.optBoolean("collect_logged_exceptions", true), jSONObject.optBoolean("collect_reports", true), jSONObject.optBoolean("collect_analytics", false), jSONObject.optBoolean("firebase_crashlytics_enabled", false));
    }

    @DexIgnore
    public final gb6 e(JSONObject jSONObject) throws JSONException {
        return new gb6(jSONObject.getString("hash"), jSONObject.getInt(DatabaseFieldConfigLoader.FIELD_NAME_WIDTH), jSONObject.getInt(Constants.PROFILE_KEY_UNITS_HEIGHT));
    }

    @DexIgnore
    public final tb6 f(JSONObject jSONObject) throws JSONException {
        return new tb6(jSONObject.optString(Explore.COLUMN_TITLE, "Send Crash Report?"), jSONObject.optString("message", "Looks like we crashed! Please help us fix the problem by sending a crash report."), jSONObject.optString("send_button_title", "Send"), jSONObject.optBoolean("show_cancel_button", true), jSONObject.optString("cancel_button_title", "Don't Send"), jSONObject.optBoolean("show_always_send_button", true), jSONObject.optString("always_send_button_title", "Always Send"));
    }

    @DexIgnore
    public final ub6 g(JSONObject jSONObject) throws JSONException {
        return new ub6(jSONObject.optInt("log_buffer_size", 64000), jSONObject.optInt("max_chained_exception_depth", 8), jSONObject.optInt("max_custom_exception_events", 64), jSONObject.optInt("max_custom_key_value_pairs", 64), jSONObject.optInt("identifier_mask", 255), jSONObject.optBoolean("send_session_without_crash", false), jSONObject.optInt("max_complete_sessions_count", 4));
    }

    @DexIgnore
    public final fb6 a(JSONObject jSONObject) {
        return new fb6(jSONObject.optString("url", "https://e.crashlytics.com/spi/v2/events"), jSONObject.optInt("flush_interval_secs", 600), jSONObject.optInt("max_byte_size_per_file", MFNetworkReturnCode.REQUEST_NOT_FOUND), jSONObject.optInt("max_file_count_per_send", 1), jSONObject.optInt("max_pending_send_file_count", 100), jSONObject.optBoolean("forward_to_google_analytics", false), jSONObject.optBoolean("include_purchase_events_in_forwarded_events", false), jSONObject.optBoolean("track_custom_events", true), jSONObject.optBoolean("track_predefined_events", true), jSONObject.optInt("sampling_rate", 1), jSONObject.optBoolean("flush_on_background", true));
    }

    @DexIgnore
    public final long a(b96 b96, long j, JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("expires_at")) {
            return jSONObject.getLong("expires_at");
        }
        return b96.a() + (j * 1000);
    }
}
