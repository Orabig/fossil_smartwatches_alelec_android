package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wk6 {
    @DexIgnore
    public static final <T> Object a(Object obj) {
        if (mc6.m7isSuccessimpl(obj)) {
            nc6.a(obj);
            return obj;
        }
        Throwable r4 = mc6.m4exceptionOrNullimpl(obj);
        if (r4 != null) {
            return new vk6(r4, false, 2, (qg6) null);
        }
        wg6.a();
        throw null;
    }
}
