package com.fossil;

import android.content.res.AssetManager;
import android.util.Log;
import com.fossil.fs;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ds<T> implements fs<T> {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ AssetManager b;
    @DexIgnore
    public T c;

    @DexIgnore
    public ds(AssetManager assetManager, String str) {
        this.b = assetManager;
        this.a = str;
    }

    @DexIgnore
    public abstract T a(AssetManager assetManager, String str) throws IOException;

    @DexIgnore
    public void a(br brVar, fs.a<? super T> aVar) {
        try {
            this.c = a(this.b, this.a);
            aVar.a(this.c);
        } catch (IOException e) {
            if (Log.isLoggable("AssetPathFetcher", 3)) {
                Log.d("AssetPathFetcher", "Failed to load data from asset manager", e);
            }
            aVar.a((Exception) e);
        }
    }

    @DexIgnore
    public abstract void a(T t) throws IOException;

    @DexIgnore
    public pr b() {
        return pr.LOCAL;
    }

    @DexIgnore
    public void cancel() {
    }

    @DexIgnore
    public void a() {
        T t = this.c;
        if (t != null) {
            try {
                a(t);
            } catch (IOException unused) {
            }
        }
    }
}
