package com.fossil;

import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.ConstraintProxy;
import com.fossil.sm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qm {
    @DexIgnore
    public static /* final */ String e; // = tl.a("ConstraintsCmdHandler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ sm c;
    @DexIgnore
    public /* final */ an d; // = new an(this.a, this.c.d(), (zm) null);

    @DexIgnore
    public qm(Context context, int i, sm smVar) {
        this.a = context;
        this.b = i;
        this.c = smVar;
    }

    @DexIgnore
    public void a() {
        List<zn> a2 = this.c.e().g().d().a();
        ConstraintProxy.a(this.a, a2);
        this.d.c(a2);
        ArrayList<zn> arrayList = new ArrayList<>(a2.size());
        long currentTimeMillis = System.currentTimeMillis();
        for (zn next : a2) {
            String str = next.a;
            if (currentTimeMillis >= next.a() && (!next.b() || this.d.a(str))) {
                arrayList.add(next);
            }
        }
        for (zn znVar : arrayList) {
            String str2 = znVar.a;
            Intent a3 = pm.a(this.a, str2);
            tl.a().a(e, String.format("Creating a delay_met command for workSpec with id (%s)", new Object[]{str2}), new Throwable[0]);
            sm smVar = this.c;
            smVar.a((Runnable) new sm.b(smVar, a3, this.b));
        }
        this.d.a();
    }
}
