package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.fossil.cx6;
import com.fossil.fx6;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ox6 {
    @DexIgnore
    public static /* final */ ox6 a; // = d();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends ox6 {
        @DexIgnore
        public boolean a(Method method) {
            return method.isDefault();
        }

        @DexIgnore
        public List<? extends fx6.a> b() {
            return Collections.singletonList(mx6.a);
        }

        @DexIgnore
        public int c() {
            return 1;
        }

        @DexIgnore
        public Object a(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
            Constructor<MethodHandles.Lookup> declaredConstructor = MethodHandles.Lookup.class.getDeclaredConstructor(new Class[]{Class.class, Integer.TYPE});
            declaredConstructor.setAccessible(true);
            return declaredConstructor.newInstance(new Object[]{cls, -1}).unreflectSpecial(method, cls).bindTo(obj).invokeWithArguments(objArr);
        }

        @DexIgnore
        public List<? extends cx6.a> a(Executor executor) {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(ex6.a);
            arrayList.add(new gx6(executor));
            return Collections.unmodifiableList(arrayList);
        }
    }

    @DexIgnore
    public static ox6 d() {
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                return new a();
            }
        } catch (ClassNotFoundException unused) {
        }
        try {
            Class.forName("java.util.Optional");
            return new b();
        } catch (ClassNotFoundException unused2) {
            return new ox6();
        }
    }

    @DexIgnore
    public static ox6 e() {
        return a;
    }

    @DexIgnore
    public List<? extends cx6.a> a(Executor executor) {
        return Collections.singletonList(new gx6(executor));
    }

    @DexIgnore
    public Executor a() {
        return null;
    }

    @DexIgnore
    public boolean a(Method method) {
        return false;
    }

    @DexIgnore
    public List<? extends fx6.a> b() {
        return Collections.emptyList();
    }

    @DexIgnore
    public int c() {
        return 0;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ox6 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ox6$a$a")
        /* renamed from: com.fossil.ox6$a$a  reason: collision with other inner class name */
        public static class C0004a implements Executor {
            @DexIgnore
            public /* final */ Handler a; // = new Handler(Looper.getMainLooper());

            @DexIgnore
            public void execute(Runnable runnable) {
                this.a.post(runnable);
            }
        }

        @DexIgnore
        public boolean a(Method method) {
            if (Build.VERSION.SDK_INT < 24) {
                return false;
            }
            return method.isDefault();
        }

        @DexIgnore
        public List<? extends fx6.a> b() {
            if (Build.VERSION.SDK_INT >= 24) {
                return Collections.singletonList(mx6.a);
            }
            return Collections.emptyList();
        }

        @DexIgnore
        public int c() {
            return Build.VERSION.SDK_INT >= 24 ? 1 : 0;
        }

        @DexIgnore
        public Executor a() {
            return new C0004a();
        }

        @DexIgnore
        public List<? extends cx6.a> a(Executor executor) {
            if (executor != null) {
                gx6 gx6 = new gx6(executor);
                if (Build.VERSION.SDK_INT < 24) {
                    return Collections.singletonList(gx6);
                }
                return Arrays.asList(new cx6.a[]{ex6.a, gx6});
            }
            throw new AssertionError();
        }
    }

    @DexIgnore
    public Object a(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
        throw new UnsupportedOperationException();
    }
}
