package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fj2 extends fn2<fj2, a> implements to2 {
    @DexIgnore
    public static /* final */ fj2 zzh;
    @DexIgnore
    public static volatile yo2<fj2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public boolean zze;
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public int zzg;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<fj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(fj2.zzh);
        }

        @DexIgnore
        public final a a(String str) {
            f();
            ((fj2) this.b).a(str);
            return this;
        }

        @DexIgnore
        public final String j() {
            return ((fj2) this.b).n();
        }

        @DexIgnore
        public final boolean k() {
            return ((fj2) this.b).o();
        }

        @DexIgnore
        public final boolean l() {
            return ((fj2) this.b).p();
        }

        @DexIgnore
        public final boolean m() {
            return ((fj2) this.b).q();
        }

        @DexIgnore
        public final int n() {
            return ((fj2) this.b).r();
        }

        @DexIgnore
        public /* synthetic */ a(jj2 jj2) {
            this();
        }
    }

    /*
    static {
        fj2 fj2 = new fj2();
        zzh = fj2;
        fn2.a(fj2.class, fj2);
    }
    */

    @DexIgnore
    public final void a(String str) {
        if (str != null) {
            this.zzc |= 1;
            this.zzd = str;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final String n() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean o() {
        return this.zze;
    }

    @DexIgnore
    public final boolean p() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final int r() {
        return this.zzg;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (jj2.a[i - 1]) {
            case 1:
                return new fj2();
            case 2:
                return new a((jj2) null);
            case 3:
                return fn2.a((ro2) zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\b\u0000\u0002\u0007\u0001\u0003\u0007\u0002\u0004\u0004\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                yo2<fj2> yo2 = zzi;
                if (yo2 == null) {
                    synchronized (fj2.class) {
                        yo2 = zzi;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzh);
                            zzi = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
