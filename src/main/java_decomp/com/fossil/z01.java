package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z01 extends l61 {
    @DexIgnore
    public /* final */ byte[] A;
    @DexIgnore
    public /* final */ em0 B;

    @DexIgnore
    public z01(ue1 ue1, em0 em0) {
        super(lx0.SEND_ASYNC_EVENT_ACK, ue1);
        this.B = em0;
        em0 em02 = this.B;
        byte[] b = em02.b();
        ByteBuffer order = ByteBuffer.allocate(b.length + 3).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer\n             \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(kl0.NOTIFY.a);
        order.put(em02.a.a);
        order.put(em02.b);
        order.put(b);
        byte[] array = order.array();
        wg6.a(array, "ackData.array()");
        this.A = array;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.DEVICE_EVENT, (Object) this.B.a());
    }

    @DexIgnore
    public ok0 l() {
        return new fb1(rg1.ASYNC, this.A, this.y.v);
    }
}
