package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ne0 extends p40 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public rg1 b;
    @DexIgnore
    public byte[] c;
    @DexIgnore
    public /* final */ JSONObject d;

    @DexIgnore
    public /* synthetic */ ne0(long j, rg1 rg1, byte[] bArr, JSONObject jSONObject, int i) {
        j = (i & 1) != 0 ? System.currentTimeMillis() : j;
        rg1 = (i & 2) != 0 ? null : rg1;
        bArr = (i & 4) != 0 ? new byte[0] : bArr;
        jSONObject = (i & 8) != 0 ? new JSONObject() : jSONObject;
        this.a = j;
        this.b = rg1;
        this.c = bArr;
        this.d = jSONObject;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject a2 = cw0.a(new JSONObject(), bm0.TIMESTAMP, (Object) Double.valueOf(cw0.a(this.a)));
        bm0 bm0 = bm0.CHANNEL_ID;
        rg1 rg1 = this.b;
        return cw0.a(cw0.a(cw0.a(a2, bm0, (Object) rg1 != null ? rg1.a : null), bm0.RAW_DATA, (Object) cw0.a(this.c, (String) null, 1)), this.d);
    }
}
