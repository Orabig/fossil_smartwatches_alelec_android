package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m55 implements Factory<l55> {
    @DexIgnore
    public static CommuteTimeSettingsPresenter a(z45 z45, an4 an4, UserRepository userRepository) {
        return new CommuteTimeSettingsPresenter(z45, an4, userRepository);
    }
}
