package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wv3 {
    /*
    static {
        Logger.getLogger(wv3.class.getName());
        Collections.synchronizedMap(new HashMap());
        Collections.synchronizedMap(new HashMap());
        tv3.a();
        dw3.a();
    }
    */

    @DexIgnore
    public static iw3 a(ObjectInputStream objectInputStream, int i) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[i];
        while (true) {
            int read = objectInputStream.read(bArr, 0, i);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.flush();
                return iw3.a(byteArrayOutputStream.toByteArray());
            }
        }
    }
}
