package com.fossil;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemjob.SystemJobService;
import com.fossil.am;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wm implements hm {
    @DexIgnore
    public static /* final */ String f; // = tl.a("SystemJobScheduler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ JobScheduler b;
    @DexIgnore
    public /* final */ lm c;
    @DexIgnore
    public /* final */ io d;
    @DexIgnore
    public /* final */ vm e;

    @DexIgnore
    public wm(Context context, lm lmVar) {
        this(context, lmVar, (JobScheduler) context.getSystemService("jobscheduler"), new vm(context));
    }

    @DexIgnore
    public static void b(Context context) {
        List<JobInfo> a2;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null && (a2 = a(context, jobScheduler)) != null && !a2.isEmpty()) {
            for (JobInfo next : a2) {
                if (a(next) == null) {
                    a(jobScheduler, next.getId());
                }
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public void a(zn... znVarArr) {
        int i;
        List<Integer> a2;
        int i2;
        WorkDatabase g = this.c.g();
        int length = znVarArr.length;
        int i3 = 0;
        while (i3 < length) {
            zn znVar = znVarArr[i3];
            g.beginTransaction();
            try {
                zn e2 = g.d().e(znVar.a);
                if (e2 == null) {
                    tl a3 = tl.a();
                    String str = f;
                    a3.e(str, "Skipping scheduling " + znVar.a + " because it's no longer in the DB", new Throwable[0]);
                    g.setTransactionSuccessful();
                } else if (e2.b != am.a.ENQUEUED) {
                    tl a4 = tl.a();
                    String str2 = f;
                    a4.e(str2, "Skipping scheduling " + znVar.a + " because it is no longer enqueued", new Throwable[0]);
                    g.setTransactionSuccessful();
                } else {
                    tn a5 = g.b().a(znVar.a);
                    if (a5 != null) {
                        i = a5.b;
                    } else {
                        i = this.d.a(this.c.c().f(), this.c.c().d());
                    }
                    if (a5 == null) {
                        this.c.g().b().a(new tn(znVar.a, i));
                    }
                    a(znVar, i);
                    if (Build.VERSION.SDK_INT == 23 && (a2 = a(this.a, this.b, znVar.a)) != null) {
                        int indexOf = a2.indexOf(Integer.valueOf(i));
                        if (indexOf >= 0) {
                            a2.remove(indexOf);
                        }
                        if (!a2.isEmpty()) {
                            i2 = a2.get(0).intValue();
                        } else {
                            i2 = this.d.a(this.c.c().f(), this.c.c().d());
                        }
                        a(znVar, i2);
                    }
                    g.setTransactionSuccessful();
                }
                g.endTransaction();
                i3++;
            } catch (Throwable th) {
                g.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    public wm(Context context, lm lmVar, JobScheduler jobScheduler, vm vmVar) {
        this.a = context;
        this.c = lmVar;
        this.b = jobScheduler;
        this.d = new io(context);
        this.e = vmVar;
    }

    @DexIgnore
    public void a(zn znVar, int i) {
        JobInfo a2 = this.e.a(znVar, i);
        tl.a().a(f, String.format("Scheduling work ID %s Job ID %s", new Object[]{znVar.a, Integer.valueOf(i)}), new Throwable[0]);
        try {
            this.b.schedule(a2);
        } catch (IllegalStateException e2) {
            List<JobInfo> a3 = a(this.a, this.b);
            String format = String.format(Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", new Object[]{Integer.valueOf(a3 != null ? a3.size() : 0), Integer.valueOf(this.c.g().d().a().size()), Integer.valueOf(this.c.c().e())});
            tl.a().b(f, format, new Throwable[0]);
            throw new IllegalStateException(format, e2);
        } catch (Throwable th) {
            tl.a().b(f, String.format("Unable to schedule %s", new Object[]{znVar}), th);
        }
    }

    @DexIgnore
    public void a(String str) {
        List<Integer> a2 = a(this.a, this.b, str);
        if (a2 != null && !a2.isEmpty()) {
            for (Integer intValue : a2) {
                a(this.b, intValue.intValue());
            }
            this.c.g().b().b(str);
        }
    }

    @DexIgnore
    public static void a(JobScheduler jobScheduler, int i) {
        try {
            jobScheduler.cancel(i);
        } catch (Throwable th) {
            tl.a().b(f, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", new Object[]{Integer.valueOf(i)}), th);
        }
    }

    @DexIgnore
    public static void a(Context context) {
        List<JobInfo> a2;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null && (a2 = a(context, jobScheduler)) != null && !a2.isEmpty()) {
            for (JobInfo id : a2) {
                a(jobScheduler, id.getId());
            }
        }
    }

    @DexIgnore
    public static List<JobInfo> a(Context context, JobScheduler jobScheduler) {
        List<JobInfo> list;
        try {
            list = jobScheduler.getAllPendingJobs();
        } catch (Throwable th) {
            tl.a().b(f, "getAllPendingJobs() is not reliable on this device.", th);
            list = null;
        }
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        ComponentName componentName = new ComponentName(context, SystemJobService.class);
        for (JobInfo next : list) {
            if (componentName.equals(next.getService())) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static List<Integer> a(Context context, JobScheduler jobScheduler, String str) {
        List<JobInfo> a2 = a(context, jobScheduler);
        if (a2 == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(2);
        for (JobInfo next : a2) {
            if (str.equals(a(next))) {
                arrayList.add(Integer.valueOf(next.getId()));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static String a(JobInfo jobInfo) {
        PersistableBundle extras = jobInfo.getExtras();
        if (extras == null) {
            return null;
        }
        try {
            if (extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                return extras.getString("EXTRA_WORK_SPEC_ID");
            }
            return null;
        } catch (NullPointerException unused) {
            return null;
        }
    }
}
