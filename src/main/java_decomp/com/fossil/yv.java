package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.fossil.fs;
import com.fossil.jv;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yv<DataT> implements jv<Uri, DataT> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ jv<File, DataT> b;
    @DexIgnore
    public /* final */ jv<Uri, DataT> c;
    @DexIgnore
    public /* final */ Class<DataT> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<DataT> implements kv<Uri, DataT> {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Class<DataT> b;

        @DexIgnore
        public a(Context context, Class<DataT> cls) {
            this.a = context;
            this.b = cls;
        }

        @DexIgnore
        public final jv<Uri, DataT> a(nv nvVar) {
            return new yv(this.a, nvVar.a(File.class, this.b), nvVar.a(Uri.class, this.b), this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends a<ParcelFileDescriptor> {
        @DexIgnore
        public b(Context context) {
            super(context, ParcelFileDescriptor.class);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends a<InputStream> {
        @DexIgnore
        public c(Context context) {
            super(context, InputStream.class);
        }
    }

    @DexIgnore
    public yv(Context context, jv<File, DataT> jvVar, jv<Uri, DataT> jvVar2, Class<DataT> cls) {
        this.a = context.getApplicationContext();
        this.b = jvVar;
        this.c = jvVar2;
        this.d = cls;
    }

    @DexIgnore
    public jv.a<DataT> a(Uri uri, int i, int i2, xr xrVar) {
        return new jv.a<>(new g00(uri), new d(this.a, this.b, this.c, uri, i, i2, xrVar, this.d));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return Build.VERSION.SDK_INT >= 29 && ss.b(uri);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<DataT> implements fs<DataT> {
        @DexIgnore
        public static /* final */ String[] o; // = {"_data"};
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ jv<File, DataT> b;
        @DexIgnore
        public /* final */ jv<Uri, DataT> c;
        @DexIgnore
        public /* final */ Uri d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ xr g;
        @DexIgnore
        public /* final */ Class<DataT> h;
        @DexIgnore
        public volatile boolean i;
        @DexIgnore
        public volatile fs<DataT> j;

        @DexIgnore
        public d(Context context, jv<File, DataT> jvVar, jv<Uri, DataT> jvVar2, Uri uri, int i2, int i3, xr xrVar, Class<DataT> cls) {
            this.a = context.getApplicationContext();
            this.b = jvVar;
            this.c = jvVar2;
            this.d = uri;
            this.e = i2;
            this.f = i3;
            this.g = xrVar;
            this.h = cls;
        }

        @DexIgnore
        public void a(br brVar, fs.a<? super DataT> aVar) {
            try {
                fs<DataT> d2 = d();
                if (d2 == null) {
                    aVar.a((Exception) new IllegalArgumentException("Failed to build fetcher for: " + this.d));
                    return;
                }
                this.j = d2;
                if (this.i) {
                    cancel();
                } else {
                    d2.a(brVar, aVar);
                }
            } catch (FileNotFoundException e2) {
                aVar.a((Exception) e2);
            }
        }

        @DexIgnore
        public pr b() {
            return pr.LOCAL;
        }

        @DexIgnore
        public final jv.a<DataT> c() throws FileNotFoundException {
            if (Environment.isExternalStorageLegacy()) {
                return this.b.a(a(this.d), this.e, this.f, this.g);
            }
            return this.c.a(e() ? MediaStore.setRequireOriginal(this.d) : this.d, this.e, this.f, this.g);
        }

        @DexIgnore
        public void cancel() {
            this.i = true;
            fs<DataT> fsVar = this.j;
            if (fsVar != null) {
                fsVar.cancel();
            }
        }

        @DexIgnore
        public final fs<DataT> d() throws FileNotFoundException {
            jv.a c2 = c();
            if (c2 != null) {
                return c2.c;
            }
            return null;
        }

        @DexIgnore
        public final boolean e() {
            return this.a.checkSelfPermission("android.permission.ACCESS_MEDIA_LOCATION") == 0;
        }

        @DexIgnore
        public Class<DataT> getDataClass() {
            return this.h;
        }

        @DexIgnore
        public void a() {
            fs<DataT> fsVar = this.j;
            if (fsVar != null) {
                fsVar.a();
            }
        }

        @DexIgnore
        public final File a(Uri uri) throws FileNotFoundException {
            Cursor cursor = null;
            try {
                cursor = this.a.getContentResolver().query(uri, o, (String) null, (String[]) null, (String) null);
                if (cursor == null || !cursor.moveToFirst()) {
                    throw new FileNotFoundException("Failed to media store entry for: " + uri);
                }
                String string = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
                if (!TextUtils.isEmpty(string)) {
                    return new File(string);
                }
                throw new FileNotFoundException("File path was empty in media store for: " + uri);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }
}
