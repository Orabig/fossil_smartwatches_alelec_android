package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xd4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public /* final */ DashBar C;
    @DexIgnore
    public /* final */ ConstraintLayout D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleButton H;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText r;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText s;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout t;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout u;
    @DexIgnore
    public /* final */ FloatingActionButton v;
    @DexIgnore
    public /* final */ RTLImageView w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xd4(Object obj, View view, int i, Barrier barrier, Barrier barrier2, FlexibleButton flexibleButton, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FloatingActionButton floatingActionButton, RTLImageView rTLImageView, RTLImageView rTLImageView2, ImageView imageView, ImageView imageView2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, DashBar dashBar, ConstraintLayout constraintLayout4, ScrollView scrollView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextInputEditText;
        this.s = flexibleTextInputEditText2;
        this.t = flexibleTextInputLayout;
        this.u = flexibleTextInputLayout2;
        this.v = floatingActionButton;
        this.w = rTLImageView;
        this.x = rTLImageView2;
        this.y = imageView;
        this.z = imageView2;
        this.A = constraintLayout;
        this.B = constraintLayout2;
        this.C = dashBar;
        this.D = constraintLayout4;
        this.E = flexibleTextView2;
        this.F = flexibleTextView3;
        this.G = flexibleTextView4;
        this.H = flexibleButton2;
    }
}
