package com.fossil;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up5 implements Factory<tp5> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<ls4> b;
    @DexIgnore
    public /* final */ Provider<is4> c;

    @DexIgnore
    public up5(Provider<PortfolioApp> provider, Provider<ls4> provider2, Provider<is4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static up5 a(Provider<PortfolioApp> provider, Provider<ls4> provider2, Provider<is4> provider3) {
        return new up5(provider, provider2, provider3);
    }

    @DexIgnore
    public static tp5 b(Provider<PortfolioApp> provider, Provider<ls4> provider2, Provider<is4> provider3) {
        return new tp5(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public tp5 get() {
        return b(this.a, this.b, this.c);
    }
}
