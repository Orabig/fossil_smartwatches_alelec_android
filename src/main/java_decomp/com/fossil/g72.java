package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g72 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<g72> CREATOR; // = new a82();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public g72(String str, String str2, String str3, int i) {
        this(str, str2, str3, i, 0);
    }

    @DexIgnore
    public final String B() {
        return this.b;
    }

    @DexIgnore
    public final String C() {
        return String.format("%s:%s:%s", new Object[]{this.a, this.b, this.c});
    }

    @DexIgnore
    public final int D() {
        return this.d;
    }

    @DexIgnore
    public final String E() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof g72)) {
            return false;
        }
        g72 g72 = (g72) obj;
        return u12.a(this.a, g72.a) && u12.a(this.b, g72.b) && u12.a(this.c, g72.c) && this.d == g72.d && this.e == g72.e;
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(this.a, this.b, this.c, Integer.valueOf(this.d));
    }

    @DexIgnore
    public final String p() {
        return this.a;
    }

    @DexIgnore
    public final String toString() {
        return String.format("Device{%s:%s:%s}", new Object[]{C(), Integer.valueOf(this.d), Integer.valueOf(this.e)});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, p(), false);
        g22.a(parcel, 2, B(), false);
        g22.a(parcel, 4, E(), false);
        g22.a(parcel, 5, D());
        g22.a(parcel, 6, this.e);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public g72(String str, String str2, String str3, int i, int i2) {
        w12.a(str);
        this.a = str;
        w12.a(str2);
        this.b = str2;
        if (str3 != null) {
            this.c = str3;
            this.d = i;
            this.e = i2;
            return;
        }
        throw new IllegalStateException("Device UID is null.");
    }
}
