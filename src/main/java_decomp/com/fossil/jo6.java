package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jo6 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a;
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b;
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater c;
    @DexIgnore
    public volatile Object _next; // = this;
    @DexIgnore
    public volatile Object _prev; // = this;
    @DexIgnore
    public volatile Object _removedRef; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends co6<jo6> {
        @DexIgnore
        public jo6 b;
        @DexIgnore
        public /* final */ jo6 c;

        @DexIgnore
        public a(jo6 jo6) {
            wg6.b(jo6, "newNode");
            this.c = jo6;
        }

        @DexIgnore
        public void a(jo6 jo6, Object obj) {
            wg6.b(jo6, "affected");
            boolean z = obj == null;
            jo6 jo62 = z ? this.c : this.b;
            if (jo62 != null && jo6.a.compareAndSet(jo6, this, jo62) && z) {
                jo6 jo63 = this.c;
                jo6 jo64 = this.b;
                if (jo64 != null) {
                    jo63.b(jo64);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    /*
    static {
        Class<Object> cls = Object.class;
        Class<jo6> cls2 = jo6.class;
        a = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_next");
        b = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_prev");
        c = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_removedRef");
    }
    */

    @DexIgnore
    public final jo6 b() {
        jo6 jo6 = this;
        while (!(jo6 instanceof ho6)) {
            jo6 = jo6.d();
            if (nl6.a()) {
                if (!(jo6 != this)) {
                    throw new AssertionError();
                }
            }
        }
        return jo6;
    }

    @DexIgnore
    public final void c(jo6 jo6) {
        g();
        jo6.a(io6.a(this._prev), (po6) null);
    }

    @DexIgnore
    public final jo6 d() {
        return io6.a(c());
    }

    @DexIgnore
    public final Object e() {
        while (true) {
            Object obj = this._prev;
            if (obj instanceof qo6) {
                return obj;
            }
            if (obj != null) {
                jo6 jo6 = (jo6) obj;
                if (jo6.c() == this) {
                    return obj;
                }
                a(jo6, (po6) null);
            } else {
                throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        }
    }

    @DexIgnore
    public final jo6 f() {
        return io6.a(e());
    }

    @DexIgnore
    public final void g() {
        Object c2;
        jo6 i = i();
        Object obj = this._next;
        if (obj != null) {
            jo6 jo6 = ((qo6) obj).a;
            while (true) {
                jo6 jo62 = null;
                while (true) {
                    Object c3 = jo6.c();
                    if (c3 instanceof qo6) {
                        jo6.i();
                        jo6 = ((qo6) c3).a;
                    } else {
                        c2 = i.c();
                        if (c2 instanceof qo6) {
                            if (jo62 != null) {
                                break;
                            }
                            i = io6.a(i._prev);
                        } else if (c2 != this) {
                            if (c2 != null) {
                                jo6 jo63 = (jo6) c2;
                                if (jo63 != jo6) {
                                    jo6 jo64 = jo63;
                                    jo62 = i;
                                    i = jo64;
                                } else {
                                    return;
                                }
                            } else {
                                throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                            }
                        } else if (a.compareAndSet(i, this, jo6)) {
                            return;
                        }
                    }
                }
                i.i();
                a.compareAndSet(jo62, i, ((qo6) c2).a);
                i = jo62;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Removed");
        }
    }

    @DexIgnore
    public final boolean h() {
        return c() instanceof qo6;
    }

    @DexIgnore
    public final jo6 i() {
        Object obj;
        jo6 jo6;
        do {
            obj = this._prev;
            if (obj instanceof qo6) {
                return ((qo6) obj).a;
            }
            if (obj == this) {
                jo6 = b();
            } else if (obj != null) {
                jo6 = (jo6) obj;
            } else {
                throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (!b.compareAndSet(this, obj, jo6.l()));
        return (jo6) obj;
    }

    @DexIgnore
    public boolean j() {
        Object c2;
        jo6 jo6;
        do {
            c2 = c();
            if ((c2 instanceof qo6) || c2 == this) {
                return false;
            }
            if (c2 != null) {
                jo6 = (jo6) c2;
            } else {
                throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (!a.compareAndSet(this, c2, jo6.l()));
        c(jo6);
        return true;
    }

    @DexIgnore
    public final jo6 k() {
        while (true) {
            Object c2 = c();
            if (c2 != null) {
                jo6 jo6 = (jo6) c2;
                if (jo6 == this) {
                    return null;
                }
                if (jo6.j()) {
                    return jo6;
                }
                jo6.g();
            } else {
                throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        }
    }

    @DexIgnore
    public final qo6 l() {
        qo6 qo6 = (qo6) this._removedRef;
        if (qo6 != null) {
            return qo6;
        }
        qo6 qo62 = new qo6(this);
        c.lazySet(this, qo62);
        return qo62;
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '@' + Integer.toHexString(System.identityHashCode(this));
    }

    @DexIgnore
    public final boolean a(jo6 jo6) {
        wg6.b(jo6, "node");
        b.lazySet(jo6, this);
        a.lazySet(jo6, this);
        while (c() == this) {
            if (a.compareAndSet(this, this, jo6)) {
                jo6.b(this);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final Object c() {
        while (true) {
            Object obj = this._next;
            if (!(obj instanceof po6)) {
                return obj;
            }
            ((po6) obj).a(this);
        }
    }

    @DexIgnore
    public final void b(jo6 jo6) {
        Object obj;
        do {
            obj = jo6._prev;
            if ((obj instanceof qo6) || c() != jo6) {
                return;
            }
        } while (!b.compareAndSet(jo6, obj, this));
        if (!(c() instanceof qo6)) {
            return;
        }
        if (obj != null) {
            jo6.a((jo6) obj, (po6) null);
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public final int a(jo6 jo6, jo6 jo62, a aVar) {
        wg6.b(jo6, "node");
        wg6.b(jo62, "next");
        wg6.b(aVar, "condAdd");
        b.lazySet(jo6, this);
        a.lazySet(jo6, jo62);
        aVar.b = jo62;
        if (!a.compareAndSet(this, jo62, aVar)) {
            return 0;
        }
        return aVar.a(this) == null ? 1 : 2;
    }

    @DexIgnore
    public final jo6 a(jo6 jo6, po6 po6) {
        Object obj;
        while (true) {
            jo6 jo62 = null;
            while (true) {
                obj = jo6._next;
                if (obj == po6) {
                    return jo6;
                }
                if (obj instanceof po6) {
                    ((po6) obj).a(jo6);
                } else if (!(obj instanceof qo6)) {
                    Object obj2 = this._prev;
                    if (obj2 instanceof qo6) {
                        return null;
                    }
                    if (obj != this) {
                        if (obj != null) {
                            jo62 = jo6;
                            jo6 = (jo6) obj;
                        } else {
                            throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                    } else if (obj2 == jo6) {
                        return null;
                    } else {
                        if (b.compareAndSet(this, obj2, jo6) && !(jo6._prev instanceof qo6)) {
                            return null;
                        }
                    }
                } else if (jo62 != null) {
                    break;
                } else {
                    jo6 = io6.a(jo6._prev);
                }
            }
            jo6.i();
            a.compareAndSet(jo62, jo6, ((qo6) obj).a);
            jo6 = jo62;
        }
    }
}
