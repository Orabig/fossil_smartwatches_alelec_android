package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter$start$1$startAndEnd$1", f = "ActiveTimeOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
public final class wb5$b$b extends sf6 implements ig6<il6, xe6<? super lc6<? extends Date, ? extends Date>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeOverviewWeekPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wb5$b$b(ActiveTimeOverviewWeekPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        wb5$b$b wb5_b_b = new wb5$b$b(this.this$0, xe6);
        wb5_b_b.p$ = (il6) obj;
        return wb5_b_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((wb5$b$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter = this.this$0.this$0;
            return activeTimeOverviewWeekPresenter.a(ActiveTimeOverviewWeekPresenter.e(activeTimeOverviewWeekPresenter));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
