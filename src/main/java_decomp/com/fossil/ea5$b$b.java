package com.fossil;

import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1$allSearchedMicroApps$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
public final class ea5$b$b extends sf6 implements ig6<il6, xe6<? super List<? extends MicroApp>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SearchMicroAppPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ea5$b$b(SearchMicroAppPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ea5$b$b ea5_b_b = new ea5$b$b(this.this$0, xe6);
        ea5_b_b.p$ = (il6) obj;
        return ea5_b_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ea5$b$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            MicroAppRepository e = this.this$0.this$0.l;
            List<String> r = this.this$0.this$0.m.r();
            wg6.a((Object) r, "sharedPreferencesManager.microAppSearchedIdsRecent");
            return e.getMicroAppByIds(r, this.this$0.this$0.n.e());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
