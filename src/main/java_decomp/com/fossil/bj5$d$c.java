package com.fossil;

import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$summary$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class bj5$d$c extends sf6 implements ig6<il6, xe6<? super MFSleepDay>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bj5$d$c(SleepDetailPresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        bj5$d$c bj5_d_c = new bj5$d$c(this.this$0, xe6);
        bj5_d_c.p$ = (il6) obj;
        return bj5_d_c;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((bj5$d$c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            SleepDetailPresenter sleepDetailPresenter = this.this$0.this$0;
            return sleepDetailPresenter.b(sleepDetailPresenter.f, (List<MFSleepDay>) this.this$0.this$0.l);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
