package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zs implements vr {
    @DexIgnore
    public /* final */ vr b;
    @DexIgnore
    public /* final */ vr c;

    @DexIgnore
    public zs(vr vrVar, vr vrVar2) {
        this.b = vrVar;
        this.c = vrVar2;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
        this.c.a(messageDigest);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof zs)) {
            return false;
        }
        zs zsVar = (zs) obj;
        if (!this.b.equals(zsVar.b) || !this.c.equals(zsVar.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "DataCacheKey{sourceKey=" + this.b + ", signature=" + this.c + '}';
    }
}
