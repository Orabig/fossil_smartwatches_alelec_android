package com.fossil;

import android.os.Parcelable;
import com.fossil.DianaCustomizeEditPresenter;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d45$b$a extends sf6 implements ig6<il6, xe6<? super Parcelable>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ DianaPresetComplicationSetting $complication;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeEditPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d45$b$a(DianaCustomizeEditPresenter.b bVar, DianaPresetComplicationSetting dianaPresetComplicationSetting, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
        this.$complication = dianaPresetComplicationSetting;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        d45$b$a d45_b_a = new d45$b$a(this.this$0, this.$complication, xe6);
        d45_b_a.p$ = (il6) obj;
        return d45_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((d45$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return DianaCustomizeEditPresenter.g(this.this$0.this$0).f(this.$complication.getId());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
