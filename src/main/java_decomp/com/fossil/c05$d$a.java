package com.fossil;

import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$start$1$listInactivityNudgeTimeModel$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
public final class c05$d$a extends sf6 implements ig6<il6, xe6<? super List<? extends InactivityNudgeTimeModel>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ InactivityNudgeTimePresenter.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c05$d$a(InactivityNudgeTimePresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        c05$d$a c05_d_a = new c05$d$a(this.this$0, xe6);
        c05_d_a.p$ = (il6) obj;
        return c05_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((c05$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.j.getInactivityNudgeTimeDao().getListInactivityNudgeTimeModel();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
