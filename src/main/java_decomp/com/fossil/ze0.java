package com.fossil;

import com.fossil.t40;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ze0 {
    @DexIgnore
    public static lc6 a(ii1 ii1, t40.b bVar) {
        return qc6.a(bVar, Boolean.valueOf(ii1.p()));
    }

    @DexIgnore
    public static String a(String str) {
        String uuid = UUID.randomUUID().toString();
        wg6.a(uuid, str);
        return uuid;
    }

    @DexIgnore
    public static String a(StringBuilder sb, int i, String str) {
        sb.append(i);
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public static String a(StringBuilder sb, int i, String str, String str2) {
        sb.append(i);
        sb.append(str);
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public static StringBuilder b(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        return sb;
    }
}
