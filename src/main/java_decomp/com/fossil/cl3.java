package com.fossil;

import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cl3 {
    @DexIgnore
    public static /* final */ ek3 a; // = ek3.c(", ").a("null");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ck3<Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Collection a;

        @DexIgnore
        public a(Collection collection) {
            this.a = collection;
        }

        @DexIgnore
        public Object apply(Object obj) {
            return obj == this.a ? "(this Collection)" : obj;
        }
    }

    @DexIgnore
    public static boolean a(Collection<?> collection, Object obj) {
        jk3.a(collection);
        try {
            return collection.contains(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return false;
        }
    }

    @DexIgnore
    public static boolean a(Collection<?> collection, Collection<?> collection2) {
        return pm3.a(collection2, lk3.a(collection));
    }

    @DexIgnore
    public static String a(Collection<?> collection) {
        StringBuilder a2 = a(collection.size());
        a2.append('[');
        a.a(a2, (Iterable<?>) pm3.a(collection, new a(collection)));
        a2.append(']');
        return a2.toString();
    }

    @DexIgnore
    public static StringBuilder a(int i) {
        bl3.a(i, "size");
        return new StringBuilder((int) Math.min(((long) i) * 8, 1073741824));
    }

    @DexIgnore
    public static <T> Collection<T> a(Iterable<T> iterable) {
        return (Collection) iterable;
    }
}
