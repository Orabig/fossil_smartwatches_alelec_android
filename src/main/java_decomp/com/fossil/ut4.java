package com.fossil;

import android.content.Context;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ut4 {
    @DexIgnore
    public static /* final */ a a; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(int i) {
            int i2 = (int) (((double) i) * 1.15d);
            if (i2 >= 0 && 9 >= i2) {
                if (i2 % 2 != 0) {
                    return ((i2 / 2) * 2) + 2;
                }
                return i2;
            } else if (10 <= i2 && 99 >= i2) {
                if (i2 % 10 != 0) {
                    return ((i2 / 10) * 10) + 10;
                }
                return i2;
            } else if (100 > i2 || 999 < i2) {
                return i2 % MFNetworkReturnCode.RESPONSE_OK != 0 ? ((i2 / MFNetworkReturnCode.RESPONSE_OK) * MFNetworkReturnCode.RESPONSE_OK) + MFNetworkReturnCode.RESPONSE_OK : i2;
            } else {
                if (i2 % 20 != 0) {
                    return ((i2 / 20) * 20) + 20;
                }
                return i2;
            }
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final ArrayList<String> a(Context context, BarChart.c cVar) {
            wg6.b(context, "context");
            wg6.b(cVar, "chartModel");
            ArrayList<String> arrayList = new ArrayList<>();
            Calendar instance = Calendar.getInstance();
            for (BarChart.a aVar : cVar.a()) {
                if (aVar.b() > 0) {
                    wg6.a((Object) instance, "calendar");
                    instance.setTimeInMillis(aVar.b());
                    Boolean t = bk4.t(instance.getTime());
                    wg6.a((Object) t, "DateHelper.isToday(calendar.time)");
                    if (!t.booleanValue()) {
                        switch (instance.get(7)) {
                            case 1:
                                arrayList.add(jm4.a(context, 2131886537));
                                break;
                            case 2:
                                arrayList.add(jm4.a(context, 2131886536));
                                break;
                            case 3:
                                arrayList.add(jm4.a(context, 2131886539));
                                break;
                            case 4:
                                arrayList.add(jm4.a(context, 2131886541));
                                break;
                            case 5:
                                arrayList.add(jm4.a(context, 2131886540));
                                break;
                            case 6:
                                arrayList.add(jm4.a(context, 2131886535));
                                break;
                            case 7:
                                arrayList.add(jm4.a(context, 2131886538));
                                break;
                        }
                    } else {
                        arrayList.add(jm4.a(context, 2131886435));
                    }
                } else {
                    arrayList.add("");
                }
            }
            return arrayList;
        }
    }
}
