package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vt1 {
    @DexIgnore
    public static /* final */ Lock c; // = new ReentrantLock();
    @DexIgnore
    public static vt1 d;
    @DexIgnore
    public /* final */ Lock a; // = new ReentrantLock();
    @DexIgnore
    public /* final */ SharedPreferences b;

    @DexIgnore
    public vt1(Context context) {
        this.b = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }

    @DexIgnore
    public static vt1 a(Context context) {
        w12.a(context);
        c.lock();
        try {
            if (d == null) {
                d = new vt1(context.getApplicationContext());
            }
            return d;
        } finally {
            c.unlock();
        }
    }

    @DexIgnore
    public GoogleSignInAccount b() {
        return a(c("defaultGoogleSignInAccount"));
    }

    @DexIgnore
    public GoogleSignInOptions c() {
        return b(c("defaultGoogleSignInAccount"));
    }

    @DexIgnore
    public String d() {
        return c("refreshToken");
    }

    @DexIgnore
    public final void e() {
        String c2 = c("defaultGoogleSignInAccount");
        d("defaultGoogleSignInAccount");
        if (!TextUtils.isEmpty(c2)) {
            d(b("googleSignInAccount", c2));
            d(b("googleSignInOptions", c2));
        }
    }

    @DexIgnore
    public final void d(String str) {
        this.a.lock();
        try {
            this.b.edit().remove(str).apply();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final GoogleSignInOptions b(String str) {
        String c2;
        if (!TextUtils.isEmpty(str) && (c2 = c(b("googleSignInOptions", str))) != null) {
            try {
                return GoogleSignInOptions.e(c2);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public final String c(String str) {
        this.a.lock();
        try {
            return this.b.getString(str, (String) null);
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public static String b(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length());
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public void a(GoogleSignInAccount googleSignInAccount, GoogleSignInOptions googleSignInOptions) {
        w12.a(googleSignInAccount);
        w12.a(googleSignInOptions);
        a("defaultGoogleSignInAccount", googleSignInAccount.L());
        w12.a(googleSignInAccount);
        w12.a(googleSignInOptions);
        String L = googleSignInAccount.L();
        a(b("googleSignInAccount", L), googleSignInAccount.M());
        a(b("googleSignInOptions", L), googleSignInOptions.J());
    }

    @DexIgnore
    public final void a(String str, String str2) {
        this.a.lock();
        try {
            this.b.edit().putString(str, str2).apply();
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final GoogleSignInAccount a(String str) {
        String c2;
        if (!TextUtils.isEmpty(str) && (c2 = c(b("googleSignInAccount", str))) != null) {
            try {
                return GoogleSignInAccount.e(c2);
            } catch (JSONException unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public void a() {
        this.a.lock();
        try {
            this.b.edit().clear().apply();
        } finally {
            this.a.unlock();
        }
    }
}
