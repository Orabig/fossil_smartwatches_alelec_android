package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dw2 extends e22 implements ew1 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<dw2> CREATOR; // = new ow2();
    @DexIgnore
    public /* final */ Status a;
    @DexIgnore
    public /* final */ ew2 b;

    @DexIgnore
    public dw2(Status status) {
        this(status, (ew2) null);
    }

    @DexIgnore
    public dw2(Status status, ew2 ew2) {
        this.a = status;
        this.b = ew2;
    }

    @DexIgnore
    public final Status o() {
        return this.a;
    }

    @DexIgnore
    public final ew2 p() {
        return this.b;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, (Parcelable) o(), i, false);
        g22.a(parcel, 2, (Parcelable) p(), i, false);
        g22.a(parcel, a2);
    }
}
