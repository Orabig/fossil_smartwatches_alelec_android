package com.fossil;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import com.fossil.hc;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public final class ub implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ub> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ ArrayList<String> b;
    @DexIgnore
    public /* final */ int[] c;
    @DexIgnore
    public /* final */ int[] d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ CharSequence i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ CharSequence o;
    @DexIgnore
    public /* final */ ArrayList<String> p;
    @DexIgnore
    public /* final */ ArrayList<String> q;
    @DexIgnore
    public /* final */ boolean r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<ub> {
        @DexIgnore
        public ub createFromParcel(Parcel parcel) {
            return new ub(parcel);
        }

        @DexIgnore
        public ub[] newArray(int i) {
            return new ub[i];
        }
    }

    @DexIgnore
    public ub(tb tbVar) {
        int size = tbVar.a.size();
        this.a = new int[(size * 5)];
        if (tbVar.g) {
            this.b = new ArrayList<>(size);
            this.c = new int[size];
            this.d = new int[size];
            int i2 = 0;
            int i3 = 0;
            while (i2 < size) {
                hc.a aVar = tbVar.a.get(i2);
                int i4 = i3 + 1;
                this.a[i3] = aVar.a;
                ArrayList<String> arrayList = this.b;
                Fragment fragment = aVar.b;
                arrayList.add(fragment != null ? fragment.mWho : null);
                int[] iArr = this.a;
                int i5 = i4 + 1;
                iArr[i4] = aVar.c;
                int i6 = i5 + 1;
                iArr[i5] = aVar.d;
                int i7 = i6 + 1;
                iArr[i6] = aVar.e;
                iArr[i7] = aVar.f;
                this.c[i2] = aVar.g.ordinal();
                this.d[i2] = aVar.h.ordinal();
                i2++;
                i3 = i7 + 1;
            }
            this.e = tbVar.f;
            this.f = tbVar.i;
            this.g = tbVar.t;
            this.h = tbVar.j;
            this.i = tbVar.k;
            this.j = tbVar.l;
            this.o = tbVar.m;
            this.p = tbVar.n;
            this.q = tbVar.o;
            this.r = tbVar.p;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    @DexIgnore
    public tb a(FragmentManager fragmentManager) {
        tb tbVar = new tb(fragmentManager);
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.a.length) {
            hc.a aVar = new hc.a();
            int i4 = i2 + 1;
            aVar.a = this.a[i2];
            if (FragmentManager.d(2)) {
                Log.v("FragmentManager", "Instantiate " + tbVar + " op #" + i3 + " base fragment #" + this.a[i4]);
            }
            String str = this.b.get(i3);
            if (str != null) {
                aVar.b = fragmentManager.a(str);
            } else {
                aVar.b = null;
            }
            aVar.g = Lifecycle.State.values()[this.c[i3]];
            aVar.h = Lifecycle.State.values()[this.d[i3]];
            int[] iArr = this.a;
            int i5 = i4 + 1;
            aVar.c = iArr[i4];
            int i6 = i5 + 1;
            aVar.d = iArr[i5];
            int i7 = i6 + 1;
            aVar.e = iArr[i6];
            aVar.f = iArr[i7];
            tbVar.b = aVar.c;
            tbVar.c = aVar.d;
            tbVar.d = aVar.e;
            tbVar.e = aVar.f;
            tbVar.a(aVar);
            i3++;
            i2 = i7 + 1;
        }
        tbVar.f = this.e;
        tbVar.i = this.f;
        tbVar.t = this.g;
        tbVar.g = true;
        tbVar.j = this.h;
        tbVar.k = this.i;
        tbVar.l = this.j;
        tbVar.m = this.o;
        tbVar.n = this.p;
        tbVar.o = this.q;
        tbVar.p = this.r;
        tbVar.a(1);
        return tbVar;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.a);
        parcel.writeStringList(this.b);
        parcel.writeIntArray(this.c);
        parcel.writeIntArray(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
        parcel.writeInt(this.j);
        TextUtils.writeToParcel(this.o, parcel, 0);
        parcel.writeStringList(this.p);
        parcel.writeStringList(this.q);
        parcel.writeInt(this.r ? 1 : 0);
    }

    @DexIgnore
    public ub(Parcel parcel) {
        this.a = parcel.createIntArray();
        this.b = parcel.createStringArrayList();
        this.c = parcel.createIntArray();
        this.d = parcel.createIntArray();
        this.e = parcel.readInt();
        this.f = parcel.readString();
        this.g = parcel.readInt();
        this.h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.j = parcel.readInt();
        this.o = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.p = parcel.createStringArrayList();
        this.q = parcel.createStringArrayList();
        this.r = parcel.readInt() != 0;
    }
}
