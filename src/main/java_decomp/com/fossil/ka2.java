package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.jt1;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka2 extends i12<la2> {
    @DexIgnore
    public /* final */ jt1.a E;

    @DexIgnore
    public ka2(Context context, Looper looper, e12 e12, jt1.a aVar, wv1.b bVar, wv1.c cVar) {
        super(context, looper, 68, e12, bVar, cVar);
        this.E = aVar;
    }

    @DexIgnore
    public final String A() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
        if (queryLocalInterface instanceof la2) {
            return (la2) queryLocalInterface;
        }
        return new ma2(iBinder);
    }

    @DexIgnore
    public final int j() {
        return 12800000;
    }

    @DexIgnore
    public final Bundle v() {
        jt1.a aVar = this.E;
        return aVar == null ? new Bundle() : aVar.a();
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }
}
