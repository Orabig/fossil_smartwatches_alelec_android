package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fe4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerViewCalendar q;

    @DexIgnore
    public fe4(Object obj, View view, int i, RecyclerViewCalendar recyclerViewCalendar) {
        super(obj, view, i);
        this.q = recyclerViewCalendar;
    }
}
