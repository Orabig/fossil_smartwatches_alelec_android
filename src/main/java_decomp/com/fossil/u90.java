package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u90 extends z90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<u90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new u90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new u90[i];
        }
    }

    @DexIgnore
    public u90(byte b, vd0 vd0) {
        super(e90.COMMUTE_TIME_ETA_MICRO_APP, b, vd0);
    }

    @DexIgnore
    public /* synthetic */ u90(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
