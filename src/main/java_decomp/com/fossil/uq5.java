package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uq5 implements Factory<tq5> {
    @DexIgnore
    public static OnboardingHeightWeightPresenter a(qq5 qq5, UserRepository userRepository, GetRecommendedGoalUseCase getRecommendedGoalUseCase) {
        return new OnboardingHeightWeightPresenter(qq5, userRepository, getRecommendedGoalUseCase);
    }
}
