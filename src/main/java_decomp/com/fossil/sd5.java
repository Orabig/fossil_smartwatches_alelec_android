package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sd5 implements Factory<rd5> {
    @DexIgnore
    public static CaloriesOverviewMonthPresenter a(qd5 qd5, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new CaloriesOverviewMonthPresenter(qd5, userRepository, summariesRepository, portfolioApp);
    }
}
