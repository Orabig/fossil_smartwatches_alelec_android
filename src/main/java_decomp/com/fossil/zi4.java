package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ActiveMinutesComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.BatteryComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.CaloriesComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ChanceOfRainComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.DateComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.HeartRateComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.NoneComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.SecondTimezoneComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.StepsComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.WeatherComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLEGoalTrackingCustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLENonCustomization;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.CommuteTimeWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.DiagnosticsWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.MusicWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.NoneWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.NotificationPanelWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.StopWatchWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.TimerWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WeatherWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WellnessWatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WorkoutWatchAppMapping;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Gesture;
import com.portfolio.platform.data.legacy.threedotzero.PusherConfiguration;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zi4 {
    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    public static final ComplicationAppMappingSettings a(List<DianaPresetComplicationSetting> list, Gson gson) {
        ComplicationAppMapping complicationAppMapping;
        SecondTimezoneSetting secondTimezoneSetting;
        Gson gson2 = gson;
        wg6.b(list, "$this$toComplicationSetting");
        wg6.b(gson2, "mGson");
        FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Convert DianaPresetSetting to complication mapping");
        ComplicationAppMapping complicationAppMapping2 = null;
        ComplicationAppMapping complicationAppMapping3 = null;
        ComplicationAppMapping complicationAppMapping4 = null;
        ComplicationAppMapping complicationAppMapping5 = null;
        for (DianaPresetComplicationSetting next : list) {
            String component1 = next.component1();
            String component2 = next.component2();
            String component4 = next.component4();
            switch (component2.hashCode()) {
                case -331239923:
                    if (component2.equals(Constants.BATTERY)) {
                        complicationAppMapping = new BatteryComplicationAppMapping();
                        break;
                    }
                case -168965370:
                    if (component2.equals(Constants.CALORIES)) {
                        complicationAppMapping = new CaloriesComplicationAppMapping();
                        break;
                    }
                case -85386984:
                    if (component2.equals("active-minutes")) {
                        complicationAppMapping = new ActiveMinutesComplicationAppMapping();
                        break;
                    }
                case -48173007:
                    if (component2.equals("chance-of-rain")) {
                        complicationAppMapping = new ChanceOfRainComplicationAppMapping();
                        break;
                    }
                case 3076014:
                    if (component2.equals(HardwareLog.COLUMN_DATE)) {
                        complicationAppMapping = new DateComplicationAppMapping();
                        break;
                    }
                case 96634189:
                    if (component2.equals("empty")) {
                        complicationAppMapping = new NoneComplicationAppMapping();
                        break;
                    }
                case 109761319:
                    if (component2.equals("steps")) {
                        complicationAppMapping = new StepsComplicationAppMapping();
                        break;
                    }
                case 134170930:
                    if (component2.equals("second-timezone")) {
                        if (component4 != null) {
                            secondTimezoneSetting = (SecondTimezoneSetting) gson2.a(component4, SecondTimezoneSetting.class);
                            if (secondTimezoneSetting == null) {
                                secondTimezoneSetting = new SecondTimezoneSetting((String) null, (String) null, 0, (String) null, 15, (qg6) null);
                            }
                        } else {
                            secondTimezoneSetting = new SecondTimezoneSetting((String) null, (String) null, 0, (String) null, 15, (qg6) null);
                        }
                        secondTimezoneSetting.setTimezoneOffset(ConversionUtils.INSTANCE.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId()));
                        complicationAppMapping = new SecondTimezoneComplicationAppMapping(secondTimezoneSetting.getCityCode(), secondTimezoneSetting.getTimezoneOffset());
                        break;
                    }
                case 1223440372:
                    if (component2.equals("weather")) {
                        complicationAppMapping = new WeatherComplicationAppMapping();
                        break;
                    }
                case 1884273159:
                    if (component2.equals("heart-rate")) {
                        complicationAppMapping = new HeartRateComplicationAppMapping();
                        break;
                    }
                default:
                    complicationAppMapping = new NoneComplicationAppMapping();
                    break;
            }
            switch (component1.hashCode()) {
                case -1383228885:
                    if (!component1.equals("bottom")) {
                        break;
                    } else {
                        complicationAppMapping5 = complicationAppMapping;
                        break;
                    }
                case 115029:
                    if (!component1.equals("top")) {
                        break;
                    } else {
                        complicationAppMapping2 = complicationAppMapping;
                        break;
                    }
                case 3317767:
                    if (!component1.equals("left")) {
                        break;
                    } else {
                        complicationAppMapping4 = complicationAppMapping;
                        break;
                    }
                case 108511772:
                    if (!component1.equals("right")) {
                        break;
                    } else {
                        complicationAppMapping3 = complicationAppMapping;
                        break;
                    }
            }
        }
        if (complicationAppMapping2 == null) {
            complicationAppMapping2 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Top complication empty");
        }
        ComplicationAppMapping complicationAppMapping6 = complicationAppMapping2;
        if (complicationAppMapping3 == null) {
            complicationAppMapping3 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Right complication empty");
        }
        ComplicationAppMapping complicationAppMapping7 = complicationAppMapping3;
        if (complicationAppMapping4 == null) {
            complicationAppMapping4 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Left complication empty");
        }
        ComplicationAppMapping complicationAppMapping8 = complicationAppMapping4;
        if (complicationAppMapping5 == null) {
            complicationAppMapping5 = new NoneComplicationAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Bottom complication empty");
        }
        return new ComplicationAppMappingSettings(complicationAppMapping6, complicationAppMapping5, complicationAppMapping8, complicationAppMapping7, System.currentTimeMillis());
    }

    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    public static final WatchAppMappingSettings b(List<DianaPresetWatchAppSetting> list, Gson gson) {
        WatchAppMapping watchAppMapping;
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        wg6.b(list, "$this$toWatchAppMappingSettings");
        wg6.b(gson, "mGson");
        FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "Convert DianaPresetSetting to watch app mapping");
        WatchAppMapping watchAppMapping2 = null;
        WatchAppMapping watchAppMapping3 = null;
        WatchAppMapping watchAppMapping4 = null;
        for (DianaPresetWatchAppSetting next : list) {
            String component1 = next.component1();
            String component2 = next.component2();
            String component4 = next.component4();
            switch (component2.hashCode()) {
                case -829740640:
                    if (component2.equals("commute-time")) {
                        if (component4 != null) {
                            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) gson.a(component4, CommuteTimeWatchAppSetting.class);
                            if (commuteTimeWatchAppSetting == null) {
                                commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting((List) null, 1, (qg6) null);
                            }
                        } else {
                            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting((List) null, 1, (qg6) null);
                        }
                        watchAppMapping = new CommuteTimeWatchAppMapping((List<String>) commuteTimeWatchAppSetting.getListAddressNameExceptOf((AddressWrapper) null));
                        break;
                    }
                case -740386388:
                    if (component2.equals("diagnostics")) {
                        watchAppMapping = new DiagnosticsWatchAppMapping();
                        break;
                    }
                case -420342747:
                    if (component2.equals("wellness")) {
                        watchAppMapping = new WellnessWatchAppMapping();
                        break;
                    }
                case 96634189:
                    if (component2.equals("empty")) {
                        watchAppMapping = new NoneWatchAppMapping();
                        break;
                    }
                case 104263205:
                    if (component2.equals(Constants.MUSIC)) {
                        watchAppMapping = new MusicWatchAppMapping();
                        break;
                    }
                case 110364485:
                    if (component2.equals("timer")) {
                        watchAppMapping = new TimerWatchAppMapping();
                        break;
                    }
                case 1223440372:
                    if (component2.equals("weather")) {
                        watchAppMapping = new WeatherWatchAppMapping();
                        break;
                    }
                case 1374620322:
                    if (component2.equals("notification-panel")) {
                        watchAppMapping = new NotificationPanelWatchAppMapping();
                        break;
                    }
                case 1525170845:
                    if (component2.equals("workout")) {
                        watchAppMapping = new WorkoutWatchAppMapping();
                        break;
                    }
                case 1860261700:
                    if (component2.equals("stop-watch")) {
                        watchAppMapping = new StopWatchWatchAppMapping();
                        break;
                    }
                default:
                    watchAppMapping = new NoneWatchAppMapping();
                    break;
            }
            int hashCode = component1.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && component1.equals("top")) {
                        watchAppMapping2 = watchAppMapping;
                    }
                } else if (component1.equals("middle")) {
                    watchAppMapping3 = watchAppMapping;
                }
            } else if (component1.equals("bottom")) {
                watchAppMapping4 = watchAppMapping;
            }
        }
        if (watchAppMapping2 == null) {
            watchAppMapping2 = new NoneWatchAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "top watch app empty");
        }
        WatchAppMapping watchAppMapping5 = watchAppMapping2;
        if (watchAppMapping3 == null) {
            watchAppMapping3 = new NoneWatchAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "middle watch app empty");
        }
        WatchAppMapping watchAppMapping6 = watchAppMapping3;
        if (watchAppMapping4 == null) {
            watchAppMapping4 = new NoneWatchAppMapping();
            FLogger.INSTANCE.getLocal().d("CustomizeConfigurationExt", "bottom watch app empty");
        }
        return new WatchAppMappingSettings(watchAppMapping5, watchAppMapping6, watchAppMapping4, System.currentTimeMillis());
    }

    @DexIgnore
    public static final ArrayList<DianaPresetWatchAppSetting> c(List<DianaPresetWatchAppSetting> list) {
        wg6.b(list, "$this$deepCopyPresetWatchAppSetting");
        ArrayList<DianaPresetWatchAppSetting> arrayList = new ArrayList<>();
        for (DianaPresetWatchAppSetting next : list) {
            arrayList.add(new DianaPresetWatchAppSetting(next.component1(), next.component2(), next.component3(), next.component4()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final boolean c(List<DianaPresetWatchAppSetting> list, Gson gson, List<DianaPresetWatchAppSetting> list2) {
        T t;
        wg6.b(list, "$this$isWatchAppSettingListEquals");
        wg6.b(gson, "gson");
        wg6.b(list2, "other");
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z = true;
        for (DianaPresetWatchAppSetting next : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (wg6.a((Object) next.getPosition(), (Object) ((DianaPresetWatchAppSetting) t).getPosition())) {
                    break;
                }
            }
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
            if (dianaPresetWatchAppSetting != null && !a(dianaPresetWatchAppSetting, gson, next)) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public static final ArrayList<DianaPresetComplicationSetting> b(List<DianaPresetComplicationSetting> list) {
        wg6.b(list, "$this$deepCopyPresetComplicationSetting");
        ArrayList<DianaPresetComplicationSetting> arrayList = new ArrayList<>();
        for (DianaPresetComplicationSetting next : list) {
            arrayList.add(new DianaPresetComplicationSetting(next.component1(), next.component2(), next.component3(), next.component4()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final boolean b(List<HybridPresetAppSetting> list, Gson gson, List<HybridPresetAppSetting> list2) {
        T t;
        wg6.b(list, "$this$isHybridAppSettingListEquals");
        wg6.b(gson, "gson");
        wg6.b(list2, "other");
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z = true;
        for (HybridPresetAppSetting next : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (wg6.a((Object) next.getPosition(), (Object) ((HybridPresetAppSetting) t).getPosition())) {
                    break;
                }
            }
            HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
            if (hybridPresetAppSetting != null && !a(next, gson, hybridPresetAppSetting)) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public static final List<MicroAppMapping> a(HybridPreset hybridPreset, String str, DeviceRepository deviceRepository, MicroAppRepository microAppRepository) {
        wg6.b(hybridPreset, "$this$toMicroAppMappingBLE");
        wg6.b(str, "serialNumber");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(microAppRepository, "mMicroAppRepository");
        Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
        if (deviceBySerial == null) {
            return new ArrayList();
        }
        if (deviceBySerial.getMajor() == 255 || deviceBySerial.getMinor() == 255) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("toMicroAppMappingBLE", "buttons " + hybridPreset.getButtons());
        long currentTimeMillis = System.currentTimeMillis();
        Iterator<HybridPresetAppSetting> it = hybridPreset.getButtons().iterator();
        while (it.hasNext()) {
            HybridPresetAppSetting next = it.next();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("toMicroAppMappingBLE", "setting for " + next + " is " + next.getSettings());
            wg6.a((Object) next, "buttonMapping");
            MicroAppVariant a = a(next, str, deviceBySerial.getMajor(), microAppRepository, next.getSettings());
            if (a == null) {
                return new ArrayList();
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("toMicroAppMappingBLE", "variant for " + next + " is " + a);
            MicroAppMapping a2 = a(next, currentTimeMillis, a, next.getSettings());
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("toMicroAppMappingBLE", "bleMicroAppMapping for " + next + " is " + a2);
            arrayList.add(a2);
        }
        return arrayList;
    }

    @DexIgnore
    public static final MicroAppVariant a(HybridPresetAppSetting hybridPresetAppSetting, String str, int i, MicroAppRepository microAppRepository, String str2) {
        String str3;
        CommuteTimeSetting commuteTimeSetting;
        wg6.b(hybridPresetAppSetting, "$this$getVariant");
        wg6.b(str, "serialNumber");
        wg6.b(microAppRepository, "mMicroAppRepository");
        MicroAppInstruction.MicroAppID microAppId = MicroAppInstruction.MicroAppID.Companion.getMicroAppId(hybridPresetAppSetting.getAppId());
        if (microAppId != MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME || str2 == null || TextUtils.isEmpty(str2) || (commuteTimeSetting = (CommuteTimeSetting) new Gson().a(str2, CommuteTimeSetting.class)) == null) {
            str3 = "";
        } else {
            str3 = commuteTimeSetting.getFormat();
            if (TextUtils.isEmpty(str3)) {
                str3 = "travel";
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("getVariant", "variantName " + str3 + " microAppId " + hybridPresetAppSetting.getAppId() + ' ' + str + " major " + i + " bleAppEnum " + microAppId + " setting " + str2);
        if (str3.length() == 0) {
            return microAppRepository.getMicroAppVariant(str, hybridPresetAppSetting.getAppId(), i);
        }
        String appId = hybridPresetAppSetting.getAppId();
        if (str3 != null) {
            String lowerCase = str3.toLowerCase();
            wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            return microAppRepository.getMicroAppVariant(str, appId, lowerCase, i);
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final MicroAppMapping a(HybridPresetAppSetting hybridPresetAppSetting, long j, MicroAppVariant microAppVariant, String str) {
        wg6.b(hybridPresetAppSetting, "$this$toBleMicroAppMapping");
        Gesture a = a(hybridPresetAppSetting);
        MicroAppInstruction.MicroAppID microAppId = MicroAppInstruction.MicroAppID.Companion.getMicroAppId(hybridPresetAppSetting.getAppId());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("bleGesture ");
        sb.append(a);
        sb.append(" bleAppEnum ");
        sb.append(microAppId);
        sb.append(' ');
        sb.append("setting ");
        sb.append(str);
        sb.append(" variant ");
        sb.append(microAppVariant);
        sb.append(" declarationsFile ");
        sb.append(microAppVariant != null ? microAppVariant.getDeclarationFileList() : null);
        local.d("toBleMicroAppMapping", sb.toString());
        if (microAppVariant == null || !(!microAppVariant.getDeclarationFileList().isEmpty())) {
            return null;
        }
        String[] strArr = new String[microAppVariant.getDeclarationFileList().size()];
        int size = microAppVariant.getDeclarationFileList().size();
        for (int i = 0; i < size; i++) {
            strArr[i] = microAppVariant.getDeclarationFileList().get(i).getContent();
        }
        BLECustomization bLENonCustomization = new BLENonCustomization();
        if (microAppId == MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID) {
            bLENonCustomization = new BLEGoalTrackingCustomization(0);
        }
        return new MicroAppMapping(a, microAppId.getValue(), strArr, bLENonCustomization, Long.valueOf(j));
    }

    @DexIgnore
    public static final Gesture a(HybridPresetAppSetting hybridPresetAppSetting) {
        wg6.b(hybridPresetAppSetting, "$this$getBleGesture");
        String position = hybridPresetAppSetting.getPosition();
        if (wg6.a((Object) position, (Object) PusherConfiguration.Pusher.TOP_PUSHER.getValue())) {
            return Gesture.SAM_BT1_SINGLE_PRESS;
        }
        if (wg6.a((Object) position, (Object) PusherConfiguration.Pusher.MID_PUSHER.getValue())) {
            return Gesture.SAM_BT2_SINGLE_PRESS;
        }
        return Gesture.SAM_BT3_SINGLE_PRESS;
    }

    @DexIgnore
    public static final ArrayList<HybridPresetAppSetting> a(List<HybridPresetAppSetting> list) {
        wg6.b(list, "$this$deepCopyAppSetting");
        ArrayList<HybridPresetAppSetting> arrayList = new ArrayList<>();
        for (HybridPresetAppSetting hybridPresetAppSetting : list) {
            arrayList.add(new HybridPresetAppSetting(hybridPresetAppSetting.getPosition(), hybridPresetAppSetting.getAppId(), hybridPresetAppSetting.getLocalUpdateAt(), hybridPresetAppSetting.getSettings()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final boolean a(DianaPresetWatchAppSetting dianaPresetWatchAppSetting, Gson gson, DianaPresetWatchAppSetting dianaPresetWatchAppSetting2) {
        wg6.b(dianaPresetWatchAppSetting, "$this$isWatchAppSettingEquals");
        wg6.b(gson, "gson");
        wg6.b(dianaPresetWatchAppSetting2, "other");
        if ((!wg6.a((Object) dianaPresetWatchAppSetting.getPosition(), (Object) dianaPresetWatchAppSetting2.getPosition())) || (!wg6.a((Object) dianaPresetWatchAppSetting.getId(), (Object) dianaPresetWatchAppSetting2.getId()))) {
            return false;
        }
        if (TextUtils.isEmpty(dianaPresetWatchAppSetting.getSettings()) && TextUtils.isEmpty(dianaPresetWatchAppSetting2.getSettings())) {
            return true;
        }
        String id = dianaPresetWatchAppSetting.getId();
        int hashCode = id.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 1223440372 && id.equals("weather")) {
                return vi4.e(dianaPresetWatchAppSetting.getSettings(), gson, dianaPresetWatchAppSetting2.getSettings());
            }
            return true;
        } else if (id.equals("commute-time")) {
            return vi4.b(dianaPresetWatchAppSetting.getSettings(), gson, dianaPresetWatchAppSetting2.getSettings());
        } else {
            return true;
        }
    }

    @DexIgnore
    public static final boolean a(List<DianaPresetComplicationSetting> list, Gson gson, List<DianaPresetComplicationSetting> list2) {
        T t;
        wg6.b(list, "$this$isComplicationListSettingEquals");
        wg6.b(gson, "gson");
        wg6.b(list2, "other");
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z = true;
        for (DianaPresetComplicationSetting next : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (wg6.a((Object) next.getPosition(), (Object) ((DianaPresetComplicationSetting) t).getPosition())) {
                    break;
                }
            }
            DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
            if (dianaPresetComplicationSetting != null && !a(next, gson, dianaPresetComplicationSetting)) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public static final boolean a(DianaPresetComplicationSetting dianaPresetComplicationSetting, Gson gson, DianaPresetComplicationSetting dianaPresetComplicationSetting2) {
        wg6.b(dianaPresetComplicationSetting, "$this$isComplicationSettingEquals");
        wg6.b(gson, "gson");
        wg6.b(dianaPresetComplicationSetting2, "other");
        if ((!wg6.a((Object) dianaPresetComplicationSetting.getPosition(), (Object) dianaPresetComplicationSetting2.getPosition())) || (!wg6.a((Object) dianaPresetComplicationSetting.getId(), (Object) dianaPresetComplicationSetting2.getId()))) {
            return false;
        }
        if (TextUtils.isEmpty(dianaPresetComplicationSetting.getSettings()) && TextUtils.isEmpty(dianaPresetComplicationSetting2.getSettings())) {
            return true;
        }
        String id = dianaPresetComplicationSetting.getId();
        if (id.hashCode() == 134170930 && id.equals("second-timezone")) {
            return vi4.d(dianaPresetComplicationSetting.getSettings(), gson, dianaPresetComplicationSetting2.getSettings());
        }
        return true;
    }

    @DexIgnore
    public static final boolean a(HybridPresetAppSetting hybridPresetAppSetting, Gson gson, HybridPresetAppSetting hybridPresetAppSetting2) {
        wg6.b(hybridPresetAppSetting, "$this$isHybridAppSettingEquals");
        wg6.b(gson, "gson");
        wg6.b(hybridPresetAppSetting2, "other");
        if ((!wg6.a((Object) hybridPresetAppSetting.getPosition(), (Object) hybridPresetAppSetting2.getPosition())) || (!wg6.a((Object) hybridPresetAppSetting.getAppId(), (Object) hybridPresetAppSetting2.getAppId()))) {
            return false;
        }
        if (TextUtils.isEmpty(hybridPresetAppSetting.getSettings()) && TextUtils.isEmpty(hybridPresetAppSetting2.getSettings())) {
            return true;
        }
        String appId = hybridPresetAppSetting.getAppId();
        if (wg6.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            return vi4.a(hybridPresetAppSetting.getSettings(), gson, hybridPresetAppSetting2.getSettings());
        }
        if (wg6.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
            return vi4.d(hybridPresetAppSetting.getSettings(), gson, hybridPresetAppSetting2.getSettings());
        }
        if (wg6.a((Object) appId, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
            return vi4.c(hybridPresetAppSetting.getSettings(), gson, hybridPresetAppSetting2.getSettings());
        }
        return true;
    }
}
