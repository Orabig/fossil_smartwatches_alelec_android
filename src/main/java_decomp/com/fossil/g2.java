package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.b9;
import com.fossil.x1;
import com.fossil.y1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g2 extends l1 implements b9.a {
    @DexIgnore
    public /* final */ SparseBooleanArray A; // = new SparseBooleanArray();
    @DexIgnore
    public e B;
    @DexIgnore
    public a C;
    @DexIgnore
    public c D;
    @DexIgnore
    public b E;
    @DexIgnore
    public /* final */ f F; // = new f();
    @DexIgnore
    public int G;
    @DexIgnore
    public d j;
    @DexIgnore
    public Drawable o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends w1 {
        @DexIgnore
        public a(Context context, c2 c2Var, View view) {
            super(context, c2Var, view, false, a0.actionOverflowMenuStyle);
            if (!((t1) c2Var.getItem()).h()) {
                View view2 = g2.this.j;
                a(view2 == null ? (View) g2.this.h : view2);
            }
            a((x1.a) g2.this.F);
        }

        @DexIgnore
        public void e() {
            g2 g2Var = g2.this;
            g2Var.C = null;
            g2Var.G = 0;
            super.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ActionMenuItemView.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public a2 a() {
            a aVar = g2.this.C;
            if (aVar != null) {
                return aVar.c();
            }
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public e a;

        @DexIgnore
        public c(e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public void run() {
            if (g2.this.c != null) {
                g2.this.c.a();
            }
            View view = (View) g2.this.h;
            if (!(view == null || view.getWindowToken() == null || !this.a.g())) {
                g2.this.B = this.a;
            }
            g2.this.D = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AppCompatImageView implements ActionMenuView.a {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends v2 {
            @DexIgnore
            public a(View view, g2 g2Var) {
                super(view);
            }

            @DexIgnore
            public a2 b() {
                e eVar = g2.this.B;
                if (eVar == null) {
                    return null;
                }
                return eVar.c();
            }

            @DexIgnore
            public boolean c() {
                g2.this.j();
                return true;
            }

            @DexIgnore
            public boolean d() {
                g2 g2Var = g2.this;
                if (g2Var.D != null) {
                    return false;
                }
                g2Var.f();
                return true;
            }
        }

        @DexIgnore
        public d(Context context) {
            super(context, (AttributeSet) null, a0.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            k3.a(this, getContentDescription());
            setOnTouchListener(new a(this, g2.this));
        }

        @DexIgnore
        public boolean b() {
            return false;
        }

        @DexIgnore
        public boolean c() {
            return false;
        }

        @DexIgnore
        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            g2.this.j();
            return true;
        }

        @DexIgnore
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                o7.a(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends w1 {
        @DexIgnore
        public e(Context context, q1 q1Var, View view, boolean z) {
            super(context, q1Var, view, z, a0.actionOverflowMenuStyle);
            a(8388613);
            a((x1.a) g2.this.F);
        }

        @DexIgnore
        public void e() {
            if (g2.this.c != null) {
                g2.this.c.close();
            }
            g2.this.B = null;
            super.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"BanParcelableUsage"})
    public static class g implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<g> CREATOR; // = new a();
        @DexIgnore
        public int a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Parcelable.Creator<g> {
            @DexIgnore
            public g createFromParcel(Parcel parcel) {
                return new g(parcel);
            }

            @DexIgnore
            public g[] newArray(int i) {
                return new g[i];
            }
        }

        @DexIgnore
        public g() {
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
        }

        @DexIgnore
        public g(Parcel parcel) {
            this.a = parcel.readInt();
        }
    }

    @DexIgnore
    public g2(Context context) {
        super(context, g0.abc_action_menu_layout, g0.abc_action_menu_item_layout);
    }

    @DexIgnore
    public boolean g() {
        a aVar = this.C;
        if (aVar == null) {
            return false;
        }
        aVar.b();
        return true;
    }

    @DexIgnore
    public boolean h() {
        return this.D != null || i();
    }

    @DexIgnore
    public boolean i() {
        e eVar = this.B;
        return eVar != null && eVar.d();
    }

    @DexIgnore
    public boolean j() {
        q1 q1Var;
        if (!this.q || i() || (q1Var = this.c) == null || this.h == null || this.D != null || q1Var.j().isEmpty()) {
            return false;
        }
        this.D = new c(new e(this.b, this.c, this.j, true));
        ((View) this.h).post(this.D);
        super.a((c2) null);
        return true;
    }

    @DexIgnore
    public void a(Context context, q1 q1Var) {
        super.a(context, q1Var);
        Resources resources = context.getResources();
        c1 a2 = c1.a(context);
        if (!this.r) {
            this.q = a2.g();
        }
        if (!this.x) {
            this.s = a2.b();
        }
        if (!this.v) {
            this.u = a2.c();
        }
        int i = this.s;
        if (this.q) {
            if (this.j == null) {
                this.j = new d(this.a);
                if (this.p) {
                    this.j.setImageDrawable(this.o);
                    this.o = null;
                    this.p = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.j.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.j.getMeasuredWidth();
        } else {
            this.j = null;
        }
        this.t = i;
        this.z = (int) (resources.getDisplayMetrics().density * 56.0f);
    }

    @DexIgnore
    public y1 b(ViewGroup viewGroup) {
        y1 y1Var = this.h;
        y1 b2 = super.b(viewGroup);
        if (y1Var != b2) {
            ((ActionMenuView) b2).setPresenter(this);
        }
        return b2;
    }

    @DexIgnore
    public void c(boolean z2) {
        this.y = z2;
    }

    @DexIgnore
    public void d(boolean z2) {
        this.q = z2;
        this.r = true;
    }

    @DexIgnore
    public Drawable e() {
        d dVar = this.j;
        if (dVar != null) {
            return dVar.getDrawable();
        }
        if (this.p) {
            return this.o;
        }
        return null;
    }

    @DexIgnore
    public boolean f() {
        y1 y1Var;
        c cVar = this.D;
        if (cVar == null || (y1Var = this.h) == null) {
            e eVar = this.B;
            if (eVar == null) {
                return false;
            }
            eVar.b();
            return true;
        }
        ((View) y1Var).removeCallbacks(cVar);
        this.D = null;
        return true;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements x1.a {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public boolean a(q1 q1Var) {
            if (q1Var == null) {
                return false;
            }
            g2.this.G = ((c2) q1Var).getItem().getItemId();
            x1.a c = g2.this.c();
            if (c != null) {
                return c.a(q1Var);
            }
            return false;
        }

        @DexIgnore
        public void a(q1 q1Var, boolean z) {
            if (q1Var instanceof c2) {
                q1Var.m().a(false);
            }
            x1.a c = g2.this.c();
            if (c != null) {
                c.a(q1Var, z);
            }
        }
    }

    @DexIgnore
    public boolean d() {
        return f() | g();
    }

    @DexIgnore
    public Parcelable b() {
        g gVar = new g();
        gVar.a = this.G;
        return gVar;
    }

    @DexIgnore
    public void b(boolean z2) {
        if (z2) {
            super.a((c2) null);
            return;
        }
        q1 q1Var = this.c;
        if (q1Var != null) {
            q1Var.a(false);
        }
    }

    @DexIgnore
    public void a(Configuration configuration) {
        if (!this.v) {
            this.u = c1.a(this.b).c();
        }
        q1 q1Var = this.c;
        if (q1Var != null) {
            q1Var.c(true);
        }
    }

    @DexIgnore
    public void a(Drawable drawable) {
        d dVar = this.j;
        if (dVar != null) {
            dVar.setImageDrawable(drawable);
            return;
        }
        this.p = true;
        this.o = drawable;
    }

    @DexIgnore
    public View a(t1 t1Var, View view, ViewGroup viewGroup) {
        View actionView = t1Var.getActionView();
        if (actionView == null || t1Var.f()) {
            actionView = super.a(t1Var, view, viewGroup);
        }
        actionView.setVisibility(t1Var.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    @DexIgnore
    public void a(t1 t1Var, y1.a aVar) {
        aVar.a(t1Var, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.h);
        if (this.E == null) {
            this.E = new b();
        }
        actionMenuItemView.setPopupCallback(this.E);
    }

    @DexIgnore
    public boolean a(int i, t1 t1Var) {
        return t1Var.h();
    }

    @DexIgnore
    public void a(boolean z2) {
        y1 y1Var;
        super.a(z2);
        ((View) this.h).requestLayout();
        q1 q1Var = this.c;
        boolean z3 = false;
        if (q1Var != null) {
            ArrayList<t1> c2 = q1Var.c();
            int size = c2.size();
            for (int i = 0; i < size; i++) {
                b9 a2 = c2.get(i).a();
                if (a2 != null) {
                    a2.setSubUiVisibilityListener(this);
                }
            }
        }
        q1 q1Var2 = this.c;
        ArrayList<t1> j2 = q1Var2 != null ? q1Var2.j() : null;
        if (this.q && j2 != null) {
            int size2 = j2.size();
            if (size2 == 1) {
                z3 = !j2.get(0).isActionViewExpanded();
            } else if (size2 > 0) {
                z3 = true;
            }
        }
        if (z3) {
            if (this.j == null) {
                this.j = new d(this.a);
            }
            ViewGroup viewGroup = (ViewGroup) this.j.getParent();
            if (viewGroup != this.h) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.j);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.h;
                actionMenuView.addView(this.j, actionMenuView.c());
            }
        } else {
            d dVar = this.j;
            if (dVar != null && dVar.getParent() == (y1Var = this.h)) {
                ((ViewGroup) y1Var).removeView(this.j);
            }
        }
        ((ActionMenuView) this.h).setOverflowReserved(this.q);
    }

    @DexIgnore
    public boolean a(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.j) {
            return false;
        }
        return super.a(viewGroup, i);
    }

    @DexIgnore
    public boolean a(c2 c2Var) {
        boolean z2 = false;
        if (!c2Var.hasVisibleItems()) {
            return false;
        }
        c2 c2Var2 = c2Var;
        while (c2Var2.t() != this.c) {
            c2Var2 = (c2) c2Var2.t();
        }
        View a2 = a(c2Var2.getItem());
        if (a2 == null) {
            return false;
        }
        this.G = c2Var.getItem().getItemId();
        int size = c2Var.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            MenuItem item = c2Var.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i++;
        }
        this.C = new a(this.b, c2Var, a2);
        this.C.a(z2);
        this.C.f();
        super.a(c2Var);
        return true;
    }

    @DexIgnore
    public final View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.h;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof y1.a) && ((y1.a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    @DexIgnore
    public boolean a() {
        int i;
        ArrayList<t1> arrayList;
        int i2;
        int i3;
        int i4;
        g2 g2Var = this;
        q1 q1Var = g2Var.c;
        View view = null;
        int i5 = 0;
        if (q1Var != null) {
            arrayList = q1Var.n();
            i = arrayList.size();
        } else {
            arrayList = null;
            i = 0;
        }
        int i6 = g2Var.u;
        int i7 = g2Var.t;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) g2Var.h;
        int i8 = i6;
        boolean z2 = false;
        int i9 = 0;
        int i10 = 0;
        for (int i11 = 0; i11 < i; i11++) {
            t1 t1Var = arrayList.get(i11);
            if (t1Var.k()) {
                i9++;
            } else if (t1Var.j()) {
                i10++;
            } else {
                z2 = true;
            }
            if (g2Var.y && t1Var.isActionViewExpanded()) {
                i8 = 0;
            }
        }
        if (g2Var.q && (z2 || i10 + i9 > i8)) {
            i8--;
        }
        int i12 = i8 - i9;
        SparseBooleanArray sparseBooleanArray = g2Var.A;
        sparseBooleanArray.clear();
        if (g2Var.w) {
            int i13 = g2Var.z;
            i2 = i7 / i13;
            i3 = i13 + ((i7 % i13) / i2);
        } else {
            i3 = 0;
            i2 = 0;
        }
        int i14 = i7;
        int i15 = 0;
        int i16 = 0;
        while (i15 < i) {
            t1 t1Var2 = arrayList.get(i15);
            if (t1Var2.k()) {
                View a2 = g2Var.a(t1Var2, view, viewGroup);
                if (g2Var.w) {
                    i2 -= ActionMenuView.a(a2, i3, i2, makeMeasureSpec, i5);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = a2.getMeasuredWidth();
                i14 -= measuredWidth;
                if (i16 != 0) {
                    measuredWidth = i16;
                }
                int groupId = t1Var2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                t1Var2.d(true);
                i16 = measuredWidth;
                i4 = i;
            } else if (t1Var2.j()) {
                int groupId2 = t1Var2.getGroupId();
                boolean z3 = sparseBooleanArray.get(groupId2);
                boolean z4 = (i12 > 0 || z3) && i14 > 0 && (!g2Var.w || i2 > 0);
                boolean z5 = z4;
                i4 = i;
                if (z4) {
                    View a3 = g2Var.a(t1Var2, (View) null, viewGroup);
                    if (g2Var.w) {
                        int a4 = ActionMenuView.a(a3, i3, i2, makeMeasureSpec, 0);
                        i2 -= a4;
                        z5 = a4 == 0 ? false : z5;
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth2 = a3.getMeasuredWidth();
                    i14 -= measuredWidth2;
                    if (i16 == 0) {
                        i16 = measuredWidth2;
                    }
                    z4 = z5 & (!g2Var.w ? i14 + i16 > 0 : i14 >= 0);
                }
                if (z4 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z3) {
                    sparseBooleanArray.put(groupId2, false);
                    int i17 = 0;
                    while (i17 < i15) {
                        t1 t1Var3 = arrayList.get(i17);
                        if (t1Var3.getGroupId() == groupId2) {
                            if (t1Var3.h()) {
                                i12++;
                            }
                            t1Var3.d(false);
                        }
                        i17++;
                    }
                }
                if (z4) {
                    i12--;
                }
                t1Var2.d(z4);
            } else {
                i4 = i;
                t1Var2.d(false);
                i15++;
                i = i4;
                view = null;
                i5 = 0;
                g2Var = this;
            }
            i15++;
            i = i4;
            view = null;
            i5 = 0;
            g2Var = this;
        }
        return true;
    }

    @DexIgnore
    public void a(q1 q1Var, boolean z2) {
        d();
        super.a(q1Var, z2);
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        int i;
        MenuItem findItem;
        if ((parcelable instanceof g) && (i = ((g) parcelable).a) > 0 && (findItem = this.c.findItem(i)) != null) {
            a((c2) findItem.getSubMenu());
        }
    }

    @DexIgnore
    public void a(ActionMenuView actionMenuView) {
        this.h = actionMenuView;
        actionMenuView.a(this.c);
    }
}
