package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ ra3 c;
    @DexIgnore
    public /* final */ /* synthetic */ ev2 d;
    @DexIgnore
    public /* final */ /* synthetic */ l83 e;

    @DexIgnore
    public x83(l83 l83, String str, String str2, ra3 ra3, ev2 ev2) {
        this.e = l83;
        this.a = str;
        this.b = str2;
        this.c = ra3;
        this.d = ev2;
    }

    @DexIgnore
    public final void run() {
        ArrayList<Bundle> arrayList = new ArrayList<>();
        try {
            l43 d2 = this.e.d;
            if (d2 == null) {
                this.e.b().t().a("Failed to get conditional properties", this.a, this.b);
                return;
            }
            arrayList = ma3.b(d2.a(this.a, this.b, this.c));
            this.e.I();
            this.e.j().a(this.d, arrayList);
        } catch (RemoteException e2) {
            this.e.b().t().a("Failed to get conditional properties", this.a, this.b, e2);
        } finally {
            this.e.j().a(this.d, arrayList);
        }
    }
}
