package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z16 {
    @DexIgnore
    public Context a; // = null;

    @DexIgnore
    public z16(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void a(w16 w16) {
        if (w16 != null) {
            String w162 = w16.toString();
            if (a()) {
                a(b26.d(w162));
            }
        }
    }

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public final w16 c() {
        String c = a() ? b26.c(b()) : null;
        if (c != null) {
            return w16.a(c);
        }
        return null;
    }
}
