package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zx implements cy<Bitmap, BitmapDrawable> {
    @DexIgnore
    public /* final */ Resources a;

    @DexIgnore
    public zx(Resources resources) {
        q00.a(resources);
        this.a = resources;
    }

    @DexIgnore
    public rt<BitmapDrawable> a(rt<Bitmap> rtVar, xr xrVar) {
        return xw.a(this.a, rtVar);
    }
}
