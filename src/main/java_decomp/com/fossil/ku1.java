package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ku1 extends ha2 implements ju1 {
    @DexIgnore
    public ku1() {
        super("com.google.android.gms.auth.api.signin.internal.IRevocationService");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            j();
        } else if (i != 2) {
            return false;
        } else {
            k();
        }
        return true;
    }
}
