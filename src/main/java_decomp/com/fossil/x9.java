package com.fossil;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.KeyEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.a9;
import com.fossil.ia;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x9 {
    @DexIgnore
    public static /* final */ AtomicInteger a; // = new AtomicInteger(1);
    @DexIgnore
    public static Field b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static Field d;
    @DexIgnore
    public static boolean e;
    @DexIgnore
    public static WeakHashMap<View, String> f;
    @DexIgnore
    public static WeakHashMap<View, ba> g; // = null;
    @DexIgnore
    public static Field h;
    @DexIgnore
    public static boolean i; // = false;
    @DexIgnore
    public static ThreadLocal<Rect> j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements View.OnApplyWindowInsetsListener {
        @DexIgnore
        public /* final */ /* synthetic */ s9 a;

        @DexIgnore
        public a(s9 s9Var) {
            this.a = s9Var;
        }

        @DexIgnore
        public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
            return (WindowInsets) fa.a(this.a.a(view, fa.a((Object) windowInsets)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Class<T> b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public f(int i, Class<T> cls, int i2) {
            this(i, cls, 0, i2);
        }

        @DexIgnore
        public abstract T a(View view);

        @DexIgnore
        public abstract void a(View view, T t);

        @DexIgnore
        public final boolean a() {
            return Build.VERSION.SDK_INT >= 19;
        }

        @DexIgnore
        public abstract boolean a(T t, T t2);

        @DexIgnore
        public void b(View view, T t) {
            if (b()) {
                a(view, t);
            } else if (a() && a(b(view), (Object) t)) {
                x9.r(view);
                view.setTag(this.a, t);
                x9.c(view, 0);
            }
        }

        @DexIgnore
        public f(int i, Class<T> cls, int i2, int i3) {
            this.a = i;
            this.b = cls;
            this.c = i3;
        }

        @DexIgnore
        public boolean a(Boolean bool, Boolean bool2) {
            boolean z;
            boolean booleanValue = bool == null ? false : bool.booleanValue();
            if (bool2 == null) {
                z = false;
            } else {
                z = bool2.booleanValue();
            }
            if (booleanValue == z) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public T b(View view) {
            if (b()) {
                return a(view);
            }
            if (!a()) {
                return null;
            }
            T tag = view.getTag(this.a);
            if (this.b.isInstance(tag)) {
                return tag;
            }
            return null;
        }

        @DexIgnore
        public final boolean b() {
            return Build.VERSION.SDK_INT >= this.c;
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        boolean a(View view, KeyEvent keyEvent);
    }

    /*
    static {
        int[] iArr = {f6.accessibility_custom_action_0, f6.accessibility_custom_action_1, f6.accessibility_custom_action_2, f6.accessibility_custom_action_3, f6.accessibility_custom_action_4, f6.accessibility_custom_action_5, f6.accessibility_custom_action_6, f6.accessibility_custom_action_7, f6.accessibility_custom_action_8, f6.accessibility_custom_action_9, f6.accessibility_custom_action_10, f6.accessibility_custom_action_11, f6.accessibility_custom_action_12, f6.accessibility_custom_action_13, f6.accessibility_custom_action_14, f6.accessibility_custom_action_15, f6.accessibility_custom_action_16, f6.accessibility_custom_action_17, f6.accessibility_custom_action_18, f6.accessibility_custom_action_19, f6.accessibility_custom_action_20, f6.accessibility_custom_action_21, f6.accessibility_custom_action_22, f6.accessibility_custom_action_23, f6.accessibility_custom_action_24, f6.accessibility_custom_action_25, f6.accessibility_custom_action_26, f6.accessibility_custom_action_27, f6.accessibility_custom_action_28, f6.accessibility_custom_action_29, f6.accessibility_custom_action_30, f6.accessibility_custom_action_31};
        new e();
    }
    */

    @DexIgnore
    public static boolean A(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.hasOverlappingRendering();
        }
        return true;
    }

    @DexIgnore
    public static boolean B(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.hasTransientState();
        }
        return false;
    }

    @DexIgnore
    public static boolean C(View view) {
        Boolean b2 = a().b(view);
        if (b2 == null) {
            return false;
        }
        return b2.booleanValue();
    }

    @DexIgnore
    public static boolean D(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.isAttachedToWindow();
        }
        return view.getWindowToken() != null;
    }

    @DexIgnore
    public static boolean E(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.isLaidOut();
        }
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    @DexIgnore
    public static boolean F(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.isNestedScrollingEnabled();
        }
        if (view instanceof m9) {
            return ((m9) view).isNestedScrollingEnabled();
        }
        return false;
    }

    @DexIgnore
    public static boolean G(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.isPaddingRelative();
        }
        return false;
    }

    @DexIgnore
    public static boolean H(View view) {
        Boolean b2 = e().b(view);
        if (b2 == null) {
            return false;
        }
        return b2.booleanValue();
    }

    @DexIgnore
    public static void I(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation();
        } else {
            view.postInvalidate();
        }
    }

    @DexIgnore
    public static void J(View view) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 20) {
            view.requestApplyInsets();
        } else if (i2 >= 16) {
            view.requestFitSystemWindows();
        }
    }

    @DexIgnore
    public static void K(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.stopNestedScroll();
        } else if (view instanceof m9) {
            ((m9) view).stopNestedScroll();
        }
    }

    @DexIgnore
    public static void L(View view) {
        float translationY = view.getTranslationY();
        view.setTranslationY(1.0f + translationY);
        view.setTranslationY(translationY);
    }

    @DexIgnore
    public static void a(View view, ia iaVar) {
        view.onInitializeAccessibilityNodeInfo(iaVar.A());
    }

    @DexIgnore
    public static a9 b(View view) {
        View.AccessibilityDelegate c2 = c(view);
        if (c2 == null) {
            return null;
        }
        if (c2 instanceof a9.a) {
            return ((a9.a) c2).a;
        }
        return new a9(c2);
    }

    @DexIgnore
    public static Rect c() {
        if (j == null) {
            j = new ThreadLocal<>();
        }
        Rect rect = j.get();
        if (rect == null) {
            rect = new Rect();
            j.set(rect);
        }
        rect.setEmpty();
        return rect;
    }

    @DexIgnore
    public static int d(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.getAccessibilityLiveRegion();
        }
        return 0;
    }

    @DexIgnore
    public static void e(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 23) {
            view.offsetTopAndBottom(i2);
        } else if (i3 >= 21) {
            Rect c2 = c();
            boolean z = false;
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                c2.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !c2.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            b(view, i2);
            if (z && c2.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(c2);
            }
        } else {
            b(view, i2);
        }
    }

    @DexIgnore
    public static void f(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 21) {
            a(i2, view);
            c(view, 0);
        }
    }

    @DexIgnore
    public static void g(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 19) {
            view.setAccessibilityLiveRegion(i2);
        }
    }

    @DexIgnore
    public static void h(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 19) {
            view.setImportantForAccessibility(i2);
        } else if (i3 >= 16) {
            if (i2 == 4) {
                i2 = 2;
            }
            view.setImportantForAccessibility(i2);
        }
    }

    @DexIgnore
    public static void i(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 26) {
            view.setImportantForAutofill(i2);
        }
    }

    @DexIgnore
    public static Display j(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getDisplay();
        }
        if (D(view)) {
            return ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }

    @DexIgnore
    public static float k(View view) {
        return Build.VERSION.SDK_INT >= 21 ? view.getElevation() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public static boolean l(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getFitsSystemWindows();
        }
        return false;
    }

    @DexIgnore
    public static int m(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getImportantForAccessibility();
        }
        return 0;
    }

    @DexIgnore
    @SuppressLint({"InlinedApi"})
    public static int n(View view) {
        if (Build.VERSION.SDK_INT >= 26) {
            return view.getImportantForAutofill();
        }
        return 0;
    }

    @DexIgnore
    public static int o(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getLayoutDirection();
        }
        return 0;
    }

    @DexIgnore
    public static int p(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumHeight();
        }
        if (!e) {
            try {
                d = View.class.getDeclaredField("mMinHeight");
                d.setAccessible(true);
            } catch (NoSuchFieldException unused) {
            }
            e = true;
        }
        Field field = d;
        if (field == null) {
            return 0;
        }
        try {
            return ((Integer) field.get(view)).intValue();
        } catch (Exception unused2) {
            return 0;
        }
    }

    @DexIgnore
    public static int q(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumWidth();
        }
        if (!c) {
            try {
                b = View.class.getDeclaredField("mMinWidth");
                b.setAccessible(true);
            } catch (NoSuchFieldException unused) {
            }
            c = true;
        }
        Field field = b;
        if (field == null) {
            return 0;
        }
        try {
            return ((Integer) field.get(view)).intValue();
        } catch (Exception unused2) {
            return 0;
        }
    }

    @DexIgnore
    public static a9 r(View view) {
        a9 b2 = b(view);
        if (b2 == null) {
            b2 = new a9();
        }
        a(view, b2);
        return b2;
    }

    @DexIgnore
    public static int s(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getPaddingEnd();
        }
        return view.getPaddingRight();
    }

    @DexIgnore
    public static int t(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getPaddingStart();
        }
        return view.getPaddingLeft();
    }

    @DexIgnore
    public static ViewParent u(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getParentForAccessibility();
        }
        return view.getParent();
    }

    @DexIgnore
    public static String v(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getTransitionName();
        }
        WeakHashMap<View, String> weakHashMap = f;
        if (weakHashMap == null) {
            return null;
        }
        return weakHashMap.get(view);
    }

    @DexIgnore
    public static float w(View view) {
        return Build.VERSION.SDK_INT >= 21 ? view.getTranslationZ() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public static int x(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getWindowSystemUiVisibility();
        }
        return 0;
    }

    @DexIgnore
    public static float y(View view) {
        return Build.VERSION.SDK_INT >= 21 ? view.getZ() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public static boolean z(View view) {
        if (Build.VERSION.SDK_INT >= 15) {
            return view.hasOnClickListeners();
        }
        return false;
    }

    @DexIgnore
    public static void a(View view, a9 a9Var) {
        View.AccessibilityDelegate accessibilityDelegate;
        if (a9Var == null && (c(view) instanceof a9.a)) {
            a9Var = new a9();
        }
        if (a9Var == null) {
            accessibilityDelegate = null;
        } else {
            accessibilityDelegate = a9Var.a();
        }
        view.setAccessibilityDelegate(accessibilityDelegate);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends f<Boolean> {
        @DexIgnore
        public b(int i, Class cls, int i2) {
            super(i, cls, i2);
        }

        @DexIgnore
        /* renamed from: b */
        public boolean a(Boolean bool, Boolean bool2) {
            return !a(bool, bool2);
        }

        @DexIgnore
        public Boolean a(View view) {
            return Boolean.valueOf(view.isScreenReaderFocusable());
        }

        @DexIgnore
        public void a(View view, Boolean bool) {
            view.setScreenReaderFocusable(bool.booleanValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends f<CharSequence> {
        @DexIgnore
        public c(int i, Class cls, int i2, int i3) {
            super(i, cls, i2, i3);
        }

        @DexIgnore
        public CharSequence a(View view) {
            return view.getAccessibilityPaneTitle();
        }

        @DexIgnore
        public void a(View view, CharSequence charSequence) {
            view.setAccessibilityPaneTitle(charSequence);
        }

        @DexIgnore
        public boolean a(CharSequence charSequence, CharSequence charSequence2) {
            return !TextUtils.equals(charSequence, charSequence2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends f<Boolean> {
        @DexIgnore
        public d(int i, Class cls, int i2) {
            super(i, cls, i2);
        }

        @DexIgnore
        /* renamed from: b */
        public boolean a(Boolean bool, Boolean bool2) {
            return !a(bool, bool2);
        }

        @DexIgnore
        public Boolean a(View view) {
            return Boolean.valueOf(view.isAccessibilityHeading());
        }

        @DexIgnore
        public void a(View view, Boolean bool) {
            view.setAccessibilityHeading(bool.booleanValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements ViewTreeObserver.OnGlobalLayoutListener, View.OnAttachStateChangeListener {
        @DexIgnore
        public WeakHashMap<View, Boolean> a; // = new WeakHashMap<>();

        @DexIgnore
        public final void a(View view, boolean z) {
            boolean z2 = view.getVisibility() == 0;
            if (z != z2) {
                if (z2) {
                    x9.c(view, 16);
                }
                this.a.put(view, Boolean.valueOf(z2));
            }
        }

        @DexIgnore
        public void onGlobalLayout() {
            for (Map.Entry next : this.a.entrySet()) {
                a((View) next.getKey(), ((Boolean) next.getValue()).booleanValue());
            }
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            a(view);
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }

        @DexIgnore
        public final void a(View view) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h {
        @DexIgnore
        public static /* final */ ArrayList<WeakReference<View>> d; // = new ArrayList<>();
        @DexIgnore
        public WeakHashMap<View, Boolean> a; // = null;
        @DexIgnore
        public SparseArray<WeakReference<View>> b; // = null;
        @DexIgnore
        public WeakReference<KeyEvent> c; // = null;

        @DexIgnore
        public final SparseArray<WeakReference<View>> a() {
            if (this.b == null) {
                this.b = new SparseArray<>();
            }
            return this.b;
        }

        @DexIgnore
        public final View b(View view, KeyEvent keyEvent) {
            WeakHashMap<View, Boolean> weakHashMap = this.a;
            if (weakHashMap != null && weakHashMap.containsKey(view)) {
                if (view instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view;
                    for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                        View b2 = b(viewGroup.getChildAt(childCount), keyEvent);
                        if (b2 != null) {
                            return b2;
                        }
                    }
                }
                if (c(view, keyEvent)) {
                    return view;
                }
            }
            return null;
        }

        @DexIgnore
        public final boolean c(View view, KeyEvent keyEvent) {
            ArrayList arrayList = (ArrayList) view.getTag(f6.tag_unhandled_key_listeners);
            if (arrayList == null) {
                return false;
            }
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                if (((g) arrayList.get(size)).a(view, keyEvent)) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public static h a(View view) {
            h hVar = (h) view.getTag(f6.tag_unhandled_key_event_manager);
            if (hVar != null) {
                return hVar;
            }
            h hVar2 = new h();
            view.setTag(f6.tag_unhandled_key_event_manager, hVar2);
            return hVar2;
        }

        @DexIgnore
        public boolean a(View view, KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                b();
            }
            View b2 = b(view, keyEvent);
            if (keyEvent.getAction() == 0) {
                int keyCode = keyEvent.getKeyCode();
                if (b2 != null && !KeyEvent.isModifierKey(keyCode)) {
                    a().put(keyCode, new WeakReference(b2));
                }
            }
            return b2 != null;
        }

        @DexIgnore
        public final void b() {
            WeakHashMap<View, Boolean> weakHashMap = this.a;
            if (weakHashMap != null) {
                weakHashMap.clear();
            }
            if (!d.isEmpty()) {
                synchronized (d) {
                    if (this.a == null) {
                        this.a = new WeakHashMap<>();
                    }
                    for (int size = d.size() - 1; size >= 0; size--) {
                        View view = (View) d.get(size).get();
                        if (view == null) {
                            d.remove(size);
                        } else {
                            this.a.put(view, Boolean.TRUE);
                            for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
                                this.a.put((View) parent, Boolean.TRUE);
                            }
                        }
                    }
                }
            }
        }

        @DexIgnore
        public boolean a(KeyEvent keyEvent) {
            int indexOfKey;
            WeakReference<KeyEvent> weakReference = this.c;
            if (weakReference != null && weakReference.get() == keyEvent) {
                return false;
            }
            this.c = new WeakReference<>(keyEvent);
            WeakReference weakReference2 = null;
            SparseArray<WeakReference<View>> a2 = a();
            if (keyEvent.getAction() == 1 && (indexOfKey = a2.indexOfKey(keyEvent.getKeyCode())) >= 0) {
                weakReference2 = a2.valueAt(indexOfKey);
                a2.removeAt(indexOfKey);
            }
            if (weakReference2 == null) {
                weakReference2 = a2.get(keyEvent.getKeyCode());
            }
            if (weakReference2 == null) {
                return false;
            }
            View view = (View) weakReference2.get();
            if (view != null && x9.D(view)) {
                c(view, keyEvent);
            }
            return true;
        }
    }

    @DexIgnore
    public static void d(View view, boolean z) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setNestedScrollingEnabled(z);
        } else if (view instanceof m9) {
            ((m9) view).setNestedScrollingEnabled(z);
        }
    }

    @DexIgnore
    public static ColorStateList g(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintList();
        }
        if (view instanceof w9) {
            return ((w9) view).getSupportBackgroundTintList();
        }
        return null;
    }

    @DexIgnore
    public static Rect i(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return view.getClipBounds();
        }
        return null;
    }

    @DexIgnore
    public static List<ia.a> f(View view) {
        ArrayList arrayList = (ArrayList) view.getTag(f6.tag_accessibility_actions);
        if (arrayList != null) {
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList();
        view.setTag(f6.tag_accessibility_actions, arrayList2);
        return arrayList2;
    }

    @DexIgnore
    public static PorterDuff.Mode h(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintMode();
        }
        if (view instanceof w9) {
            return ((w9) view).getSupportBackgroundTintMode();
        }
        return null;
    }

    @DexIgnore
    public static void a(View view, int i2, int i3, int i4, int i5) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation(i2, i3, i4, i5);
        } else {
            view.postInvalidate(i2, i3, i4, i5);
        }
    }

    @DexIgnore
    public static void b(View view, int i2, int i3, int i4, int i5) {
        if (Build.VERSION.SDK_INT >= 17) {
            view.setPaddingRelative(i2, i3, i4, i5);
        } else {
            view.setPadding(i2, i3, i4, i5);
        }
    }

    @DexIgnore
    public static View.AccessibilityDelegate c(View view) {
        if (i) {
            return null;
        }
        if (h == null) {
            try {
                h = View.class.getDeclaredField("mAccessibilityDelegate");
                h.setAccessible(true);
            } catch (Throwable unused) {
                i = true;
                return null;
            }
        }
        try {
            Object obj = h.get(view);
            if (obj instanceof View.AccessibilityDelegate) {
                return (View.AccessibilityDelegate) obj;
            }
            return null;
        } catch (Throwable unused2) {
            i = true;
            return null;
        }
    }

    @DexIgnore
    public static void d(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 23) {
            view.offsetLeftAndRight(i2);
        } else if (i3 >= 21) {
            Rect c2 = c();
            boolean z = false;
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                c2.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !c2.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            a(view, i2);
            if (z && c2.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(c2);
            }
        } else {
            a(view, i2);
        }
    }

    @DexIgnore
    public static void a(View view, Runnable runnable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimation(runnable);
        } else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay());
        }
    }

    @DexIgnore
    public static void b(View view, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setTranslationZ(f2);
        }
    }

    @DexIgnore
    @Deprecated
    public static void b(View view, boolean z) {
        view.setFitsSystemWindows(z);
    }

    @DexIgnore
    public static void a(View view, Runnable runnable, long j2) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimationDelayed(runnable, j2);
        } else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay() + j2);
        }
    }

    @DexIgnore
    public static fa b(View view, fa faVar) {
        if (Build.VERSION.SDK_INT < 21) {
            return faVar;
        }
        WindowInsets windowInsets = (WindowInsets) fa.a(faVar);
        WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(windowInsets);
        if (!onApplyWindowInsets.equals(windowInsets)) {
            windowInsets = new WindowInsets(onApplyWindowInsets);
        }
        return fa.a((Object) windowInsets);
    }

    @DexIgnore
    public static boolean a(View view, int i2, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.performAccessibilityAction(i2, bundle);
        }
        return false;
    }

    @DexIgnore
    public static void a(View view, ia.a aVar, CharSequence charSequence, la laVar) {
        a(view, aVar.a(charSequence, laVar));
    }

    @DexIgnore
    public static void a(View view, ia.a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            r(view);
            a(aVar.a(), view);
            f(view).add(aVar);
            c(view, 0);
        }
    }

    @DexIgnore
    public static void b(View view, int i2) {
        view.offsetTopAndBottom(i2);
        if (view.getVisibility() == 0) {
            L(view);
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                L((View) parent);
            }
        }
    }

    @DexIgnore
    public static void c(View view, boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.setHasTransientState(z);
        }
    }

    @DexIgnore
    public static f<Boolean> e() {
        return new b(f6.tag_screen_reader_focusable, Boolean.class, 28);
    }

    @DexIgnore
    public static CharSequence e(View view) {
        return d().b(view);
    }

    @DexIgnore
    public static void c(View view, int i2) {
        if (((AccessibilityManager) view.getContext().getSystemService("accessibility")).isEnabled()) {
            boolean z = e(view) != null;
            if (d(view) != 0 || (z && view.getVisibility() == 0)) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain();
                obtain.setEventType(z ? 32 : 2048);
                obtain.setContentChangeTypes(i2);
                view.sendAccessibilityEventUnchecked(obtain);
            } else if (view.getParent() != null) {
                try {
                    view.getParent().notifySubtreeAccessibilityStateChanged(view, view, i2);
                } catch (AbstractMethodError e2) {
                    Log.e("ViewCompat", view.getParent().getClass().getSimpleName() + " does not fully implement ViewParent", e2);
                }
            }
        }
    }

    @DexIgnore
    public static void a(int i2, View view) {
        List<ia.a> f2 = f(view);
        for (int i3 = 0; i3 < f2.size(); i3++) {
            if (f2.get(i3).a() == i2) {
                f2.remove(i3);
                return;
            }
        }
    }

    @DexIgnore
    public static int b() {
        int i2;
        int i3;
        if (Build.VERSION.SDK_INT >= 17) {
            return View.generateViewId();
        }
        do {
            i2 = a.get();
            i3 = i2 + 1;
            if (i3 > 16777215) {
                i3 = 1;
            }
        } while (!a.compareAndSet(i2, i3));
        return i2;
    }

    @DexIgnore
    public static f<CharSequence> d() {
        return new c(f6.tag_accessibility_pane_title, CharSequence.class, 8, 28);
    }

    @DexIgnore
    public static void a(View view, Paint paint) {
        if (Build.VERSION.SDK_INT >= 17) {
            view.setLayerPaint(paint);
            return;
        }
        view.setLayerType(view.getLayerType(), paint);
        view.invalidate();
    }

    @DexIgnore
    public static boolean b(View view, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return h.a(view).a(keyEvent);
    }

    @DexIgnore
    public static ba a(View view) {
        if (g == null) {
            g = new WeakHashMap<>();
        }
        ba baVar = g.get(view);
        if (baVar != null) {
            return baVar;
        }
        ba baVar2 = new ba(view);
        g.put(view, baVar2);
        return baVar2;
    }

    @DexIgnore
    public static void a(View view, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setElevation(f2);
        }
    }

    @DexIgnore
    public static void a(View view, String str) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setTransitionName(str);
            return;
        }
        if (f == null) {
            f = new WeakHashMap<>();
        }
        f.put(view, str);
    }

    @DexIgnore
    public static void a(View view, s9 s9Var) {
        if (Build.VERSION.SDK_INT < 21) {
            return;
        }
        if (s9Var == null) {
            view.setOnApplyWindowInsetsListener((View.OnApplyWindowInsetsListener) null);
        } else {
            view.setOnApplyWindowInsetsListener(new a(s9Var));
        }
    }

    @DexIgnore
    public static fa a(View view, fa faVar) {
        if (Build.VERSION.SDK_INT < 21) {
            return faVar;
        }
        WindowInsets windowInsets = (WindowInsets) fa.a(faVar);
        WindowInsets dispatchApplyWindowInsets = view.dispatchApplyWindowInsets(windowInsets);
        if (!dispatchApplyWindowInsets.equals(windowInsets)) {
            windowInsets = new WindowInsets(dispatchApplyWindowInsets);
        }
        return fa.a((Object) windowInsets);
    }

    @DexIgnore
    public static void a(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    @DexIgnore
    public static void a(View view, ColorStateList colorStateList) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintList(colorStateList);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        } else if (view instanceof w9) {
            ((w9) view).setSupportBackgroundTintList(colorStateList);
        }
    }

    @DexIgnore
    public static void a(View view, PorterDuff.Mode mode) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintMode(mode);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        } else if (view instanceof w9) {
            ((w9) view).setSupportBackgroundTintMode(mode);
        }
    }

    @DexIgnore
    public static void a(View view, int i2) {
        view.offsetLeftAndRight(i2);
        if (view.getVisibility() == 0) {
            L(view);
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                L((View) parent);
            }
        }
    }

    @DexIgnore
    public static void a(View view, Rect rect) {
        if (Build.VERSION.SDK_INT >= 18) {
            view.setClipBounds(rect);
        }
    }

    @DexIgnore
    public static void a(View view, int i2, int i3) {
        if (Build.VERSION.SDK_INT >= 23) {
            view.setScrollIndicators(i2, i3);
        }
    }

    @DexIgnore
    public static void a(View view, u9 u9Var) {
        if (Build.VERSION.SDK_INT >= 24) {
            view.setPointerIcon((PointerIcon) (u9Var != null ? u9Var.a() : null));
        }
    }

    @DexIgnore
    public static boolean a(View view, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return h.a(view).a(view, keyEvent);
    }

    @DexIgnore
    public static void a(View view, boolean z) {
        a().b(view, Boolean.valueOf(z));
    }

    @DexIgnore
    public static f<Boolean> a() {
        return new d(f6.tag_accessibility_heading, Boolean.class, 28);
    }
}
