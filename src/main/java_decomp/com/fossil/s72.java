package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s72 implements Parcelable.Creator<t72> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            if (f22.a(a) != 1) {
                f22.v(parcel, a);
            } else {
                str = f22.e(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new t72(str);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new t72[i];
    }
}
