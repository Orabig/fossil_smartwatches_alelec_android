package com.fossil;

import com.fossil.ew1;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yv1<R extends ew1> {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Status status);
    }

    @DexIgnore
    public abstract R a(long j, TimeUnit timeUnit);

    @DexIgnore
    public abstract void a(fw1<? super R> fw1);

    @DexIgnore
    public abstract void a(a aVar);
}
