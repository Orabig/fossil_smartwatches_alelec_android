package com.fossil;

import android.content.Context;
import android.util.TypedValue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class py5 {
    @DexIgnore
    public static int a(Context context, float f) {
        float applyDimension = TypedValue.applyDimension(1, f, context.getResources().getDisplayMetrics());
        int i = (int) (((double) applyDimension) + 0.5d);
        if (i != 0 || applyDimension <= 0.0f) {
            return i;
        }
        return 1;
    }
}
