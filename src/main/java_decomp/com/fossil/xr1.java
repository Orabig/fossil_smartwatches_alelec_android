package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr1 implements Factory<Integer> {
    @DexIgnore
    public static /* final */ xr1 a; // = new xr1();

    @DexIgnore
    public static xr1 a() {
        return a;
    }

    @DexIgnore
    public static int b() {
        return wr1.a();
    }

    @DexIgnore
    public Integer get() {
        return Integer.valueOf(b());
    }
}
