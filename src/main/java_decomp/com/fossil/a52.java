package com.fossil;

import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a52 implements Runnable {
    @DexIgnore
    public /* final */ Runnable a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public a52(Runnable runnable, int i) {
        this.a = runnable;
        this.b = i;
    }

    @DexIgnore
    public final void run() {
        Process.setThreadPriority(this.b);
        this.a.run();
    }
}
