package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class du4 extends RecyclerView.g<a> {
    @DexIgnore
    public List<AddressWrapper> a;
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ TextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ du4 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.du4$a$a")
        /* renamed from: com.fossil.du4$a$a  reason: collision with other inner class name */
        public static final class C0010a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0010a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b;
                int adapterPosition = this.a.getAdapterPosition();
                if (this.a.getAdapterPosition() != -1 && (b = this.a.d.b) != null) {
                    List a2 = this.a.d.a;
                    if (a2 != null) {
                        b.a((AddressWrapper) a2.get(adapterPosition));
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(du4 du4, View view) {
            super(view);
            wg6.b(view, "view");
            this.d = du4;
            view.setOnClickListener(new C0010a(this));
            View findViewById = view.findViewById(2131362442);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131362312);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                    View findViewById3 = view.findViewById(2131362602);
                    if (findViewById3 != null) {
                        this.c = (ImageView) findViewById3;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final ImageView b() {
            return this.c;
        }

        @DexIgnore
        public final TextView c() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(AddressWrapper addressWrapper);
    }

    @DexIgnore
    public int getItemCount() {
        List<AddressWrapper> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        List<AddressWrapper> list = this.a;
        if (list != null) {
            AddressWrapper addressWrapper = list.get(i);
            aVar.c().setText(addressWrapper.getName());
            aVar.a().setText(addressWrapper.getAddress());
            int i2 = eu4.a[addressWrapper.getType().ordinal()];
            if (i2 == 1) {
                aVar.b().setImageDrawable(w6.c(PortfolioApp.get.instance(), 2131231029));
            } else if (i2 == 2) {
                aVar.b().setImageDrawable(w6.c(PortfolioApp.get.instance(), 2131231031));
            } else if (i2 == 3) {
                aVar.b().setImageDrawable(w6.c(PortfolioApp.get.instance(), 2131231030));
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558636, viewGroup, false);
        wg6.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<AddressWrapper> list) {
        wg6.b(list, "addressList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final AddressWrapper a(int i) {
        if (i != -1) {
            List<AddressWrapper> list = this.a;
            if (list == null) {
                wg6.a();
                throw null;
            } else if (i <= list.size()) {
                List<AddressWrapper> list2 = this.a;
                if (list2 != null) {
                    return list2.get(i);
                }
                wg6.a();
                throw null;
            }
        }
        return null;
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "listener");
        this.b = bVar;
    }
}
