package com.fossil;

import com.fossil.ap6;
import java.lang.Comparable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zo6<T extends ap6 & Comparable<? super T>> {
    @DexIgnore
    public volatile int _size; // = 0;
    @DexIgnore
    public T[] a;

    @DexIgnore
    public final T a() {
        T[] tArr = this.a;
        if (tArr != null) {
            return tArr[0];
        }
        return null;
    }

    @DexIgnore
    public final int b() {
        return this._size;
    }

    @DexIgnore
    public final boolean c() {
        return b() == 0;
    }

    @DexIgnore
    public final void d(int i) {
        while (i > 0) {
            T[] tArr = this.a;
            if (tArr != null) {
                int i2 = (i - 1) / 2;
                T t = tArr[i2];
                if (t != null) {
                    Comparable comparable = (Comparable) t;
                    T t2 = tArr[i];
                    if (t2 == null) {
                        wg6.a();
                        throw null;
                    } else if (comparable.compareTo(t2) > 0) {
                        a(i, i2);
                        i = i2;
                    } else {
                        return;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final T[] e() {
        T[] tArr = this.a;
        if (tArr == null) {
            T[] tArr2 = new ap6[4];
            this.a = tArr2;
            return tArr2;
        } else if (b() < tArr.length) {
            return tArr;
        } else {
            T[] copyOf = Arrays.copyOf(tArr, b() * 2);
            wg6.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
            T[] tArr3 = (ap6[]) copyOf;
            this.a = tArr3;
            return tArr3;
        }
    }

    @DexIgnore
    public final T f() {
        T a2;
        synchronized (this) {
            a2 = b() > 0 ? a(0) : null;
        }
        return a2;
    }

    @DexIgnore
    public final T a(int i) {
        boolean z = false;
        if (nl6.a()) {
            if (!(b() > 0)) {
                throw new AssertionError();
            }
        }
        T[] tArr = this.a;
        if (tArr != null) {
            b(b() - 1);
            if (i < b()) {
                a(i, b());
                int i2 = (i - 1) / 2;
                if (i > 0) {
                    T t = tArr[i];
                    if (t != null) {
                        Comparable comparable = (Comparable) t;
                        T t2 = tArr[i2];
                        if (t2 == null) {
                            wg6.a();
                            throw null;
                        } else if (comparable.compareTo(t2) < 0) {
                            a(i, i2);
                            d(i2);
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                c(i);
            }
            T t3 = tArr[b()];
            if (t3 != null) {
                if (nl6.a()) {
                    if (t3.a() == this) {
                        z = true;
                    }
                    if (!z) {
                        throw new AssertionError();
                    }
                }
                t3.a((zo6<?>) null);
                t3.a(-1);
                tArr[b()] = null;
                return t3;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void b(int i) {
        this._size = i;
    }

    @DexIgnore
    public final void c(int i) {
        while (true) {
            int i2 = (i * 2) + 1;
            if (i2 < b()) {
                T[] tArr = this.a;
                if (tArr != null) {
                    int i3 = i2 + 1;
                    if (i3 < b()) {
                        T t = tArr[i3];
                        if (t != null) {
                            Comparable comparable = (Comparable) t;
                            T t2 = tArr[i2];
                            if (t2 == null) {
                                wg6.a();
                                throw null;
                            } else if (comparable.compareTo(t2) < 0) {
                                i2 = i3;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    T t3 = tArr[i];
                    if (t3 != null) {
                        Comparable comparable2 = (Comparable) t3;
                        T t4 = tArr[i2];
                        if (t4 == null) {
                            wg6.a();
                            throw null;
                        } else if (comparable2.compareTo(t4) > 0) {
                            a(i, i2);
                            i = i2;
                        } else {
                            return;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final boolean b(T t) {
        boolean z;
        wg6.b(t, "node");
        synchronized (this) {
            z = true;
            boolean z2 = false;
            if (t.a() == null) {
                z = false;
            } else {
                int b = t.b();
                if (nl6.a()) {
                    if (b >= 0) {
                        z2 = true;
                    }
                    if (!z2) {
                        throw new AssertionError();
                    }
                }
                a(b);
            }
        }
        return z;
    }

    @DexIgnore
    public final T d() {
        T a2;
        synchronized (this) {
            a2 = a();
        }
        return a2;
    }

    @DexIgnore
    public final void a(T t) {
        wg6.b(t, "node");
        if (nl6.a()) {
            if (!(t.a() == null)) {
                throw new AssertionError();
            }
        }
        t.a((zo6<?>) this);
        ap6[] e = e();
        int b = b();
        b(b + 1);
        e[b] = t;
        t.a(b);
        d(b);
    }

    @DexIgnore
    public final void a(int i, int i2) {
        T[] tArr = this.a;
        if (tArr != null) {
            T t = tArr[i2];
            if (t != null) {
                T t2 = tArr[i];
                if (t2 != null) {
                    tArr[i] = t;
                    tArr[i2] = t2;
                    t.a(i);
                    t2.a(i2);
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }
}
