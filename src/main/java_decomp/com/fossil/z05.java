package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface z05 extends k24<y05> {
    @DexIgnore
    void a(String str);

    @DexIgnore
    void b(int i);

    @DexIgnore
    void close();
}
