package com.fossil;

import android.text.TextUtils;
import com.fossil.yq6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ip4 implements Interceptor {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((qg6) null);
    @DexIgnore
    public AtomicInteger a; // = new AtomicInteger(0);
    @DexIgnore
    public Auth b;
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ zm4 d;
    @DexIgnore
    public /* final */ AuthApiGuestService e;
    @DexIgnore
    public /* final */ an4 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ip4.g;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ip4.class.getSimpleName();
        wg6.a((Object) simpleName, "AuthenticationInterceptor::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public ip4(PortfolioApp portfolioApp, zm4 zm4, AuthApiGuestService authApiGuestService, an4 an4) {
        wg6.b(portfolioApp, "mApplication");
        wg6.b(zm4, "mProviderManager");
        wg6.b(authApiGuestService, "mAuthApiGuestService");
        wg6.b(an4, "mSharedPreferencesManager");
        this.c = portfolioApp;
        this.d = zm4;
        this.e = authApiGuestService;
        this.f = an4;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) {
        ServerError serverError;
        wg6.b(chain, "chain");
        yq6.a f2 = chain.t().f();
        MFUser b2 = this.d.n().b();
        String userAccessToken = b2 != null ? b2.getUserAccessToken() : null;
        if (!TextUtils.isEmpty(userAccessToken)) {
            long currentTimeMillis = (System.currentTimeMillis() - this.f.a()) / ((long) 1000);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            StringBuilder sb = new StringBuilder();
            sb.append("loginDurationInSeconds = ");
            sb.append(currentTimeMillis);
            sb.append(" tokenExpiresIn ");
            wg6.a((Object) b2, "currentUser");
            sb.append(b2.getAccessTokenExpiresIn());
            local.d(str, sb.toString());
            if (currentTimeMillis >= ((long) b2.getAccessTokenExpiresIn())) {
                int i = 0;
                Auth a2 = a(b2, this.a.getAndIncrement() == 0);
                if (this.a.decrementAndGet() == 0) {
                    this.b = null;
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "intercept decrementAndGet mCount=" + this.a);
                if ((a2 != null ? a2.getAccessToken() : null) != null) {
                    b2.setUserAccessToken(a2.getAccessToken());
                    b2.setRefreshToken(a2.getRefreshToken());
                    b2.setAccessTokenExpiresAt(bk4.u(a2.getAccessTokenExpiresAt()));
                    Integer accessTokenExpiresIn = a2.getAccessTokenExpiresIn();
                    if (accessTokenExpiresIn != null) {
                        i = accessTokenExpiresIn.intValue();
                    }
                    b2.setAccessTokenExpiresIn(Integer.valueOf(i));
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = g;
                    local3.d(str3, "accessToken = " + a2.getAccessToken() + ", refreshToken = " + a2.getRefreshToken() + " userOnboardingComplete=" + b2.isOnboardingComplete());
                    this.d.n().a(b2);
                    this.f.w(a2.getAccessToken());
                    this.f.a(System.currentTimeMillis());
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Bearer ");
                    sb2.append(a2.getAccessToken());
                    f2.a("Authorization", sb2.toString());
                    f2.a();
                } else {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Bearer ");
                    if (userAccessToken != null) {
                        sb3.append(userAccessToken);
                        f2.a("Authorization", sb3.toString());
                        f2.a();
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Bearer ");
                if (userAccessToken != null) {
                    sb4.append(userAccessToken);
                    f2.a("Authorization", sb4.toString());
                    f2.a();
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        f2.a("Content-Type", Constants.CONTENT_TYPE);
        f2.a(com.zendesk.sdk.network.Constants.USER_AGENT_HEADER, mh4.b.a());
        f2.a("locale", this.c.o());
        try {
            Response a3 = chain.a(f2.a());
            wg6.a((Object) a3, "chain.proceed(requestBuilder.build())");
            return a3;
        } catch (Exception e2) {
            if (e2 instanceof ServerErrorException) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str4 = g;
                StringBuilder sb5 = new StringBuilder();
                sb5.append("exception=");
                ServerErrorException serverErrorException = (ServerErrorException) e2;
                sb5.append(serverErrorException.getServerError());
                local4.d(str4, sb5.toString());
                serverError = serverErrorException.getServerError();
            } else if (e2 instanceof UnknownHostException) {
                serverError = new ServerError(601, "");
            } else if (e2 instanceof SocketTimeoutException) {
                serverError = new ServerError(MFNetworkReturnCode.CLIENT_TIMEOUT, "");
            } else {
                serverError = new ServerError(600, "");
            }
            Response.a aVar = new Response.a();
            aVar.a(f2.a());
            aVar.a(wq6.HTTP_1_1);
            Integer code = serverError.getCode();
            wg6.a((Object) code, "serverError.code");
            aVar.a(code.intValue());
            String message = serverError.getMessage();
            if (message == null) {
                message = "";
            }
            aVar.a(message);
            aVar.a(zq6.create(uq6.b(com.zendesk.sdk.network.Constants.APPLICATION_JSON), new Gson().a(serverError)));
            aVar.a("Content-Type", Constants.CONTENT_TYPE);
            aVar.a(com.zendesk.sdk.network.Constants.USER_AGENT_HEADER, mh4.b.a());
            aVar.a("Locale", this.c.o());
            Response a4 = aVar.a();
            wg6.a((Object) a4, "Response.Builder()\n     \u2026                 .build()");
            return a4;
        }
    }

    @DexIgnore
    public final synchronized Auth a(MFUser mFUser, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "getAuth needProcess=" + z + " mCount=" + this.a);
        if (z) {
            ku3 ku3 = new ku3();
            ku3.a("refreshToken", mFUser.getRefreshToken());
            try {
                rx6 s = this.e.tokenRefresh(ku3).s();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a2 = mp4.h.a();
                local2.d(a2, "Refresh Token error=" + s.b() + " body=" + s.c());
                this.b = (Auth) s.a();
            } catch (IOException e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local3.d(str2, "getAuth needProcess=" + z + " mCount=" + this.a + " exception=" + e2.getMessage());
                e2.printStackTrace();
                return null;
            }
        }
        return this.b;
    }
}
