package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw2 extends bg2 {
    @DexIgnore
    public /* final */ /* synthetic */ rc3 a;

    @DexIgnore
    public xw2(wv2 wv2, rc3 rc3) {
        this.a = rc3;
    }

    @DexIgnore
    public final void a(xf2 xf2) throws RemoteException {
        Status o = xf2.o();
        if (o == null) {
            this.a.b((Exception) new sv1(new Status(8, "Got null status from location service")));
        } else if (o.B() == 0) {
            this.a.a(true);
        } else {
            this.a.b((Exception) b12.a(o));
        }
    }
}
