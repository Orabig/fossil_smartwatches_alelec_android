package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.rv1;
import com.fossil.wv1;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz1 extends qb3 implements wv1.b, wv1.c {
    @DexIgnore
    public static rv1.a<? extends ac3, lb3> h; // = zb3.c;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ rv1.a<? extends ac3, lb3> c;
    @DexIgnore
    public Set<Scope> d;
    @DexIgnore
    public e12 e;
    @DexIgnore
    public ac3 f;
    @DexIgnore
    public ez1 g;

    @DexIgnore
    public dz1(Context context, Handler handler, e12 e12) {
        this(context, handler, e12, h);
    }

    @DexIgnore
    public final void a(ez1 ez1) {
        ac3 ac3 = this.f;
        if (ac3 != null) {
            ac3.a();
        }
        this.e.a(Integer.valueOf(System.identityHashCode(this)));
        rv1.a<? extends ac3, lb3> aVar = this.c;
        Context context = this.a;
        Looper looper = this.b.getLooper();
        e12 e12 = this.e;
        this.f = (ac3) aVar.a(context, looper, e12, e12.j(), this, this);
        this.g = ez1;
        Set<Scope> set = this.d;
        if (set == null || set.isEmpty()) {
            this.b.post(new cz1(this));
        } else {
            this.f.b();
        }
    }

    @DexIgnore
    public final void b(xb3 xb3) {
        gv1 p = xb3.p();
        if (p.E()) {
            y12 B = xb3.B();
            gv1 B2 = B.B();
            if (!B2.E()) {
                String valueOf = String.valueOf(B2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                this.g.b(B2);
                this.f.a();
                return;
            }
            this.g.a(B.p(), this.d);
        } else {
            this.g.b(p);
        }
        this.f.a();
    }

    @DexIgnore
    public final void f(Bundle bundle) {
        this.f.a(this);
    }

    @DexIgnore
    public final void g(int i) {
        this.f.a();
    }

    @DexIgnore
    public final ac3 q() {
        return this.f;
    }

    @DexIgnore
    public final void r() {
        ac3 ac3 = this.f;
        if (ac3 != null) {
            ac3.a();
        }
    }

    @DexIgnore
    public dz1(Context context, Handler handler, e12 e12, rv1.a<? extends ac3, lb3> aVar) {
        this.a = context;
        this.b = handler;
        w12.a(e12, (Object) "ClientSettings must not be null");
        this.e = e12;
        this.d = e12.i();
        this.c = aVar;
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        this.g.b(gv1);
    }

    @DexIgnore
    public final void a(xb3 xb3) {
        this.b.post(new fz1(this, xb3));
    }
}
