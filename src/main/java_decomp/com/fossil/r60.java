package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r60 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ s60 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<r60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                s60 valueOf = s60.valueOf(readString);
                parcel.setDataPosition(0);
                switch (e01.a[valueOf.ordinal()]) {
                    case 1:
                        return h60.CREATOR.createFromParcel(parcel);
                    case 2:
                        return o60.CREATOR.createFromParcel(parcel);
                    case 3:
                        return p60.CREATOR.createFromParcel(parcel);
                    case 4:
                        return k60.CREATOR.createFromParcel(parcel);
                    case 5:
                        return l60.CREATOR.createFromParcel(parcel);
                    case 6:
                        return q60.CREATOR.createFromParcel(parcel);
                    case 7:
                        return j60.CREATOR.createFromParcel(parcel);
                    case 8:
                        return m60.CREATOR.createFromParcel(parcel);
                    case 9:
                        return x60.CREATOR.createFromParcel(parcel);
                    case 10:
                        return a70.CREATOR.createFromParcel(parcel);
                    case 11:
                        return u60.CREATOR.createFromParcel(parcel);
                    case 12:
                        return z60.CREATOR.createFromParcel(parcel);
                    case 13:
                        return g60.CREATOR.createFromParcel(parcel);
                    case 14:
                        return v60.CREATOR.createFromParcel(parcel);
                    case 15:
                        return n60.CREATOR.createFromParcel(parcel);
                    case 16:
                        return t60.CREATOR.createFromParcel(parcel);
                    case 17:
                        return y60.CREATOR.createFromParcel(parcel);
                    case 18:
                        return i60.CREATOR.createFromParcel(parcel);
                    case 19:
                        return w60.CREATOR.createFromParcel(parcel);
                    default:
                        throw new kc6();
                }
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new r60[i];
        }
    }

    @DexIgnore
    public r60(s60 s60) {
        this.a = s60;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(jSONObject, bm0.KEY, (Object) this.a.b());
            cw0.a(jSONObject, bm0.VALUE, d());
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final byte[] b() {
        ByteBuffer allocate = ByteBuffer.allocate(c().length + 3);
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.a.a()).put((byte) c().length).array();
        wg6.a(array, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        byte[] array2 = allocate.put(array).put(c()).array();
        wg6.a(array2, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        return array2;
    }

    @DexIgnore
    public abstract byte[] c();

    @DexIgnore
    public abstract Object d();

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((r60) obj).a;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DeviceConfigItem");
    }

    @DexIgnore
    public final s60 getKey() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public r60(Parcel parcel) {
        this(s60.valueOf(r2));
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            return;
        }
        wg6.a();
        throw null;
    }
}
