package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ h83 a;
    @DexIgnore
    public /* final */ /* synthetic */ g83 b;

    @DexIgnore
    public i83(g83 g83, h83 h83) {
        this.b = g83;
        this.a = h83;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.a, false);
        g83 g83 = this.b;
        g83.c = null;
        g83.q().a((h83) null);
    }
}
