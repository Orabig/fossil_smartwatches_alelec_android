package com.fossil;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kt6 extends yt6, WritableByteChannel {
    @DexIgnore
    long a(zt6 zt6) throws IOException;

    @DexIgnore
    jt6 a();

    @DexIgnore
    kt6 a(mt6 mt6) throws IOException;

    @DexIgnore
    kt6 a(String str) throws IOException;

    @DexIgnore
    kt6 c() throws IOException;

    @DexIgnore
    kt6 c(long j) throws IOException;

    @DexIgnore
    kt6 d(long j) throws IOException;

    @DexIgnore
    OutputStream d();

    @DexIgnore
    void flush() throws IOException;

    @DexIgnore
    kt6 write(byte[] bArr) throws IOException;

    @DexIgnore
    kt6 write(byte[] bArr, int i, int i2) throws IOException;

    @DexIgnore
    kt6 writeByte(int i) throws IOException;

    @DexIgnore
    kt6 writeInt(int i) throws IOException;

    @DexIgnore
    kt6 writeShort(int i) throws IOException;
}
