package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nc0 extends tc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ ec0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<nc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(r90.class.getClassLoader());
            if (readParcelable != null) {
                return new nc0((r90) readParcelable, (uc0) parcel.readParcelable(uc0.class.getClassLoader()), (ec0) parcel.readParcelable(ec0.class.getClassLoader()));
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new nc0[i];
        }
    }

    @DexIgnore
    public nc0(r90 r90, ec0 ec0) {
        super(r90, (uc0) null);
        this.c = ec0;
    }

    @DexIgnore
    public byte[] a(short s, w40 w40) {
        x90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.c != null) {
                jSONObject.put("buddyChallengeApp._.config.sync", this.c.b());
            } else {
                getDeviceMessage();
            }
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        wg6.a(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset f = mi0.A.f();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(f);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public JSONObject b() {
        JSONObject jSONObject;
        JSONObject b = super.b();
        ec0 ec0 = this.c;
        if (ec0 == null || (jSONObject = ec0.a()) == null) {
            jSONObject = new JSONObject();
        }
        return cw0.a(b, jSONObject);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(nc0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.c, ((nc0) obj).c) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.BuddyChallengeWatchAppGetChallengeInfoData");
    }

    @DexIgnore
    public final ec0 getBuddyChallenge() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        ec0 ec0 = this.c;
        return hashCode + (ec0 != null ? ec0.hashCode() : 0);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public nc0(r90 r90, uc0 uc0, ec0 ec0) {
        super(r90, uc0);
        this.c = ec0;
    }
}
