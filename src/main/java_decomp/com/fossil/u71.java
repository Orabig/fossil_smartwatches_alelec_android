package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u71 extends r91 {
    @DexIgnore
    public y11 U; // = y11.LOW;

    @DexIgnore
    public u71(ue1 ue1, q41 q41, HashMap<io0, Object> hashMap) {
        super(ue1, q41, eh1.GET_HARDWARE_LOG_IN_BACKGROUND, hashMap, ze0.a("UUID.randomUUID().toString()"));
    }

    @DexIgnore
    public void a(y11 y11) {
        this.U = y11;
    }

    @DexIgnore
    public y11 e() {
        return this.U;
    }
}
