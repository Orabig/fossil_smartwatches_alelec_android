package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeBackgroundViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class in5 implements Factory<hn5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public in5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static in5 a(Provider<ThemeRepository> provider) {
        return new in5(provider);
    }

    @DexIgnore
    public static hn5 b(Provider<ThemeRepository> provider) {
        return new CustomizeBackgroundViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeBackgroundViewModel get() {
        return b(this.a);
    }
}
