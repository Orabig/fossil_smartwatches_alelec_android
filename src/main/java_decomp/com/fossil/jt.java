package com.fossil;

import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jt implements vr {
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ Class<?> e;
    @DexIgnore
    public /* final */ Class<?> f;
    @DexIgnore
    public /* final */ vr g;
    @DexIgnore
    public /* final */ Map<Class<?>, bs<?>> h;
    @DexIgnore
    public /* final */ xr i;
    @DexIgnore
    public int j;

    @DexIgnore
    public jt(Object obj, vr vrVar, int i2, int i3, Map<Class<?>, bs<?>> map, Class<?> cls, Class<?> cls2, xr xrVar) {
        q00.a(obj);
        this.b = obj;
        q00.a(vrVar, "Signature must not be null");
        this.g = vrVar;
        this.c = i2;
        this.d = i3;
        q00.a(map);
        this.h = map;
        q00.a(cls, "Resource class must not be null");
        this.e = cls;
        q00.a(cls2, "Transcode class must not be null");
        this.f = cls2;
        q00.a(xrVar);
        this.i = xrVar;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof jt)) {
            return false;
        }
        jt jtVar = (jt) obj;
        if (!this.b.equals(jtVar.b) || !this.g.equals(jtVar.g) || this.d != jtVar.d || this.c != jtVar.c || !this.h.equals(jtVar.h) || !this.e.equals(jtVar.e) || !this.f.equals(jtVar.f) || !this.i.equals(jtVar.i)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        if (this.j == 0) {
            this.j = this.b.hashCode();
            this.j = (this.j * 31) + this.g.hashCode();
            this.j = (this.j * 31) + this.c;
            this.j = (this.j * 31) + this.d;
            this.j = (this.j * 31) + this.h.hashCode();
            this.j = (this.j * 31) + this.e.hashCode();
            this.j = (this.j * 31) + this.f.hashCode();
            this.j = (this.j * 31) + this.i.hashCode();
        }
        return this.j;
    }

    @DexIgnore
    public String toString() {
        return "EngineKey{model=" + this.b + ", width=" + this.c + ", height=" + this.d + ", resourceClass=" + this.e + ", transcodeClass=" + this.f + ", signature=" + this.g + ", hashCode=" + this.j + ", transformations=" + this.h + ", options=" + this.i + '}';
    }
}
