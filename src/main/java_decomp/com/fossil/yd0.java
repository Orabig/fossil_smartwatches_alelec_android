package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yd0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ y70[] a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<yd0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Object[] createTypedArray = parcel.createTypedArray(y70.CREATOR);
            if (createTypedArray != null) {
                wg6.a(createTypedArray, "parcel.createTypedArray(\u2026tificationReplyMessage)!!");
                return new yd0((y70[]) createTypedArray);
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new yd0[i];
        }
    }

    @DexIgnore
    public yd0(y70[] y70Arr) {
        Object[] copyOf = Arrays.copyOf(y70Arr, y70Arr.length);
        wg6.a(copyOf, "java.util.Arrays.copyOf(this, size)");
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        for (Object obj : copyOf) {
            if (hashSet.add(Byte.valueOf(((y70) obj).getMessageId()))) {
                arrayList.add(obj);
            }
        }
        Object[] array = yd6.a(arrayList.subList(0, Math.min(10, arrayList.size())), new va1()).toArray(new y70[0]);
        if (array != null) {
            this.a = (y70[]) array;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (y70 c : this.a) {
                byteArrayOutputStream.write(c.c());
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            wg6.a(byteArray, "entriesData.toByteArray()");
            this.b = byteArray;
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(new JSONObject(), bm0.REPLY_MESSAGES, (Object) cw0.a((p40[]) this.a));
    }

    @DexIgnore
    public final byte[] b() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(yd0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.a, ((yd0) obj).a);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.notification.reply_message.AppNotificationReplyMessageGroup");
    }

    @DexIgnore
    public final y70[] getReplyMessages() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return kd6.a(this.a);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(this.a, i);
    }
}
