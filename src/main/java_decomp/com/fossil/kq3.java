package com.fossil;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kq3 implements rq3, qq3 {
    @DexIgnore
    public /* final */ Map<Class<?>, ConcurrentHashMap<pq3<Object>, Executor>> a; // = new HashMap();
    @DexIgnore
    public Queue<oq3<?>> b; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Executor c;

    @DexIgnore
    public kq3(Executor executor) {
        this.c = executor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r0.hasNext() == false) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r1 = r0.next();
        ((java.util.concurrent.Executor) r1.getValue()).execute(com.fossil.jq3.a(r1, r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        r0 = a(r4).iterator();
     */
    @DexIgnore
    public void b(oq3<?> oq3) {
        w12.a(oq3);
        synchronized (this) {
            if (this.b != null) {
                this.b.add(oq3);
            }
        }
    }

    @DexIgnore
    public final synchronized Set<Map.Entry<pq3<Object>, Executor>> a(oq3<?> oq3) {
        Map map;
        map = this.a.get(oq3.b());
        return map == null ? Collections.emptySet() : map.entrySet();
    }

    @DexIgnore
    public synchronized <T> void a(Class<T> cls, Executor executor, pq3<? super T> pq3) {
        w12.a(cls);
        w12.a(pq3);
        w12.a(executor);
        if (!this.a.containsKey(cls)) {
            this.a.put(cls, new ConcurrentHashMap());
        }
        this.a.get(cls).put(pq3, executor);
    }

    @DexIgnore
    public <T> void a(Class<T> cls, pq3<? super T> pq3) {
        a(cls, this.c, pq3);
    }

    @DexIgnore
    public void a() {
        Queue<oq3<?>> queue;
        synchronized (this) {
            if (this.b != null) {
                queue = this.b;
                this.b = null;
            } else {
                queue = null;
            }
        }
        if (queue != null) {
            for (oq3 b2 : queue) {
                b(b2);
            }
        }
    }
}
