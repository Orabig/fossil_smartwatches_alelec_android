package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.bottomnavigation.BottomNavigationView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xa4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ BottomNavigationView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ViewPager2 s;

    @DexIgnore
    public xa4(Object obj, View view, int i, BottomNavigationView bottomNavigationView, ConstraintLayout constraintLayout, ViewPager2 viewPager2) {
        super(obj, view, i);
        this.q = bottomNavigationView;
        this.r = constraintLayout;
        this.s = viewPager2;
    }
}
