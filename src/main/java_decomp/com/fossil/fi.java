package com.fossil;

import android.database.Cursor;
import android.os.Build;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fi {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Map<String, a> b;
    @DexIgnore
    public /* final */ Set<b> c;
    @DexIgnore
    public /* final */ Set<d> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ String f;
        @DexIgnore
        public /* final */ int g;

        @DexIgnore
        @Deprecated
        public a(String str, String str2, boolean z, int i) {
            this(str, str2, z, i, (String) null, 0);
        }

        @DexIgnore
        public static int a(String str) {
            if (str == null) {
                return 5;
            }
            String upperCase = str.toUpperCase(Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                return 2;
            }
            if (upperCase.contains("BLOB")) {
                return 5;
            }
            return (upperCase.contains("REAL") || upperCase.contains("FLOA") || upperCase.contains("DOUB")) ? 4 : 1;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            String str;
            String str2;
            String str3;
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (Build.VERSION.SDK_INT >= 20) {
                if (this.e != aVar.e) {
                    return false;
                }
            } else if (a() != aVar.a()) {
                return false;
            }
            if (!this.a.equals(aVar.a) || this.d != aVar.d) {
                return false;
            }
            if (this.g == 1 && aVar.g == 2 && (str3 = this.f) != null && !str3.equals(aVar.f)) {
                return false;
            }
            if (this.g == 2 && aVar.g == 1 && (str2 = aVar.f) != null && !str2.equals(this.f)) {
                return false;
            }
            int i = this.g;
            if ((i == 0 || i != aVar.g || ((str = this.f) == null ? aVar.f == null : str.equals(aVar.f))) && this.c == aVar.c) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.a.hashCode() * 31) + this.c) * 31) + (this.d ? 1231 : 1237)) * 31) + this.e;
        }

        @DexIgnore
        public String toString() {
            return "Column{name='" + this.a + '\'' + ", type='" + this.b + '\'' + ", affinity='" + this.c + '\'' + ", notNull=" + this.d + ", primaryKeyPosition=" + this.e + ", defaultValue='" + this.f + '\'' + '}';
        }

        @DexIgnore
        public a(String str, String str2, boolean z, int i, String str3, int i2) {
            this.a = str;
            this.b = str2;
            this.d = z;
            this.e = i;
            this.c = a(str2);
            this.f = str3;
            this.g = i2;
        }

        @DexIgnore
        public boolean a() {
            return this.e > 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ List<String> d;
        @DexIgnore
        public /* final */ List<String> e;

        @DexIgnore
        public b(String str, String str2, String str3, List<String> list, List<String> list2) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.e = Collections.unmodifiableList(list2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a.equals(bVar.a) && this.b.equals(bVar.b) && this.c.equals(bVar.c) && this.d.equals(bVar.d)) {
                return this.e.equals(bVar.e);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "ForeignKey{referenceTable='" + this.a + '\'' + ", onDelete='" + this.b + '\'' + ", onUpdate='" + this.c + '\'' + ", columnNames=" + this.d + ", referenceColumnNames=" + this.e + '}';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Comparable<c> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ String d;

        @DexIgnore
        public c(int i, int i2, String str, String str2) {
            this.a = i;
            this.b = i2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(c cVar) {
            int i = this.a - cVar.a;
            return i == 0 ? this.b - cVar.b : i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ List<String> c;

        @DexIgnore
        public d(String str, boolean z, List<String> list) {
            this.a = str;
            this.b = z;
            this.c = list;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || d.class != obj.getClass()) {
                return false;
            }
            d dVar = (d) obj;
            if (this.b != dVar.b || !this.c.equals(dVar.c)) {
                return false;
            }
            if (this.a.startsWith("index_")) {
                return dVar.a.startsWith("index_");
            }
            return this.a.equals(dVar.a);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            if (this.a.startsWith("index_")) {
                i = "index_".hashCode();
            } else {
                i = this.a.hashCode();
            }
            return (((i * 31) + (this.b ? 1 : 0)) * 31) + this.c.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Index{name='" + this.a + '\'' + ", unique=" + this.b + ", columns=" + this.c + '}';
        }
    }

    @DexIgnore
    public fi(String str, Map<String, a> map, Set<b> set, Set<d> set2) {
        Set<d> set3;
        this.a = str;
        this.b = Collections.unmodifiableMap(map);
        this.c = Collections.unmodifiableSet(set);
        if (set2 == null) {
            set3 = null;
        } else {
            set3 = Collections.unmodifiableSet(set2);
        }
        this.d = set3;
    }

    @DexIgnore
    public static fi a(ii iiVar, String str) {
        return new fi(str, b(iiVar, str), c(iiVar, str), d(iiVar, str));
    }

    @DexIgnore
    public static Map<String, a> b(ii iiVar, String str) {
        Cursor d2 = iiVar.d("PRAGMA table_info(`" + str + "`)");
        HashMap hashMap = new HashMap();
        try {
            if (d2.getColumnCount() > 0) {
                int columnIndex = d2.getColumnIndex("name");
                int columnIndex2 = d2.getColumnIndex("type");
                int columnIndex3 = d2.getColumnIndex("notnull");
                int columnIndex4 = d2.getColumnIndex("pk");
                int columnIndex5 = d2.getColumnIndex("dflt_value");
                while (d2.moveToNext()) {
                    String string = d2.getString(columnIndex);
                    hashMap.put(string, new a(string, d2.getString(columnIndex2), d2.getInt(columnIndex3) != 0, d2.getInt(columnIndex4), d2.getString(columnIndex5), 2));
                }
            }
            return hashMap;
        } finally {
            d2.close();
        }
    }

    @DexIgnore
    public static Set<b> c(ii iiVar, String str) {
        HashSet hashSet = new HashSet();
        Cursor d2 = iiVar.d("PRAGMA foreign_key_list(`" + str + "`)");
        try {
            int columnIndex = d2.getColumnIndex("id");
            int columnIndex2 = d2.getColumnIndex("seq");
            int columnIndex3 = d2.getColumnIndex("table");
            int columnIndex4 = d2.getColumnIndex("on_delete");
            int columnIndex5 = d2.getColumnIndex("on_update");
            List<c> a2 = a(d2);
            int count = d2.getCount();
            for (int i = 0; i < count; i++) {
                d2.moveToPosition(i);
                if (d2.getInt(columnIndex2) == 0) {
                    int i2 = d2.getInt(columnIndex);
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    for (c next : a2) {
                        if (next.a == i2) {
                            arrayList.add(next.c);
                            arrayList2.add(next.d);
                        }
                    }
                    hashSet.add(new b(d2.getString(columnIndex3), d2.getString(columnIndex4), d2.getString(columnIndex5), arrayList, arrayList2));
                }
            }
            return hashSet;
        } finally {
            d2.close();
        }
    }

    @DexIgnore
    public static Set<d> d(ii iiVar, String str) {
        Cursor d2 = iiVar.d("PRAGMA index_list(`" + str + "`)");
        try {
            int columnIndex = d2.getColumnIndex("name");
            int columnIndex2 = d2.getColumnIndex("origin");
            int columnIndex3 = d2.getColumnIndex("unique");
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    HashSet hashSet = new HashSet();
                    while (d2.moveToNext()) {
                        if ("c".equals(d2.getString(columnIndex2))) {
                            String string = d2.getString(columnIndex);
                            boolean z = true;
                            if (d2.getInt(columnIndex3) != 1) {
                                z = false;
                            }
                            d a2 = a(iiVar, string, z);
                            if (a2 == null) {
                                d2.close();
                                return null;
                            }
                            hashSet.add(a2);
                        }
                    }
                    d2.close();
                    return hashSet;
                }
            }
            return null;
        } finally {
            d2.close();
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Set<d> set;
        if (this == obj) {
            return true;
        }
        if (obj == null || fi.class != obj.getClass()) {
            return false;
        }
        fi fiVar = (fi) obj;
        String str = this.a;
        if (str == null ? fiVar.a != null : !str.equals(fiVar.a)) {
            return false;
        }
        Map<String, a> map = this.b;
        if (map == null ? fiVar.b != null : !map.equals(fiVar.b)) {
            return false;
        }
        Set<b> set2 = this.c;
        if (set2 == null ? fiVar.c != null : !set2.equals(fiVar.c)) {
            return false;
        }
        Set<d> set3 = this.d;
        if (set3 == null || (set = fiVar.d) == null) {
            return true;
        }
        return set3.equals(set);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Map<String, a> map = this.b;
        int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
        Set<b> set = this.c;
        if (set != null) {
            i = set.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        return "TableInfo{name='" + this.a + '\'' + ", columns=" + this.b + ", foreignKeys=" + this.c + ", indices=" + this.d + '}';
    }

    @DexIgnore
    public static List<c> a(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("id");
        int columnIndex2 = cursor.getColumnIndex("seq");
        int columnIndex3 = cursor.getColumnIndex("from");
        int columnIndex4 = cursor.getColumnIndex("to");
        int count = cursor.getCount();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            arrayList.add(new c(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @DexIgnore
    public static d a(ii iiVar, String str, boolean z) {
        Cursor d2 = iiVar.d("PRAGMA index_xinfo(`" + str + "`)");
        try {
            int columnIndex = d2.getColumnIndex("seqno");
            int columnIndex2 = d2.getColumnIndex("cid");
            int columnIndex3 = d2.getColumnIndex("name");
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    TreeMap treeMap = new TreeMap();
                    while (d2.moveToNext()) {
                        if (d2.getInt(columnIndex2) >= 0) {
                            int i = d2.getInt(columnIndex);
                            treeMap.put(Integer.valueOf(i), d2.getString(columnIndex3));
                        }
                    }
                    ArrayList arrayList = new ArrayList(treeMap.size());
                    arrayList.addAll(treeMap.values());
                    d dVar = new d(str, z, arrayList);
                    d2.close();
                    return dVar;
                }
            }
            return null;
        } finally {
            d2.close();
        }
    }
}
