package com.fossil;

import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr5 implements MembersInjector<lr5> {
    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter, SignUpEmailUseCase signUpEmailUseCase) {
        profileSetupPresenter.e = signUpEmailUseCase;
    }

    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter, SignUpSocialUseCase signUpSocialUseCase) {
        profileSetupPresenter.f = signUpSocialUseCase;
    }

    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter, dt4 dt4) {
        profileSetupPresenter.g = dt4;
    }

    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter, AnalyticsHelper analyticsHelper) {
        profileSetupPresenter.h = analyticsHelper;
    }

    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter) {
        profileSetupPresenter.A();
    }
}
