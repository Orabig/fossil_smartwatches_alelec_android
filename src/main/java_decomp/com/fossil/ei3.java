package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ia;
import com.fossil.x1;
import com.google.android.material.internal.NavigationMenuItemView;
import com.google.android.material.internal.NavigationMenuView;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ei3 implements x1 {
    @DexIgnore
    public NavigationMenuView a;
    @DexIgnore
    public LinearLayout b;
    @DexIgnore
    public x1.a c;
    @DexIgnore
    public q1 d;
    @DexIgnore
    public int e;
    @DexIgnore
    public c f;
    @DexIgnore
    public LayoutInflater g;
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList o;
    @DexIgnore
    public Drawable p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u; // = true;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y; // = -1;
    @DexIgnore
    public /* final */ View.OnClickListener z; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            boolean z = true;
            ei3.this.c(true);
            t1 itemData = ((NavigationMenuItemView) view).getItemData();
            ei3 ei3 = ei3.this;
            boolean a2 = ei3.d.a((MenuItem) itemData, (x1) ei3, 0);
            if (itemData == null || !itemData.isCheckable() || !a2) {
                z = false;
            } else {
                ei3.this.f.a(itemData);
            }
            ei3.this.c(false);
            if (z) {
                ei3.this.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends l {
        @DexIgnore
        public b(View view) {
            super(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.g<l> {
        @DexIgnore
        public /* final */ ArrayList<e> a; // = new ArrayList<>();
        @DexIgnore
        public t1 b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public c() {
            f();
        }

        @DexIgnore
        /* renamed from: a */
        public void onBindViewHolder(l lVar, int i) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0) {
                NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) lVar.itemView;
                navigationMenuItemView.setIconTintList(ei3.this.o);
                ei3 ei3 = ei3.this;
                if (ei3.i) {
                    navigationMenuItemView.setTextAppearance(ei3.h);
                }
                ColorStateList colorStateList = ei3.this.j;
                if (colorStateList != null) {
                    navigationMenuItemView.setTextColor(colorStateList);
                }
                Drawable drawable = ei3.this.p;
                x9.a((View) navigationMenuItemView, drawable != null ? drawable.getConstantState().newDrawable() : null);
                g gVar = (g) this.a.get(i);
                navigationMenuItemView.setNeedsEmptyIcon(gVar.b);
                navigationMenuItemView.setHorizontalPadding(ei3.this.q);
                navigationMenuItemView.setIconPadding(ei3.this.r);
                ei3 ei32 = ei3.this;
                if (ei32.t) {
                    navigationMenuItemView.setIconSize(ei32.s);
                }
                navigationMenuItemView.setMaxLines(ei3.this.v);
                navigationMenuItemView.a(gVar.a(), 0);
            } else if (itemViewType == 1) {
                ((TextView) lVar.itemView).setText(((g) this.a.get(i)).a().getTitle());
            } else if (itemViewType == 2) {
                f fVar = (f) this.a.get(i);
                lVar.itemView.setPadding(0, fVar.b(), 0, fVar.a());
            }
        }

        @DexIgnore
        public Bundle c() {
            Bundle bundle = new Bundle();
            t1 t1Var = this.b;
            if (t1Var != null) {
                bundle.putInt("android:menu:checked", t1Var.getItemId());
            }
            SparseArray sparseArray = new SparseArray();
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                e eVar = this.a.get(i);
                if (eVar instanceof g) {
                    t1 a2 = ((g) eVar).a();
                    View actionView = a2 != null ? a2.getActionView() : null;
                    if (actionView != null) {
                        gi3 gi3 = new gi3();
                        actionView.saveHierarchyState(gi3);
                        sparseArray.put(a2.getItemId(), gi3);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:action_views", sparseArray);
            return bundle;
        }

        @DexIgnore
        public t1 d() {
            return this.b;
        }

        @DexIgnore
        public int e() {
            int i = ei3.this.b.getChildCount() == 0 ? 0 : 1;
            for (int i2 = 0; i2 < ei3.this.f.getItemCount(); i2++) {
                if (ei3.this.f.getItemViewType(i2) == 0) {
                    i++;
                }
            }
            return i;
        }

        @DexIgnore
        public final void f() {
            if (!this.c) {
                this.c = true;
                this.a.clear();
                this.a.add(new d());
                int size = ei3.this.d.n().size();
                int i = -1;
                boolean z = false;
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    t1 t1Var = ei3.this.d.n().get(i3);
                    if (t1Var.isChecked()) {
                        a(t1Var);
                    }
                    if (t1Var.isCheckable()) {
                        t1Var.c(false);
                    }
                    if (t1Var.hasSubMenu()) {
                        SubMenu subMenu = t1Var.getSubMenu();
                        if (subMenu.hasVisibleItems()) {
                            if (i3 != 0) {
                                this.a.add(new f(ei3.this.x, 0));
                            }
                            this.a.add(new g(t1Var));
                            int size2 = this.a.size();
                            int size3 = subMenu.size();
                            boolean z2 = false;
                            for (int i4 = 0; i4 < size3; i4++) {
                                t1 t1Var2 = (t1) subMenu.getItem(i4);
                                if (t1Var2.isVisible()) {
                                    if (!z2 && t1Var2.getIcon() != null) {
                                        z2 = true;
                                    }
                                    if (t1Var2.isCheckable()) {
                                        t1Var2.c(false);
                                    }
                                    if (t1Var.isChecked()) {
                                        a(t1Var);
                                    }
                                    this.a.add(new g(t1Var2));
                                }
                            }
                            if (z2) {
                                a(size2, this.a.size());
                            }
                        }
                    } else {
                        int groupId = t1Var.getGroupId();
                        if (groupId != i) {
                            i2 = this.a.size();
                            boolean z3 = t1Var.getIcon() != null;
                            if (i3 != 0) {
                                i2++;
                                ArrayList<e> arrayList = this.a;
                                int i5 = ei3.this.x;
                                arrayList.add(new f(i5, i5));
                            }
                            z = z3;
                        } else if (!z && t1Var.getIcon() != null) {
                            a(i2, this.a.size());
                            z = true;
                        }
                        g gVar = new g(t1Var);
                        gVar.b = z;
                        this.a.add(gVar);
                        i = groupId;
                    }
                }
                this.c = false;
            }
        }

        @DexIgnore
        public void g() {
            f();
            notifyDataSetChanged();
        }

        @DexIgnore
        public int getItemCount() {
            return this.a.size();
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public int getItemViewType(int i) {
            e eVar = this.a.get(i);
            if (eVar instanceof f) {
                return 2;
            }
            if (eVar instanceof d) {
                return 3;
            }
            if (eVar instanceof g) {
                return ((g) eVar).a().hasSubMenu() ? 1 : 0;
            }
            throw new RuntimeException("Unknown item type.");
        }

        @DexIgnore
        public l onCreateViewHolder(ViewGroup viewGroup, int i) {
            if (i == 0) {
                ei3 ei3 = ei3.this;
                return new i(ei3.g, viewGroup, ei3.z);
            } else if (i == 1) {
                return new k(ei3.this.g, viewGroup);
            } else {
                if (i == 2) {
                    return new j(ei3.this.g, viewGroup);
                }
                if (i != 3) {
                    return null;
                }
                return new b(ei3.this.b);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void onViewRecycled(l lVar) {
            if (lVar instanceof i) {
                ((NavigationMenuItemView) lVar.itemView).d();
            }
        }

        @DexIgnore
        public final void a(int i, int i2) {
            while (i < i2) {
                ((g) this.a.get(i)).b = true;
                i++;
            }
        }

        @DexIgnore
        public void a(t1 t1Var) {
            if (this.b != t1Var && t1Var.isCheckable()) {
                t1 t1Var2 = this.b;
                if (t1Var2 != null) {
                    t1Var2.setChecked(false);
                }
                this.b = t1Var;
                t1Var.setChecked(true);
            }
        }

        @DexIgnore
        public void a(Bundle bundle) {
            t1 a2;
            View actionView;
            gi3 gi3;
            t1 a3;
            int i = bundle.getInt("android:menu:checked", 0);
            if (i != 0) {
                this.c = true;
                int size = this.a.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    e eVar = this.a.get(i2);
                    if ((eVar instanceof g) && (a3 = ((g) eVar).a()) != null && a3.getItemId() == i) {
                        a(a3);
                        break;
                    }
                    i2++;
                }
                this.c = false;
                f();
            }
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:action_views");
            if (sparseParcelableArray != null) {
                int size2 = this.a.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    e eVar2 = this.a.get(i3);
                    if (!(!(eVar2 instanceof g) || (a2 = ((g) eVar2).a()) == null || (actionView = a2.getActionView()) == null || (gi3 = (gi3) sparseParcelableArray.get(a2.getItemId())) == null)) {
                        actionView.restoreHierarchyState(gi3);
                    }
                }
            }
        }

        @DexIgnore
        public void a(boolean z) {
            this.c = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements e {
    }

    @DexIgnore
    public interface e {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements e {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public int a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g implements e {
        @DexIgnore
        public /* final */ t1 a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public g(t1 t1Var) {
            this.a = t1Var;
        }

        @DexIgnore
        public t1 a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends ng {
        @DexIgnore
        public h(RecyclerView recyclerView) {
            super(recyclerView);
        }

        @DexIgnore
        public void a(View view, ia iaVar) {
            super.a(view, iaVar);
            iaVar.a((Object) ia.b.a(ei3.this.f.e(), 0, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i extends l {
        @DexIgnore
        public i(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(tf3.design_navigation_item, viewGroup, false));
            this.itemView.setOnClickListener(onClickListener);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j extends l {
        @DexIgnore
        public j(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(tf3.design_navigation_item_separator, viewGroup, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class k extends l {
        @DexIgnore
        public k(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(tf3.design_navigation_item_subheader, viewGroup, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class l extends RecyclerView.ViewHolder {
        @DexIgnore
        public l(View view) {
            super(view);
        }
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public boolean a(c2 c2Var) {
        return false;
    }

    @DexIgnore
    public boolean a(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public void b(int i2) {
        this.e = i2;
    }

    @DexIgnore
    public boolean b(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public t1 c() {
        return this.f.d();
    }

    @DexIgnore
    public int d() {
        return this.b.getChildCount();
    }

    @DexIgnore
    public Drawable e() {
        return this.p;
    }

    @DexIgnore
    public int f() {
        return this.q;
    }

    @DexIgnore
    public void g(int i2) {
        this.h = i2;
        this.i = true;
        a(false);
    }

    @DexIgnore
    public int getId() {
        return this.e;
    }

    @DexIgnore
    public int h() {
        return this.v;
    }

    @DexIgnore
    public ColorStateList i() {
        return this.j;
    }

    @DexIgnore
    public ColorStateList j() {
        return this.o;
    }

    @DexIgnore
    public final void k() {
        int i2 = (this.b.getChildCount() != 0 || !this.u) ? 0 : this.w;
        NavigationMenuView navigationMenuView = this.a;
        navigationMenuView.setPadding(0, i2, 0, navigationMenuView.getPaddingBottom());
    }

    @DexIgnore
    public void a(Context context, q1 q1Var) {
        this.g = LayoutInflater.from(context);
        this.d = q1Var;
        this.x = context.getResources().getDimensionPixelOffset(pf3.design_navigation_separator_vertical_padding);
    }

    @DexIgnore
    public Parcelable b() {
        Bundle bundle = new Bundle();
        if (this.a != null) {
            SparseArray sparseArray = new SparseArray();
            this.a.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        c cVar = this.f;
        if (cVar != null) {
            bundle.putBundle("android:menu:adapter", cVar.c());
        }
        if (this.b != null) {
            SparseArray sparseArray2 = new SparseArray();
            this.b.saveHierarchyState(sparseArray2);
            bundle.putSparseParcelableArray("android:menu:header", sparseArray2);
        }
        return bundle;
    }

    @DexIgnore
    public void c(int i2) {
        this.q = i2;
        a(false);
    }

    @DexIgnore
    public void d(int i2) {
        this.r = i2;
        a(false);
    }

    @DexIgnore
    public void e(int i2) {
        if (this.s != i2) {
            this.s = i2;
            this.t = true;
            a(false);
        }
    }

    @DexIgnore
    public void f(int i2) {
        this.v = i2;
        a(false);
    }

    @DexIgnore
    public void h(int i2) {
        this.y = i2;
        NavigationMenuView navigationMenuView = this.a;
        if (navigationMenuView != null) {
            navigationMenuView.setOverScrollMode(i2);
        }
    }

    @DexIgnore
    public void c(boolean z2) {
        c cVar = this.f;
        if (cVar != null) {
            cVar.a(z2);
        }
    }

    @DexIgnore
    public int g() {
        return this.r;
    }

    @DexIgnore
    public y1 a(ViewGroup viewGroup) {
        if (this.a == null) {
            this.a = (NavigationMenuView) this.g.inflate(tf3.design_navigation_menu, viewGroup, false);
            NavigationMenuView navigationMenuView = this.a;
            navigationMenuView.setAccessibilityDelegateCompat(new h(navigationMenuView));
            if (this.f == null) {
                this.f = new c();
            }
            int i2 = this.y;
            if (i2 != -1) {
                this.a.setOverScrollMode(i2);
            }
            this.b = (LinearLayout) this.g.inflate(tf3.design_navigation_item_header, this.a, false);
            this.a.setAdapter(this.f);
        }
        return this.a;
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        this.j = colorStateList;
        a(false);
    }

    @DexIgnore
    public void b(boolean z2) {
        if (this.u != z2) {
            this.u = z2;
            k();
        }
    }

    @DexIgnore
    public void a(boolean z2) {
        c cVar = this.f;
        if (cVar != null) {
            cVar.g();
        }
    }

    @DexIgnore
    public void a(q1 q1Var, boolean z2) {
        x1.a aVar = this.c;
        if (aVar != null) {
            aVar.a(q1Var, z2);
        }
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
            if (sparseParcelableArray != null) {
                this.a.restoreHierarchyState(sparseParcelableArray);
            }
            Bundle bundle2 = bundle.getBundle("android:menu:adapter");
            if (bundle2 != null) {
                this.f.a(bundle2);
            }
            SparseArray sparseParcelableArray2 = bundle.getSparseParcelableArray("android:menu:header");
            if (sparseParcelableArray2 != null) {
                this.b.restoreHierarchyState(sparseParcelableArray2);
            }
        }
    }

    @DexIgnore
    public void a(t1 t1Var) {
        this.f.a(t1Var);
    }

    @DexIgnore
    public View a(int i2) {
        View inflate = this.g.inflate(i2, this.b, false);
        a(inflate);
        return inflate;
    }

    @DexIgnore
    public void a(View view) {
        this.b.addView(view);
        NavigationMenuView navigationMenuView = this.a;
        navigationMenuView.setPadding(0, 0, 0, navigationMenuView.getPaddingBottom());
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        this.o = colorStateList;
        a(false);
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.p = drawable;
        a(false);
    }

    @DexIgnore
    public void a(fa faVar) {
        int e2 = faVar.e();
        if (this.w != e2) {
            this.w = e2;
            k();
        }
        NavigationMenuView navigationMenuView = this.a;
        navigationMenuView.setPadding(0, navigationMenuView.getPaddingTop(), 0, faVar.b());
        x9.a((View) this.b, faVar);
    }
}
