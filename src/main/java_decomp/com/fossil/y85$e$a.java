package com.fossil;

import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y85$e$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ HybridPreset $it;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y85$e$a(HybridPreset hybridPreset, xe6 xe6, HomeHybridCustomizePresenter.e eVar) {
        super(2, xe6);
        this.$it = hybridPreset;
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        y85$e$a y85_e_a = new y85$e$a(this.$it, xe6, this.this$0);
        y85_e_a.p$ = (il6) obj;
        return y85_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((y85$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            HybridPresetRepository e = this.this$0.this$0.p;
            HybridPreset hybridPreset = this.$it;
            this.L$0 = il6;
            this.label = 1;
            if (e.upsertHybridPreset(hybridPreset, this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
