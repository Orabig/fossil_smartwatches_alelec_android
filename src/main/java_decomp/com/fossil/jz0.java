package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jz0 extends ml0 {
    @DexIgnore
    public int J; // = 20;
    @DexIgnore
    public /* final */ boolean K;

    @DexIgnore
    public jz0(ue1 ue1) {
        super(tf1.GET_OPTIMAL_PAYLOAD, lx0.GET_OPTIMAL_PAYLOAD, ue1, 0, 8);
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 2) {
            this.J = cw0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            cw0.a(jSONObject, bm0.OPTIMAL_PAYLOAD, (Object) Integer.valueOf(this.J));
            this.v = bn0.a(this.v, (lx0) null, (String) null, il0.SUCCESS, (ch0) null, (sj0) null, 27);
        } else {
            this.v = bn0.a(this.v, (lx0) null, (String) null, il0.INVALID_RESPONSE_LENGTH, (ch0) null, (sj0) null, 27);
        }
        this.C = true;
        return jSONObject;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.OPTIMAL_PAYLOAD, (Object) Integer.valueOf(this.J));
    }

    @DexIgnore
    public boolean q() {
        return this.K;
    }
}
