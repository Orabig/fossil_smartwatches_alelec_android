package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cu6 extends nu6 {
    @DexIgnore
    public ku6 group;
    @DexIgnore
    public ju6 option;

    @DexIgnore
    public cu6(String str) {
        super(str);
    }

    @DexIgnore
    public ju6 getOption() {
        return this.option;
    }

    @DexIgnore
    public ku6 getOptionGroup() {
        return this.group;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public cu6(ku6 ku6, ju6 ju6) {
        this(r0.toString());
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("The option '");
        stringBuffer.append(ju6.getKey());
        stringBuffer.append("' was specified but an option from this group ");
        stringBuffer.append("has already been selected: '");
        stringBuffer.append(ku6.getSelected());
        stringBuffer.append("'");
        this.group = ku6;
        this.option = ju6;
    }
}
