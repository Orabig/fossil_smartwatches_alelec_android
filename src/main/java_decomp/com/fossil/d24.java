package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d24 implements Factory<ey5> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public d24(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static d24 a(b14 b14) {
        return new d24(b14);
    }

    @DexIgnore
    public static ey5 b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static ey5 c(b14 b14) {
        ey5 o = b14.o();
        z76.a(o, "Cannot return null from a non-@Nullable @Provides method");
        return o;
    }

    @DexIgnore
    public ey5 get() {
        return b(this.a);
    }
}
