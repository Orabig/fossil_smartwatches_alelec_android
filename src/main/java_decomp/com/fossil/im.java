package com.fossil;

import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class im {
    @DexIgnore
    public static /* final */ String a; // = tl.a("Schedulers");

    @DexIgnore
    public static void a(ml mlVar, WorkDatabase workDatabase, List<hm> list) {
        if (list != null && list.size() != 0) {
            ao d = workDatabase.d();
            workDatabase.beginTransaction();
            try {
                List<zn> a2 = d.a(mlVar.e());
                if (a2 != null && a2.size() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    for (zn znVar : a2) {
                        d.a(znVar.a, currentTimeMillis);
                    }
                }
                workDatabase.setTransactionSuccessful();
                if (a2 != null && a2.size() > 0) {
                    zn[] znVarArr = (zn[]) a2.toArray(new zn[0]);
                    for (hm a3 : list) {
                        a3.a(znVarArr);
                    }
                }
            } finally {
                workDatabase.endTransaction();
            }
        }
    }

    @DexIgnore
    public static hm a(Context context, lm lmVar) {
        if (Build.VERSION.SDK_INT >= 23) {
            wm wmVar = new wm(context, lmVar);
            jo.a(context, SystemJobService.class, true);
            tl.a().a(a, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
            return wmVar;
        }
        hm a2 = a(context);
        if (a2 != null) {
            return a2;
        }
        tm tmVar = new tm(context);
        jo.a(context, SystemAlarmService.class, true);
        tl.a().a(a, "Created SystemAlarmScheduler", new Throwable[0]);
        return tmVar;
    }

    @DexIgnore
    public static hm a(Context context) {
        try {
            hm hmVar = (hm) Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(new Class[]{Context.class}).newInstance(new Object[]{context});
            tl.a().a(a, String.format("Created %s", new Object[]{"androidx.work.impl.background.gcm.GcmScheduler"}), new Throwable[0]);
            return hmVar;
        } catch (Throwable th) {
            tl.a().a(a, "Unable to create GCM Scheduler", th);
            return null;
        }
    }
}
