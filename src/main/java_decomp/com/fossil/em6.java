package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class em6 extends dl6 {
    @DexIgnore
    public long a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public ao6<yl6<?>> c;

    @DexIgnore
    public static /* synthetic */ void b(em6 em6, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            em6.c(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: incrementUseCount");
    }

    @DexIgnore
    public final boolean B() {
        ao6<yl6<?>> ao6 = this.c;
        if (ao6 != null) {
            return ao6.b();
        }
        return true;
    }

    @DexIgnore
    public long C() {
        if (!D()) {
            return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        return o();
    }

    @DexIgnore
    public final boolean D() {
        yl6 c2;
        ao6<yl6<?>> ao6 = this.c;
        if (ao6 == null || (c2 = ao6.c()) == null) {
            return false;
        }
        c2.run();
        return true;
    }

    @DexIgnore
    public boolean E() {
        return false;
    }

    @DexIgnore
    public final void a(yl6<?> yl6) {
        wg6.b(yl6, "task");
        ao6<yl6<?>> ao6 = this.c;
        if (ao6 == null) {
            ao6 = new ao6<>();
            this.c = ao6;
        }
        ao6.a(yl6);
    }

    @DexIgnore
    public final long b(boolean z) {
        return z ? 4294967296L : 1;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.a += b(z);
        if (!z) {
            this.b = true;
        }
    }

    @DexIgnore
    public long o() {
        ao6<yl6<?>> ao6 = this.c;
        if (ao6 == null || ao6.b()) {
            return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        return 0;
    }

    @DexIgnore
    public final boolean p() {
        return this.a >= b(true);
    }

    @DexIgnore
    public void shutdown() {
    }

    @DexIgnore
    public static /* synthetic */ void a(em6 em6, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            em6.a(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: decrementUseCount");
    }

    @DexIgnore
    public final void a(boolean z) {
        this.a -= b(z);
        if (this.a <= 0) {
            if (nl6.a()) {
                if (!(this.a == 0)) {
                    throw new AssertionError();
                }
            }
            if (this.b) {
                shutdown();
            }
        }
    }
}
