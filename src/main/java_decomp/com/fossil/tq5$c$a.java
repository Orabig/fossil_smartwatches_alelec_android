package com.fossil;

import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1$1", f = "OnboardingHeightWeightPresenter.kt", l = {123}, m = "invokeSuspend")
public final class tq5$c$a extends sf6 implements ig6<il6, xe6<? super ap4<MFUser>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ OnboardingHeightWeightPresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tq5$c$a(OnboardingHeightWeightPresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        tq5$c$a tq5_c_a = new tq5$c$a(this.this$0, xe6);
        tq5_c_a.p$ = (il6) obj;
        return tq5_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((tq5$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            UserRepository c = this.this$0.this$0.g;
            MFUser mFUser = this.this$0.$user;
            this.L$0 = il6;
            this.label = 1;
            obj = c.updateUser(mFUser, true, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
