package com.fossil;

import com.portfolio.platform.data.model.setting.CommuteTimeSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y45 extends j24 {
    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, String str2, nh4 nh4, boolean z, String str3);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract CommuteTimeSetting i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public abstract void l();

    @DexIgnore
    public abstract void m();
}
