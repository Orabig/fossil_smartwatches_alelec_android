package com.fossil;

import com.fossil.js6;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ns6 implements Closeable {
    @DexIgnore
    public static /* final */ Logger e; // = Logger.getLogger(ks6.class.getName());
    @DexIgnore
    public /* final */ lt6 a;
    @DexIgnore
    public /* final */ a b; // = new a(this.a);
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ js6.a d; // = new js6.a(4096, this.b);

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(int i, int i2, int i3, boolean z);

        @DexIgnore
        void a(int i, int i2, List<is6> list) throws IOException;

        @DexIgnore
        void a(int i, long j);

        @DexIgnore
        void a(int i, hs6 hs6);

        @DexIgnore
        void a(int i, hs6 hs6, mt6 mt6);

        @DexIgnore
        void a(boolean z, int i, int i2);

        @DexIgnore
        void a(boolean z, int i, int i2, List<is6> list);

        @DexIgnore
        void a(boolean z, int i, lt6 lt6, int i2) throws IOException;

        @DexIgnore
        void a(boolean z, ss6 ss6);
    }

    @DexIgnore
    public ns6(lt6 lt6, boolean z) {
        this.a = lt6;
        this.c = z;
    }

    @DexIgnore
    public void a(b bVar) throws IOException {
        if (!this.c) {
            mt6 e2 = this.a.e((long) ks6.a.size());
            if (e.isLoggable(Level.FINE)) {
                e.fine(fr6.a("<< CONNECTION %s", e2.hex()));
            }
            if (!ks6.a.equals(e2)) {
                ks6.b("Expected a connection header but was %s", e2.utf8());
                throw null;
            }
        } else if (!a(true, bVar)) {
            ks6.b("Required SETTINGS preface not received", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void b(b bVar, int i, byte b2, int i2) throws IOException {
        if (i < 8) {
            ks6.b("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.a.readInt();
            int readInt2 = this.a.readInt();
            int i3 = i - 8;
            hs6 fromHttp2 = hs6.fromHttp2(readInt2);
            if (fromHttp2 != null) {
                mt6 mt6 = mt6.EMPTY;
                if (i3 > 0) {
                    mt6 = this.a.e((long) i3);
                }
                bVar.a(readInt, fromHttp2, mt6);
                return;
            }
            ks6.b("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(readInt2));
            throw null;
        } else {
            ks6.b("TYPE_GOAWAY streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void c(b bVar, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            boolean z = (b2 & 1) != 0;
            if ((b2 & 8) != 0) {
                s = (short) (this.a.readByte() & 255);
            }
            if ((b2 & 32) != 0) {
                a(bVar, i2);
                i -= 5;
            }
            bVar.a(z, i2, -1, a(a(i, b2, s), s, b2, i2));
            return;
        }
        ks6.b("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public final void d(b bVar, int i, byte b2, int i2) throws IOException {
        boolean z = false;
        if (i != 8) {
            ks6.b("TYPE_PING length != 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.a.readInt();
            int readInt2 = this.a.readInt();
            if ((b2 & 1) != 0) {
                z = true;
            }
            bVar.a(z, readInt, readInt2);
        } else {
            ks6.b("TYPE_PING streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void e(b bVar, int i, byte b2, int i2) throws IOException {
        if (i != 5) {
            ks6.b("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            a(bVar, i2);
        } else {
            ks6.b("TYPE_PRIORITY streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void f(b bVar, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            if ((b2 & 8) != 0) {
                s = (short) (this.a.readByte() & 255);
            }
            bVar.a(i2, this.a.readInt() & Integer.MAX_VALUE, a(a(i - 4, b2, s), s, b2, i2));
            return;
        }
        ks6.b("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void g(b bVar, int i, byte b2, int i2) throws IOException {
        if (i != 4) {
            ks6.b("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            int readInt = this.a.readInt();
            hs6 fromHttp2 = hs6.fromHttp2(readInt);
            if (fromHttp2 != null) {
                bVar.a(i2, fromHttp2);
                return;
            }
            ks6.b("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(readInt));
            throw null;
        } else {
            ks6.b("TYPE_RST_STREAM streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void h(b bVar, int i, byte b2, int i2) throws IOException {
        if (i2 != 0) {
            ks6.b("TYPE_SETTINGS streamId != 0", new Object[0]);
            throw null;
        } else if ((b2 & 1) != 0) {
            if (i == 0) {
                bVar.a();
            } else {
                ks6.b("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                throw null;
            }
        } else if (i % 6 == 0) {
            ss6 ss6 = new ss6();
            for (int i3 = 0; i3 < i; i3 += 6) {
                short readShort = this.a.readShort() & 65535;
                int readInt = this.a.readInt();
                switch (readShort) {
                    case 2:
                        if (!(readInt == 0 || readInt == 1)) {
                            ks6.b("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                            throw null;
                        }
                    case 3:
                        readShort = 4;
                        break;
                    case 4:
                        readShort = 7;
                        if (readInt >= 0) {
                            break;
                        } else {
                            ks6.b("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                            throw null;
                        }
                    case 5:
                        if (readInt >= 16384 && readInt <= 16777215) {
                            break;
                        } else {
                            ks6.b("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(readInt));
                            throw null;
                        }
                        break;
                }
                ss6.a(readShort, readInt);
            }
            bVar.a(false, ss6);
        } else {
            ks6.b("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
            throw null;
        }
    }

    @DexIgnore
    public final void i(b bVar, int i, byte b2, int i2) throws IOException {
        if (i == 4) {
            long readInt = ((long) this.a.readInt()) & 2147483647L;
            if (readInt != 0) {
                bVar.a(i2, readInt);
                return;
            }
            ks6.b("windowSizeIncrement was 0", Long.valueOf(readInt));
            throw null;
        }
        ks6.b("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
        throw null;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements zt6 {
        @DexIgnore
        public /* final */ lt6 a;
        @DexIgnore
        public int b;
        @DexIgnore
        public byte c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public short f;

        @DexIgnore
        public a(lt6 lt6) {
            this.a = lt6;
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            while (true) {
                int i = this.e;
                if (i == 0) {
                    this.a.skip((long) this.f);
                    this.f = 0;
                    if ((this.c & 4) != 0) {
                        return -1;
                    }
                    c();
                } else {
                    long b2 = this.a.b(jt6, Math.min(j, (long) i));
                    if (b2 == -1) {
                        return -1;
                    }
                    this.e = (int) (((long) this.e) - b2);
                    return b2;
                }
            }
        }

        @DexIgnore
        public final void c() throws IOException {
            int i = this.d;
            int a2 = ns6.a(this.a);
            this.e = a2;
            this.b = a2;
            byte readByte = (byte) (this.a.readByte() & 255);
            this.c = (byte) (this.a.readByte() & 255);
            if (ns6.e.isLoggable(Level.FINE)) {
                ns6.e.fine(ks6.a(true, this.d, this.b, readByte, this.c));
            }
            this.d = this.a.readInt() & Integer.MAX_VALUE;
            if (readByte != 9) {
                ks6.b("%s != TYPE_CONTINUATION", Byte.valueOf(readByte));
                throw null;
            } else if (this.d != i) {
                ks6.b("TYPE_CONTINUATION streamId changed", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        public void close() throws IOException {
        }

        @DexIgnore
        public au6 b() {
            return this.a.b();
        }
    }

    @DexIgnore
    public boolean a(boolean z, b bVar) throws IOException {
        try {
            this.a.i(9);
            int a2 = a(this.a);
            if (a2 < 0 || a2 > 16384) {
                ks6.b("FRAME_SIZE_ERROR: %s", Integer.valueOf(a2));
                throw null;
            }
            byte readByte = (byte) (this.a.readByte() & 255);
            if (!z || readByte == 4) {
                byte readByte2 = (byte) (this.a.readByte() & 255);
                int readInt = this.a.readInt() & Integer.MAX_VALUE;
                if (e.isLoggable(Level.FINE)) {
                    e.fine(ks6.a(true, readInt, a2, readByte, readByte2));
                }
                switch (readByte) {
                    case 0:
                        a(bVar, a2, readByte2, readInt);
                        break;
                    case 1:
                        c(bVar, a2, readByte2, readInt);
                        break;
                    case 2:
                        e(bVar, a2, readByte2, readInt);
                        break;
                    case 3:
                        g(bVar, a2, readByte2, readInt);
                        break;
                    case 4:
                        h(bVar, a2, readByte2, readInt);
                        break;
                    case 5:
                        f(bVar, a2, readByte2, readInt);
                        break;
                    case 6:
                        d(bVar, a2, readByte2, readInt);
                        break;
                    case 7:
                        b(bVar, a2, readByte2, readInt);
                        break;
                    case 8:
                        i(bVar, a2, readByte2, readInt);
                        break;
                    default:
                        this.a.skip((long) a2);
                        break;
                }
                return true;
            }
            ks6.b("Expected a SETTINGS frame but was %s", Byte.valueOf(readByte));
            throw null;
        } catch (IOException unused) {
            return false;
        }
    }

    @DexIgnore
    public final List<is6> a(int i, short s, byte b2, int i2) throws IOException {
        a aVar = this.b;
        aVar.e = i;
        aVar.b = i;
        aVar.f = s;
        aVar.c = b2;
        aVar.d = i2;
        this.d.f();
        return this.d.c();
    }

    @DexIgnore
    public final void a(b bVar, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            boolean z = true;
            boolean z2 = (b2 & 1) != 0;
            if ((b2 & 32) == 0) {
                z = false;
            }
            if (!z) {
                if ((b2 & 8) != 0) {
                    s = (short) (this.a.readByte() & 255);
                }
                bVar.a(z2, i2, this.a, a(i, b2, s));
                this.a.skip((long) s);
                return;
            }
            ks6.b("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            throw null;
        }
        ks6.b("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void a(b bVar, int i) throws IOException {
        int readInt = this.a.readInt();
        bVar.a(i, readInt & Integer.MAX_VALUE, (this.a.readByte() & 255) + 1, (Integer.MIN_VALUE & readInt) != 0);
    }

    @DexIgnore
    public static int a(lt6 lt6) throws IOException {
        return (lt6.readByte() & 255) | ((lt6.readByte() & 255) << DateTimeFieldType.CLOCKHOUR_OF_DAY) | ((lt6.readByte() & 255) << 8);
    }

    @DexIgnore
    public static int a(int i, byte b2, short s) throws IOException {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        ks6.b("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
        throw null;
    }
}
