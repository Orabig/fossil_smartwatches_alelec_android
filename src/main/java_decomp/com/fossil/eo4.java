package com.fossil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eo4 extends BaseDbProvider implements do4 {
    @DexIgnore
    public static /* final */ String a; // = "eo4";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements UpgradeCommand {
        @DexIgnore
        public a(eo4 eo4) {
        }

        @DexIgnore
        public void execute(SQLiteDatabase sQLiteDatabase) {
            FLogger.INSTANCE.getLocal().d(eo4.a, " ---- UPGRADE DB phone_favorites_contact, rename table uappsystemversion to PhoneFavoritesContact");
            sQLiteDatabase.execSQL("ALTER TABLE uappsystemversion RENAME TO PhoneFavoritesContact");
            FLogger.INSTANCE.getLocal().d(eo4.a, " ---- UPGRADE DB phone_favorites_contact, rename table uappsystemversion to PhoneFavoritesContact SUCCESS");
        }
    }

    @DexIgnore
    public eo4(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public void a(PhoneFavoritesContact phoneFavoritesContact) {
        if (phoneFavoritesContact != null) {
            try {
                g().createOrUpdate(phoneFavoritesContact);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.d(str, "addOrUpdatePhoneFavoritesContact - number= " + phoneFavoritesContact.getPhoneNumber());
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = a;
                local2.e(str2, "addOrUpdatePhoneFavoritesContact - e= " + e);
            }
        } else {
            FLogger.INSTANCE.getLocal().e(a, "addOrUpdatePhoneFavoritesContact - contact=NULL!!!");
        }
    }

    @DexIgnore
    public void e() {
        FLogger.INSTANCE.getLocal().d(a, "removeAllPhoneFavoritesContacts");
        try {
            g().deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.e(str, "removeAllPhoneFavoritesContacts - e=" + e);
        }
    }

    @DexIgnore
    public final Dao<PhoneFavoritesContact, Integer> g() throws SQLException {
        return this.databaseHelper.getDao(PhoneFavoritesContact.class);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{PhoneFavoritesContact.class};
    }

    @DexIgnore
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        HashMap hashMap = new HashMap();
        hashMap.put(2, new a(this));
        return hashMap;
    }

    @DexIgnore
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public void removePhoneFavoritesContact(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.d(str2, "removePhoneFavoritesContact - number=" + str);
        try {
            DeleteBuilder<PhoneFavoritesContact, Integer> deleteBuilder = g().deleteBuilder();
            deleteBuilder.where().eq(PhoneFavoritesContact.COLUMN_PHONE_NUMBER, str);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local2.e(str3, "removePhoneFavoritesContact - e=" + e);
        }
    }
}
