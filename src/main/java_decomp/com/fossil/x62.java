package com.fossil;

import android.content.Context;
import android.os.Build;
import com.fossil.rv1;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x62 {
    @DexIgnore
    @Deprecated
    public static /* final */ rv1<rv1.d.C0044d> a; // = gc2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ rv1<rv1.d.C0044d> b; // = ac2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ a72 c; // = new wd2();
    @DexIgnore
    @Deprecated
    public static /* final */ rv1<rv1.d.C0044d> d; // = we2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ w62 e; // = new rd2();

    /*
    static {
        rv1<rv1.d.C0044d> rv1 = lc2.G;
        new yd2();
        new xd2();
        rv1<rv1.d.C0044d> rv12 = pc2.G;
        new zd2();
        rv1<rv1.d.C0044d> rv13 = cf2.G;
        new ud2();
        rv1<rv1.d.C0044d> rv14 = se2.G;
        if (Build.VERSION.SDK_INT >= 18) {
            new qd2();
        } else {
            new ce2();
        }
        new Scope("https://www.googleapis.com/auth/fitness.activity.read");
        new Scope("https://www.googleapis.com/auth/fitness.activity.write");
        new Scope("https://www.googleapis.com/auth/fitness.location.read");
        new Scope("https://www.googleapis.com/auth/fitness.location.write");
        new Scope("https://www.googleapis.com/auth/fitness.body.read");
        new Scope("https://www.googleapis.com/auth/fitness.body.write");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.read");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.write");
    }
    */

    @DexIgnore
    public static e72 a(Context context, GoogleSignInAccount googleSignInAccount) {
        w12.a(googleSignInAccount);
        return new e72(context, new v82(context, googleSignInAccount));
    }
}
