package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class go2 implements oo2 {
    @DexIgnore
    public oo2[] a;

    @DexIgnore
    public go2(oo2... oo2Arr) {
        this.a = oo2Arr;
    }

    @DexIgnore
    public final boolean zza(Class<?> cls) {
        for (oo2 zza : this.a) {
            if (zza.zza(cls)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final po2 zzb(Class<?> cls) {
        for (oo2 oo2 : this.a) {
            if (oo2.zza(cls)) {
                return oo2.zzb(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
