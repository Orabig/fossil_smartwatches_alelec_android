package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q63 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ab3 a;
    @DexIgnore
    public /* final */ /* synthetic */ ra3 b;
    @DexIgnore
    public /* final */ /* synthetic */ d63 c;

    @DexIgnore
    public q63(d63 d63, ab3 ab3, ra3 ra3) {
        this.c = d63;
        this.a = ab3;
        this.b = ra3;
    }

    @DexIgnore
    public final void run() {
        this.c.a.t();
        if (this.a.c.zza() == null) {
            this.c.a.b(this.a, this.b);
        } else {
            this.c.a.a(this.a, this.b);
        }
    }
}
