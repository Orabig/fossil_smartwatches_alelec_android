package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hi2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ cs2 f;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hi2(ov2 ov2, String str, cs2 cs2) {
        super(ov2);
        this.g = ov2;
        this.e = str;
        this.f = cs2;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        this.g.g.getMaxUserProperties(this.e, this.f);
    }

    @DexIgnore
    public final void b() {
        this.f.d((Bundle) null);
    }
}
