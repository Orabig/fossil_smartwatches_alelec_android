package com.fossil;

import java.util.List;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class no6 {
    @DexIgnore
    public static final cn6 a(MainDispatcherFactory mainDispatcherFactory, List<? extends MainDispatcherFactory> list) {
        wg6.b(mainDispatcherFactory, "$this$tryCreateDispatcher");
        wg6.b(list, "factories");
        try {
            return mainDispatcherFactory.createDispatcher(list);
        } catch (Throwable th) {
            return new oo6(th, mainDispatcherFactory.hintOnError());
        }
    }
}
