package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h90 extends n90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ r70 c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<h90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new h90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new h90[i];
        }
    }

    @DexIgnore
    public h90(byte b, int i, r70 r70) {
        super(e90.APP_NOTIFICATION_CONTROL, b);
        this.c = r70;
        this.d = i;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.t0, (Object) this.c.a()), bm0.NOTIFICATION_UID, (Object) Integer.valueOf(this.d));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(h90.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            h90 h90 = (h90) obj;
            return !(wg6.a(this.c, h90.c) ^ true) && this.d == h90.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.AppNotificationControlNotification");
    }

    @DexIgnore
    public final r70 getAction() {
        return this.c;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return Integer.valueOf(this.d).hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ h90(Parcel parcel, qg6 qg6) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(r70.class.getClassLoader());
        if (readParcelable != null) {
            this.c = (r70) readParcelable;
            this.d = parcel.readInt();
            return;
        }
        wg6.a();
        throw null;
    }
}
