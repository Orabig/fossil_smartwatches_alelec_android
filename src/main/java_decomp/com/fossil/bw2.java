package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bw2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<bw2> CREATOR; // = new nw2();
    @DexIgnore
    public /* final */ List<LocationRequest> a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public lw2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ ArrayList<LocationRequest> a; // = new ArrayList<>();
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public final a a(LocationRequest locationRequest) {
            if (locationRequest != null) {
                this.a.add(locationRequest);
            }
            return this;
        }

        @DexIgnore
        public final bw2 a() {
            return new bw2(this.a, this.b, this.c, (lw2) null);
        }
    }

    @DexIgnore
    public bw2(List<LocationRequest> list, boolean z, boolean z2, lw2 lw2) {
        this.a = list;
        this.b = z;
        this.c = z2;
        this.d = lw2;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.c(parcel, 1, Collections.unmodifiableList(this.a), false);
        g22.a(parcel, 2, this.b);
        g22.a(parcel, 3, this.c);
        g22.a(parcel, 5, (Parcelable) this.d, i, false);
        g22.a(parcel, a2);
    }
}
