package com.fossil;

import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sa6 implements va6 {
    @DexIgnore
    public /* final */ l86 a;
    @DexIgnore
    public xa6 b;
    @DexIgnore
    public SSLSocketFactory c;
    @DexIgnore
    public boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[ta6.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[ta6.GET.ordinal()] = 1;
            a[ta6.POST.ordinal()] = 2;
            a[ta6.PUT.ordinal()] = 3;
            try {
                a[ta6.DELETE.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public sa6() {
        this(new b86());
    }

    @DexIgnore
    public void a(xa6 xa6) {
        if (this.b != xa6) {
            this.b = xa6;
            c();
        }
    }

    @DexIgnore
    public final synchronized SSLSocketFactory b() {
        SSLSocketFactory a2;
        this.d = true;
        try {
            a2 = wa6.a(this.b);
            this.a.d("Fabric", "Custom SSL pinning enabled");
        } catch (Exception e) {
            this.a.e("Fabric", "Exception while validating pinned certs", e);
            return null;
        }
        return a2;
    }

    @DexIgnore
    public final synchronized void c() {
        this.d = false;
        this.c = null;
    }

    @DexIgnore
    public sa6(l86 l86) {
        this.a = l86;
    }

    @DexIgnore
    public ua6 a(ta6 ta6, String str, Map<String, String> map) {
        ua6 ua6;
        SSLSocketFactory a2;
        int i = a.a[ta6.ordinal()];
        if (i == 1) {
            ua6 = ua6.a((CharSequence) str, (Map<?, ?>) map, true);
        } else if (i == 2) {
            ua6 = ua6.b((CharSequence) str, (Map<?, ?>) map, true);
        } else if (i == 3) {
            ua6 = ua6.f((CharSequence) str);
        } else if (i == 4) {
            ua6 = ua6.b((CharSequence) str);
        } else {
            throw new IllegalArgumentException("Unsupported HTTP method!");
        }
        if (!(!a(str) || this.b == null || (a2 = a()) == null)) {
            ((HttpsURLConnection) ua6.l()).setSSLSocketFactory(a2);
        }
        return ua6;
    }

    @DexIgnore
    public final boolean a(String str) {
        return str != null && str.toLowerCase(Locale.US).startsWith("https");
    }

    @DexIgnore
    public final synchronized SSLSocketFactory a() {
        if (this.c == null && !this.d) {
            this.c = b();
        }
        return this.c;
    }
}
