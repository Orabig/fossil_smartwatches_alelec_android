package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n70 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ l70 a;
    @DexIgnore
    public /* final */ m70 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<n70> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                l70 valueOf = l70.valueOf(readString);
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    return new n70(valueOf, m70.valueOf(readString2));
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new n70[i];
        }
    }

    @DexIgnore
    public n70(l70 l70, m70 m70) {
        this.a = l70;
        this.b = m70;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.t0, (Object) cw0.a((Enum<?>) this.a)), bm0.STATUS, (Object) cw0.a((Enum<?>) this.b));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(n70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            n70 n70 = (n70) obj;
            return this.a == n70.a && this.b == n70.b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.music.MusicEvent");
    }

    @DexIgnore
    public final l70 getAction() {
        return this.a;
    }

    @DexIgnore
    public final m70 getActionStatus() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }
}
