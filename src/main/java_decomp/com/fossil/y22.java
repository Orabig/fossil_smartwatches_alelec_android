package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y22 extends g12 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent a;
    @DexIgnore
    public /* final */ /* synthetic */ tw1 b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;

    @DexIgnore
    public y22(Intent intent, tw1 tw1, int i) {
        this.a = intent;
        this.b = tw1;
        this.c = i;
    }

    @DexIgnore
    public final void a() {
        Intent intent = this.a;
        if (intent != null) {
            this.b.startActivityForResult(intent, this.c);
        }
    }
}
