package com.fossil;

import android.content.Context;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i96 {
    @DexIgnore
    public String a(Context context) {
        int a = z86.a(context, "google_app_id", "string");
        if (a == 0) {
            return null;
        }
        c86.g().d("Fabric", "Generating Crashlytics ApiKey from google_app_id in Strings");
        return a(context.getResources().getString(a));
    }

    @DexIgnore
    public boolean b(Context context) {
        if (!TextUtils.isEmpty(new x86().b(context))) {
            return true;
        }
        return !TextUtils.isEmpty(new x86().c(context));
    }

    @DexIgnore
    public boolean c(Context context) {
        int a = z86.a(context, "google_app_id", "string");
        if (a == 0) {
            return false;
        }
        return !TextUtils.isEmpty(context.getResources().getString(a));
    }

    @DexIgnore
    public boolean d(Context context) {
        int a = z86.a(context, "io.fabric.auto_initialize", "bool");
        if (a == 0) {
            return false;
        }
        boolean z = context.getResources().getBoolean(a);
        if (z) {
            c86.g().d("Fabric", "Found Fabric auto-initialization flag for joint Firebase/Fabric customers");
        }
        return z;
    }

    @DexIgnore
    public boolean e(Context context) {
        if (z86.a(context, "com.crashlytics.useFirebaseAppId", false)) {
            return true;
        }
        if (!c(context) || b(context)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public String a(String str) {
        return z86.d(str).substring(0, 40);
    }
}
