package com.fossil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class iw6 implements xv6 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public volatile xv6 b;
    @DexIgnore
    public Boolean c;
    @DexIgnore
    public Method d;
    @DexIgnore
    public aw6 e;
    @DexIgnore
    public Queue<dw6> f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore
    public iw6(String str, Queue<dw6> queue, boolean z) {
        this.a = str;
        this.f = queue;
        this.g = z;
    }

    @DexIgnore
    public xv6 a() {
        if (this.b != null) {
            return this.b;
        }
        if (this.g) {
            return fw6.NOP_LOGGER;
        }
        return b();
    }

    @DexIgnore
    public final xv6 b() {
        if (this.e == null) {
            this.e = new aw6(this, this.f);
        }
        return this.e;
    }

    @DexIgnore
    public String c() {
        return this.a;
    }

    @DexIgnore
    public boolean d() {
        Boolean bool = this.c;
        if (bool != null) {
            return bool.booleanValue();
        }
        try {
            this.d = this.b.getClass().getMethod("log", new Class[]{cw6.class});
            this.c = Boolean.TRUE;
        } catch (NoSuchMethodException unused) {
            this.c = Boolean.FALSE;
        }
        return this.c.booleanValue();
    }

    @DexIgnore
    public void debug(String str) {
        a().debug(str);
    }

    @DexIgnore
    public boolean e() {
        return this.b instanceof fw6;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && iw6.class == obj.getClass() && this.a.equals(((iw6) obj).a);
    }

    @DexIgnore
    public void error(String str) {
        a().error(str);
    }

    @DexIgnore
    public boolean f() {
        return this.b == null;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void info(String str) {
        a().info(str);
    }

    @DexIgnore
    public boolean isDebugEnabled() {
        return a().isDebugEnabled();
    }

    @DexIgnore
    public boolean isErrorEnabled() {
        return a().isErrorEnabled();
    }

    @DexIgnore
    public boolean isInfoEnabled() {
        return a().isInfoEnabled();
    }

    @DexIgnore
    public boolean isTraceEnabled() {
        return a().isTraceEnabled();
    }

    @DexIgnore
    public boolean isWarnEnabled() {
        return a().isWarnEnabled();
    }

    @DexIgnore
    public void trace(String str) {
        a().trace(str);
    }

    @DexIgnore
    public void warn(String str) {
        a().warn(str);
    }

    @DexIgnore
    public void debug(String str, Throwable th) {
        a().debug(str, th);
    }

    @DexIgnore
    public void error(String str, Object... objArr) {
        a().error(str, objArr);
    }

    @DexIgnore
    public void info(String str, Object obj) {
        a().info(str, obj);
    }

    @DexIgnore
    public void trace(String str, Throwable th) {
        a().trace(str, th);
    }

    @DexIgnore
    public void warn(String str, Object obj, Object obj2) {
        a().warn(str, obj, obj2);
    }

    @DexIgnore
    public void error(String str, Throwable th) {
        a().error(str, th);
    }

    @DexIgnore
    public void info(String str, Throwable th) {
        a().info(str, th);
    }

    @DexIgnore
    public void warn(String str, Throwable th) {
        a().warn(str, th);
    }

    @DexIgnore
    public void a(xv6 xv6) {
        this.b = xv6;
    }

    @DexIgnore
    public void a(cw6 cw6) {
        if (d()) {
            try {
                this.d.invoke(this.b, new Object[]{cw6});
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException unused) {
            }
        }
    }
}
