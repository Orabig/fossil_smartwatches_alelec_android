package com.fossil;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import com.fossil.lh;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sh<T> extends LiveData<T> {
    @DexIgnore
    public /* final */ oh k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Callable<T> m;
    @DexIgnore
    public /* final */ kh n;
    @DexIgnore
    public /* final */ lh.c o;
    @DexIgnore
    public /* final */ AtomicBoolean p; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean q; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean r; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable s; // = new a();
    @DexIgnore
    public /* final */ Runnable t; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x005d  */
        /* JADX WARNING: Removed duplicated region for block: B:5:0x0025  */
        public void run() {
            boolean z;
            if (sh.this.r.compareAndSet(false, true)) {
                sh.this.k.getInvalidationTracker().b(sh.this.o);
            }
            do {
                if (!sh.this.q.compareAndSet(false, true)) {
                    Object obj = null;
                    z = false;
                    while (sh.this.p.compareAndSet(true, false)) {
                        try {
                            obj = sh.this.m.call();
                            z = true;
                        } catch (Exception e) {
                            throw new RuntimeException("Exception while computing database live data.", e);
                        } catch (Throwable th) {
                            sh.this.q.set(false);
                            throw th;
                        }
                    }
                    if (z) {
                        sh.this.a(obj);
                    }
                    sh.this.q.set(false);
                } else {
                    z = false;
                }
                if (!z || !sh.this.p.get()) {
                }
                if (!sh.this.q.compareAndSet(false, true)) {
                }
                return;
            } while (!sh.this.p.get());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            boolean c = sh.this.c();
            if (sh.this.p.compareAndSet(false, true) && c) {
                sh.this.f().execute(sh.this.s);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends lh.c {
        @DexIgnore
        public c(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            q3.c().b(sh.this.t);
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public sh(oh ohVar, kh khVar, boolean z, Callable<T> callable, String[] strArr) {
        this.k = ohVar;
        this.l = z;
        this.m = callable;
        this.n = khVar;
        this.o = new c(strArr);
    }

    @DexIgnore
    public void d() {
        super.d();
        this.n.a(this);
        f().execute(this.s);
    }

    @DexIgnore
    public void e() {
        super.e();
        this.n.b(this);
    }

    @DexIgnore
    public Executor f() {
        if (this.l) {
            return this.k.getTransactionExecutor();
        }
        return this.k.getQueryExecutor();
    }
}
