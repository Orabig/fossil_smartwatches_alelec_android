package com.fossil;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ei6<R> extends di6 {
    @DexIgnore
    R call(Object... objArr);

    @DexIgnore
    R callBy(Map<Object, ? extends Object> map);

    @DexIgnore
    List<Object> getParameters();

    @DexIgnore
    oi6 getReturnType();

    @DexIgnore
    List<Object> getTypeParameters();

    @DexIgnore
    pi6 getVisibility();

    @DexIgnore
    boolean isAbstract();

    @DexIgnore
    boolean isFinal();

    @DexIgnore
    boolean isOpen();

    @DexIgnore
    boolean isSuspend();
}
