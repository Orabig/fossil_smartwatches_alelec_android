package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b4 {
    @DexIgnore
    public /* final */ g a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends x3 {
        @DexIgnore
        public a(b4 b4Var) {
        }
    }

    @DexIgnore
    public b4(g gVar) {
        this.a = gVar;
        new a(this);
    }

    @DexIgnore
    public IBinder a() {
        return this.a.asBinder();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof b4)) {
            return false;
        }
        return ((b4) obj).a().equals(this.a.asBinder());
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }
}
