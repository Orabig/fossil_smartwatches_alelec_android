package com.fossil;

import android.content.Context;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j46 implements Runnable {
    @DexIgnore
    public Context a; // = null;
    @DexIgnore
    public Map<String, Integer> b; // = null;
    @DexIgnore
    public r36 c; // = null;

    @DexIgnore
    public j46(Context context, Map<String, Integer> map, r36 r36) {
        this.a = context;
        this.c = r36;
        if (map != null) {
            this.b = map;
        }
    }

    @DexIgnore
    public final m36 a(String str, int i) {
        int i2;
        m36 m36 = new m36();
        Socket socket = new Socket();
        try {
            m36.a(str);
            m36.a(i);
            long currentTimeMillis = System.currentTimeMillis();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            socket.connect(inetSocketAddress, 30000);
            m36.a(System.currentTimeMillis() - currentTimeMillis);
            m36.b(inetSocketAddress.getAddress().getHostAddress());
            socket.close();
            try {
                socket.close();
            } catch (Throwable th) {
                q36.m.a(th);
            }
            i2 = 0;
        } catch (IOException e) {
            try {
                q36.m.a((Throwable) e);
                socket.close();
            } catch (Throwable th2) {
                q36.m.a(th2);
            }
        } catch (Throwable th3) {
            q36.m.a(th3);
        }
        m36.b(i2);
        return m36;
        i2 = -1;
        m36.b(i2);
        return m36;
        throw th;
    }

    @DexIgnore
    public final Map<String, Integer> a() {
        String str;
        HashMap hashMap = new HashMap();
        String a2 = n36.a("__MTA_TEST_SPEED__", (String) null);
        if (!(a2 == null || a2.trim().length() == 0)) {
            for (String split : a2.split(";")) {
                String[] split2 = split.split(",");
                if (!(split2 == null || split2.length != 2 || (str = split2[0]) == null || str.trim().length() == 0)) {
                    try {
                        hashMap.put(str, Integer.valueOf(Integer.valueOf(split2[1]).intValue()));
                    } catch (NumberFormatException e) {
                        q36.m.a((Throwable) e);
                    }
                }
            }
        }
        return hashMap;
    }

    @DexIgnore
    public void run() {
        b56 f;
        String str;
        try {
            if (this.b == null) {
                this.b = a();
            }
            if (this.b != null) {
                if (this.b.size() != 0) {
                    JSONArray jSONArray = new JSONArray();
                    for (Map.Entry next : this.b.entrySet()) {
                        String str2 = (String) next.getKey();
                        if (str2 != null) {
                            if (str2.length() != 0) {
                                if (((Integer) next.getValue()) == null) {
                                    f = q36.m;
                                    str = "port is null for " + str2;
                                    f.g(str);
                                } else {
                                    jSONArray.put(a((String) next.getKey(), ((Integer) next.getValue()).intValue()).a());
                                }
                            }
                        }
                        f = q36.m;
                        str = "empty domain name.";
                        f.g(str);
                    }
                    if (jSONArray.length() != 0) {
                        y36 y36 = new y36(this.a, q36.a(this.a, false, this.c), this.c);
                        y36.a(jSONArray.toString());
                        new k46(y36).a();
                        return;
                    }
                    return;
                }
            }
            q36.m.e("empty domain list.");
        } catch (Throwable th) {
            q36.m.a(th);
        }
    }
}
