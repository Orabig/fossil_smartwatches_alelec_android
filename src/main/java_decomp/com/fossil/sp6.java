package com.fossil;

import com.j256.ormlite.logger.Logger;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sp6 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b;
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater c;
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater d;
    @DexIgnore
    public /* final */ AtomicReferenceArray<mp6> a; // = new AtomicReferenceArray<>(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
    @DexIgnore
    public volatile int consumerIndex; // = 0;
    @DexIgnore
    public volatile Object lastScheduledTask; // = null;
    @DexIgnore
    public volatile int producerIndex; // = 0;

    /*
    static {
        Class<sp6> cls = sp6.class;
        b = AtomicReferenceFieldUpdater.newUpdater(cls, Object.class, "lastScheduledTask");
        c = AtomicIntegerFieldUpdater.newUpdater(cls, "producerIndex");
        d = AtomicIntegerFieldUpdater.newUpdater(cls, "consumerIndex");
    }
    */

    @DexIgnore
    public final mp6 b() {
        mp6 mp6 = (mp6) b.getAndSet(this, (Object) null);
        if (mp6 != null) {
            return mp6;
        }
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                return null;
            }
            int i2 = i & 127;
            if (((mp6) this.a.get(i2)) != null && d.compareAndSet(this, i, i + 1)) {
                return (mp6) this.a.getAndSet(i2, (Object) null);
            }
        }
    }

    @DexIgnore
    public final int c() {
        return this.lastScheduledTask != null ? a() + 1 : a();
    }

    @DexIgnore
    public final int a() {
        return this.producerIndex - this.consumerIndex;
    }

    @DexIgnore
    public final boolean a(mp6 mp6, ip6 ip6) {
        wg6.b(mp6, "task");
        wg6.b(ip6, "globalQueue");
        mp6 mp62 = (mp6) b.getAndSet(this, mp6);
        if (mp62 != null) {
            return b(mp62, ip6);
        }
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v2, resolved type: com.fossil.mp6} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final boolean a(sp6 sp6, ip6 ip6) {
        mp6 mp6;
        sp6 sp62 = sp6;
        ip6 ip62 = ip6;
        wg6.b(sp62, "victim");
        wg6.b(ip62, "globalQueue");
        long a2 = qp6.f.a();
        int a3 = sp6.a();
        if (a3 == 0) {
            return a(a2, sp62, ip62);
        }
        int a4 = ci6.a(a3 / 2, 1);
        int i = 0;
        boolean z = false;
        while (i < a4) {
            while (true) {
                int i2 = sp62.consumerIndex;
                mp6 = null;
                if (i2 - sp62.producerIndex != 0) {
                    int i3 = i2 & 127;
                    mp6 mp62 = (mp6) sp6.a.get(i3);
                    if (mp62 != null) {
                        if (!(a2 - mp62.a >= qp6.a || sp6.a() > qp6.b)) {
                            break;
                        } else if (d.compareAndSet(sp62, i2, i2 + 1)) {
                            mp6 = sp6.a.getAndSet(i3, (Object) null);
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
            if (mp6 == null) {
                break;
            }
            a(mp6, ip62);
            i++;
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final boolean b(mp6 mp6, ip6 ip6) {
        wg6.b(mp6, "task");
        wg6.b(ip6, "globalQueue");
        boolean z = true;
        while (!a(mp6)) {
            b(ip6);
            z = false;
        }
        return z;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: com.fossil.mp6} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void b(ip6 ip6) {
        mp6 mp6;
        int a2 = ci6.a(a() / 2, 1);
        int i = 0;
        while (i < a2) {
            while (true) {
                int i2 = this.consumerIndex;
                mp6 = null;
                if (i2 - this.producerIndex != 0) {
                    int i3 = i2 & 127;
                    if (((mp6) this.a.get(i3)) != null && d.compareAndSet(this, i2, i2 + 1)) {
                        mp6 = this.a.getAndSet(i3, (Object) null);
                        break;
                    }
                } else {
                    break;
                }
            }
            if (mp6 != null) {
                a(ip6, mp6);
                i++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final boolean a(long j, sp6 sp6, ip6 ip6) {
        mp6 mp6 = (mp6) sp6.lastScheduledTask;
        if (mp6 == null || j - mp6.a < qp6.a || !b.compareAndSet(sp6, mp6, (Object) null)) {
            return false;
        }
        a(mp6, ip6);
        return true;
    }

    @DexIgnore
    public final void a(ip6 ip6, mp6 mp6) {
        if (!ip6.a(mp6)) {
            throw new IllegalStateException("GlobalQueue could not be closed yet".toString());
        }
    }

    @DexIgnore
    public final void a(ip6 ip6) {
        mp6 mp6;
        wg6.b(ip6, "globalQueue");
        mp6 mp62 = (mp6) b.getAndSet(this, (Object) null);
        if (mp62 != null) {
            a(ip6, mp62);
        }
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                mp6 = null;
            } else {
                int i2 = i & 127;
                if (((mp6) this.a.get(i2)) != null && d.compareAndSet(this, i, i + 1)) {
                    mp6 = (mp6) this.a.getAndSet(i2, (Object) null);
                }
            }
            if (mp6 != null) {
                a(ip6, mp6);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final boolean a(mp6 mp6) {
        if (a() == 127) {
            return false;
        }
        int i = this.producerIndex & 127;
        if (this.a.get(i) != null) {
            return false;
        }
        this.a.lazySet(i, mp6);
        c.incrementAndGet(this);
        return true;
    }
}
