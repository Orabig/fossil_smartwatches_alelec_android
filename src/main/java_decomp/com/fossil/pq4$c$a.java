package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1$1", f = "WatchAppCommuteTimeManager.kt", l = {147}, m = "invokeSuspend")
public final class pq4$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppCommuteTimeManager.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pq4$c$a(WatchAppCommuteTimeManager.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pq4$c$a pq4_c_a = new pq4$c$a(this.this$0, xe6);
        pq4_c_a.p$ = (il6) obj;
        return pq4_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pq4$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        T t;
        Object a = ff6.a();
        int i = this.label;
        String str = null;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            DianaPresetRepository b = this.this$0.this$0.b();
            String c = this.this$0.this$0.j;
            if (c != null) {
                DianaPreset activePresetBySerial = b.getActivePresetBySerial(c);
                if (activePresetBySerial == null) {
                    return null;
                }
                Iterator<T> it = activePresetBySerial.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (hf6.a(wg6.a((Object) ((DianaPresetWatchAppSetting) t).getId(), (Object) "commute-time")).booleanValue()) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp activePreset " + activePresetBySerial);
                if (dianaPresetWatchAppSetting == null) {
                    return null;
                }
                FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp json=" + dianaPresetWatchAppSetting.getSettings());
                if (!vi4.a(dianaPresetWatchAppSetting.getSettings())) {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) this.this$0.this$0.i.a(dianaPresetWatchAppSetting.getSettings(), CommuteTimeWatchAppSetting.class);
                    AddressWrapper addressByName = commuteTimeWatchAppSetting.getAddressByName(this.this$0.$destinationAlias);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getCommuteTimeForWatchApp destinationAlias ");
                    sb.append(this.this$0.$destinationAlias);
                    sb.append(" destinationAddress ");
                    sb.append(addressByName != null ? addressByName.getAddress() : null);
                    local.d("WatchAppCommuteTimeManager", sb.toString());
                    JSONObject jSONObject = new JSONObject();
                    String id = addressByName != null ? addressByName.getId() : null;
                    AddressWrapper a2 = this.this$0.this$0.o;
                    if (wg6.a((Object) id, (Object) a2 != null ? a2.getId() : null)) {
                        AddressWrapper a3 = this.this$0.this$0.o;
                        jSONObject.put("des", a3 != null ? a3.getId() : null);
                        jSONObject.put("type", "refresh");
                    } else {
                        AddressWrapper a4 = this.this$0.this$0.o;
                        jSONObject.put("des", a4 != null ? a4.getId() : null);
                        jSONObject.put("type", "start");
                        FLogger.INSTANCE.getRemote().startSession(FLogger.Session.DIANA_COMMUTE_TIME, this.this$0.$serial, "WatchAppCommuteTimeManager");
                    }
                    this.this$0.this$0.o = addressByName;
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("getDurationTimeBaseOnLocation currentDestinationAddress ");
                    AddressWrapper a5 = this.this$0.this$0.o;
                    if (a5 != null) {
                        str = a5.getAddress();
                    }
                    sb2.append(str);
                    local2.d("WatchAppCommuteTimeManager", sb2.toString());
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                    String str2 = this.this$0.$serial;
                    String jSONObject2 = jSONObject.toString();
                    wg6.a((Object) jSONObject2, "jsonObject.toString()");
                    remote.i(component, session, str2, "WatchAppCommuteTimeManager", jSONObject2);
                    WatchAppCommuteTimeManager watchAppCommuteTimeManager = this.this$0.this$0;
                    this.L$0 = il6;
                    this.L$1 = activePresetBySerial;
                    this.L$2 = activePresetBySerial;
                    this.L$3 = dianaPresetWatchAppSetting;
                    this.L$4 = commuteTimeWatchAppSetting;
                    this.L$5 = addressByName;
                    this.L$6 = jSONObject;
                    this.L$7 = dianaPresetWatchAppSetting;
                    this.label = 1;
                    if (watchAppCommuteTimeManager.a((xe6<? super cd6>) this) == a) {
                        return a;
                    }
                }
            } else {
                wg6.a();
                throw null;
            }
        } else if (i == 1) {
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) this.L$7;
            JSONObject jSONObject3 = (JSONObject) this.L$6;
            AddressWrapper addressWrapper = (AddressWrapper) this.L$5;
            CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = (CommuteTimeWatchAppSetting) this.L$4;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) this.L$3;
            DianaPreset dianaPreset = (DianaPreset) this.L$2;
            DianaPreset dianaPreset2 = (DianaPreset) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
