package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k53 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public String c;
    @DexIgnore
    public /* final */ /* synthetic */ h53 d;

    @DexIgnore
    public k53(h53 h53, String str, String str2) {
        this.d = h53;
        w12.b(str);
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        if (!this.b) {
            this.b = true;
            this.c = this.d.A().getString(this.a, (String) null);
        }
        return this.c;
    }

    @DexIgnore
    public final void a(String str) {
        if (!ma3.d(str, this.c)) {
            SharedPreferences.Editor edit = this.d.A().edit();
            edit.putString(this.a, str);
            edit.apply();
            this.c = str;
        }
    }
}
