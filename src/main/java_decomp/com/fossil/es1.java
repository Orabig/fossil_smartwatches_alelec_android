package com.fossil;

import android.database.Cursor;
import com.fossil.rs1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class es1 implements rs1.b {
    @DexIgnore
    public /* final */ List a;
    @DexIgnore
    public /* final */ rp1 b;

    @DexIgnore
    public es1(List list, rp1 rp1) {
        this.a = list;
        this.b = rp1;
    }

    @DexIgnore
    public static rs1.b a(List list, rp1 rp1) {
        return new es1(list, rp1);
    }

    @DexIgnore
    public Object apply(Object obj) {
        return rs1.a(this.a, this.b, (Cursor) obj);
    }
}
