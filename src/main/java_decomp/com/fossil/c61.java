package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c61 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ w91 a;
    @DexIgnore
    public /* final */ /* synthetic */ qv0 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c61(w91 w91, qv0 qv0) {
        super(1);
        this.a = w91;
        this.b = qv0;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        qv0 qv0 = (qv0) obj;
        qv0 qv02 = this.b;
        if ((qv02 instanceof ol0) || qv02.v.c == il0.INTERRUPTED) {
            this.a.a.a(this.b.v);
        } else {
            this.a.b.invoke();
        }
        return cd6.a;
    }
}
