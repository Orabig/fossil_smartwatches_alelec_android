package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka3 implements Parcelable.Creator<la3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        String str = null;
        Long l = null;
        Float f = null;
        String str2 = null;
        String str3 = null;
        Double d = null;
        long j = 0;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    i = f22.q(parcel2, a);
                    break;
                case 2:
                    str = f22.e(parcel2, a);
                    break;
                case 3:
                    j = f22.s(parcel2, a);
                    break;
                case 4:
                    l = f22.t(parcel2, a);
                    break;
                case 5:
                    f = f22.o(parcel2, a);
                    break;
                case 6:
                    str2 = f22.e(parcel2, a);
                    break;
                case 7:
                    str3 = f22.e(parcel2, a);
                    break;
                case 8:
                    d = f22.m(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new la3(i, str, j, l, f, str2, str3, d);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new la3[i];
    }
}
