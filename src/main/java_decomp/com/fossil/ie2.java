package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ie2 extends yb2 implements ge2 {
    @DexIgnore
    public ie2() {
        super("com.google.android.gms.fitness.internal.service.IFitnessSensorService");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            a((de2) sd2.a(parcel, de2.CREATOR), yc2.a(parcel.readStrongBinder()));
        } else if (i == 2) {
            a((r82) sd2.a(parcel, r82.CREATOR), pd2.a(parcel.readStrongBinder()));
        } else if (i != 3) {
            return false;
        } else {
            a((ee2) sd2.a(parcel, ee2.CREATOR), pd2.a(parcel.readStrongBinder()));
        }
        return true;
    }
}
