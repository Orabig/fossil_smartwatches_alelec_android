package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s4<E> implements Cloneable {
    @DexIgnore
    public static /* final */ Object e; // = new Object();
    @DexIgnore
    public boolean a;
    @DexIgnore
    public long[] b;
    @DexIgnore
    public Object[] c;
    @DexIgnore
    public int d;

    @DexIgnore
    public s4() {
        this(10);
    }

    @DexIgnore
    @Deprecated
    public void a(long j) {
        d(j);
    }

    @DexIgnore
    public E b(long j) {
        return b(j, (Object) null);
    }

    @DexIgnore
    public void c(long j, E e2) {
        int a2 = r4.a(this.b, this.d, j);
        if (a2 >= 0) {
            this.c[a2] = e2;
            return;
        }
        int i = ~a2;
        if (i < this.d) {
            Object[] objArr = this.c;
            if (objArr[i] == e) {
                this.b[i] = j;
                objArr[i] = e2;
                return;
            }
        }
        if (this.a && this.d >= this.b.length) {
            b();
            i = ~r4.a(this.b, this.d, j);
        }
        int i2 = this.d;
        if (i2 >= this.b.length) {
            int c2 = r4.c(i2 + 1);
            long[] jArr = new long[c2];
            Object[] objArr2 = new Object[c2];
            long[] jArr2 = this.b;
            System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            Object[] objArr3 = this.c;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.b = jArr;
            this.c = objArr2;
        }
        int i3 = this.d;
        if (i3 - i != 0) {
            long[] jArr3 = this.b;
            int i4 = i + 1;
            System.arraycopy(jArr3, i, jArr3, i4, i3 - i);
            Object[] objArr4 = this.c;
            System.arraycopy(objArr4, i, objArr4, i4, this.d - i);
        }
        this.b[i] = j;
        this.c[i] = e2;
        this.d++;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000a, code lost:
        r4 = r2.c;
     */
    @DexIgnore
    public void d(long j) {
        Object[] objArr;
        Object obj;
        int a2 = r4.a(this.b, this.d, j);
        if (a2 >= 0 && objArr[a2] != (obj = e)) {
            objArr[a2] = obj;
            this.a = true;
        }
    }

    @DexIgnore
    public String toString() {
        if (c() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.d * 28);
        sb.append('{');
        for (int i = 0; i < this.d; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(a(i));
            sb.append('=');
            Object c2 = c(i);
            if (c2 != this) {
                sb.append(c2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public s4(int i) {
        this.a = false;
        if (i == 0) {
            this.b = r4.b;
            this.c = r4.c;
            return;
        }
        int c2 = r4.c(i);
        this.b = new long[c2];
        this.c = new Object[c2];
    }

    @DexIgnore
    public long a(int i) {
        if (this.a) {
            b();
        }
        return this.b[i];
    }

    @DexIgnore
    public E b(long j, E e2) {
        int a2 = r4.a(this.b, this.d, j);
        if (a2 >= 0) {
            E[] eArr = this.c;
            if (eArr[a2] != e) {
                return eArr[a2];
            }
        }
        return e2;
    }

    @DexIgnore
    public s4<E> clone() {
        try {
            s4<E> s4Var = (s4) super.clone();
            s4Var.b = (long[]) this.b.clone();
            s4Var.c = (Object[]) this.c.clone();
            return s4Var;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public void a() {
        int i = this.d;
        Object[] objArr = this.c;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.d = 0;
        this.a = false;
    }

    @DexIgnore
    public void b(int i) {
        Object[] objArr = this.c;
        Object obj = objArr[i];
        Object obj2 = e;
        if (obj != obj2) {
            objArr[i] = obj2;
            this.a = true;
        }
    }

    @DexIgnore
    public final void b() {
        int i = this.d;
        long[] jArr = this.b;
        Object[] objArr = this.c;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != e) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.a = false;
        this.d = i2;
    }

    @DexIgnore
    public void a(long j, E e2) {
        int i = this.d;
        if (i == 0 || j > this.b[i - 1]) {
            if (this.a && this.d >= this.b.length) {
                b();
            }
            int i2 = this.d;
            if (i2 >= this.b.length) {
                int c2 = r4.c(i2 + 1);
                long[] jArr = new long[c2];
                Object[] objArr = new Object[c2];
                long[] jArr2 = this.b;
                System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
                Object[] objArr2 = this.c;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.b = jArr;
                this.c = objArr;
            }
            this.b[i2] = j;
            this.c[i2] = e2;
            this.d = i2 + 1;
            return;
        }
        c(j, e2);
    }

    @DexIgnore
    public int c() {
        if (this.a) {
            b();
        }
        return this.d;
    }

    @DexIgnore
    public E c(int i) {
        if (this.a) {
            b();
        }
        return this.c[i];
    }

    @DexIgnore
    public int c(long j) {
        if (this.a) {
            b();
        }
        return r4.a(this.b, this.d, j);
    }
}
