package com.fossil;

import android.os.Process;
import com.fossil.lt;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ Map<vr, d> b;
    @DexIgnore
    public /* final */ ReferenceQueue<lt<?>> c;
    @DexIgnore
    public lt.a d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public volatile c f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ws$a$a")
        /* renamed from: com.fossil.ws$a$a  reason: collision with other inner class name */
        public class C0054a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable a;

            @DexIgnore
            public C0054a(a aVar, Runnable runnable) {
                this.a = runnable;
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(10);
                this.a.run();
            }
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(new C0054a(this, runnable), "glide-active-resources");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            ws.this.a();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends WeakReference<lt<?>> {
        @DexIgnore
        public /* final */ vr a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public rt<?> c;

        @DexIgnore
        public d(vr vrVar, lt<?> ltVar, ReferenceQueue<? super lt<?>> referenceQueue, boolean z) {
            super(ltVar, referenceQueue);
            rt<?> rtVar;
            q00.a(vrVar);
            this.a = vrVar;
            if (!ltVar.f() || !z) {
                rtVar = null;
            } else {
                rt<?> e = ltVar.e();
                q00.a(e);
                rtVar = e;
            }
            this.c = rtVar;
            this.b = ltVar.f();
        }

        @DexIgnore
        public void a() {
            this.c = null;
            clear();
        }
    }

    @DexIgnore
    public ws(boolean z) {
        this(z, Executors.newSingleThreadExecutor(new a()));
    }

    @DexIgnore
    public void a(lt.a aVar) {
        synchronized (aVar) {
            synchronized (this) {
                this.d = aVar;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return r0;
     */
    @DexIgnore
    public synchronized lt<?> b(vr vrVar) {
        d dVar = this.b.get(vrVar);
        if (dVar == null) {
            return null;
        }
        lt<?> ltVar = (lt) dVar.get();
        if (ltVar == null) {
            a(dVar);
        }
    }

    @DexIgnore
    public ws(boolean z, Executor executor) {
        this.b = new HashMap();
        this.c = new ReferenceQueue<>();
        this.a = z;
        executor.execute(new b());
    }

    @DexIgnore
    public synchronized void a(vr vrVar, lt<?> ltVar) {
        d put = this.b.put(vrVar, new d(vrVar, ltVar, this.c, this.a));
        if (put != null) {
            put.a();
        }
    }

    @DexIgnore
    public synchronized void a(vr vrVar) {
        d remove = this.b.remove(vrVar);
        if (remove != null) {
            remove.a();
        }
    }

    @DexIgnore
    public void a(d dVar) {
        synchronized (this) {
            this.b.remove(dVar.a);
            if (dVar.b) {
                if (dVar.c != null) {
                    this.d.a(dVar.a, new lt(dVar.c, true, false, dVar.a, this.d));
                }
            }
        }
    }

    @DexIgnore
    public void a() {
        while (!this.e) {
            try {
                a((d) this.c.remove());
                c cVar = this.f;
                if (cVar != null) {
                    cVar.a();
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
