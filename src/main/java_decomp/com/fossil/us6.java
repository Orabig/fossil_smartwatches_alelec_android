package com.fossil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface us6 {
    @DexIgnore
    public static final us6 a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements us6 {
        @DexIgnore
        public zt6 a(File file) throws FileNotFoundException {
            return st6.c(file);
        }

        @DexIgnore
        public yt6 b(File file) throws FileNotFoundException {
            try {
                return st6.b(file);
            } catch (FileNotFoundException unused) {
                file.getParentFile().mkdirs();
                return st6.b(file);
            }
        }

        @DexIgnore
        public void c(File file) throws IOException {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                int length = listFiles.length;
                int i = 0;
                while (i < length) {
                    File file2 = listFiles[i];
                    if (file2.isDirectory()) {
                        c(file2);
                    }
                    if (file2.delete()) {
                        i++;
                    } else {
                        throw new IOException("failed to delete " + file2);
                    }
                }
                return;
            }
            throw new IOException("not a readable directory: " + file);
        }

        @DexIgnore
        public boolean d(File file) {
            return file.exists();
        }

        @DexIgnore
        public void e(File file) throws IOException {
            if (!file.delete() && file.exists()) {
                throw new IOException("failed to delete " + file);
            }
        }

        @DexIgnore
        public yt6 f(File file) throws FileNotFoundException {
            try {
                return st6.a(file);
            } catch (FileNotFoundException unused) {
                file.getParentFile().mkdirs();
                return st6.a(file);
            }
        }

        @DexIgnore
        public long g(File file) {
            return file.length();
        }

        @DexIgnore
        public void a(File file, File file2) throws IOException {
            e(file2);
            if (!file.renameTo(file2)) {
                throw new IOException("failed to rename " + file + " to " + file2);
            }
        }
    }

    @DexIgnore
    zt6 a(File file) throws FileNotFoundException;

    @DexIgnore
    void a(File file, File file2) throws IOException;

    @DexIgnore
    yt6 b(File file) throws FileNotFoundException;

    @DexIgnore
    void c(File file) throws IOException;

    @DexIgnore
    boolean d(File file);

    @DexIgnore
    void e(File file) throws IOException;

    @DexIgnore
    yt6 f(File file) throws FileNotFoundException;

    @DexIgnore
    long g(File file);
}
