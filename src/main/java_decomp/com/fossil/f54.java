package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class f54 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon20 a;
    @DexIgnore
    private /* final */ /* synthetic */ SavedPreset b;

    @DexIgnore
    public /* synthetic */ f54(PresetRepository.Anon20 anon20, SavedPreset savedPreset) {
        this.a = anon20;
        this.b = savedPreset;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b);
    }
}
