package com.fossil;

import com.fossil.zf;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ff {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends zf.b {
        @DexIgnore
        public /* final */ /* synthetic */ ef a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ ef c;
        @DexIgnore
        public /* final */ /* synthetic */ zf.d d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;

        @DexIgnore
        public a(ef efVar, int i, ef efVar2, zf.d dVar, int i2, int i3) {
            this.a = efVar;
            this.b = i;
            this.c = efVar2;
            this.d = dVar;
            this.e = i2;
            this.f = i3;
        }

        @DexIgnore
        public int a() {
            return this.f;
        }

        @DexIgnore
        public int b() {
            return this.e;
        }

        @DexIgnore
        public Object c(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            ef efVar = this.c;
            Object obj2 = efVar.get(i2 + efVar.e());
            if (obj == null || obj2 == null) {
                return null;
            }
            return this.d.getChangePayload(obj, obj2);
        }

        @DexIgnore
        public boolean a(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            ef efVar = this.c;
            Object obj2 = efVar.get(i2 + efVar.e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areContentsTheSame(obj, obj2);
        }

        @DexIgnore
        public boolean b(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            ef efVar = this.c;
            Object obj2 = efVar.get(i2 + efVar.e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areItemsTheSame(obj, obj2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements jg {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ jg b;

        @DexIgnore
        public b(int i, jg jgVar) {
            this.a = i;
            this.b = jgVar;
        }

        @DexIgnore
        public void a(int i, int i2) {
            jg jgVar = this.b;
            int i3 = this.a;
            jgVar.a(i + i3, i2 + i3);
        }

        @DexIgnore
        public void b(int i, int i2) {
            this.b.b(i + this.a, i2);
        }

        @DexIgnore
        public void c(int i, int i2) {
            this.b.c(i + this.a, i2);
        }

        @DexIgnore
        public void a(int i, int i2, Object obj) {
            this.b.a(i + this.a, i2, obj);
        }
    }

    @DexIgnore
    public static <T> zf.c a(ef<T> efVar, ef<T> efVar2, zf.d<T> dVar) {
        int a2 = efVar.a();
        int a3 = efVar2.a();
        return zf.a(new a(efVar, a2, efVar2, dVar, (efVar.size() - a2) - efVar.b(), (efVar2.size() - a3) - efVar2.b()), true);
    }

    @DexIgnore
    public static <T> void a(jg jgVar, ef<T> efVar, ef<T> efVar2, zf.c cVar) {
        int b2 = efVar.b();
        int b3 = efVar2.b();
        int a2 = efVar.a();
        int a3 = efVar2.a();
        if (b2 == 0 && b3 == 0 && a2 == 0 && a3 == 0) {
            cVar.a(jgVar);
            return;
        }
        if (b2 > b3) {
            int i = b2 - b3;
            jgVar.c(efVar.size() - i, i);
        } else if (b2 < b3) {
            jgVar.b(efVar.size(), b3 - b2);
        }
        if (a2 > a3) {
            jgVar.c(0, a2 - a3);
        } else if (a2 < a3) {
            jgVar.b(0, a3 - a2);
        }
        if (a3 != 0) {
            cVar.a((jg) new b(a3, jgVar));
        } else {
            cVar.a(jgVar);
        }
    }

    @DexIgnore
    public static int a(zf.c cVar, ef efVar, ef efVar2, int i) {
        int a2;
        int a3 = efVar.a();
        int i2 = i - a3;
        int size = (efVar.size() - a3) - efVar.b();
        if (i2 >= 0 && i2 < size) {
            for (int i3 = 0; i3 < 30; i3++) {
                int i4 = ((i3 / 2) * (i3 % 2 == 1 ? -1 : 1)) + i2;
                if (i4 >= 0 && i4 < efVar.o() && (a2 = cVar.a(i4)) != -1) {
                    return a2 + efVar2.e();
                }
            }
        }
        return Math.max(0, Math.min(i, efVar2.size() - 1));
    }
}
