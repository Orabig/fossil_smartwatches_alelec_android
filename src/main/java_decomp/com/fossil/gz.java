package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.gz;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gz<T extends gz<T>> implements Cloneable {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public boolean C; // = true;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public int a;
    @DexIgnore
    public float b; // = 1.0f;
    @DexIgnore
    public ft c; // = ft.d;
    @DexIgnore
    public br d; // = br.NORMAL;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public int o; // = -1;
    @DexIgnore
    public vr p; // = f00.a();
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public Drawable s;
    @DexIgnore
    public int t;
    @DexIgnore
    public xr u; // = new xr();
    @DexIgnore
    public Map<Class<?>, bs<?>> v; // = new i00();
    @DexIgnore
    public Class<?> w; // = Object.class;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public Resources.Theme y;
    @DexIgnore
    public boolean z;

    @DexIgnore
    public static boolean b(int i2, int i3) {
        return (i2 & i3) != 0;
    }

    @DexIgnore
    public final boolean A() {
        return a(4);
    }

    @DexIgnore
    public final boolean B() {
        return this.i;
    }

    @DexIgnore
    public final boolean C() {
        return a(8);
    }

    @DexIgnore
    public boolean D() {
        return this.C;
    }

    @DexIgnore
    public final boolean E() {
        return a(256);
    }

    @DexIgnore
    public final boolean F() {
        return this.r;
    }

    @DexIgnore
    public final boolean G() {
        return this.q;
    }

    @DexIgnore
    public final boolean H() {
        return a(2048);
    }

    @DexIgnore
    public final boolean I() {
        return r00.b(this.o, this.j);
    }

    @DexIgnore
    public T J() {
        this.x = true;
        N();
        return this;
    }

    @DexIgnore
    public T K() {
        return b(ow.c, (bs<Bitmap>) new lw());
    }

    @DexIgnore
    public T L() {
        return a(ow.b, (bs<Bitmap>) new mw());
    }

    @DexIgnore
    public T M() {
        return a(ow.a, (bs<Bitmap>) new tw());
    }

    @DexIgnore
    public final T N() {
        return this;
    }

    @DexIgnore
    public final T O() {
        if (!this.x) {
            N();
            return this;
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }

    @DexIgnore
    public T a(float f2) {
        if (this.z) {
            return clone().a(f2);
        }
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f2 > 1.0f) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }
        this.b = f2;
        this.a |= 2;
        O();
        return this;
    }

    @DexIgnore
    public T b(boolean z2) {
        if (this.z) {
            return clone().b(z2);
        }
        this.D = z2;
        this.a |= 1048576;
        O();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.bs<android.graphics.Bitmap>, com.fossil.bs] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T c(ow owVar, bs<Bitmap> r3) {
        return a(owVar, (bs<Bitmap>) r3, true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.bs<android.graphics.Bitmap>, com.fossil.bs] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T d(ow owVar, bs<Bitmap> r3) {
        if (this.z) {
            return clone().d(owVar, r3);
        }
        a(owVar);
        return a((bs<Bitmap>) r3);
    }

    @DexIgnore
    public final int e() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof gz)) {
            return false;
        }
        gz gzVar = (gz) obj;
        if (Float.compare(gzVar.b, this.b) == 0 && this.f == gzVar.f && r00.b((Object) this.e, (Object) gzVar.e) && this.h == gzVar.h && r00.b((Object) this.g, (Object) gzVar.g) && this.t == gzVar.t && r00.b((Object) this.s, (Object) gzVar.s) && this.i == gzVar.i && this.j == gzVar.j && this.o == gzVar.o && this.q == gzVar.q && this.r == gzVar.r && this.A == gzVar.A && this.B == gzVar.B && this.c.equals(gzVar.c) && this.d == gzVar.d && this.u.equals(gzVar.u) && this.v.equals(gzVar.v) && this.w.equals(gzVar.w) && r00.b((Object) this.p, (Object) gzVar.p) && r00.b((Object) this.y, (Object) gzVar.y)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final Drawable f() {
        return this.e;
    }

    @DexIgnore
    public final Drawable g() {
        return this.s;
    }

    @DexIgnore
    public final int h() {
        return this.t;
    }

    @DexIgnore
    public int hashCode() {
        return r00.a((Object) this.y, r00.a((Object) this.p, r00.a((Object) this.w, r00.a((Object) this.v, r00.a((Object) this.u, r00.a((Object) this.d, r00.a((Object) this.c, r00.a(this.B, r00.a(this.A, r00.a(this.r, r00.a(this.q, r00.a(this.o, r00.a(this.j, r00.a(this.i, r00.a((Object) this.s, r00.a(this.t, r00.a((Object) this.g, r00.a(this.h, r00.a((Object) this.e, r00.a(this.f, r00.a(this.b)))))))))))))))))))));
    }

    @DexIgnore
    public final boolean i() {
        return this.B;
    }

    @DexIgnore
    public final xr j() {
        return this.u;
    }

    @DexIgnore
    public final int k() {
        return this.j;
    }

    @DexIgnore
    public final int l() {
        return this.o;
    }

    @DexIgnore
    public final Drawable m() {
        return this.g;
    }

    @DexIgnore
    public final int n() {
        return this.h;
    }

    @DexIgnore
    public final br o() {
        return this.d;
    }

    @DexIgnore
    public final Class<?> p() {
        return this.w;
    }

    @DexIgnore
    public final vr q() {
        return this.p;
    }

    @DexIgnore
    public final float r() {
        return this.b;
    }

    @DexIgnore
    public final Resources.Theme w() {
        return this.y;
    }

    @DexIgnore
    public final Map<Class<?>, bs<?>> x() {
        return this.v;
    }

    @DexIgnore
    public final boolean y() {
        return this.D;
    }

    @DexIgnore
    public final boolean z() {
        return this.A;
    }

    @DexIgnore
    public T c() {
        return a(wx.b, true);
    }

    @DexIgnore
    public T clone() {
        try {
            T t2 = (gz) super.clone();
            t2.u = new xr();
            t2.u.a(this.u);
            t2.v = new i00();
            t2.v.putAll(this.v);
            t2.x = false;
            t2.z = false;
            return t2;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public final ft d() {
        return this.c;
    }

    @DexIgnore
    public T b(int i2) {
        if (this.z) {
            return clone().b(i2);
        }
        this.h = i2;
        this.a |= 128;
        this.g = null;
        this.a &= -65;
        O();
        return this;
    }

    @DexIgnore
    public T a(ft ftVar) {
        if (this.z) {
            return clone().a(ftVar);
        }
        q00.a(ftVar);
        this.c = ftVar;
        this.a |= 4;
        O();
        return this;
    }

    @DexIgnore
    public T a(br brVar) {
        if (this.z) {
            return clone().a(brVar);
        }
        q00.a(brVar);
        this.d = brVar;
        this.a |= 8;
        O();
        return this;
    }

    @DexIgnore
    public T b() {
        return c(ow.b, new mw());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.bs<android.graphics.Bitmap>, com.fossil.bs] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T b(ow owVar, bs<Bitmap> r3) {
        if (this.z) {
            return clone().b(owVar, (bs<Bitmap>) r3);
        }
        a(owVar);
        return a((bs<Bitmap>) r3, false);
    }

    @DexIgnore
    public T a(boolean z2) {
        if (this.z) {
            return clone().a(true);
        }
        this.i = !z2;
        this.a |= 256;
        O();
        return this;
    }

    @DexIgnore
    public T a(int i2, int i3) {
        if (this.z) {
            return clone().a(i2, i3);
        }
        this.o = i2;
        this.j = i3;
        this.a |= 512;
        O();
        return this;
    }

    @DexIgnore
    public T a(vr vrVar) {
        if (this.z) {
            return clone().a(vrVar);
        }
        q00.a(vrVar);
        this.p = vrVar;
        this.a |= 1024;
        O();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.wr<Y>, java.lang.Object, com.fossil.wr] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public <Y> T a(wr<Y> r2, Y y2) {
        if (this.z) {
            return clone().a(r2, y2);
        }
        q00.a(r2);
        q00.a(y2);
        this.u.a(r2, y2);
        O();
        return this;
    }

    @DexIgnore
    public T a(Class<?> cls) {
        if (this.z) {
            return clone().a(cls);
        }
        q00.a(cls);
        this.w = cls;
        this.a |= 4096;
        O();
        return this;
    }

    @DexIgnore
    public T a(ow owVar) {
        wr wrVar = ow.f;
        q00.a(owVar);
        return a(wrVar, owVar);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.bs<android.graphics.Bitmap>, com.fossil.bs] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T a(ow owVar, bs<Bitmap> r3) {
        return a(owVar, (bs<Bitmap>) r3, false);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.bs<android.graphics.Bitmap>, com.fossil.bs] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T a(ow owVar, bs<Bitmap> r2, boolean z2) {
        T t2;
        if (z2) {
            t2 = d(owVar, r2);
        } else {
            t2 = b(owVar, (bs<Bitmap>) r2);
        }
        t2.C = true;
        return t2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.bs<android.graphics.Bitmap>, com.fossil.bs] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public T a(bs<Bitmap> r2) {
        return a((bs<Bitmap>) r2, true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.bs<android.graphics.Bitmap>, com.fossil.bs] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public T a(bs<Bitmap> r3, boolean z2) {
        if (this.z) {
            return clone().a((bs<Bitmap>) r3, z2);
        }
        rw rwVar = new rw(r3, z2);
        a(Bitmap.class, r3, z2);
        a(Drawable.class, rwVar, z2);
        rwVar.a();
        a(BitmapDrawable.class, rwVar, z2);
        a(qx.class, new tx(r3), z2);
        O();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.Class<Y>, java.lang.Object, java.lang.Class] */
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.bs<Y>, com.fossil.bs, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 2 */
    public <Y> T a(Class<Y> r2, bs<Y> r3, boolean z2) {
        if (this.z) {
            return clone().a(r2, r3, z2);
        }
        q00.a(r2);
        q00.a(r3);
        this.v.put(r2, r3);
        this.a |= 2048;
        this.r = true;
        this.a |= 65536;
        this.C = false;
        if (z2) {
            this.a |= 131072;
            this.q = true;
        }
        O();
        return this;
    }

    @DexIgnore
    public T a(gz<?> gzVar) {
        if (this.z) {
            return clone().a(gzVar);
        }
        if (b(gzVar.a, 2)) {
            this.b = gzVar.b;
        }
        if (b(gzVar.a, 262144)) {
            this.A = gzVar.A;
        }
        if (b(gzVar.a, 1048576)) {
            this.D = gzVar.D;
        }
        if (b(gzVar.a, 4)) {
            this.c = gzVar.c;
        }
        if (b(gzVar.a, 8)) {
            this.d = gzVar.d;
        }
        if (b(gzVar.a, 16)) {
            this.e = gzVar.e;
            this.f = 0;
            this.a &= -33;
        }
        if (b(gzVar.a, 32)) {
            this.f = gzVar.f;
            this.e = null;
            this.a &= -17;
        }
        if (b(gzVar.a, 64)) {
            this.g = gzVar.g;
            this.h = 0;
            this.a &= -129;
        }
        if (b(gzVar.a, 128)) {
            this.h = gzVar.h;
            this.g = null;
            this.a &= -65;
        }
        if (b(gzVar.a, 256)) {
            this.i = gzVar.i;
        }
        if (b(gzVar.a, 512)) {
            this.o = gzVar.o;
            this.j = gzVar.j;
        }
        if (b(gzVar.a, 1024)) {
            this.p = gzVar.p;
        }
        if (b(gzVar.a, 4096)) {
            this.w = gzVar.w;
        }
        if (b(gzVar.a, 8192)) {
            this.s = gzVar.s;
            this.t = 0;
            this.a &= -16385;
        }
        if (b(gzVar.a, 16384)) {
            this.t = gzVar.t;
            this.s = null;
            this.a &= -8193;
        }
        if (b(gzVar.a, 32768)) {
            this.y = gzVar.y;
        }
        if (b(gzVar.a, 65536)) {
            this.r = gzVar.r;
        }
        if (b(gzVar.a, 131072)) {
            this.q = gzVar.q;
        }
        if (b(gzVar.a, 2048)) {
            this.v.putAll(gzVar.v);
            this.C = gzVar.C;
        }
        if (b(gzVar.a, 524288)) {
            this.B = gzVar.B;
        }
        if (!this.r) {
            this.v.clear();
            this.a &= -2049;
            this.q = false;
            this.a &= -131073;
            this.C = true;
        }
        this.a |= gzVar.a;
        this.u.a(gzVar.u);
        O();
        return this;
    }

    @DexIgnore
    public T a() {
        if (!this.x || this.z) {
            this.z = true;
            return J();
        }
        throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
    }

    @DexIgnore
    public final boolean a(int i2) {
        return b(this.a, i2);
    }
}
