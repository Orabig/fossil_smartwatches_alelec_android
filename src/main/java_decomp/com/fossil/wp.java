package com.fossil;

import com.fossil.ip;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wp<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ ip.a b;
    @DexIgnore
    public /* final */ bq c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void onErrorResponse(bq bqVar);
    }

    @DexIgnore
    public interface b<T> {
        @DexIgnore
        void onResponse(T t);
    }

    @DexIgnore
    public wp(T t, ip.a aVar) {
        this.d = false;
        this.a = t;
        this.b = aVar;
        this.c = null;
    }

    @DexIgnore
    public static <T> wp<T> a(T t, ip.a aVar) {
        return new wp<>(t, aVar);
    }

    @DexIgnore
    public static <T> wp<T> a(bq bqVar) {
        return new wp<>(bqVar);
    }

    @DexIgnore
    public boolean a() {
        return this.c == null;
    }

    @DexIgnore
    public wp(bq bqVar) {
        this.d = false;
        this.a = null;
        this.b = null;
        this.c = bqVar;
    }
}
