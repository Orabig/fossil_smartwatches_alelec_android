package com.fossil;

import com.fossil.qq6;
import com.fossil.sq6;
import com.fossil.tq6;
import com.fossil.vq6;
import com.fossil.yq6;
import java.io.IOException;
import java.util.regex.Pattern;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px6 {
    @DexIgnore
    public static /* final */ char[] l; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("(.*/)?(\\.|%2e|%2E){1,2}(/.*)?");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ tq6 b;
    @DexIgnore
    public String c;
    @DexIgnore
    public tq6.a d;
    @DexIgnore
    public /* final */ yq6.a e; // = new yq6.a();
    @DexIgnore
    public /* final */ sq6.a f;
    @DexIgnore
    public uq6 g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public vq6.a i;
    @DexIgnore
    public qq6.a j;
    @DexIgnore
    public RequestBody k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RequestBody {
        @DexIgnore
        public /* final */ RequestBody a;
        @DexIgnore
        public /* final */ uq6 b;

        @DexIgnore
        public a(RequestBody requestBody, uq6 uq6) {
            this.a = requestBody;
            this.b = uq6;
        }

        @DexIgnore
        public long a() throws IOException {
            return this.a.a();
        }

        @DexIgnore
        public uq6 b() {
            return this.b;
        }

        @DexIgnore
        public void a(kt6 kt6) throws IOException {
            this.a.a(kt6);
        }
    }

    @DexIgnore
    public px6(String str, tq6 tq6, String str2, sq6 sq6, uq6 uq6, boolean z, boolean z2, boolean z3) {
        this.a = str;
        this.b = tq6;
        this.c = str2;
        this.g = uq6;
        this.h = z;
        if (sq6 != null) {
            this.f = sq6.a();
        } else {
            this.f = new sq6.a();
        }
        if (z2) {
            this.j = new qq6.a();
        } else if (z3) {
            this.i = new vq6.a();
            this.i.a(vq6.f);
        }
    }

    @DexIgnore
    public void a(Object obj) {
        this.c = obj.toString();
    }

    @DexIgnore
    public void b(String str, String str2, boolean z) {
        if (this.c != null) {
            String a2 = a(str2, z);
            String str3 = this.c;
            String replace = str3.replace("{" + str + "}", a2);
            if (!m.matcher(replace).matches()) {
                this.c = replace;
                return;
            }
            throw new IllegalArgumentException("@Path parameters shouldn't perform path traversal ('.' or '..'): " + str2);
        }
        throw new AssertionError();
    }

    @DexIgnore
    public void c(String str, String str2, boolean z) {
        String str3 = this.c;
        if (str3 != null) {
            this.d = this.b.a(str3);
            if (this.d != null) {
                this.c = null;
            } else {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        if (z) {
            this.d.a(str, str2);
        } else {
            this.d.b(str, str2);
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        if ("Content-Type".equalsIgnoreCase(str)) {
            try {
                this.g = uq6.a(str2);
            } catch (IllegalArgumentException e2) {
                throw new IllegalArgumentException("Malformed content type: " + str2, e2);
            }
        } else {
            this.f.a(str, str2);
        }
    }

    @DexIgnore
    public static String a(String str, boolean z) {
        int length = str.length();
        int i2 = 0;
        while (i2 < length) {
            int codePointAt = str.codePointAt(i2);
            if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                jt6 jt6 = new jt6();
                jt6.a(str, 0, i2);
                a(jt6, str, i2, length, z);
                return jt6.n();
            }
            i2 += Character.charCount(codePointAt);
        }
        return str;
    }

    @DexIgnore
    public static void a(jt6 jt6, String str, int i2, int i3, boolean z) {
        jt6 jt62 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                    if (jt62 == null) {
                        jt62 = new jt6();
                    }
                    jt62.c(codePointAt);
                    while (!jt62.f()) {
                        byte readByte = jt62.readByte() & 255;
                        jt6.writeByte(37);
                        jt6.writeByte(l[(readByte >> 4) & 15]);
                        jt6.writeByte(l[readByte & 15]);
                    }
                } else {
                    jt6.c(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        if (z) {
            this.j.b(str, str2);
        } else {
            this.j.a(str, str2);
        }
    }

    @DexIgnore
    public void a(sq6 sq6, RequestBody requestBody) {
        this.i.a(sq6, requestBody);
    }

    @DexIgnore
    public void a(vq6.b bVar) {
        this.i.a(bVar);
    }

    @DexIgnore
    public void a(RequestBody requestBody) {
        this.k = requestBody;
    }

    @DexIgnore
    public yq6.a a() {
        tq6 tq6;
        tq6.a aVar = this.d;
        if (aVar != null) {
            tq6 = aVar.a();
        } else {
            tq6 = this.b.b(this.c);
            if (tq6 == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        qq6 qq6 = this.k;
        if (qq6 == null) {
            qq6.a aVar2 = this.j;
            if (aVar2 != null) {
                qq6 = aVar2.a();
            } else {
                vq6.a aVar3 = this.i;
                if (aVar3 != null) {
                    qq6 = aVar3.a();
                } else if (this.h) {
                    qq6 = RequestBody.a((uq6) null, new byte[0]);
                }
            }
        }
        uq6 uq6 = this.g;
        if (uq6 != null) {
            if (qq6 != null) {
                qq6 = new a(qq6, uq6);
            } else {
                this.f.a("Content-Type", uq6.toString());
            }
        }
        yq6.a aVar4 = this.e;
        aVar4.a(tq6);
        aVar4.a(this.f.a());
        aVar4.a(this.a, qq6);
        return aVar4;
    }
}
