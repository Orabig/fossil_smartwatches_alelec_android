package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.SectionIndexer;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mv4 extends lv4<c> implements SectionIndexer {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public /* final */ ArrayList<String> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Integer> i; // = new ArrayList<>();
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public int o; // = -1;
    @DexIgnore
    public b p;
    @DexIgnore
    public AlphabetIndexer q;
    @DexIgnore
    public /* final */ List<wx4> r;
    @DexIgnore
    public /* final */ int s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(wx4 wx4);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public /* final */ dg4 h;
        @DexIgnore
        public /* final */ /* synthetic */ mv4 i;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.b();
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v7, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(mv4 mv4, dg4 dg4) {
            super(dg4.d());
            wg6.b(dg4, "binding");
            this.i = mv4;
            this.h = dg4;
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            String b3 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                this.h.s.setBackgroundColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b3)) {
                this.h.x.setBackgroundColor(Color.parseColor(b3));
            }
            this.h.q.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void b() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                Iterator it = this.i.r.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    Contact contact = ((wx4) it.next()).getContact();
                    if (contact != null && contact.getContactId() == this.e) {
                        break;
                    }
                    i2++;
                }
                if (i2 == -1) {
                    this.i.r.add(a());
                    this.i.notifyItemChanged(adapterPosition);
                } else if (((wx4) this.i.r.get(i2)).getCurrentHandGroup() != this.i.s) {
                    b d2 = this.i.p;
                    if (d2 != null) {
                        d2.a((wx4) this.i.r.get(i2));
                    }
                } else {
                    this.i.r.remove(i2);
                    this.i.notifyItemChanged(adapterPosition);
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r10v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v19, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v22, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v24, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r10v27, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v29, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v23, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void a(Cursor cursor, int i2) {
            boolean z;
            Object obj;
            boolean z2;
            wg6.b(cursor, "cursor");
            cursor.moveToPosition(i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d2 = mv4.t;
            local.d(d2, ".Inside renderData, cursor move position=" + i2);
            this.a = cursor.getString(cursor.getColumnIndex("display_name"));
            this.b = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
            this.e = cursor.getInt(cursor.getColumnIndex("contact_id"));
            this.f = cursor.getInt(cursor.getColumnIndex("has_phone_number"));
            this.c = cursor.getString(cursor.getColumnIndex("data1"));
            this.d = cursor.getString(cursor.getColumnIndex("sort_key"));
            this.g = cursor.getInt(cursor.getColumnIndex("starred")) == 1;
            String b2 = gy5.b(this.c);
            if (this.i.i.contains(Integer.valueOf(i2)) || (this.i.o < i2 && this.i.j == this.e && this.i.h.contains(b2))) {
                if (!this.i.i.contains(Integer.valueOf(i2))) {
                    this.i.i.add(Integer.valueOf(i2));
                }
                a(8);
                z = false;
            } else {
                if (i2 > this.i.o) {
                    this.i.o = i2;
                }
                if (this.i.j != this.e) {
                    this.i.h.clear();
                    this.i.j = this.e;
                }
                this.i.h.add(b2);
                a(0);
                if (!cursor.moveToPrevious() || cursor.getInt(cursor.getColumnIndex("contact_id")) != this.e) {
                    z = false;
                } else {
                    Object r0 = this.h.q;
                    wg6.a((Object) r0, "binding.accbSelect");
                    r0.setVisibility(4);
                    Object r02 = this.h.w;
                    wg6.a((Object) r02, "binding.pickContactTitle");
                    r02.setVisibility(8);
                    z = true;
                }
                cursor.moveToNext();
            }
            Object r10 = this.h.w;
            wg6.a((Object) r10, "binding.pickContactTitle");
            r10.setText(this.a);
            if (this.f == 1) {
                Object r102 = this.h.v;
                wg6.a((Object) r102, "binding.pickContactPhone");
                r102.setText(this.c);
            }
            Iterator it = this.i.r.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                Contact contact = ((wx4) obj).getContact();
                if (contact == null || contact.getContactId() != this.e) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            wx4 wx4 = (wx4) obj;
            if (wx4 == null) {
                Object r103 = this.h.q;
                wg6.a((Object) r103, "binding.accbSelect");
                r103.setChecked(false);
            } else if (wx4.getCurrentHandGroup() == 0 || wx4.getCurrentHandGroup() == this.i.s) {
                Object r104 = this.h.q;
                wg6.a((Object) r104, "binding.accbSelect");
                r104.setChecked(true);
            } else {
                Object r105 = this.h.v;
                wg6.a((Object) r105, "binding.pickContactPhone");
                nh6 nh6 = nh6.a;
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886150);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                Object[] objArr = {Integer.valueOf(wx4.getCurrentHandGroup())};
                String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r105.setText(format);
                Object r106 = this.h.q;
                wg6.a((Object) r106, "binding.accbSelect");
                r106.setChecked(false);
                if (z) {
                    a(8);
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String d3 = mv4.t;
            local2.d(d3, "Inside renderData, contactId = " + this.e + ", displayName = " + this.a + ", hasPhoneNumber = " + this.f + ',' + " phoneNumber = " + this.c + ", newSortKey = " + this.d);
            if (i2 == this.i.getPositionForSection(this.i.getSectionForPosition(i2))) {
                Object r107 = this.h.t;
                wg6.a((Object) r107, "binding.ftvAlphabet");
                r107.setVisibility(0);
                Object r108 = this.h.t;
                wg6.a((Object) r108, "binding.ftvAlphabet");
                r108.setText(Character.toString(yk4.b.a(this.d)));
                return;
            }
            Object r109 = this.h.t;
            wg6.a((Object) r109, "binding.ftvAlphabet");
            r109.setVisibility(8);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void a(int i2) {
            Object r0 = this.h.q;
            wg6.a((Object) r0, "binding.accbSelect");
            r0.setVisibility(i2);
            ConstraintLayout constraintLayout = this.h.r;
            wg6.a((Object) constraintLayout, "binding.clMainContainer");
            constraintLayout.setVisibility(i2);
            Object r02 = this.h.t;
            wg6.a((Object) r02, "binding.ftvAlphabet");
            r02.setVisibility(i2);
            ConstraintLayout constraintLayout2 = this.h.u;
            wg6.a((Object) constraintLayout2, "binding.llTextContainer");
            constraintLayout2.setVisibility(i2);
            Object r03 = this.h.v;
            wg6.a((Object) r03, "binding.pickContactPhone");
            r03.setVisibility(i2);
            Object r04 = this.h.w;
            wg6.a((Object) r04, "binding.pickContactTitle");
            r04.setVisibility(i2);
            View view = this.h.x;
            wg6.a((Object) view, "binding.vLineSeparation");
            view.setVisibility(i2);
            ConstraintLayout constraintLayout3 = this.h.s;
            wg6.a((Object) constraintLayout3, "binding.clRoot");
            constraintLayout3.setVisibility(i2);
        }

        @DexIgnore
        public final wx4 a() {
            Contact contact = new Contact();
            contact.setContactId(this.e);
            contact.setFirstName(this.a);
            contact.setPhotoThumbUri(this.b);
            wx4 wx4 = new wx4(contact, (String) null, 2, (qg6) null);
            if (this.f == 1) {
                wx4.setHasPhoneNumber(true);
                wx4.setPhoneNumber(this.c);
            } else {
                wx4.setHasPhoneNumber(false);
            }
            Contact contact2 = wx4.getContact();
            if (contact2 != null) {
                contact2.setUseSms(true);
            }
            Contact contact3 = wx4.getContact();
            if (contact3 != null) {
                contact3.setUseCall(true);
            }
            wx4.setFavorites(this.g);
            wx4.setAdded(true);
            wx4.setCurrentHandGroup(this.i.s);
            return wx4;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = mv4.class.getSimpleName();
        wg6.a((Object) simpleName, "HybridCursorContactSearc\u2026er::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mv4(Cursor cursor, List<wx4> list, int i2) {
        super(cursor);
        wg6.b(list, "mContactWrapperList");
        this.r = list;
        this.s = i2;
    }

    @DexIgnore
    public int getPositionForSection(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.q;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getPositionForSection(i2);
            }
            wg6.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public int getSectionForPosition(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.q;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getSectionForPosition(i2);
            }
            wg6.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public Object[] getSections() {
        AlphabetIndexer alphabetIndexer = this.q;
        if (alphabetIndexer != null) {
            return alphabetIndexer.getSections();
        }
        return null;
    }

    @DexIgnore
    public Cursor c(Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return null;
        }
        this.q = new AlphabetIndexer(cursor, cursor.getColumnIndex("display_name"), "#ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        AlphabetIndexer alphabetIndexer = this.q;
        if (alphabetIndexer != null) {
            alphabetIndexer.setCursor(cursor);
            this.h.clear();
            this.i.clear();
            this.j = -1;
            this.o = -1;
            return super.c(cursor);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i2) {
        wg6.b(viewGroup, "parent");
        dg4 a2 = dg4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wg6.a((Object) a2, "ItemContactHybridBinding\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public void a(c cVar, Cursor cursor, int i2) {
        wg6.b(cVar, "holder");
        if (cursor != null) {
            cVar.a(cursor, i2);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "constraint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.d(str2, "filter: constraint = " + str);
        getFilter().filter(str);
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "itemClickListener");
        this.p = bVar;
    }
}
