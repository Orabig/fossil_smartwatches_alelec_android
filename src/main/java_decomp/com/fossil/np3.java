package com.fossil;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.FirebaseApp;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class np3 implements mp3 {
    @DexIgnore
    public static volatile mp3 b;
    @DexIgnore
    public /* final */ AppMeasurement a;

    @DexIgnore
    public np3(AppMeasurement appMeasurement) {
        w12.a(appMeasurement);
        this.a = appMeasurement;
        new ConcurrentHashMap();
    }

    @DexIgnore
    public static mp3 a(FirebaseApp firebaseApp, Context context, rq3 rq3) {
        w12.a(firebaseApp);
        w12.a(context);
        w12.a(rq3);
        w12.a(context.getApplicationContext());
        if (b == null) {
            synchronized (np3.class) {
                if (b == null) {
                    Bundle bundle = new Bundle(1);
                    if (firebaseApp.g()) {
                        rq3.a(jp3.class, rp3.a, qp3.a);
                        bundle.putBoolean("dataCollectionDefaultEnabled", firebaseApp.isDataCollectionDefaultEnabled());
                    }
                    b = new np3(AppMeasurement.a(context, bundle));
                }
            }
        }
        return b;
    }

    @DexIgnore
    public void a(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (pp3.a(str) && pp3.a(str2, bundle) && pp3.a(str, str2, bundle)) {
            this.a.logEventInternal(str, str2, bundle);
        }
    }

    @DexIgnore
    public void a(String str, String str2, Object obj) {
        if (pp3.a(str) && pp3.a(str, str2)) {
            this.a.a(str, str2, obj);
        }
    }

    @DexIgnore
    public static final /* synthetic */ void a(oq3 oq3) {
        boolean z = ((jp3) oq3.a()).a;
        synchronized (np3.class) {
            ((np3) b).a.a(z);
        }
    }
}
