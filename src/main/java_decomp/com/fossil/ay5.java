package com.fossil;

import com.fossil.ui;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay5 implements ui {
    @DexIgnore
    public /* final */ AtomicInteger a; // = new AtomicInteger(0);
    @DexIgnore
    public volatile ui.a b;

    @DexIgnore
    public ay5(String str) {
    }

    @DexIgnore
    public boolean a() {
        return this.a.get() == 0;
    }

    @DexIgnore
    public void b() {
        int decrementAndGet = this.a.decrementAndGet();
        if (decrementAndGet == 0 && this.b != null) {
            this.b.a();
        }
        if (decrementAndGet < 0) {
            throw new IllegalArgumentException("Counter has been corrupted!");
        }
    }

    @DexIgnore
    public void c() {
        this.a.getAndIncrement();
    }
}
