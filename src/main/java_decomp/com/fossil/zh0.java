package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum zh0 implements sj0 {
    SUCCESS((byte) 0),
    INVALID_OPERATION((byte) 1),
    INVALID_FILE_HANDLE((byte) 2),
    OPERATION_IN_PROGRESS((byte) 4),
    VERIFICATION_FAIL((byte) 5),
    UNKNOWN((byte) 255);
    
    @DexIgnore
    public static /* final */ ig0 j; // = null;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        j = new ig0((qg6) null);
    }
    */

    @DexIgnore
    public zh0(byte b2) {
        this.b = b2;
        this.a = cw0.a((Enum<?>) this);
    }

    @DexIgnore
    public boolean a() {
        return this == SUCCESS;
    }

    @DexIgnore
    public String getLogName() {
        return this.a;
    }
}
