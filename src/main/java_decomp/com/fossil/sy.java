package com.fossil;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sy {
    @DexIgnore
    public /* final */ Set<jz> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ List<jz> b; // = new ArrayList();
    @DexIgnore
    public boolean c;

    @DexIgnore
    public boolean a(jz jzVar) {
        boolean z = true;
        if (jzVar == null) {
            return true;
        }
        boolean remove = this.a.remove(jzVar);
        if (!this.b.remove(jzVar) && !remove) {
            z = false;
        }
        if (z) {
            jzVar.clear();
        }
        return z;
    }

    @DexIgnore
    public void b(jz jzVar) {
        this.a.add(jzVar);
        if (!this.c) {
            jzVar.c();
            return;
        }
        jzVar.clear();
        if (Log.isLoggable("RequestTracker", 2)) {
            Log.v("RequestTracker", "Paused, delaying request");
        }
        this.b.add(jzVar);
    }

    @DexIgnore
    public void c() {
        this.c = true;
        for (T t : r00.a(this.a)) {
            if (t.isRunning()) {
                t.e();
                this.b.add(t);
            }
        }
    }

    @DexIgnore
    public void d() {
        for (T t : r00.a(this.a)) {
            if (!t.g() && !t.f()) {
                t.clear();
                if (!this.c) {
                    t.c();
                } else {
                    this.b.add(t);
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        this.c = false;
        for (T t : r00.a(this.a)) {
            if (!t.g() && !t.isRunning()) {
                t.c();
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{numRequests=" + this.a.size() + ", isPaused=" + this.c + "}";
    }

    @DexIgnore
    public void a() {
        for (T a2 : r00.a(this.a)) {
            a(a2);
        }
        this.b.clear();
    }

    @DexIgnore
    public void b() {
        this.c = true;
        for (T t : r00.a(this.a)) {
            if (t.isRunning() || t.g()) {
                t.clear();
                this.b.add(t);
            }
        }
    }
}
