package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.fossil.x1;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n1 extends v1 implements x1, View.OnKeyListener, PopupWindow.OnDismissListener {
    @DexIgnore
    public static /* final */ int F; // = g0.abc_cascading_menu_item_layout;
    @DexIgnore
    public boolean A;
    @DexIgnore
    public x1.a B;
    @DexIgnore
    public ViewTreeObserver C;
    @DexIgnore
    public PopupWindow.OnDismissListener D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ Handler g;
    @DexIgnore
    public /* final */ List<q1> h; // = new ArrayList();
    @DexIgnore
    public /* final */ List<d> i; // = new ArrayList();
    @DexIgnore
    public /* final */ ViewTreeObserver.OnGlobalLayoutListener j; // = new a();
    @DexIgnore
    public /* final */ View.OnAttachStateChangeListener o; // = new b();
    @DexIgnore
    public /* final */ w2 p; // = new c();
    @DexIgnore
    public int q; // = 0;
    @DexIgnore
    public int r; // = 0;
    @DexIgnore
    public View s;
    @DexIgnore
    public View t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (n1.this.c() && n1.this.i.size() > 0 && !n1.this.i.get(0).a.m()) {
                View view = n1.this.t;
                if (view == null || !view.isShown()) {
                    n1.this.dismiss();
                    return;
                }
                for (d dVar : n1.this.i) {
                    dVar.a.d();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = n1.this.C;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    n1.this.C = view.getViewTreeObserver();
                }
                n1 n1Var = n1.this;
                n1Var.C.removeGlobalOnLayoutListener(n1Var.j);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements w2 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ d a;
            @DexIgnore
            public /* final */ /* synthetic */ MenuItem b;
            @DexIgnore
            public /* final */ /* synthetic */ q1 c;

            @DexIgnore
            public a(d dVar, MenuItem menuItem, q1 q1Var) {
                this.a = dVar;
                this.b = menuItem;
                this.c = q1Var;
            }

            @DexIgnore
            public void run() {
                d dVar = this.a;
                if (dVar != null) {
                    n1.this.E = true;
                    dVar.b.a(false);
                    n1.this.E = false;
                }
                if (this.b.isEnabled() && this.b.hasSubMenu()) {
                    this.c.a(this.b, 4);
                }
            }
        }

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(q1 q1Var, MenuItem menuItem) {
            d dVar = null;
            n1.this.g.removeCallbacksAndMessages((Object) null);
            int size = n1.this.i.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    i = -1;
                    break;
                } else if (q1Var == n1.this.i.get(i).b) {
                    break;
                } else {
                    i++;
                }
            }
            if (i != -1) {
                int i2 = i + 1;
                if (i2 < n1.this.i.size()) {
                    dVar = n1.this.i.get(i2);
                }
                n1.this.g.postAtTime(new a(dVar, menuItem, q1Var), q1Var, SystemClock.uptimeMillis() + 200);
            }
        }

        @DexIgnore
        public void b(q1 q1Var, MenuItem menuItem) {
            n1.this.g.removeCallbacksAndMessages(q1Var);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ x2 a;
        @DexIgnore
        public /* final */ q1 b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public d(x2 x2Var, q1 q1Var, int i) {
            this.a = x2Var;
            this.b = q1Var;
            this.c = i;
        }

        @DexIgnore
        public ListView a() {
            return this.a.f();
        }
    }

    @DexIgnore
    public n1(Context context, View view, int i2, int i3, boolean z2) {
        this.b = context;
        this.s = view;
        this.d = i2;
        this.e = i3;
        this.f = z2;
        this.z = false;
        this.u = i();
        Resources resources = context.getResources();
        this.c = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(d0.abc_config_prefDialogWidth));
        this.g = new Handler();
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
    }

    @DexIgnore
    public void a(q1 q1Var) {
        q1Var.a((x1) this, this.b);
        if (c()) {
            d(q1Var);
        } else {
            this.h.add(q1Var);
        }
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public Parcelable b() {
        return null;
    }

    @DexIgnore
    public void b(boolean z2) {
        this.z = z2;
    }

    @DexIgnore
    public boolean c() {
        return this.i.size() > 0 && this.i.get(0).a.c();
    }

    @DexIgnore
    public void d() {
        if (!c()) {
            for (q1 d2 : this.h) {
                d(d2);
            }
            this.h.clear();
            this.t = this.s;
            if (this.t != null) {
                boolean z2 = this.C == null;
                this.C = this.t.getViewTreeObserver();
                if (z2) {
                    this.C.addOnGlobalLayoutListener(this.j);
                }
                this.t.addOnAttachStateChangeListener(this.o);
            }
        }
    }

    @DexIgnore
    public void dismiss() {
        int size = this.i.size();
        if (size > 0) {
            d[] dVarArr = (d[]) this.i.toArray(new d[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                d dVar = dVarArr[i2];
                if (dVar.a.c()) {
                    dVar.a.dismiss();
                }
            }
        }
    }

    @DexIgnore
    public boolean e() {
        return false;
    }

    @DexIgnore
    public ListView f() {
        if (this.i.isEmpty()) {
            return null;
        }
        List<d> list = this.i;
        return list.get(list.size() - 1).a();
    }

    @DexIgnore
    public final x2 h() {
        x2 x2Var = new x2(this.b, (AttributeSet) null, this.d, this.e);
        x2Var.a(this.p);
        x2Var.a((AdapterView.OnItemClickListener) this);
        x2Var.a((PopupWindow.OnDismissListener) this);
        x2Var.a(this.s);
        x2Var.f(this.r);
        x2Var.a(true);
        x2Var.g(2);
        return x2Var;
    }

    @DexIgnore
    public final int i() {
        return x9.o(this.s) == 1 ? 0 : 1;
    }

    @DexIgnore
    public void onDismiss() {
        d dVar;
        int size = this.i.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                dVar = null;
                break;
            }
            dVar = this.i.get(i2);
            if (!dVar.a.c()) {
                break;
            }
            i2++;
        }
        if (dVar != null) {
            dVar.b.a(false);
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    public void b(int i2) {
        this.v = true;
        this.x = i2;
    }

    @DexIgnore
    public final int c(q1 q1Var) {
        int size = this.i.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (q1Var == this.i.get(i2).b) {
                return i2;
            }
        }
        return -1;
    }

    @DexIgnore
    public final MenuItem a(q1 q1Var, q1 q1Var2) {
        int size = q1Var.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = q1Var.getItem(i2);
            if (item.hasSubMenu() && q1Var2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    @DexIgnore
    public void c(int i2) {
        this.w = true;
        this.y = i2;
    }

    @DexIgnore
    public void c(boolean z2) {
        this.A = z2;
    }

    @DexIgnore
    public final View a(d dVar, q1 q1Var) {
        int i2;
        p1 p1Var;
        int firstVisiblePosition;
        MenuItem a2 = a(dVar.b, q1Var);
        if (a2 == null) {
            return null;
        }
        ListView a3 = dVar.a();
        ListAdapter adapter = a3.getAdapter();
        int i3 = 0;
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            p1Var = (p1) headerViewListAdapter.getWrappedAdapter();
        } else {
            p1Var = (p1) adapter;
            i2 = 0;
        }
        int count = p1Var.getCount();
        while (true) {
            if (i3 >= count) {
                i3 = -1;
                break;
            } else if (a2 == p1Var.getItem(i3)) {
                break;
            } else {
                i3++;
            }
        }
        if (i3 != -1 && (firstVisiblePosition = (i3 + i2) - a3.getFirstVisiblePosition()) >= 0 && firstVisiblePosition < a3.getChildCount()) {
            return a3.getChildAt(firstVisiblePosition);
        }
        return null;
    }

    @DexIgnore
    public final int d(int i2) {
        List<d> list = this.i;
        ListView a2 = list.get(list.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.t.getWindowVisibleDisplayFrame(rect);
        if (this.u == 1) {
            if (iArr[0] + a2.getWidth() + i2 > rect.right) {
                return 0;
            }
            return 1;
        } else if (iArr[0] - i2 < 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @DexIgnore
    public final void d(q1 q1Var) {
        View view;
        d dVar;
        int i2;
        int i3;
        int i4;
        LayoutInflater from = LayoutInflater.from(this.b);
        p1 p1Var = new p1(q1Var, from, this.f, F);
        if (!c() && this.z) {
            p1Var.a(true);
        } else if (c()) {
            p1Var.a(v1.b(q1Var));
        }
        int a2 = v1.a(p1Var, (ViewGroup) null, this.b, this.c);
        x2 h2 = h();
        h2.a((ListAdapter) p1Var);
        h2.e(a2);
        h2.f(this.r);
        if (this.i.size() > 0) {
            List<d> list = this.i;
            dVar = list.get(list.size() - 1);
            view = a(dVar, q1Var);
        } else {
            dVar = null;
            view = null;
        }
        if (view != null) {
            h2.d(false);
            h2.a((Object) null);
            int d2 = d(a2);
            boolean z2 = d2 == 1;
            this.u = d2;
            if (Build.VERSION.SDK_INT >= 26) {
                h2.a(view);
                i3 = 0;
                i2 = 0;
            } else {
                int[] iArr = new int[2];
                this.s.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                if ((this.r & 7) == 5) {
                    iArr[0] = iArr[0] + this.s.getWidth();
                    iArr2[0] = iArr2[0] + view.getWidth();
                }
                i2 = iArr2[0] - iArr[0];
                i3 = iArr2[1] - iArr[1];
            }
            if ((this.r & 5) != 5) {
                if (z2) {
                    a2 = view.getWidth();
                }
                i4 = i2 - a2;
                h2.a(i4);
                h2.b(true);
                h2.b(i3);
            } else if (!z2) {
                a2 = view.getWidth();
                i4 = i2 - a2;
                h2.a(i4);
                h2.b(true);
                h2.b(i3);
            }
            i4 = i2 + a2;
            h2.a(i4);
            h2.b(true);
            h2.b(i3);
        } else {
            if (this.v) {
                h2.a(this.x);
            }
            if (this.w) {
                h2.b(this.y);
            }
            h2.a(g());
        }
        this.i.add(new d(h2, q1Var, this.u));
        h2.d();
        ListView f2 = h2.f();
        f2.setOnKeyListener(this);
        if (dVar == null && this.A && q1Var.h() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(g0.abc_popup_menu_header_item_layout, f2, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(q1Var.h());
            f2.addHeaderView(frameLayout, (Object) null, false);
            h2.d();
        }
    }

    @DexIgnore
    public void a(boolean z2) {
        for (d a2 : this.i) {
            v1.a(a2.a().getAdapter()).notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void a(x1.a aVar) {
        this.B = aVar;
    }

    @DexIgnore
    public boolean a(c2 c2Var) {
        for (d next : this.i) {
            if (c2Var == next.b) {
                next.a().requestFocus();
                return true;
            }
        }
        if (!c2Var.hasVisibleItems()) {
            return false;
        }
        a((q1) c2Var);
        x1.a aVar = this.B;
        if (aVar != null) {
            aVar.a(c2Var);
        }
        return true;
    }

    @DexIgnore
    public void a(q1 q1Var, boolean z2) {
        int c2 = c(q1Var);
        if (c2 >= 0) {
            int i2 = c2 + 1;
            if (i2 < this.i.size()) {
                this.i.get(i2).b.a(false);
            }
            d remove = this.i.remove(c2);
            remove.b.b((x1) this);
            if (this.E) {
                remove.a.b((Object) null);
                remove.a.d(0);
            }
            remove.a.dismiss();
            int size = this.i.size();
            if (size > 0) {
                this.u = this.i.get(size - 1).c;
            } else {
                this.u = i();
            }
            if (size == 0) {
                dismiss();
                x1.a aVar = this.B;
                if (aVar != null) {
                    aVar.a(q1Var, true);
                }
                ViewTreeObserver viewTreeObserver = this.C;
                if (viewTreeObserver != null) {
                    if (viewTreeObserver.isAlive()) {
                        this.C.removeGlobalOnLayoutListener(this.j);
                    }
                    this.C = null;
                }
                this.t.removeOnAttachStateChangeListener(this.o);
                this.D.onDismiss();
            } else if (z2) {
                this.i.get(0).b.a(false);
            }
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (this.q != i2) {
            this.q = i2;
            this.r = e9.a(i2, x9.o(this.s));
        }
    }

    @DexIgnore
    public void a(View view) {
        if (this.s != view) {
            this.s = view;
            this.r = e9.a(this.q, x9.o(this.s));
        }
    }

    @DexIgnore
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.D = onDismissListener;
    }
}
