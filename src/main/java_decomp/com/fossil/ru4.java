package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ru4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public List<SecondTimezoneSetting> a; // = new ArrayList();
    @DexIgnore
    public /* final */ ArrayList<c> b; // = new ArrayList<>();
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ru4 ru4, View view) {
            super(view);
            wg6.b(view, "view");
            View findViewById = view.findViewById(2131362438);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
                String b = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(b)) {
                    this.a.setBackgroundColor(Color.parseColor(b));
                    return;
                }
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public SecondTimezoneSetting a;
        @DexIgnore
        public String b;
        @DexIgnore
        public a c; // = a.TYPE_HEADER;

        @DexIgnore
        public enum a {
            TYPE_HEADER,
            TYPE_VALUE
        }

        @DexIgnore
        public final void a(SecondTimezoneSetting secondTimezoneSetting) {
            this.a = secondTimezoneSetting;
        }

        @DexIgnore
        public final a b() {
            return this.c;
        }

        @DexIgnore
        public final SecondTimezoneSetting c() {
            return this.a;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final void a(String str) {
            this.b = str;
        }

        @DexIgnore
        public final void a(a aVar) {
            wg6.b(aVar, "<set-?>");
            this.c = aVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ b b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b a2 = this.a.b;
                if (a2 != null) {
                    a2.a(this.a.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ru4 ru4, View view, b bVar) {
            super(view);
            wg6.b(view, "view");
            this.b = bVar;
            View findViewById = view.findViewById(2131362850);
            View findViewById2 = view.findViewById(2131362653);
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            String b3 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                findViewById.setBackgroundColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b3)) {
                findViewById2.setBackgroundColor(Color.parseColor(b3));
            }
            view.setOnClickListener(new a(this));
            View findViewById3 = view.findViewById(2131363203);
            if (findViewById3 != null) {
                this.a = (FlexibleTextView) findViewById3;
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(((SecondTimezoneSetting) t).getTimeZoneName(), ((SecondTimezoneSetting) t2).getTimeZoneName());
        }
    }

    @DexIgnore
    public final void a(List<SecondTimezoneSetting> list) {
        wg6.b(list, "secondTimeZoneList");
        this.b.clear();
        this.a = list;
        List<T> a2 = yd6.a(list, new e());
        int size = a2.size();
        String str = "";
        int i = 0;
        while (i < size) {
            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) a2.get(i);
            String timeZoneName = secondTimezoneSetting.getTimeZoneName();
            if (timeZoneName != null) {
                char[] charArray = timeZoneName.toCharArray();
                wg6.a((Object) charArray, "(this as java.lang.String).toCharArray()");
                String valueOf = String.valueOf(Character.toUpperCase(charArray[0]));
                if (!TextUtils.equals(str, valueOf)) {
                    c cVar = new c();
                    cVar.a(valueOf);
                    cVar.a(c.a.TYPE_HEADER);
                    this.b.add(cVar);
                    str = valueOf;
                }
                c cVar2 = new c();
                cVar2.a(secondTimezoneSetting);
                cVar2.a(c.a.TYPE_VALUE);
                this.b.add(cVar2);
                i++;
            } else {
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int b(String str) {
        wg6.b(str, "letter");
        int i = 0;
        for (c next : this.b) {
            if (next.b() == c.a.TYPE_HEADER && wg6.a((Object) next.a(), (Object) str)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public final List<c> c() {
        return this.b;
    }

    @DexIgnore
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        return this.b.get(i).b().ordinal();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wg6.b(viewHolder, "holder");
        if (getItemViewType(i) == c.a.TYPE_HEADER.ordinal()) {
            Object a2 = ((a) viewHolder).a();
            String a3 = this.b.get(i).a();
            if (a3 != null) {
                a2.setText(a3);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            SecondTimezoneSetting c2 = this.b.get(i).c();
            if (c2 != null) {
                ((d) viewHolder).a().setText(c2.getTimeZoneName());
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        if (i == c.a.TYPE_HEADER.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558683, viewGroup, false);
            wg6.a((Object) inflate, "v");
            return new a(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558697, viewGroup, false);
        wg6.a((Object) inflate2, "v");
        return new d(this, inflate2, this.c);
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "filterText");
        if (TextUtils.isEmpty(str)) {
            a(this.a);
        } else {
            this.b.clear();
            for (SecondTimezoneSetting next : this.a) {
                String timeZoneName = next.getTimeZoneName();
                if (timeZoneName != null) {
                    String lowerCase = timeZoneName.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    String lowerCase2 = str.toLowerCase();
                    wg6.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                    if (yj6.a((CharSequence) lowerCase, (CharSequence) lowerCase2, false, 2, (Object) null)) {
                        c cVar = new c();
                        cVar.a(next);
                        cVar.a(c.a.TYPE_VALUE);
                        this.b.add(cVar);
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "mItemClickListener");
        this.c = bVar;
    }
}
