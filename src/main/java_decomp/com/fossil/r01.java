package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r01 extends if1 {
    @DexIgnore
    public /* final */ ArrayList<hl1> B; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.ASYNC}));
    @DexIgnore
    public /* final */ em0 C;

    @DexIgnore
    public r01(ue1 ue1, q41 q41, em0 em0) {
        super(ue1, q41, eh1.SEND_ASYNC_EVENT_ACK, (String) null, 8);
        this.C = em0;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.B;
    }

    @DexIgnore
    public void h() {
        if1.a((if1) this, (qv0) new z01(this.w, this.C), (hg6) iv0.a, (hg6) dx0.a, (ig6) null, (hg6) new xy0(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.DEVICE_EVENT, (Object) this.C.a());
    }
}
