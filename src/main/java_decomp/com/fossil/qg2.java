package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qg2 extends gg2 {
    @DexIgnore
    public ow1<dw2> a;

    @DexIgnore
    public qg2(ow1<dw2> ow1) {
        w12.a(ow1 != null, (Object) "listener can't be null.");
        this.a = ow1;
    }

    @DexIgnore
    public final void a(dw2 dw2) throws RemoteException {
        this.a.a(dw2);
        this.a = null;
    }
}
