package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yi2 extends fn2<yi2, b> implements to2 {
    @DexIgnore
    public static /* final */ yi2 zzi;
    @DexIgnore
    public static volatile yo2<yi2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public boolean zze;
    @DexIgnore
    public String zzf; // = "";
    @DexIgnore
    public String zzg; // = "";
    @DexIgnore
    public String zzh; // = "";

    @DexIgnore
    public enum a implements kn2 {
        UNKNOWN_COMPARISON_TYPE(0),
        LESS_THAN(1),
        GREATER_THAN(2),
        EQUAL(3),
        BETWEEN(4);
        
        @DexIgnore
        public /* final */ int zzg;

        /*
        static {
            new dj2();
        }
        */

        @DexIgnore
        public a(int i) {
            this.zzg = i;
        }

        @DexIgnore
        public static mn2 zzb() {
            return cj2.a;
        }

        @DexIgnore
        public final String toString() {
            return "<" + a.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzg + " name=" + name() + '>';
        }

        @DexIgnore
        public final int zza() {
            return this.zzg;
        }

        @DexIgnore
        public static a zza(int i) {
            if (i == 0) {
                return UNKNOWN_COMPARISON_TYPE;
            }
            if (i == 1) {
                return LESS_THAN;
            }
            if (i == 2) {
                return GREATER_THAN;
            }
            if (i == 3) {
                return EQUAL;
            }
            if (i != 4) {
                return null;
            }
            return BETWEEN;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fn2.b<yi2, b> implements to2 {
        @DexIgnore
        public b() {
            super(yi2.zzi);
        }

        @DexIgnore
        public /* synthetic */ b(bj2 bj2) {
            this();
        }
    }

    /*
    static {
        yi2 yi2 = new yi2();
        zzi = yi2;
        fn2.a(yi2.class, yi2);
    }
    */

    @DexIgnore
    public static yi2 y() {
        return zzi;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (bj2.a[i - 1]) {
            case 1:
                return new yi2();
            case 2:
                return new b((bj2) null);
            case 3:
                return fn2.a((ro2) zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\f\u0000\u0002\u0007\u0001\u0003\b\u0002\u0004\b\u0003\u0005\b\u0004", new Object[]{"zzc", "zzd", a.zzb(), "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                yo2<yi2> yo2 = zzj;
                if (yo2 == null) {
                    synchronized (yi2.class) {
                        yo2 = zzj;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzi);
                            zzj = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final a o() {
        a zza = a.zza(this.zzd);
        return zza == null ? a.UNKNOWN_COMPARISON_TYPE : zza;
    }

    @DexIgnore
    public final boolean p() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final boolean q() {
        return this.zze;
    }

    @DexIgnore
    public final boolean r() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final String s() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean t() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final String v() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean w() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    public final String x() {
        return this.zzh;
    }
}
