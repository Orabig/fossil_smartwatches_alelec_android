package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z5 {
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification; // = 2131952210;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Info; // = 2131952211;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Line2; // = 2131952213;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Time; // = 2131952216;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Title; // = 2131952218;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionContainer; // = 2131952462;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionText; // = 2131952463;
    @DexIgnore
    public static /* final */ int Widget_Support_CoordinatorLayout; // = 2131952566;
}
