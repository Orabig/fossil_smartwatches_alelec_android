package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hj3 {
    @DexIgnore
    public static /* final */ zi3 m; // = new fj3(0.5f);
    @DexIgnore
    public aj3 a;
    @DexIgnore
    public aj3 b;
    @DexIgnore
    public aj3 c;
    @DexIgnore
    public aj3 d;
    @DexIgnore
    public zi3 e;
    @DexIgnore
    public zi3 f;
    @DexIgnore
    public zi3 g;
    @DexIgnore
    public zi3 h;
    @DexIgnore
    public cj3 i;
    @DexIgnore
    public cj3 j;
    @DexIgnore
    public cj3 k;
    @DexIgnore
    public cj3 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public aj3 a; // = ej3.a();
        @DexIgnore
        public aj3 b; // = ej3.a();
        @DexIgnore
        public aj3 c; // = ej3.a();
        @DexIgnore
        public aj3 d; // = ej3.a();
        @DexIgnore
        public zi3 e; // = new xi3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public zi3 f; // = new xi3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public zi3 g; // = new xi3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public zi3 h; // = new xi3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public cj3 i; // = ej3.b();
        @DexIgnore
        public cj3 j; // = ej3.b();
        @DexIgnore
        public cj3 k; // = ej3.b();
        @DexIgnore
        public cj3 l; // = ej3.b();

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public b a(float f2) {
            d(f2);
            e(f2);
            c(f2);
            b(f2);
            return this;
        }

        @DexIgnore
        public b b(zi3 zi3) {
            this.g = zi3;
            return this;
        }

        @DexIgnore
        public b c(zi3 zi3) {
            this.e = zi3;
            return this;
        }

        @DexIgnore
        public b d(float f2) {
            this.e = new xi3(f2);
            return this;
        }

        @DexIgnore
        public b e(float f2) {
            this.f = new xi3(f2);
            return this;
        }

        @DexIgnore
        public static float e(aj3 aj3) {
            if (aj3 instanceof gj3) {
                return ((gj3) aj3).a;
            }
            if (aj3 instanceof bj3) {
                return ((bj3) aj3).a;
            }
            return -1.0f;
        }

        @DexIgnore
        public b b(float f2) {
            this.h = new xi3(f2);
            return this;
        }

        @DexIgnore
        public b c(float f2) {
            this.g = new xi3(f2);
            return this;
        }

        @DexIgnore
        public b d(zi3 zi3) {
            this.f = zi3;
            return this;
        }

        @DexIgnore
        public b b(int i2, zi3 zi3) {
            b(ej3.a(i2));
            b(zi3);
            return this;
        }

        @DexIgnore
        public b c(int i2, zi3 zi3) {
            c(ej3.a(i2));
            c(zi3);
            return this;
        }

        @DexIgnore
        public b d(int i2, zi3 zi3) {
            d(ej3.a(i2));
            d(zi3);
            return this;
        }

        @DexIgnore
        public b a(zi3 zi3) {
            this.h = zi3;
            return this;
        }

        @DexIgnore
        public b b(aj3 aj3) {
            this.c = aj3;
            float e2 = e(aj3);
            if (e2 != -1.0f) {
                c(e2);
            }
            return this;
        }

        @DexIgnore
        public b c(aj3 aj3) {
            this.a = aj3;
            float e2 = e(aj3);
            if (e2 != -1.0f) {
                d(e2);
            }
            return this;
        }

        @DexIgnore
        public b d(aj3 aj3) {
            this.b = aj3;
            float e2 = e(aj3);
            if (e2 != -1.0f) {
                e(e2);
            }
            return this;
        }

        @DexIgnore
        public b a(int i2, zi3 zi3) {
            a(ej3.a(i2));
            a(zi3);
            return this;
        }

        @DexIgnore
        public b a(aj3 aj3) {
            this.d = aj3;
            float e2 = e(aj3);
            if (e2 != -1.0f) {
                b(e2);
            }
            return this;
        }

        @DexIgnore
        public b a(cj3 cj3) {
            this.i = cj3;
            return this;
        }

        @DexIgnore
        public hj3 a() {
            return new hj3(this);
        }

        @DexIgnore
        public b(hj3 hj3) {
            this.a = hj3.a;
            this.b = hj3.b;
            this.c = hj3.c;
            this.d = hj3.d;
            this.e = hj3.e;
            this.f = hj3.f;
            this.g = hj3.g;
            this.h = hj3.h;
            this.i = hj3.i;
            this.j = hj3.j;
            this.k = hj3.k;
            this.l = hj3.l;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        zi3 a(zi3 zi3);
    }

    @DexIgnore
    public static b a(Context context, AttributeSet attributeSet, int i2, int i3) {
        return a(context, attributeSet, i2, i3, 0);
    }

    @DexIgnore
    public static b n() {
        return new b();
    }

    @DexIgnore
    public aj3 b() {
        return this.d;
    }

    @DexIgnore
    public zi3 c() {
        return this.h;
    }

    @DexIgnore
    public aj3 d() {
        return this.c;
    }

    @DexIgnore
    public zi3 e() {
        return this.g;
    }

    @DexIgnore
    public cj3 f() {
        return this.l;
    }

    @DexIgnore
    public cj3 g() {
        return this.j;
    }

    @DexIgnore
    public cj3 h() {
        return this.i;
    }

    @DexIgnore
    public aj3 i() {
        return this.a;
    }

    @DexIgnore
    public zi3 j() {
        return this.e;
    }

    @DexIgnore
    public aj3 k() {
        return this.b;
    }

    @DexIgnore
    public zi3 l() {
        return this.f;
    }

    @DexIgnore
    public b m() {
        return new b(this);
    }

    @DexIgnore
    public hj3(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
        this.g = bVar.g;
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
        this.k = bVar.k;
        this.l = bVar.l;
    }

    @DexIgnore
    public static b a(Context context, AttributeSet attributeSet, int i2, int i3, int i4) {
        return a(context, attributeSet, i2, i3, (zi3) new xi3((float) i4));
    }

    @DexIgnore
    public static b a(Context context, AttributeSet attributeSet, int i2, int i3, zi3 zi3) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, xf3.MaterialShape, i2, i3);
        int resourceId = obtainStyledAttributes.getResourceId(xf3.MaterialShape_shapeAppearance, 0);
        int resourceId2 = obtainStyledAttributes.getResourceId(xf3.MaterialShape_shapeAppearanceOverlay, 0);
        obtainStyledAttributes.recycle();
        return a(context, resourceId, resourceId2, zi3);
    }

    @DexIgnore
    public static b a(Context context, int i2, int i3) {
        return a(context, i2, i3, 0);
    }

    @DexIgnore
    public static b a(Context context, int i2, int i3, int i4) {
        return a(context, i2, i3, (zi3) new xi3((float) i4));
    }

    @DexIgnore
    public static b a(Context context, int i2, int i3, zi3 zi3) {
        if (i3 != 0) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, i2);
            i2 = i3;
            context = contextThemeWrapper;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, xf3.ShapeAppearance);
        try {
            int i4 = obtainStyledAttributes.getInt(xf3.ShapeAppearance_cornerFamily, 0);
            int i5 = obtainStyledAttributes.getInt(xf3.ShapeAppearance_cornerFamilyTopLeft, i4);
            int i6 = obtainStyledAttributes.getInt(xf3.ShapeAppearance_cornerFamilyTopRight, i4);
            int i7 = obtainStyledAttributes.getInt(xf3.ShapeAppearance_cornerFamilyBottomRight, i4);
            int i8 = obtainStyledAttributes.getInt(xf3.ShapeAppearance_cornerFamilyBottomLeft, i4);
            zi3 a2 = a(obtainStyledAttributes, xf3.ShapeAppearance_cornerSize, zi3);
            zi3 a3 = a(obtainStyledAttributes, xf3.ShapeAppearance_cornerSizeTopLeft, a2);
            zi3 a4 = a(obtainStyledAttributes, xf3.ShapeAppearance_cornerSizeTopRight, a2);
            zi3 a5 = a(obtainStyledAttributes, xf3.ShapeAppearance_cornerSizeBottomRight, a2);
            zi3 a6 = a(obtainStyledAttributes, xf3.ShapeAppearance_cornerSizeBottomLeft, a2);
            b bVar = new b();
            bVar.c(i5, a3);
            bVar.d(i6, a4);
            bVar.b(i7, a5);
            bVar.a(i8, a6);
            return bVar;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public hj3() {
        this.a = ej3.a();
        this.b = ej3.a();
        this.c = ej3.a();
        this.d = ej3.a();
        this.e = new xi3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.f = new xi3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g = new xi3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.h = new xi3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.i = ej3.b();
        this.j = ej3.b();
        this.k = ej3.b();
        this.l = ej3.b();
    }

    @DexIgnore
    public static zi3 a(TypedArray typedArray, int i2, zi3 zi3) {
        TypedValue peekValue = typedArray.peekValue(i2);
        if (peekValue == null) {
            return zi3;
        }
        int i3 = peekValue.type;
        if (i3 == 5) {
            return new xi3((float) TypedValue.complexToDimensionPixelSize(peekValue.data, typedArray.getResources().getDisplayMetrics()));
        }
        return i3 == 6 ? new fj3(peekValue.getFraction(1.0f, 1.0f)) : zi3;
    }

    @DexIgnore
    public cj3 a() {
        return this.k;
    }

    @DexIgnore
    public hj3 a(float f2) {
        b m2 = m();
        m2.a(f2);
        return m2.a();
    }

    @DexIgnore
    public hj3 a(c cVar) {
        b m2 = m();
        m2.c(cVar.a(j()));
        m2.d(cVar.a(l()));
        m2.a(cVar.a(c()));
        m2.b(cVar.a(e()));
        return m2.a();
    }

    @DexIgnore
    public boolean a(RectF rectF) {
        Class<cj3> cls = cj3.class;
        boolean z = this.l.getClass().equals(cls) && this.j.getClass().equals(cls) && this.i.getClass().equals(cls) && this.k.getClass().equals(cls);
        float a2 = this.e.a(rectF);
        boolean z2 = this.f.a(rectF) == a2 && this.h.a(rectF) == a2 && this.g.a(rectF) == a2;
        boolean z3 = (this.b instanceof gj3) && (this.a instanceof gj3) && (this.c instanceof gj3) && (this.d instanceof gj3);
        if (!z || !z2 || !z3) {
            return false;
        }
        return true;
    }
}
