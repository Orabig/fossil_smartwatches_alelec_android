package com.fossil;

import android.util.Log;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y32 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public y32(String str, String... strArr) {
        this(str, r8);
        String str2;
        if (strArr.length == 0) {
            str2 = "";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append('[');
            for (String str3 : strArr) {
                if (sb.length() > 1) {
                    sb.append(",");
                }
                sb.append(str3);
            }
            sb.append(']');
            sb.append(' ');
            str2 = sb.toString();
        }
    }

    @DexIgnore
    public boolean a(int i) {
        return this.c <= i;
    }

    @DexIgnore
    public void b(String str, Object... objArr) {
        Log.e(this.a, c(str, objArr));
    }

    @DexIgnore
    public final String c(String str, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str = String.format(Locale.US, str, objArr);
        }
        return this.b.concat(str);
    }

    @DexIgnore
    public void a(String str, Object... objArr) {
        if (a(3)) {
            Log.d(this.a, c(str, objArr));
        }
    }

    @DexIgnore
    public y32(String str, String str2) {
        this.b = str2;
        this.a = str;
        new l12(str);
        int i = 2;
        while (7 >= i && !Log.isLoggable(this.a, i)) {
            i++;
        }
        this.c = i;
    }
}
