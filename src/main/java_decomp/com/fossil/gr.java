package com.fossil;

import com.fossil.gr;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gr<CHILD extends gr<CHILD, TranscodeType>, TranscodeType> implements Cloneable {
    @DexIgnore
    public c00<? super TranscodeType> a; // = a00.a();

    @DexIgnore
    public final c00<? super TranscodeType> a() {
        return this.a;
    }

    @DexIgnore
    public final CHILD clone() {
        try {
            return (gr) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
