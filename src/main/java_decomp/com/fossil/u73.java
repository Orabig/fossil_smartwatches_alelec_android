package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u73 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ e73 b;

    @DexIgnore
    public u73(e73 e73, boolean z) {
        this.b = e73;
        this.a = z;
    }

    @DexIgnore
    public final void run() {
        boolean g = this.b.a.g();
        boolean f = this.b.a.f();
        this.b.a.a(this.a);
        if (f == this.a) {
            this.b.a.b().B().a("Default data collection state already set to", Boolean.valueOf(this.a));
        }
        if (this.b.a.g() == g || this.b.a.g() != this.b.a.f()) {
            this.b.a.b().y().a("Default data collection is different than actual status", Boolean.valueOf(this.a), Boolean.valueOf(g));
        }
        this.b.L();
    }
}
