package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tb4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleEditText q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ RTLImageView s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ RecyclerView u;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat v;

    @DexIgnore
    public tb4(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, RTLImageView rTLImageView2, ConstraintLayout constraintLayout2, RecyclerView recyclerView, FlexibleSwitchCompat flexibleSwitchCompat) {
        super(obj, view, i);
        this.q = flexibleEditText;
        this.r = rTLImageView;
        this.s = rTLImageView2;
        this.t = constraintLayout2;
        this.u = recyclerView;
        this.v = flexibleSwitchCompat;
    }
}
