package com.fossil;

import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$setReplyMessageToDevice$1$messages$1", f = "HomeDashboardPresenter.kt", l = {309}, m = "invokeSuspend")
public final class wa5$d$b extends sf6 implements ig6<il6, xe6<? super List<? extends QuickResponseMessage>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDashboardPresenter.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wa5$d$b(HomeDashboardPresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        wa5$d$b wa5_d_b = new wa5$d$b(this.this$0, xe6);
        wa5_d_b.p$ = (il6) obj;
        return wa5_d_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((wa5$d$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            QuickResponseRepository j = this.this$0.this$0.E;
            this.L$0 = il6;
            this.label = 1;
            obj = j.getAllQuickResponse(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
