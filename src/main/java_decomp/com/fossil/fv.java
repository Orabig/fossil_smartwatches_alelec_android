package com.fossil;

import android.text.TextUtils;
import com.facebook.GraphRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv implements dv {
    @DexIgnore
    public /* final */ Map<String, List<ev>> b;
    @DexIgnore
    public volatile Map<String, String> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ String b; // = b();
        @DexIgnore
        public static /* final */ Map<String, List<ev>> c;
        @DexIgnore
        public Map<String, List<ev>> a; // = c;

        /*
        static {
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(b)) {
                hashMap.put(GraphRequest.USER_AGENT_HEADER, Collections.singletonList(new b(b)));
            }
            c = Collections.unmodifiableMap(hashMap);
        }
        */

        @DexIgnore
        public static String b() {
            String property = System.getProperty("http.agent");
            if (TextUtils.isEmpty(property)) {
                return property;
            }
            int length = property.length();
            StringBuilder sb = new StringBuilder(property.length());
            for (int i = 0; i < length; i++) {
                char charAt = property.charAt(i);
                if ((charAt > 31 || charAt == 9) && charAt < 127) {
                    sb.append(charAt);
                } else {
                    sb.append('?');
                }
            }
            return sb.toString();
        }

        @DexIgnore
        public fv a() {
            return new fv(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ev {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            this.a = str;
        }

        @DexIgnore
        public String a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return this.a.equals(((b) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "StringHeaderFactory{value='" + this.a + '\'' + '}';
        }
    }

    @DexIgnore
    public fv(Map<String, List<ev>> map) {
        this.b = Collections.unmodifiableMap(map);
    }

    @DexIgnore
    public Map<String, String> a() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = Collections.unmodifiableMap(b());
                }
            }
        }
        return this.c;
    }

    @DexIgnore
    public final Map<String, String> b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.b.entrySet()) {
            String a2 = a((List) next.getValue());
            if (!TextUtils.isEmpty(a2)) {
                hashMap.put(next.getKey(), a2);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof fv) {
            return this.b.equals(((fv) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "LazyHeaders{headers=" + this.b + '}';
    }

    @DexIgnore
    public final String a(List<ev> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            String a2 = list.get(i).a();
            if (!TextUtils.isEmpty(a2)) {
                sb.append(a2);
                if (i != list.size() - 1) {
                    sb.append(',');
                }
            }
        }
        return sb.toString();
    }
}
