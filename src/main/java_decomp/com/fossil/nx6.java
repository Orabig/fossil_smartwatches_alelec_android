package com.fossil;

import com.fossil.vq6;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nx6<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends nx6<Iterable<T>> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(px6 px6, Iterable<T> iterable) throws IOException {
            if (iterable != null) {
                for (T a2 : iterable) {
                    nx6.this.a(px6, a2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends nx6<Object> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(px6 px6, Object obj) throws IOException {
            if (obj != null) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    nx6.this.a(px6, Array.get(obj, i));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> extends nx6<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ fx6<T, RequestBody> c;

        @DexIgnore
        public c(Method method, int i, fx6<T, RequestBody> fx6) {
            this.a = method;
            this.b = i;
            this.c = fx6;
        }

        @DexIgnore
        public void a(px6 px6, T t) {
            if (t != null) {
                try {
                    px6.a(this.c.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw vx6.a(method, e, i, "Unable to convert " + t + " to RequestBody", new Object[0]);
                }
            } else {
                throw vx6.a(this.a, this.b, "Body parameter value must not be null.", new Object[0]);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> extends nx6<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ fx6<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public d(String str, fx6<T, String> fx6, boolean z) {
            vx6.a(str, "name == null");
            this.a = str;
            this.b = fx6;
            this.c = z;
        }

        @DexIgnore
        public void a(px6 px6, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                px6.a(this.a, a2, this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> extends nx6<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ fx6<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public e(Method method, int i, fx6<T, String> fx6, boolean z) {
            this.a = method;
            this.b = i;
            this.c = fx6;
            this.d = z;
        }

        @DexIgnore
        public void a(px6 px6, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    String str = (String) next.getKey();
                    if (str != null) {
                        Object value = next.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                px6.a(str, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw vx6.a(method, i, "Field map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw vx6.a(method2, i2, "Field map contained null value for key '" + str + "'.", new Object[0]);
                        }
                    } else {
                        throw vx6.a(this.a, this.b, "Field map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw vx6.a(this.a, this.b, "Field map was null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> extends nx6<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ fx6<T, String> b;

        @DexIgnore
        public f(String str, fx6<T, String> fx6) {
            vx6.a(str, "name == null");
            this.a = str;
            this.b = fx6;
        }

        @DexIgnore
        public void a(px6 px6, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                px6.a(this.a, a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> extends nx6<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ sq6 c;
        @DexIgnore
        public /* final */ fx6<T, RequestBody> d;

        @DexIgnore
        public g(Method method, int i, sq6 sq6, fx6<T, RequestBody> fx6) {
            this.a = method;
            this.b = i;
            this.c = sq6;
            this.d = fx6;
        }

        @DexIgnore
        public void a(px6 px6, T t) {
            if (t != null) {
                try {
                    px6.a(this.c, this.d.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw vx6.a(method, i, "Unable to convert " + t + " to RequestBody", e);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> extends nx6<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ fx6<T, RequestBody> c;
        @DexIgnore
        public /* final */ String d;

        @DexIgnore
        public h(Method method, int i, fx6<T, RequestBody> fx6, String str) {
            this.a = method;
            this.b = i;
            this.c = fx6;
            this.d = str;
        }

        @DexIgnore
        public void a(px6 px6, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    String str = (String) next.getKey();
                    if (str != null) {
                        Object value = next.getValue();
                        if (value != null) {
                            px6.a(sq6.a(new String[]{"Content-Disposition", "form-data; name=\"" + str + "\"", "Content-Transfer-Encoding", this.d}), this.c.a(value));
                        } else {
                            Method method = this.a;
                            int i = this.b;
                            throw vx6.a(method, i, "Part map contained null value for key '" + str + "'.", new Object[0]);
                        }
                    } else {
                        throw vx6.a(this.a, this.b, "Part map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw vx6.a(this.a, this.b, "Part map was null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> extends nx6<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ fx6<T, String> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public i(Method method, int i, String str, fx6<T, String> fx6, boolean z) {
            this.a = method;
            this.b = i;
            vx6.a(str, "name == null");
            this.c = str;
            this.d = fx6;
            this.e = z;
        }

        @DexIgnore
        public void a(px6 px6, T t) throws IOException {
            if (t != null) {
                px6.b(this.c, this.d.a(t), this.e);
                return;
            }
            Method method = this.a;
            int i = this.b;
            throw vx6.a(method, i, "Path parameter \"" + this.c + "\" value must not be null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> extends nx6<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ fx6<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public j(String str, fx6<T, String> fx6, boolean z) {
            vx6.a(str, "name == null");
            this.a = str;
            this.b = fx6;
            this.c = z;
        }

        @DexIgnore
        public void a(px6 px6, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                px6.c(this.a, a2, this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> extends nx6<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ fx6<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public k(Method method, int i, fx6<T, String> fx6, boolean z) {
            this.a = method;
            this.b = i;
            this.c = fx6;
            this.d = z;
        }

        @DexIgnore
        public void a(px6 px6, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    String str = (String) next.getKey();
                    if (str != null) {
                        Object value = next.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                px6.c(str, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw vx6.a(method, i, "Query map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw vx6.a(method2, i2, "Query map contained null value for key '" + str + "'.", new Object[0]);
                        }
                    } else {
                        throw vx6.a(this.a, this.b, "Query map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw vx6.a(this.a, this.b, "Query map was null", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> extends nx6<T> {
        @DexIgnore
        public /* final */ fx6<T, String> a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public l(fx6<T, String> fx6, boolean z) {
            this.a = fx6;
            this.b = z;
        }

        @DexIgnore
        public void a(px6 px6, T t) throws IOException {
            if (t != null) {
                px6.c(this.a.a(t), (String) null, this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends nx6<vq6.b> {
        @DexIgnore
        public static /* final */ m a; // = new m();

        @DexIgnore
        public void a(px6 px6, vq6.b bVar) {
            if (bVar != null) {
                px6.a(bVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends nx6<Object> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public n(Method method, int i) {
            this.a = method;
            this.b = i;
        }

        @DexIgnore
        public void a(px6 px6, Object obj) {
            if (obj != null) {
                px6.a(obj);
                return;
            }
            throw vx6.a(this.a, this.b, "@Url parameter is null.", new Object[0]);
        }
    }

    @DexIgnore
    public final nx6<Object> a() {
        return new b();
    }

    @DexIgnore
    public abstract void a(px6 px6, T t) throws IOException;

    @DexIgnore
    public final nx6<Iterable<T>> b() {
        return new a();
    }
}
