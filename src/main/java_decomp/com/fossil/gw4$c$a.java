package com.fossil;

import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter$start$1$1", f = "ConnectedAppsPresenter.kt", l = {}, m = "invokeSuspend")
public final class gw4$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ConnectedAppsPresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gw4$c$a(ConnectedAppsPresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        gw4$c$a gw4_c_a = new gw4$c$a(this.this$0, xe6);
        gw4_c_a.p$ = (il6) obj;
        return gw4_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((gw4$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            ConnectedAppsPresenter connectedAppsPresenter = this.this$0.this$0;
            connectedAppsPresenter.e = connectedAppsPresenter.h.getCurrentUser();
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
