package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class te4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleEditText s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ RecyclerViewEmptySupport u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public te4(Object obj, View view, int i, FlexibleTextView flexibleTextView, ImageView imageView, ConstraintLayout constraintLayout, FlexibleEditText flexibleEditText, ConstraintLayout constraintLayout2, RecyclerViewEmptySupport recyclerViewEmptySupport, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = imageView;
        this.s = flexibleEditText;
        this.t = constraintLayout2;
        this.u = recyclerViewEmptySupport;
        this.v = flexibleTextView2;
    }
}
