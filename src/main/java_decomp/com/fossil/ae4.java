package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ae4 extends zd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j M; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray N; // = new SparseIntArray();
    @DexIgnore
    public long L;

    /*
    static {
        N.put(2131362082, 1);
        N.put(2131362542, 2);
        N.put(2131363218, 3);
        N.put(2131362056, 4);
        N.put(2131362543, 5);
        N.put(2131362321, 6);
        N.put(2131362320, 7);
        N.put(2131362653, 8);
        N.put(2131362318, 9);
        N.put(2131362316, 10);
        N.put(2131362319, 11);
        N.put(2131362317, 12);
        N.put(2131362609, 13);
        N.put(2131362074, 14);
        N.put(2131362058, 15);
        N.put(2131362780, 16);
        N.put(2131362407, 17);
        N.put(2131362357, 18);
        N.put(2131362034, 19);
        N.put(2131362890, 20);
        N.put(2131362123, 21);
        N.put(2131362003, 22);
        N.put(2131363264, 23);
        N.put(2131362600, 24);
        N.put(2131362365, 25);
        N.put(2131362876, 26);
        N.put(2131362364, 27);
    }
    */

    @DexIgnore
    public ae4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 28, M, N));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.L = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.L != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.L = 1;
        }
        g();
    }

    @DexIgnore
    public ae4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[19], objArr[22], objArr[4], objArr[15], objArr[14], objArr[1], objArr[21], objArr[10], objArr[12], objArr[9], objArr[11], objArr[7], objArr[6], objArr[18], objArr[27], objArr[25], objArr[17], objArr[2], objArr[5], objArr[24], objArr[13], objArr[8], objArr[16], objArr[0], objArr[26], objArr[20], objArr[3], objArr[23]);
        this.L = -1;
        this.I.setTag((Object) null);
        a(view);
        f();
    }
}
