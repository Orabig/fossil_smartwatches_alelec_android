package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oy3 implements uy3 {
    @DexIgnore
    public int a() {
        return 0;
    }

    @DexIgnore
    public void a(vy3 vy3) {
        if (xy3.a((CharSequence) vy3.d(), vy3.f) >= 2) {
            vy3.a(a(vy3.d().charAt(vy3.f), vy3.d().charAt(vy3.f + 1)));
            vy3.f += 2;
            return;
        }
        char c = vy3.c();
        int a = xy3.a(vy3.d(), vy3.f, a());
        if (a != a()) {
            if (a == 1) {
                vy3.a(230);
                vy3.b(1);
            } else if (a == 2) {
                vy3.a(239);
                vy3.b(2);
            } else if (a == 3) {
                vy3.a(238);
                vy3.b(3);
            } else if (a == 4) {
                vy3.a(240);
                vy3.b(4);
            } else if (a == 5) {
                vy3.a(231);
                vy3.b(5);
            } else {
                throw new IllegalStateException("Illegal mode: " + a);
            }
        } else if (xy3.c(c)) {
            vy3.a(235);
            vy3.a((char) ((c - 128) + 1));
            vy3.f++;
        } else {
            vy3.a((char) (c + 1));
            vy3.f++;
        }
    }

    @DexIgnore
    public static char a(char c, char c2) {
        if (xy3.b(c) && xy3.b(c2)) {
            return (char) (((c - '0') * 10) + (c2 - '0') + 130);
        }
        throw new IllegalArgumentException("not digits: " + c + c2);
    }
}
