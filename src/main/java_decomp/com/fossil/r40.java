package com.fossil;

import android.bluetooth.BluetoothAdapter;
import com.fossil.w40;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r40 extends p40 {
    @DexIgnore
    public static /* final */ c t; // = new c((qg6) null);
    @DexIgnore
    public s40 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ w40 i;
    @DexIgnore
    public /* final */ w40 j;
    @DexIgnore
    public /* final */ w40 k;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, w40> l;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, w40> m;
    @DexIgnore
    public /* final */ a n;
    @DexIgnore
    public /* final */ s60[] o;
    @DexIgnore
    public /* final */ w40 p;
    @DexIgnore
    public /* final */ String q;
    @DexIgnore
    public /* final */ w40 r;
    @DexIgnore
    public /* final */ String s;

    @DexIgnore
    public enum a {
        NO_REQUIRE((byte) 0),
        REQUIRE((byte) 1);
        
        @DexIgnore
        public static /* final */ C0043a c; // = null;
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.r40$a$a")
        /* renamed from: com.fossil.r40$a$a  reason: collision with other inner class name */
        public static final class C0043a {
            @DexIgnore
            public /* synthetic */ C0043a(qg6 qg6) {
            }

            @DexIgnore
            public final a a(byte b) {
                for (a aVar : a.values()) {
                    if (aVar.a() == b) {
                        return aVar;
                    }
                }
                return null;
            }
        }

        /*
        static {
            c = new C0043a((qg6) null);
        }
        */

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore
    public enum b {
        SERIAL_NUMBER(1),
        HARDWARE_REVISION(2),
        FIRMWARE_VERSION(3),
        MODEL_NUMBER(4),
        HEART_RATE_SERIAL_NUMBER(5),
        BOOTLOADER_VERSION(6),
        WATCH_APP_VERSION(7),
        FONT_VERSION(8),
        LUTS_VERSION(9),
        SUPPORTED_FILES_VERSION(10),
        BOND_REQUIREMENT(11),
        SUPPORTED_DEVICE_CONFIGS(12),
        DEVICE_SECURITY_VERSION(14),
        SOCKET_INFO(15),
        LOCALE(16),
        MICRO_APP_SYSTEM_VERSION(17),
        LOCALE_VERSION(18),
        CURRENT_FILES_VERSION(19),
        UNKNOWN((short) 65535);
        
        @DexIgnore
        public static /* final */ a c; // = null;
        @DexIgnore
        public /* final */ short a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(qg6 qg6) {
            }

            @DexIgnore
            public final b a(short s) {
                b bVar;
                b[] values = b.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bVar = null;
                        break;
                    }
                    bVar = values[i];
                    if (bVar.a() == s) {
                        break;
                    }
                    i++;
                }
                return bVar != null ? bVar : b.UNKNOWN;
            }
        }

        /*
        static {
            c = new a((qg6) null);
        }
        */

        @DexIgnore
        public b(short s) {
            this.a = s;
        }

        @DexIgnore
        public final short a() {
            return this.a;
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ r40(String str, String str2, String str3, String str4, String str5, String str6, String str7, w40 w40, w40 w402, w40 w403, LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, a aVar, s60[] s60Arr, w40 w404, String str8, w40 w405, String str9, int i2) {
        this(str, str2, str3, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21);
        String str10;
        int i3 = i2;
        String str11 = (i3 & 8) != 0 ? "" : str4;
        String str12 = (i3 & 16) != 0 ? "" : str5;
        String str13 = (i3 & 32) != 0 ? "" : str6;
        String str14 = (i3 & 64) != 0 ? "" : str7;
        w40 w406 = (i3 & 128) != 0 ? new w40((byte) 0, (byte) 0) : w40;
        w40 w407 = (i3 & 256) != 0 ? new w40((byte) 0, (byte) 0) : w402;
        w40 w408 = (i3 & 512) != 0 ? new w40((byte) 0, (byte) 0) : w403;
        LinkedHashMap linkedHashMap3 = (i3 & 1024) != 0 ? new LinkedHashMap() : linkedHashMap;
        LinkedHashMap linkedHashMap4 = (i3 & 2048) != 0 ? new LinkedHashMap() : linkedHashMap2;
        a aVar2 = (i3 & 4096) != 0 ? a.NO_REQUIRE : aVar;
        s60[] s60Arr2 = (i3 & 8192) != 0 ? new s60[0] : s60Arr;
        w40 w409 = (i3 & 16384) != 0 ? new w40((byte) 0, (byte) 0) : w404;
        String str15 = (32768 & i3) != 0 ? "en_US" : str8;
        w40 w4010 = (65536 & i3) != 0 ? new w40((byte) 0, (byte) 0) : w405;
        if ((i3 & 131072) != 0) {
            str10 = "";
        } else {
            str10 = str9;
        }
    }

    @DexIgnore
    public static /* synthetic */ r40 a(r40 r40, String str, String str2, String str3, String str4, String str5, String str6, String str7, w40 w40, w40 w402, w40 w403, LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, a aVar, s60[] s60Arr, w40 w404, String str8, w40 w405, String str9, int i2) {
        r40 r402 = r40;
        int i3 = i2;
        return r40.a((i3 & 1) != 0 ? r402.b : str, (i3 & 2) != 0 ? r402.c : str2, (i3 & 4) != 0 ? r402.d : str3, (i3 & 8) != 0 ? r402.e : str4, (i3 & 16) != 0 ? r402.f : str5, (i3 & 32) != 0 ? r402.g : str6, (i3 & 64) != 0 ? r402.h : str7, (i3 & 128) != 0 ? r402.i : w40, (i3 & 256) != 0 ? r402.j : w402, (i3 & 512) != 0 ? r402.k : w403, (i3 & 1024) != 0 ? r402.l : linkedHashMap, (i3 & 2048) != 0 ? r402.m : linkedHashMap2, (i3 & 4096) != 0 ? r402.n : aVar, (i3 & 8192) != 0 ? r402.o : s60Arr, (i3 & 16384) != 0 ? r402.p : w404, (i3 & 32768) != 0 ? r402.q : str8, (i3 & 65536) != 0 ? r402.r : w405, (i3 & 131072) != 0 ? r402.s : str9);
    }

    @DexIgnore
    public final r40 a(String str, String str2, String str3, String str4, String str5, String str6, String str7, w40 w40, w40 w402, w40 w403, LinkedHashMap<Short, w40> linkedHashMap, LinkedHashMap<Short, w40> linkedHashMap2, a aVar, s60[] s60Arr, w40 w404, String str8, w40 w405, String str9) {
        return new r40(str, str2, str3, str4, str5, str6, str7, w40, w402, w403, linkedHashMap, linkedHashMap2, aVar, s60Arr, w404, str8, w405, str9);
    }

    @DexIgnore
    public final void a(s40 s40) {
        this.a = s40;
    }

    @DexIgnore
    public final a b() {
        return this.n;
    }

    @DexIgnore
    public final w40 c() {
        return this.i;
    }

    @DexIgnore
    public final LinkedHashMap<Short, w40> d() {
        return this.m;
    }

    @DexIgnore
    public final w40 e() {
        return this.p;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(r40.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            r40 r40 = (r40) obj;
            return !(wg6.a(this.b, r40.b) ^ true) && !(wg6.a(this.c, r40.c) ^ true) && !(wg6.a(this.d, r40.d) ^ true) && !(wg6.a(this.e, r40.e) ^ true) && !(wg6.a(this.f, r40.f) ^ true) && !(wg6.a(this.g, r40.g) ^ true) && !(wg6.a(this.h, r40.h) ^ true) && !(wg6.a(this.i, r40.i) ^ true) && !(wg6.a(this.j, r40.j) ^ true) && !(wg6.a(this.k, r40.k) ^ true) && !(wg6.a(this.l, r40.l) ^ true) && !(wg6.a(this.m, r40.m) ^ true) && this.n == r40.n && Arrays.equals(this.o, r40.o) && !(wg6.a(this.p, r40.p) ^ true) && !(wg6.a(this.q, r40.q) ^ true) && !(wg6.a(this.r, r40.r) ^ true) && !(wg6.a(this.s, r40.s) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.DeviceInformation");
    }

    @DexIgnore
    public final w40 f() {
        return this.k;
    }

    @DexIgnore
    public final String g() {
        return this.h;
    }

    @DexIgnore
    public final s40 getDeviceType() {
        return this.a;
    }

    @DexIgnore
    public final String getFastPairIdInHexString() {
        return this.s;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.f;
    }

    @DexIgnore
    public final String getHardwareRevision() {
        return this.e;
    }

    @DexIgnore
    public final String getLocaleString() {
        return this.q;
    }

    @DexIgnore
    public final x40 getLocaleVersions() {
        w40 w40 = this.m.get((short) 1794);
        if (w40 == null) {
            w40 = mi0.A.g();
        }
        w40 w402 = this.l.get(Short.valueOf(w31.ASSET.a));
        if (w402 == null) {
            w402 = mi0.A.g();
        }
        return new x40(w40, w402);
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.c;
    }

    @DexIgnore
    public final w40 getMicroAppVersion() {
        return this.r;
    }

    @DexIgnore
    public final String getModelNumber() {
        return this.g;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.d;
    }

    @DexIgnore
    public final x40 getWatchParameterVersions() {
        w40 w40 = this.m.get(Short.valueOf(w31.WATCH_PARAMETERS_FILE.a));
        if (w40 == null) {
            w40 = mi0.A.g();
        }
        w40 w402 = this.l.get(Short.valueOf(w31.WATCH_PARAMETERS_FILE.a));
        if (w402 == null) {
            w402 = mi0.A.g();
        }
        return new x40(w40, w402);
    }

    @DexIgnore
    public final s60[] h() {
        return this.o;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        int hashCode6 = this.h.hashCode();
        int hashCode7 = this.i.hashCode();
        int hashCode8 = this.j.hashCode();
        int hashCode9 = this.k.hashCode();
        int hashCode10 = this.l.hashCode();
        int hashCode11 = this.m.hashCode();
        int hashCode12 = this.n.hashCode();
        int hashCode13 = this.p.hashCode();
        int hashCode14 = this.q.hashCode();
        int hashCode15 = this.r.hashCode();
        return this.s.hashCode() + ((hashCode15 + ((hashCode14 + ((hashCode13 + ((((hashCode12 + ((hashCode11 + ((hashCode10 + ((hashCode9 + ((hashCode8 + ((hashCode7 + ((hashCode6 + ((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (this.b.hashCode() * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + Arrays.hashCode(this.o)) * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public final LinkedHashMap<Short, w40> i() {
        return this.l;
    }

    @DexIgnore
    public final w40 j() {
        return this.j;
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = ze0.b("DeviceInformation(name=");
        b2.append(this.b);
        b2.append(", macAddress=");
        b2.append(this.c);
        b2.append(", serialNumber=");
        b2.append(this.d);
        b2.append(", hardwareRevision=");
        b2.append(this.e);
        b2.append(", firmwareVersion=");
        b2.append(this.f);
        b2.append(", modelNumber=");
        b2.append(this.g);
        b2.append(", heartRateSerialNumber=");
        b2.append(this.h);
        b2.append(", bootloaderVersion=");
        b2.append(this.i);
        b2.append(", watchAppVersion=");
        b2.append(this.j);
        b2.append(", fontVersion=");
        b2.append(this.k);
        b2.append(", supportedFilesVersion=");
        b2.append(this.l);
        b2.append(", currentFilesVersion=");
        b2.append(this.m);
        b2.append(", bondRequired=");
        b2.append(this.n);
        b2.append(", supportedDeviceConfigKeys=");
        b2.append(Arrays.toString(this.o));
        b2.append(", deviceSecurityVersion=");
        b2.append(this.p);
        b2.append(", localeString=");
        b2.append(this.q);
        b2.append(", microAppVersion=");
        b2.append(this.r);
        b2.append(", fastPairIdInHexString=");
        b2.append(this.s);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject a2 = cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.NAME, (Object) this.b), bm0.DEVICE_TYPE, (Object) cw0.a((Enum<?>) this.a)), bm0.MAC_ADDRESS, (Object) this.c), bm0.SERIAL_NUMBER, (Object) this.d), bm0.HARDWARE_REVISION, (Object) this.e), bm0.FIRMWARE_VERSION, (Object) this.f), bm0.MODEL_NUMBER, (Object) this.g), bm0.HEART_RATE_SERIAL_NUMBER, (Object) this.h), bm0.BOOT_LOADER_VERSION, (Object) this.i.getShortDescription()), bm0.WATCH_APP_VERSION, (Object) this.j.getShortDescription()), bm0.FONT_VERSION, (Object) this.k.getShortDescription()), bm0.SUPPORTED_FILES_VERSION, (Object) a((HashMap<Short, w40>) this.l)), bm0.CURRENT_FILES_VERSION, (Object) a((HashMap<Short, w40>) this.m));
        bm0 bm0 = bm0.BOND_REQUIRED;
        int i2 = vl1.a[this.n.ordinal()];
        boolean z = true;
        if (i2 == 1) {
            z = false;
        } else if (i2 != 2) {
            throw new kc6();
        }
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(a2, bm0, (Object) Boolean.valueOf(z)), bm0.SUPPORTED_DEVICE_CONFIG, (Object) cw0.a(this.o)), bm0.DEVICE_SECURITY_VERSION, (Object) this.p.getShortDescription()), bm0.LOCALE, (Object) this.q), bm0.MICRO_APP_VERSION, (Object) this.r.getShortDescription()), bm0.FAST_PAIR_ID_HEX_STRING, (Object) this.s);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* synthetic */ c(qg6 qg6) {
        }

        @DexIgnore
        public final boolean a(String str) {
            if (str.length() != 10) {
                return false;
            }
            for (int i = 0; i < str.length(); i++) {
                if (!Character.isLetterOrDigit(str.charAt(i))) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public final r40 a(byte[] bArr) throws IllegalArgumentException {
            a a;
            byte[] bArr2 = bArr;
            r40 r40 = new r40("", "", "", (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262136);
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            r40 r402 = r40;
            int i = 0;
            while (i <= bArr2.length - 3) {
                short s = order.getShort(i);
                byte b = order.get(i + 2);
                b a2 = b.c.a(s);
                int i2 = i + 3;
                int i3 = i2 + b;
                if (bArr2.length < i3) {
                    return r402;
                }
                byte[] a3 = md6.a(bArr2, i2, i3);
                switch (ck1.a[a2.ordinal()]) {
                    case 1:
                        Charset forName = Charset.forName("US-ASCII");
                        wg6.a(forName, "Charset.forName(\"US-ASCII\")");
                        String str = new String(a3, forName);
                        if (a(str)) {
                            r402 = r40.a(r402, (String) null, (String) null, str, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262139);
                            break;
                        } else {
                            throw new IllegalArgumentException("Invalid Serial Number: " + str);
                        }
                    case 2:
                        Charset forName2 = Charset.forName("US-ASCII");
                        wg6.a(forName2, "Charset.forName(\"US-ASCII\")");
                        String str2 = r6;
                        String str3 = new String(a3, forName2);
                        r402 = r40.a(r402, (String) null, (String) null, (String) null, str2, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262135);
                        break;
                    case 3:
                        Charset forName3 = Charset.forName("US-ASCII");
                        wg6.a(forName3, "Charset.forName(\"US-ASCII\")");
                        String str4 = r6;
                        String str5 = new String(a3, forName3);
                        r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, str4, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262127);
                        break;
                    case 4:
                        Charset forName4 = Charset.forName("US-ASCII");
                        wg6.a(forName4, "Charset.forName(\"US-ASCII\")");
                        String str6 = r6;
                        String str7 = new String(a3, forName4);
                        r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, str6, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262111);
                        break;
                    case 5:
                        Charset forName5 = Charset.forName("US-ASCII");
                        wg6.a(forName5, "Charset.forName(\"US-ASCII\")");
                        String str8 = r6;
                        String str9 = new String(a3, forName5);
                        r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, str8, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262079);
                        break;
                    case 6:
                        w40 a4 = w40.CREATOR.a(a3);
                        if (a4 == null) {
                            break;
                        } else {
                            r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, a4, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262015);
                            break;
                        }
                    case 7:
                        w40 a5 = w40.CREATOR.a(a3);
                        if (a5 == null) {
                            break;
                        } else {
                            r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (w40) null, a5, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 261887);
                            break;
                        }
                    case 8:
                        w40 a6 = w40.CREATOR.a(a3);
                        if (a6 == null) {
                            break;
                        } else {
                            r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, a6, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 261631);
                            break;
                        }
                    case 9:
                        w40 a7 = w40.CREATOR.a(a3);
                        if (a7 == null) {
                            break;
                        } else {
                            r402.d().put(Short.valueOf(w31.LUTS_FILE.a), a7);
                            break;
                        }
                    case 10:
                        uh6 a8 = ci6.a(nd6.a(a3), 3);
                        int a9 = a8.a();
                        int b2 = a8.b();
                        int c = a8.c();
                        if (c < 0) {
                            if (a9 < b2) {
                                break;
                            }
                        } else if (a9 > b2) {
                            break;
                        }
                        while (true) {
                            w31 a10 = w31.x.a(a3[a9]);
                            if (a10 != null) {
                                r402.i().put(Short.valueOf(a10.a), new w40(a3[a9 + 1], a3[a9 + 2]));
                                cd6 cd6 = cd6.a;
                            }
                            if (a9 == b2) {
                                break;
                            } else {
                                a9 += c;
                            }
                        }
                    case 11:
                        uh6 a11 = ci6.a(nd6.a(a3), 4);
                        int a12 = a11.a();
                        int b3 = a11.b();
                        int c2 = a11.c();
                        if (c2 < 0) {
                            if (a12 < b3) {
                                break;
                            }
                        } else if (a12 > b3) {
                            break;
                        }
                        while (true) {
                            ByteBuffer order2 = ByteBuffer.wrap(a3).order(ByteOrder.LITTLE_ENDIAN);
                            r402.d().put(Short.valueOf(order2.getShort(a12)), new w40(order2.get(a12 + 2), order2.get(a12 + 3)));
                            if (a12 == b3) {
                                break;
                            } else {
                                a12 += c2;
                            }
                        }
                    case 12:
                        if ((!(a3.length == 0)) && (a = a.c.a(a3[0])) != null) {
                            r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, a, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 258047);
                            cd6 cd62 = cd6.a;
                            break;
                        }
                    case 13:
                        ByteBuffer order3 = ByteBuffer.wrap(a3).order(ByteOrder.LITTLE_ENDIAN);
                        ArrayList arrayList = new ArrayList();
                        uh6 a13 = ci6.a(nd6.a(a3), 2);
                        int a14 = a13.a();
                        int b4 = a13.b();
                        int c3 = a13.c();
                        if (c3 < 0 ? a14 >= b4 : a14 <= b4) {
                            while (true) {
                                s60 a15 = s60.d.a(order3.getShort(a14));
                                if (a15 != null) {
                                    Boolean.valueOf(arrayList.add(a15));
                                }
                                if (a14 != b4) {
                                    a14 += c3;
                                }
                            }
                        }
                        Object[] array = arrayList.toArray(new s60[0]);
                        if (array != null) {
                            r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) array, (w40) null, (String) null, (w40) null, (String) null, 253951);
                            break;
                        } else {
                            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    case 14:
                        w40 a16 = w40.CREATOR.a(a3);
                        if (a16 == null) {
                            break;
                        } else {
                            r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, a16, (String) null, (w40) null, (String) null, 245759);
                            break;
                        }
                    case 16:
                        Charset forName6 = Charset.forName("US-ASCII");
                        wg6.a(forName6, "Charset.forName(\"US-ASCII\")");
                        String str10 = r6;
                        String str11 = new String(a3, forName6);
                        r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, str10, (w40) null, (String) null, 229375);
                        break;
                    case 17:
                        w40 a17 = w40.CREATOR.a(a3);
                        if (a17 == null) {
                            break;
                        } else {
                            r402.d().put((short) 1794, a17);
                            break;
                        }
                    case 18:
                        w40 a18 = w40.CREATOR.a(a3);
                        if (a18 == null) {
                            break;
                        } else {
                            r402 = r40.a(r402, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (a) null, (s60[]) null, (w40) null, (String) null, a18, (String) null, 196607);
                            break;
                        }
                }
                i = b + 3 + i;
            }
            return r402;
        }

        @DexIgnore
        public final r40 a(JSONObject jSONObject) {
            JSONObject jSONObject2 = jSONObject;
            String optString = jSONObject2.optString(cw0.a((Enum<?>) bm0.NAME), "");
            String optString2 = jSONObject2.optString(cw0.a((Enum<?>) bm0.MAC_ADDRESS), "");
            wg6.a(optString, "name");
            if ((optString.length() == 0) || !BluetoothAdapter.checkBluetoothAddress(optString2)) {
                return null;
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            JSONArray optJSONArray = jSONObject2.optJSONArray(cw0.a((Enum<?>) bm0.SUPPORTED_FILES_VERSION));
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i = 0; i < length; i++) {
                    try {
                        JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                        if (optJSONObject != null) {
                            Short valueOf = Short.valueOf(Short.parseShort(optJSONObject.get(cw0.a((Enum<?>) bm0.FILE_HANDLE)).toString()));
                            w40.a aVar = w40.CREATOR;
                            String optString3 = optJSONObject.optString(cw0.a((Enum<?>) bm0.VERSION));
                            wg6.a(optString3, "fileVersionJSON.optStrin\u2026ey.VERSION.lowerCaseName)");
                            linkedHashMap.put(valueOf, aVar.a(optString3));
                        }
                    } catch (JSONException e) {
                        qs0.h.a(e);
                    }
                }
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            JSONArray optJSONArray2 = jSONObject2.optJSONArray(cw0.a((Enum<?>) bm0.CURRENT_FILES_VERSION));
            if (optJSONArray2 != null) {
                int length2 = optJSONArray2.length();
                for (int i2 = 0; i2 < length2; i2++) {
                    try {
                        JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                        if (optJSONObject2 != null) {
                            Short valueOf2 = Short.valueOf(Short.parseShort(optJSONObject2.get(cw0.a((Enum<?>) bm0.FILE_HANDLE)).toString()));
                            w40.a aVar2 = w40.CREATOR;
                            String optString4 = optJSONObject2.optString(cw0.a((Enum<?>) bm0.VERSION));
                            wg6.a(optString4, "fileVersionJSON.optStrin\u2026ey.VERSION.lowerCaseName)");
                            linkedHashMap2.put(valueOf2, aVar2.a(optString4));
                        }
                    } catch (JSONException e2) {
                        qs0.h.a(e2);
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray3 = jSONObject2.optJSONArray(cw0.a((Enum<?>) bm0.SUPPORTED_DEVICE_CONFIG));
            if (optJSONArray3 != null) {
                int length3 = optJSONArray3.length();
                for (int i3 = 0; i3 < length3; i3++) {
                    try {
                        s60 a = s60.d.a(optJSONArray3.get(i3).toString());
                        if (a != null) {
                            arrayList.add(a);
                        }
                    } catch (JSONException e3) {
                        qs0.h.a(e3);
                    }
                }
            }
            wg6.a(optString2, "macAddress");
            String optString5 = jSONObject2.optString(cw0.a((Enum<?>) bm0.SERIAL_NUMBER), "");
            wg6.a(optString5, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            String optString6 = jSONObject2.optString(cw0.a((Enum<?>) bm0.HARDWARE_REVISION), "");
            wg6.a(optString6, "jsonObject.optString(JSO\u2026VISION.lowerCaseName, \"\")");
            String optString7 = jSONObject2.optString(cw0.a((Enum<?>) bm0.FIRMWARE_VERSION), "");
            wg6.a(optString7, "jsonObject.optString(JSO\u2026ERSION.lowerCaseName, \"\")");
            String optString8 = jSONObject2.optString(cw0.a((Enum<?>) bm0.MODEL_NUMBER), "");
            wg6.a(optString8, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            String optString9 = jSONObject2.optString(cw0.a((Enum<?>) bm0.HEART_RATE_SERIAL_NUMBER), "");
            wg6.a(optString9, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            w40.a aVar3 = w40.CREATOR;
            String optString10 = jSONObject2.optString(cw0.a((Enum<?>) bm0.BOOT_LOADER_VERSION), "");
            wg6.a(optString10, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            w40 a2 = aVar3.a(optString10);
            w40.a aVar4 = w40.CREATOR;
            String optString11 = jSONObject2.optString(cw0.a((Enum<?>) bm0.WATCH_APP_VERSION), "");
            wg6.a(optString11, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            w40 a3 = aVar4.a(optString11);
            w40.a aVar5 = w40.CREATOR;
            String optString12 = jSONObject2.optString(cw0.a((Enum<?>) bm0.FONT_VERSION), "");
            wg6.a(optString12, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            w40 a4 = aVar5.a(optString12);
            LinkedHashMap linkedHashMap3 = linkedHashMap2;
            a a5 = a.c.a((byte) jSONObject2.optInt(cw0.a((Enum<?>) bm0.BOND_REQUIRED), 0));
            if (a5 == null) {
                a5 = a.NO_REQUIRE;
            }
            a aVar6 = a5;
            Object[] array = arrayList.toArray(new s60[0]);
            if (array != null) {
                w40.a aVar7 = w40.CREATOR;
                String optString13 = jSONObject2.optString(cw0.a((Enum<?>) bm0.DEVICE_SECURITY_VERSION), "");
                wg6.a(optString13, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
                w40 a6 = aVar7.a(optString13);
                String optString14 = jSONObject2.optString(cw0.a((Enum<?>) bm0.LOCALE), "en_US");
                String str = optString14;
                wg6.a(optString14, "jsonObject.optString(JSO\u2026nt.DEFAULT_LOCALE_STRING)");
                w40.a aVar8 = w40.CREATOR;
                String optString15 = jSONObject2.optString(cw0.a((Enum<?>) bm0.MICRO_APP_VERSION), "");
                wg6.a(optString15, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
                w40 a7 = aVar8.a(optString15);
                String optString16 = jSONObject2.optString(cw0.a((Enum<?>) bm0.FAST_PAIR_ID_HEX_STRING), "");
                wg6.a(optString16, "jsonObject.optString(JSO\u2026STRING.lowerCaseName, \"\")");
                return new r40(optString, optString2, optString5, optString6, optString7, optString8, optString9, a2, a3, a4, linkedHashMap, linkedHashMap3, aVar6, (s60[]) array, a6, str, a7, optString16);
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final r40 a(r40 r40, r40 r402) {
            String str;
            String str2;
            String str3;
            String str4;
            String str5;
            String str6;
            String str7;
            w40 w40;
            w40 w402;
            w40 w403;
            LinkedHashMap<Short, w40> linkedHashMap;
            LinkedHashMap<Short, w40> linkedHashMap2;
            s60[] s60Arr;
            w40 w404;
            String str8;
            w40 w405;
            String str9;
            if (r402.getName().length() > 0) {
                str = r402.getName();
            } else {
                str = r40.getName();
            }
            String str10 = str;
            if (r402.getMacAddress().length() > 0) {
                str2 = r402.getMacAddress();
            } else {
                str2 = r40.getMacAddress();
            }
            String str11 = str2;
            if (a(r402.getSerialNumber())) {
                str3 = r402.getSerialNumber();
            } else {
                str3 = r40.getSerialNumber();
            }
            String str12 = str3;
            if (r402.getHardwareRevision().length() > 0) {
                str4 = r402.getHardwareRevision();
            } else {
                str4 = r40.getHardwareRevision();
            }
            String str13 = str4;
            if (r402.getFirmwareVersion().length() > 0) {
                str5 = r402.getFirmwareVersion();
            } else {
                str5 = r40.getFirmwareVersion();
            }
            String str14 = str5;
            if (r402.getModelNumber().length() > 0) {
                str6 = r402.getModelNumber();
            } else {
                str6 = r40.getModelNumber();
            }
            String str15 = str6;
            if (r402.g().length() > 0) {
                str7 = r402.g();
            } else {
                str7 = r40.g();
            }
            String str16 = str7;
            if (!wg6.a(r402.c(), new w40((byte) 0, (byte) 0))) {
                w40 = r402.c();
            } else {
                w40 = r40.c();
            }
            w40 w406 = w40;
            if (!wg6.a(r402.j(), new w40((byte) 0, (byte) 0))) {
                w402 = r402.j();
            } else {
                w402 = r40.j();
            }
            w40 w407 = w402;
            if (!wg6.a(r402.f(), new w40((byte) 0, (byte) 0))) {
                w403 = r402.f();
            } else {
                w403 = r40.f();
            }
            w40 w408 = w403;
            if (!r402.i().isEmpty()) {
                linkedHashMap = r402.i();
            } else {
                linkedHashMap = r40.i();
            }
            LinkedHashMap<Short, w40> linkedHashMap3 = linkedHashMap;
            if (!r402.d().isEmpty()) {
                linkedHashMap2 = r402.d();
            } else {
                linkedHashMap2 = r40.d();
            }
            a b = r402.b();
            if (!(r402.h().length == 0)) {
                s60Arr = r402.h();
            } else {
                s60Arr = r40.h();
            }
            s60[] s60Arr2 = s60Arr;
            if (!wg6.a(r402.e(), new w40((byte) 0, (byte) 0))) {
                w404 = r402.e();
            } else {
                w404 = r40.e();
            }
            w40 w409 = w404;
            if (!wg6.a(r402.getLocaleString(), "en_US")) {
                str8 = r402.getLocaleString();
            } else {
                str8 = r40.getLocaleString();
            }
            if (!wg6.a(r402.getMicroAppVersion(), new w40((byte) 0, (byte) 0))) {
                w405 = r402.getMicroAppVersion();
            } else {
                w405 = r40.getMicroAppVersion();
            }
            w40 w4010 = w405;
            if (!wg6.a(r402.getFastPairIdInHexString(), "")) {
                str9 = r402.getFastPairIdInHexString();
            } else {
                str9 = r40.getFastPairIdInHexString();
            }
            return new r40(str10, str11, str12, str13, str14, str15, str16, w406, w407, w408, linkedHashMap3, linkedHashMap2, b, s60Arr2, w409, str8, w4010, str9);
        }
    }

    @DexIgnore
    public r40(String str, String str2, String str3, String str4, String str5, String str6, String str7, w40 w40, w40 w402, w40 w403, LinkedHashMap<Short, w40> linkedHashMap, LinkedHashMap<Short, w40> linkedHashMap2, a aVar, s60[] s60Arr, w40 w404, String str8, w40 w405, String str9) {
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = w40;
        this.j = w402;
        this.k = w403;
        this.l = linkedHashMap;
        this.m = linkedHashMap2;
        this.n = aVar;
        this.o = s60Arr;
        this.p = w404;
        this.q = str8;
        this.r = w405;
        this.s = str9;
        this.a = s40.c.a(this.g, this.d);
    }

    @DexIgnore
    public final JSONArray a(HashMap<Short, w40> hashMap) {
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry next : hashMap.entrySet()) {
            jSONArray.put(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.FILE_HANDLE, (Object) cw0.a(((Number) next.getKey()).shortValue())), bm0.FILE_HANDLE_DESCRIPTION, (Object) g01.d.a(((Number) next.getKey()).shortValue())), bm0.VERSION, (Object) ((w40) next.getValue()).toString()));
        }
        return jSONArray;
    }
}
