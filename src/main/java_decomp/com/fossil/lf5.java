package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lf5 {
    @DexIgnore
    public /* final */ gf5 a;
    @DexIgnore
    public /* final */ vf5 b;
    @DexIgnore
    public /* final */ qf5 c;

    @DexIgnore
    public lf5(gf5 gf5, vf5 vf5, qf5 qf5) {
        wg6.b(gf5, "mHeartRateOverviewDayView");
        wg6.b(vf5, "mHeartRateOverviewWeekView");
        wg6.b(qf5, "mHeartRateOverviewMonthView");
        this.a = gf5;
        this.b = vf5;
        this.c = qf5;
    }

    @DexIgnore
    public final gf5 a() {
        return this.a;
    }

    @DexIgnore
    public final qf5 b() {
        return this.c;
    }

    @DexIgnore
    public final vf5 c() {
        return this.b;
    }
}
