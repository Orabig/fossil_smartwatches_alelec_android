package com.fossil;

import java.io.IOException;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rq6 {
    @DexIgnore
    public /* final */ br6 a;
    @DexIgnore
    public /* final */ gq6 b;
    @DexIgnore
    public /* final */ List<Certificate> c;
    @DexIgnore
    public /* final */ List<Certificate> d;

    @DexIgnore
    public rq6(br6 br6, gq6 gq6, List<Certificate> list, List<Certificate> list2) {
        this.a = br6;
        this.b = gq6;
        this.c = list;
        this.d = list2;
    }

    @DexIgnore
    public static rq6 a(SSLSession sSLSession) throws IOException {
        Certificate[] certificateArr;
        List list;
        List list2;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        } else if (!"SSL_NULL_WITH_NULL_NULL".equals(cipherSuite)) {
            gq6 a2 = gq6.a(cipherSuite);
            String protocol = sSLSession.getProtocol();
            if (protocol == null) {
                throw new IllegalStateException("tlsVersion == null");
            } else if (!"NONE".equals(protocol)) {
                br6 forJavaName = br6.forJavaName(protocol);
                try {
                    certificateArr = sSLSession.getPeerCertificates();
                } catch (SSLPeerUnverifiedException unused) {
                    certificateArr = null;
                }
                if (certificateArr != null) {
                    list = fr6.a((T[]) certificateArr);
                } else {
                    list = Collections.emptyList();
                }
                Certificate[] localCertificates = sSLSession.getLocalCertificates();
                if (localCertificates != null) {
                    list2 = fr6.a((T[]) localCertificates);
                } else {
                    list2 = Collections.emptyList();
                }
                return new rq6(forJavaName, a2, list, list2);
            } else {
                throw new IOException("tlsVersion == NONE");
            }
        } else {
            throw new IOException("cipherSuite == SSL_NULL_WITH_NULL_NULL");
        }
    }

    @DexIgnore
    public List<Certificate> b() {
        return this.d;
    }

    @DexIgnore
    public List<Certificate> c() {
        return this.c;
    }

    @DexIgnore
    public br6 d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof rq6)) {
            return false;
        }
        rq6 rq6 = (rq6) obj;
        if (!this.a.equals(rq6.a) || !this.b.equals(rq6.b) || !this.c.equals(rq6.c) || !this.d.equals(rq6.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    public static rq6 a(br6 br6, gq6 gq6, List<Certificate> list, List<Certificate> list2) {
        if (br6 == null) {
            throw new NullPointerException("tlsVersion == null");
        } else if (gq6 != null) {
            return new rq6(br6, gq6, fr6.a(list), fr6.a(list2));
        } else {
            throw new NullPointerException("cipherSuite == null");
        }
    }

    @DexIgnore
    public gq6 a() {
        return this.b;
    }
}
