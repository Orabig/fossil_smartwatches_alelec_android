package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cq0 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ h21 a;
    @DexIgnore
    public /* final */ /* synthetic */ short b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cq0(h21 h21, short s) {
        super(1);
        this.a = h21;
        this.b = s;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        byte[] bArr = ((kz0) obj).O;
        if (bArr.length == 0) {
            h21 h21 = this.a;
            h21.a(km1.a(h21.v, (eh1) null, sk1.INVALID_RESPONSE, (bn0) null, 5));
        } else {
            h21 h212 = this.a;
            h212.F.add(new ie1(h212.w.t, new g01(this.b).a, new g01(this.b).b, bArr, (long) bArr.length, h51.a.a(bArr, q11.CRC32), this.a.g(), true));
            h21.d(this.a);
        }
        return cd6.a;
    }
}
