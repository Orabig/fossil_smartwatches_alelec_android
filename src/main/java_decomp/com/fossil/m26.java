package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class m26 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore
    public void a(Bundle bundle) {
        this.a = g26.a(bundle, "_wxapi_basereq_transaction");
        this.b = g26.a(bundle, "_wxapi_basereq_openid");
    }

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public void b(Bundle bundle) {
        bundle.putInt("_wxapi_command_type", b());
        bundle.putString("_wxapi_basereq_transaction", this.a);
        bundle.putString("_wxapi_basereq_openid", this.b);
    }
}
