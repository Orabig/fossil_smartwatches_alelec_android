package com.fossil;

import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class hr3 implements ic3 {
    @DexIgnore
    public /* final */ ir3 a;
    @DexIgnore
    public /* final */ Pair b;

    @DexIgnore
    public hr3(ir3 ir3, Pair pair) {
        this.a = ir3;
        this.b = pair;
    }

    @DexIgnore
    public final Object then(qc3 qc3) {
        this.a.a(this.b, qc3);
        return qc3;
    }
}
