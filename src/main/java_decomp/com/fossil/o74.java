package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o74 extends n74 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D; // = new SparseIntArray();
    @DexIgnore
    public long B;

    /*
    static {
        D.put(2131362026, 1);
        D.put(2131362869, 2);
        D.put(2131362870, 3);
        D.put(2131363205, 4);
        D.put(2131362024, 5);
        D.put(2131362562, 6);
        D.put(2131363110, 7);
        D.put(2131362991, 8);
        D.put(2131363191, 9);
        D.put(2131363109, 10);
        D.put(2131363107, 11);
    }
    */

    @DexIgnore
    public o74(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 12, C, D));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public o74(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[5], objArr[1], objArr[6], objArr[0], objArr[2], objArr[3], objArr[8], objArr[11], objArr[10], objArr[7], objArr[9], objArr[4]);
        this.B = -1;
        this.s.setTag((Object) null);
        a(view);
        f();
    }
}
