package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct3 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ct3> CREATOR; // = new qt3();
    @DexIgnore
    public Bundle a;
    @DexIgnore
    public Map<String, String> b;
    @DexIgnore
    public a c;

    @DexIgnore
    public ct3(Bundle bundle) {
        this.a = bundle;
    }

    @DexIgnore
    public final String B() {
        return this.a.getString("from");
    }

    @DexIgnore
    public final a C() {
        if (this.c == null && ot3.a(this.a)) {
            this.c = new a(new ot3(this.a));
        }
        return this.c;
    }

    @DexIgnore
    public final Map<String, String> p() {
        if (this.b == null) {
            Bundle bundle = this.a;
            p4 p4Var = new p4();
            for (String str : bundle.keySet()) {
                Object obj = bundle.get(str);
                if (obj instanceof String) {
                    String str2 = (String) obj;
                    if (!str.startsWith("google.") && !str.startsWith("gcm.") && !str.equals("from") && !str.equals("message_type") && !str.equals("collapse_key")) {
                        p4Var.put(str, str2);
                    }
                }
            }
            this.b = p4Var;
        }
        return this.b;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, this.a, false);
        g22.a(parcel, a2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public a(ot3 ot3) {
            this.a = ot3.a("gcm.n.title");
            ot3.e("gcm.n.title");
            a(ot3, "gcm.n.title");
            this.b = ot3.a("gcm.n.body");
            ot3.e("gcm.n.body");
            a(ot3, "gcm.n.body");
            ot3.a("gcm.n.icon");
            ot3.b();
            ot3.a("gcm.n.tag");
            ot3.a("gcm.n.color");
            ot3.a("gcm.n.click_action");
            ot3.a("gcm.n.android_channel_id");
            ot3.a();
            ot3.a("gcm.n.image");
            ot3.a("gcm.n.ticker");
            ot3.c("gcm.n.notification_priority");
            ot3.c("gcm.n.visibility");
            ot3.c("gcm.n.notification_count");
            ot3.b("gcm.n.sticky");
            ot3.b("gcm.n.local_only");
            ot3.b("gcm.n.default_sound");
            ot3.b("gcm.n.default_vibrate_timings");
            ot3.b("gcm.n.default_light_settings");
            ot3.d("gcm.n.event_time");
            ot3.d();
            ot3.c();
        }

        @DexIgnore
        public static String[] a(ot3 ot3, String str) {
            Object[] f = ot3.f(str);
            if (f == null) {
                return null;
            }
            String[] strArr = new String[f.length];
            for (int i = 0; i < f.length; i++) {
                strArr[i] = String.valueOf(f[i]);
            }
            return strArr;
        }

        @DexIgnore
        public String b() {
            return this.a;
        }

        @DexIgnore
        public String a() {
            return this.b;
        }
    }
}
