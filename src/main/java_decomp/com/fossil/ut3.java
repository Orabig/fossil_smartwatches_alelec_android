package com.fossil;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ut3 {
    @DexIgnore
    public static volatile ut3 b;
    @DexIgnore
    public /* final */ Set<vt3> a; // = new HashSet();

    @DexIgnore
    public static ut3 b() {
        ut3 ut3 = b;
        if (ut3 == null) {
            synchronized (ut3.class) {
                ut3 = b;
                if (ut3 == null) {
                    ut3 = new ut3();
                    b = ut3;
                }
            }
        }
        return ut3;
    }

    @DexIgnore
    public Set<vt3> a() {
        Set<vt3> unmodifiableSet;
        synchronized (this.a) {
            unmodifiableSet = Collections.unmodifiableSet(this.a);
        }
        return unmodifiableSet;
    }
}
