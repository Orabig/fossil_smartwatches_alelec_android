package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ pr0 CREATOR; // = new pr0((qg6) null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public it0(int i, int i2, int i3, int i4) {
        boolean z = false;
        if (true == (i < 6)) {
            i = 6;
        } else {
            if (true == (i > 3200)) {
                i = 3200;
            }
        }
        this.a = i;
        this.b = (this.a > i2 || 3200 < i2) ? this.a : i2;
        int i5 = 499;
        if (true == (i3 < 0)) {
            i5 = 0;
        } else {
            if (true != (i3 > 499)) {
                i5 = i3;
            }
        }
        this.c = i5;
        int i6 = 10;
        if (true != (i4 < 10)) {
            i6 = true == (i4 > 3200 ? true : z) ? 3200 : i4;
        }
        this.d = i6;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.MIN_INTERVAL, (Object) Integer.valueOf(this.a)), bm0.MAX_INTERVAL, (Object) Integer.valueOf(this.b)), bm0.LATENCY, (Object) Integer.valueOf(this.c)), bm0.TIMEOUT, (Object) Integer.valueOf(this.d));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
    }
}
