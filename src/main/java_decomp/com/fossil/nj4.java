package com.fossil;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.fossil.fs;
import com.fossil.jv;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj4 implements jv<oj4, InputStream> {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ a b; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return nj4.a;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements kv<oj4, InputStream> {
        @DexIgnore
        public nj4 a(nv nvVar) {
            wg6.b(nvVar, "multiFactory");
            return new nj4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements fs<InputStream> {
        @DexIgnore
        public volatile boolean a;
        @DexIgnore
        public /* final */ oj4 b;

        @DexIgnore
        public c(nj4 nj4, oj4 oj4) {
            this.b = oj4;
        }

        @DexIgnore
        public void a() {
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v11, resolved type: com.fossil.wearables.fsl.contact.ContactGroup} */
        /* JADX WARNING: type inference failed for: r6v8, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r5v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r7v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r7v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: Can't wrap try/catch for region: R(2:38|39) */
        /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
            r4 = com.portfolio.platform.PortfolioApp.get.instance().getPackageManager().getApplicationIcon(r4.packageName);
         */
        @DexIgnore
        /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x00e1 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x023f  */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x024b  */
        public void a(br brVar, fs.a<? super InputStream> aVar) {
            List<BaseFeatureModel> a2;
            ByteArrayInputStream byteArrayInputStream;
            Bitmap bitmap;
            Drawable drawable;
            Bitmap bitmap2;
            wg6.b(brVar, "priority");
            wg6.b(aVar, Constants.CALLBACK);
            ArrayList<Bitmap> arrayList = new ArrayList<>(5);
            oj4 oj4 = this.b;
            if (oj4 != null && (a2 = oj4.a()) != null) {
                Iterator<BaseFeatureModel> it = a2.iterator();
                int i = 0;
                while (true) {
                    byteArrayInputStream = null;
                    if (!it.hasNext()) {
                        break;
                    }
                    ContactGroup contactGroup = (BaseFeatureModel) it.next();
                    if (contactGroup instanceof ContactGroup) {
                        for (Contact contact : contactGroup.getContacts()) {
                            if (arrayList.size() < 5) {
                                wg6.a((Object) contact, "contactItem");
                                if (!TextUtils.isEmpty(contact.getPhotoThumbUri())) {
                                    bitmap2 = cx5.a(Long.valueOf((long) contact.getContactId()));
                                } else if (contact.getContactId() == -100) {
                                    bitmap2 = cx5.a(w6.c(PortfolioApp.get.instance(), 2131231131));
                                } else if (contact.getContactId() == -200) {
                                    bitmap2 = cx5.a(w6.c(PortfolioApp.get.instance(), 2131231132));
                                } else {
                                    bitmap2 = cx5.a(contact);
                                }
                                if (bitmap2 != null) {
                                    arrayList.add(bitmap2);
                                }
                            }
                            i++;
                        }
                    }
                    if (contactGroup instanceof AppFilter) {
                        if (arrayList.size() < 5) {
                            PackageManager packageManager = PortfolioApp.get.instance().getPackageManager();
                            try {
                                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(((AppFilter) contactGroup).getType(), Logger.DEFAULT_FULL_MESSAGE_LENGTH);
                                wg6.a((Object) applicationInfo, "packageManager.getApplic\u2026ageManager.GET_META_DATA)");
                                if (applicationInfo.icon != 0) {
                                    drawable = packageManager.getResourcesForApplication(applicationInfo).getDrawableForDensity(applicationInfo.icon, 480, (Resources.Theme) null);
                                } else {
                                    drawable = PortfolioApp.get.instance().getPackageManager().getApplicationIcon(applicationInfo.packageName);
                                }
                                Bitmap a3 = cx5.a(drawable);
                                if (a3 != null) {
                                    arrayList.add(a3);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        i++;
                    }
                }
                FLogger.INSTANCE.getLocal().d(nj4.b.a(), "loadData: itemCounter = " + i);
                ArrayList arrayList2 = new ArrayList(rd6.a(arrayList, 10));
                for (Bitmap bitmap3 : arrayList) {
                    arrayList2.add(Integer.valueOf(Math.max(bitmap3.getWidth(), bitmap3.getHeight())));
                }
                Integer num = (Integer) yd6.g(arrayList2);
                int intValue = num != null ? num.intValue() : 0;
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 3) {
                                if (i != 4) {
                                    StringBuilder sb = new StringBuilder();
                                    sb.append('+');
                                    sb.append((i - 4) + 1);
                                    Bitmap a4 = cx5.a(sb.toString());
                                    Bitmap a5 = cx5.a((Bitmap) arrayList.get(0), (Bitmap) arrayList.get(1), (Bitmap) arrayList.get(2), a4, intValue);
                                    if (a4 != null) {
                                        arrayList.add(a4);
                                    }
                                    bitmap = a5;
                                } else if (arrayList.size() > 3) {
                                    bitmap = cx5.a((Bitmap) arrayList.get(0), (Bitmap) arrayList.get(1), (Bitmap) arrayList.get(2), (Bitmap) arrayList.get(3), intValue);
                                }
                                FLogger.INSTANCE.getLocal().d(nj4.b.a(), "loadData: result = " + bitmap);
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                if (bitmap != null) {
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                                }
                                if (!this.a) {
                                    byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                                }
                                aVar.a(byteArrayInputStream);
                            } else if (arrayList.size() > 2) {
                                bitmap = cx5.a((Bitmap) arrayList.get(0), (Bitmap) arrayList.get(1), (Bitmap) arrayList.get(2), intValue);
                                FLogger.INSTANCE.getLocal().d(nj4.b.a(), "loadData: result = " + bitmap);
                                ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                                if (bitmap != null) {
                                }
                                if (!this.a) {
                                }
                                aVar.a(byteArrayInputStream);
                            }
                        } else if (arrayList.size() > 1) {
                            bitmap = cx5.a((Bitmap) arrayList.get(0), (Bitmap) arrayList.get(1), intValue);
                            FLogger.INSTANCE.getLocal().d(nj4.b.a(), "loadData: result = " + bitmap);
                            ByteArrayOutputStream byteArrayOutputStream22 = new ByteArrayOutputStream();
                            if (bitmap != null) {
                            }
                            if (!this.a) {
                            }
                            aVar.a(byteArrayInputStream);
                        }
                    } else if (arrayList.size() > 0) {
                        bitmap = (Bitmap) arrayList.get(0);
                        FLogger.INSTANCE.getLocal().d(nj4.b.a(), "loadData: result = " + bitmap);
                        ByteArrayOutputStream byteArrayOutputStream222 = new ByteArrayOutputStream();
                        if (bitmap != null) {
                        }
                        if (!this.a) {
                        }
                        aVar.a(byteArrayInputStream);
                    }
                }
                bitmap = null;
                FLogger.INSTANCE.getLocal().d(nj4.b.a(), "loadData: result = " + bitmap);
                ByteArrayOutputStream byteArrayOutputStream2222 = new ByteArrayOutputStream();
                if (bitmap != null) {
                }
                if (!this.a) {
                }
                aVar.a(byteArrayInputStream);
            }
        }

        @DexIgnore
        public pr b() {
            return pr.LOCAL;
        }

        @DexIgnore
        public void cancel() {
            this.a = true;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    /*
    static {
        String simpleName = nj4.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationLoader::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public boolean a(oj4 oj4) {
        wg6.b(oj4, "notificationModel");
        return true;
    }

    @DexIgnore
    public jv.a<InputStream> a(oj4 oj4, int i, int i2, xr xrVar) {
        wg6.b(oj4, "notificationModel");
        wg6.b(xrVar, "options");
        return new jv.a<>(oj4, new c(this, oj4));
    }
}
