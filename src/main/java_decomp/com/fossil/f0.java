package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f0 {
    @DexIgnore
    public static /* final */ int accessibility_action_clickable_span; // = 2131361817;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_0; // = 2131361818;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_1; // = 2131361819;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_10; // = 2131361820;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_11; // = 2131361821;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_12; // = 2131361822;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_13; // = 2131361823;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_14; // = 2131361824;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_15; // = 2131361825;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_16; // = 2131361826;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_17; // = 2131361827;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_18; // = 2131361828;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_19; // = 2131361829;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_2; // = 2131361830;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_20; // = 2131361831;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_21; // = 2131361832;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_22; // = 2131361833;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_23; // = 2131361834;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_24; // = 2131361835;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_25; // = 2131361836;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_26; // = 2131361837;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_27; // = 2131361838;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_28; // = 2131361839;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_29; // = 2131361840;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_3; // = 2131361841;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_30; // = 2131361842;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_31; // = 2131361843;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_4; // = 2131361844;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_5; // = 2131361845;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_6; // = 2131361846;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_7; // = 2131361847;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_8; // = 2131361848;
    @DexIgnore
    public static /* final */ int accessibility_custom_action_9; // = 2131361849;
    @DexIgnore
    public static /* final */ int action_bar; // = 2131361857;
    @DexIgnore
    public static /* final */ int action_bar_activity_content; // = 2131361858;
    @DexIgnore
    public static /* final */ int action_bar_container; // = 2131361859;
    @DexIgnore
    public static /* final */ int action_bar_root; // = 2131361860;
    @DexIgnore
    public static /* final */ int action_bar_spinner; // = 2131361861;
    @DexIgnore
    public static /* final */ int action_bar_subtitle; // = 2131361862;
    @DexIgnore
    public static /* final */ int action_bar_title; // = 2131361863;
    @DexIgnore
    public static /* final */ int action_container; // = 2131361864;
    @DexIgnore
    public static /* final */ int action_context_bar; // = 2131361865;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361867;
    @DexIgnore
    public static /* final */ int action_menu_divider; // = 2131361868;
    @DexIgnore
    public static /* final */ int action_menu_presenter; // = 2131361869;
    @DexIgnore
    public static /* final */ int action_mode_bar; // = 2131361870;
    @DexIgnore
    public static /* final */ int action_mode_bar_stub; // = 2131361871;
    @DexIgnore
    public static /* final */ int action_mode_close_button; // = 2131361872;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361873;
    @DexIgnore
    public static /* final */ int actions; // = 2131361874;
    @DexIgnore
    public static /* final */ int activity_chooser_view_content; // = 2131361875;
    @DexIgnore
    public static /* final */ int add; // = 2131361881;
    @DexIgnore
    public static /* final */ int alertTitle; // = 2131361884;
    @DexIgnore
    public static /* final */ int async; // = 2131361894;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361914;
    @DexIgnore
    public static /* final */ int buttonPanel; // = 2131361963;
    @DexIgnore
    public static /* final */ int checkbox; // = 2131361995;
    @DexIgnore
    public static /* final */ int checked; // = 2131361996;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131361999;
    @DexIgnore
    public static /* final */ int content; // = 2131362119;
    @DexIgnore
    public static /* final */ int contentPanel; // = 2131362120;
    @DexIgnore
    public static /* final */ int custom; // = 2131362131;
    @DexIgnore
    public static /* final */ int customPanel; // = 2131362132;
    @DexIgnore
    public static /* final */ int decor_content_parent; // = 2131362158;
    @DexIgnore
    public static /* final */ int default_activity_button; // = 2131362159;
    @DexIgnore
    public static /* final */ int dialog_button; // = 2131362165;
    @DexIgnore
    public static /* final */ int edit_query; // = 2131362176;
    @DexIgnore
    public static /* final */ int expand_activities_button; // = 2131362200;
    @DexIgnore
    public static /* final */ int expanded_menu; // = 2131362201;
    @DexIgnore
    public static /* final */ int forever; // = 2131362262;
    @DexIgnore
    public static /* final */ int group_divider; // = 2131362465;
    @DexIgnore
    public static /* final */ int home; // = 2131362475;
    @DexIgnore
    public static /* final */ int icon; // = 2131362499;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362500;
    @DexIgnore
    public static /* final */ int image; // = 2131362507;
    @DexIgnore
    public static /* final */ int info; // = 2131362512;
    @DexIgnore
    public static /* final */ int italic; // = 2131362524;
    @DexIgnore
    public static /* final */ int line1; // = 2131362654;
    @DexIgnore
    public static /* final */ int line3; // = 2131362655;
    @DexIgnore
    public static /* final */ int listMode; // = 2131362662;
    @DexIgnore
    public static /* final */ int list_item; // = 2131362663;
    @DexIgnore
    public static /* final */ int message; // = 2131362702;
    @DexIgnore
    public static /* final */ int multiply; // = 2131362733;
    @DexIgnore
    public static /* final */ int none; // = 2131362739;
    @DexIgnore
    public static /* final */ int normal; // = 2131362740;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362741;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362742;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362743;
    @DexIgnore
    public static /* final */ int off; // = 2131362759;
    @DexIgnore
    public static /* final */ int on; // = 2131362760;
    @DexIgnore
    public static /* final */ int parentPanel; // = 2131362774;
    @DexIgnore
    public static /* final */ int progress_circular; // = 2131362823;
    @DexIgnore
    public static /* final */ int progress_horizontal; // = 2131362824;
    @DexIgnore
    public static /* final */ int radio; // = 2131362827;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362838;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362839;
    @DexIgnore
    public static /* final */ int screen; // = 2131362909;
    @DexIgnore
    public static /* final */ int scrollIndicatorDown; // = 2131362911;
    @DexIgnore
    public static /* final */ int scrollIndicatorUp; // = 2131362912;
    @DexIgnore
    public static /* final */ int scrollView; // = 2131362913;
    @DexIgnore
    public static /* final */ int search_badge; // = 2131362917;
    @DexIgnore
    public static /* final */ int search_bar; // = 2131362918;
    @DexIgnore
    public static /* final */ int search_button; // = 2131362919;
    @DexIgnore
    public static /* final */ int search_close_btn; // = 2131362920;
    @DexIgnore
    public static /* final */ int search_edit_frame; // = 2131362921;
    @DexIgnore
    public static /* final */ int search_go_btn; // = 2131362922;
    @DexIgnore
    public static /* final */ int search_mag_icon; // = 2131362923;
    @DexIgnore
    public static /* final */ int search_plate; // = 2131362924;
    @DexIgnore
    public static /* final */ int search_src_text; // = 2131362925;
    @DexIgnore
    public static /* final */ int search_voice_btn; // = 2131362927;
    @DexIgnore
    public static /* final */ int select_dialog_listview; // = 2131362931;
    @DexIgnore
    public static /* final */ int shortcut; // = 2131362944;
    @DexIgnore
    public static /* final */ int spacer; // = 2131362961;
    @DexIgnore
    public static /* final */ int split_action_bar; // = 2131362963;
    @DexIgnore
    public static /* final */ int src_atop; // = 2131362967;
    @DexIgnore
    public static /* final */ int src_in; // = 2131362968;
    @DexIgnore
    public static /* final */ int src_over; // = 2131362969;
    @DexIgnore
    public static /* final */ int submenuarrow; // = 2131362975;
    @DexIgnore
    public static /* final */ int submit_area; // = 2131362976;
    @DexIgnore
    public static /* final */ int tabMode; // = 2131362994;
    @DexIgnore
    public static /* final */ int tag_accessibility_actions; // = 2131362996;
    @DexIgnore
    public static /* final */ int tag_accessibility_clickable_spans; // = 2131362997;
    @DexIgnore
    public static /* final */ int tag_accessibility_heading; // = 2131362998;
    @DexIgnore
    public static /* final */ int tag_accessibility_pane_title; // = 2131362999;
    @DexIgnore
    public static /* final */ int tag_screen_reader_focusable; // = 2131363000;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131363001;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131363002;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131363003;
    @DexIgnore
    public static /* final */ int text; // = 2131363009;
    @DexIgnore
    public static /* final */ int text2; // = 2131363010;
    @DexIgnore
    public static /* final */ int textSpacerNoButtons; // = 2131363012;
    @DexIgnore
    public static /* final */ int textSpacerNoTitle; // = 2131363013;
    @DexIgnore
    public static /* final */ int time; // = 2131363031;
    @DexIgnore
    public static /* final */ int title; // = 2131363033;
    @DexIgnore
    public static /* final */ int titleDividerNoCustom; // = 2131363034;
    @DexIgnore
    public static /* final */ int title_template; // = 2131363036;
    @DexIgnore
    public static /* final */ int topPanel; // = 2131363042;
    @DexIgnore
    public static /* final */ int unchecked; // = 2131363243;
    @DexIgnore
    public static /* final */ int uniform; // = 2131363245;
    @DexIgnore
    public static /* final */ int up; // = 2131363248;
    @DexIgnore
    public static /* final */ int wrap_content; // = 2131363348;
}
