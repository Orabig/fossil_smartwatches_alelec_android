package com.fossil;

import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface o85 extends k24<n85> {
    @DexIgnore
    void a(WatchApp watchApp);

    @DexIgnore
    void b(String str);

    @DexIgnore
    void b(List<lc6<WatchApp, String>> list);

    @DexIgnore
    void d(List<lc6<WatchApp, String>> list);

    @DexIgnore
    void u();
}
