package com.fossil;

import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jz1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ew1 a;
    @DexIgnore
    public /* final */ /* synthetic */ gz1 b;

    @DexIgnore
    public jz1(gz1 gz1, ew1 ew1) {
        this.b = gz1;
        this.a = ew1;
    }

    @DexIgnore
    public final void run() {
        try {
            BasePendingResult.p.set(true);
            this.b.g.sendMessage(this.b.g.obtainMessage(0, this.b.a.a(this.a)));
            BasePendingResult.p.set(false);
            gz1.a(this.a);
            wv1 wv1 = (wv1) this.b.f.get();
            if (wv1 != null) {
                wv1.a(this.b);
            }
        } catch (RuntimeException e) {
            this.b.g.sendMessage(this.b.g.obtainMessage(1, e));
            BasePendingResult.p.set(false);
            gz1.a(this.a);
            wv1 wv12 = (wv1) this.b.f.get();
            if (wv12 != null) {
                wv12.a(this.b);
            }
        } catch (Throwable th) {
            BasePendingResult.p.set(false);
            gz1.a(this.a);
            wv1 wv13 = (wv1) this.b.f.get();
            if (wv13 != null) {
                wv13.a(this.b);
            }
            throw th;
        }
    }
}
