package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gw3 extends lw3 {
    @DexIgnore
    public fw3[] a;

    @DexIgnore
    public gw3() {
        a();
    }

    @DexIgnore
    public gw3 a() {
        this.a = fw3.b();
        return this;
    }

    @DexIgnore
    public gw3 a(iw3 iw3) throws IOException {
        while (true) {
            int j = iw3.j();
            if (j == 0) {
                return this;
            }
            if (j == 10) {
                int a2 = nw3.a(iw3, 10);
                fw3[] fw3Arr = this.a;
                int length = fw3Arr == null ? 0 : fw3Arr.length;
                fw3[] fw3Arr2 = new fw3[(a2 + length)];
                if (length != 0) {
                    System.arraycopy(this.a, 0, fw3Arr2, 0, length);
                }
                while (length < fw3Arr2.length - 1) {
                    fw3Arr2[length] = new fw3();
                    iw3.a((lw3) fw3Arr2[length]);
                    iw3.j();
                    length++;
                }
                fw3Arr2[length] = new fw3();
                iw3.a((lw3) fw3Arr2[length]);
                this.a = fw3Arr2;
            } else if (!nw3.b(iw3, j)) {
                return this;
            }
        }
    }
}
