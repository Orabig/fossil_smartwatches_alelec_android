package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.GeneratedAppGlideModule;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.av;
import com.fossil.bv;
import com.fossil.fx;
import com.fossil.gs;
import com.fossil.gv;
import com.fossil.ms;
import com.fossil.os;
import com.fossil.ov;
import com.fossil.qv;
import com.fossil.rv;
import com.fossil.sv;
import com.fossil.tv;
import com.fossil.uv;
import com.fossil.vv;
import com.fossil.wu;
import com.fossil.wv;
import com.fossil.xu;
import com.fossil.xv;
import com.fossil.yv;
import com.fossil.zu;
import com.fossil.zv;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wq implements ComponentCallbacks2 {
    @DexIgnore
    public static volatile wq i;
    @DexIgnore
    public static volatile boolean j;
    @DexIgnore
    public /* final */ au a;
    @DexIgnore
    public /* final */ ru b;
    @DexIgnore
    public /* final */ yq c;
    @DexIgnore
    public /* final */ dr d;
    @DexIgnore
    public /* final */ xt e;
    @DexIgnore
    public /* final */ qy f;
    @DexIgnore
    public /* final */ iy g;
    @DexIgnore
    public /* final */ List<fr> h; // = new ArrayList();

    @DexIgnore
    public interface a {
        @DexIgnore
        nz build();
    }

    @DexIgnore
    public wq(Context context, gt gtVar, ru ruVar, au auVar, xt xtVar, qy qyVar, iy iyVar, int i2, a aVar, Map<Class<?>, gr<?, ?>> map, List<mz<Object>> list, boolean z, boolean z2) {
        zr zrVar;
        zr zrVar2;
        Class<kr> cls;
        Context context2 = context;
        au auVar2 = auVar;
        xt xtVar2 = xtVar;
        Class<kr> cls2 = kr.class;
        Class<byte[]> cls3 = byte[].class;
        zq zqVar = zq.NORMAL;
        this.a = auVar2;
        this.e = xtVar2;
        this.b = ruVar;
        this.f = qyVar;
        this.g = iyVar;
        Resources resources = context.getResources();
        this.d = new dr();
        this.d.a((ImageHeaderParser) new nw());
        if (Build.VERSION.SDK_INT >= 27) {
            this.d.a((ImageHeaderParser) new sw());
        }
        List<ImageHeaderParser> a2 = this.d.a();
        ox oxVar = new ox(context2, a2, auVar2, xtVar2);
        zr<ParcelFileDescriptor, Bitmap> c2 = ex.c(auVar);
        pw pwVar = new pw(this.d.a(), resources.getDisplayMetrics(), auVar2, xtVar2);
        if (!z2 || Build.VERSION.SDK_INT < 28) {
            zrVar = new jw(pwVar);
            zrVar2 = new bx(pwVar, xtVar2);
        } else {
            zrVar2 = new ww();
            zrVar = new kw();
        }
        kx kxVar = new kx(context2);
        ov.c cVar = new ov.c(resources);
        ov.d dVar = new ov.d(resources);
        Class<byte[]> cls4 = cls3;
        ov.b bVar = new ov.b(resources);
        ov.a aVar2 = new ov.a(resources);
        fw fwVar = new fw(xtVar2);
        ov.d dVar2 = dVar;
        yx yxVar = new yx();
        by byVar = new by();
        ContentResolver contentResolver = context.getContentResolver();
        dr drVar = this.d;
        ov.b bVar2 = bVar;
        ov.c cVar2 = cVar;
        drVar.a(ByteBuffer.class, new yu());
        drVar.a(InputStream.class, new pv(xtVar2));
        kx kxVar2 = kxVar;
        drVar.a("Bitmap", ByteBuffer.class, Bitmap.class, zrVar);
        drVar.a("Bitmap", InputStream.class, Bitmap.class, zrVar2);
        if (os.c()) {
            cls = cls2;
            this.d.a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, new yw(pwVar));
        } else {
            cls = cls2;
        }
        dr drVar2 = this.d;
        drVar2.a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, c2);
        drVar2.a("Bitmap", AssetFileDescriptor.class, Bitmap.class, ex.a(auVar));
        drVar2.a(Bitmap.class, Bitmap.class, rv.a.a());
        drVar2.a("Bitmap", Bitmap.class, Bitmap.class, new dx());
        drVar2.a(Bitmap.class, fwVar);
        drVar2.a("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new dw(resources, zrVar));
        drVar2.a("BitmapDrawable", InputStream.class, BitmapDrawable.class, new dw(resources, zrVar2));
        drVar2.a("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new dw(resources, c2));
        drVar2.a(BitmapDrawable.class, new ew(auVar2, fwVar));
        drVar2.a("Gif", InputStream.class, qx.class, new xx(a2, oxVar, xtVar2));
        drVar2.a("Gif", ByteBuffer.class, qx.class, oxVar);
        drVar2.a(qx.class, new rx());
        Class<kr> cls5 = cls;
        drVar2.a(cls5, cls5, rv.a.a());
        drVar2.a("Bitmap", cls5, Bitmap.class, new vx(auVar2));
        kx kxVar3 = kxVar2;
        drVar2.a(Uri.class, Drawable.class, kxVar3);
        drVar2.a(Uri.class, Bitmap.class, new ax(kxVar3, auVar2));
        drVar2.a((gs.a<?>) new fx.a());
        drVar2.a(File.class, ByteBuffer.class, new zu.b());
        drVar2.a(File.class, InputStream.class, new bv.e());
        drVar2.a(File.class, File.class, new mx());
        drVar2.a(File.class, ParcelFileDescriptor.class, new bv.b());
        drVar2.a(File.class, File.class, rv.a.a());
        drVar2.a((gs.a<?>) new ms.a(xtVar2));
        if (os.c()) {
            this.d.a((gs.a<?>) new os.a());
        }
        dr drVar3 = this.d;
        ov.c cVar3 = cVar2;
        drVar3.a(Integer.TYPE, InputStream.class, cVar3);
        ov.b bVar3 = bVar2;
        drVar3.a(Integer.TYPE, ParcelFileDescriptor.class, bVar3);
        drVar3.a(Integer.class, InputStream.class, cVar3);
        drVar3.a(Integer.class, ParcelFileDescriptor.class, bVar3);
        ov.d dVar3 = dVar2;
        drVar3.a(Integer.class, Uri.class, dVar3);
        ov.a aVar3 = aVar2;
        drVar3.a(Integer.TYPE, AssetFileDescriptor.class, aVar3);
        drVar3.a(Integer.class, AssetFileDescriptor.class, aVar3);
        drVar3.a(Integer.TYPE, Uri.class, dVar3);
        drVar3.a(String.class, InputStream.class, new av.c());
        drVar3.a(Uri.class, InputStream.class, new av.c());
        drVar3.a(String.class, InputStream.class, new qv.c());
        drVar3.a(String.class, ParcelFileDescriptor.class, new qv.b());
        drVar3.a(String.class, AssetFileDescriptor.class, new qv.a());
        drVar3.a(Uri.class, InputStream.class, new vv.a());
        drVar3.a(Uri.class, InputStream.class, new wu.c(context.getAssets()));
        drVar3.a(Uri.class, ParcelFileDescriptor.class, new wu.b(context.getAssets()));
        Context context3 = context;
        drVar3.a(Uri.class, InputStream.class, new wv.a(context3));
        drVar3.a(Uri.class, InputStream.class, new xv.a(context3));
        if (Build.VERSION.SDK_INT >= 29) {
            this.d.a(Uri.class, InputStream.class, new yv.c(context3));
            this.d.a(Uri.class, ParcelFileDescriptor.class, new yv.b(context3));
        }
        dr drVar4 = this.d;
        ContentResolver contentResolver2 = contentResolver;
        drVar4.a(Uri.class, InputStream.class, new sv.d(contentResolver2));
        drVar4.a(Uri.class, ParcelFileDescriptor.class, new sv.b(contentResolver2));
        drVar4.a(Uri.class, AssetFileDescriptor.class, new sv.a(contentResolver2));
        drVar4.a(Uri.class, InputStream.class, new tv.a());
        drVar4.a(URL.class, InputStream.class, new zv.a());
        drVar4.a(Uri.class, File.class, new gv.a(context3));
        drVar4.a(cv.class, InputStream.class, new uv.a());
        Class<byte[]> cls6 = cls4;
        drVar4.a(cls6, ByteBuffer.class, new xu.a());
        drVar4.a(cls6, InputStream.class, new xu.d());
        drVar4.a(Uri.class, Uri.class, rv.a.a());
        drVar4.a(Drawable.class, Drawable.class, rv.a.a());
        drVar4.a(Drawable.class, Drawable.class, new lx());
        drVar4.a(Bitmap.class, BitmapDrawable.class, new zx(resources));
        yx yxVar2 = yxVar;
        drVar4.a(Bitmap.class, cls6, yxVar2);
        by byVar2 = byVar;
        drVar4.a(Drawable.class, cls6, new ay(auVar2, yxVar2, byVar2));
        drVar4.a(qx.class, cls6, byVar2);
        if (Build.VERSION.SDK_INT >= 23) {
            zr<ByteBuffer, Bitmap> b2 = ex.b(auVar);
            this.d.a(ByteBuffer.class, Bitmap.class, b2);
            this.d.a(ByteBuffer.class, BitmapDrawable.class, new dw(resources, b2));
        }
        Context context4 = context;
        xt xtVar3 = xtVar;
        this.c = new yq(context4, xtVar3, this.d, new wz(), aVar, map, list, gtVar, z, i2);
    }

    @DexIgnore
    public static wq a(Context context) {
        if (i == null) {
            GeneratedAppGlideModule b2 = b(context.getApplicationContext());
            synchronized (wq.class) {
                if (i == null) {
                    a(context, b2);
                }
            }
        }
        return i;
    }

    @DexIgnore
    public static void b(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        a(context, new xq(), generatedAppGlideModule);
    }

    @DexIgnore
    public au c() {
        return this.a;
    }

    @DexIgnore
    public iy d() {
        return this.g;
    }

    @DexIgnore
    public Context e() {
        return this.c.getBaseContext();
    }

    @DexIgnore
    public yq f() {
        return this.c;
    }

    @DexIgnore
    public dr g() {
        return this.d;
    }

    @DexIgnore
    public qy h() {
        return this.f;
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
        a();
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        a(i2);
    }

    @DexIgnore
    public static GeneratedAppGlideModule b(Context context) {
        try {
            return (GeneratedAppGlideModule) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(new Class[]{Context.class}).newInstance(new Object[]{context.getApplicationContext()});
        } catch (ClassNotFoundException unused) {
            if (Log.isLoggable("Glide", 5)) {
                Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            }
            return null;
        } catch (InstantiationException e2) {
            a((Exception) e2);
            throw null;
        } catch (IllegalAccessException e3) {
            a((Exception) e3);
            throw null;
        } catch (NoSuchMethodException e4) {
            a((Exception) e4);
            throw null;
        } catch (InvocationTargetException e5) {
            a((Exception) e5);
            throw null;
        }
    }

    @DexIgnore
    public static qy c(Context context) {
        q00.a(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return a(context).h();
    }

    @DexIgnore
    public static fr d(Context context) {
        return c(context).a(context);
    }

    @DexIgnore
    public static void a(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        if (!j) {
            j = true;
            b(context, generatedAppGlideModule);
            j = false;
            return;
        }
        throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    @DexIgnore
    public xt b() {
        return this.e;
    }

    @DexIgnore
    public void b(fr frVar) {
        synchronized (this.h) {
            if (this.h.contains(frVar)) {
                this.h.remove(frVar);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }

    @DexIgnore
    public static void a(Context context, xq xqVar, GeneratedAppGlideModule generatedAppGlideModule) {
        Context applicationContext = context.getApplicationContext();
        List<wy> emptyList = Collections.emptyList();
        if (generatedAppGlideModule == null || generatedAppGlideModule.a()) {
            emptyList = new yy(applicationContext).a();
        }
        if (generatedAppGlideModule != null && !generatedAppGlideModule.b().isEmpty()) {
            Set<Class<?>> b2 = generatedAppGlideModule.b();
            Iterator<wy> it = emptyList.iterator();
            while (it.hasNext()) {
                wy next = it.next();
                if (b2.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            for (wy wyVar : emptyList) {
                Log.d("Glide", "Discovered GlideModule from manifest: " + wyVar.getClass());
            }
        }
        xqVar.a(generatedAppGlideModule != null ? generatedAppGlideModule.c() : null);
        for (wy a2 : emptyList) {
            a2.a(applicationContext, xqVar);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, xqVar);
        }
        wq a3 = xqVar.a(applicationContext);
        for (wy next2 : emptyList) {
            try {
                next2.a(applicationContext, a3, a3.d);
            } catch (AbstractMethodError e2) {
                throw new IllegalStateException("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: " + next2.getClass().getName(), e2);
            }
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, a3, a3.d);
        }
        applicationContext.registerComponentCallbacks(a3);
        i = a3;
    }

    @DexIgnore
    public static void a(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    @DexIgnore
    public void a() {
        r00.b();
        this.b.a();
        this.a.a();
        this.e.a();
    }

    @DexIgnore
    public void a(int i2) {
        r00.b();
        for (fr onTrimMemory : this.h) {
            onTrimMemory.onTrimMemory(i2);
        }
        this.b.a(i2);
        this.a.a(i2);
        this.e.a(i2);
    }

    @DexIgnore
    public static fr a(Fragment fragment) {
        return c(fragment.getContext()).a(fragment);
    }

    @DexIgnore
    public static fr a(View view) {
        return c(view.getContext()).a(view);
    }

    @DexIgnore
    public boolean a(yz<?> yzVar) {
        synchronized (this.h) {
            for (fr b2 : this.h) {
                if (b2.b(yzVar)) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void a(fr frVar) {
        synchronized (this.h) {
            if (!this.h.contains(frVar)) {
                this.h.add(frVar);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }
}
