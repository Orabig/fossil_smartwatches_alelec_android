package com.fossil;

import com.fossil.qp1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up1<T> implements io1<T> {
    @DexIgnore
    public /* final */ rp1 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ho1<T, byte[]> c;
    @DexIgnore
    public /* final */ vp1 d;

    @DexIgnore
    public up1(rp1 rp1, String str, ho1<T, byte[]> ho1, vp1 vp1) {
        this.a = rp1;
        this.b = str;
        this.c = ho1;
        this.d = vp1;
    }

    @DexIgnore
    public static /* synthetic */ void a(Exception exc) {
    }

    @DexIgnore
    public void a(fo1<T> fo1) {
        a(fo1, tp1.a());
    }

    @DexIgnore
    public void a(fo1<T> fo1, ko1 ko1) {
        vp1 vp1 = this.d;
        qp1.a f = qp1.f();
        f.a(this.a);
        f.a((fo1<?>) fo1);
        f.a(this.b);
        f.a((ho1<?, byte[]>) this.c);
        vp1.a(f.a(), ko1);
    }
}
