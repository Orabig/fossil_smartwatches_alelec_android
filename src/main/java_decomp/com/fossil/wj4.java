package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wj4 {
    @DexIgnore
    public static /* final */ wj4 a; // = new wj4();

    @DexIgnore
    public final int a() {
        return 30;
    }

    @DexIgnore
    public final int a(int i) {
        if (i < 18) {
            return 540;
        }
        return (18 <= i && 65 >= i) ? 480 : 450;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        if (r9 < 35) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r0 = 70;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0049, code lost:
        if (r9 < 35) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        if (r9 < 35) goto L_0x002d;
     */
    @DexIgnore
    public final lc6<Integer, Integer> a(rh4 rh4, int i) {
        int i2;
        int i3;
        wg6.b(rh4, "gender");
        int i4 = vj4.a[rh4.ordinal()];
        if (i4 != 1) {
            if (i4 == 2) {
                i2 = 175;
                i3 = 80;
                if (i >= 18) {
                }
            } else if (i4 == 3) {
                i2 = 170;
                i3 = 75;
                if (i >= 18) {
                }
            } else {
                throw new kc6();
            }
            i3 = 65;
            return new lc6<>(Integer.valueOf(i2), Integer.valueOf(i3));
        }
        i2 = 165;
        if (i < 18) {
            i3 = 55;
            return new lc6<>(Integer.valueOf(i2), Integer.valueOf(i3));
        }
        i3 = 60;
        return new lc6<>(Integer.valueOf(i2), Integer.valueOf(i3));
    }

    @DexIgnore
    public final int b() {
        return 5000;
    }

    @DexIgnore
    public final int a(int i, int i2, int i3, rh4 rh4) {
        double d;
        wg6.b(rh4, "gender");
        double d2 = i < 18 ? 0.0d : (18 <= i && 30 >= i) ? 21.5650154d : (30 <= i && 50 >= i) ? 25.291792d : 33.3115727d;
        int i4 = vj4.b[rh4.ordinal()];
        if (i4 == 1) {
            d = -171.5010333d;
        } else if (i4 == 2) {
            d = -159.3516885d;
        } else if (i4 == 3) {
            d = -64.4165128d;
        } else {
            throw new kc6();
        }
        return rh6.a(d2 + 147.7007883d + (((double) i2) * 2.3639882d) + (((double) i3) * 0.2297702d) + d);
    }
}
