package com.fossil;

import com.fossil.fx6;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bx6 extends fx6.a {
    @DexIgnore
    public boolean a; // = true;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements fx6<zq6, zq6> {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        public zq6 a(zq6 zq6) throws IOException {
            try {
                return vx6.a(zq6);
            } finally {
                zq6.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fx6<RequestBody, RequestBody> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public /* bridge */ /* synthetic */ Object a(Object obj) throws IOException {
            RequestBody requestBody = (RequestBody) obj;
            a(requestBody);
            return requestBody;
        }

        @DexIgnore
        public RequestBody a(RequestBody requestBody) {
            return requestBody;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fx6<zq6, zq6> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public zq6 a(zq6 zq6) {
            return zq6;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object a(Object obj) throws IOException {
            zq6 zq6 = (zq6) obj;
            a(zq6);
            return zq6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fx6<Object, String> {
        @DexIgnore
        public static /* final */ d a; // = new d();

        @DexIgnore
        public String a(Object obj) {
            return obj.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fx6<zq6, cd6> {
        @DexIgnore
        public static /* final */ e a; // = new e();

        @DexIgnore
        public cd6 a(zq6 zq6) {
            zq6.close();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements fx6<zq6, Void> {
        @DexIgnore
        public static /* final */ f a; // = new f();

        @DexIgnore
        public Void a(zq6 zq6) {
            zq6.close();
            return null;
        }
    }

    @DexIgnore
    public fx6<zq6, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == zq6.class) {
            if (vx6.a(annotationArr, (Class<? extends Annotation>) cz6.class)) {
                return c.a;
            }
            return a.a;
        } else if (type == Void.class) {
            return f.a;
        } else {
            if (!this.a || type != cd6.class) {
                return null;
            }
            try {
                return e.a;
            } catch (NoClassDefFoundError unused) {
                this.a = false;
                return null;
            }
        }
    }

    @DexIgnore
    public fx6<?, RequestBody> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (RequestBody.class.isAssignableFrom(vx6.b(type))) {
            return b.a;
        }
        return null;
    }
}
