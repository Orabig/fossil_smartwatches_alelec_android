package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl5 implements Factory<gl5> {
    @DexIgnore
    public static HelpPresenter a(dl5 dl5, DeviceRepository deviceRepository, ws4 ws4, AnalyticsHelper analyticsHelper) {
        return new HelpPresenter(dl5, deviceRepository, ws4, analyticsHelper);
    }
}
