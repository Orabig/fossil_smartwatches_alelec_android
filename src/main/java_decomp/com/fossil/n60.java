package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n60 extends r60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public static /* final */ int f; // = 65535;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<n60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final n60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new n60(cw0.b(order.getShort(0)), cw0.b(order.getShort(2)), cw0.b(order.getShort(4)), cw0.b(order.getShort(6)));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 8"));
        }

        @DexIgnore
        public n60 createFromParcel(Parcel parcel) {
            return new n60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new n60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m45createFromParcel(Parcel parcel) {
            return new n60(parcel, (qg6) null);
        }
    }

    /*
    static {
        mh6 mh6 = mh6.a;
    }
    */

    @DexIgnore
    public n60(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        super(s60.DAILY_SLEEP);
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        e();
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.b).putShort((short) this.c).putShort((short) this.d).putShort((short) this.e).array();
        wg6.a(array, "ByteBuffer.allocate(Dail\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        int i = f;
        int i2 = this.b;
        boolean z = true;
        if (i2 >= 0 && i >= i2) {
            int i3 = f;
            int i4 = this.c;
            if (i4 >= 0 && i3 >= i4) {
                int i5 = f;
                int i6 = this.d;
                if (i6 >= 0 && i5 >= i6) {
                    int i7 = f;
                    int i8 = this.e;
                    if (i8 < 0 || i7 < i8) {
                        z = false;
                    }
                    if (!z) {
                        StringBuilder b2 = ze0.b("deepSleepInMinute (");
                        b2.append(this.e);
                        b2.append(") is out of range ");
                        b2.append("[0, ");
                        throw new IllegalArgumentException(ze0.a(b2, f, "]."));
                    }
                    return;
                }
                StringBuilder b3 = ze0.b("lightSleepInMinute (");
                b3.append(this.d);
                b3.append(") is out of range ");
                b3.append("[0, ");
                throw new IllegalArgumentException(ze0.a(b3, f, "]."));
            }
            StringBuilder b4 = ze0.b("awakeInMinute (");
            b4.append(this.c);
            b4.append(") is out of range ");
            b4.append("[0, ");
            throw new IllegalArgumentException(ze0.a(b4, f, "]."));
        }
        StringBuilder b5 = ze0.b("totalSleepInMinute (");
        b5.append(this.b);
        b5.append(") is out of range ");
        b5.append("[0, ");
        throw new IllegalArgumentException(ze0.a(b5, f, "]."));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(n60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            n60 n60 = (n60) obj;
            return this.b == n60.b && this.c == n60.c && this.d == n60.d && this.e == n60.e;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailySleepConfig");
    }

    @DexIgnore
    public final int getAwakeInMinute() {
        return this.c;
    }

    @DexIgnore
    public final int getDeepSleepInMinute() {
        return this.e;
    }

    @DexIgnore
    public final int getLightSleepInMinute() {
        return this.d;
    }

    @DexIgnore
    public final int getTotalSleepInMinute() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.b * 31) + this.c) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(cw0.a(cw0.a(cw0.a(jSONObject, bm0.TOTAL_SLEEP_IN_MINUTE, (Object) Integer.valueOf(this.b)), bm0.AWAKE_IN_MINUTE, (Object) Integer.valueOf(this.c)), bm0.LIGHT_SLEEP_IN_MINUTE, (Object) Integer.valueOf(this.d)), bm0.DEEP_SLEEP_IN_MINUTE, (Object) Integer.valueOf(this.e));
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ n60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        e();
    }
}
