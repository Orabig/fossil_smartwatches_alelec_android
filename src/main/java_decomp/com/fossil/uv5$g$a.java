package com.fossil;

import com.fossil.bv5;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1", f = "FindDevicePresenter.kt", l = {66, 69}, m = "invokeSuspend")
public final class uv5$g$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FindDevicePresenter.g this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1$deviceModel$1", f = "FindDevicePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super Device>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uv5$g$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(uv5$g$a uv5_g_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = uv5_g_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                DeviceRepository b = this.this$0.this$0.a.n;
                String str = this.this$0.$serial;
                wg6.a((Object) str, "serial");
                return b.getDeviceBySerial(str);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends sf6 implements ig6<il6, xe6<? super String>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uv5$g$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(xe6 xe6, uv5$g$a uv5_g_a) {
            super(2, xe6);
            this.this$0 = uv5_g_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(xe6, this.this$0);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                DeviceRepository b = this.this$0.this$0.a.n;
                String str = this.this$0.$serial;
                wg6.a((Object) str, "serial");
                return b.getDeviceNameBySerial(str);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public uv5$g$a(FindDevicePresenter.g gVar, String str, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
        this.$serial = str;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        uv5$g$a uv5_g_a = new uv5$g$a(this.this$0, this.$serial, xe6);
        uv5_g_a.p$ = (il6) obj;
        return uv5_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((uv5$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b9  */
    public final Object invokeSuspend(Object obj) {
        Device device;
        il6 il6;
        Object a2 = ff6.a();
        int i = this.label;
        Boolean bool = null;
        boolean z = true;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            dl6 a3 = this.this$0.a.c();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = gk6.a(a3, aVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            Device device2 = (Device) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            device = (Device) this.L$2;
            String str = (String) obj;
            if (!wg6.a((Object) this.$serial, (Object) this.this$0.a.i())) {
                IButtonConnectivity b2 = PortfolioApp.get.b();
                if (b2 != null) {
                    if (b2.getGattState(this.$serial) != 2) {
                        z = false;
                    }
                    bool = hf6.a(z);
                }
                this.this$0.a.f.a(new bv5.c(device, str, bool != null ? bool.booleanValue() : false, true, (Boolean) null, 16, (qg6) null));
            } else {
                this.this$0.a.f.a(new bv5.c(device, str, false, false, (Boolean) null, 16, (qg6) null));
            }
            this.this$0.a.p.c(this.this$0.a.o.G(), false);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Device device3 = (Device) obj;
        if (device3 != null) {
            dl6 a4 = this.this$0.a.c();
            b bVar = new b((xe6) null, this);
            this.L$0 = il6;
            this.L$1 = device3;
            this.L$2 = device3;
            this.label = 2;
            Object a5 = gk6.a(a4, bVar, this);
            if (a5 == a2) {
                return a2;
            }
            device = device3;
            obj = a5;
            String str2 = (String) obj;
            if (!wg6.a((Object) this.$serial, (Object) this.this$0.a.i())) {
            }
        }
        this.this$0.a.p.c(this.this$0.a.o.G(), false);
        return cd6.a;
    }
}
