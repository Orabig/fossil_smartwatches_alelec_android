package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u93 {
    @DexIgnore
    public /* final */ b03 a; // = new x93(this, this.b.a);
    @DexIgnore
    public /* final */ /* synthetic */ m93 b;

    @DexIgnore
    public u93(m93 m93) {
        this.b = m93;
    }

    @DexIgnore
    public final void a(long j) {
        this.b.g();
        if (this.b.a.g()) {
            if (this.b.l().e(this.b.p().A(), l03.a0)) {
                this.b.k().y.a(false);
            }
            if (this.b.l().n(this.b.p().A())) {
                a(this.b.zzm().b(), false);
                return;
            }
            this.a.c();
            if (this.b.k().a(this.b.zzm().b())) {
                this.b.k().r.a(true);
                this.b.k().w.a(0);
            }
            if (this.b.k().r.a()) {
                this.a.a(Math.max(0, this.b.k().p.a() - this.b.k().w.a()));
            }
        }
    }

    @DexIgnore
    public final void b(long j, boolean z) {
        this.b.g();
        if (qs2.a() && this.b.l().e(this.b.p().A(), l03.e0)) {
            if (this.b.a.g()) {
                this.b.k().v.a(j);
            } else {
                return;
            }
        }
        this.b.b().B().a("Session started, time", Long.valueOf(this.b.zzm().c()));
        Long l = null;
        if (this.b.l().l(this.b.p().A())) {
            l = Long.valueOf(j / 1000);
        }
        this.b.o().a("auto", "_sid", (Object) l, j);
        this.b.k().r.a(false);
        Bundle bundle = new Bundle();
        if (this.b.l().l(this.b.p().A())) {
            bundle.putLong("_sid", l.longValue());
        }
        if (this.b.l().a(l03.M0) && z) {
            bundle.putLong("_aib", 1);
        }
        this.b.o().a("auto", "_s", j, bundle);
        if (es2.a() && this.b.l().a(l03.T0)) {
            String a2 = this.b.k().B.a();
            if (!TextUtils.isEmpty(a2)) {
                Bundle bundle2 = new Bundle();
                bundle2.putString("_ffr", a2);
                this.b.o().a("auto", "_ssr", j, bundle2);
            }
        }
        if (!qs2.a() || !this.b.l().e(this.b.p().A(), l03.e0)) {
            this.b.k().v.a(j);
        }
    }

    @DexIgnore
    public final void a(long j, boolean z) {
        this.b.g();
        this.b.B();
        if (this.b.k().a(j)) {
            this.b.k().r.a(true);
            this.b.k().w.a(0);
        }
        if (z && this.b.l().o(this.b.p().A())) {
            this.b.k().v.a(j);
        }
        if (this.b.k().r.a()) {
            b(j, z);
        }
    }
}
