package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f20 {
    @DexIgnore
    public static /* final */ h20 a;

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0011  */
    /*
    static {
        h20 h20;
        try {
            h20 = e20.a();
        } catch (IllegalStateException | NoClassDefFoundError unused) {
            h20 = null;
            if (h20 == null) {
            }
            a = h20;
        } catch (Throwable th) {
            Log.w("AnswersOptionalLogger", "Unexpected error creating AnswersKitEventLogger", th);
            h20 = null;
            if (h20 == null) {
            }
            a = h20;
        }
        if (h20 == null) {
            h20 = i20.a();
        }
        a = h20;
    }
    */

    @DexIgnore
    public static h20 a() {
        return a;
    }
}
