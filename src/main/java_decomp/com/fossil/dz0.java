package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz0 extends l61 {
    @DexIgnore
    public /* final */ byte[] A;
    @DexIgnore
    public /* final */ rg1 B;

    @DexIgnore
    public dz0(ue1 ue1, byte[] bArr, rg1 rg1) {
        super(lx0.CUSTOM_COMMAND, ue1);
        this.A = bArr;
        this.B = rg1;
    }

    @DexIgnore
    public ok0 l() {
        return new fb1(this.B, this.A, this.y.v);
    }
}
