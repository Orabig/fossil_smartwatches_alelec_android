package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class md4 extends ld4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j R; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray S; // = new SparseIntArray();
    @DexIgnore
    public long Q;

    /*
    static {
        S.put(2131362561, 1);
        S.put(2131362820, 2);
        S.put(2131363218, 3);
        S.put(2131362978, 4);
        S.put(2131362516, 5);
        S.put(2131362185, 6);
        S.put(2131362554, 7);
        S.put(2131362517, 8);
        S.put(2131362187, 9);
        S.put(2131362555, 10);
        S.put(2131362518, 11);
        S.put(2131362189, 12);
        S.put(2131362556, 13);
        S.put(2131362514, 14);
        S.put(2131362181, 15);
        S.put(2131362553, 16);
        S.put(2131362351, 17);
        S.put(2131362219, 18);
        S.put(2131362214, 19);
        S.put(2131362221, 20);
        S.put(2131361980, 21);
        S.put(2131362403, 22);
        S.put(2131361982, 23);
        S.put(2131362446, 24);
        S.put(2131361981, 25);
        S.put(2131362439, 26);
        S.put(2131361976, 27);
        S.put(2131362350, 28);
        S.put(2131361975, 29);
        S.put(2131362349, 30);
        S.put(2131362211, 31);
    }
    */

    @DexIgnore
    public md4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 32, R, S));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.Q = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.Q != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.Q = 1;
        }
        g();
    }

    @DexIgnore
    public md4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[29], objArr[27], objArr[21], objArr[25], objArr[23], objArr[15], objArr[6], objArr[9], objArr[12], objArr[31], objArr[19], objArr[18], objArr[20], objArr[30], objArr[28], objArr[17], objArr[22], objArr[26], objArr[24], objArr[14], objArr[5], objArr[8], objArr[11], objArr[16], objArr[7], objArr[10], objArr[13], objArr[1], objArr[2], objArr[0], objArr[4], objArr[3]);
        this.Q = -1;
        this.P.setTag((Object) null);
        a(view);
        f();
    }
}
