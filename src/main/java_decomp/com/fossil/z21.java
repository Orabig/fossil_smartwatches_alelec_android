package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z21 extends qx0 {
    @DexIgnore
    public short O;
    @DexIgnore
    public long P;
    @DexIgnore
    public /* final */ byte[] Q;
    @DexIgnore
    public byte[] R;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ z21(short s, ue1 ue1, int i, int i2) {
        super(du0.LEGACY_LIST_FILE, s, lx0.LEGACY_LIST_FILE, ue1, (i2 & 4) != 0 ? 3 : i);
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(du0.LEGACY_LIST_FILE.a).array();
        wg6.a(array, "ByteBuffer.allocate(1)\n \u2026ode)\n            .array()");
        this.Q = array;
        byte[] array2 = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(du0.LEGACY_LIST_FILE.a()).array();
        wg6.a(array2, "ByteBuffer.allocate(1)\n \u2026e())\n            .array()");
        this.R = array2;
    }

    @DexIgnore
    public void b(byte[] bArr) {
        this.R = bArr;
    }

    @DexIgnore
    public void c(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.wrap(received\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.O = cw0.b(order.get(0));
        this.P = cw0.b(order.getInt(1));
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.FILE_HANDLE, (Object) cw0.a(this.I));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(super.i(), bm0.NUMBER_OF_FILES, (Object) Short.valueOf(this.O)), bm0.TOTAL_FILE_SIZE, (Object) Long.valueOf(this.P));
    }

    @DexIgnore
    public byte[] o() {
        return this.Q;
    }

    @DexIgnore
    public byte[] r() {
        return this.R;
    }
}
