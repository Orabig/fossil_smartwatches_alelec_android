package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vt6 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public vt6 f;
    @DexIgnore
    public vt6 g;

    @DexIgnore
    public vt6() {
        this.a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    @DexIgnore
    public final vt6 a(vt6 vt6) {
        vt6.g = this;
        vt6.f = this.f;
        this.f.g = vt6;
        this.f = vt6;
        return vt6;
    }

    @DexIgnore
    public final vt6 b() {
        vt6 vt6 = this.f;
        if (vt6 == this) {
            vt6 = null;
        }
        vt6 vt62 = this.g;
        vt62.f = this.f;
        this.f.g = vt62;
        this.f = null;
        this.g = null;
        return vt6;
    }

    @DexIgnore
    public final vt6 c() {
        this.d = true;
        return new vt6(this.a, this.b, this.c, true, false);
    }

    @DexIgnore
    public vt6(byte[] bArr, int i, int i2, boolean z, boolean z2) {
        this.a = bArr;
        this.b = i;
        this.c = i2;
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public final vt6 a(int i) {
        vt6 vt6;
        if (i <= 0 || i > this.c - this.b) {
            throw new IllegalArgumentException();
        }
        if (i >= 1024) {
            vt6 = c();
        } else {
            vt6 = wt6.a();
            System.arraycopy(this.a, this.b, vt6.a, 0, i);
        }
        vt6.c = vt6.b + i;
        this.b += i;
        this.g.a(vt6);
        return vt6;
    }

    @DexIgnore
    public final void a() {
        vt6 vt6 = this.g;
        if (vt6 == this) {
            throw new IllegalStateException();
        } else if (vt6.e) {
            int i = this.c - this.b;
            if (i <= (8192 - vt6.c) + (vt6.d ? 0 : vt6.b)) {
                a(this.g, i);
                b();
                wt6.a(this);
            }
        }
    }

    @DexIgnore
    public final void a(vt6 vt6, int i) {
        if (vt6.e) {
            int i2 = vt6.c;
            if (i2 + i > 8192) {
                if (!vt6.d) {
                    int i3 = vt6.b;
                    if ((i2 + i) - i3 <= 8192) {
                        byte[] bArr = vt6.a;
                        System.arraycopy(bArr, i3, bArr, 0, i2 - i3);
                        vt6.c -= vt6.b;
                        vt6.b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            System.arraycopy(this.a, this.b, vt6.a, vt6.c, i);
            vt6.c += i;
            this.b += i;
            return;
        }
        throw new IllegalArgumentException();
    }
}
