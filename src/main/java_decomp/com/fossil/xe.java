package com.fossil;

import com.fossil.bf;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xe<Key, Value> {
    @DexIgnore
    public AtomicBoolean mInvalid; // = new AtomicBoolean(false);
    @DexIgnore
    public CopyOnWriteArrayList<c> mOnInvalidatedCallbacks; // = new CopyOnWriteArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements v3<List<X>, List<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ v3 a;

        @DexIgnore
        public a(v3 v3Var) {
            this.a = v3Var;
        }

        @DexIgnore
        /* renamed from: a */
        public List<Y> apply(List<X> list) {
            ArrayList arrayList = new ArrayList(list.size());
            for (int i = 0; i < list.size(); i++) {
                arrayList.add(this.a.apply(list.get(i)));
            }
            return arrayList;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<Key, Value> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends b<Key, ToValue> {
            @DexIgnore
            public /* final */ /* synthetic */ v3 a;

            @DexIgnore
            public a(v3 v3Var) {
                this.a = v3Var;
            }

            @DexIgnore
            public xe<Key, ToValue> create() {
                return b.this.create().mapByPage(this.a);
            }
        }

        @DexIgnore
        public abstract xe<Key, Value> create();

        @DexIgnore
        public <ToValue> b<Key, ToValue> map(v3<Value, ToValue> v3Var) {
            return mapByPage(xe.createListFunction(v3Var));
        }

        @DexIgnore
        public <ToValue> b<Key, ToValue> mapByPage(v3<List<Value>, List<ToValue>> v3Var) {
            return new a(v3Var);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public static <A, B> List<B> convert(v3<List<A>, List<B>> v3Var, List<A> list) {
        List<B> apply = v3Var.apply(list);
        if (apply.size() == list.size()) {
            return apply;
        }
        throw new IllegalStateException("Invalid Function " + v3Var + " changed return size. This is not supported.");
    }

    @DexIgnore
    public static <X, Y> v3<List<X>, List<Y>> createListFunction(v3<X, Y> v3Var) {
        return new a(v3Var);
    }

    @DexIgnore
    public void addInvalidatedCallback(c cVar) {
        this.mOnInvalidatedCallbacks.add(cVar);
    }

    @DexIgnore
    public void invalidate() {
        if (this.mInvalid.compareAndSet(false, true)) {
            Iterator<c> it = this.mOnInvalidatedCallbacks.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    @DexIgnore
    public abstract boolean isContiguous();

    @DexIgnore
    public boolean isInvalid() {
        return this.mInvalid.get();
    }

    @DexIgnore
    public abstract <ToValue> xe<Key, ToValue> map(v3<Value, ToValue> v3Var);

    @DexIgnore
    public abstract <ToValue> xe<Key, ToValue> mapByPage(v3<List<Value>, List<ToValue>> v3Var);

    @DexIgnore
    public void removeInvalidatedCallback(c cVar) {
        this.mOnInvalidatedCallbacks.remove(cVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ xe b;
        @DexIgnore
        public /* final */ bf.a<T> c;
        @DexIgnore
        public /* final */ Object d; // = new Object();
        @DexIgnore
        public Executor e; // = null;
        @DexIgnore
        public boolean f; // = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ bf a;
            @DexIgnore
            public /* final */ /* synthetic */ Throwable b;
            @DexIgnore
            public /* final */ /* synthetic */ boolean c;

            @DexIgnore
            public a(bf bfVar, Throwable th, boolean z) {
                this.a = bfVar;
                this.b = th;
                this.c = z;
            }

            @DexIgnore
            public void run() {
                d.this.a(this.a, this.b, this.c);
            }
        }

        @DexIgnore
        public d(xe xeVar, int i, Executor executor, bf.a<T> aVar) {
            this.b = xeVar;
            this.a = i;
            this.e = executor;
            this.c = aVar;
        }

        @DexIgnore
        public static void a(List<?> list, int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Position must be non-negative");
            } else if (list.size() + i > i2) {
                throw new IllegalArgumentException("List size + position too large, last item in list beyond totalCount.");
            } else if (list.size() == 0 && i2 > 0) {
                throw new IllegalArgumentException("Initial result cannot be empty if items are present in data set.");
            }
        }

        @DexIgnore
        public final void b(bf<T> bfVar, Throwable th, boolean z) {
            Executor executor;
            synchronized (this.d) {
                if (!this.f) {
                    this.f = true;
                    executor = this.e;
                } else {
                    throw new IllegalStateException("callback.onResult/onError already called, cannot call again.");
                }
            }
            if (executor != null) {
                executor.execute(new a(bfVar, th, z));
            } else {
                a(bfVar, th, z);
            }
        }

        @DexIgnore
        public void a(Executor executor) {
            synchronized (this.d) {
                this.e = executor;
            }
        }

        @DexIgnore
        public boolean a() {
            if (!this.b.isInvalid()) {
                return false;
            }
            a(bf.c());
            return true;
        }

        @DexIgnore
        public void a(bf<T> bfVar) {
            b(bfVar, (Throwable) null, false);
        }

        @DexIgnore
        public void a(bf<T> bfVar, Throwable th, boolean z) {
            if (bfVar != null) {
                this.c.a(this.a, bfVar);
            } else {
                this.c.a(this.a, th, z);
            }
        }
    }
}
