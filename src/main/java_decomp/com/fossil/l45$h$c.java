package com.fossil;

import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l45$h$c<T> implements ld<String> {
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter.h a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements ld<List<? extends DianaPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ l45$h$c a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.l45$h$c$a$a")
        /* renamed from: com.fossil.l45$h$c$a$a  reason: collision with other inner class name */
        public static final class C0024a<T> implements Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return ue6.a(Boolean.valueOf(((DianaPreset) t2).isActive()), Boolean.valueOf(((DianaPreset) t).isActive()));
            }
        }

        @DexIgnore
        public a(l45$h$c l45_h_c) {
            this.a = l45_h_c;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<DianaPreset> list) {
            if (list != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDianaCustomizePresenter", "onObserve on preset list change " + list);
                List<T> a2 = yd6.a(list, new C0024a());
                boolean a3 = wg6.a((Object) a2, (Object) this.a.a.this$0.i) ^ true;
                int itemCount = this.a.a.this$0.t.getItemCount();
                if (a3 || a2.size() != itemCount - 1) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("HomeDianaCustomizePresenter", "process change - " + a3 + " - itemCount: " + itemCount + " - sortedPresetSize: " + a2.size());
                    if (this.a.a.this$0.n == 1) {
                        this.a.a.this$0.a((List<DianaPreset>) a2);
                    } else {
                        this.a.a.this$0.r = qc6.a(true, a2);
                    }
                }
            }
        }
    }

    @DexIgnore
    public l45$h$c(HomeDianaCustomizePresenter.h hVar) {
        this.a = hVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(String str) {
        if (!TextUtils.isEmpty(str)) {
            HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.a.this$0;
            DianaPresetRepository k = homeDianaCustomizePresenter.x;
            if (str != null) {
                homeDianaCustomizePresenter.e = k.getPresetListAsLiveData(str);
                this.a.this$0.e.a(this.a.this$0.t, new a(this));
                this.a.this$0.t.a(false);
                return;
            }
            wg6.a();
            throw null;
        }
        this.a.this$0.t.a(true);
    }
}
