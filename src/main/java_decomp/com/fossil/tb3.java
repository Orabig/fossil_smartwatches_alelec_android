package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tb3 extends ta2 implements rb3 {
    @DexIgnore
    public tb3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    @DexIgnore
    public final void a(n12 n12, int i, boolean z) throws RemoteException {
        Parcel q = q();
        ua2.a(q, (IInterface) n12);
        q.writeInt(i);
        ua2.a(q, z);
        b(9, q);
    }

    @DexIgnore
    public final void d(int i) throws RemoteException {
        Parcel q = q();
        q.writeInt(i);
        b(7, q);
    }

    @DexIgnore
    public final void a(vb3 vb3, pb3 pb3) throws RemoteException {
        Parcel q = q();
        ua2.a(q, (Parcelable) vb3);
        ua2.a(q, (IInterface) pb3);
        b(12, q);
    }
}
