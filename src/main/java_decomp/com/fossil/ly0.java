package com.fossil;

import com.fossil.v60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ly0 extends tg6 implements hg6<byte[], v60> {
    @DexIgnore
    public ly0(v60.b bVar) {
        super(1, bVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(v60.b.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/HeartRateModeConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((v60.b) this.receiver).a((byte[]) obj);
    }
}
