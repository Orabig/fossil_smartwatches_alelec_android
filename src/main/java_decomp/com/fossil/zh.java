package com.fossil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zh {
    @DexIgnore
    public static /* final */ Map<String, Lock> e; // = new HashMap();
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ Lock b; // = a(this.a.getAbsolutePath());
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public FileChannel d;

    @DexIgnore
    public zh(String str, File file, boolean z) {
        this.a = new File(file, str + ".lck");
        this.c = z;
    }

    @DexIgnore
    public void a() {
        this.b.lock();
        if (this.c) {
            try {
                this.d = new FileOutputStream(this.a).getChannel();
                this.d.lock();
            } catch (IOException e2) {
                throw new IllegalStateException("Unable to grab copy lock.", e2);
            }
        }
    }

    @DexIgnore
    public void b() {
        FileChannel fileChannel = this.d;
        if (fileChannel != null) {
            try {
                fileChannel.close();
            } catch (IOException unused) {
            }
        }
        this.b.unlock();
    }

    @DexIgnore
    public static Lock a(String str) {
        Lock lock;
        synchronized (e) {
            lock = e.get(str);
            if (lock == null) {
                lock = new ReentrantLock();
                e.put(str, lock);
            }
        }
        return lock;
    }
}
