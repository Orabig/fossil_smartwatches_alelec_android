package com.fossil;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hv1 extends DialogFragment {
    @DexIgnore
    public Dialog a; // = null;
    @DexIgnore
    public DialogInterface.OnCancelListener b; // = null;

    @DexIgnore
    public static hv1 a(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        hv1 hv1 = new hv1();
        w12.a(dialog, (Object) "Cannot display null dialog");
        Dialog dialog2 = dialog;
        dialog2.setOnCancelListener((DialogInterface.OnCancelListener) null);
        dialog2.setOnDismissListener((DialogInterface.OnDismissListener) null);
        hv1.a = dialog2;
        if (onCancelListener != null) {
            hv1.b = onCancelListener;
        }
        return hv1;
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.b;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        if (this.a == null) {
            setShowsDialog(false);
        }
        return this.a;
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
