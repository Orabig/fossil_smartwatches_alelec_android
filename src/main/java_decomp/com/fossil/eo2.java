package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eo2 implements hp2 {
    @DexIgnore
    public static /* final */ oo2 b; // = new ho2();
    @DexIgnore
    public /* final */ oo2 a;

    @DexIgnore
    public eo2() {
        this(new go2(gn2.a(), a()));
    }

    @DexIgnore
    public static boolean a(po2 po2) {
        return po2.zza() == fn2.e.i;
    }

    @DexIgnore
    public final <T> fp2<T> zza(Class<T> cls) {
        Class<fn2> cls2 = fn2.class;
        gp2.a((Class<?>) cls);
        po2 zzb = this.a.zzb(cls);
        if (zzb.zzb()) {
            if (cls2.isAssignableFrom(cls)) {
                return vo2.a(gp2.c(), vm2.a(), zzb.zzc());
            }
            return vo2.a(gp2.a(), vm2.b(), zzb.zzc());
        } else if (cls2.isAssignableFrom(cls)) {
            if (a(zzb)) {
                return uo2.a(cls, zzb, zo2.b(), ao2.b(), gp2.c(), vm2.a(), mo2.b());
            }
            return uo2.a(cls, zzb, zo2.b(), ao2.b(), gp2.c(), (um2<?>) null, mo2.b());
        } else if (a(zzb)) {
            return uo2.a(cls, zzb, zo2.a(), ao2.a(), gp2.a(), vm2.b(), mo2.a());
        } else {
            return uo2.a(cls, zzb, zo2.a(), ao2.a(), gp2.b(), (um2<?>) null, mo2.a());
        }
    }

    @DexIgnore
    public static oo2 a() {
        try {
            return (oo2) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }

    @DexIgnore
    public eo2(oo2 oo2) {
        hn2.a(oo2, "messageInfoFactory");
        this.a = oo2;
    }
}
