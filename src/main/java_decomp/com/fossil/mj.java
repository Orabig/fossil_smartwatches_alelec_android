package com.fossil;

import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mj {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public Runnable b;

    @DexIgnore
    public void a() {
        Runnable runnable;
        if (a(this.a) == this && (runnable = this.b) != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public static void a(ViewGroup viewGroup, mj mjVar) {
        viewGroup.setTag(kj.transition_current_scene, mjVar);
    }

    @DexIgnore
    public static mj a(ViewGroup viewGroup) {
        return (mj) viewGroup.getTag(kj.transition_current_scene);
    }
}
