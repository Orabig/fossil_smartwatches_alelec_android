package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;
import com.google.android.material.datepicker.MaterialCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bh3 {
    @DexIgnore
    public /* final */ ah3 a;
    @DexIgnore
    public /* final */ ah3 b;
    @DexIgnore
    public /* final */ ah3 c;
    @DexIgnore
    public /* final */ ah3 d;
    @DexIgnore
    public /* final */ ah3 e;
    @DexIgnore
    public /* final */ ah3 f;
    @DexIgnore
    public /* final */ ah3 g;
    @DexIgnore
    public /* final */ Paint h; // = new Paint();

    @DexIgnore
    public bh3(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(oi3.a(context, nf3.materialCalendarStyle, MaterialCalendar.class.getCanonicalName()), xf3.MaterialCalendar);
        this.a = ah3.a(context, obtainStyledAttributes.getResourceId(xf3.MaterialCalendar_dayStyle, 0));
        this.g = ah3.a(context, obtainStyledAttributes.getResourceId(xf3.MaterialCalendar_dayInvalidStyle, 0));
        this.b = ah3.a(context, obtainStyledAttributes.getResourceId(xf3.MaterialCalendar_daySelectedStyle, 0));
        this.c = ah3.a(context, obtainStyledAttributes.getResourceId(xf3.MaterialCalendar_dayTodayStyle, 0));
        ColorStateList a2 = pi3.a(context, obtainStyledAttributes, xf3.MaterialCalendar_rangeFillColor);
        this.d = ah3.a(context, obtainStyledAttributes.getResourceId(xf3.MaterialCalendar_yearStyle, 0));
        this.e = ah3.a(context, obtainStyledAttributes.getResourceId(xf3.MaterialCalendar_yearSelectedStyle, 0));
        this.f = ah3.a(context, obtainStyledAttributes.getResourceId(xf3.MaterialCalendar_yearTodayStyle, 0));
        this.h.setColor(a2.getDefaultColor());
        obtainStyledAttributes.recycle();
    }
}
