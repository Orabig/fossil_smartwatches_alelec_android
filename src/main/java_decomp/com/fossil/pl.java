package com.fossil;

import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pl {
    @DexIgnore
    public static /* final */ String b; // = tl.a("Data");
    @DexIgnore
    public static /* final */ pl c; // = new a().a();
    @DexIgnore
    public Map<String, Object> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Map<String, Object> a; // = new HashMap();

        @DexIgnore
        public a a(String str, String str2) {
            this.a.put(str, str2);
            return this;
        }

        @DexIgnore
        public a a(pl plVar) {
            a(plVar.a);
            return this;
        }

        @DexIgnore
        public a a(Map<String, Object> map) {
            for (Map.Entry next : map.entrySet()) {
                a((String) next.getKey(), next.getValue());
            }
            return this;
        }

        @DexIgnore
        public a a(String str, Object obj) {
            if (obj == null) {
                this.a.put(str, (Object) null);
            } else {
                Class<?> cls = obj.getClass();
                if (cls == Boolean.class || cls == Byte.class || cls == Integer.class || cls == Long.class || cls == Float.class || cls == Double.class || cls == String.class || cls == Boolean[].class || cls == Byte[].class || cls == Integer[].class || cls == Long[].class || cls == Float[].class || cls == Double[].class || cls == String[].class) {
                    this.a.put(str, obj);
                } else if (cls == boolean[].class) {
                    this.a.put(str, pl.a((boolean[]) obj));
                } else if (cls == byte[].class) {
                    this.a.put(str, pl.a((byte[]) obj));
                } else if (cls == int[].class) {
                    this.a.put(str, pl.a((int[]) obj));
                } else if (cls == long[].class) {
                    this.a.put(str, pl.a((long[]) obj));
                } else if (cls == float[].class) {
                    this.a.put(str, pl.a((float[]) obj));
                } else if (cls == double[].class) {
                    this.a.put(str, pl.a((double[]) obj));
                } else {
                    throw new IllegalArgumentException(String.format("Key %s has invalid type %s", new Object[]{str, cls}));
                }
            }
            return this;
        }

        @DexIgnore
        public pl a() {
            pl plVar = new pl((Map<String, ?>) this.a);
            pl.a(plVar);
            return plVar;
        }
    }

    @DexIgnore
    public pl() {
    }

    @DexIgnore
    public String a(String str) {
        Object obj = this.a.get(str);
        if (obj instanceof String) {
            return (String) obj;
        }
        return null;
    }

    @DexIgnore
    public int b() {
        return this.a.size();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || pl.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((pl) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() * 31;
    }

    @DexIgnore
    public pl(pl plVar) {
        this.a = new HashMap(plVar.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e A[SYNTHETIC, Splitter:B:24:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006b A[SYNTHETIC, Splitter:B:36:0x006b] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x0035=Splitter:B:14:0x0035, B:28:0x0058=Splitter:B:28:0x0058} */
    public static pl b(byte[] bArr) {
        ObjectInputStream objectInputStream;
        Throwable e;
        if (bArr.length <= 10240) {
            HashMap hashMap = new HashMap();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            try {
                objectInputStream = new ObjectInputStream(byteArrayInputStream);
                try {
                    for (int readInt = objectInputStream.readInt(); readInt > 0; readInt--) {
                        hashMap.put(objectInputStream.readUTF(), objectInputStream.readObject());
                    }
                    try {
                        objectInputStream.close();
                    } catch (IOException e2) {
                        Log.e(b, "Error in Data#fromByteArray: ", e2);
                    }
                } catch (IOException | ClassNotFoundException e3) {
                    e = e3;
                    try {
                        Log.e(b, "Error in Data#fromByteArray: ", e);
                        if (objectInputStream != null) {
                        }
                        byteArrayInputStream.close();
                        return new pl((Map<String, ?>) hashMap);
                    } catch (Throwable th) {
                        th = th;
                        if (objectInputStream != null) {
                        }
                        try {
                            byteArrayInputStream.close();
                        } catch (IOException e4) {
                            Log.e(b, "Error in Data#fromByteArray: ", e4);
                        }
                        throw th;
                    }
                }
                try {
                    byteArrayInputStream.close();
                } catch (IOException e5) {
                    Log.e(b, "Error in Data#fromByteArray: ", e5);
                }
            } catch (IOException | ClassNotFoundException e6) {
                Throwable th2 = e6;
                objectInputStream = null;
                e = th2;
                Log.e(b, "Error in Data#fromByteArray: ", e);
                if (objectInputStream != null) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e7) {
                        Log.e(b, "Error in Data#fromByteArray: ", e7);
                    }
                }
                byteArrayInputStream.close();
                return new pl((Map<String, ?>) hashMap);
            } catch (Throwable th3) {
                objectInputStream = null;
                th = th3;
                if (objectInputStream != null) {
                    try {
                        objectInputStream.close();
                    } catch (IOException e8) {
                        Log.e(b, "Error in Data#fromByteArray: ", e8);
                    }
                }
                byteArrayInputStream.close();
                throw th;
            }
            return new pl((Map<String, ?>) hashMap);
        }
        throw new IllegalStateException("Data cannot occupy more than 10240 bytes when serialized");
    }

    @DexIgnore
    public pl(Map<String, ?> map) {
        this.a = new HashMap(map);
    }

    @DexIgnore
    public Map<String, Object> a() {
        return Collections.unmodifiableMap(this.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0078 A[SYNTHETIC, Splitter:B:31:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008f A[SYNTHETIC, Splitter:B:41:0x008f] */
    public static byte[] a(pl plVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = null;
        try {
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(byteArrayOutputStream);
            try {
                objectOutputStream2.writeInt(plVar.b());
                for (Map.Entry next : plVar.a.entrySet()) {
                    objectOutputStream2.writeUTF((String) next.getKey());
                    objectOutputStream2.writeObject(next.getValue());
                }
                try {
                    objectOutputStream2.close();
                } catch (IOException e) {
                    Log.e(b, "Error in Data#toByteArray: ", e);
                }
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e2) {
                    Log.e(b, "Error in Data#toByteArray: ", e2);
                }
                if (byteArrayOutputStream.size() <= 10240) {
                    return byteArrayOutputStream.toByteArray();
                }
                throw new IllegalStateException("Data cannot occupy more than 10240 bytes when serialized");
            } catch (IOException e3) {
                e = e3;
                objectOutputStream = objectOutputStream2;
                try {
                    Log.e(b, "Error in Data#toByteArray: ", e);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    if (objectOutputStream != null) {
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e4) {
                        Log.e(b, "Error in Data#toByteArray: ", e4);
                    }
                    return byteArray;
                } catch (Throwable th) {
                    th = th;
                    objectOutputStream2 = objectOutputStream;
                    if (objectOutputStream2 != null) {
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e5) {
                        Log.e(b, "Error in Data#toByteArray: ", e5);
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (objectOutputStream2 != null) {
                    try {
                        objectOutputStream2.close();
                    } catch (IOException e6) {
                        Log.e(b, "Error in Data#toByteArray: ", e6);
                    }
                }
                byteArrayOutputStream.close();
                throw th;
            }
        } catch (IOException e7) {
            e = e7;
            Log.e(b, "Error in Data#toByteArray: ", e);
            byte[] byteArray2 = byteArrayOutputStream.toByteArray();
            if (objectOutputStream != null) {
                try {
                    objectOutputStream.close();
                } catch (IOException e8) {
                    Log.e(b, "Error in Data#toByteArray: ", e8);
                }
            }
            byteArrayOutputStream.close();
            return byteArray2;
        }
    }

    @DexIgnore
    public static Boolean[] a(boolean[] zArr) {
        Boolean[] boolArr = new Boolean[zArr.length];
        for (int i = 0; i < zArr.length; i++) {
            boolArr[i] = Boolean.valueOf(zArr[i]);
        }
        return boolArr;
    }

    @DexIgnore
    public static Byte[] a(byte[] bArr) {
        Byte[] bArr2 = new Byte[bArr.length];
        for (int i = 0; i < bArr.length; i++) {
            bArr2[i] = Byte.valueOf(bArr[i]);
        }
        return bArr2;
    }

    @DexIgnore
    public static Integer[] a(int[] iArr) {
        Integer[] numArr = new Integer[iArr.length];
        for (int i = 0; i < iArr.length; i++) {
            numArr[i] = Integer.valueOf(iArr[i]);
        }
        return numArr;
    }

    @DexIgnore
    public static Long[] a(long[] jArr) {
        Long[] lArr = new Long[jArr.length];
        for (int i = 0; i < jArr.length; i++) {
            lArr[i] = Long.valueOf(jArr[i]);
        }
        return lArr;
    }

    @DexIgnore
    public static Float[] a(float[] fArr) {
        Float[] fArr2 = new Float[fArr.length];
        for (int i = 0; i < fArr.length; i++) {
            fArr2[i] = Float.valueOf(fArr[i]);
        }
        return fArr2;
    }

    @DexIgnore
    public static Double[] a(double[] dArr) {
        Double[] dArr2 = new Double[dArr.length];
        for (int i = 0; i < dArr.length; i++) {
            dArr2[i] = Double.valueOf(dArr[i]);
        }
        return dArr2;
    }
}
