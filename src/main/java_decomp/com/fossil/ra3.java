package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ra3 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ra3> CREATOR; // = new ta3();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ long j;
    @DexIgnore
    public /* final */ String o;
    @DexIgnore
    public /* final */ long p;
    @DexIgnore
    public /* final */ long q;
    @DexIgnore
    public /* final */ int r;
    @DexIgnore
    public /* final */ boolean s;
    @DexIgnore
    public /* final */ boolean t;
    @DexIgnore
    public /* final */ boolean u;
    @DexIgnore
    public /* final */ String v;
    @DexIgnore
    public /* final */ Boolean w;
    @DexIgnore
    public /* final */ long x;
    @DexIgnore
    public /* final */ List<String> y;
    @DexIgnore
    public /* final */ String z;

    @DexIgnore
    public ra3(String str, String str2, String str3, long j2, String str4, long j3, long j4, String str5, boolean z2, boolean z3, String str6, long j5, long j6, int i2, boolean z4, boolean z5, boolean z6, String str7, Boolean bool, long j7, List<String> list, String str8) {
        w12.b(str);
        this.a = str;
        this.b = TextUtils.isEmpty(str2) ? null : str2;
        this.c = str3;
        this.j = j2;
        this.d = str4;
        this.e = j3;
        this.f = j4;
        this.g = str5;
        this.h = z2;
        this.i = z3;
        this.o = str6;
        this.p = j5;
        this.q = j6;
        this.r = i2;
        this.s = z4;
        this.t = z5;
        this.u = z6;
        this.v = str7;
        this.w = bool;
        this.x = j7;
        this.y = list;
        this.z = str8;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, this.a, false);
        g22.a(parcel, 3, this.b, false);
        g22.a(parcel, 4, this.c, false);
        g22.a(parcel, 5, this.d, false);
        g22.a(parcel, 6, this.e);
        g22.a(parcel, 7, this.f);
        g22.a(parcel, 8, this.g, false);
        g22.a(parcel, 9, this.h);
        g22.a(parcel, 10, this.i);
        g22.a(parcel, 11, this.j);
        g22.a(parcel, 12, this.o, false);
        g22.a(parcel, 13, this.p);
        g22.a(parcel, 14, this.q);
        g22.a(parcel, 15, this.r);
        g22.a(parcel, 16, this.s);
        g22.a(parcel, 17, this.t);
        g22.a(parcel, 18, this.u);
        g22.a(parcel, 19, this.v, false);
        g22.a(parcel, 21, this.w, false);
        g22.a(parcel, 22, this.x);
        g22.b(parcel, 23, this.y, false);
        g22.a(parcel, 24, this.z, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public ra3(String str, String str2, String str3, String str4, long j2, long j3, String str5, boolean z2, boolean z3, long j4, String str6, long j5, long j6, int i2, boolean z4, boolean z5, boolean z6, String str7, Boolean bool, long j7, List<String> list, String str8) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.j = j4;
        this.d = str4;
        this.e = j2;
        this.f = j3;
        this.g = str5;
        this.h = z2;
        this.i = z3;
        this.o = str6;
        this.p = j5;
        this.q = j6;
        this.r = i2;
        this.s = z4;
        this.t = z5;
        this.u = z6;
        this.v = str7;
        this.w = bool;
        this.x = j7;
        this.y = list;
        this.z = str8;
    }
}
