package com.fossil;

import android.content.Context;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bm {
    @DexIgnore
    @Deprecated
    public static bm a() {
        lm a = lm.a();
        if (a != null) {
            return a;
        }
        throw new IllegalStateException("WorkManager is not initialized properly.  The most likely cause is that you disabled WorkManagerInitializer in your manifest but forgot to call WorkManager#initialize in your Application#onCreate or a ContentProvider.");
    }

    @DexIgnore
    public abstract wl a(String str, ql qlVar, List<vl> list);

    @DexIgnore
    public static void a(Context context, ml mlVar) {
        lm.a(context, mlVar);
    }

    @DexIgnore
    public wl a(String str, ql qlVar, vl vlVar) {
        return a(str, qlVar, (List<vl>) Collections.singletonList(vlVar));
    }
}
