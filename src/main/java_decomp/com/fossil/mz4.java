package com.fossil;

import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mz4 extends m24<c, d, b> {
    @DexIgnore
    public static /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public b(String str) {
            wg6.b(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ List<ContactGroup> a;

        @DexIgnore
        public d(List<? extends ContactGroup> list) {
            wg6.b(list, "contactGroups");
            this.a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = mz4.class.getSimpleName();
        wg6.a((Object) simpleName, "GetAllContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public String c() {
        return d;
    }

    @DexIgnore
    public Object a(c cVar, xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List allContactGroups = zm4.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups != null) {
            return new d(allContactGroups);
        }
        return new b("Get all contact group failed");
    }
}
