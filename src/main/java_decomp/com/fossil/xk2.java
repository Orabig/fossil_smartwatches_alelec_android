package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class xk2 implements SharedPreferences.OnSharedPreferenceChangeListener {
    @DexIgnore
    public /* final */ yk2 a;

    @DexIgnore
    public xk2(yk2 yk2) {
        this.a = yk2;
    }

    @DexIgnore
    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        this.a.a(sharedPreferences, str);
    }
}
