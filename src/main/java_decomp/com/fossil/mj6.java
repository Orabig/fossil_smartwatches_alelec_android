package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mj6 implements Serializable {
    @DexIgnore
    public static /* final */ a Companion; // = new a((qg6) null);
    @DexIgnore
    public Set<? extends pj6> _options;
    @DexIgnore
    public /* final */ Pattern nativePattern;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(int i) {
            return (i & 2) != 0 ? i | 64 : i;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Serializable {
        @DexIgnore
        public static /* final */ a Companion; // = new a((qg6) null);
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int flags;
        @DexIgnore
        public /* final */ String pattern;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(qg6 qg6) {
                this();
            }
        }

        @DexIgnore
        public b(String str, int i) {
            wg6.b(str, "pattern");
            this.pattern = str;
            this.flags = i;
        }

        @DexIgnore
        private final Object readResolve() {
            Pattern compile = Pattern.compile(this.pattern, this.flags);
            wg6.a((Object) compile, "Pattern.compile(pattern, flags)");
            return new mj6(compile);
        }

        @DexIgnore
        public final int getFlags() {
            return this.flags;
        }

        @DexIgnore
        public final String getPattern() {
            return this.pattern;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements gg6<kj6> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $input;
        @DexIgnore
        public /* final */ /* synthetic */ int $startIndex;
        @DexIgnore
        public /* final */ /* synthetic */ mj6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(mj6 mj6, CharSequence charSequence, int i) {
            super(0);
            this.this$0 = mj6;
            this.$input = charSequence;
            this.$startIndex = i;
        }

        @DexIgnore
        public final kj6 invoke() {
            return this.this$0.find(this.$input, this.$startIndex);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final /* synthetic */ class d extends tg6 implements hg6<kj6, kj6> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(1);
        }

        @DexIgnore
        public final String getName() {
            return "next";
        }

        @DexIgnore
        public final hi6 getOwner() {
            return kh6.a(kj6.class);
        }

        @DexIgnore
        public final String getSignature() {
            return "next()Lkotlin/text/MatchResult;";
        }

        @DexIgnore
        public final kj6 invoke(kj6 kj6) {
            wg6.b(kj6, "p1");
            return kj6.next();
        }
    }

    @DexIgnore
    public mj6(Pattern pattern) {
        wg6.b(pattern, "nativePattern");
        this.nativePattern = pattern;
    }

    @DexIgnore
    public static /* synthetic */ kj6 find$default(mj6 mj6, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return mj6.find(charSequence, i);
    }

    @DexIgnore
    public static /* synthetic */ ti6 findAll$default(mj6 mj6, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return mj6.findAll(charSequence, i);
    }

    @DexIgnore
    public static /* synthetic */ List split$default(mj6 mj6, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return mj6.split(charSequence, i);
    }

    @DexIgnore
    private final Object writeReplace() {
        String pattern = this.nativePattern.pattern();
        wg6.a((Object) pattern, "nativePattern.pattern()");
        return new b(pattern, this.nativePattern.flags());
    }

    @DexIgnore
    public final boolean containsMatchIn(CharSequence charSequence) {
        wg6.b(charSequence, "input");
        return this.nativePattern.matcher(charSequence).find();
    }

    @DexIgnore
    public final kj6 find(CharSequence charSequence, int i) {
        wg6.b(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        wg6.a((Object) matcher, "nativePattern.matcher(input)");
        return oj6.b(matcher, i, charSequence);
    }

    @DexIgnore
    public final ti6<kj6> findAll(CharSequence charSequence, int i) {
        wg6.b(charSequence, "input");
        return yi6.a(new c(this, charSequence, i), d.INSTANCE);
    }

    @DexIgnore
    public final Set<pj6> getOptions() {
        Set<? extends pj6> set = this._options;
        if (set != null) {
            return set;
        }
        int flags = this.nativePattern.flags();
        EnumSet<E> allOf = EnumSet.allOf(pj6.class);
        vd6.a(allOf, new nj6(flags));
        Set<? extends pj6> unmodifiableSet = Collections.unmodifiableSet(allOf);
        wg6.a((Object) unmodifiableSet, "Collections.unmodifiable\u2026mask == it.value }\n    })");
        this._options = unmodifiableSet;
        return unmodifiableSet;
    }

    @DexIgnore
    public final String getPattern() {
        String pattern = this.nativePattern.pattern();
        wg6.a((Object) pattern, "nativePattern.pattern()");
        return pattern;
    }

    @DexIgnore
    public final kj6 matchEntire(CharSequence charSequence) {
        wg6.b(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        wg6.a((Object) matcher, "nativePattern.matcher(input)");
        return oj6.b(matcher, charSequence);
    }

    @DexIgnore
    public final boolean matches(CharSequence charSequence) {
        wg6.b(charSequence, "input");
        return this.nativePattern.matcher(charSequence).matches();
    }

    @DexIgnore
    public final String replace(CharSequence charSequence, String str) {
        wg6.b(charSequence, "input");
        wg6.b(str, "replacement");
        String replaceAll = this.nativePattern.matcher(charSequence).replaceAll(str);
        wg6.a((Object) replaceAll, "nativePattern.matcher(in\u2026).replaceAll(replacement)");
        return replaceAll;
    }

    @DexIgnore
    public final String replaceFirst(CharSequence charSequence, String str) {
        wg6.b(charSequence, "input");
        wg6.b(str, "replacement");
        String replaceFirst = this.nativePattern.matcher(charSequence).replaceFirst(str);
        wg6.a((Object) replaceFirst, "nativePattern.matcher(in\u2026replaceFirst(replacement)");
        return replaceFirst;
    }

    @DexIgnore
    public final List<String> split(CharSequence charSequence, int i) {
        wg6.b(charSequence, "input");
        int i2 = 0;
        if (i >= 0) {
            Matcher matcher = this.nativePattern.matcher(charSequence);
            if (!matcher.find() || i == 1) {
                return pd6.a(charSequence.toString());
            }
            int i3 = 10;
            if (i > 0) {
                i3 = ci6.b(i, 10);
            }
            ArrayList arrayList = new ArrayList(i3);
            int i4 = i - 1;
            do {
                arrayList.add(charSequence.subSequence(i2, matcher.start()).toString());
                i2 = matcher.end();
                if ((i4 >= 0 && arrayList.size() == i4) || !matcher.find()) {
                    arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
                }
                arrayList.add(charSequence.subSequence(i2, matcher.start()).toString());
                i2 = matcher.end();
                break;
            } while (!matcher.find());
            arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }

    @DexIgnore
    public final Pattern toPattern() {
        return this.nativePattern;
    }

    @DexIgnore
    public String toString() {
        String pattern = this.nativePattern.toString();
        wg6.a((Object) pattern, "nativePattern.toString()");
        return pattern;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public mj6(String str) {
        this(r2);
        wg6.b(str, "pattern");
        Pattern compile = Pattern.compile(str);
        wg6.a((Object) compile, "Pattern.compile(pattern)");
    }

    @DexIgnore
    public final String replace(CharSequence charSequence, hg6<? super kj6, ? extends CharSequence> hg6) {
        wg6.b(charSequence, "input");
        wg6.b(hg6, "transform");
        int i = 0;
        kj6 find$default = find$default(this, charSequence, 0, 2, (Object) null);
        if (find$default == null) {
            return charSequence.toString();
        }
        int length = charSequence.length();
        StringBuilder sb = new StringBuilder(length);
        while (find$default != null) {
            sb.append(charSequence, i, find$default.a().e().intValue());
            sb.append((CharSequence) hg6.invoke(find$default));
            i = find$default.a().d().intValue() + 1;
            find$default = find$default.next();
            if (i < length) {
                if (find$default == null) {
                }
            }
            if (i < length) {
                sb.append(charSequence, i, length);
            }
            String sb2 = sb.toString();
            wg6.a((Object) sb2, "sb.toString()");
            return sb2;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public mj6(String str, pj6 pj6) {
        this(r2);
        wg6.b(str, "pattern");
        wg6.b(pj6, "option");
        Pattern compile = Pattern.compile(str, Companion.a(pj6.getValue()));
        wg6.a((Object) compile, "Pattern.compile(pattern,\u2026nicodeCase(option.value))");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public mj6(String str, Set<? extends pj6> set) {
        this(r2);
        wg6.b(str, "pattern");
        wg6.b(set, "options");
        Pattern compile = Pattern.compile(str, Companion.a(oj6.b((Iterable<? extends gj6>) set)));
        wg6.a((Object) compile, "Pattern.compile(pattern,\u2026odeCase(options.toInt()))");
    }
}
