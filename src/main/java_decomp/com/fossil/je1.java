package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class je1 extends uj1 {
    @DexIgnore
    public static /* final */ oc1 CREATOR; // = new oc1((qg6) null);
    @DexIgnore
    public /* final */ rg0 b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public /* synthetic */ je1(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = rg0.values()[parcel.readInt()];
        this.c = parcel.readInt() != 0;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.HID_CODE, (Object) cw0.a((Enum<?>) this.b)), bm0.IMMEDIATE_RELEASE, (Object) Boolean.valueOf(this.c));
    }

    @DexIgnore
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.putShort(this.b.a);
        order.put((this.b.b.a << 7) | this.c ? (byte) 1 : 0);
        byte[] array = order.array();
        wg6.a(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(je1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            je1 je1 = (je1) obj;
            return this.b == je1.b && this.c == je1.c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.HIDInstr");
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.c).hashCode() + (this.b.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
    }
}
