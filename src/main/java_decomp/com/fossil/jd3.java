package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jd3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ qc3 a;
    @DexIgnore
    public /* final */ /* synthetic */ id3 b;

    @DexIgnore
    public jd3(id3 id3, qc3 qc3) {
        this.b = id3;
        this.a = qc3;
    }

    @DexIgnore
    public final void run() {
        try {
            qc3 then = this.b.b.then(this.a.b());
            if (then == null) {
                this.b.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            then.a(sc3.b, this.b);
            then.a(sc3.b, (lc3) this.b);
            then.a(sc3.b, (jc3) this.b);
        } catch (oc3 e) {
            if (e.getCause() instanceof Exception) {
                this.b.onFailure((Exception) e.getCause());
            } else {
                this.b.onFailure(e);
            }
        } catch (CancellationException unused) {
            this.b.onCanceled();
        } catch (Exception e2) {
            this.b.onFailure(e2);
        }
    }
}
