package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zs3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ SharedPreferences b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(b());

    @DexIgnore
    public zs3(Context context, String str, qq3 qq3) {
        this.a = a(context);
        this.b = context.getSharedPreferences("com.google.firebase.common.prefs:" + str, 0);
    }

    @DexIgnore
    public static Context a(Context context) {
        return (Build.VERSION.SDK_INT < 24 || w6.d(context)) ? context : w6.a(context);
    }

    @DexIgnore
    public final boolean b() {
        ApplicationInfo applicationInfo;
        if (this.b.contains("firebase_data_collection_default_enabled")) {
            return this.b.getBoolean("firebase_data_collection_default_enabled", true);
        }
        try {
            PackageManager packageManager = this.a.getPackageManager();
            if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(this.a.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_data_collection_default_enabled"))) {
                return applicationInfo.metaData.getBoolean("firebase_data_collection_default_enabled");
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return true;
    }

    @DexIgnore
    public boolean a() {
        return this.c.get();
    }
}
