package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x70 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ t70 b;
    @DexIgnore
    public /* final */ s70 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<x70> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                t70 valueOf = t70.valueOf(readString);
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    return new x70(readInt, valueOf, s70.valueOf(readString2));
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new x70[i];
        }
    }

    @DexIgnore
    public x70(int i, t70 t70, s70 s70) {
        this.a = i;
        this.b = t70;
        this.c = s70;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.NOTIFICATION_UID, (Object) Integer.valueOf(this.a)), bm0.t0, (Object) cw0.a((Enum<?>) this.b)), bm0.STATUS, (Object) cw0.a((Enum<?>) this.c));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(x70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            x70 x70 = (x70) obj;
            return this.a == x70.a && this.b == x70.b && this.c == x70.c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotificationEvent");
    }

    @DexIgnore
    public final t70 getAction() {
        return this.b;
    }

    @DexIgnore
    public final s70 getActionStatus() {
        return this.c;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return this.c.hashCode() + ((hashCode + (Integer.valueOf(this.a).hashCode() * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
