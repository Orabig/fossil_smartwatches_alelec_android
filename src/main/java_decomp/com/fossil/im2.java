package com.fossil;

import java.io.IOException;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class im2 extends jm2 {
    @DexIgnore
    public /* final */ byte[] zzb;

    @DexIgnore
    public im2(byte[] bArr) {
        if (bArr != null) {
            this.zzb = bArr;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof yl2) || zza() != ((yl2) obj).zza()) {
            return false;
        }
        if (zza() == 0) {
            return true;
        }
        if (!(obj instanceof im2)) {
            return obj.equals(this);
        }
        im2 im2 = (im2) obj;
        int zzd = zzd();
        int zzd2 = im2.zzd();
        if (zzd == 0 || zzd2 == 0 || zzd == zzd2) {
            return zza((yl2) im2, 0, zza());
        }
        return false;
    }

    @DexIgnore
    public byte zza(int i) {
        return this.zzb[i];
    }

    @DexIgnore
    public byte zzb(int i) {
        return this.zzb[i];
    }

    @DexIgnore
    public final boolean zzc() {
        int zze = zze();
        return fq2.a(this.zzb, zze, zza() + zze);
    }

    @DexIgnore
    public int zze() {
        return 0;
    }

    @DexIgnore
    public int zza() {
        return this.zzb.length;
    }

    @DexIgnore
    public final yl2 zza(int i, int i2) {
        int zzb2 = yl2.zzb(0, i2, zza());
        if (zzb2 == 0) {
            return yl2.zza;
        }
        return new fm2(this.zzb, zze(), zzb2);
    }

    @DexIgnore
    public final void zza(zl2 zl2) throws IOException {
        zl2.a(this.zzb, zze(), zza());
    }

    @DexIgnore
    public final String zza(Charset charset) {
        return new String(this.zzb, zze(), zza(), charset);
    }

    @DexIgnore
    public final boolean zza(yl2 yl2, int i, int i2) {
        if (i2 > yl2.zza()) {
            int zza = zza();
            StringBuilder sb = new StringBuilder(40);
            sb.append("Length too large: ");
            sb.append(i2);
            sb.append(zza);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 > yl2.zza()) {
            int zza2 = yl2.zza();
            StringBuilder sb2 = new StringBuilder(59);
            sb2.append("Ran off end of other: 0, ");
            sb2.append(i2);
            sb2.append(", ");
            sb2.append(zza2);
            throw new IllegalArgumentException(sb2.toString());
        } else if (!(yl2 instanceof im2)) {
            return yl2.zza(0, i2).equals(zza(0, i2));
        } else {
            im2 im2 = (im2) yl2;
            byte[] bArr = this.zzb;
            byte[] bArr2 = im2.zzb;
            int zze = zze() + i2;
            int zze2 = zze();
            int zze3 = im2.zze();
            while (zze2 < zze) {
                if (bArr[zze2] != bArr2[zze3]) {
                    return false;
                }
                zze2++;
                zze3++;
            }
            return true;
        }
    }

    @DexIgnore
    public final int zza(int i, int i2, int i3) {
        return hn2.a(i, this.zzb, zze(), i3);
    }
}
