package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rj2 extends fn2<rj2, b> implements to2 {
    @DexIgnore
    public static /* final */ rj2 zzf;
    @DexIgnore
    public static volatile yo2<rj2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd; // = 1;
    @DexIgnore
    public nn2<nj2> zze; // = fn2.m();

    @DexIgnore
    public enum a implements kn2 {
        RADS(1),
        PROVISIONING(2);
        
        @DexIgnore
        public /* final */ int zzd;

        /*
        static {
            new xj2();
        }
        */

        @DexIgnore
        public a(int i) {
            this.zzd = i;
        }

        @DexIgnore
        public static mn2 zzb() {
            return wj2.a;
        }

        @DexIgnore
        public final String toString() {
            return "<" + a.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzd + " name=" + name() + '>';
        }

        @DexIgnore
        public final int zza() {
            return this.zzd;
        }

        @DexIgnore
        public static a zza(int i) {
            if (i == 1) {
                return RADS;
            }
            if (i != 2) {
                return null;
            }
            return PROVISIONING;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fn2.b<rj2, b> implements to2 {
        @DexIgnore
        public b() {
            super(rj2.zzf);
        }

        @DexIgnore
        public /* synthetic */ b(vj2 vj2) {
            this();
        }
    }

    /*
    static {
        rj2 rj2 = new rj2();
        zzf = rj2;
        fn2.a(rj2.class, rj2);
    }
    */

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new rj2();
            case 2:
                return new b((vj2) null);
            case 3:
                return fn2.a((ro2) zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\f\u0000\u0002\u001b", new Object[]{"zzc", "zzd", a.zzb(), "zze", nj2.class});
            case 4:
                return zzf;
            case 5:
                yo2<rj2> yo2 = zzg;
                if (yo2 == null) {
                    synchronized (rj2.class) {
                        yo2 = zzg;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzf);
                            zzg = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
