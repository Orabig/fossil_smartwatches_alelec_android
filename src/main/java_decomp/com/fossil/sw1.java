package com.fossil;

import android.app.Activity;
import androidx.fragment.app.FragmentActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sw1 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public sw1(Activity activity) {
        w12.a(activity, (Object) "Activity must not be null");
        this.a = activity;
    }

    @DexIgnore
    public Activity a() {
        return (Activity) this.a;
    }

    @DexIgnore
    public FragmentActivity b() {
        return (FragmentActivity) this.a;
    }

    @DexIgnore
    public boolean c() {
        return this.a instanceof FragmentActivity;
    }

    @DexIgnore
    public final boolean d() {
        return this.a instanceof Activity;
    }
}
