package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ps2 implements dl2<os2> {
    @DexIgnore
    public static ps2 b; // = new ps2();
    @DexIgnore
    public /* final */ dl2<os2> a;

    @DexIgnore
    public ps2(dl2<os2> dl2) {
        this.a = hl2.a(dl2);
    }

    @DexIgnore
    public static boolean a() {
        return ((os2) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((os2) b.zza()).zzb();
    }

    @DexIgnore
    public final /* synthetic */ Object zza() {
        return this.a.zza();
    }

    @DexIgnore
    public ps2() {
        this(hl2.a(new rs2()));
    }
}
