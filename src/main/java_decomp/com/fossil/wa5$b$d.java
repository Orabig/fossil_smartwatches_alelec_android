package com.fossil;

import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$totalDevices$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
public final class wa5$b$d extends sf6 implements ig6<il6, xe6<? super List<? extends Device>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDashboardPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wa5$b$d(HomeDashboardPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        wa5$b$d wa5_b_d = new wa5$b$d(this.this$0, xe6);
        wa5_b_d.p$ = (il6) obj;
        return wa5_b_d;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((wa5$b$d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.y.getAllDevice();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
