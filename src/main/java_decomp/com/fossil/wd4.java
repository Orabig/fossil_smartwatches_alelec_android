package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wd4 extends vd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H; // = new SparseIntArray();
    @DexIgnore
    public long F;

    /*
    static {
        H.put(2131362676, 1);
        H.put(2131362561, 2);
        H.put(2131363153, 3);
        H.put(2131363218, 4);
        H.put(2131362979, 5);
        H.put(2131362516, 6);
        H.put(2131362185, 7);
        H.put(2131362554, 8);
        H.put(2131362521, 9);
        H.put(2131362193, 10);
        H.put(2131363150, 11);
        H.put(2131363149, 12);
        H.put(2131361919, 13);
        H.put(2131361921, 14);
        H.put(2131363212, 15);
        H.put(2131362589, 16);
        H.put(2131362594, 17);
        H.put(2131362537, 18);
        H.put(2131362642, 19);
        H.put(2131362641, 20);
        H.put(2131363157, 21);
        H.put(2131363211, 22);
        H.put(2131361934, 23);
    }
    */

    @DexIgnore
    public wd4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 24, G, H));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.F != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.F = 1;
        }
        g();
    }

    @DexIgnore
    public wd4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[13], objArr[14], objArr[23], objArr[7], objArr[10], objArr[6], objArr[9], objArr[18], objArr[8], objArr[2], objArr[16], objArr[17], objArr[20], objArr[19], objArr[1], objArr[0], objArr[5], objArr[12], objArr[11], objArr[3], objArr[21], objArr[22], objArr[15], objArr[4]);
        this.F = -1;
        this.B.setTag((Object) null);
        a(view);
        f();
    }
}
