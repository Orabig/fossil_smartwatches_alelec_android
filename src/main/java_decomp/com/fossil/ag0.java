package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ag0 extends xg6 implements ig6<if1, Float, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ym0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ag0(ym0 ym0) {
        super(2);
        this.a = ym0;
    }

    @DexIgnore
    public Object invoke(Object obj, Object obj2) {
        if1 if1 = (if1) obj;
        float floatValue = ((Number) obj2).floatValue();
        if (floatValue == 1.0f) {
            this.a.a(floatValue);
        } else {
            ym0 ym0 = this.a;
            ym0.a((floatValue * this.a.I) + ym0.H);
        }
        return cd6.a;
    }
}
