package com.fossil;

import com.fossil.bm3;
import com.fossil.hp3;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep3 {
    @DexIgnore
    public /* final */ c a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends gp3 {
        @DexIgnore
        public /* final */ /* synthetic */ Map b;
        @DexIgnore
        public /* final */ /* synthetic */ Type c;

        @DexIgnore
        public a(Map map, Type type) {
            this.b = map;
            this.c = type;
        }

        @DexIgnore
        public void a(TypeVariable<?> typeVariable) {
            this.b.put(new d(typeVariable), this.c);
        }

        @DexIgnore
        public void a(WildcardType wildcardType) {
            Type type = this.c;
            if (type instanceof WildcardType) {
                WildcardType wildcardType2 = (WildcardType) type;
                Type[] upperBounds = wildcardType.getUpperBounds();
                Type[] upperBounds2 = wildcardType2.getUpperBounds();
                Type[] lowerBounds = wildcardType.getLowerBounds();
                Type[] lowerBounds2 = wildcardType2.getLowerBounds();
                jk3.a(upperBounds.length == upperBounds2.length && lowerBounds.length == lowerBounds2.length, "Incompatible type: %s vs. %s", (Object) wildcardType, (Object) this.c);
                for (int i = 0; i < upperBounds.length; i++) {
                    ep3.b(this.b, upperBounds[i], upperBounds2[i]);
                }
                for (int i2 = 0; i2 < lowerBounds.length; i2++) {
                    ep3.b(this.b, lowerBounds[i2], lowerBounds2[i2]);
                }
            }
        }

        @DexIgnore
        public void a(ParameterizedType parameterizedType) {
            Type type = this.c;
            if (!(type instanceof WildcardType)) {
                ParameterizedType parameterizedType2 = (ParameterizedType) ep3.b(ParameterizedType.class, type);
                if (!(parameterizedType.getOwnerType() == null || parameterizedType2.getOwnerType() == null)) {
                    ep3.b(this.b, parameterizedType.getOwnerType(), parameterizedType2.getOwnerType());
                }
                jk3.a(parameterizedType.getRawType().equals(parameterizedType2.getRawType()), "Inconsistent raw type: %s vs. %s", (Object) parameterizedType, (Object) this.c);
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                Type[] actualTypeArguments2 = parameterizedType2.getActualTypeArguments();
                jk3.a(actualTypeArguments.length == actualTypeArguments2.length, "%s not compatible with %s", (Object) parameterizedType, (Object) parameterizedType2);
                for (int i = 0; i < actualTypeArguments.length; i++) {
                    ep3.b(this.b, actualTypeArguments[i], actualTypeArguments2[i]);
                }
            }
        }

        @DexIgnore
        public void a(GenericArrayType genericArrayType) {
            Type type = this.c;
            if (!(type instanceof WildcardType)) {
                Type a = hp3.a(type);
                jk3.a(a != null, "%s is not an array type.", (Object) this.c);
                ep3.b(this.b, genericArrayType.getGenericComponentType(), a);
            }
        }

        @DexIgnore
        public void a(Class<?> cls) {
            if (!(this.c instanceof WildcardType)) {
                throw new IllegalArgumentException("No type mapping from " + cls + " to " + this.c);
            }
        }
    }

    @DexIgnore
    public /* synthetic */ ep3(c cVar, a aVar) {
        this(cVar);
    }

    @DexIgnore
    public static ep3 b(Type type) {
        return new ep3().a((Map<d, ? extends Type>) b.a(type));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ bm3<d, Type> a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends c {
            @DexIgnore
            public /* final */ /* synthetic */ TypeVariable b;
            @DexIgnore
            public /* final */ /* synthetic */ c c;

            @DexIgnore
            public a(c cVar, TypeVariable typeVariable, c cVar2) {
                this.b = typeVariable;
                this.c = cVar2;
            }

            @DexIgnore
            public Type a(TypeVariable<?> typeVariable, c cVar) {
                if (typeVariable.getGenericDeclaration().equals(this.b.getGenericDeclaration())) {
                    return typeVariable;
                }
                return this.c.a(typeVariable, cVar);
            }
        }

        @DexIgnore
        public c() {
            this.a = bm3.of();
        }

        @DexIgnore
        public final c a(Map<d, ? extends Type> map) {
            bm3.b builder = bm3.builder();
            builder.a(this.a);
            for (Map.Entry next : map.entrySet()) {
                d dVar = (d) next.getKey();
                Type type = (Type) next.getValue();
                jk3.a(!dVar.a(type), "Type variable %s bound to itself", (Object) dVar);
                builder.a(dVar, type);
            }
            return new c(builder.a());
        }

        @DexIgnore
        public c(bm3<d, Type> bm3) {
            this.a = bm3;
        }

        @DexIgnore
        public final Type a(TypeVariable<?> typeVariable) {
            return a(typeVariable, new a(this, typeVariable, this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v0, types: [java.lang.reflect.Type, java.lang.reflect.TypeVariable, java.lang.reflect.TypeVariable<?>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        public Type a(TypeVariable<?> r4, c cVar) {
            Type type = this.a.get(new d(r4));
            if (type != null) {
                return new ep3(cVar, (a) null).a(type);
            }
            Type[] bounds = r4.getBounds();
            if (bounds.length == 0) {
                return r4;
            }
            Type[] a2 = new ep3(cVar, (a) null).a(bounds);
            if (!hp3.f.a || !Arrays.equals(bounds, a2)) {
                return hp3.a(r4.getGenericDeclaration(), r4.getName(), a2);
            }
            return r4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ TypeVariable<?> a;

        @DexIgnore
        public d(TypeVariable<?> typeVariable) {
            jk3.a(typeVariable);
            this.a = typeVariable;
        }

        @DexIgnore
        public static d b(Type type) {
            if (type instanceof TypeVariable) {
                return new d((TypeVariable) type);
            }
            return null;
        }

        @DexIgnore
        public boolean a(Type type) {
            if (type instanceof TypeVariable) {
                return a((TypeVariable<?>) (TypeVariable) type);
            }
            return false;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return a(((d) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return gk3.a(this.a.getGenericDeclaration(), this.a.getName());
        }

        @DexIgnore
        public String toString() {
            return this.a.toString();
        }

        @DexIgnore
        public final boolean a(TypeVariable<?> typeVariable) {
            return this.a.getGenericDeclaration().equals(typeVariable.getGenericDeclaration()) && this.a.getName().equals(typeVariable.getName());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public /* final */ AtomicInteger a;

        @DexIgnore
        public e() {
            this.a = new AtomicInteger();
        }

        @DexIgnore
        public Type a(Type type) {
            jk3.a(type);
            if ((type instanceof Class) || (type instanceof TypeVariable)) {
                return type;
            }
            if (type instanceof GenericArrayType) {
                return hp3.b(a(((GenericArrayType) type).getGenericComponentType()));
            }
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                return hp3.a(b(parameterizedType.getOwnerType()), (Class<?>) (Class) parameterizedType.getRawType(), a(parameterizedType.getActualTypeArguments()));
            } else if (type instanceof WildcardType) {
                WildcardType wildcardType = (WildcardType) type;
                if (wildcardType.getLowerBounds().length != 0) {
                    return type;
                }
                Type[] upperBounds = wildcardType.getUpperBounds();
                return hp3.a(e.class, "capture#" + this.a.incrementAndGet() + "-of ? extends " + ek3.a('&').a((Object[]) upperBounds), wildcardType.getUpperBounds());
            } else {
                throw new AssertionError("must have been one of the known types");
            }
        }

        @DexIgnore
        public final Type b(Type type) {
            if (type == null) {
                return null;
            }
            return a(type);
        }

        @DexIgnore
        public /* synthetic */ e(a aVar) {
            this();
        }

        @DexIgnore
        public final Type[] a(Type[] typeArr) {
            Type[] typeArr2 = new Type[typeArr.length];
            for (int i = 0; i < typeArr.length; i++) {
                typeArr2[i] = a(typeArr[i]);
            }
            return typeArr2;
        }
    }

    @DexIgnore
    public ep3() {
        this.a = new c();
    }

    @DexIgnore
    public static void b(Map<d, Type> map, Type type, Type type2) {
        if (!type.equals(type2)) {
            new a(map, type2).a(type);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends gp3 {
        @DexIgnore
        public static /* final */ e c; // = new e((a) null);
        @DexIgnore
        public /* final */ Map<d, Type> b; // = ym3.b();

        @DexIgnore
        public static bm3<d, Type> a(Type type) {
            b bVar = new b();
            bVar.a(c.a(type));
            return bm3.copyOf(bVar.b);
        }

        @DexIgnore
        public void a(Class<?> cls) {
            a(cls.getGenericSuperclass());
            a(cls.getGenericInterfaces());
        }

        @DexIgnore
        public void a(ParameterizedType parameterizedType) {
            Class cls = (Class) parameterizedType.getRawType();
            TypeVariable[] typeParameters = cls.getTypeParameters();
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            jk3.b(typeParameters.length == actualTypeArguments.length);
            for (int i = 0; i < typeParameters.length; i++) {
                a(new d(typeParameters[i]), actualTypeArguments[i]);
            }
            a(cls);
            a(parameterizedType.getOwnerType());
        }

        @DexIgnore
        public void a(TypeVariable<?> typeVariable) {
            a(typeVariable.getBounds());
        }

        @DexIgnore
        public void a(WildcardType wildcardType) {
            a(wildcardType.getUpperBounds());
        }

        @DexIgnore
        public final void a(d dVar, Type type) {
            if (!this.b.containsKey(dVar)) {
                Type type2 = type;
                while (type2 != null) {
                    if (dVar.a(type2)) {
                        while (type != null) {
                            type = this.b.remove(d.b(type));
                        }
                        return;
                    }
                    type2 = this.b.get(d.b(type2));
                }
                this.b.put(dVar, type);
            }
        }
    }

    @DexIgnore
    public ep3(c cVar) {
        this.a = cVar;
    }

    @DexIgnore
    public static <T> T b(Class<T> cls, Object obj) {
        try {
            return cls.cast(obj);
        } catch (ClassCastException unused) {
            throw new IllegalArgumentException(obj + " is not a " + cls.getSimpleName());
        }
    }

    @DexIgnore
    public ep3 a(Type type, Type type2) {
        HashMap b2 = ym3.b();
        jk3.a(type);
        jk3.a(type2);
        b(b2, type, type2);
        return a((Map<d, ? extends Type>) b2);
    }

    @DexIgnore
    public ep3 a(Map<d, ? extends Type> map) {
        return new ep3(this.a.a(map));
    }

    @DexIgnore
    public Type a(Type type) {
        jk3.a(type);
        if (type instanceof TypeVariable) {
            return this.a.a((TypeVariable<?>) (TypeVariable) type);
        }
        if (type instanceof ParameterizedType) {
            return a((ParameterizedType) type);
        }
        if (type instanceof GenericArrayType) {
            return a((GenericArrayType) type);
        }
        return type instanceof WildcardType ? a((WildcardType) type) : type;
    }

    @DexIgnore
    public final Type[] a(Type[] typeArr) {
        Type[] typeArr2 = new Type[typeArr.length];
        for (int i = 0; i < typeArr.length; i++) {
            typeArr2[i] = a(typeArr[i]);
        }
        return typeArr2;
    }

    @DexIgnore
    public final WildcardType a(WildcardType wildcardType) {
        return new hp3.j(a(wildcardType.getLowerBounds()), a(wildcardType.getUpperBounds()));
    }

    @DexIgnore
    public final Type a(GenericArrayType genericArrayType) {
        return hp3.b(a(genericArrayType.getGenericComponentType()));
    }

    @DexIgnore
    public final ParameterizedType a(ParameterizedType parameterizedType) {
        Type type;
        Type ownerType = parameterizedType.getOwnerType();
        if (ownerType == null) {
            type = null;
        } else {
            type = a(ownerType);
        }
        return hp3.a(type, (Class<?>) (Class) a(parameterizedType.getRawType()), a(parameterizedType.getActualTypeArguments()));
    }
}
