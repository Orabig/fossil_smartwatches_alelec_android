package com.fossil;

import android.os.Bundle;
import com.fossil.rv1;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jt1 {
    @DexIgnore
    public static /* final */ rv1.g<ka2> a; // = new rv1.g<>();
    @DexIgnore
    public static /* final */ rv1.g<bu1> b; // = new rv1.g<>();
    @DexIgnore
    public static /* final */ rv1.a<ka2, a> c; // = new zu1();
    @DexIgnore
    public static /* final */ rv1.a<bu1, GoogleSignInOptions> d; // = new av1();
    @DexIgnore
    public static /* final */ rv1<GoogleSignInOptions> e; // = new rv1<>("Auth.GOOGLE_SIGN_IN_API", d, b);
    @DexIgnore
    public static /* final */ pt1 f; // = new au1();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static class a implements rv1.d.f {
        @DexIgnore
        public /* final */ boolean a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jt1$a$a")
        @Deprecated
        /* renamed from: com.fossil.jt1$a$a  reason: collision with other inner class name */
        public static class C0025a {
            @DexIgnore
            public Boolean a; // = false;

            @DexIgnore
            public a a() {
                return new a(this);
            }
        }

        /*
        static {
            new C0025a().a();
        }
        */

        @DexIgnore
        public a(C0025a aVar) {
            this.a = aVar.a.booleanValue();
        }

        @DexIgnore
        public final Bundle a() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", (String) null);
            bundle.putBoolean("force_save_dialog", this.a);
            return bundle;
        }
    }

    /*
    static {
        rv1<lt1> rv1 = kt1.c;
        new rv1("Auth.CREDENTIALS_API", c, a);
        nt1 nt1 = kt1.d;
        new ja2();
    }
    */
}
