package com.fossil;

import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y72 {
    /*
    static {
        DataType[] dataTypeArr = {DataType.t, DataType.J, DataType.K, DataType.s, DataType.j, DataType.N, i72.e, i72.o, i72.b, i72.l, i72.a, i72.k, DataType.G, DataType.W, i72.d, i72.n, DataType.q, DataType.P, DataType.p, i72.f, i72.g, DataType.D, DataType.C, DataType.A, DataType.B, DataType.TYPE_DISTANCE_CUMULATIVE, DataType.y, DataType.o, DataType.O, DataType.g, DataType.h, DataType.Q, DataType.R, DataType.v, DataType.S, DataType.E, DataType.Y, DataType.T, DataType.w, DataType.x, i72.h, DataType.H, DataType.I, DataType.Z, i72.i, i72.c, i72.m, DataType.r, DataType.U, DataType.u, DataType.z, DataType.V, DataType.f, DataType.i, DataType.TYPE_STEP_COUNT_CUMULATIVE, DataType.e, i72.j, DataType.F, DataType.X, DataType.L, DataType.M};
        DataType[] dataTypeArr2 = {i72.e, i72.o, i72.b, i72.l, i72.a, i72.k, i72.d, i72.n, i72.f, i72.g, i72.h, i72.i, i72.c, i72.m, i72.j};
    }
    */

    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    public static DataType a(String str) {
        char c;
        switch (str.hashCode()) {
            case -2060095039:
                if (str.equals("com.google.cycling.wheel_revolution.rpm")) {
                    c = 24;
                    break;
                }
            case -2023954015:
                if (str.equals("com.google.location.bounding_box")) {
                    c = '%';
                    break;
                }
            case -2001464928:
                if (str.equals("com.google.internal.symptom")) {
                    c = 30;
                    break;
                }
            case -1999891138:
                if (str.equals("com.google.heart_minutes")) {
                    c = 31;
                    break;
                }
            case -1939429191:
                if (str.equals("com.google.blood_glucose.summary")) {
                    c = 10;
                    break;
                }
            case -1783842905:
                if (str.equals("com.google.accelerometer")) {
                    c = 0;
                    break;
                }
            case -1757812901:
                if (str.equals("com.google.location.sample")) {
                    c = '&';
                    break;
                }
            case -1659958877:
                if (str.equals("com.google.menstruation")) {
                    c = '(';
                    break;
                }
            case -1487055015:
                if (str.equals("com.google.body.temperature.basal.summary")) {
                    c = 7;
                    break;
                }
            case -1466904157:
                if (str.equals("com.google.floor_change.summary")) {
                    c = 28;
                    break;
                }
            case -1465729060:
                if (str.equals("com.google.internal.primary_device")) {
                    c = '>';
                    break;
                }
            case -1431431801:
                if (str.equals("com.google.height.summary")) {
                    c = '$';
                    break;
                }
            case -1248818137:
                if (str.equals("com.google.distance.delta")) {
                    c = 26;
                    break;
                }
            case -1196687875:
                if (str.equals("com.google.internal.session.v2")) {
                    c = '3';
                    break;
                }
            case -1196687874:
                if (str.equals("com.google.internal.session.v3")) {
                    c = '4';
                    break;
                }
            case -1103712522:
                if (str.equals("com.google.heart_minutes.summary")) {
                    c = ' ';
                    break;
                }
            case -1102520626:
                if (str.equals("com.google.step_count.delta")) {
                    c = '8';
                    break;
                }
            case -1091068721:
                if (str.equals("com.google.height")) {
                    c = '#';
                    break;
                }
            case -922976890:
                if (str.equals("com.google.cycling.pedaling.cumulative")) {
                    c = 22;
                    break;
                }
            case -900592674:
                if (str.equals("com.google.cycling.pedaling.cadence")) {
                    c = 21;
                    break;
                }
            case -886569606:
                if (str.equals("com.google.location.track")) {
                    c = '\'';
                    break;
                }
            case -777285735:
                if (str.equals("com.google.heart_rate.summary")) {
                    c = '\"';
                    break;
                }
            case -700668164:
                if (str.equals("com.google.internal.goal")) {
                    c = 29;
                    break;
                }
            case -661631456:
                if (str.equals("com.google.weight")) {
                    c = ';';
                    break;
                }
            case -424876584:
                if (str.equals("com.google.weight.summary")) {
                    c = '<';
                    break;
                }
            case -362418992:
                if (str.equals("com.google.body.temperature")) {
                    c = 14;
                    break;
                }
            case -217611775:
                if (str.equals("com.google.blood_glucose")) {
                    c = 9;
                    break;
                }
            case -185830635:
                if (str.equals("com.google.power.summary")) {
                    c = '0';
                    break;
                }
            case -177293656:
                if (str.equals("com.google.nutrition.summary")) {
                    c = '+';
                    break;
                }
            case -164586193:
                if (str.equals("com.google.activity.exercise")) {
                    c = 2;
                    break;
                }
            case -98150574:
                if (str.equals("com.google.heart_rate.bpm")) {
                    c = '!';
                    break;
                }
            case -56824761:
                if (str.equals("com.google.calories.bmr")) {
                    c = 16;
                    break;
                }
            case 53773386:
                if (str.equals("com.google.blood_pressure.summary")) {
                    c = 12;
                    break;
                }
            case 269180370:
                if (str.equals("com.google.activity.samples")) {
                    c = 3;
                    break;
                }
            case 295793957:
                if (str.equals("com.google.sensor.events")) {
                    c = '1';
                    break;
                }
            case 296250623:
                if (str.equals("com.google.calories.bmr.summary")) {
                    c = 17;
                    break;
                }
            case 324760871:
                if (str.equals("com.google.step_count.cadence")) {
                    c = '6';
                    break;
                }
            case 378060028:
                if (str.equals("com.google.activity.segment")) {
                    c = 4;
                    break;
                }
            case 529727579:
                if (str.equals("com.google.power.sample")) {
                    c = '/';
                    break;
                }
            case 657433501:
                if (str.equals("com.google.step_count.cumulative")) {
                    c = '7';
                    break;
                }
            case 682891187:
                if (str.equals("com.google.body.fat.percentage")) {
                    c = 8;
                    break;
                }
            case 841663855:
                if (str.equals("com.google.activity.summary")) {
                    c = 5;
                    break;
                }
            case 877955159:
                if (str.equals("com.google.speed.summary")) {
                    c = '5';
                    break;
                }
            case 899666941:
                if (str.equals("com.google.calories.expended")) {
                    c = 18;
                    break;
                }
            case 936279698:
                if (str.equals("com.google.blood_pressure")) {
                    c = 11;
                    break;
                }
            case 946706510:
                if (str.equals("com.google.hydration")) {
                    c = '*';
                    break;
                }
            case 946938859:
                if (str.equals("com.google.stride_model")) {
                    c = '9';
                    break;
                }
            case 1029221057:
                if (str.equals("com.google.device_on_body")) {
                    c = '=';
                    break;
                }
            case 1098265835:
                if (str.equals("com.google.floor_change")) {
                    c = 27;
                    break;
                }
            case 1111714923:
                if (str.equals("com.google.body.fat.percentage.summary")) {
                    c = 13;
                    break;
                }
            case 1214093899:
                if (str.equals("com.google.vaginal_spotting")) {
                    c = ':';
                    break;
                }
            case 1404118825:
                if (str.equals("com.google.oxygen_saturation")) {
                    c = '-';
                    break;
                }
            case 1439932546:
                if (str.equals("com.google.ovulation_test")) {
                    c = ',';
                    break;
                }
            case 1483133089:
                if (str.equals("com.google.body.temperature.basal")) {
                    c = 6;
                    break;
                }
            case 1524007137:
                if (str.equals("com.google.cycling.wheel_revolution.cumulative")) {
                    c = 23;
                    break;
                }
            case 1532018766:
                if (str.equals("com.google.active_minutes")) {
                    c = 1;
                    break;
                }
            case 1633152752:
                if (str.equals("com.google.nutrition")) {
                    c = ')';
                    break;
                }
            case 1921738212:
                if (str.equals("com.google.distance.cumulative")) {
                    c = 25;
                    break;
                }
            case 1925848149:
                if (str.equals("com.google.cervical_position")) {
                    c = 20;
                    break;
                }
            case 1975902189:
                if (str.equals("com.google.cervical_mucus")) {
                    c = 19;
                    break;
                }
            case 2051843553:
                if (str.equals("com.google.oxygen_saturation.summary")) {
                    c = '.';
                    break;
                }
            case 2053496735:
                if (str.equals("com.google.speed")) {
                    c = '2';
                    break;
                }
            case 2131809416:
                if (str.equals("com.google.body.temperature.summary")) {
                    c = 15;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                return DataType.t;
            case 1:
                return DataType.K;
            case 2:
                return DataType.J;
            case 3:
                return DataType.s;
            case 4:
                return DataType.j;
            case 5:
                return DataType.N;
            case 6:
                return i72.e;
            case 7:
                return i72.o;
            case 8:
                return DataType.G;
            case 9:
                return i72.b;
            case 10:
                return i72.l;
            case 11:
                return i72.a;
            case 12:
                return i72.k;
            case 13:
                return DataType.W;
            case 14:
                return i72.d;
            case 15:
                return i72.n;
            case 16:
                return DataType.q;
            case 17:
                return DataType.P;
            case 18:
                return DataType.p;
            case 19:
                return i72.f;
            case 20:
                return i72.g;
            case 21:
                return DataType.D;
            case 22:
                return DataType.C;
            case 23:
                return DataType.A;
            case 24:
                return DataType.B;
            case 25:
                return DataType.TYPE_DISTANCE_CUMULATIVE;
            case 26:
                return DataType.y;
            case 27:
                return DataType.o;
            case 28:
                return DataType.O;
            case 29:
                return DataType.g;
            case 30:
                return DataType.h;
            case 31:
                return DataType.Q;
            case ' ':
                return DataType.R;
            case '!':
                return DataType.v;
            case '\"':
                return DataType.S;
            case '#':
                return DataType.E;
            case '$':
                return DataType.Y;
            case '%':
                return DataType.T;
            case '&':
                return DataType.w;
            case '\'':
                return DataType.x;
            case '(':
                return i72.h;
            case ')':
                return DataType.H;
            case '*':
                return DataType.I;
            case '+':
                return DataType.Z;
            case ',':
                return i72.i;
            case '-':
                return i72.c;
            case '.':
                return i72.m;
            case '/':
                return DataType.r;
            case '0':
                return DataType.U;
            case '1':
                return DataType.u;
            case '2':
                return DataType.z;
            case '3':
                return DataType.a.a;
            case '4':
                return DataType.a.b;
            case '5':
                return DataType.V;
            case '6':
                return DataType.f;
            case '7':
                return DataType.TYPE_STEP_COUNT_CUMULATIVE;
            case '8':
                return DataType.e;
            case '9':
                return DataType.i;
            case ':':
                return i72.j;
            case ';':
                return DataType.F;
            case '<':
                return DataType.X;
            case '=':
                return DataType.L;
            case '>':
                return DataType.M;
            default:
                return null;
        }
    }
}
