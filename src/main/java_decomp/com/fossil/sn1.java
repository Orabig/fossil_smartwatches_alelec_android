package com.fossil;

import com.fossil.w60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class sn1 extends tg6 implements hg6<byte[], w60> {
    @DexIgnore
    public sn1(w60.c cVar) {
        super(1, cVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(w60.c.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/HellasBatteryConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((w60.c) this.receiver).a((byte[]) obj);
    }
}
