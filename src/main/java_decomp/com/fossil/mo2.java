package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo2 {
    @DexIgnore
    public static /* final */ ko2 a; // = c();
    @DexIgnore
    public static /* final */ ko2 b; // = new no2();

    @DexIgnore
    public static ko2 a() {
        return a;
    }

    @DexIgnore
    public static ko2 b() {
        return b;
    }

    @DexIgnore
    public static ko2 c() {
        try {
            return (ko2) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
