package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g00 implements vr {
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public g00(Object obj) {
        q00.a(obj);
        this.b = obj;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(this.b.toString().getBytes(vr.a));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof g00) {
            return this.b.equals(((g00) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ObjectKey{object=" + this.b + '}';
    }
}
