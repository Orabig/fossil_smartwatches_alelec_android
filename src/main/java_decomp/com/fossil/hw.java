package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hw implements rt<Bitmap>, nt {
    @DexIgnore
    public /* final */ Bitmap a;
    @DexIgnore
    public /* final */ au b;

    @DexIgnore
    public hw(Bitmap bitmap, au auVar) {
        q00.a(bitmap, "Bitmap must not be null");
        this.a = bitmap;
        q00.a(auVar, "BitmapPool must not be null");
        this.b = auVar;
    }

    @DexIgnore
    public static hw a(Bitmap bitmap, au auVar) {
        if (bitmap == null) {
            return null;
        }
        return new hw(bitmap, auVar);
    }

    @DexIgnore
    public int b() {
        return r00.a(this.a);
    }

    @DexIgnore
    public Class<Bitmap> c() {
        return Bitmap.class;
    }

    @DexIgnore
    public void d() {
        this.a.prepareToDraw();
    }

    @DexIgnore
    public void a() {
        this.b.a(this.a);
    }

    @DexIgnore
    public Bitmap get() {
        return this.a;
    }
}
