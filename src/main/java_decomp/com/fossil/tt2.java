package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface tt2 {
    @DexIgnore
    boolean zza();

    @DexIgnore
    double zzb();

    @DexIgnore
    long zzc();

    @DexIgnore
    long zzd();

    @DexIgnore
    String zze();
}
