package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Calorie implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Calorie> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mResolutionInSecond;
    @DexIgnore
    public /* final */ int mTotal;
    @DexIgnore
    public /* final */ ArrayList<Byte> mValues;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<Calorie> {
        @DexIgnore
        public Calorie createFromParcel(Parcel parcel) {
            return new Calorie(parcel);
        }

        @DexIgnore
        public Calorie[] newArray(int i) {
            return new Calorie[i];
        }
    }

    @DexIgnore
    public Calorie(int i, ArrayList<Byte> arrayList, int i2) {
        this.mResolutionInSecond = i;
        this.mValues = arrayList;
        this.mTotal = i2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Calorie)) {
            return false;
        }
        Calorie calorie = (Calorie) obj;
        if (this.mResolutionInSecond == calorie.mResolutionInSecond && this.mValues.equals(calorie.mValues) && this.mTotal == calorie.mTotal) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int getResolutionInSecond() {
        return this.mResolutionInSecond;
    }

    @DexIgnore
    public int getTotal() {
        return this.mTotal;
    }

    @DexIgnore
    public ArrayList<Byte> getValues() {
        return this.mValues;
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.mResolutionInSecond) * 31) + this.mValues.hashCode()) * 31) + this.mTotal;
    }

    @DexIgnore
    public String toString() {
        return "Calorie{mResolutionInSecond=" + this.mResolutionInSecond + ",mValues=" + this.mValues + ",mTotal=" + this.mTotal + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mResolutionInSecond);
        parcel.writeList(this.mValues);
        parcel.writeInt(this.mTotal);
    }

    @DexIgnore
    public Calorie(Parcel parcel) {
        this.mResolutionInSecond = parcel.readInt();
        this.mValues = new ArrayList<>();
        parcel.readList(this.mValues, Calorie.class.getClassLoader());
        this.mTotal = parcel.readInt();
    }
}
