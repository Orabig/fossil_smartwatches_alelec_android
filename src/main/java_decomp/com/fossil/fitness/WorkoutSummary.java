package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSummary implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<WorkoutSummary> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mActiveDuration;
    @DexIgnore
    public /* final */ Short mAverageHeartrate;
    @DexIgnore
    public /* final */ Integer mAveragePace;
    @DexIgnore
    public /* final */ int mCalorie;
    @DexIgnore
    public /* final */ int mDistanceInCentimeters;
    @DexIgnore
    public /* final */ int mDuration;
    @DexIgnore
    public /* final */ int mId;
    @DexIgnore
    public /* final */ Short mMaxHeartrate;
    @DexIgnore
    public /* final */ int mSteps;
    @DexIgnore
    public /* final */ WorkoutType mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<WorkoutSummary> {
        @DexIgnore
        public WorkoutSummary createFromParcel(Parcel parcel) {
            return new WorkoutSummary(parcel);
        }

        @DexIgnore
        public WorkoutSummary[] newArray(int i) {
            return new WorkoutSummary[i];
        }
    }

    @DexIgnore
    public WorkoutSummary(int i, WorkoutType workoutType, int i2, int i3, int i4, int i5, int i6, Short sh, Short sh2, Integer num) {
        this.mId = i;
        this.mType = workoutType;
        this.mDuration = i2;
        this.mActiveDuration = i3;
        this.mSteps = i4;
        this.mDistanceInCentimeters = i5;
        this.mCalorie = i6;
        this.mAverageHeartrate = sh;
        this.mMaxHeartrate = sh2;
        this.mAveragePace = num;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        Short sh;
        Short sh2;
        if (!(obj instanceof WorkoutSummary)) {
            return false;
        }
        WorkoutSummary workoutSummary = (WorkoutSummary) obj;
        if (this.mId != workoutSummary.mId || this.mType != workoutSummary.mType || this.mDuration != workoutSummary.mDuration || this.mActiveDuration != workoutSummary.mActiveDuration || this.mSteps != workoutSummary.mSteps || this.mDistanceInCentimeters != workoutSummary.mDistanceInCentimeters || this.mCalorie != workoutSummary.mCalorie) {
            return false;
        }
        if ((this.mAverageHeartrate != null || workoutSummary.mAverageHeartrate != null) && ((sh2 = this.mAverageHeartrate) == null || !sh2.equals(workoutSummary.mAverageHeartrate))) {
            return false;
        }
        if ((this.mMaxHeartrate != null || workoutSummary.mMaxHeartrate != null) && ((sh = this.mMaxHeartrate) == null || !sh.equals(workoutSummary.mMaxHeartrate))) {
            return false;
        }
        if ((this.mAveragePace != null || workoutSummary.mAveragePace != null) && ((num = this.mAveragePace) == null || !num.equals(workoutSummary.mAveragePace))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int getActiveDuration() {
        return this.mActiveDuration;
    }

    @DexIgnore
    public Short getAverageHeartrate() {
        return this.mAverageHeartrate;
    }

    @DexIgnore
    public Integer getAveragePace() {
        return this.mAveragePace;
    }

    @DexIgnore
    public int getCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public int getDistanceInCentimeters() {
        return this.mDistanceInCentimeters;
    }

    @DexIgnore
    public int getDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public Short getMaxHeartrate() {
        return this.mMaxHeartrate;
    }

    @DexIgnore
    public int getSteps() {
        return this.mSteps;
    }

    @DexIgnore
    public WorkoutType getType() {
        return this.mType;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((((527 + this.mId) * 31) + this.mType.hashCode()) * 31) + this.mDuration) * 31) + this.mActiveDuration) * 31) + this.mSteps) * 31) + this.mDistanceInCentimeters) * 31) + this.mCalorie) * 31;
        Short sh = this.mAverageHeartrate;
        int i = 0;
        int hashCode2 = (hashCode + (sh == null ? 0 : sh.hashCode())) * 31;
        Short sh2 = this.mMaxHeartrate;
        int hashCode3 = (hashCode2 + (sh2 == null ? 0 : sh2.hashCode())) * 31;
        Integer num = this.mAveragePace;
        if (num != null) {
            i = num.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSummary{mId=" + this.mId + ",mType=" + this.mType + ",mDuration=" + this.mDuration + ",mActiveDuration=" + this.mActiveDuration + ",mSteps=" + this.mSteps + ",mDistanceInCentimeters=" + this.mDistanceInCentimeters + ",mCalorie=" + this.mCalorie + ",mAverageHeartrate=" + this.mAverageHeartrate + ",mMaxHeartrate=" + this.mMaxHeartrate + ",mAveragePace=" + this.mAveragePace + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mId);
        parcel.writeInt(this.mType.ordinal());
        parcel.writeInt(this.mDuration);
        parcel.writeInt(this.mActiveDuration);
        parcel.writeInt(this.mSteps);
        parcel.writeInt(this.mDistanceInCentimeters);
        parcel.writeInt(this.mCalorie);
        if (this.mAverageHeartrate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mAverageHeartrate.shortValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mMaxHeartrate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mMaxHeartrate.shortValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mAveragePace != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mAveragePace.intValue());
            return;
        }
        parcel.writeByte((byte) 0);
    }

    @DexIgnore
    public WorkoutSummary(Parcel parcel) {
        this.mId = parcel.readInt();
        this.mType = WorkoutType.values()[parcel.readInt()];
        this.mDuration = parcel.readInt();
        this.mActiveDuration = parcel.readInt();
        this.mSteps = parcel.readInt();
        this.mDistanceInCentimeters = parcel.readInt();
        this.mCalorie = parcel.readInt();
        if (parcel.readByte() == 0) {
            this.mAverageHeartrate = null;
        } else {
            this.mAverageHeartrate = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mMaxHeartrate = null;
        } else {
            this.mMaxHeartrate = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mAveragePace = null;
        } else {
            this.mAveragePace = Integer.valueOf(parcel.readInt());
        }
    }
}
