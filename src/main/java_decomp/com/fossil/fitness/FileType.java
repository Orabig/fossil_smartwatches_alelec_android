package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum FileType {
    __0014__(20),
    __0016__(22),
    __0080__(128),
    UNKNOWN(255);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public FileType(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
