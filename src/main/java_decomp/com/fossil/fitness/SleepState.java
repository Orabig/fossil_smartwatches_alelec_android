package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum SleepState {
    AWAKE(0),
    SLEEP(1),
    DEEP_SLEEP(2),
    UNKNOWN(3);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public SleepState(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
