package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GpsDataPoint implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<GpsDataPoint> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ float mHorizontalAccuracy;
    @DexIgnore
    public /* final */ double mLatitude;
    @DexIgnore
    public /* final */ double mLongitude;
    @DexIgnore
    public /* final */ int mTimestamp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<GpsDataPoint> {
        @DexIgnore
        public GpsDataPoint createFromParcel(Parcel parcel) {
            return new GpsDataPoint(parcel);
        }

        @DexIgnore
        public GpsDataPoint[] newArray(int i) {
            return new GpsDataPoint[i];
        }
    }

    @DexIgnore
    public GpsDataPoint(int i, double d, double d2, float f) {
        this.mTimestamp = i;
        this.mLongitude = d;
        this.mLatitude = d2;
        this.mHorizontalAccuracy = f;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof GpsDataPoint)) {
            return false;
        }
        GpsDataPoint gpsDataPoint = (GpsDataPoint) obj;
        if (this.mTimestamp == gpsDataPoint.mTimestamp && this.mLongitude == gpsDataPoint.mLongitude && this.mLatitude == gpsDataPoint.mLatitude && this.mHorizontalAccuracy == gpsDataPoint.mHorizontalAccuracy) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public float getHorizontalAccuracy() {
        return this.mHorizontalAccuracy;
    }

    @DexIgnore
    public double getLatitude() {
        return this.mLatitude;
    }

    @DexIgnore
    public double getLongitude() {
        return this.mLongitude;
    }

    @DexIgnore
    public int getTimestamp() {
        return this.mTimestamp;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((527 + this.mTimestamp) * 31) + ((int) (Double.doubleToLongBits(this.mLongitude) ^ (Double.doubleToLongBits(this.mLongitude) >>> 32)))) * 31) + ((int) (Double.doubleToLongBits(this.mLatitude) ^ (Double.doubleToLongBits(this.mLatitude) >>> 32)))) * 31) + Float.floatToIntBits(this.mHorizontalAccuracy);
    }

    @DexIgnore
    public String toString() {
        return "GpsDataPoint{mTimestamp=" + this.mTimestamp + ",mLongitude=" + this.mLongitude + ",mLatitude=" + this.mLatitude + ",mHorizontalAccuracy=" + this.mHorizontalAccuracy + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mTimestamp);
        parcel.writeDouble(this.mLongitude);
        parcel.writeDouble(this.mLatitude);
        parcel.writeFloat(this.mHorizontalAccuracy);
    }

    @DexIgnore
    public GpsDataPoint(Parcel parcel) {
        this.mTimestamp = parcel.readInt();
        this.mLongitude = parcel.readDouble();
        this.mLatitude = parcel.readDouble();
        this.mHorizontalAccuracy = parcel.readFloat();
    }
}
