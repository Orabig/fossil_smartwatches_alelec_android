package com.fossil;

import com.fossil.im3;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em3<K, V> extends im3.b<K> {
    @DexIgnore
    public /* final */ bm3<K, V> map;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ bm3<K, ?> map;

        @DexIgnore
        public a(bm3<K, ?> bm3) {
            this.map = bm3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.keySet();
        }
    }

    @DexIgnore
    public em3(bm3<K, V> bm3) {
        this.map = bm3;
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return this.map.containsKey(obj);
    }

    @DexIgnore
    public K get(int i) {
        return this.map.entrySet().asList().get(i).getKey();
    }

    @DexIgnore
    public boolean isPartialView() {
        return true;
    }

    @DexIgnore
    public int size() {
        return this.map.size();
    }

    @DexIgnore
    public Object writeReplace() {
        return new a(this.map);
    }

    @DexIgnore
    public jo3<K> iterator() {
        return this.map.keyIterator();
    }
}
