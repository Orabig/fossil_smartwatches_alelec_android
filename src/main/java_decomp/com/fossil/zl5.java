package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zl5 implements Factory<yl5> {
    @DexIgnore
    public static ProfileOptInPresenter a(vl5 vl5, UpdateUser updateUser, UserRepository userRepository) {
        return new ProfileOptInPresenter(vl5, updateUser, userRepository);
    }
}
