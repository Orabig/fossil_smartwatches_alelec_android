package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr0 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ yy0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zr0(yy0 yy0) {
        super(1);
        this.a = yy0;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        yy0 yy0 = this.a;
        yy0.B = ((uv0) obj).J;
        fo0 fo0 = yy0.B;
        if (fo0 == null) {
            yy0.a(km1.a(yy0.v, (eh1) null, sk1.FLOW_BROKEN, (bn0) null, 5));
        } else if (xp0.a.a(fo0, yy0.D)) {
            yy0 yy02 = this.a;
            yy02.a(km1.a(yy02.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
        } else {
            yy0 yy03 = this.a;
            if1.a((if1) yy03, (qv0) new p61(yy03.D, yy03.w), (hg6) new jv0(yy03), (hg6) new ex0(yy03), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
        }
        return cd6.a;
    }
}
