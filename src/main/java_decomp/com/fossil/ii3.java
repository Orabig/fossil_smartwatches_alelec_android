package com.fossil;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import com.facebook.places.internal.LocationScannerImpl;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ii3 {
    @DexIgnore
    public /* final */ TextPaint a; // = new TextPaint(1);
    @DexIgnore
    public /* final */ si3 b; // = new a();
    @DexIgnore
    public float c;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public WeakReference<b> e; // = new WeakReference<>((Object) null);
    @DexIgnore
    public qi3 f;

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        int[] getState();

        @DexIgnore
        boolean onStateChange(int[] iArr);
    }

    @DexIgnore
    public ii3(b bVar) {
        a(bVar);
    }

    @DexIgnore
    public TextPaint b() {
        return this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends si3 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(Typeface typeface, boolean z) {
            if (!z) {
                boolean unused = ii3.this.d = true;
                b bVar = (b) ii3.this.e.get();
                if (bVar != null) {
                    bVar.a();
                }
            }
        }

        @DexIgnore
        public void a(int i) {
            boolean unused = ii3.this.d = true;
            b bVar = (b) ii3.this.e.get();
            if (bVar != null) {
                bVar.a();
            }
        }
    }

    @DexIgnore
    public void a(b bVar) {
        this.e = new WeakReference<>(bVar);
    }

    @DexIgnore
    public void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public float a(String str) {
        if (!this.d) {
            return this.c;
        }
        this.c = a((CharSequence) str);
        this.d = false;
        return this.c;
    }

    @DexIgnore
    public final float a(CharSequence charSequence) {
        return charSequence == null ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : this.a.measureText(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public qi3 a() {
        return this.f;
    }

    @DexIgnore
    public void a(qi3 qi3, Context context) {
        if (this.f != qi3) {
            this.f = qi3;
            if (qi3 != null) {
                qi3.c(context, this.a, this.b);
                b bVar = (b) this.e.get();
                if (bVar != null) {
                    this.a.drawableState = bVar.getState();
                }
                qi3.b(context, this.a, this.b);
                this.d = true;
            }
            b bVar2 = (b) this.e.get();
            if (bVar2 != null) {
                bVar2.a();
                bVar2.onStateChange(bVar2.getState());
            }
        }
    }

    @DexIgnore
    public void a(Context context) {
        this.f.b(context, this.a, this.b);
    }
}
