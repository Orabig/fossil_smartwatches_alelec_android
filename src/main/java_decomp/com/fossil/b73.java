package com.fossil;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b73 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ev2 a;
    @DexIgnore
    public /* final */ /* synthetic */ j03 b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurementDynamiteService d;

    @DexIgnore
    public b73(AppMeasurementDynamiteService appMeasurementDynamiteService, ev2 ev2, j03 j03, String str) {
        this.d = appMeasurementDynamiteService;
        this.a = ev2;
        this.b = j03;
        this.c = str;
    }

    @DexIgnore
    public final void run() {
        this.d.a.F().a(this.a, this.b, this.c);
    }
}
