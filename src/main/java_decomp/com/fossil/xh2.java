package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xh2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xh2(ov2 ov2, Activity activity, String str, String str2) {
        super(ov2);
        this.h = ov2;
        this.e = activity;
        this.f = str;
        this.g = str2;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        this.h.g.setCurrentScreen(z52.a(this.e), this.f, this.g, this.a);
    }
}
