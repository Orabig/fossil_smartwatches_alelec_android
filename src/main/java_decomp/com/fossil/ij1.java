package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ij1 extends xg6 implements hg6<if1, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ yb0 a;
    @DexIgnore
    public /* final */ /* synthetic */ ii1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ij1(yb0 yb0, ii1 ii1, if1 if1) {
        super(1);
        this.a = yb0;
        this.b = ii1;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        if1 if1 = (if1) obj;
        if (if1.v.b == sk1.SUCCESS) {
            Object d = if1.d();
            if (d instanceof Integer) {
                this.b.b.post(new wd1(this, d));
            } else {
                this.b.b.post(new sf1(this));
            }
        } else {
            this.b.b.post(new oh1(this, if1));
            this.b.a(if1.v);
        }
        return cd6.a;
    }
}
