package com.fossil;

import android.support.v4.media.MediaBrowserCompat;
import com.fossil.mc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq4$f$c extends MediaBrowserCompat.b {
    @DexIgnore
    public /* final */ /* synthetic */ lk6 c;
    @DexIgnore
    public /* final */ /* synthetic */ jh6 d;

    @DexIgnore
    public fq4$f$c(lk6 lk6, jh6 jh6) {
        this.c = lk6;
        this.d = jh6;
    }

    @DexIgnore
    public void a() {
        fq4$f$c.super.a();
        if (this.c.isActive()) {
            lk6 lk6 = this.c;
            mc6.a aVar = mc6.Companion;
            lk6.resumeWith(mc6.m1constructorimpl((MediaBrowserCompat) this.d.element));
        }
    }

    @DexIgnore
    public void b() {
        fq4$f$c.super.b();
        if (this.c.isActive()) {
            lk6 lk6 = this.c;
            mc6.a aVar = mc6.Companion;
            lk6.resumeWith(mc6.m1constructorimpl((Object) null));
        }
    }
}
