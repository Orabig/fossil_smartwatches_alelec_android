package com.fossil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pa3 extends SSLSocketFactory {
    @DexIgnore
    public /* final */ SSLSocketFactory a;

    @DexIgnore
    public pa3() {
        this(HttpsURLConnection.getDefaultSSLSocketFactory());
    }

    @DexIgnore
    public final SSLSocket a(SSLSocket sSLSocket) {
        return new oa3(this, sSLSocket);
    }

    @DexIgnore
    public final Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return a((SSLSocket) this.a.createSocket(socket, str, i, z));
    }

    @DexIgnore
    public final String[] getDefaultCipherSuites() {
        return this.a.getDefaultCipherSuites();
    }

    @DexIgnore
    public final String[] getSupportedCipherSuites() {
        return this.a.getSupportedCipherSuites();
    }

    @DexIgnore
    public pa3(SSLSocketFactory sSLSocketFactory) {
        this.a = sSLSocketFactory;
    }

    @DexIgnore
    public final Socket createSocket(String str, int i) throws IOException {
        return a((SSLSocket) this.a.createSocket(str, i));
    }

    @DexIgnore
    public final Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return a((SSLSocket) this.a.createSocket(inetAddress, i));
    }

    @DexIgnore
    public final Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException {
        return a((SSLSocket) this.a.createSocket(str, i, inetAddress, i2));
    }

    @DexIgnore
    public final Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return a((SSLSocket) this.a.createSocket(inetAddress, i, inetAddress2, i2));
    }

    @DexIgnore
    public final Socket createSocket() throws IOException {
        return a((SSLSocket) this.a.createSocket());
    }
}
