package com.fossil;

import com.fossil.yq6;
import com.misfit.frameworks.common.enums.Action;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds6 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public volatile tr6 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public volatile boolean d;

    @DexIgnore
    public ds6(OkHttpClient okHttpClient, boolean z) {
        this.a = okHttpClient;
    }

    @DexIgnore
    public void a() {
        this.d = true;
        tr6 tr6 = this.b;
        if (tr6 != null) {
            tr6.a();
        }
    }

    @DexIgnore
    public boolean b() {
        return this.d;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        yq6 t = chain.t();
        as6 as6 = (as6) chain;
        dq6 e = as6.e();
        pq6 f = as6.f();
        tr6 tr6 = new tr6(this.a.e(), a(t.g()), e, f, this.c);
        this.b = tr6;
        Response response = null;
        int i = 0;
        while (!this.d) {
            try {
                Response a2 = as6.a(t, tr6, (wr6) null, (pr6) null);
                if (response != null) {
                    Response.a E = a2.E();
                    Response.a E2 = response.E();
                    E2.a((zq6) null);
                    E.d(E2.a());
                    a2 = E.a();
                }
                try {
                    yq6 a3 = a(a2, tr6.h());
                    if (a3 == null) {
                        tr6.f();
                        return a2;
                    }
                    fr6.a((Closeable) a2.k());
                    int i2 = i + 1;
                    if (i2 <= 20) {
                        a3.a();
                        if (!a(a2, a3.g())) {
                            tr6.f();
                            tr6 = new tr6(this.a.e(), a(a3.g()), e, f, this.c);
                            this.b = tr6;
                        } else if (tr6.b() != null) {
                            throw new IllegalStateException("Closing the body of " + a2 + " didn't close its backing stream. Bad interceptor?");
                        }
                        response = a2;
                        t = a3;
                        i = i2;
                    } else {
                        tr6.f();
                        throw new ProtocolException("Too many follow-up requests: " + i2);
                    }
                } catch (IOException e2) {
                    tr6.f();
                    throw e2;
                }
            } catch (rr6 e3) {
                if (!a(e3.getLastConnectException(), tr6, false, t)) {
                    throw e3.getFirstConnectException();
                }
            } catch (IOException e4) {
                if (!a(e4, tr6, !(e4 instanceof gs6), t)) {
                    throw e4;
                }
            } catch (Throwable th) {
                tr6.a((IOException) null);
                tr6.f();
                throw th;
            }
        }
        tr6.f();
        throw new IOException("Canceled");
    }

    @DexIgnore
    public void a(Object obj) {
        this.c = obj;
    }

    @DexIgnore
    public final aq6 a(tq6 tq6) {
        fq6 fq6;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (tq6.h()) {
            SSLSocketFactory C = this.a.C();
            hostnameVerifier = this.a.m();
            sSLSocketFactory = C;
            fq6 = this.a.c();
        } else {
            sSLSocketFactory = null;
            hostnameVerifier = null;
            fq6 = null;
        }
        return new aq6(tq6.g(), tq6.k(), this.a.i(), this.a.B(), sSLSocketFactory, hostnameVerifier, fq6, this.a.x(), this.a.w(), this.a.r(), this.a.f(), this.a.y());
    }

    @DexIgnore
    public final boolean a(IOException iOException, tr6 tr6, boolean z, yq6 yq6) {
        tr6.a(iOException);
        if (!this.a.A()) {
            return false;
        }
        if (z) {
            yq6.a();
        }
        if (a(iOException, z) && tr6.d()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean a(IOException iOException, boolean z) {
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (iOException instanceof InterruptedIOException) {
            if (!(iOException instanceof SocketTimeoutException) || z) {
                return false;
            }
            return true;
        } else if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public final yq6 a(Response response, ar6 ar6) throws IOException {
        String e;
        tq6 b2;
        Proxy proxy;
        if (response != null) {
            int n = response.n();
            String e2 = response.I().e();
            RequestBody requestBody = null;
            if (n == 307 || n == 308) {
                if (!e2.equals("GET") && !e2.equals("HEAD")) {
                    return null;
                }
            } else if (n == 401) {
                return this.a.a().authenticate(ar6, response);
            } else {
                if (n != 503) {
                    if (n == 407) {
                        if (ar6 != null) {
                            proxy = ar6.b();
                        } else {
                            proxy = this.a.w();
                        }
                        if (proxy.type() == Proxy.Type.HTTP) {
                            return this.a.x().authenticate(ar6, response);
                        }
                        throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                    } else if (n != 408) {
                        switch (n) {
                            case 300:
                            case Action.Presenter.NEXT /*301*/:
                            case Action.Presenter.PREVIOUS /*302*/:
                            case Action.Presenter.BLACKOUT /*303*/:
                                break;
                            default:
                                return null;
                        }
                    } else if (!this.a.A()) {
                        return null;
                    } else {
                        response.I().a();
                        if ((response.F() == null || response.F().n() != 408) && a(response, 0) <= 0) {
                            return response.I();
                        }
                        return null;
                    }
                } else if ((response.F() == null || response.F().n() != 503) && a(response, Integer.MAX_VALUE) == 0) {
                    return response.I();
                } else {
                    return null;
                }
            }
            if (!this.a.k() || (e = response.e("Location")) == null || (b2 = response.I().g().b(e)) == null) {
                return null;
            }
            if (!b2.n().equals(response.I().g().n()) && !this.a.l()) {
                return null;
            }
            yq6.a f = response.I().f();
            if (zr6.b(e2)) {
                boolean d2 = zr6.d(e2);
                if (zr6.c(e2)) {
                    f.a("GET", (RequestBody) null);
                } else {
                    if (d2) {
                        requestBody = response.I().a();
                    }
                    f.a(e2, requestBody);
                }
                if (!d2) {
                    f.a("Transfer-Encoding");
                    f.a("Content-Length");
                    f.a("Content-Type");
                }
            }
            if (!a(response, b2)) {
                f.a("Authorization");
            }
            f.a(b2);
            return f.a();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final int a(Response response, int i) {
        String e = response.e("Retry-After");
        if (e == null) {
            return i;
        }
        if (e.matches("\\d+")) {
            return Integer.valueOf(e).intValue();
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public final boolean a(Response response, tq6 tq6) {
        tq6 g = response.I().g();
        return g.g().equals(tq6.g()) && g.k() == tq6.k() && g.n().equals(tq6.n());
    }
}
