package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ey<Z> implements cy<Z, Z> {
    @DexIgnore
    public static /* final */ ey<?> a; // = new ey<>();

    @DexIgnore
    public static <Z> cy<Z, Z> a() {
        return a;
    }

    @DexIgnore
    public rt<Z> a(rt<Z> rtVar, xr xrVar) {
        return rtVar;
    }
}
