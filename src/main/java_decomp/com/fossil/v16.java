package com.fossil;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v16 extends z16 {
    @DexIgnore
    public v16(Context context) {
        super(context);
    }

    @DexIgnore
    public final void a(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to InternalStorage");
            u16.a(Environment.getExternalStorageDirectory() + ZendeskConfig.SLASH + b26.c("6X8Y4XdM2Vhvn0I="));
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(Environment.getExternalStorageDirectory(), b26.c("6X8Y4XdM2Vhvn0KfzcEatGnWaNU="))));
                bufferedWriter.write(b26.c("4kU71lN96TJUomD1vOU9lgj9Tw==") + "," + str);
                bufferedWriter.write("\n");
                bufferedWriter.close();
            } catch (Exception e) {
                Log.w("MID", e);
            }
        }
    }

    @DexIgnore
    public final boolean a() {
        return b26.a(this.a, "android.permission.WRITE_EXTERNAL_STORAGE") && Environment.getExternalStorageState().equals("mounted");
    }

    @DexIgnore
    public final String b() {
        String str;
        synchronized (this) {
            Log.i("MID", "read mid from InternalStorage");
            try {
                Iterator<String> it = u16.a(new File(Environment.getExternalStorageDirectory(), b26.c("6X8Y4XdM2Vhvn0KfzcEatGnWaNU="))).iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    String[] split = it.next().split(",");
                    if (split.length == 2 && split[0].equals(b26.c("4kU71lN96TJUomD1vOU9lgj9Tw=="))) {
                        Log.i("MID", "read mid from InternalStorage:" + split[1]);
                        str = split[1];
                        break;
                    }
                }
            } catch (IOException e) {
                Log.w("MID", e);
            }
            str = null;
        }
        return str;
    }
}
