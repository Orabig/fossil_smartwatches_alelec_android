package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface l43 extends IInterface {
    @DexIgnore
    List<la3> a(ra3 ra3, boolean z) throws RemoteException;

    @DexIgnore
    List<ab3> a(String str, String str2, ra3 ra3) throws RemoteException;

    @DexIgnore
    List<ab3> a(String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    List<la3> a(String str, String str2, String str3, boolean z) throws RemoteException;

    @DexIgnore
    List<la3> a(String str, String str2, boolean z, ra3 ra3) throws RemoteException;

    @DexIgnore
    void a(long j, String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    void a(ab3 ab3) throws RemoteException;

    @DexIgnore
    void a(ab3 ab3, ra3 ra3) throws RemoteException;

    @DexIgnore
    void a(j03 j03, ra3 ra3) throws RemoteException;

    @DexIgnore
    void a(j03 j03, String str, String str2) throws RemoteException;

    @DexIgnore
    void a(la3 la3, ra3 ra3) throws RemoteException;

    @DexIgnore
    void a(ra3 ra3) throws RemoteException;

    @DexIgnore
    byte[] a(j03 j03, String str) throws RemoteException;

    @DexIgnore
    void b(ra3 ra3) throws RemoteException;

    @DexIgnore
    String c(ra3 ra3) throws RemoteException;

    @DexIgnore
    void d(ra3 ra3) throws RemoteException;
}
