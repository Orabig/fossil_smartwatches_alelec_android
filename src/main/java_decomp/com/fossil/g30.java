package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g30 {
    @DexIgnore
    public static /* final */ IntentFilter f; // = new IntentFilter("android.intent.action.BATTERY_CHANGED");
    @DexIgnore
    public static /* final */ IntentFilter g; // = new IntentFilter("android.intent.action.ACTION_POWER_CONNECTED");
    @DexIgnore
    public static /* final */ IntentFilter h; // = new IntentFilter("android.intent.action.ACTION_POWER_DISCONNECTED");
    @DexIgnore
    public /* final */ AtomicBoolean a; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ BroadcastReceiver c; // = new b();
    @DexIgnore
    public /* final */ BroadcastReceiver d; // = new a();
    @DexIgnore
    public boolean e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            boolean unused = g30.this.e = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends BroadcastReceiver {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            boolean unused = g30.this.e = false;
        }
    }

    @DexIgnore
    public g30(Context context) {
        this.b = context;
    }

    @DexIgnore
    public void b() {
        boolean z = true;
        if (!this.a.getAndSet(true)) {
            Intent registerReceiver = this.b.registerReceiver((BroadcastReceiver) null, f);
            int i = -1;
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("status", -1);
            }
            if (!(i == 2 || i == 5)) {
                z = false;
            }
            this.e = z;
            this.b.registerReceiver(this.d, g);
            this.b.registerReceiver(this.c, h);
        }
    }

    @DexIgnore
    public boolean c() {
        return this.e;
    }

    @DexIgnore
    public void a() {
        if (this.a.getAndSet(false)) {
            this.b.unregisterReceiver(this.d);
            this.b.unregisterReceiver(this.c);
        }
    }
}
