package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xf4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ vf4 r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;

    @DexIgnore
    public xf4(Object obj, View view, int i, ConstraintLayout constraintLayout, vf4 vf4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = vf4;
        a(this.r);
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
    }

    @DexIgnore
    public static xf4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, kb.a());
    }

    @DexIgnore
    @Deprecated
    public static xf4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return ViewDataBinding.a(layoutInflater, 2131558644, viewGroup, z, obj);
    }
}
