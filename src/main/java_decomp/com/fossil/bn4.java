package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;
import java.io.FileNotFoundException;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn4 implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static bn4 n;
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public MediaPlayer a;
    @DexIgnore
    public AudioManager b;
    @DexIgnore
    public volatile boolean c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public long i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public AudioAttributes k;
    @DexIgnore
    public /* final */ Runnable l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(bn4 bn4) {
            bn4.n = bn4;
        }

        @DexIgnore
        public final bn4 b() {
            return bn4.n;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final synchronized bn4 a() {
            bn4 b;
            if (bn4.o.b() == null) {
                bn4.o.a(new bn4((qg6) null));
            }
            b = bn4.o.b();
            if (b == null) {
                wg6.a();
                throw null;
            }
            return b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public /* final */ Ringtone a;
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ /* synthetic */ bn4 c;

        @DexIgnore
        public b(bn4 bn4, Ringtone ringtone, long j) {
            wg6.b(ringtone, "mRingtone");
            this.c = bn4;
            this.a = ringtone;
            this.b = j;
        }

        @DexIgnore
        public void run() {
            this.c.a(this.a, Integer.MAX_VALUE, this.b, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements Runnable {
        @DexIgnore
        public /* final */ Ringtone a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ bn4 c;

        @DexIgnore
        public c(bn4 bn4, Ringtone ringtone, int i) {
            wg6.b(ringtone, "mRingtone");
            this.c = bn4;
            this.a = ringtone;
            this.b = i;
        }

        @DexIgnore
        public void run() {
            this.c.a(this.a, this.b, 0, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ bn4 a;

        @DexIgnore
        public d(bn4 bn4) {
            this.a = bn4;
        }

        @DexIgnore
        public final void run() {
            if (this.a.a != null) {
                MediaPlayer c = this.a.a;
                if (c != null) {
                    c.stop();
                    if (this.a.d) {
                        this.a.a();
                        this.a.b.setStreamVolume(4, this.a.f, 1);
                    }
                    FLogger.INSTANCE.getLocal().d(bn4.m, "On play ringtone complete");
                    this.a.c = false;
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = bn4.class.getSimpleName();
        wg6.a((Object) simpleName, "SoundManager::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public bn4() {
        this.i = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        this.j = new Handler(PortfolioApp.get.instance().getMainLooper());
        this.a = new MediaPlayer();
        Object systemService = PortfolioApp.get.instance().getApplicationContext().getSystemService("audio");
        if (systemService != null) {
            this.b = (AudioManager) systemService;
            this.e = this.b.getStreamMaxVolume(4);
            AudioAttributes build = new AudioAttributes.Builder().setUsage(4).build();
            wg6.a((Object) build, "AudioAttributes.Builder(\u2026utes.USAGE_ALARM).build()");
            this.k = build;
            this.l = new d(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type android.media.AudioManager");
    }

    @DexIgnore
    public void onCompletion(MediaPlayer mediaPlayer) {
        wg6.b(mediaPlayer, "mp");
        if (this.g > 0) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, "current loop is" + this.g);
            this.g = this.g + -1;
            MediaPlayer mediaPlayer2 = this.a;
            if (mediaPlayer2 != null) {
                mediaPlayer2.seekTo(0);
                MediaPlayer mediaPlayer3 = this.a;
                if (mediaPlayer3 != null) {
                    mediaPlayer3.start();
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            MediaPlayer mediaPlayer4 = this.a;
            if (mediaPlayer4 != null) {
                mediaPlayer4.stop();
                if (this.d) {
                    a();
                    this.b.setStreamVolume(4, this.f, 1);
                }
                FLogger.INSTANCE.getLocal().d(m, "On play ringtone complete");
                this.c = false;
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void onPrepared(MediaPlayer mediaPlayer) {
        wg6.b(mediaPlayer, "mp");
        this.f = this.b.getStreamVolume(4);
        if (this.d) {
            this.b.setStreamVolume(4, this.e, 1);
        }
        if (!this.h) {
            this.j.postDelayed(this.l, (long) (mediaPlayer.getDuration() * this.g));
        } else {
            long j2 = this.i;
            if (j2 < ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                this.j.postDelayed(this.l, j2);
            }
            MediaPlayer mediaPlayer2 = this.a;
            if (mediaPlayer2 != null) {
                mediaPlayer2.setLooping(true);
            } else {
                wg6.a();
                throw null;
            }
        }
        MediaPlayer mediaPlayer3 = this.a;
        if (mediaPlayer3 != null) {
            mediaPlayer3.start();
            this.c = true;
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void b(Ringtone ringtone) {
        wg6.b(ringtone, Constants.RINGTONE);
        a(ringtone, 2);
    }

    @DexIgnore
    public final void a(Ringtone ringtone, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "Inside " + m + ".playRingtoneInfinitive - ringtone=" + ringtone);
        if (ringtone != null) {
            this.d = true;
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            if (!this.c) {
                FLogger.INSTANCE.getLocal().d(m, "On button play ringtone event");
                new Thread(new b(this, ringtone, j2)).start();
            }
        }
    }

    @DexIgnore
    public final void b() {
        MediaPlayer mediaPlayer = this.a;
        if (mediaPlayer != null) {
            if (mediaPlayer == null) {
                wg6.a();
                throw null;
            } else if (mediaPlayer.isPlaying()) {
                FLogger.INSTANCE.getLocal().d(m, "On stop playing ringtone");
                MediaPlayer mediaPlayer2 = this.a;
                if (mediaPlayer2 != null) {
                    mediaPlayer2.stop();
                    this.j.removeCallbacks(this.l);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        if (this.d) {
            this.b.setStreamVolume(4, this.f, 1);
            this.d = false;
        }
        this.c = false;
    }

    @DexIgnore
    public /* synthetic */ bn4(qg6 qg6) {
        this();
    }

    @DexIgnore
    public final void a(Ringtone ringtone, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "Inside " + m + ".playRingtone - ringtone=" + ringtone);
        if (ringtone != null) {
            this.d = true;
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            if (this.c) {
                FLogger.INSTANCE.getLocal().d(m, "On button stop ringtone event");
                a();
                return;
            }
            FLogger.INSTANCE.getLocal().d(m, "On button play ringtone event");
            new Thread(new c(this, ringtone, i2)).start();
        }
    }

    @DexIgnore
    public final void a(Ringtone ringtone) {
        if (ringtone != null) {
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            b();
            new Thread(new c(this, ringtone, 1)).start();
        }
    }

    @DexIgnore
    public final synchronized void a() {
        FLogger.INSTANCE.getLocal().d(m, "On release media event");
        if (this.c) {
            b();
        }
        if (!this.d) {
            if (this.a != null) {
                MediaPlayer mediaPlayer = this.a;
                if (mediaPlayer != null) {
                    mediaPlayer.reset();
                    MediaPlayer mediaPlayer2 = this.a;
                    if (mediaPlayer2 != null) {
                        mediaPlayer2.release();
                        this.a = null;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.d = false;
            this.c = false;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v16, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void a(Ringtone ringtone, int i2, long j2, boolean z) {
        Ringtone ringtone2;
        wg6.b(ringtone, Constants.RINGTONE);
        try {
            MediaPlayer mediaPlayer = this.a;
            if (mediaPlayer != null) {
                mediaPlayer.setOnPreparedListener(this);
                MediaPlayer mediaPlayer2 = this.a;
                if (mediaPlayer2 != null) {
                    mediaPlayer2.setOnCompletionListener(this);
                    if (i2 >= Integer.MAX_VALUE) {
                        this.h = true;
                        this.g = 0;
                        this.i = j2;
                    } else {
                        this.h = false;
                        this.i = 0;
                        this.g = i2;
                    }
                    MediaPlayer mediaPlayer3 = this.a;
                    if (mediaPlayer3 != null) {
                        mediaPlayer3.reset();
                        if (TextUtils.isEmpty(ringtone.getRingtoneId())) {
                            AssetFileDescriptor openFd = PortfolioApp.get.instance().getAssets().openFd("ringtones/" + ringtone.getRingtoneName() + "." + Constants.MP3_EXTENSION);
                            wg6.a((Object) openFd, "PortfolioApp.instance.as\u2026me + \".\" + MP3_EXTENSION)");
                            MediaPlayer mediaPlayer4 = this.a;
                            if (mediaPlayer4 != null) {
                                mediaPlayer4.setDataSource(openFd.getFileDescriptor(), openFd.getStartOffset(), openFd.getLength());
                                openFd.close();
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            Iterator<T> it = tj4.f.d().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    ringtone2 = null;
                                    break;
                                }
                                ringtone2 = it.next();
                                if (wg6.a((Object) ringtone2.getRingtoneName(), (Object) ringtone.getRingtoneName())) {
                                    break;
                                }
                            }
                            if (((Ringtone) ringtone2) != null) {
                                try {
                                    MediaPlayer mediaPlayer5 = this.a;
                                    if (mediaPlayer5 != null) {
                                        mediaPlayer5.setDataSource(PortfolioApp.get.instance(), Uri.parse(MediaStore.Audio.Media.INTERNAL_CONTENT_URI + '/' + ringtone.getRingtoneId()));
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } catch (Exception e2) {
                                    FLogger.INSTANCE.getLocal().d(m, "playRingtoneFromAsset exeption=" + e2);
                                    MediaPlayer mediaPlayer6 = this.a;
                                    if (mediaPlayer6 != null) {
                                        mediaPlayer6.setDataSource(PortfolioApp.get.instance(), Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + '/' + ringtone.getRingtoneId()));
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                            }
                        }
                        MediaPlayer mediaPlayer7 = this.a;
                        if (mediaPlayer7 != null) {
                            mediaPlayer7.setAudioAttributes(this.k);
                            MediaPlayer mediaPlayer8 = this.a;
                            if (mediaPlayer8 != null) {
                                mediaPlayer8.prepare();
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } catch (FileNotFoundException e3) {
            FLogger.INSTANCE.getLocal().e(m, "Error Inside " + m + ".playRingtoneFromAsset - Cant find ringtone, play default instead, ex=" + e3);
            Ringtone ringtone3 = new Ringtone(Constants.RINGTONE_DEFAULT, "");
            if (!z) {
                a(ringtone3, i2, j2, true);
                return;
            }
            FLogger.INSTANCE.getLocal().e(m, "Error Inside " + m + ".playRingtoneFromAsset - Cant find ringtone, play default instead");
        } catch (Exception e4) {
            FLogger.INSTANCE.getLocal().e(m, "Error when playing ringtone " + e4);
        }
    }
}
