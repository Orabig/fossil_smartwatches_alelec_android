package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface n15 extends zq4<m15> {
    @DexIgnore
    void a(int i, ArrayList<wx4> arrayList);

    @DexIgnore
    void b(int i, ArrayList<String> arrayList);

    @DexIgnore
    void c(int i, ArrayList<wx4> arrayList);

    @DexIgnore
    void close();

    @DexIgnore
    void d();

    @DexIgnore
    void e();

    @DexIgnore
    void e(List<Object> list);

    @DexIgnore
    void i(int i);

    @DexIgnore
    void j();

    @DexIgnore
    void p(boolean z);
}
