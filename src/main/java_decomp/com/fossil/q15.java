package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q15 implements Factory<n15> {
    @DexIgnore
    public static n15 a(o15 o15) {
        n15 c = o15.c();
        z76.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
