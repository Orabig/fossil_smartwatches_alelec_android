package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class jk2 implements hk2 {
    @DexIgnore
    public /* final */ kk2 a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public jk2(kk2 kk2, String str) {
        this.a = kk2;
        this.b = str;
    }

    @DexIgnore
    public final Object zza() {
        return this.a.a(this.b);
    }
}
