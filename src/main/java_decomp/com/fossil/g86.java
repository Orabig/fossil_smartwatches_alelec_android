package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g86 extends RuntimeException {
    @DexIgnore
    public g86(String str) {
        super(str);
    }

    @DexIgnore
    public g86(String str, Throwable th) {
        super(str, th);
    }
}
