package com.fossil;

import java.util.Comparator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws0<T> extends CopyOnWriteArrayList<T> {
    @DexIgnore
    public /* final */ Comparator<T> a;

    @DexIgnore
    public ws0(Comparator<T> comparator) {
        this.a = comparator;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        return r1;
     */
    @DexIgnore
    public final int a(T t) {
        synchronized (this) {
            int size = super.size();
            int i = 0;
            while (i < size) {
                Object a2 = yd6.a(this, i);
                if (a2 != null && this.a.compare(t, a2) <= 0) {
                    i++;
                }
            }
            int size2 = super.size();
            return size2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(4:4|5|6|7) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x000f */
    public final boolean b(T t) {
        synchronized (this) {
            if (indexOf(t) < 0) {
                add(a(t), t);
                add(t);
            }
        }
        return true;
    }

    @DexIgnore
    public final T remove(int i) {
        return super.remove(i);
    }

    @DexIgnore
    public final int size() {
        return super.size();
    }
}
