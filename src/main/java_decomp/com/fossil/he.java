package com.fossil;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.text.TextUtils;
import android.util.Log;
import com.fossil.ie;
import com.fossil.je;
import com.fossil.ke;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class he extends Service {
    @DexIgnore
    public static /* final */ boolean f; // = Log.isLoggable("MBServiceCompat", 3);
    @DexIgnore
    public g a;
    @DexIgnore
    public /* final */ p4<IBinder, f> b; // = new p4<>();
    @DexIgnore
    public f c;
    @DexIgnore
    public /* final */ q d; // = new q();
    @DexIgnore
    public MediaSessionCompat.Token e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends m<List<MediaBrowserCompat.MediaItem>> {
        @DexIgnore
        public /* final */ /* synthetic */ f f;
        @DexIgnore
        public /* final */ /* synthetic */ String g;
        @DexIgnore
        public /* final */ /* synthetic */ Bundle h;
        @DexIgnore
        public /* final */ /* synthetic */ Bundle i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Object obj, f fVar, String str, Bundle bundle, Bundle bundle2) {
            super(obj);
            this.f = fVar;
            this.g = str;
            this.h = bundle;
            this.i = bundle2;
        }

        @DexIgnore
        public void a(List<MediaBrowserCompat.MediaItem> list) {
            if (he.this.b.get(this.f.b.asBinder()) == this.f) {
                if ((a() & 1) != 0) {
                    list = he.this.a(list, this.h);
                }
                try {
                    this.f.b.a(this.g, list, this.h, this.i);
                } catch (RemoteException unused) {
                    Log.w("MBServiceCompat", "Calling onLoadChildren() failed for id=" + this.g + " package=" + this.f.a);
                }
            } else if (he.f) {
                Log.d("MBServiceCompat", "Not sending onLoadChildren result for connection that has been disconnected. pkg=" + this.f.a + " id=" + this.g);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends m<MediaBrowserCompat.MediaItem> {
        @DexIgnore
        public /* final */ /* synthetic */ w f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(he heVar, Object obj, w wVar) {
            super(obj);
            this.f = wVar;
        }

        @DexIgnore
        public void a(MediaBrowserCompat.MediaItem mediaItem) {
            if ((a() & 2) != 0) {
                this.f.b(-1, (Bundle) null);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("media_item", mediaItem);
            this.f.b(0, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends m<List<MediaBrowserCompat.MediaItem>> {
        @DexIgnore
        public /* final */ /* synthetic */ w f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(he heVar, Object obj, w wVar) {
            super(obj);
            this.f = wVar;
        }

        @DexIgnore
        public void a(List<MediaBrowserCompat.MediaItem> list) {
            if ((a() & 4) != 0 || list == null) {
                this.f.b(-1, (Bundle) null);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putParcelableArray("search_results", (Parcelable[]) list.toArray(new MediaBrowserCompat.MediaItem[0]));
            this.f.b(0, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends m<Bundle> {
        @DexIgnore
        public /* final */ /* synthetic */ w f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(he heVar, Object obj, w wVar) {
            super(obj);
            this.f = wVar;
        }

        @DexIgnore
        /* renamed from: c */
        public void a(Bundle bundle) {
            this.f.b(0, bundle);
        }

        @DexIgnore
        public void a(Bundle bundle) {
            this.f.b(-1, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public Bundle a() {
            throw null;
        }

        @DexIgnore
        public String b() {
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements IBinder.DeathRecipient {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ o b;
        @DexIgnore
        public /* final */ HashMap<String, List<u8<IBinder, Bundle>>> c; // = new HashMap<>();
        @DexIgnore
        public e d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void run() {
                f fVar = f.this;
                he.this.b.remove(fVar.b.asBinder());
            }
        }

        @DexIgnore
        public f(String str, int i, int i2, Bundle bundle, o oVar) {
            this.a = str;
            new le(str, i, i2);
            this.b = oVar;
        }

        @DexIgnore
        public void binderDied() {
            he.this.d.post(new a());
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        IBinder a(Intent intent);

        @DexIgnore
        void e();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements g, ie.d {
        @DexIgnore
        public /* final */ List<Bundle> a; // = new ArrayList();
        @DexIgnore
        public Object b;
        @DexIgnore
        public Messenger c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends m<List<MediaBrowserCompat.MediaItem>> {
            @DexIgnore
            public /* final */ /* synthetic */ ie.c f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, Object obj, ie.c cVar) {
                super(obj);
                this.f = cVar;
            }

            @DexIgnore
            public void a(List<MediaBrowserCompat.MediaItem> list) {
                ArrayList arrayList;
                if (list != null) {
                    arrayList = new ArrayList();
                    for (MediaBrowserCompat.MediaItem writeToParcel : list) {
                        Parcel obtain = Parcel.obtain();
                        writeToParcel.writeToParcel(obtain, 0);
                        arrayList.add(obtain);
                    }
                } else {
                    arrayList = null;
                }
                this.f.a(arrayList);
            }
        }

        @DexIgnore
        public h() {
        }

        @DexIgnore
        public IBinder a(Intent intent) {
            return ie.a(this.b, intent);
        }

        @DexIgnore
        public void b(String str, ie.c<List<Parcel>> cVar) {
            he.this.a(str, (m<List<MediaBrowserCompat.MediaItem>>) new a(this, str, cVar));
        }

        @DexIgnore
        public void e() {
            this.b = ie.a((Context) he.this, (ie.d) this);
            ie.a(this.b);
        }

        @DexIgnore
        public ie.a a(String str, int i, Bundle bundle) {
            Bundle bundle2;
            IBinder iBinder;
            if (bundle == null || bundle.getInt("extra_client_version", 0) == 0) {
                bundle2 = null;
            } else {
                bundle.remove("extra_client_version");
                this.c = new Messenger(he.this.d);
                bundle2 = new Bundle();
                bundle2.putInt("extra_service_version", 2);
                l6.a(bundle2, "extra_messenger", this.c.getBinder());
                MediaSessionCompat.Token token = he.this.e;
                if (token != null) {
                    q a2 = token.a();
                    if (a2 == null) {
                        iBinder = null;
                    } else {
                        iBinder = a2.asBinder();
                    }
                    l6.a(bundle2, "extra_session_binder", iBinder);
                } else {
                    this.a.add(bundle2);
                }
            }
            he heVar = he.this;
            heVar.c = new f(str, -1, i, bundle, (o) null);
            e a3 = he.this.a(str, i, bundle);
            he.this.c = null;
            if (a3 == null) {
                return null;
            }
            if (bundle2 == null) {
                a3.a();
                throw null;
            }
            a3.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends h implements je.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends m<MediaBrowserCompat.MediaItem> {
            @DexIgnore
            public /* final */ /* synthetic */ ie.c f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, Object obj, ie.c cVar) {
                super(obj);
                this.f = cVar;
            }

            @DexIgnore
            public void a(MediaBrowserCompat.MediaItem mediaItem) {
                if (mediaItem == null) {
                    this.f.a(null);
                    return;
                }
                Parcel obtain = Parcel.obtain();
                mediaItem.writeToParcel(obtain, 0);
                this.f.a(obtain);
            }
        }

        @DexIgnore
        public i() {
            super();
        }

        @DexIgnore
        public void a(String str, ie.c<Parcel> cVar) {
            he.this.b(str, new a(this, str, cVar));
        }

        @DexIgnore
        public void e() {
            this.b = je.a(he.this, this);
            ie.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends i implements ke.c {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends m<List<MediaBrowserCompat.MediaItem>> {
            @DexIgnore
            public /* final */ /* synthetic */ ke.b f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(j jVar, Object obj, ke.b bVar) {
                super(obj);
                this.f = bVar;
            }

            @DexIgnore
            public void a(List<MediaBrowserCompat.MediaItem> list) {
                ArrayList arrayList;
                if (list != null) {
                    arrayList = new ArrayList();
                    for (MediaBrowserCompat.MediaItem writeToParcel : list) {
                        Parcel obtain = Parcel.obtain();
                        writeToParcel.writeToParcel(obtain, 0);
                        arrayList.add(obtain);
                    }
                } else {
                    arrayList = null;
                }
                this.f.a(arrayList, a());
            }
        }

        @DexIgnore
        public j() {
            super();
        }

        @DexIgnore
        public void a(String str, ke.b bVar, Bundle bundle) {
            he.this.a(str, (m<List<MediaBrowserCompat.MediaItem>>) new a(this, str, bVar), bundle);
        }

        @DexIgnore
        public void e() {
            this.b = ke.a(he.this, this);
            ie.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends j {
        @DexIgnore
        public k(he heVar) {
            super();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l implements g {
        @DexIgnore
        public Messenger a;

        @DexIgnore
        public l() {
        }

        @DexIgnore
        public IBinder a(Intent intent) {
            if ("android.media.browse.MediaBrowserService".equals(intent.getAction())) {
                return this.a.getBinder();
            }
            return null;
        }

        @DexIgnore
        public void e() {
            this.a = new Messenger(he.this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class m<T> {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public int e;

        @DexIgnore
        public m(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public void a(int i) {
            this.e = i;
        }

        @DexIgnore
        public void a(T t) {
            throw null;
        }

        @DexIgnore
        public void b(T t) {
            if (this.c || this.d) {
                throw new IllegalStateException("sendResult() called when either sendResult() or sendError() had already been called for: " + this.a);
            }
            this.c = true;
            a(t);
        }

        @DexIgnore
        public int a() {
            return this.e;
        }

        @DexIgnore
        public void a(Bundle bundle) {
            throw new UnsupportedOperationException("It is not supported to send an error for " + this.a);
        }

        @DexIgnore
        public void b(Bundle bundle) {
            if (this.c || this.d) {
                throw new IllegalStateException("sendError() called when either sendResult() or sendError() had already been called for: " + this.a);
            }
            this.d = true;
            a(bundle);
        }

        @DexIgnore
        public boolean b() {
            return this.b || this.c || this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;
            @DexIgnore
            public /* final */ /* synthetic */ int c;
            @DexIgnore
            public /* final */ /* synthetic */ int d;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle e;

            @DexIgnore
            public a(o oVar, String str, int i, int i2, Bundle bundle) {
                this.a = oVar;
                this.b = str;
                this.c = i;
                this.d = i2;
                this.e = bundle;
            }

            @DexIgnore
            public void run() {
                IBinder asBinder = this.a.asBinder();
                he.this.b.remove(asBinder);
                f fVar = new f(this.b, this.c, this.d, this.e, this.a);
                he heVar = he.this;
                heVar.c = fVar;
                fVar.d = heVar.a(this.b, this.d, this.e);
                he heVar2 = he.this;
                heVar2.c = null;
                if (fVar.d == null) {
                    Log.i("MBServiceCompat", "No root for client " + this.b + " from service " + a.class.getName());
                    try {
                        this.a.a();
                    } catch (RemoteException unused) {
                        Log.w("MBServiceCompat", "Calling onConnectFailed() failed. Ignoring. pkg=" + this.b);
                    }
                } else {
                    try {
                        heVar2.b.put(asBinder, fVar);
                        asBinder.linkToDeath(fVar, 0);
                        if (he.this.e != null) {
                            fVar.d.b();
                            throw null;
                        }
                    } catch (RemoteException unused2) {
                        Log.w("MBServiceCompat", "Calling onConnect() failed. Dropping client. pkg=" + this.b);
                        he.this.b.remove(asBinder);
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o a;

            @DexIgnore
            public b(o oVar) {
                this.a = oVar;
            }

            @DexIgnore
            public void run() {
                f remove = he.this.b.remove(this.a.asBinder());
                if (remove != null) {
                    remove.b.asBinder().unlinkToDeath(remove, 0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class c implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;
            @DexIgnore
            public /* final */ /* synthetic */ IBinder c;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle d;

            @DexIgnore
            public c(o oVar, String str, IBinder iBinder, Bundle bundle) {
                this.a = oVar;
                this.b = str;
                this.c = iBinder;
                this.d = bundle;
            }

            @DexIgnore
            public void run() {
                f fVar = he.this.b.get(this.a.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "addSubscription for callback that isn't registered id=" + this.b);
                    return;
                }
                he.this.a(this.b, fVar, this.c, this.d);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class d implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;
            @DexIgnore
            public /* final */ /* synthetic */ IBinder c;

            @DexIgnore
            public d(o oVar, String str, IBinder iBinder) {
                this.a = oVar;
                this.b = str;
                this.c = iBinder;
            }

            @DexIgnore
            public void run() {
                f fVar = he.this.b.get(this.a.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "removeSubscription for callback that isn't registered id=" + this.b);
                } else if (!he.this.a(this.b, fVar, this.c)) {
                    Log.w("MBServiceCompat", "removeSubscription called for " + this.b + " which is not subscribed");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class e implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;
            @DexIgnore
            public /* final */ /* synthetic */ w c;

            @DexIgnore
            public e(o oVar, String str, w wVar) {
                this.a = oVar;
                this.b = str;
                this.c = wVar;
            }

            @DexIgnore
            public void run() {
                f fVar = he.this.b.get(this.a.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "getMediaItem for callback that isn't registered id=" + this.b);
                    return;
                }
                he.this.a(this.b, fVar, this.c);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class f implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;
            @DexIgnore
            public /* final */ /* synthetic */ int c;
            @DexIgnore
            public /* final */ /* synthetic */ int d;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle e;

            @DexIgnore
            public f(o oVar, String str, int i, int i2, Bundle bundle) {
                this.a = oVar;
                this.b = str;
                this.c = i;
                this.d = i2;
                this.e = bundle;
            }

            @DexIgnore
            public void run() {
                IBinder asBinder = this.a.asBinder();
                he.this.b.remove(asBinder);
                f fVar = new f(this.b, this.c, this.d, this.e, this.a);
                he.this.b.put(asBinder, fVar);
                try {
                    asBinder.linkToDeath(fVar, 0);
                } catch (RemoteException unused) {
                    Log.w("MBServiceCompat", "IBinder is already dead.");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class g implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o a;

            @DexIgnore
            public g(o oVar) {
                this.a = oVar;
            }

            @DexIgnore
            public void run() {
                IBinder asBinder = this.a.asBinder();
                f remove = he.this.b.remove(asBinder);
                if (remove != null) {
                    asBinder.unlinkToDeath(remove, 0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class h implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle c;
            @DexIgnore
            public /* final */ /* synthetic */ w d;

            @DexIgnore
            public h(o oVar, String str, Bundle bundle, w wVar) {
                this.a = oVar;
                this.b = str;
                this.c = bundle;
                this.d = wVar;
            }

            @DexIgnore
            public void run() {
                f fVar = he.this.b.get(this.a.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "search for callback that isn't registered query=" + this.b);
                    return;
                }
                he.this.b(this.b, this.c, fVar, this.d);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class i implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ o a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;
            @DexIgnore
            public /* final */ /* synthetic */ Bundle c;
            @DexIgnore
            public /* final */ /* synthetic */ w d;

            @DexIgnore
            public i(o oVar, String str, Bundle bundle, w wVar) {
                this.a = oVar;
                this.b = str;
                this.c = bundle;
                this.d = wVar;
            }

            @DexIgnore
            public void run() {
                f fVar = he.this.b.get(this.a.asBinder());
                if (fVar == null) {
                    Log.w("MBServiceCompat", "sendCustomAction for callback that isn't registered action=" + this.b + ", extras=" + this.c);
                    return;
                }
                he.this.a(this.b, this.c, fVar, this.d);
            }
        }

        @DexIgnore
        public n() {
        }

        @DexIgnore
        public void a(String str, int i2, int i3, Bundle bundle, o oVar) {
            if (he.this.a(str, i3)) {
                he.this.d.a(new a(oVar, str, i2, i3, bundle));
                return;
            }
            throw new IllegalArgumentException("Package/uid mismatch: uid=" + i3 + " package=" + str);
        }

        @DexIgnore
        public void b(o oVar) {
            he.this.d.a(new g(oVar));
        }

        @DexIgnore
        public void b(String str, Bundle bundle, w wVar, o oVar) {
            if (!TextUtils.isEmpty(str) && wVar != null) {
                he.this.d.a(new i(oVar, str, bundle, wVar));
            }
        }

        @DexIgnore
        public void a(o oVar) {
            he.this.d.a(new b(oVar));
        }

        @DexIgnore
        public void a(String str, IBinder iBinder, Bundle bundle, o oVar) {
            he.this.d.a(new c(oVar, str, iBinder, bundle));
        }

        @DexIgnore
        public void a(String str, IBinder iBinder, o oVar) {
            he.this.d.a(new d(oVar, str, iBinder));
        }

        @DexIgnore
        public void a(String str, w wVar, o oVar) {
            if (!TextUtils.isEmpty(str) && wVar != null) {
                he.this.d.a(new e(oVar, str, wVar));
            }
        }

        @DexIgnore
        public void a(o oVar, String str, int i2, int i3, Bundle bundle) {
            he.this.d.a(new f(oVar, str, i2, i3, bundle));
        }

        @DexIgnore
        public void a(String str, Bundle bundle, w wVar, o oVar) {
            if (!TextUtils.isEmpty(str) && wVar != null) {
                he.this.d.a(new h(oVar, str, bundle, wVar));
            }
        }
    }

    @DexIgnore
    public interface o {
        @DexIgnore
        void a() throws RemoteException;

        @DexIgnore
        void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle, Bundle bundle2) throws RemoteException;

        @DexIgnore
        IBinder asBinder();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class p implements o {
        @DexIgnore
        public /* final */ Messenger a;

        @DexIgnore
        public p(Messenger messenger) {
            this.a = messenger;
        }

        @DexIgnore
        public void a() throws RemoteException {
            a(2, (Bundle) null);
        }

        @DexIgnore
        public IBinder asBinder() {
            return this.a.getBinder();
        }

        @DexIgnore
        public void a(String str, List<MediaBrowserCompat.MediaItem> list, Bundle bundle, Bundle bundle2) throws RemoteException {
            Bundle bundle3 = new Bundle();
            bundle3.putString("data_media_item_id", str);
            bundle3.putBundle("data_options", bundle);
            bundle3.putBundle("data_notify_children_changed_options", bundle2);
            if (list != null) {
                bundle3.putParcelableArrayList("data_media_item_list", list instanceof ArrayList ? (ArrayList) list : new ArrayList(list));
            }
            a(3, bundle3);
        }

        @DexIgnore
        public final void a(int i, Bundle bundle) throws RemoteException {
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.arg1 = 2;
            obtain.setData(bundle);
            this.a.send(obtain);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q extends Handler {
        @DexIgnore
        public /* final */ n a; // = new n();

        @DexIgnore
        public q() {
        }

        @DexIgnore
        public void a(Runnable runnable) {
            if (Thread.currentThread() == getLooper().getThread()) {
                runnable.run();
            } else {
                post(runnable);
            }
        }

        @DexIgnore
        public void handleMessage(Message message) {
            Bundle data = message.getData();
            switch (message.what) {
                case 1:
                    Bundle bundle = data.getBundle("data_root_hints");
                    MediaSessionCompat.a(bundle);
                    this.a.a(data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle, (o) new p(message.replyTo));
                    return;
                case 2:
                    this.a.a(new p(message.replyTo));
                    return;
                case 3:
                    Bundle bundle2 = data.getBundle("data_options");
                    MediaSessionCompat.a(bundle2);
                    this.a.a(data.getString("data_media_item_id"), l6.a(data, "data_callback_token"), bundle2, (o) new p(message.replyTo));
                    return;
                case 4:
                    this.a.a(data.getString("data_media_item_id"), l6.a(data, "data_callback_token"), (o) new p(message.replyTo));
                    return;
                case 5:
                    this.a.a(data.getString("data_media_item_id"), (w) data.getParcelable("data_result_receiver"), (o) new p(message.replyTo));
                    return;
                case 6:
                    Bundle bundle3 = data.getBundle("data_root_hints");
                    MediaSessionCompat.a(bundle3);
                    n nVar = this.a;
                    p pVar = new p(message.replyTo);
                    nVar.a((o) pVar, data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle3);
                    return;
                case 7:
                    this.a.b(new p(message.replyTo));
                    return;
                case 8:
                    Bundle bundle4 = data.getBundle("data_search_extras");
                    MediaSessionCompat.a(bundle4);
                    this.a.a(data.getString("data_search_query"), bundle4, (w) data.getParcelable("data_result_receiver"), (o) new p(message.replyTo));
                    return;
                case 9:
                    Bundle bundle5 = data.getBundle("data_custom_action_extras");
                    MediaSessionCompat.a(bundle5);
                    this.a.b(data.getString("data_custom_action"), bundle5, (w) data.getParcelable("data_result_receiver"), new p(message.replyTo));
                    return;
                default:
                    Log.w("MBServiceCompat", "Unhandled message: " + message + "\n  Service version: " + 2 + "\n  Client version: " + message.arg1);
                    return;
            }
        }

        @DexIgnore
        public boolean sendMessageAtTime(Message message, long j) {
            Bundle data = message.getData();
            data.setClassLoader(MediaBrowserCompat.class.getClassLoader());
            data.putInt("data_calling_uid", Binder.getCallingUid());
            data.putInt("data_calling_pid", Binder.getCallingPid());
            return super.sendMessageAtTime(message, j);
        }
    }

    @DexIgnore
    public abstract e a(String str, int i2, Bundle bundle);

    @DexIgnore
    public void a(String str) {
    }

    @DexIgnore
    public void a(String str, Bundle bundle) {
    }

    @DexIgnore
    public abstract void a(String str, m<List<MediaBrowserCompat.MediaItem>> mVar);

    @DexIgnore
    public void a(String str, m<List<MediaBrowserCompat.MediaItem>> mVar, Bundle bundle) {
        mVar.a(1);
        a(str, mVar);
    }

    @DexIgnore
    public void b(String str, m<MediaBrowserCompat.MediaItem> mVar) {
        mVar.a(2);
        mVar.b(null);
    }

    @DexIgnore
    public void dump(FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.a.a(intent);
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 28) {
            this.a = new k(this);
        } else if (i2 >= 26) {
            this.a = new j();
        } else if (i2 >= 23) {
            this.a = new i();
        } else if (i2 >= 21) {
            this.a = new h();
        } else {
            this.a = new l();
        }
        this.a.e();
    }

    @DexIgnore
    public void a(String str, Bundle bundle, m<Bundle> mVar) {
        mVar.b((Bundle) null);
    }

    @DexIgnore
    public void b(String str, Bundle bundle, m<List<MediaBrowserCompat.MediaItem>> mVar) {
        mVar.a(4);
        mVar.b(null);
    }

    @DexIgnore
    public boolean a(String str, int i2) {
        if (str == null) {
            return false;
        }
        for (String equals : getPackageManager().getPackagesForUid(i2)) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void b(String str, Bundle bundle, f fVar, w wVar) {
        c cVar = new c(this, str, wVar);
        b(str, bundle, cVar);
        if (!cVar.b()) {
            throw new IllegalStateException("onSearch must call detach() or sendResult() before returning for query=" + str);
        }
    }

    @DexIgnore
    public void a(String str, f fVar, IBinder iBinder, Bundle bundle) {
        List<u8> list = fVar.c.get(str);
        if (list == null) {
            list = new ArrayList<>();
        }
        for (u8 u8Var : list) {
            if (iBinder == u8Var.a && ge.a(bundle, (Bundle) u8Var.b)) {
                return;
            }
        }
        list.add(new u8(iBinder, bundle));
        fVar.c.put(str, list);
        a(str, fVar, bundle, (Bundle) null);
        a(str, bundle);
    }

    @DexIgnore
    public boolean a(String str, f fVar, IBinder iBinder) {
        boolean z = true;
        boolean z2 = false;
        if (iBinder == null) {
            try {
                if (fVar.c.remove(str) == null) {
                    z = false;
                }
                return z;
            } finally {
                a(str);
            }
        } else {
            List list = fVar.c.get(str);
            if (list != null) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    if (iBinder == ((u8) it.next()).a) {
                        it.remove();
                        z2 = true;
                    }
                }
                if (list.size() == 0) {
                    fVar.c.remove(str);
                }
            }
            a(str);
            return z2;
        }
    }

    @DexIgnore
    public void a(String str, f fVar, Bundle bundle, Bundle bundle2) {
        a aVar = new a(str, fVar, str, bundle, bundle2);
        if (bundle == null) {
            a(str, (m<List<MediaBrowserCompat.MediaItem>>) aVar);
        } else {
            a(str, (m<List<MediaBrowserCompat.MediaItem>>) aVar, bundle);
        }
        if (!aVar.b()) {
            throw new IllegalStateException("onLoadChildren must call detach() or sendResult() before returning for package=" + fVar.a + " id=" + str);
        }
    }

    @DexIgnore
    public List<MediaBrowserCompat.MediaItem> a(List<MediaBrowserCompat.MediaItem> list, Bundle bundle) {
        if (list == null) {
            return null;
        }
        int i2 = bundle.getInt("android.media.browse.extra.PAGE", -1);
        int i3 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
        if (i2 == -1 && i3 == -1) {
            return list;
        }
        int i4 = i3 * i2;
        int i5 = i4 + i3;
        if (i2 < 0 || i3 < 1 || i4 >= list.size()) {
            return Collections.emptyList();
        }
        if (i5 > list.size()) {
            i5 = list.size();
        }
        return list.subList(i4, i5);
    }

    @DexIgnore
    public void a(String str, f fVar, w wVar) {
        b bVar = new b(this, str, wVar);
        b(str, bVar);
        if (!bVar.b()) {
            throw new IllegalStateException("onLoadItem must call detach() or sendResult() before returning for id=" + str);
        }
    }

    @DexIgnore
    public void a(String str, Bundle bundle, f fVar, w wVar) {
        d dVar = new d(this, str, wVar);
        a(str, bundle, (m<Bundle>) dVar);
        if (!dVar.b()) {
            throw new IllegalStateException("onCustomAction must call detach() or sendResult() or sendError() before returning for action=" + str + " extras=" + bundle);
        }
    }
}
