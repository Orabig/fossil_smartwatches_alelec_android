package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jj5 extends hj5 {
    @DexIgnore
    public String e; // = PortfolioApp.get.instance().e();
    @DexIgnore
    public /* final */ b f; // = new b(this);
    @DexIgnore
    public /* final */ ij5 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ jj5 a;

        @DexIgnore
        public b(jj5 jj5) {
            this.a = jj5;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            if (otaEvent != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeUpdateFirmwarePresenter", "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
                if (!TextUtils.isEmpty(otaEvent.getSerial()) && xj6.b(otaEvent.getSerial(), PortfolioApp.get.instance().e(), true)) {
                    this.a.i().h((int) (otaEvent.getProcess() * ((float) 10)));
                }
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public jj5(ij5 ij5) {
        wg6.b(ij5, "mView");
        this.g = ij5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void f() {
        Object instance = PortfolioApp.get.instance();
        b bVar = this.f;
        instance.registerReceiver(bVar, new IntentFilter(PortfolioApp.get.instance().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        h();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void g() {
        try {
            PortfolioApp.get.instance().unregisterReceiver(this.f);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeUpdateFirmwarePresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v24, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v27, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void h() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeUpdateFirmwarePresenter", "serial=" + this.e + ", mCurrentDeviceType=" + deviceBySerial);
        if (deviceBySerial == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            explore.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886684));
            explore.setBackground(2131231385);
            explore2.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886685));
            explore2.setBackground(2131231383);
            explore3.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886682));
            explore3.setBackground(2131231386);
            explore4.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886683));
            explore4.setBackground(2131231382);
        } else {
            explore.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886691));
            explore.setBackground(2131231385);
            explore2.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886690));
            explore2.setBackground(2131231387);
            explore3.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886688));
            explore3.setBackground(2131231386);
            explore4.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886689));
            explore4.setBackground(2131231384);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.g.h((List<? extends Explore>) arrayList);
    }

    @DexIgnore
    public final ij5 i() {
        return this.g;
    }

    @DexIgnore
    public void j() {
        this.g.a(this);
    }
}
