package com.fossil;

import com.fossil.dr;
import com.fossil.jv;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nv {
    @DexIgnore
    public static /* final */ c e; // = new c();
    @DexIgnore
    public static /* final */ jv<Object, Object> f; // = new a();
    @DexIgnore
    public /* final */ List<b<?, ?>> a;
    @DexIgnore
    public /* final */ c b;
    @DexIgnore
    public /* final */ Set<b<?, ?>> c;
    @DexIgnore
    public /* final */ v8<List<Throwable>> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements jv<Object, Object> {
        @DexIgnore
        public jv.a<Object> a(Object obj, int i, int i2, xr xrVar) {
            return null;
        }

        @DexIgnore
        public boolean a(Object obj) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model, Data> {
        @DexIgnore
        public /* final */ Class<Model> a;
        @DexIgnore
        public /* final */ Class<Data> b;
        @DexIgnore
        public /* final */ kv<? extends Model, ? extends Data> c;

        @DexIgnore
        public b(Class<Model> cls, Class<Data> cls2, kv<? extends Model, ? extends Data> kvVar) {
            this.a = cls;
            this.b = cls2;
            this.c = kvVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return a(cls) && this.b.isAssignableFrom(cls2);
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public <Model, Data> mv<Model, Data> a(List<jv<Model, Data>> list, v8<List<Throwable>> v8Var) {
            return new mv<>(list, v8Var);
        }
    }

    @DexIgnore
    public nv(v8<List<Throwable>> v8Var) {
        this(v8Var, e);
    }

    @DexIgnore
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, kv<? extends Model, ? extends Data> kvVar) {
        a(cls, cls2, kvVar, true);
    }

    @DexIgnore
    public synchronized List<Class<?>> b(Class<?> cls) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (b next : this.a) {
            if (!arrayList.contains(next.b) && next.a(cls)) {
                arrayList.add(next.b);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public nv(v8<List<Throwable>> v8Var, c cVar) {
        this.a = new ArrayList();
        this.c = new HashSet();
        this.d = v8Var;
        this.b = cVar;
    }

    @DexIgnore
    public final <Model, Data> void a(Class<Model> cls, Class<Data> cls2, kv<? extends Model, ? extends Data> kvVar, boolean z) {
        b bVar = new b(cls, cls2, kvVar);
        List<b<?, ?>> list = this.a;
        list.add(z ? list.size() : 0, bVar);
    }

    @DexIgnore
    public synchronized <Model> List<jv<Model, ?>> a(Class<Model> cls) {
        ArrayList arrayList;
        try {
            arrayList = new ArrayList();
            for (b next : this.a) {
                if (!this.c.contains(next)) {
                    if (next.a(cls)) {
                        this.c.add(next);
                        arrayList.add(a((b<?, ?>) next));
                        this.c.remove(next);
                    }
                }
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
        return arrayList;
    }

    @DexIgnore
    public synchronized <Model, Data> jv<Model, Data> a(Class<Model> cls, Class<Data> cls2) {
        try {
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (b next : this.a) {
                if (this.c.contains(next)) {
                    z = true;
                } else if (next.a(cls, cls2)) {
                    this.c.add(next);
                    arrayList.add(a((b<?, ?>) next));
                    this.c.remove(next);
                }
            }
            if (arrayList.size() > 1) {
                return this.b.a(arrayList, this.d);
            } else if (arrayList.size() == 1) {
                return (jv) arrayList.get(0);
            } else if (z) {
                return a();
            } else {
                throw new dr.c((Class<?>) cls, (Class<?>) cls2);
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
    }

    @DexIgnore
    public final <Model, Data> jv<Model, Data> a(b<?, ?> bVar) {
        jv<? extends Model, ? extends Data> a2 = bVar.c.a(this);
        q00.a(a2);
        return a2;
    }

    @DexIgnore
    public static <Model, Data> jv<Model, Data> a() {
        return f;
    }
}
