package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class se5 implements Factory<re5> {
    @DexIgnore
    public static GoalTrackingOverviewMonthPresenter a(qe5 qe5, UserRepository userRepository, an4 an4, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewMonthPresenter(qe5, userRepository, an4, goalTrackingRepository);
    }
}
