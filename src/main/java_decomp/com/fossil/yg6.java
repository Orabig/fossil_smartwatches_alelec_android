package com.fossil;

import com.fossil.ki6;
import com.fossil.mi6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yg6 extends zg6 implements ki6 {
    @DexIgnore
    public yg6() {
    }

    @DexIgnore
    public ei6 computeReflected() {
        kh6.a(this);
        return this;
    }

    @DexIgnore
    public Object getDelegate() {
        return ((ki6) getReflected()).getDelegate();
    }

    @DexIgnore
    public Object invoke() {
        return get();
    }

    @DexIgnore
    public yg6(Object obj) {
        super(obj);
    }

    @DexIgnore
    public mi6.a getGetter() {
        return ((ki6) getReflected()).getGetter();
    }

    @DexIgnore
    public ki6.a getSetter() {
        return ((ki6) getReflected()).getSetter();
    }
}
