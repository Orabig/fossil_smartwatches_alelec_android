package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class od extends Fragment {
    @DexIgnore
    public a a;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void d();

        @DexIgnore
        void e();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityDestroyed(Activity activity) {
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
        }

        @DexIgnore
        public void onActivityPostCreated(Activity activity, Bundle bundle) {
            od.a(activity, Lifecycle.a.ON_CREATE);
        }

        @DexIgnore
        public void onActivityPostResumed(Activity activity) {
            od.a(activity, Lifecycle.a.ON_RESUME);
        }

        @DexIgnore
        public void onActivityPostStarted(Activity activity) {
            od.a(activity, Lifecycle.a.ON_START);
        }

        @DexIgnore
        public void onActivityPreDestroyed(Activity activity) {
            od.a(activity, Lifecycle.a.ON_DESTROY);
        }

        @DexIgnore
        public void onActivityPrePaused(Activity activity) {
            od.a(activity, Lifecycle.a.ON_PAUSE);
        }

        @DexIgnore
        public void onActivityPreStopped(Activity activity) {
            od.a(activity, Lifecycle.a.ON_STOP);
        }

        @DexIgnore
        public void onActivityResumed(Activity activity) {
        }

        @DexIgnore
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityStarted(Activity activity) {
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public static void a(Activity activity, Lifecycle.a aVar) {
        if (activity instanceof gd) {
            ((gd) activity).getLifecycle().a(aVar);
        } else if (activity instanceof LifecycleOwner) {
            Lifecycle lifecycle = ((LifecycleOwner) activity).getLifecycle();
            if (lifecycle instanceof LifecycleRegistry) {
                ((LifecycleRegistry) lifecycle).a(aVar);
            }
        }
    }

    @DexIgnore
    public static void b(Activity activity) {
        if (Build.VERSION.SDK_INT >= 29) {
            activity.registerActivityLifecycleCallbacks(new b());
        }
        FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add(new od(), "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }

    @DexIgnore
    public final void c(a aVar) {
        if (aVar != null) {
            aVar.a();
        }
    }

    @DexIgnore
    public void d(a aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        a(this.a);
        a(Lifecycle.a.ON_CREATE);
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        a(Lifecycle.a.ON_DESTROY);
        this.a = null;
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        a(Lifecycle.a.ON_PAUSE);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        b(this.a);
        a(Lifecycle.a.ON_RESUME);
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        c(this.a);
        a(Lifecycle.a.ON_START);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        a(Lifecycle.a.ON_STOP);
    }

    @DexIgnore
    public static od a(Activity activity) {
        return (od) activity.getFragmentManager().findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag");
    }

    @DexIgnore
    public final void b(a aVar) {
        if (aVar != null) {
            aVar.d();
        }
    }

    @DexIgnore
    public final void a(a aVar) {
        if (aVar != null) {
            aVar.e();
        }
    }

    @DexIgnore
    public final void a(Lifecycle.a aVar) {
        if (Build.VERSION.SDK_INT < 29) {
            a(getActivity(), aVar);
        }
    }
}
