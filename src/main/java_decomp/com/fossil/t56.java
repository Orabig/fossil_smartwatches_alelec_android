package com.fossil;

import android.content.Context;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t56 {
    @DexIgnore
    public static volatile t56 c;
    @DexIgnore
    public Timer a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public t56(Context context) {
        this.b = context.getApplicationContext();
        this.a = new Timer(false);
    }

    @DexIgnore
    public static t56 a(Context context) {
        if (c == null) {
            synchronized (t56.class) {
                if (c == null) {
                    c = new t56(context);
                }
            }
        }
        return c;
    }

    @DexIgnore
    public void a() {
        if (n36.o() == o36.PERIOD) {
            long l = (long) (n36.l() * 60 * 1000);
            if (n36.q()) {
                b56 b2 = m56.b();
                b2.e("setupPeriodTimer delay:" + l);
            }
            a(new u56(this), l);
        }
    }

    @DexIgnore
    public void a(TimerTask timerTask, long j) {
        if (this.a != null) {
            if (n36.q()) {
                b56 b2 = m56.b();
                b2.e("setupPeriodTimer schedule delay:" + j);
            }
            this.a.schedule(timerTask, j);
        } else if (n36.q()) {
            m56.b().g("setupPeriodTimer schedule timer == null");
        }
    }
}
