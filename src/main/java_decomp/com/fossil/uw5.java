package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uw5 implements Factory<tw5> {
    @DexIgnore
    public /* final */ Provider<d15> a;
    @DexIgnore
    public /* final */ Provider<mz4> b;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> c;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> d;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> e;
    @DexIgnore
    public /* final */ Provider<an4> f;

    @DexIgnore
    public uw5(Provider<d15> provider, Provider<mz4> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<an4> provider6) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static uw5 a(Provider<d15> provider, Provider<mz4> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<an4> provider6) {
        return new uw5(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static tw5 b(Provider<d15> provider, Provider<mz4> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<an4> provider6) {
        return new SetNotificationUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get());
    }

    @DexIgnore
    public SetNotificationUseCase get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f);
    }
}
