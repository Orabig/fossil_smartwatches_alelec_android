package com.fossil;

import java.util.Arrays;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep2<E> extends sl2<E> implements RandomAccess {
    @DexIgnore
    public static /* final */ ep2<Object> d;
    @DexIgnore
    public E[] b;
    @DexIgnore
    public int c;

    /*
    static {
        ep2<Object> ep2 = new ep2<>(new Object[0], 0);
        d = ep2;
        ep2.k();
    }
    */

    @DexIgnore
    public ep2(E[] eArr, int i) {
        this.b = eArr;
        this.c = i;
    }

    @DexIgnore
    public static <E> ep2<E> b() {
        return d;
    }

    @DexIgnore
    public final boolean add(E e) {
        a();
        int i = this.c;
        E[] eArr = this.b;
        if (i == eArr.length) {
            this.b = Arrays.copyOf(eArr, ((i * 3) / 2) + 1);
        }
        E[] eArr2 = this.b;
        int i2 = this.c;
        this.c = i2 + 1;
        eArr2[i2] = e;
        this.modCount++;
        return true;
    }

    @DexIgnore
    public final E get(int i) {
        zzb(i);
        return this.b[i];
    }

    @DexIgnore
    public final E remove(int i) {
        a();
        zzb(i);
        E[] eArr = this.b;
        E e = eArr[i];
        int i2 = this.c;
        if (i < i2 - 1) {
            System.arraycopy(eArr, i + 1, eArr, i, (i2 - i) - 1);
        }
        this.c--;
        this.modCount++;
        return e;
    }

    @DexIgnore
    public final E set(int i, E e) {
        a();
        zzb(i);
        E[] eArr = this.b;
        E e2 = eArr[i];
        eArr[i] = e;
        this.modCount++;
        return e2;
    }

    @DexIgnore
    public final int size() {
        return this.c;
    }

    @DexIgnore
    public final /* synthetic */ nn2 zza(int i) {
        if (i >= this.c) {
            return new ep2(Arrays.copyOf(this.b, i), this.c);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final void zzb(int i) {
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException(zzc(i));
        }
    }

    @DexIgnore
    public final String zzc(int i) {
        int i2 = this.c;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    public final void add(int i, E e) {
        int i2;
        a();
        if (i < 0 || i > (i2 = this.c)) {
            throw new IndexOutOfBoundsException(zzc(i));
        }
        E[] eArr = this.b;
        if (i2 < eArr.length) {
            System.arraycopy(eArr, i, eArr, i + 1, i2 - i);
        } else {
            E[] eArr2 = new Object[(((i2 * 3) / 2) + 1)];
            System.arraycopy(eArr, 0, eArr2, 0, i);
            System.arraycopy(this.b, i, eArr2, i + 1, this.c - i);
            this.b = eArr2;
        }
        this.b[i] = e;
        this.c++;
        this.modCount++;
    }
}
