package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e00 {
    @DexIgnore
    public static /* final */ ConcurrentMap<String, vr> a; // = new ConcurrentHashMap();

    @DexIgnore
    public static String a(PackageInfo packageInfo) {
        if (packageInfo != null) {
            return String.valueOf(packageInfo.versionCode);
        }
        return UUID.randomUUID().toString();
    }

    @DexIgnore
    public static vr b(Context context) {
        String packageName = context.getPackageName();
        vr vrVar = (vr) a.get(packageName);
        if (vrVar != null) {
            return vrVar;
        }
        vr c = c(context);
        vr putIfAbsent = a.putIfAbsent(packageName, c);
        return putIfAbsent == null ? c : putIfAbsent;
    }

    @DexIgnore
    public static vr c(Context context) {
        return new g00(a(a(context)));
    }

    @DexIgnore
    public static PackageInfo a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("AppVersionSignature", "Cannot resolve info for" + context.getPackageName(), e);
            return null;
        }
    }
}
