package com.fossil;

import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$1$pair$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ph5$f$b extends sf6 implements ig6<il6, xe6<? super lc6<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivityDetailPresenter.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ph5$f$b(ActivityDetailPresenter.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ph5$f$b ph5_f_b = new ph5$f$b(this.this$0, xe6);
        ph5_f_b.p$ = (il6) obj;
        return ph5_f_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ph5$f$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return gj5.a.a(this.this$0.this$0.g, (List<ActivitySample>) this.this$0.this$0.n, 0);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
