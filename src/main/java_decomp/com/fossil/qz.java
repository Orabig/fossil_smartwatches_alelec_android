package com.fossil;

import com.fossil.kz;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qz implements kz, jz {
    @DexIgnore
    public /* final */ kz a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public volatile jz c;
    @DexIgnore
    public volatile jz d;
    @DexIgnore
    public kz.a e;
    @DexIgnore
    public kz.a f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public qz(Object obj, kz kzVar) {
        kz.a aVar = kz.a.CLEARED;
        this.e = aVar;
        this.f = aVar;
        this.b = obj;
        this.a = kzVar;
    }

    @DexIgnore
    public void a(jz jzVar, jz jzVar2) {
        this.c = jzVar;
        this.d = jzVar2;
    }

    @DexIgnore
    public final boolean b() {
        kz kzVar = this.a;
        return kzVar == null || kzVar.f(this);
    }

    @DexIgnore
    public boolean c(jz jzVar) {
        boolean z;
        synchronized (this.b) {
            z = h() && jzVar.equals(this.c) && !d();
        }
        return z;
    }

    @DexIgnore
    public void clear() {
        synchronized (this.b) {
            this.g = false;
            this.e = kz.a.CLEARED;
            this.f = kz.a.CLEARED;
            this.d.clear();
            this.c.clear();
        }
    }

    @DexIgnore
    public boolean d(jz jzVar) {
        boolean z;
        synchronized (this.b) {
            z = i() && (jzVar.equals(this.c) || this.e != kz.a.SUCCESS);
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        return;
     */
    @DexIgnore
    public void e(jz jzVar) {
        synchronized (this.b) {
            if (jzVar.equals(this.d)) {
                this.f = kz.a.SUCCESS;
                return;
            }
            this.e = kz.a.SUCCESS;
            if (this.a != null) {
                this.a.e(this);
            }
            if (!this.f.isComplete()) {
                this.d.clear();
            }
        }
    }

    @DexIgnore
    public boolean f(jz jzVar) {
        boolean z;
        synchronized (this.b) {
            z = b() && jzVar.equals(this.c) && this.e != kz.a.PAUSED;
        }
        return z;
    }

    @DexIgnore
    public boolean g() {
        boolean z;
        synchronized (this.b) {
            z = this.e == kz.a.SUCCESS;
        }
        return z;
    }

    @DexIgnore
    public final boolean h() {
        kz kzVar = this.a;
        return kzVar == null || kzVar.c(this);
    }

    @DexIgnore
    public final boolean i() {
        kz kzVar = this.a;
        return kzVar == null || kzVar.d(this);
    }

    @DexIgnore
    public boolean isRunning() {
        boolean z;
        synchronized (this.b) {
            z = this.e == kz.a.RUNNING;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    @DexIgnore
    public void b(jz jzVar) {
        synchronized (this.b) {
            if (!jzVar.equals(this.c)) {
                this.f = kz.a.FAILED;
                return;
            }
            this.e = kz.a.FAILED;
            if (this.a != null) {
                this.a.b(this);
            }
        }
    }

    @DexIgnore
    public kz a() {
        kz a2;
        synchronized (this.b) {
            a2 = this.a != null ? this.a.a() : this;
        }
        return a2;
    }

    @DexIgnore
    public void c() {
        synchronized (this.b) {
            this.g = true;
            try {
                if (!(this.e == kz.a.SUCCESS || this.f == kz.a.RUNNING)) {
                    this.f = kz.a.RUNNING;
                    this.d.c();
                }
                if (this.g && this.e != kz.a.RUNNING) {
                    this.e = kz.a.RUNNING;
                    this.c.c();
                }
            } finally {
                this.g = false;
            }
        }
    }

    @DexIgnore
    public boolean d() {
        boolean z;
        synchronized (this.b) {
            if (!this.d.d()) {
                if (!this.c.d()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public boolean f() {
        boolean z;
        synchronized (this.b) {
            z = this.e == kz.a.CLEARED;
        }
        return z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public boolean a(jz jzVar) {
        if (!(jzVar instanceof qz)) {
            return false;
        }
        qz qzVar = (qz) jzVar;
        if (this.c == null) {
            if (qzVar.c != null) {
                return false;
            }
        } else if (!this.c.a(qzVar.c)) {
            return false;
        }
        if (this.d == null) {
            if (qzVar.d == null) {
                return true;
            }
            return false;
        } else if (!this.d.a(qzVar.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public void e() {
        synchronized (this.b) {
            if (!this.f.isComplete()) {
                this.f = kz.a.PAUSED;
                this.d.e();
            }
            if (!this.e.isComplete()) {
                this.e = kz.a.PAUSED;
                this.c.e();
            }
        }
    }
}
