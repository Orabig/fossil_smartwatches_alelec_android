package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.u12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class iv1 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<iv1> CREATOR; // = new i52();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    @Deprecated
    public /* final */ int b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore
    public iv1(String str, int i, long j) {
        this.a = str;
        this.b = i;
        this.c = j;
    }

    @DexIgnore
    public long B() {
        long j = this.c;
        return j == -1 ? (long) this.b : j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof iv1) {
            iv1 iv1 = (iv1) obj;
            if (((p() == null || !p().equals(iv1.p())) && (p() != null || iv1.p() != null)) || B() != iv1.B()) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return u12.a(p(), Long.valueOf(B()));
    }

    @DexIgnore
    public String p() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        u12.a a2 = u12.a((Object) this);
        a2.a("name", p());
        a2.a("version", Long.valueOf(B()));
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, p(), false);
        g22.a(parcel, 2, this.b);
        g22.a(parcel, 3, B());
        g22.a(parcel, a2);
    }
}
