package com.fossil;

import com.fossil.r40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class ck1 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[r40.b.values().length];

    /*
    static {
        a[r40.b.SERIAL_NUMBER.ordinal()] = 1;
        a[r40.b.HARDWARE_REVISION.ordinal()] = 2;
        a[r40.b.FIRMWARE_VERSION.ordinal()] = 3;
        a[r40.b.MODEL_NUMBER.ordinal()] = 4;
        a[r40.b.HEART_RATE_SERIAL_NUMBER.ordinal()] = 5;
        a[r40.b.BOOTLOADER_VERSION.ordinal()] = 6;
        a[r40.b.WATCH_APP_VERSION.ordinal()] = 7;
        a[r40.b.FONT_VERSION.ordinal()] = 8;
        a[r40.b.LUTS_VERSION.ordinal()] = 9;
        a[r40.b.SUPPORTED_FILES_VERSION.ordinal()] = 10;
        a[r40.b.CURRENT_FILES_VERSION.ordinal()] = 11;
        a[r40.b.BOND_REQUIREMENT.ordinal()] = 12;
        a[r40.b.SUPPORTED_DEVICE_CONFIGS.ordinal()] = 13;
        a[r40.b.DEVICE_SECURITY_VERSION.ordinal()] = 14;
        a[r40.b.SOCKET_INFO.ordinal()] = 15;
        a[r40.b.LOCALE.ordinal()] = 16;
        a[r40.b.LOCALE_VERSION.ordinal()] = 17;
        a[r40.b.MICRO_APP_SYSTEM_VERSION.ordinal()] = 18;
        a[r40.b.UNKNOWN.ordinal()] = 19;
    }
    */
}
