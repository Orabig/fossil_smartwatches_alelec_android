package com.fossil;

import com.portfolio.platform.PortfolioApp;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ph5$e$a extends sf6 implements ig6<il6, xe6<? super Date>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    public ph5$e$a(xe6 xe6) {
        super(2, xe6);
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ph5$e$a ph5_e_a = new ph5$e$a(xe6);
        ph5_e_a.p$ = (il6) obj;
        return ph5_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ph5$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return PortfolioApp.get.instance().k();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
