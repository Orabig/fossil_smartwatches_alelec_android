package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kg0 extends ol0 {
    @DexIgnore
    public boolean K;

    @DexIgnore
    public kg0(short s, ue1 ue1) {
        super(s, lx0.VERIFY_FILE, ue1, 0, 8);
    }

    @DexIgnore
    public void a(ni1 ni1) {
        if (ni1.a != h91.DISCONNECTED) {
            a(bn0.a(this.v, (lx0) null, (String) null, il0.UNKNOWN_ERROR, (ch0) null, (sj0) null, 27));
        } else if (ni1.b == 19 || this.K) {
            a(this.v);
        } else {
            a(bn0.a(this.v, (lx0) null, (String) null, il0.CONNECTION_DROPPED, (ch0) null, (sj0) null, 27));
        }
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        if (this.v.c != il0.SUCCESS) {
            this.C = true;
        } else {
            this.K = true;
        }
        return new JSONObject();
    }
}
