package com.fossil;

import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$loadData$2$currentUser$1", f = "SleepOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
public final class rg5$b$a extends sf6 implements ig6<il6, xe6<? super MFUser>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepOverviewMonthPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rg5$b$a(SleepOverviewMonthPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        rg5$b$a rg5_b_a = new rg5$b$a(this.this$0, xe6);
        rg5_b_a.p$ = (il6) obj;
        return rg5_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((rg5$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.p.getCurrentUser();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
