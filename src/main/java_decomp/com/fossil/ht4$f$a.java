package com.fossil;

import android.content.Context;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$resetQuickResponseMessage$1$1", f = "DeleteLogoutUserUseCase.kt", l = {132, 136}, m = "invokeSuspend")
public final class ht4$f$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteLogoutUserUseCase.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ht4$f$a(DeleteLogoutUserUseCase.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ht4$f$a ht4_f_a = new ht4$f$a(this.this$0, xe6);
        ht4_f_a.p$ = (il6) obj;
        return ht4_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ht4$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            QuickResponseRepository s = this.this$0.this$0.I;
            this.L$0 = il6;
            this.label = 1;
            if (s.removeAll(this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            List list = (List) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131887257);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026.string.qr_call_you_back)");
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131887259);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026e, R.string.qr_on_my_way)");
        String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131887258);
        wg6.a((Object) a4, "LanguageHelper.getString\u2026.string.qr_cant_talk_now)");
        List c = qd6.c(new QuickResponseMessage(a2), new QuickResponseMessage(a3), new QuickResponseMessage(a4));
        QuickResponseRepository s2 = this.this$0.this$0.I;
        this.L$0 = il6;
        this.L$1 = c;
        this.label = 2;
        if (s2.insertQRs(c, this) == a) {
            return a;
        }
        return cd6.a;
    }
}
