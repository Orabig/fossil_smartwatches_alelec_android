package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dy {
    @DexIgnore
    public /* final */ List<a<?, ?>> a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<Z, R> {
        @DexIgnore
        public /* final */ Class<Z> a;
        @DexIgnore
        public /* final */ Class<R> b;
        @DexIgnore
        public /* final */ cy<Z, R> c;

        @DexIgnore
        public a(Class<Z> cls, Class<R> cls2, cy<Z, R> cyVar) {
            this.a = cls;
            this.b = cls2;
            this.c = cyVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.b);
        }
    }

    @DexIgnore
    public synchronized <Z, R> void a(Class<Z> cls, Class<R> cls2, cy<Z, R> cyVar) {
        this.a.add(new a(cls, cls2, cyVar));
    }

    @DexIgnore
    public synchronized <Z, R> List<Class<R>> b(Class<Z> cls, Class<R> cls2) {
        ArrayList arrayList = new ArrayList();
        if (cls2.isAssignableFrom(cls)) {
            arrayList.add(cls2);
            return arrayList;
        }
        for (a<?, ?> a2 : this.a) {
            if (a2.a(cls, cls2)) {
                arrayList.add(cls2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public synchronized <Z, R> cy<Z, R> a(Class<Z> cls, Class<R> cls2) {
        if (cls2.isAssignableFrom(cls)) {
            return ey.a();
        }
        for (a next : this.a) {
            if (next.a(cls, cls2)) {
                return next.c;
            }
        }
        throw new IllegalArgumentException("No transcoder registered to transcode from " + cls + " to " + cls2);
    }
}
