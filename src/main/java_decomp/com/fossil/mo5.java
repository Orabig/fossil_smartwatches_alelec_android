package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeTextViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo5 implements Factory<lo5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public mo5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static mo5 a(Provider<ThemeRepository> provider) {
        return new mo5(provider);
    }

    @DexIgnore
    public static lo5 b(Provider<ThemeRepository> provider) {
        return new CustomizeTextViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeTextViewModel get() {
        return b(this.a);
    }
}
