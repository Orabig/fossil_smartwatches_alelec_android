package com.fossil;

import android.os.SystemClock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n42 implements k42 {
    @DexIgnore
    public static /* final */ n42 a; // = new n42();

    @DexIgnore
    public static k42 d() {
        return a;
    }

    @DexIgnore
    public long a() {
        return System.nanoTime();
    }

    @DexIgnore
    public long b() {
        return System.currentTimeMillis();
    }

    @DexIgnore
    public long c() {
        return SystemClock.elapsedRealtime();
    }
}
