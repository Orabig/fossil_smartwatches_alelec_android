package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f66 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ r36 b;
    @DexIgnore
    public /* final */ /* synthetic */ t36 c;

    @DexIgnore
    public f66(Context context, r36 r36, t36 t36) {
        this.a = context;
        this.b = r36;
        this.c = t36;
    }

    @DexIgnore
    public final void run() {
        try {
            s36 s36 = new s36(this.a, q36.a(this.a, false, this.b), this.c.a, this.b);
            s36.g().c = this.c.c;
            new k46(s36).a();
        } catch (Throwable th) {
            q36.m.a(th);
            q36.a(this.a, th);
        }
    }
}
