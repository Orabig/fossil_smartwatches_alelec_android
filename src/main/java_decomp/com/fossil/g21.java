package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g21 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ hm1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g21(hm1 hm1) {
        super(1);
        this.a = hm1;
    }

    @DexIgnore
    public final void a(qv0 qv0) {
        o81 o81 = (o81) qv0;
        hm1 hm1 = this.a;
        byte[] bArr = hm1.M;
        long j = o81.L;
        byte[] a2 = md6.a(bArr, (int) j, (int) Math.min(hm1.C, j + o81.M));
        hm1 hm12 = this.a;
        long j2 = o81.L;
        short s = hm12.O;
        ue1 ue1 = hm12.w;
        ae1 ae1 = new ae1(s, new ds0(a2, Math.min(ue1.j, ue1.o), rg1.FTD), hm12.w);
        s91 s91 = new s91(hm12, a2, j2);
        if1.a((if1) hm12, (qv0) ae1, (hg6) new nb1(hm12, a2), (hg6) new id1(hm12), (ig6) s91, (hg6) null, (hg6) null, 48, (Object) null);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((qv0) obj);
        return cd6.a;
    }
}
