package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kz5 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public kz5(int i, int i2, int i3, int i4) {
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.a = this.d == this.e;
    }

    @DexIgnore
    public final int a() {
        return this.e;
    }

    @DexIgnore
    public final int b() {
        return this.d;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final int d() {
        return this.c;
    }

    @DexIgnore
    public final boolean e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof kz5)) {
            return false;
        }
        kz5 kz5 = (kz5) obj;
        return this.b == kz5.b && this.c == kz5.c && this.d == kz5.d && this.e == kz5.e;
    }

    @DexIgnore
    public int hashCode() {
        return (((((d.a(this.b) * 31) + d.a(this.c)) * 31) + d.a(this.d)) * 31) + d.a(this.e);
    }

    @DexIgnore
    public String toString() {
        return "DashBarInformation(length=" + this.b + ", progress=" + this.c + ", animStart=" + this.d + ", animEnd=" + this.e + ")";
    }
}
