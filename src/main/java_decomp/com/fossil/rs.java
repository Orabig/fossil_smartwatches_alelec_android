package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rs {
    @DexIgnore
    public boolean a(File file) {
        return file.exists();
    }

    @DexIgnore
    public long b(File file) {
        return file.length();
    }

    @DexIgnore
    public File a(String str) {
        return new File(str);
    }
}
