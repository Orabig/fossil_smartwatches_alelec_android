package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ie3 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ie3> CREATOR; // = new je3();
    @DexIgnore
    public /* final */ ke3 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public ie3(ke3 ke3, int i, int i2, int i3) {
        this.a = ke3;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public final void a(td3 td3) {
        int i = this.b;
        if (i == 1) {
            td3.a(this.a);
        } else if (i == 2) {
            td3.a(this.a, this.c, this.d);
        } else if (i == 3) {
            td3.b(this.a, this.c, this.d);
        } else if (i != 4) {
            StringBuilder sb = new StringBuilder(25);
            sb.append("Unknown type: ");
            sb.append(i);
            Log.w("ChannelEventParcelable", sb.toString());
        } else {
            td3.c(this.a, this.c, this.d);
        }
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.a);
        int i = this.b;
        String num = i != 1 ? i != 2 ? i != 3 ? i != 4 ? Integer.toString(i) : "OUTPUT_CLOSED" : "INPUT_CLOSED" : "CHANNEL_CLOSED" : "CHANNEL_OPENED";
        int i2 = this.c;
        String num2 = i2 != 0 ? i2 != 1 ? i2 != 2 ? i2 != 3 ? Integer.toString(i2) : "CLOSE_REASON_LOCAL_CLOSE" : "CLOSE_REASON_REMOTE_CLOSE" : "CLOSE_REASON_DISCONNECTED" : "CLOSE_REASON_NORMAL";
        int i3 = this.d;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81 + String.valueOf(num).length() + String.valueOf(num2).length());
        sb.append("ChannelEventParcelable[, channel=");
        sb.append(valueOf);
        sb.append(", type=");
        sb.append(num);
        sb.append(", closeReason=");
        sb.append(num2);
        sb.append(", appErrorCode=");
        sb.append(i3);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, (Parcelable) this.a, i, false);
        g22.a(parcel, 3, this.b);
        g22.a(parcel, 4, this.c);
        g22.a(parcel, 5, this.d);
        g22.a(parcel, a2);
    }
}
