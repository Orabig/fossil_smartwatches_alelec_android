package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ra3 a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ l83 c;

    @DexIgnore
    public p83(l83 l83, ra3 ra3, boolean z) {
        this.c = l83;
        this.a = ra3;
        this.b = z;
    }

    @DexIgnore
    public final void run() {
        l43 d = this.c.d;
        if (d == null) {
            this.c.b().t().a("Discarding data. Failed to send app launch");
            return;
        }
        try {
            d.d(this.a);
            if (this.b) {
                this.c.s().C();
            }
            this.c.a(d, (e22) null, this.a);
            this.c.I();
        } catch (RemoteException e) {
            this.c.b().t().a("Failed to send app launch to the service", e);
        }
    }
}
