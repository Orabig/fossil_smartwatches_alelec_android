package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w80 extends p40 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ j70 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<w80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            float readFloat = parcel.readFloat();
            float readFloat2 = parcel.readFloat();
            float readFloat3 = parcel.readFloat();
            int readInt = parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                return new w80(readFloat, readFloat2, readFloat3, readInt, j70.valueOf(readString));
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new w80[i];
        }
    }

    @DexIgnore
    public w80(float f, float f2, float f3, int i, j70 j70) {
        this.a = cw0.a(f, 2);
        this.b = cw0.a(f2, 2);
        this.c = cw0.a(f3, 2);
        this.d = i;
        this.e = j70;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.CURRENT_TEMPERATURE, (Object) Float.valueOf(this.a)), bm0.HIGH_TEMPERATURE, (Object) Float.valueOf(this.b)), bm0.LOW_TEMPERATURE, (Object) Float.valueOf(this.c)), bm0.CHANCE_OF_RAIN, (Object) Integer.valueOf(this.d)), bm0.WEATHER_CONDITION, (Object) cw0.a((Enum<?>) this.e));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject put = new JSONObject().put("temp", Float.valueOf(this.a)).put("high", Float.valueOf(this.b)).put("low", Float.valueOf(this.c)).put("rain", this.d).put("cond_id", this.e.a());
        wg6.a(put, "JSONObject()\n           \u2026rrentWeatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(w80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            w80 w80 = (w80) obj;
            return this.a == w80.a && this.b == w80.b && this.c == w80.c && this.d == w80.d && this.e == w80.e;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.CurrentWeatherInfo");
    }

    @DexIgnore
    public final int getChanceOfRain() {
        return this.d;
    }

    @DexIgnore
    public final float getCurrentTemperature() {
        return this.a;
    }

    @DexIgnore
    public final j70 getCurrentWeatherCondition() {
        return this.e;
    }

    @DexIgnore
    public final float getHighTemperature() {
        return this.b;
    }

    @DexIgnore
    public final float getLowTemperature() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Float.valueOf(this.b).hashCode();
        int hashCode2 = Float.valueOf(this.c).hashCode();
        return this.e.hashCode() + ((((hashCode2 + ((hashCode + (Float.valueOf(this.a).hashCode() * 31)) * 31)) * 31) + this.d) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeFloat(this.a);
        }
        if (parcel != null) {
            parcel.writeFloat(this.b);
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
