package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xd5 implements Factory<wd5> {
    @DexIgnore
    public static CaloriesOverviewWeekPresenter a(vd5 vd5, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new CaloriesOverviewWeekPresenter(vd5, userRepository, summariesRepository, portfolioApp);
    }
}
