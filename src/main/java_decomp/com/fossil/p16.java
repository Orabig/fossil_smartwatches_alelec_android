package com.fossil;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.squareup.picasso.Picasso;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p16 {
    @DexIgnore
    public /* final */ HandlerThread a; // = new HandlerThread("Picasso-Stats", 10);
    @DexIgnore
    public /* final */ u06 b;
    @DexIgnore
    public /* final */ Handler c;
    @DexIgnore
    public long d;
    @DexIgnore
    public long e;
    @DexIgnore
    public long f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public long j;
    @DexIgnore
    public long k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Handler {
        @DexIgnore
        public /* final */ p16 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.p16$a$a")
        /* renamed from: com.fossil.p16$a$a  reason: collision with other inner class name */
        public class C0034a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Message a;

            @DexIgnore
            public C0034a(a aVar, Message message) {
                this.a = message;
            }

            @DexIgnore
            public void run() {
                throw new AssertionError("Unhandled stats message." + this.a.what);
            }
        }

        @DexIgnore
        public a(Looper looper, p16 p16) {
            super(looper);
            this.a = p16;
        }

        @DexIgnore
        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                this.a.d();
            } else if (i == 1) {
                this.a.e();
            } else if (i == 2) {
                this.a.b((long) message.arg1);
            } else if (i == 3) {
                this.a.c((long) message.arg1);
            } else if (i != 4) {
                Picasso.p.post(new C0034a(this, message));
            } else {
                this.a.a((Long) message.obj);
            }
        }
    }

    @DexIgnore
    public p16(u06 u06) {
        this.b = u06;
        this.a.start();
        t16.a(this.a.getLooper());
        this.c = new a(this.a.getLooper(), this);
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        a(bitmap, 2);
    }

    @DexIgnore
    public void b(Bitmap bitmap) {
        a(bitmap, 3);
    }

    @DexIgnore
    public void c() {
        this.c.sendEmptyMessage(1);
    }

    @DexIgnore
    public void d() {
        this.d++;
    }

    @DexIgnore
    public void e() {
        this.e++;
    }

    @DexIgnore
    public void a(long j2) {
        Handler handler = this.c;
        handler.sendMessage(handler.obtainMessage(4, Long.valueOf(j2)));
    }

    @DexIgnore
    public void b() {
        this.c.sendEmptyMessage(0);
    }

    @DexIgnore
    public void c(long j2) {
        this.n++;
        this.h += j2;
        this.k = a(this.m, this.h);
    }

    @DexIgnore
    public void a(Long l2) {
        this.l++;
        this.f += l2.longValue();
        this.i = a(this.l, this.f);
    }

    @DexIgnore
    public void b(long j2) {
        this.m++;
        this.g += j2;
        this.j = a(this.m, this.g);
    }

    @DexIgnore
    public q16 a() {
        return new q16(this.b.a(), this.b.size(), this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, System.currentTimeMillis());
    }

    @DexIgnore
    public final void a(Bitmap bitmap, int i2) {
        int a2 = t16.a(bitmap);
        Handler handler = this.c;
        handler.sendMessage(handler.obtainMessage(i2, a2, 0));
    }

    @DexIgnore
    public static long a(int i2, long j2) {
        return j2 / ((long) i2);
    }
}
