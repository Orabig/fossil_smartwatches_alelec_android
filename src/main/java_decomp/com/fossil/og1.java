package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class og1 implements Parcelable.Creator<ji1> {
    @DexIgnore
    public /* synthetic */ og1(qg6 qg6) {
    }

    @DexIgnore
    public ji1 createFromParcel(Parcel parcel) {
        return new ji1(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new ji1[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m49createFromParcel(Parcel parcel) {
        return new ji1(parcel, (qg6) null);
    }
}
