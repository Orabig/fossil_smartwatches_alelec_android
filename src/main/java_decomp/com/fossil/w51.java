package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w51 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ t71 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w51(t71 t71) {
        super(1);
        this.a = t71;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        t71 t71 = this.a;
        uj0 uj0 = (uj0) obj;
        long j = uj0.K;
        long j2 = uj0.L;
        long j3 = uj0.M;
        if (0 == j2) {
            t71.C = t71.D;
            t71.a(km1.a(t71.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
        } else if (j != t71.D) {
            t71.a(km1.a(t71.v, (eh1) null, sk1.FLOW_BROKEN, (bn0) null, 5));
        } else {
            t71.E = j + j2;
            long a2 = h51.a.a(t71.G.e, (int) j, (int) j2, q11.CRC32);
            cc0 cc0 = cc0.DEBUG;
            new Object[1][0] = Long.valueOf(a2);
            if (j3 == a2) {
                long j4 = t71.E;
                long j5 = t71.F;
                if (j4 == j5) {
                    t71.C = j4;
                    t71.a(km1.a(t71.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
                } else {
                    t71.E = Math.min(((j4 - t71.D) / ((long) 2)) + j4, j5);
                    t71.D = j4;
                    if1.a(t71, lx0.VERIFY_DATA, (lx0) null, 2, (Object) null);
                }
            } else {
                t71.E = (t71.E + t71.D) / ((long) 2);
                if1.a(t71, lx0.VERIFY_DATA, (lx0) null, 2, (Object) null);
            }
        }
        return cd6.a;
    }
}
