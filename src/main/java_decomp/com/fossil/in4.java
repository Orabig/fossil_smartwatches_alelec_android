package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.ui.BaseActivity;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class in4 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public GoogleSignInOptions a;
    @DexIgnore
    public qt1 b;
    @DexIgnore
    public /* final */ String c; // = "23412525";
    @DexIgnore
    public WeakReference<wq4> d;
    @DexIgnore
    public ln4 e;
    @DexIgnore
    public int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<TResult> implements kc3<Void> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onComplete(qc3<Void> qc3) {
            wg6.b(qc3, "it");
            FLogger.INSTANCE.getLocal().d(in4.g, "Log out google account completely");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements lc3 {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final void onFailure(Exception exc) {
            wg6.b(exc, "it");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = in4.g;
            local.d(d, "Could not log out google account, error = " + exc.getLocalizedMessage());
            exc.printStackTrace();
        }
    }

    /*
    static {
        new a((qg6) null);
        String canonicalName = in4.class.getCanonicalName();
        if (canonicalName != null) {
            wg6.a((Object) canonicalName, "MFLoginGoogleManager::class.java.canonicalName!!");
            g = canonicalName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void a(WeakReference<wq4> weakReference, ln4 ln4) {
        String str;
        wg6.b(weakReference, Constants.ACTIVITY);
        wg6.b(ln4, Constants.CALLBACK);
        Access a2 = new SoLibraryLoader().a((Context) PortfolioApp.get.instance());
        if (a2 == null || (str = a2.getA()) == null) {
            str = this.c;
        }
        if (str != null) {
            String obj = yj6.d(str).toString();
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(GoogleSignInOptions.t);
            aVar.a(obj);
            aVar.b(obj);
            aVar.b();
            aVar.d();
            GoogleSignInOptions a3 = aVar.a();
            wg6.a((Object) a3, "GoogleSignInOptions.Buil\u2026\n                .build()");
            this.a = a3;
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            GoogleSignInOptions googleSignInOptions = this.a;
            if (googleSignInOptions != null) {
                this.b = ot1.a(applicationContext, googleSignInOptions);
                this.e = ln4;
                this.d = weakReference;
                b();
                return;
            }
            wg6.d("gso");
            throw null;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @DexIgnore
    public final void b() {
        a((WeakReference<Activity>) this.d);
        a();
    }

    @DexIgnore
    public final void c() {
        a((WeakReference<Activity>) this.d);
    }

    @DexIgnore
    public final boolean a(int i, int i2, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Inside .onActivityResult requestCode=" + i + ", resultCode=" + i2);
        if (i != 922) {
            return true;
        }
        if (intent != null) {
            st1 a2 = jt1.f.a(intent);
            a(a2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = g;
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .onActivityResult googleSignInResult=");
            wg6.a((Object) a2, Constants.RESULT);
            sb.append(a2.o());
            local2.d(str2, sb.toString());
            return true;
        }
        a((st1) null);
        return true;
    }

    @DexIgnore
    public final void a(st1 st1) {
        int i = 600;
        if (st1 == null) {
            c();
            ln4 ln4 = this.e;
            if (ln4 != null) {
                ln4.a(600, (gv1) null, "");
            }
        } else if (st1.b()) {
            GoogleSignInAccount a2 = st1.a();
            if (a2 == null) {
                ln4 ln42 = this.e;
                if (ln42 != null) {
                    ln42.a(600, (gv1) null, "");
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(g, "Step 1: Login using google success");
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String C = a2.C();
                if (C == null) {
                    C = "";
                }
                signUpSocialAuth.setEmail(C);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = g;
                local.d(str, "Google user email is " + a2.C());
                String E = a2.E();
                if (E == null) {
                    E = "";
                }
                signUpSocialAuth.setFirstName(E);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "Google user first name is " + a2.E());
                String D = a2.D();
                if (D == null) {
                    D = "";
                }
                signUpSocialAuth.setLastName(D);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = g;
                local3.d(str3, "Google user last name is " + a2.D());
                String H = a2.H();
                if (H == null) {
                    H = "";
                }
                signUpSocialAuth.setToken(H);
                signUpSocialAuth.setClientId(tj4.f.a(""));
                signUpSocialAuth.setService("google");
                ln4 ln43 = this.e;
                if (ln43 != null) {
                    ln43.a(signUpSocialAuth);
                }
                this.f = 0;
                c();
            }
        } else {
            Status o = st1.o();
            wg6.a((Object) o, "result.status");
            int B = o.B();
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = g;
            local4.d(str4, "login result code from google: " + B);
            if (B == 12502) {
                int i2 = this.f;
                if (i2 < 2) {
                    this.f = i2 + 1;
                    a();
                    return;
                }
                this.f = 0;
                FLogger.INSTANCE.getLocal().d(g, "why the login session always end up here, bug from Google!!");
                c();
                ln4 ln44 = this.e;
                if (ln44 != null) {
                    ln44.a(600, (gv1) null, "");
                    return;
                }
                return;
            }
            if (B == 12501) {
                i = 2;
            }
            c();
            ln4 ln45 = this.e;
            if (ln45 != null) {
                ln45.a(i, (gv1) null, "");
            }
        }
    }

    @DexIgnore
    public final void a() {
        qt1 qt1;
        FLogger.INSTANCE.getLocal().d(g, "connectNewAccount");
        WeakReference<wq4> weakReference = this.d;
        if (weakReference != null) {
            if (weakReference == null) {
                wg6.a();
                throw null;
            } else if (!(weakReference.get() == null || (qt1 = this.b) == null)) {
                if (qt1 != null) {
                    Intent i = qt1.i();
                    wg6.a((Object) i, "googleSignInClient!!.signInIntent");
                    WeakReference<wq4> weakReference2 = this.d;
                    if (weakReference2 != null) {
                        BaseActivity baseActivity = (BaseActivity) weakReference2.get();
                        if (baseActivity != null) {
                            baseActivity.startActivityForResult(i, 922);
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
        }
        ln4 ln4 = this.e;
        if (ln4 != null) {
            ln4.a(600, (gv1) null, "");
        }
    }

    @DexIgnore
    public final void a(WeakReference<Activity> weakReference) {
        if ((weakReference != null ? (Activity) weakReference.get() : null) != null && ot1.a((Context) weakReference.get()) != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "inside .logOut(), googleSignInClient=" + this.b);
            qt1 qt1 = this.b;
            if (qt1 == null) {
                return;
            }
            if (qt1 != null) {
                qc3 j = qt1.j();
                if (j != null) {
                    j.a(b.a);
                }
                if (j != null) {
                    j.a(c.a);
                    return;
                }
                return;
            }
            wg6.a();
            throw null;
        }
    }
}
