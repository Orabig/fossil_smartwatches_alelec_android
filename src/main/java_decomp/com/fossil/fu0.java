package com.fossil;

import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.content.Context;
import java.util.concurrent.CopyOnWriteArraySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu0 {
    @DexIgnore
    public BluetoothGattServer a;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<h11> b; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ ce1 d; // = new ce1(this);
    @DexIgnore
    public /* final */ BluetoothGattServerCallback e; // = new ql0(this);
    @DexIgnore
    public /* final */ vj0 f; // = new vj0();
    @DexIgnore
    public /* final */ jn0 g; // = new jn0(this);
    @DexIgnore
    public /* final */ Context h;

    @DexIgnore
    public fu0(Context context) {
        this.h = context;
    }
}
