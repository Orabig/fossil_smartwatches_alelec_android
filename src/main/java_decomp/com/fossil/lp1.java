package com.fossil;

import android.content.Context;
import com.fossil.xp1;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp1 extends xp1 {
    @DexIgnore
    public Provider<Executor> a;
    @DexIgnore
    public Provider<Context> b;
    @DexIgnore
    public Provider c;
    @DexIgnore
    public Provider d;
    @DexIgnore
    public Provider e;
    @DexIgnore
    public Provider<rs1> f;
    @DexIgnore
    public Provider<fr1> g;
    @DexIgnore
    public Provider<rr1> h;
    @DexIgnore
    public Provider<sq1> i;
    @DexIgnore
    public Provider<lr1> j;
    @DexIgnore
    public Provider<pr1> o;
    @DexIgnore
    public Provider<wp1> p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements xp1.a {
        @DexIgnore
        public Context a;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public xp1 build() {
            z76.a(this.a, Context.class);
            return new lp1(this.a);
        }

        @DexIgnore
        public b a(Context context) {
            z76.a(context);
            this.a = context;
            return this;
        }
    }

    @DexIgnore
    public static xp1.a m() {
        return new b();
    }

    @DexIgnore
    public final void a(Context context) {
        this.a = w76.a(pp1.a());
        this.b = x76.a(context);
        this.c = iq1.a(this.b, bt1.a(), ct1.a());
        this.d = w76.a(kq1.a(this.b, this.c));
        this.e = ws1.a(this.b, xr1.a());
        this.f = w76.a(ss1.a(bt1.a(), ct1.a(), yr1.a(), this.e));
        this.g = wq1.a((Provider<zs1>) bt1.a());
        this.h = yq1.a(this.b, (Provider<ur1>) this.f, this.g, (Provider<zs1>) ct1.a());
        Provider<Executor> provider = this.a;
        Provider provider2 = this.d;
        Provider<rr1> provider3 = this.h;
        Provider<rs1> provider4 = this.f;
        this.i = tq1.a(provider, provider2, provider3, provider4, provider4);
        Provider<Context> provider5 = this.b;
        Provider provider6 = this.d;
        Provider<rs1> provider7 = this.f;
        this.j = mr1.a(provider5, provider6, provider7, this.h, this.a, provider7, bt1.a());
        Provider<Executor> provider8 = this.a;
        Provider<rs1> provider9 = this.f;
        this.o = qr1.a(provider8, provider9, this.h, provider9);
        this.p = w76.a(yp1.a(bt1.a(), ct1.a(), this.i, this.j, this.o));
    }

    @DexIgnore
    public ur1 k() {
        return (ur1) this.f.get();
    }

    @DexIgnore
    public wp1 l() {
        return (wp1) this.p.get();
    }

    @DexIgnore
    public lp1(Context context) {
        a(context);
    }
}
