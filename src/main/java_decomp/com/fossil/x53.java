package com.fossil;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x53 implements v63 {
    @DexIgnore
    public static volatile x53 G;
    @DexIgnore
    public volatile Boolean A;
    @DexIgnore
    public Boolean B;
    @DexIgnore
    public Boolean C;
    @DexIgnore
    public int D;
    @DexIgnore
    public AtomicInteger E; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ long F;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ bb3 f;
    @DexIgnore
    public /* final */ cb3 g;
    @DexIgnore
    public /* final */ h53 h;
    @DexIgnore
    public /* final */ t43 i;
    @DexIgnore
    public /* final */ u53 j;
    @DexIgnore
    public /* final */ m93 k;
    @DexIgnore
    public /* final */ ma3 l;
    @DexIgnore
    public /* final */ r43 m;
    @DexIgnore
    public /* final */ k42 n;
    @DexIgnore
    public /* final */ g83 o;
    @DexIgnore
    public /* final */ e73 p;
    @DexIgnore
    public /* final */ w03 q;
    @DexIgnore
    public /* final */ b83 r;
    @DexIgnore
    public p43 s;
    @DexIgnore
    public l83 t;
    @DexIgnore
    public d03 u;
    @DexIgnore
    public q43 v;
    @DexIgnore
    public n53 w;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public Boolean y;
    @DexIgnore
    public long z;

    @DexIgnore
    public x53(a73 a73) {
        Bundle bundle;
        boolean z2 = false;
        w12.a(a73);
        this.f = new bb3(a73.a);
        l03.a(this.f);
        this.a = a73.a;
        this.b = a73.b;
        this.c = a73.c;
        this.d = a73.d;
        this.e = a73.h;
        this.A = a73.e;
        mv2 mv2 = a73.g;
        if (!(mv2 == null || (bundle = mv2.g) == null)) {
            Object obj = bundle.get("measurementEnabled");
            if (obj instanceof Boolean) {
                this.B = (Boolean) obj;
            }
            Object obj2 = mv2.g.get("measurementDeactivated");
            if (obj2 instanceof Boolean) {
                this.C = (Boolean) obj2;
            }
        }
        pk2.a(this.a);
        this.n = n42.d();
        this.F = this.n.b();
        this.g = new cb3(this);
        h53 h53 = new h53(this);
        h53.o();
        this.h = h53;
        t43 t43 = new t43(this);
        t43.o();
        this.i = t43;
        ma3 ma3 = new ma3(this);
        ma3.o();
        this.l = ma3;
        r43 r43 = new r43(this);
        r43.o();
        this.m = r43;
        this.q = new w03(this);
        g83 g83 = new g83(this);
        g83.x();
        this.o = g83;
        e73 e73 = new e73(this);
        e73.x();
        this.p = e73;
        m93 m93 = new m93(this);
        m93.x();
        this.k = m93;
        b83 b83 = new b83(this);
        b83.o();
        this.r = b83;
        u53 u53 = new u53(this);
        u53.o();
        this.j = u53;
        mv2 mv22 = a73.g;
        if (!(mv22 == null || mv22.b == 0)) {
            z2 = true;
        }
        boolean z3 = !z2;
        if (this.a.getApplicationContext() instanceof Application) {
            e73 v2 = v();
            if (v2.c().getApplicationContext() instanceof Application) {
                Application application = (Application) v2.c().getApplicationContext();
                if (v2.c == null) {
                    v2.c = new v73(v2, (g73) null);
                }
                if (z3) {
                    application.unregisterActivityLifecycleCallbacks(v2.c);
                    application.registerActivityLifecycleCallbacks(v2.c);
                    v2.b().B().a("Registered activity lifecycle callback");
                }
            }
        } else {
            b().w().a("Application context is not an Application");
        }
        this.j.a((Runnable) new z53(this, a73));
    }

    @DexIgnore
    public final String A() {
        return this.b;
    }

    @DexIgnore
    public final String B() {
        return this.c;
    }

    @DexIgnore
    public final String C() {
        return this.d;
    }

    @DexIgnore
    public final boolean D() {
        return this.e;
    }

    @DexIgnore
    public final g83 E() {
        b((z33) this.o);
        return this.o;
    }

    @DexIgnore
    public final l83 F() {
        b((z33) this.t);
        return this.t;
    }

    @DexIgnore
    public final d03 G() {
        b((s63) this.u);
        return this.u;
    }

    @DexIgnore
    public final q43 H() {
        b((z33) this.v);
        return this.v;
    }

    @DexIgnore
    public final w03 I() {
        w03 w03 = this.q;
        if (w03 != null) {
            return w03;
        }
        throw new IllegalStateException("Component not created");
    }

    @DexIgnore
    public final void a(a73 a73) {
        String str;
        v43 v43;
        a().g();
        cb3.v();
        d03 d03 = new d03(this);
        d03.o();
        this.u = d03;
        q43 q43 = new q43(this, a73.f);
        q43.x();
        this.v = q43;
        p43 p43 = new p43(this);
        p43.x();
        this.s = p43;
        l83 l83 = new l83(this);
        l83.x();
        this.t = l83;
        this.l.p();
        this.h.p();
        this.w = new n53(this);
        this.v.y();
        b().z().a("App measurement is starting up, version", Long.valueOf(this.g.n()));
        b().z().a("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String A2 = q43.A();
        if (TextUtils.isEmpty(this.b)) {
            if (w().d(A2)) {
                v43 = b().z();
                str = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
            } else {
                v43 = b().z();
                String valueOf = String.valueOf(A2);
                str = valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ");
            }
            v43.a(str);
        }
        b().A().a("Debug-level message logging enabled");
        if (this.D != this.E.get()) {
            b().t().a("Not all components initialized", Integer.valueOf(this.D), Integer.valueOf(this.E.get()));
        }
        this.x = true;
    }

    @DexIgnore
    public final t43 b() {
        b((s63) this.i);
        return this.i;
    }

    @DexIgnore
    public final Context c() {
        return this.a;
    }

    @DexIgnore
    public final bb3 d() {
        return this.f;
    }

    @DexIgnore
    public final void e() {
        a().g();
        if (q().e.a() == 0) {
            q().e.a(this.n.b());
        }
        if (Long.valueOf(q().j.a()).longValue() == 0) {
            b().B().a("Persisting first open", Long.valueOf(this.F));
            q().j.a(this.F);
        }
        if (l()) {
            if (!TextUtils.isEmpty(H().B()) || !TextUtils.isEmpty(H().C())) {
                w();
                if (ma3.a(H().B(), q().s(), H().C(), q().t())) {
                    b().z().a("Rechecking which service to use due to a GMP App Id change");
                    q().v();
                    y().A();
                    this.t.G();
                    this.t.E();
                    q().j.a(this.F);
                    q().l.a((String) null);
                }
                q().c(H().B());
                q().d(H().C());
            }
            v().a(q().l.a());
            if (es2.a() && this.g.a(l03.T0) && !w().w() && !TextUtils.isEmpty(q().B.a())) {
                b().w().a("Remote config removed with active feature rollouts");
                q().B.a((String) null);
            }
            if (!TextUtils.isEmpty(H().B()) || !TextUtils.isEmpty(H().C())) {
                boolean g2 = g();
                if (!q().z() && !this.g.p()) {
                    q().d(!g2);
                }
                if (g2) {
                    v().H();
                }
                u93 u93 = s().d;
                if (u93.b.l().n(u93.b.p().A()) && qs2.a() && u93.b.l().e(u93.b.p().A(), l03.e0)) {
                    u93.b.g();
                    if (u93.b.k().a(u93.b.zzm().b())) {
                        u93.b.k().r.a(true);
                        if (Build.VERSION.SDK_INT >= 16) {
                            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
                            ActivityManager.getMyMemoryState(runningAppProcessInfo);
                            if (runningAppProcessInfo.importance == 100) {
                                u93.b.b().B().a("Detected application was in foreground");
                                u93.b(u93.b.zzm().b(), false);
                            }
                        }
                    }
                }
                F().a((AtomicReference<String>) new AtomicReference());
            }
        } else if (g()) {
            if (!w().c("android.permission.INTERNET")) {
                b().t().a("App is missing INTERNET permission");
            }
            if (!w().c("android.permission.ACCESS_NETWORK_STATE")) {
                b().t().a("App is missing ACCESS_NETWORK_STATE permission");
            }
            if (!g52.b(this.a).a() && !this.g.u()) {
                if (!o53.a(this.a)) {
                    b().t().a("AppMeasurementReceiver not registered/enabled");
                }
                if (!ma3.a(this.a, false)) {
                    b().t().a("AppMeasurementService not registered/enabled");
                }
            }
            b().t().a("Uploading is not possible. App measurement disabled");
        }
        q().t.a(this.g.a(l03.q0));
        q().u.a(this.g.a(l03.r0));
    }

    @DexIgnore
    public final boolean f() {
        return this.A != null && this.A.booleanValue();
    }

    @DexIgnore
    public final boolean g() {
        boolean z2;
        a().g();
        o();
        if (this.g.a(l03.k0)) {
            if (this.g.p()) {
                return false;
            }
            Boolean bool = this.C;
            if (bool != null && bool.booleanValue()) {
                return false;
            }
            Boolean w2 = q().w();
            if (w2 != null) {
                return w2.booleanValue();
            }
            Boolean q2 = this.g.q();
            if (q2 != null) {
                return q2.booleanValue();
            }
            Boolean bool2 = this.B;
            if (bool2 != null) {
                return bool2.booleanValue();
            }
            if (rw1.b()) {
                return false;
            }
            if (!this.g.a(l03.f0) || this.A == null) {
                return true;
            }
            return this.A.booleanValue();
        } else if (this.g.p()) {
            return false;
        } else {
            Boolean q3 = this.g.q();
            if (q3 != null) {
                z2 = q3.booleanValue();
            } else {
                z2 = !rw1.b();
                if (z2 && this.A != null && l03.f0.a(null).booleanValue()) {
                    z2 = this.A.booleanValue();
                }
            }
            return q().c(z2);
        }
    }

    @DexIgnore
    public final long h() {
        Long valueOf = Long.valueOf(q().j.a());
        if (valueOf.longValue() == 0) {
            return this.F;
        }
        return Math.min(this.F, valueOf.longValue());
    }

    @DexIgnore
    public final void i() {
    }

    @DexIgnore
    public final void j() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    @DexIgnore
    public final void k() {
        this.E.incrementAndGet();
    }

    @DexIgnore
    public final boolean l() {
        o();
        a().g();
        Boolean bool = this.y;
        if (bool == null || this.z == 0 || (bool != null && !bool.booleanValue() && Math.abs(this.n.c() - this.z) > 1000)) {
            this.z = this.n.c();
            boolean z2 = true;
            this.y = Boolean.valueOf(w().c("android.permission.INTERNET") && w().c("android.permission.ACCESS_NETWORK_STATE") && (g52.b(this.a).a() || this.g.u() || (o53.a(this.a) && ma3.a(this.a, false))));
            if (this.y.booleanValue()) {
                if (!w().c(H().B(), H().C()) && TextUtils.isEmpty(H().C())) {
                    z2 = false;
                }
                this.y = Boolean.valueOf(z2);
            }
        }
        return this.y.booleanValue();
    }

    @DexIgnore
    public final void m() {
        a().g();
        b((s63) n());
        String A2 = H().A();
        Pair<String, Boolean> a2 = q().a(A2);
        if (!this.g.r().booleanValue() || ((Boolean) a2.second).booleanValue() || TextUtils.isEmpty((CharSequence) a2.first)) {
            b().A().a("ADID unavailable to retrieve Deferred Deep Link. Skipping");
        } else if (!n().s()) {
            b().w().a("Network is not available for Deferred Deep Link request. Skipping");
        } else {
            ma3 w2 = w();
            URL a3 = w2.a(H().l().n(), A2, (String) a2.first, q().A.a() - 1);
            b83 n2 = n();
            b63 b63 = new b63(this);
            n2.g();
            n2.n();
            w12.a(a3);
            w12.a(b63);
            n2.a().b((Runnable) new d83(n2, A2, a3, (byte[]) null, (Map<String, String>) null, b63));
        }
    }

    @DexIgnore
    public final b83 n() {
        b((s63) this.r);
        return this.r;
    }

    @DexIgnore
    public final void o() {
        if (!this.x) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }

    @DexIgnore
    public final cb3 p() {
        return this.g;
    }

    @DexIgnore
    public final h53 q() {
        a((t63) this.h);
        return this.h;
    }

    @DexIgnore
    public final t43 r() {
        t43 t43 = this.i;
        if (t43 == null || !t43.r()) {
            return null;
        }
        return this.i;
    }

    @DexIgnore
    public final m93 s() {
        b((z33) this.k);
        return this.k;
    }

    @DexIgnore
    public final n53 t() {
        return this.w;
    }

    @DexIgnore
    public final u53 u() {
        return this.j;
    }

    @DexIgnore
    public final e73 v() {
        b((z33) this.p);
        return this.p;
    }

    @DexIgnore
    public final ma3 w() {
        a((t63) this.l);
        return this.l;
    }

    @DexIgnore
    public final r43 x() {
        a((t63) this.m);
        return this.m;
    }

    @DexIgnore
    public final p43 y() {
        b((z33) this.s);
        return this.s;
    }

    @DexIgnore
    public final boolean z() {
        return TextUtils.isEmpty(this.b);
    }

    @DexIgnore
    public final k42 zzm() {
        return this.n;
    }

    @DexIgnore
    public static void b(s63 s63) {
        if (s63 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!s63.r()) {
            String valueOf = String.valueOf(s63.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public static void b(z33 z33) {
        if (z33 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!z33.v()) {
            String valueOf = String.valueOf(z33.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public final u53 a() {
        b((s63) this.j);
        return this.j;
    }

    @DexIgnore
    public static x53 a(Context context, String str, String str2, Bundle bundle) {
        return a(context, new mv2(0, 0, true, (String) null, (String) null, (String) null, bundle));
    }

    @DexIgnore
    public static x53 a(Context context, mv2 mv2) {
        Bundle bundle;
        if (mv2 != null && (mv2.e == null || mv2.f == null)) {
            mv2 = new mv2(mv2.a, mv2.b, mv2.c, mv2.d, (String) null, (String) null, mv2.g);
        }
        w12.a(context);
        w12.a(context.getApplicationContext());
        if (G == null) {
            synchronized (x53.class) {
                if (G == null) {
                    G = new x53(new a73(context, mv2));
                }
            }
        } else if (!(mv2 == null || (bundle = mv2.g) == null || !bundle.containsKey("dataCollectionDefaultEnabled"))) {
            G.a(mv2.g.getBoolean("dataCollectionDefaultEnabled"));
        }
        return G;
    }

    @DexIgnore
    public static void a(t63 t63) {
        if (t63 == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.A = Boolean.valueOf(z2);
    }

    @DexIgnore
    public final void a(s63 s63) {
        this.D++;
    }

    @DexIgnore
    public final void a(z33 z33) {
        this.D++;
    }

    @DexIgnore
    public final /* synthetic */ void a(String str, int i2, Throwable th, byte[] bArr, Map map) {
        List<ResolveInfo> queryIntentActivities;
        boolean z2 = true;
        if (!((i2 == 200 || i2 == 204 || i2 == 304) && th == null)) {
            b().w().a("Network Request for Deferred Deep Link failed. response, exception", Integer.valueOf(i2), th);
            return;
        }
        q().z.a(true);
        if (bArr.length == 0) {
            b().A().a("Deferred Deep Link response empty.");
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject(new String(bArr));
            String optString = jSONObject.optString("deeplink", "");
            String optString2 = jSONObject.optString("gclid", "");
            double optDouble = jSONObject.optDouble("timestamp", 0.0d);
            if (TextUtils.isEmpty(optString)) {
                b().A().a("Deferred Deep Link is empty.");
                return;
            }
            ma3 w2 = w();
            w2.e();
            if (TextUtils.isEmpty(optString) || (queryIntentActivities = w2.c().getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(optString)), 0)) == null || queryIntentActivities.isEmpty()) {
                z2 = false;
            }
            if (!z2) {
                b().w().a("Deferred Deep Link validation failed. gclid, deep link", optString2, optString);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("gclid", optString2);
            bundle.putString("_cis", "ddp");
            this.p.a("auto", "_cmp", bundle);
            ma3 w3 = w();
            if (!TextUtils.isEmpty(optString) && w3.a(optString, optDouble)) {
                w3.c().sendBroadcast(new Intent("android.google.analytics.action.DEEPLINK_ACTION"));
            }
        } catch (JSONException e2) {
            b().t().a("Failed to parse the Deferred Deep Link response. exception", e2);
        }
    }
}
