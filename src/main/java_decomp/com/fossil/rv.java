package com.fossil;

import com.fossil.fs;
import com.fossil.jv;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rv<Model> implements jv<Model, Model> {
    @DexIgnore
    public static /* final */ rv<?> a; // = new rv<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Model> implements kv<Model, Model> {
        @DexIgnore
        public static /* final */ a<?> a; // = new a<>();

        @DexIgnore
        public static <T> a<T> a() {
            return a;
        }

        @DexIgnore
        public jv<Model, Model> a(nv nvVar) {
            return rv.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model> implements fs<Model> {
        @DexIgnore
        public /* final */ Model a;

        @DexIgnore
        public b(Model model) {
            this.a = model;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(br brVar, fs.a<? super Model> aVar) {
            aVar.a(this.a);
        }

        @DexIgnore
        public pr b() {
            return pr.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<Model> getDataClass() {
            return this.a.getClass();
        }
    }

    @DexIgnore
    public static <T> rv<T> a() {
        return a;
    }

    @DexIgnore
    public boolean a(Model model) {
        return true;
    }

    @DexIgnore
    public jv.a<Model> a(Model model, int i, int i2, xr xrVar) {
        return new jv.a<>(new g00(model), new b(model));
    }
}
