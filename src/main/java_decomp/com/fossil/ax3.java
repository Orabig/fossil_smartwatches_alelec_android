package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ax3 extends IOException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -1616151763072450476L;
    @DexIgnore
    public dx3 unfinishedMessage; // = null;

    @DexIgnore
    public ax3(String str) {
        super(str);
    }

    @DexIgnore
    public static ax3 invalidEndTag() {
        return new ax3("Protocol message end-group tag did not match expected tag.");
    }

    @DexIgnore
    public static ax3 invalidTag() {
        return new ax3("Protocol message contained an invalid tag (zero).");
    }

    @DexIgnore
    public static ax3 invalidUtf8() {
        return new ax3("Protocol message had invalid UTF-8.");
    }

    @DexIgnore
    public static ax3 invalidWireType() {
        return new ax3("Protocol message tag had invalid wire type.");
    }

    @DexIgnore
    public static ax3 malformedVarint() {
        return new ax3("CodedInputStream encountered a malformed varint.");
    }

    @DexIgnore
    public static ax3 negativeSize() {
        return new ax3("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    @DexIgnore
    public static ax3 parseFailure() {
        return new ax3("Failed to parse the message.");
    }

    @DexIgnore
    public static ax3 recursionLimitExceeded() {
        return new ax3("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }

    @DexIgnore
    public static ax3 sizeLimitExceeded() {
        return new ax3("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
    }

    @DexIgnore
    public static ax3 truncatedMessage() {
        return new ax3("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    @DexIgnore
    public dx3 getUnfinishedMessage() {
        return this.unfinishedMessage;
    }

    @DexIgnore
    public ax3 setUnfinishedMessage(dx3 dx3) {
        this.unfinishedMessage = dx3;
        return this;
    }

    @DexIgnore
    public IOException unwrapIOException() {
        return getCause() instanceof IOException ? (IOException) getCause() : this;
    }

    @DexIgnore
    public ax3(IOException iOException) {
        super(iOException.getMessage(), iOException);
    }
}
