package com.fossil;

import com.fossil.bf;
import com.fossil.xe;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gf<T> extends xe<Integer, T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Value> extends ve<Integer, Value> {
        @DexIgnore
        public /* final */ gf<Value> a;

        @DexIgnore
        public a(gf<Value> gfVar) {
            this.a = gfVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void dispatchLoadInitial(Integer num, int i, int i2, boolean z, Executor executor, bf.a<Value> aVar) {
            Integer num2;
            if (num == null) {
                num2 = 0;
            } else {
                i = Math.max(i / i2, 2) * i2;
                num2 = Integer.valueOf(Math.max(0, ((num.intValue() - (i / 2)) / i2) * i2));
            }
            this.a.dispatchLoadInitial(false, num2.intValue(), i, i2, executor, aVar);
        }

        @DexIgnore
        public void addInvalidatedCallback(xe.c cVar) {
            this.a.addInvalidatedCallback(cVar);
        }

        @DexIgnore
        public void dispatchLoadAfter(int i, Value value, int i2, Executor executor, bf.a<Value> aVar) {
            this.a.dispatchLoadRange(1, i + 1, i2, executor, aVar);
        }

        @DexIgnore
        public void dispatchLoadBefore(int i, Value value, int i2, Executor executor, bf.a<Value> aVar) {
            int i3 = i - 1;
            if (i3 < 0) {
                this.a.dispatchLoadRange(2, i3, 0, executor, aVar);
                return;
            }
            int min = Math.min(i2, i3 + 1);
            this.a.dispatchLoadRange(2, (i3 - min) + 1, min, executor, aVar);
        }

        @DexIgnore
        public void invalidate() {
            this.a.invalidate();
        }

        @DexIgnore
        public boolean isInvalid() {
            return this.a.isInvalid();
        }

        @DexIgnore
        public <ToValue> xe<Integer, ToValue> map(v3<Value, ToValue> v3Var) {
            throw new UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        public <ToValue> xe<Integer, ToValue> mapByPage(v3<List<Value>, List<ToValue>> v3Var) {
            throw new UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        public void removeInvalidatedCallback(xe.c cVar) {
            this.a.removeInvalidatedCallback(cVar);
        }

        @DexIgnore
        public Integer getKey(int i, Value value) {
            return Integer.valueOf(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<T> {
        @DexIgnore
        public abstract void a(List<T> list, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T> extends b<T> {
        @DexIgnore
        public /* final */ xe.d<T> a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public c(gf gfVar, boolean z, int i, bf.a<T> aVar) {
            this.a = new xe.d<>(gfVar, 0, (Executor) null, aVar);
            this.b = z;
            this.c = i;
            if (this.c < 1) {
                throw new IllegalArgumentException("Page size must be non-negative");
            }
        }

        @DexIgnore
        public void a(List<T> list, int i, int i2) {
            if (!this.a.a()) {
                xe.d.a((List<?>) list, i, i2);
                if (list.size() + i != i2 && list.size() % this.c != 0) {
                    throw new IllegalArgumentException("PositionalDataSource requires initial load size to be a multiple of page size to support internal tiling. loadSize " + list.size() + ", position " + i + ", totalCount " + i2 + ", pageSize " + this.c);
                } else if (this.b) {
                    this.a.a(new bf(list, i, (i2 - i) - list.size(), 0));
                } else {
                    this.a.a(new bf(list, i));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public d(int i, int i2, int i3, boolean z) {
            this.a = i;
            this.b = i2;
            this.c = i3;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<T> {
        @DexIgnore
        public abstract void a(List<T> list);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<T> extends e<T> {
        @DexIgnore
        public xe.d<T> a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(gf gfVar, int i, int i2, Executor executor, bf.a<T> aVar) {
            this.a = new xe.d<>(gfVar, i, executor, aVar);
            this.b = i2;
        }

        @DexIgnore
        public void a(List<T> list) {
            if (!this.a.a()) {
                this.a.a(new bf(list, 0, 0, this.b));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public g(int i, int i2) {
            this.a = i;
            this.b = i2;
        }
    }

    @DexIgnore
    public static int computeInitialLoadPosition(d dVar, int i) {
        int i2 = dVar.a;
        int i3 = dVar.b;
        int i4 = dVar.c;
        return Math.max(0, Math.min(((((i - i3) + i4) - 1) / i4) * i4, (i2 / i4) * i4));
    }

    @DexIgnore
    public static int computeInitialLoadSize(d dVar, int i, int i2) {
        return Math.min(i2 - i, dVar.b);
    }

    @DexIgnore
    public final void dispatchLoadInitial(boolean z, int i, int i2, int i3, Executor executor, bf.a<T> aVar) {
        c cVar = new c(this, z, i3, aVar);
        loadInitial(new d(i, i2, i3, z), cVar);
        cVar.a.a(executor);
    }

    @DexIgnore
    public final void dispatchLoadRange(int i, int i2, int i3, Executor executor, bf.a<T> aVar) {
        f fVar = new f(this, i, i2, executor, aVar);
        if (i3 == 0) {
            fVar.a(Collections.emptyList());
        } else {
            loadRange(new g(i2, i3), fVar);
        }
    }

    @DexIgnore
    public boolean isContiguous() {
        return false;
    }

    @DexIgnore
    public abstract void loadInitial(d dVar, b<T> bVar);

    @DexIgnore
    public abstract void loadRange(g gVar, e<T> eVar);

    @DexIgnore
    public ve<Integer, T> wrapAsContiguousWithoutPlaceholders() {
        return new a(this);
    }

    @DexIgnore
    public final <V> gf<V> map(v3<T, V> v3Var) {
        return mapByPage(xe.createListFunction(v3Var));
    }

    @DexIgnore
    public final <V> gf<V> mapByPage(v3<List<T>, List<V>> v3Var) {
        return new lf(this, v3Var);
    }
}
