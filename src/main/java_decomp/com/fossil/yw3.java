package com.fossil;

import com.fossil.dx3;
import com.fossil.ow3;
import com.fossil.px3;
import com.fossil.xw3;
import com.fossil.yw3;
import com.fossil.yw3.b;
import com.fossil.zw3;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yw3<MessageType extends yw3<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> extends ow3<MessageType, BuilderType> {
    @DexIgnore
    public mx3 b; // = mx3.b();
    @DexIgnore
    public int c; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[px3.c.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            a[px3.c.MESSAGE.ordinal()] = 1;
            a[px3.c.ENUM.ordinal()] = 2;
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<MessageType extends yw3<MessageType, BuilderType>, BuilderType extends b<MessageType, BuilderType>> extends ow3.a<MessageType, BuilderType> {
        @DexIgnore
        public /* final */ MessageType a;
        @DexIgnore
        public MessageType b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public b(MessageType messagetype) {
            this.a = messagetype;
            this.b = (yw3) messagetype.a(j.NEW_MUTABLE_INSTANCE);
        }

        @DexIgnore
        public BuilderType b(MessageType messagetype) {
            d();
            this.b.a((k) i.a, messagetype);
            return this;
        }

        @DexIgnore
        public MessageType c() {
            if (this.c) {
                return this.b;
            }
            this.b.g();
            this.c = true;
            return this.b;
        }

        @DexIgnore
        public void d() {
            if (this.c) {
                MessageType messagetype = (yw3) this.b.a(j.NEW_MUTABLE_INSTANCE);
                messagetype.a((k) i.a, this.b);
                this.b = messagetype;
                this.c = false;
            }
        }

        @DexIgnore
        public final MessageType build() {
            MessageType c2 = c();
            if (c2.b()) {
                return c2;
            }
            throw ow3.a.b(c2);
        }

        @DexIgnore
        public BuilderType clone() {
            BuilderType h = a().h();
            h.b(c());
            return h;
        }

        @DexIgnore
        public BuilderType a(MessageType messagetype) {
            return b(messagetype);
        }

        @DexIgnore
        public MessageType a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T extends yw3<T, ?>> extends pw3<T> {
        @DexIgnore
        public T a;

        @DexIgnore
        public c(T t) {
            this.a = t;
        }

        @DexIgnore
        public T a(tw3 tw3, ww3 ww3) throws ax3 {
            return yw3.a(this.a, tw3, ww3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements k {
        @DexIgnore
        public static /* final */ d a; // = new d();
        @DexIgnore
        public static /* final */ a b; // = new a();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends RuntimeException {
        }

        @DexIgnore
        public int a(boolean z, int i, boolean z2, int i2) {
            if (z == z2 && i == i2) {
                return i;
            }
            throw b;
        }

        @DexIgnore
        public Object b(boolean z, Object obj, Object obj2) {
            if (z && obj.equals(obj2)) {
                return obj;
            }
            throw b;
        }

        @DexIgnore
        public long a(boolean z, long j, boolean z2, long j2) {
            if (z == z2 && j == j2) {
                return j;
            }
            throw b;
        }

        @DexIgnore
        public String a(boolean z, String str, boolean z2, String str2) {
            if (z == z2 && str.equals(str2)) {
                return str;
            }
            throw b;
        }

        @DexIgnore
        public sw3 a(boolean z, sw3 sw3, boolean z2, sw3 sw32) {
            if (z == z2 && sw3.equals(sw32)) {
                return sw3;
            }
            throw b;
        }

        @DexIgnore
        public Object a(boolean z, Object obj, Object obj2) {
            if (z && obj.equals(obj2)) {
                return obj;
            }
            throw b;
        }

        @DexIgnore
        public void a(boolean z) {
            if (z) {
                throw b;
            }
        }

        @DexIgnore
        public <T extends dx3> T a(T t, T t2) {
            if (t == null && t2 == null) {
                return null;
            }
            if (t == null || t2 == null) {
                throw b;
            }
            ((yw3) t).a(this, (dx3) t2);
            return t;
        }

        @DexIgnore
        public <T> zw3.c<T> a(zw3.c<T> cVar, zw3.c<T> cVar2) {
            if (cVar.equals(cVar2)) {
                return cVar;
            }
            throw b;
        }

        @DexIgnore
        public xw3<g> a(xw3<g> xw3, xw3<g> xw32) {
            if (xw3.equals(xw32)) {
                return xw3;
            }
            throw b;
        }

        @DexIgnore
        public mx3 a(mx3 mx3, mx3 mx32) {
            if (mx3.equals(mx32)) {
                return mx3;
            }
            throw b;
        }
    }

    @DexIgnore
    public interface f<MessageType extends e<MessageType, BuilderType>, BuilderType extends Object<MessageType, BuilderType>> extends ex3 {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements xw3.b<g> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ px3.b b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public int a() {
            return this.a;
        }

        @DexIgnore
        public boolean q() {
            return this.c;
        }

        @DexIgnore
        public px3.b r() {
            return this.b;
        }

        @DexIgnore
        public px3.c w() {
            return this.b.getJavaType();
        }

        @DexIgnore
        public dx3.a a(dx3.a aVar, dx3 dx3) {
            return ((b) aVar).b((yw3) dx3);
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(g gVar) {
            return this.a - gVar.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i implements k {
        @DexIgnore
        public static /* final */ i a; // = new i();

        @DexIgnore
        public int a(boolean z, int i, boolean z2, int i2) {
            return z2 ? i2 : i;
        }

        @DexIgnore
        public long a(boolean z, long j, boolean z2, long j2) {
            return z2 ? j2 : j;
        }

        @DexIgnore
        public <T extends dx3> T a(T t, T t2) {
            if (t == null || t2 == null) {
                return t != null ? t : t2;
            }
            return t.c().a(t2).build();
        }

        @DexIgnore
        public sw3 a(boolean z, sw3 sw3, boolean z2, sw3 sw32) {
            return z2 ? sw32 : sw3;
        }

        @DexIgnore
        public Object a(boolean z, Object obj, Object obj2) {
            return obj2;
        }

        @DexIgnore
        public String a(boolean z, String str, boolean z2, String str2) {
            return z2 ? str2 : str;
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public Object b(boolean z, Object obj, Object obj2) {
            return obj2;
        }

        @DexIgnore
        public <T> zw3.c<T> a(zw3.c<T> cVar, zw3.c<T> cVar2) {
            int size = cVar.size();
            int size2 = cVar2.size();
            if (size > 0 && size2 > 0) {
                if (!cVar.m()) {
                    cVar = cVar.b(size2 + size);
                }
                cVar.addAll(cVar2);
            }
            return size > 0 ? cVar : cVar2;
        }

        @DexIgnore
        public xw3<g> a(xw3<g> xw3, xw3<g> xw32) {
            if (xw3.a()) {
                xw3 = xw3.clone();
            }
            xw3.a(xw32);
            return xw3;
        }

        @DexIgnore
        public mx3 a(mx3 mx3, mx3 mx32) {
            return mx32 == mx3.b() ? mx3 : mx3.a(mx3, mx32);
        }
    }

    @DexIgnore
    public enum j {
        IS_INITIALIZED,
        VISIT,
        MERGE_FROM_STREAM,
        MAKE_IMMUTABLE,
        NEW_MUTABLE_INSTANCE,
        NEW_BUILDER,
        GET_DEFAULT_INSTANCE,
        GET_PARSER
    }

    @DexIgnore
    public interface k {
        @DexIgnore
        int a(boolean z, int i, boolean z2, int i2);

        @DexIgnore
        long a(boolean z, long j, boolean z2, long j2);

        @DexIgnore
        <T extends dx3> T a(T t, T t2);

        @DexIgnore
        mx3 a(mx3 mx3, mx3 mx32);

        @DexIgnore
        sw3 a(boolean z, sw3 sw3, boolean z2, sw3 sw32);

        @DexIgnore
        xw3<g> a(xw3<g> xw3, xw3<g> xw32);

        @DexIgnore
        <T> zw3.c<T> a(zw3.c<T> cVar, zw3.c<T> cVar2);

        @DexIgnore
        Object a(boolean z, Object obj, Object obj2);

        @DexIgnore
        String a(boolean z, String str, boolean z2, String str2);

        @DexIgnore
        void a(boolean z);

        @DexIgnore
        Object b(boolean z, Object obj, Object obj2);
    }

    @DexIgnore
    public static <E> zw3.c<E> i() {
        return hx3.b();
    }

    @DexIgnore
    public abstract Object a(j jVar, Object obj, Object obj2);

    @DexIgnore
    public final boolean b() {
        return a(j.IS_INITIALIZED, (Object) Boolean.TRUE) != null;
    }

    @DexIgnore
    public final gx3<MessageType> e() {
        return (gx3) a(j.GET_PARSER);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!a().getClass().isInstance(obj)) {
            return false;
        }
        try {
            a((k) d.a, (yw3) obj);
            return true;
        } catch (d.a unused) {
            return false;
        }
    }

    @DexIgnore
    public void g() {
        a(j.MAKE_IMMUTABLE);
        this.b.a();
    }

    @DexIgnore
    public final BuilderType h() {
        return (b) a(j.NEW_BUILDER);
    }

    @DexIgnore
    public int hashCode() {
        if (this.a == 0) {
            h hVar = new h((a) null);
            a((k) hVar, this);
            this.a = hVar.a;
        }
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return fx3.a(this, super.toString());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<MessageType extends e<MessageType, BuilderType>, BuilderType extends Object<MessageType, BuilderType>> extends yw3<MessageType, BuilderType> implements f<MessageType, BuilderType> {
        @DexIgnore
        public xw3<g> d; // = xw3.d();

        @DexIgnore
        public /* bridge */ /* synthetic */ dx3 a() {
            return yw3.super.a();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ dx3.a c() {
            return yw3.super.c();
        }

        @DexIgnore
        public final void g() {
            yw3.super.g();
            this.d.c();
        }

        @DexIgnore
        public final void a(k kVar, MessageType messagetype) {
            yw3.super.a(kVar, messagetype);
            this.d = kVar.a(this.d, messagetype.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h implements k {
        @DexIgnore
        public int a;

        @DexIgnore
        public h() {
            this.a = 0;
        }

        @DexIgnore
        public Object b(boolean z, Object obj, Object obj2) {
            this.a = (this.a * 53) + obj.hashCode();
            return obj;
        }

        @DexIgnore
        public /* synthetic */ h(a aVar) {
            this();
        }

        @DexIgnore
        public int a(boolean z, int i, boolean z2, int i2) {
            this.a = (this.a * 53) + i;
            return i;
        }

        @DexIgnore
        public long a(boolean z, long j, boolean z2, long j2) {
            this.a = (this.a * 53) + zw3.a(j);
            return j;
        }

        @DexIgnore
        public String a(boolean z, String str, boolean z2, String str2) {
            this.a = (this.a * 53) + str.hashCode();
            return str;
        }

        @DexIgnore
        public sw3 a(boolean z, sw3 sw3, boolean z2, sw3 sw32) {
            this.a = (this.a * 53) + sw3.hashCode();
            return sw3;
        }

        @DexIgnore
        public Object a(boolean z, Object obj, Object obj2) {
            this.a = (this.a * 53) + ((Integer) obj).intValue();
            return obj;
        }

        @DexIgnore
        public void a(boolean z) {
            if (z) {
                throw new IllegalStateException();
            }
        }

        @DexIgnore
        public <T extends dx3> T a(T t, T t2) {
            int i;
            if (t != null) {
                i = t instanceof yw3 ? ((yw3) t).a(this) : t.hashCode();
            } else {
                i = 37;
            }
            this.a = (this.a * 53) + i;
            return t;
        }

        @DexIgnore
        public <T> zw3.c<T> a(zw3.c<T> cVar, zw3.c<T> cVar2) {
            this.a = (this.a * 53) + cVar.hashCode();
            return cVar;
        }

        @DexIgnore
        public xw3<g> a(xw3<g> xw3, xw3<g> xw32) {
            this.a = (this.a * 53) + xw3.hashCode();
            return xw3;
        }

        @DexIgnore
        public mx3 a(mx3 mx3, mx3 mx32) {
            this.a = (this.a * 53) + mx3.hashCode();
            return mx3;
        }
    }

    @DexIgnore
    public final MessageType a() {
        return (yw3) a(j.GET_DEFAULT_INSTANCE);
    }

    @DexIgnore
    public final BuilderType c() {
        BuilderType buildertype = (b) a(j.NEW_BUILDER);
        buildertype.b(this);
        return buildertype;
    }

    @DexIgnore
    public int a(h hVar) {
        if (this.a == 0) {
            int a2 = hVar.a;
            int unused = hVar.a = 0;
            a((k) hVar, this);
            this.a = hVar.a;
            int unused2 = hVar.a = a2;
        }
        return this.a;
    }

    @DexIgnore
    public boolean a(d dVar, dx3 dx3) {
        if (this == dx3) {
            return true;
        }
        if (!a().getClass().isInstance(dx3)) {
            return false;
        }
        a((k) dVar, (yw3) dx3);
        return true;
    }

    @DexIgnore
    public Object a(j jVar, Object obj) {
        return a(jVar, obj, (Object) null);
    }

    @DexIgnore
    public Object a(j jVar) {
        return a(jVar, (Object) null, (Object) null);
    }

    @DexIgnore
    public void a(k kVar, MessageType messagetype) {
        a(j.VISIT, (Object) kVar, (Object) messagetype);
        this.b = kVar.a(this.b, messagetype.b);
    }

    @DexIgnore
    public static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    @DexIgnore
    public static <E> zw3.c<E> a(zw3.c<E> cVar) {
        int size = cVar.size();
        return cVar.b(size == 0 ? 10 : size * 2);
    }

    @DexIgnore
    public static <T extends yw3<T, ?>> T a(T t, tw3 tw3, ww3 ww3) throws ax3 {
        T t2 = (yw3) t.a(j.NEW_MUTABLE_INSTANCE);
        try {
            t2.a(j.MERGE_FROM_STREAM, (Object) tw3, (Object) ww3);
            t2.g();
            return t2;
        } catch (RuntimeException e2) {
            if (e2.getCause() instanceof ax3) {
                throw ((ax3) e2.getCause());
            }
            throw e2;
        }
    }

    @DexIgnore
    public static <T extends yw3<T, ?>> T a(T t) throws ax3 {
        if (t == null || t.b()) {
            return t;
        }
        throw t.f().asInvalidProtocolBufferException().setUnfinishedMessage(t);
    }

    @DexIgnore
    public static <T extends yw3<T, ?>> T a(T t, InputStream inputStream) throws ax3 {
        T a2 = a(t, tw3.a(inputStream), ww3.a());
        a(a2);
        return a2;
    }
}
