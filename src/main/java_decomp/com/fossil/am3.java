package com.fossil;

import com.fossil.bm3;
import com.fossil.gm3;
import com.fossil.zl3;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class am3<K, V> extends gm3<K, V> implements tm3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public transient am3<V, K> f;

    @DexIgnore
    public am3(bm3<K, zl3<V>> bm3, int i) {
        super(bm3, i);
    }

    @DexIgnore
    public static <K, V> a<K, V> builder() {
        return new a<>();
    }

    @DexIgnore
    public static <K, V> am3<K, V> copyOf(zm3<? extends K, ? extends V> zm3) {
        if (zm3.isEmpty()) {
            return of();
        }
        if (zm3 instanceof am3) {
            am3<K, V> am3 = (am3) zm3;
            if (!am3.isPartialView()) {
                return am3;
            }
        }
        bm3.b bVar = new bm3.b(zm3.asMap().size());
        int i = 0;
        for (Map.Entry next : zm3.asMap().entrySet()) {
            zl3 copyOf = zl3.copyOf((Collection) next.getValue());
            if (!copyOf.isEmpty()) {
                bVar.a(next.getKey(), copyOf);
                i += copyOf.size();
            }
        }
        return new am3<>(bVar.a(), i);
    }

    @DexIgnore
    public static <K, V> am3<K, V> of() {
        return hl3.INSTANCE;
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            bm3.b builder = bm3.builder();
            int i = 0;
            int i2 = 0;
            while (i < readInt) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    zl3.b builder2 = zl3.builder();
                    for (int i3 = 0; i3 < readInt2; i3++) {
                        builder2.a(objectInputStream.readObject());
                    }
                    builder.a(readObject, builder2.a());
                    i2 += readInt2;
                    i++;
                } else {
                    throw new InvalidObjectException("Invalid value count " + readInt2);
                }
            }
            try {
                gm3.e.a.a(this, (Object) builder.a());
                gm3.e.b.a(this, i2);
            } catch (IllegalArgumentException e) {
                throw ((InvalidObjectException) new InvalidObjectException(e.getMessage()).initCause(e));
            }
        } else {
            throw new InvalidObjectException("Invalid key count " + readInt);
        }
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        wn3.a(this, objectOutputStream);
    }

    @DexIgnore
    public final am3<V, K> a() {
        a builder = builder();
        Iterator it = entries().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            builder.a(entry.getValue(), entry.getKey());
        }
        am3<V, K> a2 = builder.a();
        a2.f = this;
        return a2;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends gm3.c<K, V> {
        @DexIgnore
        public a<K, V> a(K k, V v) {
            super.a(k, v);
            return this;
        }

        @DexIgnore
        public a<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            super.a(entry);
            return this;
        }

        @DexIgnore
        public a<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.a(iterable);
            return this;
        }

        @DexIgnore
        public am3<K, V> a() {
            return (am3) super.a();
        }
    }

    @DexIgnore
    public static <K, V> am3<K, V> of(K k, V v) {
        a builder = builder();
        builder.a(k, v);
        return builder.a();
    }

    @DexIgnore
    public am3<V, K> inverse() {
        am3<V, K> am3 = this.f;
        if (am3 != null) {
            return am3;
        }
        am3<V, K> a2 = a();
        this.f = a2;
        return a2;
    }

    @DexIgnore
    public zl3<V> get(K k) {
        zl3<V> zl3 = (zl3) this.map.get(k);
        return zl3 == null ? zl3.of() : zl3;
    }

    @DexIgnore
    @Deprecated
    public zl3<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public zl3<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public static <K, V> am3<K, V> of(K k, V v, K k2, V v2) {
        a builder = builder();
        builder.a(k, v);
        builder.a(k2, v2);
        return builder.a();
    }

    @DexIgnore
    public static <K, V> am3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        a builder = builder();
        builder.a(k, v);
        builder.a(k2, v2);
        builder.a(k3, v3);
        return builder.a();
    }

    @DexIgnore
    public static <K, V> am3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        a aVar = new a();
        aVar.a(iterable);
        return aVar.a();
    }

    @DexIgnore
    public static <K, V> am3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        a builder = builder();
        builder.a(k, v);
        builder.a(k2, v2);
        builder.a(k3, v3);
        builder.a(k4, v4);
        return builder.a();
    }

    @DexIgnore
    public static <K, V> am3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        a builder = builder();
        builder.a(k, v);
        builder.a(k2, v2);
        builder.a(k3, v3);
        builder.a(k4, v4);
        builder.a(k5, v5);
        return builder.a();
    }
}
