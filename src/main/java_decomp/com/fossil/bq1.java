package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq1 extends gq1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ zs1 b;
    @DexIgnore
    public /* final */ zs1 c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public bq1(Context context, zs1 zs1, zs1 zs12, String str) {
        if (context != null) {
            this.a = context;
            if (zs1 != null) {
                this.b = zs1;
                if (zs12 != null) {
                    this.c = zs12;
                    if (str != null) {
                        this.d = str;
                        return;
                    }
                    throw new NullPointerException("Null backendName");
                }
                throw new NullPointerException("Null monotonicClock");
            }
            throw new NullPointerException("Null wallClock");
        }
        throw new NullPointerException("Null applicationContext");
    }

    @DexIgnore
    public Context a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.d;
    }

    @DexIgnore
    public zs1 c() {
        return this.c;
    }

    @DexIgnore
    public zs1 d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gq1)) {
            return false;
        }
        gq1 gq1 = (gq1) obj;
        if (!this.a.equals(gq1.a()) || !this.b.equals(gq1.d()) || !this.c.equals(gq1.c()) || !this.d.equals(gq1.b())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CreationContext{applicationContext=" + this.a + ", wallClock=" + this.b + ", monotonicClock=" + this.c + ", backendName=" + this.d + "}";
    }
}
