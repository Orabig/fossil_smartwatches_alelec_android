package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Parcelable;
import android.view.View;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lk4 {
    @DexIgnore
    public static /* final */ String a; // = "lk4";

    @DexIgnore
    public static Uri a(Context context) {
        File externalCacheDir = context.getExternalCacheDir();
        if (externalCacheDir != null) {
            return Uri.fromFile(new File(externalCacheDir.getPath(), "pickerImage.jpg"));
        }
        return null;
    }

    @DexIgnore
    public static Intent b(Context context) {
        Uri a2 = a(context);
        ArrayList arrayList = new ArrayList();
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        for (ResolveInfo next : packageManager.queryIntentActivities(intent, 0)) {
            Intent intent2 = new Intent(intent);
            intent2.setComponent(new ComponentName(next.activityInfo.packageName, next.activityInfo.name));
            intent2.setPackage(next.activityInfo.packageName);
            if (a2 != null) {
                intent2.putExtra("output", a2);
            }
            arrayList.add(intent2);
        }
        Intent intent3 = new Intent("android.intent.action.GET_CONTENT");
        intent3.setType("image/*");
        for (ResolveInfo next2 : packageManager.queryIntentActivities(intent3, 0)) {
            Intent intent4 = new Intent(intent3);
            intent4.setComponent(new ComponentName(next2.activityInfo.packageName, next2.activityInfo.name));
            intent4.setPackage(next2.activityInfo.packageName);
            arrayList.add(intent4);
        }
        Intent intent5 = (Intent) arrayList.get(arrayList.size() - 1);
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Intent intent6 = (Intent) it.next();
            if (intent6.getComponent() != null && "com.android.documentsui.DocumentsActivity".equalsIgnoreCase(intent6.getComponent().getClassName())) {
                intent5 = intent6;
                break;
            }
        }
        arrayList.remove(intent5);
        Intent createChooser = Intent.createChooser(intent5, context.getResources().getString(2131887072));
        createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) arrayList.toArray(new Parcelable[arrayList.size()]));
        return createChooser;
    }

    @DexIgnore
    public static Bitmap a(View view) {
        view.setDrawingCacheEnabled(true);
        view.destroyDrawingCache();
        view.buildDrawingCache();
        return view.getDrawingCache();
    }

    @DexIgnore
    public static Intent a(Context context, PackageManager packageManager) {
        ArrayList arrayList = new ArrayList();
        Uri a2 = a(context);
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        for (ResolveInfo next : packageManager.queryIntentActivities(intent, 0)) {
            Intent intent2 = new Intent(intent);
            intent2.setComponent(new ComponentName(next.activityInfo.packageName, next.activityInfo.name));
            intent2.setPackage(next.activityInfo.packageName);
            if (a2 != null) {
                intent2.putExtra("output", a2);
            }
            arrayList.add(intent2);
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        Intent intent3 = (Intent) arrayList.get(arrayList.size() - 1);
        arrayList.remove(arrayList.size() - 1);
        Intent createChooser = Intent.createChooser(intent3, "");
        createChooser.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) arrayList.toArray(new Parcelable[arrayList.size()]));
        intent3.putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) arrayList.toArray(new Parcelable[arrayList.size()]));
        return createChooser;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003d, code lost:
        if (r2.equalsIgnoreCase("inline-data") == false) goto L_0x0040;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    public static Uri a(Intent intent, Context context) {
        boolean z = false;
        if (intent != null) {
            if (intent.getData() == null) {
                String action = intent.getAction();
                if (action != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = a;
                    local.d(str, "Action is " + action);
                    if (!action.equals("android.media.action.IMAGE_CAPTURE")) {
                    }
                }
            }
            return !z ? a(context) : intent.getData();
        }
        z = true;
        if (!z) {
        }
    }

    @DexIgnore
    public static Bitmap a(String str) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(str, options);
    }

    @DexIgnore
    public static int a(BitmapFactory.Options options, int i, int i2) {
        int i3 = options.outHeight;
        int i4 = options.outWidth;
        int i5 = 1;
        if (i3 > i2 || i4 > i) {
            int i6 = i3 / 2;
            int i7 = i4 / 2;
            while (i6 / i5 >= i2 && i7 / i5 >= i) {
                i5 *= 2;
            }
        }
        return i5;
    }

    @DexIgnore
    public static Bitmap a(InputStream inputStream, int i, int i2) {
        FLogger.INSTANCE.getLocal().d(a, "fastReadingBitmapFromAssets");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = inputStream.read(bArr);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                    options.inSampleSize = a(options, i, i2);
                    options.inJustDecodeBounds = false;
                    Bitmap decodeByteArray = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                    try {
                        byteArrayOutputStream.close();
                        return decodeByteArray;
                    } catch (Exception e) {
                        e.printStackTrace();
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = a;
                        local.e(str, "fastReadingBitmapFromAssets - os close with e=" + e);
                        return decodeByteArray;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a;
            local2.e(str2, "fastReadingBitmapFromAssets - e=" + e2);
            try {
                byteArrayOutputStream.close();
            } catch (Exception e3) {
                e3.printStackTrace();
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = a;
                local3.e(str3, "fastReadingBitmapFromAssets - os close with e=" + e3);
            }
            return null;
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
            } catch (Exception e4) {
                e4.printStackTrace();
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str4 = a;
                local4.e(str4, "fastReadingBitmapFromAssets - os close with e=" + e4);
            }
            throw th;
        }
    }
}
