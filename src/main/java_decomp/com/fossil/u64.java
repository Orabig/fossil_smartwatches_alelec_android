package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u64 extends t64 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j L; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray M; // = new SparseIntArray();
    @DexIgnore
    public long K;

    /*
    static {
        M.put(2131362082, 1);
        M.put(2131362542, 2);
        M.put(2131363218, 3);
        M.put(2131362056, 4);
        M.put(2131362543, 5);
        M.put(2131362321, 6);
        M.put(2131362320, 7);
        M.put(2131362653, 8);
        M.put(2131362318, 9);
        M.put(2131362316, 10);
        M.put(2131362342, 11);
        M.put(2131362609, 12);
        M.put(2131362122, 13);
        M.put(2131361889, 14);
        M.put(2131362022, 15);
        M.put(2131362058, 16);
        M.put(2131362780, 17);
        M.put(2131362407, 18);
        M.put(2131362357, 19);
        M.put(2131362149, 20);
        M.put(2131362665, 21);
        M.put(2131363264, 22);
        M.put(2131362399, 23);
        M.put(2131362863, 24);
    }
    */

    @DexIgnore
    public u64(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 25, L, M));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.K = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.K != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.K = 1;
        }
        g();
    }

    @DexIgnore
    public u64(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[14], objArr[15], objArr[4], objArr[16], objArr[1], objArr[13], objArr[20], objArr[10], objArr[9], objArr[7], objArr[6], objArr[11], objArr[19], objArr[23], objArr[18], objArr[2], objArr[5], objArr[12], objArr[8], objArr[21], objArr[17], objArr[0], objArr[24], objArr[3], objArr[22]);
        this.K = -1;
        this.I.setTag((Object) null);
        a(view);
        f();
    }
}
