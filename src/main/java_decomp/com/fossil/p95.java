package com.fossil;

import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p95 implements Factory<o95> {
    @DexIgnore
    public static o95 a(k95 k95, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, an4 an4) {
        return new o95(k95, setHybridPresetToWatchUseCase, an4);
    }
}
