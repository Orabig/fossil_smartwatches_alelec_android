package com.fossil;

import com.fossil.ys1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ir1 implements ys1.a {
    @DexIgnore
    public /* final */ lr1 a;
    @DexIgnore
    public /* final */ fq1 b;
    @DexIgnore
    public /* final */ Iterable c;
    @DexIgnore
    public /* final */ rp1 d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public ir1(lr1 lr1, fq1 fq1, Iterable iterable, rp1 rp1, int i) {
        this.a = lr1;
        this.b = fq1;
        this.c = iterable;
        this.d = rp1;
        this.e = i;
    }

    @DexIgnore
    public static ys1.a a(lr1 lr1, fq1 fq1, Iterable iterable, rp1 rp1, int i) {
        return new ir1(lr1, fq1, iterable, rp1, i);
    }

    @DexIgnore
    public Object s() {
        return lr1.a(this.a, this.b, this.c, this.d, this.e);
    }
}
