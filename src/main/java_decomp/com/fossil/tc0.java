package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tc0 extends p40 implements Parcelable {
    @DexIgnore
    public /* final */ x90 a;
    @DexIgnore
    public /* final */ uc0 b;

    @DexIgnore
    public tc0(x90 x90, uc0 uc0) {
        this.a = x90;
        this.b = uc0;
    }

    @DexIgnore
    public final JSONObject a() {
        Object obj;
        Object obj2;
        JSONObject jSONObject = new JSONObject();
        try {
            bm0 bm0 = bm0.REQUEST;
            x90 x90 = this.a;
            if (x90 == null || (obj = x90.a()) == null) {
                obj = JSONObject.NULL;
            }
            cw0.a(jSONObject, bm0, obj);
            bm0 bm02 = bm0.RESPONSE;
            uc0 uc0 = this.b;
            if (uc0 == null || (obj2 = uc0.a()) == null) {
                obj2 = JSONObject.NULL;
            }
            cw0.a(jSONObject, bm02, obj2);
            cw0.a(jSONObject, bm0.DATA, (Object) b());
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public abstract byte[] a(short s, w40 w40);

    @DexIgnore
    public JSONObject b() {
        return new JSONObject();
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            tc0 tc0 = (tc0) obj;
            return !(wg6.a(this.a, tc0.a) ^ true) && !(wg6.a(this.b, tc0.b) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.DeviceData");
    }

    @DexIgnore
    public final uc0 getDeviceMessage() {
        return this.b;
    }

    @DexIgnore
    public final x90 getDeviceRequest() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        x90 x90 = this.a;
        int i = 0;
        int hashCode = x90 != null ? x90.hashCode() : 0;
        uc0 uc0 = this.b;
        if (uc0 != null) {
            i = uc0.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.a, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
    }
}
