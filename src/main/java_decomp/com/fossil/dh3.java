package com.fossil;

import android.os.Build;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dh3 {
    @DexIgnore
    public static String a(long j) {
        return a(j, Locale.getDefault());
    }

    @DexIgnore
    public static String b(long j) {
        return b(j, Locale.getDefault());
    }

    @DexIgnore
    public static String a(long j, Locale locale) {
        if (Build.VERSION.SDK_INT >= 24) {
            return nh3.a(locale).format(new Date(j));
        }
        return nh3.b(locale).format(new Date(j));
    }

    @DexIgnore
    public static String b(long j, Locale locale) {
        if (Build.VERSION.SDK_INT >= 24) {
            return nh3.c(locale).format(new Date(j));
        }
        return nh3.b(locale).format(new Date(j));
    }
}
