package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a46 extends v36 {
    @DexIgnore
    public d56 m;
    @DexIgnore
    public JSONObject n; // = null;

    @DexIgnore
    public a46(Context context, int i, JSONObject jSONObject, r36 r36) {
        super(context, i, r36);
        this.m = new d56(context);
        this.n = jSONObject;
    }

    @DexIgnore
    public w36 a() {
        return w36.SESSION_ENV;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        c56 c56 = this.d;
        if (c56 != null) {
            jSONObject.put("ut", c56.d());
        }
        JSONObject jSONObject2 = this.n;
        if (jSONObject2 != null) {
            jSONObject.put("cfg", jSONObject2);
        }
        if (m56.B(this.j)) {
            jSONObject.put("ncts", 1);
        }
        this.m.a(jSONObject, (Thread) null);
        return true;
    }
}
