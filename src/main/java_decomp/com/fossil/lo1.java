package com.fossil;

import java.nio.charset.Charset;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo1 implements mp1 {
    @DexIgnore
    public static /* final */ String c; // = hp1.a("hts/frbslgiggolai.o/0clgbth", "tp:/ieaeogn.ogepscmvc/o/ac");
    @DexIgnore
    public static /* final */ String d; // = hp1.a("hts/frbslgigp.ogepscmv/ieo/eaybtho", "tp:/ieaeogn-agolai.o/1frlglgc/aclg");
    @DexIgnore
    public static /* final */ String e; // = hp1.a("AzSCki82AwsLzKd5O8zo", "IayckHiZRO1EFl1aGoK");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    /*
    static {
        new lo1(c, (String) null);
        new lo1(d, e);
    }
    */

    @DexIgnore
    public lo1(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public static lo1 a(byte[] bArr) {
        String str = new String(bArr, Charset.forName("UTF-8"));
        if (str.startsWith("1$")) {
            String[] split = str.substring(2).split(Pattern.quote("\\"), 2);
            if (split.length == 2) {
                String str2 = split[0];
                if (!str2.isEmpty()) {
                    String str3 = split[1];
                    if (str3.isEmpty()) {
                        str3 = null;
                    }
                    return new lo1(str2, str3);
                }
                throw new IllegalArgumentException("Missing endpoint in CCTDestination extras");
            }
            throw new IllegalArgumentException("Extra is not a valid encoded LegacyFlgDestination");
        }
        throw new IllegalArgumentException("Version marker missing from extras");
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }
}
