package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ew6 extends hw6 implements xv6 {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 9044267456635152283L;

    @DexIgnore
    public void debug(zv6 zv6, String str) {
        debug(str);
    }

    @DexIgnore
    public void error(zv6 zv6, String str) {
        error(str);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ String getName() {
        return super.getName();
    }

    @DexIgnore
    public void info(zv6 zv6, String str) {
        info(str);
    }

    @DexIgnore
    public boolean isDebugEnabled(zv6 zv6) {
        return isDebugEnabled();
    }

    @DexIgnore
    public boolean isErrorEnabled(zv6 zv6) {
        return isErrorEnabled();
    }

    @DexIgnore
    public boolean isInfoEnabled(zv6 zv6) {
        return isInfoEnabled();
    }

    @DexIgnore
    public boolean isTraceEnabled(zv6 zv6) {
        return isTraceEnabled();
    }

    @DexIgnore
    public boolean isWarnEnabled(zv6 zv6) {
        return isWarnEnabled();
    }

    @DexIgnore
    public String toString() {
        return getClass().getName() + "(" + getName() + ")";
    }

    @DexIgnore
    public void trace(zv6 zv6, String str) {
        trace(str);
    }

    @DexIgnore
    public void warn(zv6 zv6, String str) {
        warn(str);
    }

    @DexIgnore
    public void debug(zv6 zv6, String str, Object obj) {
        debug(str, obj);
    }

    @DexIgnore
    public void error(zv6 zv6, String str, Object obj) {
        error(str, obj);
    }

    @DexIgnore
    public void info(zv6 zv6, String str, Object obj) {
        info(str, obj);
    }

    @DexIgnore
    public void trace(zv6 zv6, String str, Object obj) {
        trace(str, obj);
    }

    @DexIgnore
    public void warn(zv6 zv6, String str, Object obj) {
        warn(str, obj);
    }

    @DexIgnore
    public void debug(zv6 zv6, String str, Object obj, Object obj2) {
        debug(str, obj, obj2);
    }

    @DexIgnore
    public void error(zv6 zv6, String str, Object obj, Object obj2) {
        error(str, obj, obj2);
    }

    @DexIgnore
    public void info(zv6 zv6, String str, Object obj, Object obj2) {
        info(str, obj, obj2);
    }

    @DexIgnore
    public void trace(zv6 zv6, String str, Object obj, Object obj2) {
        trace(str, obj, obj2);
    }

    @DexIgnore
    public void warn(zv6 zv6, String str, Object obj, Object obj2) {
        warn(str, obj, obj2);
    }

    @DexIgnore
    public void debug(zv6 zv6, String str, Object... objArr) {
        debug(str, objArr);
    }

    @DexIgnore
    public void error(zv6 zv6, String str, Object... objArr) {
        error(str, objArr);
    }

    @DexIgnore
    public void info(zv6 zv6, String str, Object... objArr) {
        info(str, objArr);
    }

    @DexIgnore
    public void trace(zv6 zv6, String str, Object... objArr) {
        trace(str, objArr);
    }

    @DexIgnore
    public void warn(zv6 zv6, String str, Object... objArr) {
        warn(str, objArr);
    }

    @DexIgnore
    public void debug(zv6 zv6, String str, Throwable th) {
        debug(str, th);
    }

    @DexIgnore
    public void error(zv6 zv6, String str, Throwable th) {
        error(str, th);
    }

    @DexIgnore
    public void info(zv6 zv6, String str, Throwable th) {
        info(str, th);
    }

    @DexIgnore
    public void trace(zv6 zv6, String str, Throwable th) {
        trace(str, th);
    }

    @DexIgnore
    public void warn(zv6 zv6, String str, Throwable th) {
        warn(str, th);
    }
}
