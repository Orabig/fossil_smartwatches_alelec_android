package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d01 {
    @DexIgnore
    public /* synthetic */ d01(qg6 qg6) {
    }

    @DexIgnore
    public final x11 a(int i) {
        if (i == 0) {
            return x11.SUCCESS;
        }
        if (i == ao0.BLUETOOTH_OFF.a) {
            return x11.BLUETOOTH_OFF;
        }
        if (i == ao0.START_FAIL.a) {
            return x11.START_FAIL;
        }
        if (i == ao0.HID_PROXY_NOT_CONNECTED.a) {
            return x11.HID_PROXY_NOT_CONNECTED;
        }
        if (i == ao0.HID_FAIL_TO_INVOKE_PRIVATE_METHOD.a) {
            return x11.HID_FAIL_TO_INVOKE_PRIVATE_METHOD;
        }
        if (i == ao0.HID_INPUT_DEVICE_DISABLED.a) {
            return x11.HID_INPUT_DEVICE_DISABLED;
        }
        if (i == ao0.HID_UNKNOWN_ERROR.a) {
            return x11.HID_UNKNOWN_ERROR;
        }
        return x11.GATT_ERROR;
    }
}
