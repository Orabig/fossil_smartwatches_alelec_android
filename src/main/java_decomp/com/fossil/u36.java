package com.fossil;

import android.content.Context;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u36 extends v36 {
    @DexIgnore
    public String m;
    @DexIgnore
    public int n;
    @DexIgnore
    public Thread o; // = null;

    @DexIgnore
    public u36(Context context, int i, int i2, Throwable th, r36 r36) {
        super(context, i, r36);
        a(i2, th);
    }

    @DexIgnore
    public u36(Context context, int i, int i2, Throwable th, Thread thread, r36 r36) {
        super(context, i, r36);
        a(i2, th);
        this.o = thread;
    }

    @DexIgnore
    public w36 a() {
        return w36.ERROR;
    }

    @DexIgnore
    public final void a(int i, Throwable th) {
        if (th != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            this.m = stringWriter.toString();
            this.n = i;
            printWriter.close();
        }
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        r56.a(jSONObject, "er", this.m);
        jSONObject.put("ea", this.n);
        int i = this.n;
        if (i != 2 && i != 3) {
            return true;
        }
        new d56(this.j).a(jSONObject, this.o);
        return true;
    }
}
