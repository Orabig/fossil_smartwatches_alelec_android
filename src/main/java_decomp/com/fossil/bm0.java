package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum bm0 {
    DEVICE_INFO,
    RSSI,
    NOTIFICATION,
    TYPE,
    UID,
    APP_NAME,
    g,
    SENDER,
    SENDER_ID,
    MESSAGE,
    FLAGS,
    RECEIVED_TIMESTAMP_IN_SECOND,
    REPLY_MESSAGES,
    REQUEST,
    RESPONSE,
    REQUEST_ID,
    CHANCE_OF_RAIN,
    DESTINATION,
    WEATHER_CONDITION,
    TEMP_UNIT,
    TRACK_INFO,
    VOLUME,
    ARTIST,
    ALBUM,
    ALARMS,
    HOUR,
    MINUTE,
    DAYS,
    REPEAT,
    ALLOW_SNOOZE,
    ENABLE,
    BACKGROUND_IMAGE_CONFIG,
    NAME,
    FILE_SIZE,
    FILE_CRC,
    LOCATION,
    INTERVAL,
    MAX_INTERVAL,
    MIN_INTERVAL,
    LATENCY,
    TIMEOUT,
    PRIORITY,
    CONFIGS,
    AFFECTED_CONFIGS,
    GROUP_ID,
    NOTIFICATION_FILTERS,
    FITNESS_DATA,
    START_TIME,
    END_TIME,
    TIMEZONE_OFFSET_IN_SECOND,
    TOTAL_DISTANCE,
    TOTAL_CALORIES,
    TOTAL_STEPS,
    TOTAL_ACTIVE_MINUTES,
    SLEEP_SESSION,
    WORKOUT_SESSION,
    HEART_RATE_RECORD,
    RESTING,
    GOAL_TRACKING,
    DATA_TYPE,
    MAC_ADDRESS,
    SERIAL_NUMBER,
    HARDWARE_REVISION,
    FIRMWARE_VERSION,
    MODEL_NUMBER,
    HEART_RATE_SERIAL_NUMBER,
    BOOT_LOADER_VERSION,
    WATCH_APP_VERSION,
    FONT_VERSION,
    SUPPORTED_DEVICE_CONFIG,
    MUSIC_EVENT,
    t0,
    STATUS,
    SKIP_RESUME,
    KEY,
    VALUE,
    FILE_HANDLE,
    NEW_STATE,
    PREV_STATE,
    NEW_BOND_STATE,
    RESPONSES,
    DATA_SIZE,
    SOCKET_ID,
    PROCESSED_DATA_LENGTH,
    WRITTEN_SIZE,
    WRITTEN_DATA_CRC,
    OPTIMAL_PAYLOAD,
    VERIFIED_DATA_OFFSET,
    VERIFIED_DATA_LENGTH,
    VERIFIED_DATA_CRC,
    RESULT_CODE,
    ERROR_DETAIL,
    TIMESTAMP,
    CHANNEL_ID,
    RAW_DATA,
    RAW_DATA_LENGTH,
    RAW_DATA_CRC,
    PACKAGE_COUNT,
    MOVING_TYPE,
    HAND_ID,
    DEGREE,
    DIRECTION,
    SPEED,
    HAND_CONFIGS,
    OFFSET,
    LENGTH,
    TOTAL_LENGTH,
    AUTO_CONNECT,
    SERVICES,
    CHARACTERISTICS,
    PROPOSED_TIMEOUT,
    REQUESTED_MTU,
    EXCHANGED_MTU,
    DEVICE_EVENT,
    DEVICE,
    STATE,
    CURRENT_BOND_STATE,
    CURRENT_HID_STATE,
    NEW_HID_STATE,
    IS_CACHED,
    PERIPHERAL_CURRENT_STATE,
    STARTED_AT,
    COMPLETED_AT,
    REQUEST_TIMEOUT_IN_MS,
    FILE_LIST,
    MESSAGE_LENGTH,
    MESSAGE_CRC,
    SEQUENCE,
    HEARTBEAT_INTERVAL_IN_MS,
    PHASE_RESULT,
    CREATED_AT,
    GATT_CONNECTED_DEVICES,
    HID_CONNECTED_DEVICES,
    SCAN_CALLBACK,
    SCAN_ERROR,
    FILTER_TYPE,
    SERIAL_NUMBER_PREFIXES,
    DEVICE_TYPES,
    SERIAL_NUMBER_REGEX,
    SCAN_FILTER,
    DEVICE_FILTER,
    FRAMES,
    TOTAL_FRAMES,
    TOTAL_SLEEP_IN_MINUTE,
    AWAKE_IN_MINUTE,
    LIGHT_SLEEP_IN_MINUTE,
    DEEP_SLEEP_IN_MINUTE,
    TEMPERATURE,
    CALORIES,
    DISTANCE,
    TIME,
    DATE,
    CURRENT_TEMPERATURE,
    HIGH_TEMPERATURE,
    LOW_TEMPERATURE,
    WEEKDAY,
    CURRENT_WEATHER_INFO,
    HOURLY_FORECAST,
    DAILY_FORECAST,
    WEATHER_CONFIGS,
    REQUEST_UUID,
    CORRECT_OFFSET,
    DEVICE_FILE,
    FILE_VERSION,
    CONNECT_DURATION,
    CONNECT_COUNT,
    DISCONNECT_COUNT,
    MSL_STATUS_CODE,
    PHONE_RANDOM_NUMBER,
    DEVICE_RANDOM_NUMBER,
    BOTH_SIDES_RANDOM_NUMBERS,
    PHONE_PUBLIC_KEY,
    DEVICE_PUBLIC_KEY,
    SECRET_KEY_CRC,
    AUTHENTICATION_KEY_TYPE,
    DEVICE_SECURITY_VERSION,
    TIMEZONE_OFFSET_IN_MINUTE,
    HOUR_DEGREE,
    MINUTE_DEGREE,
    SUBEYE_DEGREE,
    DURATION_IN_MS,
    HAND_MOVING_CONFIG,
    VIBE_PATTERN,
    CHARACTERISTIC,
    DESCRIPTOR,
    PREPARED_WRITE,
    RESPONSE_NEEDED,
    NEED_CONFIRM,
    COMMAND_ID,
    GATT_RESULT,
    SERVICE,
    EXECUTE,
    SECOND,
    MILLISECOND,
    SUPPORTED_FILES_VERSION,
    FILE_TYPE,
    VERSION,
    DEVICE_TYPE,
    EXPIRED_TIMESTAMP_IN_SECOND,
    IS_COMPLETED,
    EVENT_ID,
    FILES,
    SEGMENT_OFFSET,
    SEGMENT_LENGTH,
    TOTAL_FILE_LENGTH,
    SEGMENT_CRC,
    PAGE_OFFSET,
    NEW_SIZE_WRITTEN,
    APP_BUNDLE_CRC,
    APP_PACKAGE_NAME,
    DATA,
    DEVICE_DATA,
    DEFAULT_ICON,
    ICON_CONFIG,
    NOTIFICATION_ICONS,
    REQUEST_DATA,
    LANGUAGE_CODE,
    FILE_CRC_C,
    PRESET,
    NUMBER_OF_FILES,
    TOTAL_FILE_SIZE,
    SKIP_READ_ACTIVITY_FILES,
    SKIP_ERASE_ACTIVITY_FILES,
    RESPONSE_STATUS,
    LOCALE,
    LOCALIZATION_FILE,
    ACTUAL_WRITTEN_SIZE,
    HEADER_LENGTH,
    TRANSFERRED_DATA_SIZE,
    SIZE_WRITTEN,
    BOND_REQUIRED,
    MICRO_APP_VERSION,
    FAST_PAIR_ID_HEX_STRING,
    MICRO_APP_EVENT,
    SHIP_HANDS_TO_TWELVE,
    TRAVEL_TIME_IN_MINUTE,
    CURRENT_HEART_RATE,
    WATCH_PARAMETERS_FILE,
    BACKGROUND_IMAGES,
    IS_VALID_SECRET_KEY,
    BUTTON,
    DECLARATIONS,
    TOTAL_DECLARATIONS,
    MICRO_APP_ID,
    MINOR_VERSION,
    MAJOR_VERSION,
    VARIATION_NUMBER,
    VARIANT,
    RUN_TIME,
    HAS_CUSTOMIZATION,
    CRC,
    GOAL_ID,
    CONTEXT_NUMBER,
    ACTIVITY_ID,
    INSTRUCTION_ID,
    ROTATION,
    ANIMATIONS,
    IS_RESUME,
    TIMES,
    STREAM_DATA,
    DELAY_IN_SECOND,
    HID_CODE,
    IMMEDIATE_RELEASE,
    SYSTEM_VERSION,
    MICRO_APP_MAPPINGS,
    TOTAL_MAPPINGS,
    MICRO_APP_ERROR_TYPE,
    NOTIFICATION_UID,
    APP_NOTIFICATION_EVENT,
    SOFTWARE_REVISION,
    FILE_HANDLE_DESCRIPTION,
    ABSOLUTE_FILE_NUMBER,
    PHASE_ID,
    REQUEST_RESULT,
    GATT_STATUS,
    COMMAND_RESULT,
    REQUESTED_CONNECTION_PARAMS,
    ACCEPTED_CONNECTION_PARAMS,
    ERASE_DURATION_IN_MS,
    TRANSFER_DURATION_IN_MS,
    RECONNECT_DURATION_IN_MS,
    OLD_FIRMWARE,
    NEW_FIRMWARE,
    STACK_TRACE,
    FILE_INDEX,
    ERROR_CODE,
    FILE,
    ROW_ID,
    NUMBER_OF_DELETED_ROW,
    COMMUTE_TIME_IN_MINUTE,
    COMMUTE_INFO,
    TRAFFIC,
    CURRENT_FILES_VERSION,
    CURRENT_VERSION,
    SUPPORTED_VERSION,
    CHALLENGE_ID,
    STEP,
    USER,
    RANK,
    PLAYER_NUM,
    ID,
    TIMEOUT_IN_MS,
    PERCENTAGE,
    VOLTAGE,
    MESS,
    CHALLENGES,
    BLUETOOTH_DEVICE_TYPE,
    ICON,
    REPLY_MESSAGE_GROUP,
    TIMESTAMPS,
    TARGET_FIRMWARE,
    ENCRYPT_METHOD,
    KEY_TYPE,
    BATTERY_MODE,
    NFC_STATE,
    ALWAYS_ON_SCREEN_STATE,
    TOUCH_TO_WAKE_STATE,
    TILT_TO_WAKE_STATE,
    LOCATION_STATE,
    VIBRATION_STATE,
    SPEAKER_STATE,
    WIFI_STATE,
    GOOGLE_DETECT_STATE,
    BLE_FROM_MINUTE_OFFET,
    BLE_TO_MINUTE_OFFET
}
