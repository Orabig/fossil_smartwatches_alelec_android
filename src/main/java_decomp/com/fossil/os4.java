package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.BleCommandResultManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class os4 extends m24<d, e, b> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((qg6) null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ c f; // = new c();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return os4.g;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements BleCommandResultManager.b {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r7v15, types: [com.fossil.os4, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r8v7, types: [com.fossil.os4, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(os4.h.a(), "Inside .onReceive");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal());
            if (os4.this.e() && wg6.a((Object) os4.this.d(), (Object) stringExtra) && intExtra == CommunicateMode.READ_RSSI.ordinal()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = os4.h.a();
                local.d(a2, "onReceive - blePhase: " + intExtra + " - deviceId: " + stringExtra + " - mIsExecuted: " + os4.this.e());
                os4.this.a(false);
                if (intExtra2 != ServiceActionResult.SUCCEEDED.ordinal() || intent.getExtras() == null) {
                    FLogger.INSTANCE.getLocal().e(os4.h.a(), "Inside .onReceive return error");
                    os4.this.a(new b());
                    return;
                }
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    int i = extras.getInt(Constants.RSSI, 0);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = os4.h.a();
                    local2.d(a3, "Inside .onReceive return rssi=" + i);
                    os4.this.a(new e(i));
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public d(String str) {
            wg6.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public e(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = os4.class.getSimpleName();
        wg6.a((Object) simpleName, "GetRssi::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    public final String d() {
        String str = this.e;
        if (str != null) {
            return str;
        }
        wg6.d("mDeviceId");
        throw null;
    }

    @DexIgnore
    public final boolean e() {
        return this.d;
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.READ_RSSI);
    }

    @DexIgnore
    public final void g() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.READ_RSSI);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public Object a(d dVar, xe6<Object> xe6) {
        String str;
        try {
            FLogger.INSTANCE.getLocal().d(g, "Inside .run");
            this.d = true;
            if (dVar == null || (str = dVar.a()) == null) {
                str = "";
            }
            this.e = str;
            if (PortfolioApp.get.b() != null) {
                IButtonConnectivity b2 = PortfolioApp.get.b();
                String str2 = null;
                if (b2 != null) {
                    if (dVar != null) {
                        str2 = dVar.a();
                    }
                    return hf6.a(b2.deviceGetRssi(str2));
                }
                wg6.a();
                throw null;
            }
            FLogger.INSTANCE.getLocal().e(g, "Inside .run ButtonApi is null");
            return new b();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = g;
            local.e(str3, "Inside .run caught exception=" + e2);
            return new b();
        }
    }
}
