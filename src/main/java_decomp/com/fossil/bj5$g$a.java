package com.fossil;

import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1", f = "SleepDetailPresenter.kt", l = {85}, m = "invokeSuspend")
public final class bj5$g$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter.g this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1$summary$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super MFSleepDay>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bj5$g$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bj5$g$a bj5_g_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = bj5_g_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                SleepDetailPresenter sleepDetailPresenter = this.this$0.this$0.a;
                return sleepDetailPresenter.b(sleepDetailPresenter.f, (List<MFSleepDay>) this.this$0.this$0.a.l);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bj5$g$a(SleepDetailPresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        bj5$g$a bj5_g_a = new bj5$g$a(this.this$0, xe6);
        bj5_g_a.p$ = (il6) obj;
        return bj5_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((bj5$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            dl6 a3 = this.this$0.a.b();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = gk6.a(a3, aVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        MFSleepDay mFSleepDay = (MFSleepDay) obj;
        if (this.this$0.a.n == null || (!wg6.a((Object) this.this$0.a.n, (Object) mFSleepDay))) {
            this.this$0.a.n = mFSleepDay;
            this.this$0.a.r.a(mFSleepDay);
            if (this.this$0.a.i && this.this$0.a.j) {
                rm6 unused = this.this$0.a.l();
                rm6 unused2 = this.this$0.a.m();
            }
        }
        return cd6.a;
    }
}
