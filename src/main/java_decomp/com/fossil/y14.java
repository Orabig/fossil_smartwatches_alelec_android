package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y14 implements Factory<t24> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<an4> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<bt5> d;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> e;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> f;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> g;
    @DexIgnore
    public /* final */ Provider<GoalTrackingDatabase> h;
    @DexIgnore
    public /* final */ Provider<DeviceDao> i;
    @DexIgnore
    public /* final */ Provider<HybridCustomizeDatabase> j;
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingRepository> k;
    @DexIgnore
    public /* final */ Provider<cj4> l;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> m;

    @DexIgnore
    public y14(b14 b14, Provider<an4> provider, Provider<UserRepository> provider2, Provider<bt5> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<GoalTrackingDatabase> provider7, Provider<DeviceDao> provider8, Provider<HybridCustomizeDatabase> provider9, Provider<MicroAppLastSettingRepository> provider10, Provider<cj4> provider11, Provider<AlarmsRepository> provider12) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
        this.m = provider12;
    }

    @DexIgnore
    public static y14 a(b14 b14, Provider<an4> provider, Provider<UserRepository> provider2, Provider<bt5> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<GoalTrackingDatabase> provider7, Provider<DeviceDao> provider8, Provider<HybridCustomizeDatabase> provider9, Provider<MicroAppLastSettingRepository> provider10, Provider<cj4> provider11, Provider<AlarmsRepository> provider12) {
        return new y14(b14, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12);
    }

    @DexIgnore
    public static t24 b(b14 b14, Provider<an4> provider, Provider<UserRepository> provider2, Provider<bt5> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<GoalTrackingDatabase> provider7, Provider<DeviceDao> provider8, Provider<HybridCustomizeDatabase> provider9, Provider<MicroAppLastSettingRepository> provider10, Provider<cj4> provider11, Provider<AlarmsRepository> provider12) {
        return a(b14, provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get(), provider9.get(), provider10.get(), provider11.get(), provider12.get());
    }

    @DexIgnore
    public static t24 a(b14 b14, an4 an4, UserRepository userRepository, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, cj4 cj4, AlarmsRepository alarmsRepository) {
        t24 a2 = b14.a(an4, userRepository, getHybridDeviceSettingUseCase, notificationsRepository, portfolioApp, goalTrackingRepository, goalTrackingDatabase, deviceDao, hybridCustomizeDatabase, microAppLastSettingRepository, cj4, alarmsRepository);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public t24 get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m);
    }
}
