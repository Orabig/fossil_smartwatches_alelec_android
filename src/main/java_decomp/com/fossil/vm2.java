package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm2 {
    @DexIgnore
    public static /* final */ um2<?> a; // = new wm2();
    @DexIgnore
    public static /* final */ um2<?> b; // = c();

    @DexIgnore
    public static um2<?> a() {
        return a;
    }

    @DexIgnore
    public static um2<?> b() {
        um2<?> um2 = b;
        if (um2 != null) {
            return um2;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    @DexIgnore
    public static um2<?> c() {
        try {
            return (um2) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
