package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class od4 extends nd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public long x;

    /*
    static {
        z.put(2131363218, 1);
        z.put(2131361851, 2);
        z.put(2131362190, 3);
        z.put(2131361850, 4);
        z.put(2131362383, 5);
        z.put(2131362382, 6);
        z.put(2131362384, 7);
        z.put(2131362880, 8);
    }
    */

    @DexIgnore
    public od4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 9, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public od4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[4], objArr[2], objArr[3], objArr[6], objArr[5], objArr[7], objArr[0], objArr[8], objArr[1]);
        this.x = -1;
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
