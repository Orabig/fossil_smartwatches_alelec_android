package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj4 implements MembersInjector<tj4> {
    @DexIgnore
    public static void a(tj4 tj4, an4 an4) {
        tj4.a = an4;
    }

    @DexIgnore
    public static void a(tj4 tj4, DeviceRepository deviceRepository) {
        tj4.b = deviceRepository;
    }

    @DexIgnore
    public static void a(tj4 tj4, UserRepository userRepository) {
        tj4.c = userRepository;
    }
}
