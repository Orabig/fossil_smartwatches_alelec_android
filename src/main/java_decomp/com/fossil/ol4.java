package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ol4 {
    @DexIgnore
    public static byte[] a; // = new byte[255];

    /*
    static {
        for (int i = 0; i < 255; i++) {
            a[i] = -1;
        }
        for (int i2 = 90; i2 >= 65; i2--) {
            a[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 122; i3 >= 97; i3--) {
            a[i3] = (byte) ((i3 - 97) + 26);
        }
        for (int i4 = 57; i4 >= 48; i4--) {
            a[i4] = (byte) ((i4 - 48) + 52);
        }
        byte[] bArr = a;
        bArr[43] = 62;
        bArr[47] = 63;
    }
    */

    @DexIgnore
    public static byte[] a(byte[] bArr) {
        byte[] b = b(bArr);
        if (b.length == 0) {
            return new byte[0];
        }
        int length = b.length / 4;
        int length2 = b.length;
        while (b[length2 - 1] == 61) {
            length2--;
            if (length2 == 0) {
                return new byte[0];
            }
        }
        byte[] bArr2 = new byte[(length2 - length)];
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = i2 * 4;
            byte b2 = b[i3 + 2];
            byte b3 = b[i3 + 3];
            byte[] bArr3 = a;
            byte b4 = bArr3[b[i3]];
            byte b5 = bArr3[b[i3 + 1]];
            if (b2 != 61 && b3 != 61) {
                byte b6 = bArr3[b2];
                byte b7 = bArr3[b3];
                bArr2[i] = (byte) ((b4 << 2) | (b5 >> 4));
                bArr2[i + 1] = (byte) (((b5 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) << 4) | ((b6 >> 2) & 15));
                bArr2[i + 2] = (byte) ((b6 << 6) | b7);
            } else if (b2 == 61) {
                bArr2[i] = (byte) ((b5 >> 4) | (b4 << 2));
            } else if (b3 == 61) {
                byte b8 = a[b2];
                bArr2[i] = (byte) ((b4 << 2) | (b5 >> 4));
                bArr2[i + 1] = (byte) (((b5 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) << 4) | ((b8 >> 2) & 15));
            }
            i += 3;
        }
        return bArr2;
    }

    @DexIgnore
    public static byte[] b(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            if (a(bArr[i2])) {
                bArr2[i] = bArr[i2];
                i++;
            }
        }
        byte[] bArr3 = new byte[i];
        System.arraycopy(bArr2, 0, bArr3, 0, i);
        return bArr3;
    }

    @DexIgnore
    public static boolean a(byte b) {
        return b == 61 || a[b] != -1;
    }
}
