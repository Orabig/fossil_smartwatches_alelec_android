package com.fossil;

import android.os.Build;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm0 {
    @DexIgnore
    public static /* final */ w40 a; // = new w40((byte) 4, (byte) 0);
    @DexIgnore
    public static /* final */ eh1[] b; // = {eh1.MAKE_DEVICE_READY, eh1.FETCH_DEVICE_INFORMATION, eh1.READ_DEVICE_INFO_CHARACTERISTICS, eh1.OTA, eh1.PLAY_ANIMATION, eh1.CLEAN_UP_DEVICE, eh1.SYNC, eh1.READ_RSSI, eh1.SYNC_FLOW, eh1.SEND_CUSTOM_COMMAND, eh1.CDG_GET_BATTERY, eh1.CDG_GET_AVERAGE_RSSI, eh1.CDG_VIBE, eh1.CDG_START_STREAMING_ACCEL, eh1.CDG_ABORT_STREAMING_ACCEL, eh1.REQUEST_HANDS, eh1.MOVE_HANDS, eh1.RELEASE_HANDS, eh1.SET_CALIBRATION_POSITION};
    @DexIgnore
    public static /* final */ Type[] c; // = {ka0.class, na0.class};
    @DexIgnore
    public static /* final */ it0[] d; // = {new it0(12, 12, 30, 600), new it0(18, 18, 19, 600), new it0(24, 24, 14, 600), new it0(48, 48, 6, 600), new it0(72, 72, 4, 600)};
    @DexIgnore
    public static /* final */ it0[] e; // = {new it0(12, 12, 45, 600), new it0(24, 24, 22, 600), new it0(36, 36, 15, 600), new it0(104, 112, 4, 600)};
    @DexIgnore
    public static /* final */ fm0 f; // = new fm0();

    /*
    static {
        new Type[1][0] = ia0.class;
    }
    */

    @DexIgnore
    public final long a(boolean z) {
        return z ? 60000 : 30000;
    }

    @DexIgnore
    public final boolean a(r40 r40, if1 if1) {
        if (f.b(r40)) {
            return nd6.a(b, if1.y);
        }
        return true;
    }

    @DexIgnore
    public final boolean b(r40 r40) {
        if (r40.getDeviceType() == s40.SE1 || r40.getDeviceType() == s40.SLIM || r40.getDeviceType() == s40.MINI) {
            w40 w40 = r40.i().get(Short.valueOf(w31.OTA.a));
            if (w40 == null) {
                w40 = mi0.A.i();
            }
            if (w40.compareTo(mi0.A.z()) < 0) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final Type[] c() {
        Object[] array = qd6.a(new Type[]{ib0.class, wa0.class, bb0.class, db0.class, ga0.class, fb0.class, kb0.class, ja0.class, ka0.class, la0.class, na0.class, mb0.class, oa0.class, pa0.class, ra0.class, ua0.class, va0.class, nb0.class, ya0.class, za0.class, pb0.class, ab0.class, ub0.class, xb0.class, rb0.class, ta0.class, ma0.class, eb0.class, gb0.class, hb0.class, ob0.class, jb0.class, lb0.class, vb0.class, wb0.class, sb0.class, sa0.class, tb0.class, xa0.class, ea0.class, cb0.class, fa0.class, qb0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final w40 d() {
        return a;
    }

    @DexIgnore
    public final Type[] e() {
        Object[] array = qd6.a(new Type[]{wa0.class, bb0.class, db0.class, ga0.class, fb0.class, ja0.class, ka0.class, la0.class, na0.class, oa0.class, pa0.class, ra0.class, va0.class, fd1.class, af1.class, wg1.class, qa0.class, ha0.class, ya0.class, za0.class, ab0.class, hb0.class, ob0.class, jb0.class, lb0.class, wb0.class, eb0.class, tb0.class, ea0.class, cb0.class, fa0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final boolean f() {
        return false;
    }

    @DexIgnore
    public final Type[] g() {
        Object[] array = qd6.a(new Type[]{wa0.class, bb0.class, db0.class, ga0.class, fb0.class, ja0.class, ka0.class, la0.class, na0.class, oa0.class, pa0.class, ra0.class, va0.class, fd1.class, af1.class, wg1.class, qa0.class, ha0.class, ya0.class, za0.class, ab0.class, hb0.class, ob0.class, jb0.class, lb0.class, wb0.class, eb0.class, tb0.class, ea0.class, cb0.class, fa0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] h() {
        Object[] array = qd6.a(new Type[]{wa0.class, bb0.class, db0.class, ga0.class, fb0.class, ja0.class, ka0.class, la0.class, na0.class, pa0.class, ra0.class, va0.class, fd1.class, af1.class, wg1.class, qa0.class, ha0.class, ya0.class, za0.class, ab0.class, ob0.class, jb0.class, lb0.class, wb0.class, eb0.class, tb0.class, ea0.class, cb0.class, fa0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] i() {
        return c;
    }

    @DexIgnore
    public final boolean a() {
        return Build.VERSION.SDK_INT < 28;
    }

    @DexIgnore
    public final it0[] a(r40 r40) {
        switch (mk0.a[r40.getDeviceType().ordinal()]) {
            case 1:
            case 2:
                return d;
            case 3:
            case 4:
            case 5:
            case 6:
                if (b(r40)) {
                    return new it0[0];
                }
                return e;
            case 7:
            case 8:
            case 9:
                return new it0[0];
            default:
                throw new kc6();
        }
    }

    @DexIgnore
    public final Type[] b() {
        Object[] array = qd6.a(new Type[]{ib0.class, wa0.class, bb0.class, db0.class, ga0.class, fb0.class, kb0.class, ja0.class, ka0.class, la0.class, na0.class, mb0.class, oa0.class, pa0.class, ra0.class, ua0.class, va0.class, nb0.class, ya0.class, za0.class, pb0.class, ab0.class, ub0.class, xb0.class, rb0.class, ta0.class, ma0.class, eb0.class, gb0.class, hb0.class, ob0.class, jb0.class, lb0.class, vb0.class, wb0.class, sb0.class, sa0.class, tb0.class, xa0.class, ea0.class, cb0.class, fa0.class, qb0.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
