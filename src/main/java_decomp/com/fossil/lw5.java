package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw5 implements Factory<kw5> {
    @DexIgnore
    public /* final */ Provider<an4> a;

    @DexIgnore
    public lw5(Provider<an4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static lw5 a(Provider<an4> provider) {
        return new lw5(provider);
    }

    @DexIgnore
    public static kw5 b(Provider<an4> provider) {
        return new kw5(provider.get());
    }

    @DexIgnore
    public kw5 get() {
        return b(this.a);
    }
}
