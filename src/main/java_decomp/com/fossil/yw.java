package com.fossil;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yw implements zr<ParcelFileDescriptor, Bitmap> {
    @DexIgnore
    public /* final */ pw a;

    @DexIgnore
    public yw(pw pwVar) {
        this.a = pwVar;
    }

    @DexIgnore
    public boolean a(ParcelFileDescriptor parcelFileDescriptor, xr xrVar) {
        return this.a.a(parcelFileDescriptor);
    }

    @DexIgnore
    public rt<Bitmap> a(ParcelFileDescriptor parcelFileDescriptor, int i, int i2, xr xrVar) throws IOException {
        return this.a.a(parcelFileDescriptor, i, i2, xrVar);
    }
}
