package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gt4 implements Factory<ft4> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public gt4(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static gt4 a(Provider<UserRepository> provider) {
        return new gt4(provider);
    }

    @DexIgnore
    public static ft4 b(Provider<UserRepository> provider) {
        return new UpdateUser(provider.get());
    }

    @DexIgnore
    public UpdateUser get() {
        return b(this.a);
    }
}
