package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ee2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ee2> CREATOR; // = new he2();
    @DexIgnore
    public /* final */ f72 a;

    @DexIgnore
    public ee2(f72 f72) {
        this.a = f72;
    }

    @DexIgnore
    public final f72 p() {
        return this.a;
    }

    @DexIgnore
    public final String toString() {
        return String.format("ApplicationUnregistrationRequest{%s}", new Object[]{this.a});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, (Parcelable) this.a, i, false);
        g22.a(parcel, a2);
    }
}
