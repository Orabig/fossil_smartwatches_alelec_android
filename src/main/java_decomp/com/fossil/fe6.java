package com.fossil;

import java.util.Map;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fe6 {
    @DexIgnore
    public static final <K, V> V a(Map<K, ? extends V> map, K k) {
        wg6.b(map, "$this$getOrImplicitDefault");
        if (map instanceof ee6) {
            return ((ee6) map).a(k);
        }
        V v = map.get(k);
        if (v != null || map.containsKey(k)) {
            return v;
        }
        throw new NoSuchElementException("Key " + k + " is missing in the map.");
    }
}
