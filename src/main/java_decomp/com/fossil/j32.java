package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j32 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<j32> CREATOR; // = new k32();
    @DexIgnore
    public Bundle a;
    @DexIgnore
    public iv1[] b;

    @DexIgnore
    public j32(Bundle bundle, iv1[] iv1Arr) {
        this.a = bundle;
        this.b = iv1Arr;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a, false);
        g22.a(parcel, 2, (T[]) this.b, i, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public j32() {
    }
}
