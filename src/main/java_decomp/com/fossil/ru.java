package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ru {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(rt<?> rtVar);
    }

    @DexIgnore
    rt<?> a(vr vrVar);

    @DexIgnore
    rt<?> a(vr vrVar, rt<?> rtVar);

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(a aVar);
}
