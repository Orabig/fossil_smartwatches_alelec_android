package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class km {
    @DexIgnore
    public static xh a; // = new a(1, 2);
    @DexIgnore
    public static xh b; // = new b(3, 4);
    @DexIgnore
    public static xh c; // = new c(4, 5);
    @DexIgnore
    public static xh d; // = new d(6, 7);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends xh {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            iiVar.b("INSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo");
            iiVar.b("DROP TABLE IF EXISTS alarmInfo");
            iiVar.b("INSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends xh {
        @DexIgnore
        public b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(ii iiVar) {
            if (Build.VERSION.SDK_INT >= 23) {
                iiVar.b("UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends xh {
        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(ii iiVar) {
            iiVar.b("ALTER TABLE workspec ADD COLUMN `trigger_content_update_delay` INTEGER NOT NULL DEFAULT -1");
            iiVar.b("ALTER TABLE workspec ADD COLUMN `trigger_max_content_delay` INTEGER NOT NULL DEFAULT -1");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends xh {
        @DexIgnore
        public d(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends xh {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public e(Context context, int i, int i2) {
            super(i, i2);
            this.a = context;
        }

        @DexIgnore
        public void migrate(ii iiVar) {
            new ko(this.a).a(true);
        }
    }
}
