package com.fossil;

import com.portfolio.platform.data.model.Ringtone;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$1$1", f = "SearchRingPhonePresenter.kt", l = {}, m = "invokeSuspend")
public final class u55$b$a extends sf6 implements ig6<il6, xe6<? super List<? extends Ringtone>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    public u55$b$a(xe6 xe6) {
        super(2, xe6);
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        u55$b$a u55_b_a = new u55$b$a(xe6);
        u55_b_a.p$ = (il6) obj;
        return u55_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((u55$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return tj4.f.d();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
