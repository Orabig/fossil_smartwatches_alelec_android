package com.fossil;

import com.fossil.fn2;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo2<T> implements fp2<T> {
    @DexIgnore
    public /* final */ ro2 a;
    @DexIgnore
    public /* final */ wp2<?, ?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ um2<?> d;

    @DexIgnore
    public vo2(wp2<?, ?> wp2, um2<?> um2, ro2 ro2) {
        this.b = wp2;
        this.c = um2.a(ro2);
        this.d = um2;
        this.a = ro2;
    }

    @DexIgnore
    public static <T> vo2<T> a(wp2<?, ?> wp2, um2<?> um2, ro2 ro2) {
        return new vo2<>(wp2, um2, ro2);
    }

    @DexIgnore
    public final T zza() {
        return this.a.c().u();
    }

    @DexIgnore
    public final void zzb(T t, T t2) {
        gp2.a(this.b, t, t2);
        if (this.c) {
            gp2.a(this.d, t, t2);
        }
    }

    @DexIgnore
    public final void zzc(T t) {
        this.b.b(t);
        this.d.c(t);
    }

    @DexIgnore
    public final boolean zzd(T t) {
        return this.d.a((Object) t).e();
    }

    @DexIgnore
    public final void a(T t, tq2 tq2) throws IOException {
        Iterator<Map.Entry<?, Object>> c2 = this.d.a((Object) t).c();
        if (!c2.hasNext()) {
            wp2<?, ?> wp2 = this.b;
            wp2.b(wp2.a(t), tq2);
            return;
        }
        ((zm2) c2.next().getKey()).zzc();
        throw null;
    }

    @DexIgnore
    public final boolean zza(T t, T t2) {
        if (!this.b.a(t).equals(this.b.a(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.a((Object) t).equals(this.d.a((Object) t2));
        }
        return true;
    }

    @DexIgnore
    public final int zzb(T t) {
        wp2<?, ?> wp2 = this.b;
        int c2 = wp2.c(wp2.a(t)) + 0;
        return this.c ? c2 + this.d.a((Object) t).f() : c2;
    }

    @DexIgnore
    public final int zza(T t) {
        int hashCode = this.b.a(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.a((Object) t).hashCode() : hashCode;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: com.fossil.fn2$f} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void a(T t, byte[] bArr, int i, int i2, xl2 xl2) throws IOException {
        fn2 fn2 = t;
        zp2 zp2 = fn2.zzb;
        if (zp2 == zp2.d()) {
            zp2 = zp2.e();
            fn2.zzb = zp2;
        }
        ((fn2.d) t).n();
        fn2.f fVar = null;
        while (i < i2) {
            int a2 = ul2.a(bArr, i, xl2);
            int i3 = xl2.a;
            if (i3 == 11) {
                int i4 = 0;
                yl2 yl2 = null;
                while (a2 < i2) {
                    a2 = ul2.a(bArr, a2, xl2);
                    int i5 = xl2.a;
                    int i6 = i5 >>> 3;
                    int i7 = i5 & 7;
                    if (i6 != 2) {
                        if (i6 == 3) {
                            if (fVar != null) {
                                ap2.a();
                                throw new NoSuchMethodError();
                            } else if (i7 == 2) {
                                a2 = ul2.e(bArr, a2, xl2);
                                yl2 = (yl2) xl2.c;
                            }
                        }
                    } else if (i7 == 0) {
                        a2 = ul2.a(bArr, a2, xl2);
                        i4 = xl2.a;
                        fVar = (fn2.f) this.d.a(xl2.d, this.a, i4);
                    }
                    if (i5 == 12) {
                        break;
                    }
                    a2 = ul2.a(i5, bArr, a2, i2, xl2);
                }
                if (yl2 != null) {
                    zp2.a((i4 << 3) | 2, (Object) yl2);
                }
                i = a2;
            } else if ((i3 & 7) == 2) {
                fVar = this.d.a(xl2.d, this.a, i3 >>> 3);
                if (fVar == null) {
                    i = ul2.a(i3, bArr, a2, i2, zp2, xl2);
                } else {
                    ap2.a();
                    throw new NoSuchMethodError();
                }
            } else {
                i = ul2.a(i3, bArr, a2, i2, xl2);
            }
        }
        if (i != i2) {
            throw qn2.zzg();
        }
    }
}
