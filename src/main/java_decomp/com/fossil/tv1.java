package com.fossil;

import android.text.TextUtils;
import com.fossil.rv1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tv1 extends Exception {
    @DexIgnore
    public /* final */ p4<lw1<?>, gv1> zaba;

    @DexIgnore
    public tv1(p4<lw1<?>, gv1> p4Var) {
        this.zaba = p4Var;
    }

    @DexIgnore
    public gv1 getConnectionResult(vv1<? extends rv1.d> vv1) {
        lw1<? extends rv1.d> a = vv1.a();
        w12.a(this.zaba.get(a) != null, (Object) "The given API was not part of the availability request.");
        return this.zaba.get(a);
    }

    @DexIgnore
    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        for (lw1 next : this.zaba.keySet()) {
            gv1 gv1 = this.zaba.get(next);
            if (gv1.E()) {
                z = false;
            }
            String a = next.a();
            String valueOf = String.valueOf(gv1);
            StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 2 + String.valueOf(valueOf).length());
            sb.append(a);
            sb.append(": ");
            sb.append(valueOf);
            arrayList.add(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        if (z) {
            sb2.append("None of the queried APIs are available. ");
        } else {
            sb2.append("Some of the queried APIs are unavailable. ");
        }
        sb2.append(TextUtils.join("; ", arrayList));
        return sb2.toString();
    }

    @DexIgnore
    public final p4<lw1<?>, gv1> zaj() {
        return this.zaba;
    }

    @DexIgnore
    public gv1 getConnectionResult(xv1<? extends rv1.d> xv1) {
        lw1<? extends rv1.d> a = xv1.a();
        w12.a(this.zaba.get(a) != null, (Object) "The given API was not part of the availability request.");
        return this.zaba.get(a);
    }
}
