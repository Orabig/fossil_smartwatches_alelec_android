package com.fossil;

import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2", f = "SwitchActiveDeviceUseCase.kt", l = {95}, m = "invokeSuspend")
public final class yr4$i$b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int I$0;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SwitchActiveDeviceUseCase.i this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yr4$i$b(SwitchActiveDeviceUseCase.i iVar, MisfitDeviceProfile misfitDeviceProfile, String str, xe6 xe6) {
        super(2, xe6);
        this.this$0 = iVar;
        this.$currentDeviceProfile = misfitDeviceProfile;
        this.$serial = str;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        yr4$i$b yr4_i_b = new yr4$i$b(this.this$0, this.$currentDeviceProfile, this.$serial, xe6);
        yr4_i_b.p$ = (il6) obj;
        return yr4_i_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((yr4$i$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Integer vibrationStrength;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            int b = jk4.b(this.$currentDeviceProfile.getVibrationStrength().getVibrationStrengthLevel());
            DeviceRepository b2 = this.this$0.a.i;
            String str = this.$serial;
            wg6.a((Object) str, "serial");
            Device deviceBySerial = b2.getDeviceBySerial(str);
            if (deviceBySerial != null && ((vibrationStrength = deviceBySerial.getVibrationStrength()) == null || vibrationStrength.intValue() != b)) {
                deviceBySerial.setVibrationStrength(hf6.a(b));
                DeviceRepository b3 = this.this$0.a.i;
                this.L$0 = il6;
                this.I$0 = b;
                this.L$1 = deviceBySerial;
                this.label = 1;
                if (b3.updateDevice(deviceBySerial, false, this) == a) {
                    return a;
                }
            }
        } else if (i == 1) {
            Device device = (Device) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
