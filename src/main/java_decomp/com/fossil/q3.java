package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q3 extends s3 {
    @DexIgnore
    public static volatile q3 c;
    @DexIgnore
    public static /* final */ Executor d; // = new a();
    @DexIgnore
    public static /* final */ Executor e; // = new b();
    @DexIgnore
    public s3 a; // = this.b;
    @DexIgnore
    public s3 b; // = new r3();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            q3.c().c(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            q3.c().a(runnable);
        }
    }

    @DexIgnore
    public static Executor b() {
        return e;
    }

    @DexIgnore
    public static q3 c() {
        if (c != null) {
            return c;
        }
        synchronized (q3.class) {
            if (c == null) {
                c = new q3();
            }
        }
        return c;
    }

    @DexIgnore
    public static Executor d() {
        return d;
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.a.a(runnable);
    }

    @DexIgnore
    public boolean a() {
        return this.a.a();
    }

    @DexIgnore
    public void c(Runnable runnable) {
        this.a.c(runnable);
    }
}
