package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import com.portfolio.platform.uirenew.pairing.PairingUpdateFirmwareFragment;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vs5 {
    @DexIgnore
    public static /* final */ String d; // = d;
    @DexIgnore
    public /* final */ b a; // = new b(this);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ PairingUpdateFirmwareFragment c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ vs5 a;

        @DexIgnore
        public b(vs5 vs5) {
            this.a = vs5;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.r.a();
            local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            if (!TextUtils.isEmpty(otaEvent.getSerial()) && xj6.b(otaEvent.getSerial(), this.a.c(), true)) {
                this.a.b().h((int) (otaEvent.getProcess() * ((float) 10)));
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public vs5(String str, PairingUpdateFirmwareFragment pairingUpdateFirmwareFragment) {
        wg6.b(str, "serial");
        wg6.b(pairingUpdateFirmwareFragment, "mView");
        this.b = str;
        this.c = pairingUpdateFirmwareFragment;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v24, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v27, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.b);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "serial=" + this.b + ", mCurrentDeviceType=" + deviceBySerial);
        if (deviceBySerial == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            explore.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886684));
            explore.setBackground(2131231385);
            explore2.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886685));
            explore2.setBackground(2131231383);
            explore3.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886682));
            explore3.setBackground(2131231386);
            explore4.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886683));
            explore4.setBackground(2131231382);
        } else {
            explore.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886691));
            explore.setBackground(2131231385);
            explore2.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886690));
            explore2.setBackground(2131231387);
            explore3.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886688));
            explore3.setBackground(2131231386);
            explore4.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886689));
            explore4.setBackground(2131231384);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.c.h((List<? extends Explore>) arrayList);
    }

    @DexIgnore
    public final PairingUpdateFirmwareFragment b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void d() {
        Object instance = PortfolioApp.get.instance();
        b bVar = this.a;
        instance.registerReceiver(bVar, new IntentFilter(PortfolioApp.get.instance().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        a();
        this.c.g();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void e() {
        PortfolioApp.get.instance().unregisterReceiver(this.a);
    }

    @DexIgnore
    public final void f() {
        if (!DeviceIdentityUtils.isDianaDevice(this.b)) {
            this.c.k1();
        }
    }
}
