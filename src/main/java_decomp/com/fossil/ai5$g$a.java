package com.fossil;

import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class ai5$g$a extends sf6 implements ig6<il6, xe6<? super zh4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesDetailPresenter.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ai5$g$a(CaloriesDetailPresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ai5$g$a ai5_g_a = new ai5$g$a(this.this$0, xe6);
        ai5_g_a.p$ = (il6) obj;
        return ai5_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ai5$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0018, code lost:
        r2 = r2.getDistanceUnit();
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        zh4 distanceUnit;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            MFUser currentUser = this.this$0.this$0.v.getCurrentUser();
            return (currentUser == null || distanceUnit == null) ? zh4.METRIC : distanceUnit;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
