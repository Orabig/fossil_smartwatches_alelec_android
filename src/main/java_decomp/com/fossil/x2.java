package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.transition.Transition;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import androidx.appcompat.view.menu.ListMenuItemView;
import androidx.appcompat.widget.ListPopupWindow;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x2 extends ListPopupWindow implements w2 {
    @DexIgnore
    public static Method N;
    @DexIgnore
    public w2 M;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends t2 {
        @DexIgnore
        public /* final */ int s;
        @DexIgnore
        public /* final */ int t;
        @DexIgnore
        public w2 u;
        @DexIgnore
        public MenuItem v;

        @DexIgnore
        public a(Context context, boolean z) {
            super(context, z);
            Configuration configuration = context.getResources().getConfiguration();
            if (Build.VERSION.SDK_INT < 17 || 1 != configuration.getLayoutDirection()) {
                this.s = 22;
                this.t = 21;
                return;
            }
            this.s = 21;
            this.t = 22;
        }

        @DexIgnore
        public boolean onHoverEvent(MotionEvent motionEvent) {
            int i;
            p1 p1Var;
            int pointToPosition;
            int i2;
            if (this.u != null) {
                ListAdapter adapter = getAdapter();
                if (adapter instanceof HeaderViewListAdapter) {
                    HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                    i = headerViewListAdapter.getHeadersCount();
                    p1Var = (p1) headerViewListAdapter.getWrappedAdapter();
                } else {
                    i = 0;
                    p1Var = (p1) adapter;
                }
                t1 t1Var = null;
                if (motionEvent.getAction() != 10 && (pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY())) != -1 && (i2 = pointToPosition - i) >= 0 && i2 < p1Var.getCount()) {
                    t1Var = p1Var.getItem(i2);
                }
                MenuItem menuItem = this.v;
                if (menuItem != t1Var) {
                    q1 b = p1Var.b();
                    if (menuItem != null) {
                        this.u.b(b, menuItem);
                    }
                    this.v = t1Var;
                    if (t1Var != null) {
                        this.u.a(b, t1Var);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }

        @DexIgnore
        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            ListMenuItemView listMenuItemView = (ListMenuItemView) getSelectedView();
            if (listMenuItemView != null && i == this.s) {
                if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (listMenuItemView == null || i != this.t) {
                return super.onKeyDown(i, keyEvent);
            } else {
                setSelection(-1);
                ((p1) getAdapter()).b().a(false);
                return true;
            }
        }

        @DexIgnore
        public void setHoverListener(w2 w2Var) {
            this.u = w2Var;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ void setSelector(Drawable drawable) {
            super.setSelector(drawable);
        }
    }

    /*
    static {
        try {
            if (Build.VERSION.SDK_INT <= 28) {
                N = PopupWindow.class.getDeclaredMethod("setTouchModal", new Class[]{Boolean.TYPE});
            }
        } catch (NoSuchMethodException unused) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }
    */

    @DexIgnore
    public x2(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @DexIgnore
    public t2 a(Context context, boolean z) {
        a aVar = new a(context, z);
        aVar.setHoverListener(this);
        return aVar;
    }

    @DexIgnore
    public void b(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.I.setExitTransition((Transition) obj);
        }
    }

    @DexIgnore
    public void d(boolean z) {
        if (Build.VERSION.SDK_INT <= 28) {
            Method method = N;
            if (method != null) {
                try {
                    method.invoke(this.I, new Object[]{Boolean.valueOf(z)});
                } catch (Exception unused) {
                    Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
                }
            }
        } else {
            this.I.setTouchModal(z);
        }
    }

    @DexIgnore
    public void a(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.I.setEnterTransition((Transition) obj);
        }
    }

    @DexIgnore
    public void b(q1 q1Var, MenuItem menuItem) {
        w2 w2Var = this.M;
        if (w2Var != null) {
            w2Var.b(q1Var, menuItem);
        }
    }

    @DexIgnore
    public void a(w2 w2Var) {
        this.M = w2Var;
    }

    @DexIgnore
    public void a(q1 q1Var, MenuItem menuItem) {
        w2 w2Var = this.M;
        if (w2Var != null) {
            w2Var.a(q1Var, menuItem);
        }
    }
}
