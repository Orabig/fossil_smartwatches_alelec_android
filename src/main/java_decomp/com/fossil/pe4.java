package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pe4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ProgressBar A;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public /* final */ ViewPager2 C;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ TabLayout y;
    @DexIgnore
    public /* final */ DashBar z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pe4(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, TabLayout tabLayout, ImageView imageView, DashBar dashBar, ProgressBar progressBar, ConstraintLayout constraintLayout3, ViewPager2 viewPager2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = flexibleButton;
        this.t = flexibleButton2;
        this.u = flexibleTextView;
        this.v = flexibleTextView3;
        this.w = flexibleTextView5;
        this.x = flexibleTextView6;
        this.y = tabLayout;
        this.z = dashBar;
        this.A = progressBar;
        this.B = constraintLayout3;
        this.C = viewPager2;
    }
}
