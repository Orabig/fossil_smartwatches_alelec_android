package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pc6<A, B, C> implements Serializable {
    @DexIgnore
    public /* final */ A first;
    @DexIgnore
    public /* final */ B second;
    @DexIgnore
    public /* final */ C third;

    @DexIgnore
    public pc6(A a, B b, C c) {
        this.first = a;
        this.second = b;
        this.third = c;
    }

    @DexIgnore
    public static /* synthetic */ pc6 copy$default(pc6 pc6, A a, B b, C c, int i, Object obj) {
        if ((i & 1) != 0) {
            a = pc6.first;
        }
        if ((i & 2) != 0) {
            b = pc6.second;
        }
        if ((i & 4) != 0) {
            c = pc6.third;
        }
        return pc6.copy(a, b, c);
    }

    @DexIgnore
    public final A component1() {
        return this.first;
    }

    @DexIgnore
    public final B component2() {
        return this.second;
    }

    @DexIgnore
    public final C component3() {
        return this.third;
    }

    @DexIgnore
    public final pc6<A, B, C> copy(A a, B b, C c) {
        return new pc6<>(a, b, c);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof pc6)) {
            return false;
        }
        pc6 pc6 = (pc6) obj;
        return wg6.a((Object) this.first, (Object) pc6.first) && wg6.a((Object) this.second, (Object) pc6.second) && wg6.a((Object) this.third, (Object) pc6.third);
    }

    @DexIgnore
    public final A getFirst() {
        return this.first;
    }

    @DexIgnore
    public final B getSecond() {
        return this.second;
    }

    @DexIgnore
    public final C getThird() {
        return this.third;
    }

    @DexIgnore
    public int hashCode() {
        A a = this.first;
        int i = 0;
        int hashCode = (a != null ? a.hashCode() : 0) * 31;
        B b = this.second;
        int hashCode2 = (hashCode + (b != null ? b.hashCode() : 0)) * 31;
        C c = this.third;
        if (c != null) {
            i = c.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        return '(' + this.first + ", " + this.second + ", " + this.third + ')';
    }
}
