package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class kt3 implements kc3 {
    @DexIgnore
    public /* final */ gt3 a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public kt3(gt3 gt3, Intent intent) {
        this.a = gt3;
        this.b = intent;
    }

    @DexIgnore
    public final void onComplete(qc3 qc3) {
        this.a.a(this.b, qc3);
    }
}
