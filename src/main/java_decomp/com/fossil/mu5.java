package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.uirenew.splash.SplashPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mu5 implements Factory<lu5> {
    @DexIgnore
    public static SplashPresenter a(iu5 iu5, UserRepository userRepository, MigrationHelper migrationHelper, ThemeRepository themeRepository, an4 an4) {
        return new SplashPresenter(iu5, userRepository, migrationHelper, themeRepository, an4);
    }
}
