package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum qr {
    PREFER_ARGB_8888,
    PREFER_RGB_565;
    
    @DexIgnore
    public static /* final */ qr DEFAULT; // = null;

    /*
    static {
        qr qrVar;
        DEFAULT = qrVar;
    }
    */
}
