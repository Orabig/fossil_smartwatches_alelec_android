package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum dn1 {
    CLOCK_WISE((byte) 0),
    SHORTEST_PATH((byte) 2);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public dn1(byte b) {
        this.a = b;
    }
}
