package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x74 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ View s;
    @DexIgnore
    public /* final */ View t;

    @DexIgnore
    public x74(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleButton flexibleButton, ImageView imageView, View view2, View view3) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = imageView;
        this.s = view2;
        this.t = view3;
    }
}
