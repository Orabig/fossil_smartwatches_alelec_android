package com.fossil;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m34 extends RecyclerView.g<c> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<WatchApp> c;
    @DexIgnore
    public b d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(WatchApp watchApp);

        @DexIgnore
        void b(WatchApp watchApp);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public WatchApp e;
        @DexIgnore
        public /* final */ /* synthetic */ m34 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    wg6.a(obj, "mData[adapterPosition]");
                    d.a((WatchApp) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    wg6.a(obj, "mData[adapterPosition]");
                    d.b((WatchApp) obj);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.m34$c$c")
        /* renamed from: com.fossil.m34$c$c  reason: collision with other inner class name */
        public static final class C0029c implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public C0029c(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void a(CustomizeWidget customizeWidget) {
                wg6.b(customizeWidget, "view");
                this.a.f.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v11, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(m34 m34, View view) {
            super(view);
            wg6.b(view, "view");
            this.f = m34;
            this.c = view.findViewById(2131362603);
            this.d = view.findViewById(2131362638);
            View findViewById = view.findViewById(2131363340);
            wg6.a((Object) findViewById, "view.findViewById(R.id.wc_watch_app)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363232);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.tv_watch_app_name)");
            this.b = (TextView) findViewById2;
            this.a.setOnClickListener(new a(this));
            this.d.setOnClickListener(new b(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(WatchApp watchApp) {
            String str;
            wg6.b(watchApp, "watchApp");
            this.e = watchApp;
            if (this.e != null) {
                this.a.d(watchApp.getWatchappId());
                this.b.setText(jm4.a(PortfolioApp.get.instance(), watchApp.getNameKey(), watchApp.getName()));
            }
            this.a.setSelectedWc(wg6.a((Object) watchApp.getWatchappId(), (Object) this.f.b));
            View view = this.d;
            wg6.a((Object) view, "ivWarning");
            view.setVisibility(!dl4.c.d(watchApp.getWatchappId()) ? 0 : 8);
            if (wg6.a((Object) watchApp.getWatchappId(), (Object) this.f.b)) {
                View view2 = this.c;
                wg6.a((Object) view2, "ivIndicator");
                view2.setBackground(w6.c(PortfolioApp.get.instance(), 2131230988));
            } else {
                View view3 = this.c;
                wg6.a((Object) view3, "ivIndicator");
                view3.setBackground(w6.c(PortfolioApp.get.instance(), 2131230989));
            }
            CustomizeWidget customizeWidget = this.a;
            Intent intent = new Intent();
            WatchApp watchApp2 = this.e;
            if (watchApp2 == null || (str = watchApp2.getWatchappId()) == null) {
                str = "";
            }
            Intent putExtra = intent.putExtra("KEY_ID", str);
            wg6.a((Object) putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.a(customizeWidget, "WATCH_APP", putExtra, (View.OnDragListener) null, new C0029c(this), 4, (Object) null);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ m34(ArrayList arrayList, b bVar, int i, qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : bVar);
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d("WatchAppsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public final b d() {
        return this.d;
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "watchAppId");
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558689, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026watch_app, parent, false)");
        return new c(this, inflate);
    }

    @DexIgnore
    public m34(ArrayList<WatchApp> arrayList, b bVar) {
        wg6.b(arrayList, "mData");
        this.c = arrayList;
        this.d = bVar;
        this.b = "empty";
    }

    @DexIgnore
    public final void a(List<WatchApp> list) {
        wg6.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        wg6.b(str, "watchAppId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wg6.a((Object) ((WatchApp) t).getWatchappId(), (Object) str)) {
                break;
            }
        }
        WatchApp watchApp = (WatchApp) t;
        if (watchApp != null) {
            return this.c.indexOf(watchApp);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        wg6.b(cVar, "holder");
        if (getItemCount() > i && i != -1) {
            WatchApp watchApp = this.c.get(i);
            wg6.a((Object) watchApp, "mData[position]");
            cVar.a(watchApp);
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "listener");
        this.d = bVar;
    }
}
