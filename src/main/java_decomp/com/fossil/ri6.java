package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ri6<T> implements ti6<T> {
    @DexIgnore
    public /* final */ ti6<T> a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ hg6<T, Boolean> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, ph6 {
        @DexIgnore
        public /* final */ Iterator<T> a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public T c;
        @DexIgnore
        public /* final */ /* synthetic */ ri6 d;

        @DexIgnore
        public a(ri6 ri6) {
            this.d = ri6;
            this.a = ri6.a.iterator();
        }

        @DexIgnore
        public final void a() {
            while (this.a.hasNext()) {
                T next = this.a.next();
                if (((Boolean) this.d.c.invoke(next)).booleanValue() == this.d.b) {
                    this.c = next;
                    this.b = 1;
                    return;
                }
            }
            this.b = 0;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.b == -1) {
                a();
            }
            return this.b == 1;
        }

        @DexIgnore
        public T next() {
            if (this.b == -1) {
                a();
            }
            if (this.b != 0) {
                T t = this.c;
                this.c = null;
                this.b = -1;
                return t;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    public ri6(ti6<? extends T> ti6, boolean z, hg6<? super T, Boolean> hg6) {
        wg6.b(ti6, "sequence");
        wg6.b(hg6, "predicate");
        this.a = ti6;
        this.b = z;
        this.c = hg6;
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new a(this);
    }
}
