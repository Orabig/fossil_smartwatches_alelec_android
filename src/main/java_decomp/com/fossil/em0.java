package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class em0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ lk0 CREATOR; // = new lk0((qg6) null);
    @DexIgnore
    public /* final */ xh0 a;
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public em0(xh0 xh0, byte b2) {
        this.a = xh0;
        this.b = b2;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.NAME, (Object) cw0.a((Enum<?>) this.a)), bm0.SEQUENCE, (Object) Byte.valueOf(this.b));
    }

    @DexIgnore
    public byte[] b() {
        return new byte[0];
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public em0(Parcel parcel) {
        this(xh0.valueOf(r0), parcel.readByte());
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            return;
        }
        wg6.a();
        throw null;
    }
}
