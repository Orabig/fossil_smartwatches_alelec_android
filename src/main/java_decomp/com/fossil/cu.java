package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cu implements wt<byte[]> {
    @DexIgnore
    public int a() {
        return 1;
    }

    @DexIgnore
    public String b() {
        return "ByteArrayPool";
    }

    @DexIgnore
    public int a(byte[] bArr) {
        return bArr.length;
    }

    @DexIgnore
    public byte[] newArray(int i) {
        return new byte[i];
    }
}
