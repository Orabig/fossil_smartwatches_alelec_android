package com.fossil;

import android.animation.LayoutTransition;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gl {
    @DexIgnore
    public static /* final */ ViewGroup.MarginLayoutParams b; // = new ViewGroup.MarginLayoutParams(-1, -1);
    @DexIgnore
    public LinearLayoutManager a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Comparator<int[]> {
        @DexIgnore
        public a(gl glVar) {
        }

        @DexIgnore
        /* renamed from: a */
        public int compare(int[] iArr, int[] iArr2) {
            return iArr[0] - iArr2[0];
        }
    }

    /*
    static {
        b.setMargins(0, 0, 0, 0);
    }
    */

    @DexIgnore
    public gl(LinearLayoutManager linearLayoutManager) {
        this.a = linearLayoutManager;
    }

    @DexIgnore
    public final boolean a() {
        ViewGroup.MarginLayoutParams marginLayoutParams;
        int i;
        int i2;
        int i3;
        int i4;
        int e = this.a.e();
        if (e == 0) {
            return true;
        }
        boolean z = this.a.Q() == 0;
        int[][] iArr = (int[][]) Array.newInstance(int.class, new int[]{e, 2});
        int i5 = 0;
        while (i5 < e) {
            View d = this.a.d(i5);
            if (d != null) {
                ViewGroup.LayoutParams layoutParams = d.getLayoutParams();
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                } else {
                    marginLayoutParams = b;
                }
                int[] iArr2 = iArr[i5];
                if (z) {
                    i2 = d.getLeft();
                    i = marginLayoutParams.leftMargin;
                } else {
                    i2 = d.getTop();
                    i = marginLayoutParams.topMargin;
                }
                iArr2[0] = i2 - i;
                int[] iArr3 = iArr[i5];
                if (z) {
                    i4 = d.getRight();
                    i3 = marginLayoutParams.rightMargin;
                } else {
                    i4 = d.getBottom();
                    i3 = marginLayoutParams.bottomMargin;
                }
                iArr3[1] = i4 + i3;
                i5++;
            } else {
                throw new IllegalStateException("null view contained in the view hierarchy");
            }
        }
        Arrays.sort(iArr, new a(this));
        for (int i6 = 1; i6 < e; i6++) {
            if (iArr[i6 - 1][1] != iArr[i6][0]) {
                return false;
            }
        }
        int i7 = iArr[0][1] - iArr[0][0];
        if (iArr[0][0] > 0 || iArr[e - 1][1] < i7) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean b() {
        int e = this.a.e();
        for (int i = 0; i < e; i++) {
            if (a(this.a.d(i))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean c() {
        if ((!a() || this.a.e() <= 1) && b()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean a(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            LayoutTransition layoutTransition = viewGroup.getLayoutTransition();
            if (layoutTransition != null && layoutTransition.isChangingLayout()) {
                return true;
            }
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                if (a(viewGroup.getChildAt(i))) {
                    return true;
                }
            }
        }
        return false;
    }
}
