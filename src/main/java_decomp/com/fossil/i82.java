package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i82 implements Parcelable.Creator<j82> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        IBinder iBinder = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            if (f22.a(a) != 1) {
                f22.v(parcel, a);
            } else {
                iBinder = f22.p(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new j82(iBinder);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new j82[i];
    }
}
