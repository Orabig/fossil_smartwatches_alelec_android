package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hq4<T, A> {
    @DexIgnore
    public hg6<? super A, ? extends T> a;
    @DexIgnore
    public volatile T b;

    @DexIgnore
    public hq4(hg6<? super A, ? extends T> hg6) {
        wg6.b(hg6, "creator");
        this.a = hg6;
    }

    @DexIgnore
    public final T a(A a2) {
        T t;
        T t2 = this.b;
        if (t2 != null) {
            return t2;
        }
        synchronized (this) {
            t = this.b;
            if (t == null) {
                hg6 hg6 = this.a;
                if (hg6 != null) {
                    t = hg6.invoke(a2);
                    this.b = t;
                    this.a = null;
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        return t;
    }
}
