package com.fossil;

import com.zendesk.sdk.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x24 {
    @DexIgnore
    public static /* final */ int[] ActionBar; // = {2130968768, 2130968776, 2130968777, 2130969054, 2130969055, 2130969056, 2130969057, 2130969058, 2130969059, 2130969151, 2130969225, 2130969227, 2130969258, 2130969379, 2130969387, 2130969395, 2130969396, 2130969398, 2130969420, 2130969454, 2130969576, 2130969637, 2130969677, 2130969689, 2130969690, 2130969902, 2130969905, 2130970031, 2130970041};
    @DexIgnore
    public static /* final */ int[] ActionBarLayout; // = {16842931};
    @DexIgnore
    public static /* final */ int ActionBarLayout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int ActionBar_background; // = 0;
    @DexIgnore
    public static /* final */ int ActionBar_backgroundSplit; // = 1;
    @DexIgnore
    public static /* final */ int ActionBar_backgroundStacked; // = 2;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetEnd; // = 3;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetEndWithActions; // = 4;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetLeft; // = 5;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetRight; // = 6;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetStart; // = 7;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetStartWithNavigation; // = 8;
    @DexIgnore
    public static /* final */ int ActionBar_customNavigationLayout; // = 9;
    @DexIgnore
    public static /* final */ int ActionBar_displayOptions; // = 10;
    @DexIgnore
    public static /* final */ int ActionBar_divider; // = 11;
    @DexIgnore
    public static /* final */ int ActionBar_elevation; // = 12;
    @DexIgnore
    public static /* final */ int ActionBar_height; // = 13;
    @DexIgnore
    public static /* final */ int ActionBar_hideOnContentScroll; // = 14;
    @DexIgnore
    public static /* final */ int ActionBar_homeAsUpIndicator; // = 15;
    @DexIgnore
    public static /* final */ int ActionBar_homeLayout; // = 16;
    @DexIgnore
    public static /* final */ int ActionBar_icon; // = 17;
    @DexIgnore
    public static /* final */ int ActionBar_indeterminateProgressStyle; // = 18;
    @DexIgnore
    public static /* final */ int ActionBar_itemPadding; // = 19;
    @DexIgnore
    public static /* final */ int ActionBar_logo; // = 20;
    @DexIgnore
    public static /* final */ int ActionBar_navigationMode; // = 21;
    @DexIgnore
    public static /* final */ int ActionBar_popupTheme; // = 22;
    @DexIgnore
    public static /* final */ int ActionBar_progressBarPadding; // = 23;
    @DexIgnore
    public static /* final */ int ActionBar_progressBarStyle; // = 24;
    @DexIgnore
    public static /* final */ int ActionBar_subtitle; // = 25;
    @DexIgnore
    public static /* final */ int ActionBar_subtitleTextStyle; // = 26;
    @DexIgnore
    public static /* final */ int ActionBar_title; // = 27;
    @DexIgnore
    public static /* final */ int ActionBar_titleTextStyle; // = 28;
    @DexIgnore
    public static /* final */ int[] ActionMenuItemView; // = {16843071};
    @DexIgnore
    public static /* final */ int ActionMenuItemView_android_minWidth; // = 0;
    @DexIgnore
    public static /* final */ int[] ActionMenuView; // = new int[0];
    @DexIgnore
    public static /* final */ int[] ActionMode; // = {2130968768, 2130968776, 2130968994, 2130969379, 2130969905, 2130970041};
    @DexIgnore
    public static /* final */ int ActionMode_background; // = 0;
    @DexIgnore
    public static /* final */ int ActionMode_backgroundSplit; // = 1;
    @DexIgnore
    public static /* final */ int ActionMode_closeItemLayout; // = 2;
    @DexIgnore
    public static /* final */ int ActionMode_height; // = 3;
    @DexIgnore
    public static /* final */ int ActionMode_subtitleTextStyle; // = 4;
    @DexIgnore
    public static /* final */ int ActionMode_titleTextStyle; // = 5;
    @DexIgnore
    public static /* final */ int[] ActivityChooserView; // = {2130969288, 2130969431};
    @DexIgnore
    public static /* final */ int ActivityChooserView_expandActivityOverflowButtonDrawable; // = 0;
    @DexIgnore
    public static /* final */ int ActivityChooserView_initialActivityCount; // = 1;
    @DexIgnore
    public static /* final */ int[] ActivityDayDetailsChart; // = {2130968702, 2130968703, 2130968704, 2130968705, 2130968706, 2130968707, 2130968708, 2130968709, 2130968710, 2130968711};
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_active_time_color; // = 0;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_calories_color; // = 1;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_color; // = 2;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_steps_intense_color; // = 3;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_steps_light_color; // = 4;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_bar_steps_moderate_color; // = 5;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_dash_line_color; // = 6;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_line_color; // = 7;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_text_color; // = 8;
    @DexIgnore
    public static /* final */ int ActivityDayDetailsChart_activity_day_text_size; // = 9;
    @DexIgnore
    public static /* final */ int[] ActivityHorizontalBar; // = {2130968701, 2130968712, 2130968720, 2130968721, 2130968722, 2130968723, 2130968724, 2130968725, 2130968726, 2130968727, 2130968728, 2130968729};
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_background_color; // = 0;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_fontFamily; // = 1;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_progress_color; // = 2;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_progress_margin_end; // = 3;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_progress_radius; // = 4;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_progress_width; // = 5;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_star_alpha; // = 6;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_star_res; // = 7;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_star_size; // = 8;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_text; // = 9;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_text_color; // = 10;
    @DexIgnore
    public static /* final */ int ActivityHorizontalBar_activity_text_size; // = 11;
    @DexIgnore
    public static /* final */ int[] ActivityMonthDetailsChart; // = {2130968713, 2130968714, 2130968715, 2130968716, 2130968717, 2130968718, 2130968719};
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_bar_active_color; // = 0;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_bar_color; // = 1;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_dash_line_color; // = 2;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_line_color; // = 3;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_text_color; // = 4;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_text_font; // = 5;
    @DexIgnore
    public static /* final */ int ActivityMonthDetailsChart_activity_month_text_size; // = 6;
    @DexIgnore
    public static /* final */ int[] ActivityWeekDetailsChart; // = {2130968730, 2130968731, 2130968732, 2130968733, 2130968734, 2130968735, 2130968736};
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_bar_active_color; // = 0;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_bar_color; // = 1;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_dash_line_color; // = 2;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_line_color; // = 3;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_text_color; // = 4;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_text_font; // = 5;
    @DexIgnore
    public static /* final */ int ActivityWeekDetailsChart_activity_week_text_size; // = 6;
    @DexIgnore
    public static /* final */ int[] AlertDialog; // = {16842994, 2130968894, 2130968895, 2130969561, 2130969562, 2130969630, R.attr.showTitle, 2130969798};
    @DexIgnore
    public static /* final */ int AlertDialog_android_layout; // = 0;
    @DexIgnore
    public static /* final */ int AlertDialog_buttonIconDimen; // = 1;
    @DexIgnore
    public static /* final */ int AlertDialog_buttonPanelSideLayout; // = 2;
    @DexIgnore
    public static /* final */ int AlertDialog_listItemLayout; // = 3;
    @DexIgnore
    public static /* final */ int AlertDialog_listLayout; // = 4;
    @DexIgnore
    public static /* final */ int AlertDialog_multiChoiceItemLayout; // = 5;
    @DexIgnore
    public static /* final */ int AlertDialog_showTitle; // = 6;
    @DexIgnore
    public static /* final */ int AlertDialog_singleChoiceItemLayout; // = 7;
    @DexIgnore
    public static /* final */ int[] AlphabetFastScrollRecyclerView; // = {2130969773, 2130969774, 2130969775, 2130969776, 2130969777, 2130969778, 2130969779, 2130969780, 2130969781, 2130969782, 2130969783, 2130969784, 2130969993, 2130969994};
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarColor; // = 0;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarColorRes; // = 1;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarCornerRadius; // = 2;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarHighlightTextColor; // = 3;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarHighlightTextColorRes; // = 4;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarTextColor; // = 5;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarTextColorRes; // = 6;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexBarTransparentValue; // = 7;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexTextSize; // = 8;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexbarMargin; // = 9;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setIndexbarWidth; // = 10;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_setPreviewPadding; // = 11;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_textIndexColorStyle; // = 12;
    @DexIgnore
    public static /* final */ int AlphabetFastScrollRecyclerView_textIndexFontStyle; // = 13;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableCompat; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_visible; // = 1;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableItem; // = {16842960, 16843161};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_drawable; // = 1;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_id; // = 0;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableTransition; // = {16843161, 16843849, 16843850, 16843851};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_fromId; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_reversible; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_toId; // = 1;
    @DexIgnore
    public static /* final */ int[] AnimationImageView; // = {2130969345, 2130969417, 2130969418, 2130969876};
    @DexIgnore
    public static /* final */ int AnimationImageView_fps; // = 0;
    @DexIgnore
    public static /* final */ int AnimationImageView_imagesAmount; // = 1;
    @DexIgnore
    public static /* final */ int AnimationImageView_imagesPathFromAssetsWithoutFormatted; // = 2;
    @DexIgnore
    public static /* final */ int AnimationImageView_startImageIndex; // = 3;
    @DexIgnore
    public static /* final */ int[] AnnularView; // = {2130969004, 2130969359, 2130969896};
    @DexIgnore
    public static /* final */ int AnnularView_colorArray; // = 0;
    @DexIgnore
    public static /* final */ int AnnularView_handColor; // = 1;
    @DexIgnore
    public static /* final */ int AnnularView_strokeWidth; // = 2;
    @DexIgnore
    public static /* final */ int[] AppBarLayout; // = {16842964, 16843919, 16844096, 2130969258, R.attr.expanded, 2130969550, 2130969551, 2130969889};
    @DexIgnore
    public static /* final */ int[] AppBarLayoutStates; // = {R.attr.state_collapsed, R.attr.state_collapsible, 2130969881, 2130969882};
    @DexIgnore
    public static /* final */ int AppBarLayoutStates_state_collapsed; // = 0;
    @DexIgnore
    public static /* final */ int AppBarLayoutStates_state_collapsible; // = 1;
    @DexIgnore
    public static /* final */ int AppBarLayoutStates_state_liftable; // = 2;
    @DexIgnore
    public static /* final */ int AppBarLayoutStates_state_lifted; // = 3;
    @DexIgnore
    public static /* final */ int[] AppBarLayout_Layout; // = {R.attr.layout_scrollFlags, R.attr.layout_scrollInterpolator};
    @DexIgnore
    public static /* final */ int AppBarLayout_Layout_layout_scrollFlags; // = 0;
    @DexIgnore
    public static /* final */ int AppBarLayout_Layout_layout_scrollInterpolator; // = 1;
    @DexIgnore
    public static /* final */ int AppBarLayout_android_background; // = 0;
    @DexIgnore
    public static /* final */ int AppBarLayout_android_keyboardNavigationCluster; // = 2;
    @DexIgnore
    public static /* final */ int AppBarLayout_android_touchscreenBlocksFocus; // = 1;
    @DexIgnore
    public static /* final */ int AppBarLayout_elevation; // = 3;
    @DexIgnore
    public static /* final */ int AppBarLayout_expanded; // = 4;
    @DexIgnore
    public static /* final */ int AppBarLayout_liftOnScroll; // = 5;
    @DexIgnore
    public static /* final */ int AppBarLayout_liftOnScrollTargetViewId; // = 6;
    @DexIgnore
    public static /* final */ int AppBarLayout_statusBarForeground; // = 7;
    @DexIgnore
    public static /* final */ int[] AppCompatImageView; // = {16843033, 2130969868, 2130970028, 2130970029};
    @DexIgnore
    public static /* final */ int AppCompatImageView_android_src; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatImageView_srcCompat; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatImageView_tint; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatImageView_tintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] AppCompatSeekBar; // = {16843074, 2130970024, 2130970025, 2130970026};
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_android_thumb; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMark; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMarkTint; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMarkTintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] AppCompatTextHelper; // = {16842804, 16843117, 16843118, 16843119, 16843120, 16843666, 16843667};
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableBottom; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableEnd; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableLeft; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableRight; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableStart; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableTop; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int[] AppCompatTextView; // = {16842804, 2130968759, 2130968760, 2130968761, 2130968762, 2130968763, 2130969233, 2130969234, 2130969235, 2130969236, 2130969238, 2130969239, 2130969240, 2130969241, 2130969315, 2130969327, com.misfit.frameworks.buttonservice.R.attr.fontVariationSettings, 2130969472, 2130969554, 2130969952, 2130969997};
    @DexIgnore
    public static /* final */ int AppCompatTextView_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeMaxTextSize; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeMinTextSize; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizePresetSizes; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeStepGranularity; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeTextType; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableBottomCompat; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableEndCompat; // = 7;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableLeftCompat; // = 8;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableRightCompat; // = 9;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableStartCompat; // = 10;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableTint; // = 11;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableTintMode; // = 12;
    @DexIgnore
    public static /* final */ int AppCompatTextView_drawableTopCompat; // = 13;
    @DexIgnore
    public static /* final */ int AppCompatTextView_firstBaselineToTopHeight; // = 14;
    @DexIgnore
    public static /* final */ int AppCompatTextView_fontFamily; // = 15;
    @DexIgnore
    public static /* final */ int AppCompatTextView_fontVariationSettings; // = 16;
    @DexIgnore
    public static /* final */ int AppCompatTextView_lastBaselineToBottomHeight; // = 17;
    @DexIgnore
    public static /* final */ int AppCompatTextView_lineHeight; // = 18;
    @DexIgnore
    public static /* final */ int AppCompatTextView_textAllCaps; // = 19;
    @DexIgnore
    public static /* final */ int AppCompatTextView_textLocale; // = 20;
    @DexIgnore
    public static /* final */ int[] AppCompatTheme; // = {16842839, 16842926, 2130968646, 2130968647, 2130968648, 2130968649, 2130968650, 2130968651, 2130968652, 2130968653, 2130968654, 2130968655, 2130968656, 2130968657, 2130968658, 2130968660, 2130968661, 2130968662, 2130968663, 2130968664, 2130968665, 2130968666, 2130968667, 2130968668, 2130968669, 2130968670, 2130968671, 2130968672, 2130968673, 2130968674, 2130968675, 2130968676, 2130968688, 2130968738, 2130968739, 2130968740, 2130968741, 2130968757, 2130968867, 2130968887, 2130968888, 2130968889, 2130968890, 2130968891, 2130968903, 2130968904, 2130968943, 2130968950, 2130969003, 2130969005, 2130969006, 2130969007, 2130969008, 2130969009, 2130969010, 2130969017, 2130969018, 2130969025, 2130969066, 2130969222, 2130969223, 2130969224, 2130969228, 2130969230, 2130969243, 2130969244, 2130969247, 2130969248, 2130969250, 2130969395, 2130969415, 2130969557, 2130969558, 2130969559, 2130969560, 2130969563, 2130969564, 2130969565, 2130969566, 2130969567, 2130969568, 2130969569, 2130969570, 2130969571, 2130969665, 2130969666, 2130969667, 2130969676, 2130969678, 2130969705, 2130969708, 2130969709, 2130969710, 2130969761, 2130969762, 2130969763, 2130969764, 2130969865, 2130969866, 2130969912, 2130969963, 2130969965, 2130969966, 2130969967, 2130969969, 2130969970, 2130969971, 2130969972, 2130969984, 2130969985, 2130970049, 2130970050, 2130970051, 2130970052, 2130970079, 2130970162, 2130970163, 2130970164, 2130970165, 2130970166, 2130970167, 2130970168, 2130970169, 2130970170, 2130970171};
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarDivider; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarItemBackground; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarPopupTheme; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarSize; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarSplitStyle; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarStyle; // = 7;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabBarStyle; // = 8;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabStyle; // = 9;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabTextStyle; // = 10;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTheme; // = 11;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarWidgetTheme; // = 12;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionButtonStyle; // = 13;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionDropDownStyle; // = 14;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionMenuTextAppearance; // = 15;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionMenuTextColor; // = 16;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeBackground; // = 17;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCloseButtonStyle; // = 18;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCloseDrawable; // = 19;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCopyDrawable; // = 20;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCutDrawable; // = 21;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeFindDrawable; // = 22;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModePasteDrawable; // = 23;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModePopupWindowStyle; // = 24;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeSelectAllDrawable; // = 25;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeShareDrawable; // = 26;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeSplitBackground; // = 27;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeStyle; // = 28;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeWebSearchDrawable; // = 29;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionOverflowButtonStyle; // = 30;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionOverflowMenuStyle; // = 31;
    @DexIgnore
    public static /* final */ int AppCompatTheme_activityChooserViewStyle; // = 32;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogButtonGroupStyle; // = 33;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogCenterButtons; // = 34;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogStyle; // = 35;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogTheme; // = 36;
    @DexIgnore
    public static /* final */ int AppCompatTheme_android_windowAnimationStyle; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTheme_android_windowIsFloating; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatTheme_autoCompleteTextViewStyle; // = 37;
    @DexIgnore
    public static /* final */ int AppCompatTheme_borderlessButtonStyle; // = 38;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarButtonStyle; // = 39;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarNegativeButtonStyle; // = 40;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarNeutralButtonStyle; // = 41;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarPositiveButtonStyle; // = 42;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarStyle; // = 43;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonStyle; // = 44;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonStyleSmall; // = 45;
    @DexIgnore
    public static /* final */ int AppCompatTheme_checkboxStyle; // = 46;
    @DexIgnore
    public static /* final */ int AppCompatTheme_checkedTextViewStyle; // = 47;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorAccent; // = 48;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorBackgroundFloating; // = 49;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorButtonNormal; // = 50;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlActivated; // = 51;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlHighlight; // = 52;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlNormal; // = 53;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorError; // = 54;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorPrimary; // = 55;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorPrimaryDark; // = 56;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorSwitchThumbNormal; // = 57;
    @DexIgnore
    public static /* final */ int AppCompatTheme_controlBackground; // = 58;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogCornerRadius; // = 59;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogPreferredPadding; // = 60;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogTheme; // = 61;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dividerHorizontal; // = 62;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dividerVertical; // = 63;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dropDownListViewStyle; // = 64;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dropdownListPreferredItemHeight; // = 65;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextBackground; // = 66;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextColor; // = 67;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextStyle; // = 68;
    @DexIgnore
    public static /* final */ int AppCompatTheme_homeAsUpIndicator; // = 69;
    @DexIgnore
    public static /* final */ int AppCompatTheme_imageButtonStyle; // = 70;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listChoiceBackgroundIndicator; // = 71;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listChoiceIndicatorMultipleAnimated; // = 72;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listChoiceIndicatorSingleAnimated; // = 73;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listDividerAlertDialog; // = 74;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listMenuViewStyle; // = 75;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPopupWindowStyle; // = 76;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeight; // = 77;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeightLarge; // = 78;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeightSmall; // = 79;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingEnd; // = 80;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingLeft; // = 81;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingRight; // = 82;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingStart; // = 83;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelBackground; // = 84;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelMenuListTheme; // = 85;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelMenuListWidth; // = 86;
    @DexIgnore
    public static /* final */ int AppCompatTheme_popupMenuStyle; // = 87;
    @DexIgnore
    public static /* final */ int AppCompatTheme_popupWindowStyle; // = 88;
    @DexIgnore
    public static /* final */ int AppCompatTheme_radioButtonStyle; // = 89;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyle; // = 90;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyleIndicator; // = 91;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyleSmall; // = 92;
    @DexIgnore
    public static /* final */ int AppCompatTheme_searchViewStyle; // = 93;
    @DexIgnore
    public static /* final */ int AppCompatTheme_seekBarStyle; // = 94;
    @DexIgnore
    public static /* final */ int AppCompatTheme_selectableItemBackground; // = 95;
    @DexIgnore
    public static /* final */ int AppCompatTheme_selectableItemBackgroundBorderless; // = 96;
    @DexIgnore
    public static /* final */ int AppCompatTheme_spinnerDropDownItemStyle; // = 97;
    @DexIgnore
    public static /* final */ int AppCompatTheme_spinnerStyle; // = 98;
    @DexIgnore
    public static /* final */ int AppCompatTheme_switchStyle; // = 99;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceLargePopupMenu; // = 100;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItem; // = 101;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItemSecondary; // = 102;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItemSmall; // = 103;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearancePopupMenuHeader; // = 104;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSearchResultSubtitle; // = 105;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSearchResultTitle; // = 106;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSmallPopupMenu; // = 107;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textColorAlertDialogListItem; // = 108;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textColorSearchUrl; // = 109;
    @DexIgnore
    public static /* final */ int AppCompatTheme_toolbarNavigationButtonStyle; // = 110;
    @DexIgnore
    public static /* final */ int AppCompatTheme_toolbarStyle; // = 111;
    @DexIgnore
    public static /* final */ int AppCompatTheme_tooltipForegroundColor; // = 112;
    @DexIgnore
    public static /* final */ int AppCompatTheme_tooltipFrameBackground; // = 113;
    @DexIgnore
    public static /* final */ int AppCompatTheme_viewInflaterClass; // = 114;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionBar; // = 115;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionBarOverlay; // = 116;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionModeOverlay; // = 117;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedHeightMajor; // = 118;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedHeightMinor; // = 119;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedWidthMajor; // = 120;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedWidthMinor; // = 121;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowMinWidthMajor; // = 122;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowMinWidthMinor; // = 123;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowNoTitle; // = 124;
    @DexIgnore
    public static /* final */ int[] AutoResizeTextView; // = {2130969626};
    @DexIgnore
    public static /* final */ int AutoResizeTextView_minTextSize; // = 0;
    @DexIgnore
    public static /* final */ int[] Badge; // = {2130968769, 2130968782, 2130968784, 2130969612, 2130969647};
    @DexIgnore
    public static /* final */ int Badge_backgroundColor; // = 0;
    @DexIgnore
    public static /* final */ int Badge_badgeGravity; // = 1;
    @DexIgnore
    public static /* final */ int Badge_badgeTextColor; // = 2;
    @DexIgnore
    public static /* final */ int Badge_maxCharacterCount; // = 3;
    @DexIgnore
    public static /* final */ int Badge_number; // = 4;
    @DexIgnore
    public static /* final */ int[] BarChart; // = {2130968786, 2130968787, 2130968788, 2130968789, 2130968790, 2130968791, 2130968792, 2130968793, 2130968796, 2130968797, 2130968798, 2130968799, 2130968800, 2130968801, 2130968802};
    @DexIgnore
    public static /* final */ int BarChart_bar_end_margin; // = 0;
    @DexIgnore
    public static /* final */ int BarChart_bar_goal_line_color; // = 1;
    @DexIgnore
    public static /* final */ int BarChart_bar_goal_line_dash_gap; // = 2;
    @DexIgnore
    public static /* final */ int BarChart_bar_goal_line_dash_width; // = 3;
    @DexIgnore
    public static /* final */ int BarChart_bar_goal_line_width; // = 4;
    @DexIgnore
    public static /* final */ int BarChart_bar_icon_drawable; // = 5;
    @DexIgnore
    public static /* final */ int BarChart_bar_icon_padding; // = 6;
    @DexIgnore
    public static /* final */ int BarChart_bar_icon_size; // = 7;
    @DexIgnore
    public static /* final */ int BarChart_bar_start_margin; // = 8;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_color; // = 9;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_font; // = 10;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_height; // = 11;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_margin; // = 12;
    @DexIgnore
    public static /* final */ int BarChart_bar_text_size; // = 13;
    @DexIgnore
    public static /* final */ int BarChart_bar_width; // = 14;
    @DexIgnore
    public static /* final */ int[] BaseChart; // = {2130968814, 2130968815, 2130968816, 2130968817, 2130968818, 2130968819, 2130968820, 2130968821, 2130968822, 2130968823, 2130968824, 2130968825, 2130968826, 2130968827, 2130968828, 2130968829, 2130968830, 2130968831, 2130968832, 2130968833, 2130968834, 2130968835, 2130968836, 2130968837, 2130968838, 2130968839, 2130968840, 2130968841, 2130968842, 2130968843, 2130968844, 2130968845, 2130968846, 2130968847};
    @DexIgnore
    public static /* final */ int BaseChart_bc_active_color; // = 0;
    @DexIgnore
    public static /* final */ int BaseChart_bc_background_color; // = 1;
    @DexIgnore
    public static /* final */ int BaseChart_bc_default_color; // = 2;
    @DexIgnore
    public static /* final */ int BaseChart_bc_flexible_size; // = 3;
    @DexIgnore
    public static /* final */ int BaseChart_bc_fontFamily; // = 4;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_icon_res; // = 5;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_icon_size; // = 6;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_line_color; // = 7;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_line_dash_gap; // = 8;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_line_dash_width; // = 9;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_line_width; // = 10;
    @DexIgnore
    public static /* final */ int BaseChart_bc_goal_text_format; // = 11;
    @DexIgnore
    public static /* final */ int BaseChart_bc_graph_legend_margin; // = 12;
    @DexIgnore
    public static /* final */ int BaseChart_bc_highest_color; // = 13;
    @DexIgnore
    public static /* final */ int BaseChart_bc_inactive_color; // = 14;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_height; // = 15;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_icon_res; // = 16;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_line_color; // = 17;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_line_width; // = 18;
    @DexIgnore
    public static /* final */ int BaseChart_bc_legend_texts; // = 19;
    @DexIgnore
    public static /* final */ int BaseChart_bc_lowest_color; // = 20;
    @DexIgnore
    public static /* final */ int BaseChart_bc_margin; // = 21;
    @DexIgnore
    public static /* final */ int BaseChart_bc_margin_end; // = 22;
    @DexIgnore
    public static /* final */ int BaseChart_bc_radius; // = 23;
    @DexIgnore
    public static /* final */ int BaseChart_bc_safe_area_height; // = 24;
    @DexIgnore
    public static /* final */ int BaseChart_bc_safe_area_width; // = 25;
    @DexIgnore
    public static /* final */ int BaseChart_bc_space; // = 26;
    @DexIgnore
    public static /* final */ int BaseChart_bc_star_icon_res; // = 27;
    @DexIgnore
    public static /* final */ int BaseChart_bc_star_icon_size; // = 28;
    @DexIgnore
    public static /* final */ int BaseChart_bc_text_color; // = 29;
    @DexIgnore
    public static /* final */ int BaseChart_bc_text_margin; // = 30;
    @DexIgnore
    public static /* final */ int BaseChart_bc_text_size; // = 31;
    @DexIgnore
    public static /* final */ int BaseChart_bc_touch_padding; // = 32;
    @DexIgnore
    public static /* final */ int BaseChart_bc_width; // = 33;
    @DexIgnore
    public static /* final */ int[] BatteryIndicator; // = {2130968806, 2130968807, 2130969439};
    @DexIgnore
    public static /* final */ int[] BatteryIndicatorAsset; // = {2130968808, 2130968809, 2130968810, 2130968811, 2130968812, 2130968813};
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_0; // = 0;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_100; // = 1;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_25; // = 2;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_50; // = 3;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryPercentage_75; // = 4;
    @DexIgnore
    public static /* final */ int BatteryIndicatorAsset_batteryRealPercentage; // = 5;
    @DexIgnore
    public static /* final */ int BatteryIndicator_batteryAlertPercentage; // = 0;
    @DexIgnore
    public static /* final */ int BatteryIndicator_batteryPercentage; // = 1;
    @DexIgnore
    public static /* final */ int BatteryIndicator_isDeviceConnected; // = 2;
    @DexIgnore
    public static /* final */ int[] BlurView; // = {2130968859, 2130968860, 2130968861};
    @DexIgnore
    public static /* final */ int BlurView_blurDownSampleFactor; // = 0;
    @DexIgnore
    public static /* final */ int BlurView_blurOverlayColor; // = 1;
    @DexIgnore
    public static /* final */ int BlurView_blurRadius; // = 2;
    @DexIgnore
    public static /* final */ int[] BottomAppBar; // = {2130968779, 2130969258, 2130969299, 2130969300, 2130969301, 2130969302, 2130969303, 2130969388};
    @DexIgnore
    public static /* final */ int BottomAppBar_backgroundTint; // = 0;
    @DexIgnore
    public static /* final */ int BottomAppBar_elevation; // = 1;
    @DexIgnore
    public static /* final */ int BottomAppBar_fabAlignmentMode; // = 2;
    @DexIgnore
    public static /* final */ int BottomAppBar_fabAnimationMode; // = 3;
    @DexIgnore
    public static /* final */ int BottomAppBar_fabCradleMargin; // = 4;
    @DexIgnore
    public static /* final */ int BottomAppBar_fabCradleRoundedCornerRadius; // = 5;
    @DexIgnore
    public static /* final */ int BottomAppBar_fabCradleVerticalOffset; // = 6;
    @DexIgnore
    public static /* final */ int BottomAppBar_hideOnScroll; // = 7;
    @DexIgnore
    public static /* final */ int[] BottomNavigationView; // = {2130968779, 2130969258, R.attr.itemBackground, 2130969449, 2130969451, R.attr.itemIconTint, 2130969455, 2130969467, 2130969468, R.attr.itemTextColor, 2130969471, R.attr.menu};
    @DexIgnore
    public static /* final */ int BottomNavigationView_backgroundTint; // = 0;
    @DexIgnore
    public static /* final */ int BottomNavigationView_elevation; // = 1;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemBackground; // = 2;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemHorizontalTranslationEnabled; // = 3;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemIconSize; // = 4;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemIconTint; // = 5;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemRippleColor; // = 6;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemTextAppearanceActive; // = 7;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemTextAppearanceInactive; // = 8;
    @DexIgnore
    public static /* final */ int BottomNavigationView_itemTextColor; // = 9;
    @DexIgnore
    public static /* final */ int BottomNavigationView_labelVisibilityMode; // = 10;
    @DexIgnore
    public static /* final */ int BottomNavigationView_menu; // = 11;
    @DexIgnore
    public static /* final */ int[] BottomSheetBehavior_Layout; // = {16843840, 2130968779, 2130968850, 2130968851, 2130968852, R.attr.behavior_hideable, R.attr.behavior_peekHeight, 2130968856, R.attr.behavior_skipCollapsed, 2130969786, 2130969789};
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_android_elevation; // = 0;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_backgroundTint; // = 1;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_expandedOffset; // = 2;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_fitToContents; // = 3;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_halfExpandedRatio; // = 4;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_hideable; // = 5;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_peekHeight; // = 6;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_saveFlags; // = 7;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_behavior_skipCollapsed; // = 8;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_shapeAppearance; // = 9;
    @DexIgnore
    public static /* final */ int BottomSheetBehavior_Layout_shapeAppearanceOverlay; // = 10;
    @DexIgnore
    public static /* final */ int[] ButtonBarContainerTheme; // = {2130969623, 2130969624};
    @DexIgnore
    public static /* final */ int ButtonBarContainerTheme_metaButtonBarButtonStyle; // = 0;
    @DexIgnore
    public static /* final */ int ButtonBarContainerTheme_metaButtonBarStyle; // = 1;
    @DexIgnore
    public static /* final */ int[] ButtonBarLayout; // = {2130968744};
    @DexIgnore
    public static /* final */ int ButtonBarLayout_allowStacking; // = 0;
    @DexIgnore
    public static /* final */ int[] ButtonFeatureView; // = {2130968858, 2130969311, 2130969312, 2130969407, 2130969444, 2130969900};
    @DexIgnore
    public static /* final */ int ButtonFeatureView_belongs_to_tracker; // = 0;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_feature_description; // = 1;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_feature_name; // = 2;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_icon_drawable; // = 3;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_is_locked; // = 4;
    @DexIgnore
    public static /* final */ int ButtonFeatureView_sub_icon_drawable; // = 5;
    @DexIgnore
    public static /* final */ int[] CardStackView; // = {2130968871, 2130969259, 2130969549, 2130969727, 2130969751, 2130969869, 2130969907, 2130969908, 2130969909, 2130970054, 2130970058, 2130970083};
    @DexIgnore
    public static /* final */ int CardStackView_bottomOverlay; // = 0;
    @DexIgnore
    public static /* final */ int CardStackView_elevationEnabled; // = 1;
    @DexIgnore
    public static /* final */ int CardStackView_leftOverlay; // = 2;
    @DexIgnore
    public static /* final */ int CardStackView_rightOverlay; // = 3;
    @DexIgnore
    public static /* final */ int CardStackView_scaleDiff; // = 4;
    @DexIgnore
    public static /* final */ int CardStackView_stackFrom; // = 5;
    @DexIgnore
    public static /* final */ int CardStackView_swipeDirection; // = 6;
    @DexIgnore
    public static /* final */ int CardStackView_swipeEnabled; // = 7;
    @DexIgnore
    public static /* final */ int CardStackView_swipeThreshold; // = 8;
    @DexIgnore
    public static /* final */ int CardStackView_topOverlay; // = 9;
    @DexIgnore
    public static /* final */ int CardStackView_translationDiff; // = 10;
    @DexIgnore
    public static /* final */ int CardStackView_visibleCount; // = 11;
    @DexIgnore
    public static /* final */ int[] CardView; // = {16843071, 16843072, 2130968927, 2130968928, 2130968929, 2130968931, 2130968932, 2130968933, 2130969060, 2130969061, 2130969062, 2130969063, 2130969064};
    @DexIgnore
    public static /* final */ int CardView_android_minHeight; // = 1;
    @DexIgnore
    public static /* final */ int CardView_android_minWidth; // = 0;
    @DexIgnore
    public static /* final */ int CardView_cardBackgroundColor; // = 2;
    @DexIgnore
    public static /* final */ int CardView_cardCornerRadius; // = 3;
    @DexIgnore
    public static /* final */ int CardView_cardElevation; // = 4;
    @DexIgnore
    public static /* final */ int CardView_cardMaxElevation; // = 5;
    @DexIgnore
    public static /* final */ int CardView_cardPreventCornerOverlap; // = 6;
    @DexIgnore
    public static /* final */ int CardView_cardUseCompatPadding; // = 7;
    @DexIgnore
    public static /* final */ int CardView_contentPadding; // = 8;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingBottom; // = 9;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingLeft; // = 10;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingRight; // = 11;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingTop; // = 12;
    @DexIgnore
    public static /* final */ int[] CenterDrawableGravityRadioButton; // = {16843015};
    @DexIgnore
    public static /* final */ int CenterDrawableGravityRadioButton_android_button; // = 0;
    @DexIgnore
    public static /* final */ int[] Chip; // = {16842804, 16842904, 16842923, 16843039, 16843087, 16843237, 2130968946, 2130968947, 2130968949, 2130968951, 2130968952, 2130968953, 2130968955, 2130968956, 2130968957, 2130968958, 2130968959, 2130968960, 2130968961, 2130968966, 2130968967, 2130968968, 2130968970, 2130968987, 2130968988, 2130968989, 2130968990, 2130968991, 2130968992, 2130968993, 2130969276, 2130969386, 2130969399, 2130969403, R.attr.rippleColor, 2130969786, 2130969789, 2130969794, 2130969988, 2130970000};
    @DexIgnore
    public static /* final */ int[] ChipGroup; // = {2130968945, 2130968962, 2130968963, 2130968964, 2130969799, 2130969800};
    @DexIgnore
    public static /* final */ int ChipGroup_checkedChip; // = 0;
    @DexIgnore
    public static /* final */ int ChipGroup_chipSpacing; // = 1;
    @DexIgnore
    public static /* final */ int ChipGroup_chipSpacingHorizontal; // = 2;
    @DexIgnore
    public static /* final */ int ChipGroup_chipSpacingVertical; // = 3;
    @DexIgnore
    public static /* final */ int ChipGroup_singleLine; // = 4;
    @DexIgnore
    public static /* final */ int ChipGroup_singleSelection; // = 5;
    @DexIgnore
    public static /* final */ int Chip_android_checkable; // = 5;
    @DexIgnore
    public static /* final */ int Chip_android_ellipsize; // = 2;
    @DexIgnore
    public static /* final */ int Chip_android_maxWidth; // = 3;
    @DexIgnore
    public static /* final */ int Chip_android_text; // = 4;
    @DexIgnore
    public static /* final */ int Chip_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int Chip_android_textColor; // = 1;
    @DexIgnore
    public static /* final */ int Chip_checkedIcon; // = 6;
    @DexIgnore
    public static /* final */ int Chip_checkedIconEnabled; // = 7;
    @DexIgnore
    public static /* final */ int Chip_checkedIconVisible; // = 8;
    @DexIgnore
    public static /* final */ int Chip_chipBackgroundColor; // = 9;
    @DexIgnore
    public static /* final */ int Chip_chipCornerRadius; // = 10;
    @DexIgnore
    public static /* final */ int Chip_chipEndPadding; // = 11;
    @DexIgnore
    public static /* final */ int Chip_chipIcon; // = 12;
    @DexIgnore
    public static /* final */ int Chip_chipIconEnabled; // = 13;
    @DexIgnore
    public static /* final */ int Chip_chipIconSize; // = 14;
    @DexIgnore
    public static /* final */ int Chip_chipIconTint; // = 15;
    @DexIgnore
    public static /* final */ int Chip_chipIconVisible; // = 16;
    @DexIgnore
    public static /* final */ int Chip_chipMinHeight; // = 17;
    @DexIgnore
    public static /* final */ int Chip_chipMinTouchTargetSize; // = 18;
    @DexIgnore
    public static /* final */ int Chip_chipStartPadding; // = 19;
    @DexIgnore
    public static /* final */ int Chip_chipStrokeColor; // = 20;
    @DexIgnore
    public static /* final */ int Chip_chipStrokeWidth; // = 21;
    @DexIgnore
    public static /* final */ int Chip_chipSurfaceColor; // = 22;
    @DexIgnore
    public static /* final */ int Chip_closeIcon; // = 23;
    @DexIgnore
    public static /* final */ int Chip_closeIconEnabled; // = 24;
    @DexIgnore
    public static /* final */ int Chip_closeIconEndPadding; // = 25;
    @DexIgnore
    public static /* final */ int Chip_closeIconSize; // = 26;
    @DexIgnore
    public static /* final */ int Chip_closeIconStartPadding; // = 27;
    @DexIgnore
    public static /* final */ int Chip_closeIconTint; // = 28;
    @DexIgnore
    public static /* final */ int Chip_closeIconVisible; // = 29;
    @DexIgnore
    public static /* final */ int Chip_ensureMinTouchTargetSize; // = 30;
    @DexIgnore
    public static /* final */ int Chip_hideMotionSpec; // = 31;
    @DexIgnore
    public static /* final */ int Chip_iconEndPadding; // = 32;
    @DexIgnore
    public static /* final */ int Chip_iconStartPadding; // = 33;
    @DexIgnore
    public static /* final */ int Chip_rippleColor; // = 34;
    @DexIgnore
    public static /* final */ int Chip_shapeAppearance; // = 35;
    @DexIgnore
    public static /* final */ int Chip_shapeAppearanceOverlay; // = 36;
    @DexIgnore
    public static /* final */ int Chip_showMotionSpec; // = 37;
    @DexIgnore
    public static /* final */ int Chip_textEndPadding; // = 38;
    @DexIgnore
    public static /* final */ int Chip_textStartPadding; // = 39;
    @DexIgnore
    public static /* final */ int[] ChooseDeviceTypeButton; // = {2130968971, 2130968972, 2130968973};
    @DexIgnore
    public static /* final */ int ChooseDeviceTypeButton_choose_device_type_btn_icon; // = 0;
    @DexIgnore
    public static /* final */ int ChooseDeviceTypeButton_choose_device_type_btn_prefix; // = 1;
    @DexIgnore
    public static /* final */ int ChooseDeviceTypeButton_choose_device_type_btn_title; // = 2;
    @DexIgnore
    public static /* final */ int[] Circle; // = {2130969579, 2130969580};
    @DexIgnore
    public static /* final */ int[] CircleImageViewProgressBar; // = {2130968939, 2130969232, 2130969642, 2130969692, 2130969693, 2130969694, 2130969695, 2130969696, 2130969697};
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_centercircle_diammterer; // = 0;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_draw_anticlockwise; // = 1;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_normal_border_width; // = 2;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_border_color; // = 3;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_border_overlay; // = 4;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_border_width; // = 5;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_color; // = 6;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_fill_color; // = 7;
    @DexIgnore
    public static /* final */ int CircleImageViewProgressBar_progress_startAngle; // = 8;
    @DexIgnore
    public static /* final */ int[] CirclePageIndicator; // = {16842948, 16842964, 2130969313, 2130969421, 2130969664, 2130969706, 2130969860, 2130969892, 2130969896};
    @DexIgnore
    public static /* final */ int CirclePageIndicator_android_background; // = 1;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_fillColor; // = 2;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_indicator_centered; // = 3;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_pageColor; // = 4;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_radius; // = 5;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_snap; // = 6;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_strokeColor; // = 7;
    @DexIgnore
    public static /* final */ int CirclePageIndicator_strokeWidth; // = 8;
    @DexIgnore
    public static /* final */ int[] CircleTextWidget; // = {2130968976, 2130968977, 2130968978, 2130968979, 2130968980, 2130968981, 2130968982};
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_box_bg_deselected; // = 0;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_box_bg_selected; // = 1;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_content; // = 2;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_content_color_deselected; // = 3;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_content_color_selected; // = 4;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_content_size; // = 5;
    @DexIgnore
    public static /* final */ int CircleTextWidget_circle_state_enabled; // = 6;
    @DexIgnore
    public static /* final */ int Circle_mActiveColor; // = 0;
    @DexIgnore
    public static /* final */ int Circle_mInactiveColor; // = 1;
    @DexIgnore
    public static /* final */ int[] ClearableEditText; // = {2130968985, 2130968986, 2130969249, 2130969726};
    @DexIgnore
    public static /* final */ int ClearableEditText_clearableEditBackground; // = 0;
    @DexIgnore
    public static /* final */ int ClearableEditText_clearablesBackground; // = 1;
    @DexIgnore
    public static /* final */ int ClearableEditText_editTextHint; // = 2;
    @DexIgnore
    public static /* final */ int ClearableEditText_rightIcon; // = 3;
    @DexIgnore
    public static /* final */ int[] ClockView; // = {2130969154, 2130969155, 2130969156, 2130969157, 2130969158, 2130969159, 2130969161, 2130969162, 2130969164};
    @DexIgnore
    public static /* final */ int ClockView_cv_finalNumeralHeight; // = 0;
    @DexIgnore
    public static /* final */ int ClockView_cv_finalPadding; // = 1;
    @DexIgnore
    public static /* final */ int ClockView_cv_fontColor; // = 2;
    @DexIgnore
    public static /* final */ int ClockView_cv_fontName; // = 3;
    @DexIgnore
    public static /* final */ int ClockView_cv_fontSelectedColor; // = 4;
    @DexIgnore
    public static /* final */ int ClockView_cv_fontSize; // = 5;
    @DexIgnore
    public static /* final */ int ClockView_cv_maxHeight; // = 6;
    @DexIgnore
    public static /* final */ int ClockView_cv_minHeight; // = 7;
    @DexIgnore
    public static /* final */ int ClockView_cv_numeralSelectedBackgroundColor; // = 8;
    @DexIgnore
    public static /* final */ int[] CollapsingToolbarLayout; // = {R.attr.collapsedTitleGravity, R.attr.collapsedTitleTextAppearance, R.attr.contentScrim, R.attr.expandedTitleGravity, R.attr.expandedTitleMargin, R.attr.expandedTitleMarginBottom, R.attr.expandedTitleMarginEnd, R.attr.expandedTitleMarginStart, R.attr.expandedTitleMarginTop, R.attr.expandedTitleTextAppearance, R.attr.scrimAnimationDuration, R.attr.scrimVisibleHeightTrigger, R.attr.statusBarScrim, 2130970031, R.attr.titleEnabled, R.attr.toolbarId};
    @DexIgnore
    public static /* final */ int[] CollapsingToolbarLayout_Layout; // = {R.attr.layout_collapseMode, R.attr.layout_collapseParallaxMultiplier};
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_Layout_layout_collapseMode; // = 0;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier; // = 1;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_collapsedTitleGravity; // = 0;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_collapsedTitleTextAppearance; // = 1;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_contentScrim; // = 2;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleGravity; // = 3;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMargin; // = 4;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMarginBottom; // = 5;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMarginEnd; // = 6;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMarginStart; // = 7;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleMarginTop; // = 8;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_expandedTitleTextAppearance; // = 9;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_scrimAnimationDuration; // = 10;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_scrimVisibleHeightTrigger; // = 11;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_statusBarScrim; // = 12;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_title; // = 13;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_titleEnabled; // = 14;
    @DexIgnore
    public static /* final */ int CollapsingToolbarLayout_toolbarId; // = 15;
    @DexIgnore
    public static /* final */ int[] ColorPanelView; // = {2130969105, 2130969107, 2130969114};
    @DexIgnore
    public static /* final */ int ColorPanelView_cpv_borderColor; // = 0;
    @DexIgnore
    public static /* final */ int ColorPanelView_cpv_colorShape; // = 1;
    @DexIgnore
    public static /* final */ int ColorPanelView_cpv_showOldColor; // = 2;
    @DexIgnore
    public static /* final */ int[] ColorPickerView; // = {2130969103, 2130969104, 2130969105, 2130969115};
    @DexIgnore
    public static /* final */ int ColorPickerView_cpv_alphaChannelText; // = 0;
    @DexIgnore
    public static /* final */ int ColorPickerView_cpv_alphaChannelVisible; // = 1;
    @DexIgnore
    public static /* final */ int ColorPickerView_cpv_borderColor; // = 2;
    @DexIgnore
    public static /* final */ int ColorPickerView_cpv_sliderColor; // = 3;
    @DexIgnore
    public static /* final */ int[] ColorPreference; // = {2130969101, 2130969102, 2130969106, 2130969107, 2130969108, 2130969109, 2130969110, 2130969111, 2130969112, 2130969113};
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_allowCustom; // = 0;
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_allowPresets; // = 1;
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_colorPresets; // = 2;
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_colorShape; // = 3;
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_dialogTitle; // = 4;
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_dialogType; // = 5;
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_previewSize; // = 6;
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_showAlphaSlider; // = 7;
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_showColorShades; // = 8;
    @DexIgnore
    public static /* final */ int ColorPreference_cpv_showDialog; // = 9;
    @DexIgnore
    public static /* final */ int[] ColorStateListItem; // = {16843173, 16843551, 2130968745};
    @DexIgnore
    public static /* final */ int ColorStateListItem_alpha; // = 2;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_alpha; // = 1;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int[] CompoundButton; // = {16843015, 2130968892, 2130968905, 2130968906};
    @DexIgnore
    public static /* final */ int CompoundButton_android_button; // = 0;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonCompat; // = 1;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonTint; // = 2;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonTintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] ConstraintLayout_Layout; // = {16842948, 16843039, 16843040, 16843071, 16843072, 2130968803, 2130968804, 2130968940, 2130969046, 2130969047, 2130969485, 2130969486, 2130969487, 2130969488, 2130969489, 2130969490, 2130969491, 2130969492, 2130969493, 2130969494, 2130969495, 2130969496, 2130969497, 2130969498, 2130969499, 2130969500, 2130969501, 2130969502, 2130969503, 2130969504, 2130969505, 2130969506, 2130969507, 2130969508, 2130969509, 2130969510, 2130969511, 2130969512, 2130969513, 2130969514, 2130969515, 2130969516, 2130969517, 2130969518, 2130969519, 2130969520, 2130969521, 2130969522, 2130969523, 2130969524, 2130969525, 2130969527, 2130969528, 2130969529, 2130969530, 2130969531, 2130969532, 2130969533, 2130969534, 2130969545};
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_maxHeight; // = 2;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_maxWidth; // = 1;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_minHeight; // = 4;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_minWidth; // = 3;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_barrierAllowsGoneWidgets; // = 5;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_barrierDirection; // = 6;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_chainUseRtl; // = 7;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_constraintSet; // = 8;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_constraint_referenced_ids; // = 9;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constrainedHeight; // = 10;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constrainedWidth; // = 11;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBaseline_creator; // = 12;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf; // = 13;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_creator; // = 14;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_toBottomOf; // = 15;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_toTopOf; // = 16;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircle; // = 17;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircleAngle; // = 18;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircleRadius; // = 19;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintDimensionRatio; // = 20;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintEnd_toEndOf; // = 21;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintEnd_toStartOf; // = 22;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_begin; // = 23;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_end; // = 24;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_percent; // = 25;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_default; // = 26;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_max; // = 27;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_min; // = 28;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_percent; // = 29;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_bias; // = 30;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle; // = 31;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_weight; // = 32;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_creator; // = 33;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_toLeftOf; // = 34;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_toRightOf; // = 35;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_creator; // = 36;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_toLeftOf; // = 37;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_toRightOf; // = 38;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintStart_toEndOf; // = 39;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintStart_toStartOf; // = 40;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_creator; // = 41;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_toBottomOf; // = 42;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_toTopOf; // = 43;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_bias; // = 44;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_chainStyle; // = 45;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_weight; // = 46;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_default; // = 47;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_max; // = 48;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_min; // = 49;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_percent; // = 50;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_editor_absoluteX; // = 51;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_editor_absoluteY; // = 52;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginBottom; // = 53;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginEnd; // = 54;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginLeft; // = 55;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginRight; // = 56;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginStart; // = 57;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginTop; // = 58;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_optimizationLevel; // = 59;
    @DexIgnore
    public static /* final */ int[] ConstraintLayout_placeholder; // = {2130969052, 2130969262};
    @DexIgnore
    public static /* final */ int ConstraintLayout_placeholder_content; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintLayout_placeholder_emptyVisibility; // = 1;
    @DexIgnore
    public static /* final */ int[] ConstraintSet; // = {16842948, 16842960, 16842972, 16842996, 16842997, 16842999, 16843000, 16843001, 16843002, 16843039, 16843040, 16843071, 16843072, 16843551, 16843552, 16843553, 16843554, 16843555, 16843556, 16843557, 16843558, 16843559, 16843560, 16843701, 16843702, 16843770, 16843840, 2130968803, 2130968804, 2130968940, 2130969047, 2130969485, 2130969486, 2130969487, 2130969488, 2130969489, 2130969490, 2130969491, 2130969492, 2130969493, 2130969494, 2130969495, 2130969496, 2130969497, 2130969498, 2130969499, 2130969500, 2130969501, 2130969502, 2130969503, 2130969504, 2130969505, 2130969506, 2130969507, 2130969508, 2130969509, 2130969510, 2130969511, 2130969512, 2130969513, 2130969514, 2130969515, 2130969516, 2130969517, 2130969518, 2130969519, 2130969520, 2130969521, 2130969522, 2130969523, 2130969524, 2130969525, 2130969527, 2130969528, 2130969529, 2130969530, 2130969531, 2130969532, 2130969533, 2130969534};
    @DexIgnore
    public static /* final */ int ConstraintSet_android_alpha; // = 13;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_elevation; // = 26;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_id; // = 1;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_height; // = 4;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginBottom; // = 8;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginEnd; // = 24;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginLeft; // = 5;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginRight; // = 7;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginStart; // = 23;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginTop; // = 6;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_width; // = 3;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_maxHeight; // = 10;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_maxWidth; // = 9;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_minHeight; // = 12;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_minWidth; // = 11;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotation; // = 20;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotationX; // = 21;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotationY; // = 22;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_scaleX; // = 18;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_scaleY; // = 19;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_transformPivotX; // = 14;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_transformPivotY; // = 15;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationX; // = 16;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationY; // = 17;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationZ; // = 25;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_visibility; // = 2;
    @DexIgnore
    public static /* final */ int ConstraintSet_barrierAllowsGoneWidgets; // = 27;
    @DexIgnore
    public static /* final */ int ConstraintSet_barrierDirection; // = 28;
    @DexIgnore
    public static /* final */ int ConstraintSet_chainUseRtl; // = 29;
    @DexIgnore
    public static /* final */ int ConstraintSet_constraint_referenced_ids; // = 30;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constrainedHeight; // = 31;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constrainedWidth; // = 32;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBaseline_creator; // = 33;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBaseline_toBaselineOf; // = 34;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_creator; // = 35;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_toBottomOf; // = 36;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_toTopOf; // = 37;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircle; // = 38;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircleAngle; // = 39;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircleRadius; // = 40;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintDimensionRatio; // = 41;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintEnd_toEndOf; // = 42;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintEnd_toStartOf; // = 43;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_begin; // = 44;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_end; // = 45;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_percent; // = 46;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_default; // = 47;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_max; // = 48;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_min; // = 49;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_percent; // = 50;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_bias; // = 51;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_chainStyle; // = 52;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_weight; // = 53;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_creator; // = 54;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_toLeftOf; // = 55;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_toRightOf; // = 56;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_creator; // = 57;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_toLeftOf; // = 58;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_toRightOf; // = 59;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintStart_toEndOf; // = 60;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintStart_toStartOf; // = 61;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_creator; // = 62;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_toBottomOf; // = 63;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_toTopOf; // = 64;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_bias; // = 65;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_chainStyle; // = 66;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_weight; // = 67;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_default; // = 68;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_max; // = 69;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_min; // = 70;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_percent; // = 71;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_editor_absoluteX; // = 72;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_editor_absoluteY; // = 73;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginBottom; // = 74;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginEnd; // = 75;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginLeft; // = 76;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginRight; // = 77;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginStart; // = 78;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginTop; // = 79;
    @DexIgnore
    public static /* final */ int[] ContactDetailWidget; // = {2130969050, 2130969051};
    @DexIgnore
    public static /* final */ int ContactDetailWidget_contact_detail_icon; // = 0;
    @DexIgnore
    public static /* final */ int ContactDetailWidget_contact_detail_title; // = 1;
    @DexIgnore
    public static /* final */ int[] CoordinatorLayout; // = {2130969470, 2130969888};
    @DexIgnore
    public static /* final */ int[] CoordinatorLayout_Layout; // = {16842931, 2130969479, 2130969480, 2130969482, 2130969526, 2130969536, 2130969537};
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_anchor; // = 1;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_anchorGravity; // = 2;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_behavior; // = 3;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_dodgeInsetEdges; // = 4;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_insetEdge; // = 5;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_keyline; // = 6;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_keylines; // = 0;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_statusBarBackground; // = 1;
    @DexIgnore
    public static /* final */ int[] CropImageView; // = {2130969116, 2130969117, 2130969118, 2130969119, 2130969120, 2130969121, 2130969122, 2130969123, 2130969124, 2130969125, 2130969126, 2130969127, 2130969128, 2130969129, 2130969130, 2130969131, 2130969132, 2130969133, 2130969134, 2130969135, 2130969136, 2130969137, 2130969138, 2130969139, 2130969140, 2130969141, 2130969142, 2130969143, 2130969144, 2130969145, 2130969146, 2130969147};
    @DexIgnore
    public static /* final */ int CropImageView_cropAspectRatioX; // = 0;
    @DexIgnore
    public static /* final */ int CropImageView_cropAspectRatioY; // = 1;
    @DexIgnore
    public static /* final */ int CropImageView_cropAutoZoomEnabled; // = 2;
    @DexIgnore
    public static /* final */ int CropImageView_cropBackgroundColor; // = 3;
    @DexIgnore
    public static /* final */ int CropImageView_cropBorderCornerColor; // = 4;
    @DexIgnore
    public static /* final */ int CropImageView_cropBorderCornerLength; // = 5;
    @DexIgnore
    public static /* final */ int CropImageView_cropBorderCornerOffset; // = 6;
    @DexIgnore
    public static /* final */ int CropImageView_cropBorderCornerThickness; // = 7;
    @DexIgnore
    public static /* final */ int CropImageView_cropBorderLineThickness; // = 8;
    @DexIgnore
    public static /* final */ int CropImageView_cropBordernonBrandLineColor; // = 9;
    @DexIgnore
    public static /* final */ int CropImageView_cropFixAspectRatio; // = 10;
    @DexIgnore
    public static /* final */ int CropImageView_cropFlipHorizontally; // = 11;
    @DexIgnore
    public static /* final */ int CropImageView_cropFlipVertically; // = 12;
    @DexIgnore
    public static /* final */ int CropImageView_cropGuidelines; // = 13;
    @DexIgnore
    public static /* final */ int CropImageView_cropGuidelinesColor; // = 14;
    @DexIgnore
    public static /* final */ int CropImageView_cropGuidelinesThickness; // = 15;
    @DexIgnore
    public static /* final */ int CropImageView_cropInitialCropWindowPaddingRatio; // = 16;
    @DexIgnore
    public static /* final */ int CropImageView_cropMaxCropResultHeightPX; // = 17;
    @DexIgnore
    public static /* final */ int CropImageView_cropMaxCropResultWidthPX; // = 18;
    @DexIgnore
    public static /* final */ int CropImageView_cropMaxZoom; // = 19;
    @DexIgnore
    public static /* final */ int CropImageView_cropMinCropResultHeightPX; // = 20;
    @DexIgnore
    public static /* final */ int CropImageView_cropMinCropResultWidthPX; // = 21;
    @DexIgnore
    public static /* final */ int CropImageView_cropMinCropWindowHeight; // = 22;
    @DexIgnore
    public static /* final */ int CropImageView_cropMinCropWindowWidth; // = 23;
    @DexIgnore
    public static /* final */ int CropImageView_cropMultiTouchEnabled; // = 24;
    @DexIgnore
    public static /* final */ int CropImageView_cropSaveBitmapToInstanceState; // = 25;
    @DexIgnore
    public static /* final */ int CropImageView_cropScaleType; // = 26;
    @DexIgnore
    public static /* final */ int CropImageView_cropShape; // = 27;
    @DexIgnore
    public static /* final */ int CropImageView_cropShowCropOverlay; // = 28;
    @DexIgnore
    public static /* final */ int CropImageView_cropShowProgressBar; // = 29;
    @DexIgnore
    public static /* final */ int CropImageView_cropSnapRadius; // = 30;
    @DexIgnore
    public static /* final */ int CropImageView_cropTouchRadius; // = 31;
    @DexIgnore
    public static /* final */ int[] CuriosityDrawerLayout; // = {2130968869};
    @DexIgnore
    public static /* final */ int CuriosityDrawerLayout_bottomBarHeight; // = 0;
    @DexIgnore
    public static /* final */ int[] CustomClockView; // = {2130968769, 2130968778, 2130969196, 2130969197, 2130969328, 2130969360, 2130969361, 2130969442, 2130969586, 2130969587, 2130969588, 2130969589, 2130969661, 2130969898, 2130969998, 2130970010, 2130970011};
    @DexIgnore
    public static /* final */ int CustomClockView_backgroundColor; // = 0;
    @DexIgnore
    public static /* final */ int CustomClockView_backgroundStrokeWidth; // = 1;
    @DexIgnore
    public static /* final */ int CustomClockView_defaultMarkerColor; // = 2;
    @DexIgnore
    public static /* final */ int CustomClockView_defaultMarkerColorInactive; // = 3;
    @DexIgnore
    public static /* final */ int CustomClockView_fontName; // = 4;
    @DexIgnore
    public static /* final */ int CustomClockView_handLength; // = 5;
    @DexIgnore
    public static /* final */ int CustomClockView_handWidth; // = 6;
    @DexIgnore
    public static /* final */ int CustomClockView_isRoundedPaint; // = 7;
    @DexIgnore
    public static /* final */ int CustomClockView_markerColor; // = 8;
    @DexIgnore
    public static /* final */ int CustomClockView_markerColorInactive; // = 9;
    @DexIgnore
    public static /* final */ int CustomClockView_markerLength; // = 10;
    @DexIgnore
    public static /* final */ int CustomClockView_markerWidth; // = 11;
    @DexIgnore
    public static /* final */ int CustomClockView_paddingInside; // = 12;
    @DexIgnore
    public static /* final */ int CustomClockView_style; // = 13;
    @DexIgnore
    public static /* final */ int CustomClockView_textSize; // = 14;
    @DexIgnore
    public static /* final */ int CustomClockView_textValueColor; // = 15;
    @DexIgnore
    public static /* final */ int CustomClockView_textValueColorInactive; // = 16;
    @DexIgnore
    public static /* final */ int[] CustomDrawerLayout; // = {2130969625};
    @DexIgnore
    public static /* final */ int CustomDrawerLayout_minDrawerMargin; // = 0;
    @DexIgnore
    public static /* final */ int[] DashBar; // = {2130969167, 2130969168, 2130969169, 2130969170, 2130969171, 2130969172, 2130969173};
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_background_color; // = 0;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_color_end; // = 1;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_color_start; // = 2;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_dash_width; // = 3;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_length; // = 4;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_progress; // = 5;
    @DexIgnore
    public static /* final */ int DashBar_dash_bar_stroke_width; // = 6;
    @DexIgnore
    public static /* final */ int[] DashboardVisualizationRings; // = {2130970084, 2130970085, 2130970086, 2130970087, 2130970088, 2130970089, 2130970090, 2130970091, 2130970092, 2130970093, 2130970094};
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_active_time_color; // = 0;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_active_time_second_color; // = 1;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_background_color; // = 2;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_calories_color; // = 3;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_calories_second_color; // = 4;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_ring_bg_color; // = 5;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_sleep_first_color; // = 6;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_sleep_second_color; // = 7;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_sleep_third_color; // = 8;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_steps_color; // = 9;
    @DexIgnore
    public static /* final */ int DashboardVisualizationRings_visualization_steps_second_color; // = 10;
    @DexIgnore
    public static /* final */ int[] DeviceInfoField; // = {2130969219};
    @DexIgnore
    public static /* final */ int DeviceInfoField_device_info_field_label; // = 0;
    @DexIgnore
    public static /* final */ int[] DialView; // = {2130969220, 2130969221};
    @DexIgnore
    public static /* final */ int DialView_dialViewBackgroundImage; // = 0;
    @DexIgnore
    public static /* final */ int DialView_dialViewDialImage; // = 1;
    @DexIgnore
    public static /* final */ int[] DrawerArrowToggle; // = {2130968754, 2130968755, 2130968785, 2130969002, 2130969237, 2130969346, 2130969864, 2130970016};
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_arrowHeadLength; // = 0;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_arrowShaftLength; // = 1;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_barLength; // = 2;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_color; // = 3;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_drawableSize; // = 4;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_gapBetweenBars; // = 5;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_spinBars; // = 6;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_thickness; // = 7;
    @DexIgnore
    public static /* final */ int[] EditListItem; // = {2130969251, 2130969252, 2130969253, 2130969254};
    @DexIgnore
    public static /* final */ int EditListItem_editlistitem_autocorrect; // = 0;
    @DexIgnore
    public static /* final */ int EditListItem_editlistitem_editable; // = 1;
    @DexIgnore
    public static /* final */ int EditListItem_editlistitem_icon; // = 2;
    @DexIgnore
    public static /* final */ int EditListItem_editlistitem_title; // = 3;
    @DexIgnore
    public static /* final */ int[] EmptyMessageWidget; // = {2130969263, 2130969264, 2130969265, 2130969266, 2130969267};
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_image; // = 0;
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_sub_title; // = 1;
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_subtitle_text_style; // = 2;
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_text_font_color; // = 3;
    @DexIgnore
    public static /* final */ int EmptyMessageWidget_empty_title; // = 4;
    @DexIgnore
    public static /* final */ int[] ErrorStep; // = {2130969285, 2130969286};
    @DexIgnore
    public static /* final */ int ErrorStep_error_step_message; // = 0;
    @DexIgnore
    public static /* final */ int ErrorStep_error_step_title; // = 1;
    @DexIgnore
    public static /* final */ int[] ExtendedFloatingActionButton; // = {2130969258, 2130969297, 2130969386, 2130969794, 2130969797};
    @DexIgnore
    public static /* final */ int[] ExtendedFloatingActionButton_Behavior_Layout; // = {R.attr.behavior_autoHide, 2130968849};
    @DexIgnore
    public static /* final */ int ExtendedFloatingActionButton_Behavior_Layout_behavior_autoHide; // = 0;
    @DexIgnore
    public static /* final */ int ExtendedFloatingActionButton_Behavior_Layout_behavior_autoShrink; // = 1;
    @DexIgnore
    public static /* final */ int ExtendedFloatingActionButton_elevation; // = 0;
    @DexIgnore
    public static /* final */ int ExtendedFloatingActionButton_extendMotionSpec; // = 1;
    @DexIgnore
    public static /* final */ int ExtendedFloatingActionButton_hideMotionSpec; // = 2;
    @DexIgnore
    public static /* final */ int ExtendedFloatingActionButton_showMotionSpec; // = 3;
    @DexIgnore
    public static /* final */ int ExtendedFloatingActionButton_shrinkMotionSpec; // = 4;
    @DexIgnore
    public static /* final */ int[] FlexibleAutoCompleteTextView; // = {2130968756, 2130969394, 2130969986, 2130969989, 2130969992, 2130970009, 2130970030};
    @DexIgnore
    public static /* final */ int FlexibleAutoCompleteTextView_autoCompleteBackgroundColor; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleAutoCompleteTextView_hintType; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleAutoCompleteTextView_textColorStyle; // = 2;
    @DexIgnore
    public static /* final */ int FlexibleAutoCompleteTextView_textFontStyle; // = 3;
    @DexIgnore
    public static /* final */ int FlexibleAutoCompleteTextView_textHintColor; // = 4;
    @DexIgnore
    public static /* final */ int FlexibleAutoCompleteTextView_textType; // = 5;
    @DexIgnore
    public static /* final */ int FlexibleAutoCompleteTextView_tintType; // = 6;
    @DexIgnore
    public static /* final */ int[] FlexibleButton; // = {2130968770, 2130968862, 2130969986, 2130969989, 2130970009};
    @DexIgnore
    public static /* final */ int FlexibleButton_backgroundColorStyle; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleButton_borderColorStyle; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleButton_textColorStyle; // = 2;
    @DexIgnore
    public static /* final */ int FlexibleButton_textFontStyle; // = 3;
    @DexIgnore
    public static /* final */ int FlexibleButton_textType; // = 4;
    @DexIgnore
    public static /* final */ int[] FlexibleCheckBox; // = {2130968881, 2130968882};
    @DexIgnore
    public static /* final */ int FlexibleCheckBox_boxDisableColor; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleCheckBox_boxEnableColor; // = 1;
    @DexIgnore
    public static /* final */ int[] FlexibleEditText; // = {2130968862, 2130969150, 2130969390, 2130969394, 2130969757, 2130969986, 2130969989, 2130970009, 2130970030};
    @DexIgnore
    public static /* final */ int FlexibleEditText_borderColorStyle; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleEditText_cursorEnable; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleEditText_hintColor; // = 2;
    @DexIgnore
    public static /* final */ int FlexibleEditText_hintType; // = 3;
    @DexIgnore
    public static /* final */ int FlexibleEditText_searchBackgroundColor; // = 4;
    @DexIgnore
    public static /* final */ int FlexibleEditText_textColorStyle; // = 5;
    @DexIgnore
    public static /* final */ int FlexibleEditText_textFontStyle; // = 6;
    @DexIgnore
    public static /* final */ int FlexibleEditText_textType; // = 7;
    @DexIgnore
    public static /* final */ int FlexibleEditText_tintType; // = 8;
    @DexIgnore
    public static /* final */ int[] FlexibleFitnessTab; // = {2130969318, 2130969319, 2130969320};
    @DexIgnore
    public static /* final */ int FlexibleFitnessTab_flexible_fitness_tab_background; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleFitnessTab_flexible_fitness_tab_icon_res; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleFitnessTab_flexible_fitness_tab_unit_value; // = 2;
    @DexIgnore
    public static /* final */ int[] FlexibleImageButton; // = {2130969321, 2130969322};
    @DexIgnore
    public static /* final */ int FlexibleImageButton_flexible_image_button_background_color; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleImageButton_flexible_image_button_image_color; // = 1;
    @DexIgnore
    public static /* final */ int[] FlexibleProgressBar; // = {2130969323, 2130969324};
    @DexIgnore
    public static /* final */ int FlexibleProgressBar_flexible_progress_bar_background_color; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleProgressBar_flexible_progress_bar_progress_color; // = 1;
    @DexIgnore
    public static /* final */ int[] FlexibleSwitchCompat; // = {2130969914, 2130969915, 2130969916};
    @DexIgnore
    public static /* final */ int FlexibleSwitchCompat_switch_thumb_color; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleSwitchCompat_switch_track_disableColor; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleSwitchCompat_switch_track_enabledColor; // = 2;
    @DexIgnore
    public static /* final */ int[] FlexibleTabLayout; // = {2130969943, 2130969944, 2130969945, 2130969946, 2130969947};
    @DexIgnore
    public static /* final */ int FlexibleTabLayout_tablayout_disableBackgroundColor; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleTabLayout_tablayout_disableTextColor; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleTabLayout_tablayout_enableBackgroundColor; // = 2;
    @DexIgnore
    public static /* final */ int FlexibleTabLayout_tablayout_enableTextColor; // = 3;
    @DexIgnore
    public static /* final */ int FlexibleTabLayout_tablayout_textFontStyle; // = 4;
    @DexIgnore
    public static /* final */ int[] FlexibleTextInputEditText; // = {2130969986, 2130969989};
    @DexIgnore
    public static /* final */ int FlexibleTextInputEditText_textColorStyle; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleTextInputEditText_textFontStyle; // = 1;
    @DexIgnore
    public static /* final */ int[] FlexibleTextInputLayout; // = {2130968884, 2130969284, 2130969668, 2130969986, 2130969989};
    @DexIgnore
    public static /* final */ int FlexibleTextInputLayout_boxStrokeColorStyle; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleTextInputLayout_errorTextColorStyle; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleTextInputLayout_passwordToggleColor; // = 2;
    @DexIgnore
    public static /* final */ int FlexibleTextInputLayout_textColorStyle; // = 3;
    @DexIgnore
    public static /* final */ int FlexibleTextInputLayout_textFontStyle; // = 4;
    @DexIgnore
    public static /* final */ int[] FlexibleTextView; // = {2130968770, 2130968781, 2130969394, 2130969986, 2130969989, 2130970009, 2130970030};
    @DexIgnore
    public static /* final */ int FlexibleTextView_backgroundColorStyle; // = 0;
    @DexIgnore
    public static /* final */ int FlexibleTextView_backgroundType; // = 1;
    @DexIgnore
    public static /* final */ int FlexibleTextView_hintType; // = 2;
    @DexIgnore
    public static /* final */ int FlexibleTextView_textColorStyle; // = 3;
    @DexIgnore
    public static /* final */ int FlexibleTextView_textFontStyle; // = 4;
    @DexIgnore
    public static /* final */ int FlexibleTextView_textType; // = 5;
    @DexIgnore
    public static /* final */ int FlexibleTextView_tintType; // = 6;
    @DexIgnore
    public static /* final */ int[] FloatingActionButton; // = {2130968779, 2130968780, R.attr.borderWidth, 2130969258, 2130969276, 2130969304, R.attr.fabSize, 2130969386, 2130969397, 2130969613, R.attr.pressedTranslationZ, R.attr.rippleColor, 2130969786, 2130969789, 2130969794, R.attr.useCompatPadding};
    @DexIgnore
    public static /* final */ int[] FloatingActionButton_Behavior_Layout; // = {2130968848};
    @DexIgnore
    public static /* final */ int FloatingActionButton_Behavior_Layout_behavior_autoHide; // = 0;
    @DexIgnore
    public static /* final */ int FloatingActionButton_backgroundTint; // = 0;
    @DexIgnore
    public static /* final */ int FloatingActionButton_backgroundTintMode; // = 1;
    @DexIgnore
    public static /* final */ int FloatingActionButton_borderWidth; // = 2;
    @DexIgnore
    public static /* final */ int FloatingActionButton_elevation; // = 3;
    @DexIgnore
    public static /* final */ int FloatingActionButton_ensureMinTouchTargetSize; // = 4;
    @DexIgnore
    public static /* final */ int FloatingActionButton_fabCustomSize; // = 5;
    @DexIgnore
    public static /* final */ int FloatingActionButton_fabSize; // = 6;
    @DexIgnore
    public static /* final */ int FloatingActionButton_hideMotionSpec; // = 7;
    @DexIgnore
    public static /* final */ int FloatingActionButton_hoveredFocusedTranslationZ; // = 8;
    @DexIgnore
    public static /* final */ int FloatingActionButton_maxImageSize; // = 9;
    @DexIgnore
    public static /* final */ int FloatingActionButton_pressedTranslationZ; // = 10;
    @DexIgnore
    public static /* final */ int FloatingActionButton_rippleColor; // = 11;
    @DexIgnore
    public static /* final */ int FloatingActionButton_shapeAppearance; // = 12;
    @DexIgnore
    public static /* final */ int FloatingActionButton_shapeAppearanceOverlay; // = 13;
    @DexIgnore
    public static /* final */ int FloatingActionButton_showMotionSpec; // = 14;
    @DexIgnore
    public static /* final */ int FloatingActionButton_useCompatPadding; // = 15;
    @DexIgnore
    public static /* final */ int[] FlowLayout; // = {2130969463, 2130969555};
    @DexIgnore
    public static /* final */ int FlowLayout_itemSpacing; // = 0;
    @DexIgnore
    public static /* final */ int FlowLayout_lineSpacing; // = 1;
    @DexIgnore
    public static /* final */ int[] FontFamily; // = {com.misfit.frameworks.buttonservice.R.attr.fontProviderAuthority, com.misfit.frameworks.buttonservice.R.attr.fontProviderCerts, com.misfit.frameworks.buttonservice.R.attr.fontProviderFetchStrategy, com.misfit.frameworks.buttonservice.R.attr.fontProviderFetchTimeout, com.misfit.frameworks.buttonservice.R.attr.fontProviderPackage, com.misfit.frameworks.buttonservice.R.attr.fontProviderQuery};
    @DexIgnore
    public static /* final */ int[] FontFamilyFont; // = {16844082, 16844083, 16844095, 16844143, 16844144, com.misfit.frameworks.buttonservice.R.attr.font, com.misfit.frameworks.buttonservice.R.attr.fontStyle, com.misfit.frameworks.buttonservice.R.attr.fontVariationSettings, com.misfit.frameworks.buttonservice.R.attr.fontWeight, com.misfit.frameworks.buttonservice.R.attr.ttcIndex};
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_font; // = 0;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontStyle; // = 2;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontVariationSettings; // = 4;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontWeight; // = 1;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_ttcIndex; // = 3;
    @DexIgnore
    public static /* final */ int FontFamilyFont_font; // = 5;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontStyle; // = 6;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontVariationSettings; // = 7;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontWeight; // = 8;
    @DexIgnore
    public static /* final */ int FontFamilyFont_ttcIndex; // = 9;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderAuthority; // = 0;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderCerts; // = 1;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchStrategy; // = 2;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchTimeout; // = 3;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderPackage; // = 4;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderQuery; // = 5;
    @DexIgnore
    public static /* final */ int[] ForegroundLinearLayout; // = {16843017, 16843264, R.attr.foregroundInsidePadding};
    @DexIgnore
    public static /* final */ int ForegroundLinearLayout_android_foreground; // = 0;
    @DexIgnore
    public static /* final */ int ForegroundLinearLayout_android_foregroundGravity; // = 1;
    @DexIgnore
    public static /* final */ int ForegroundLinearLayout_foregroundInsidePadding; // = 2;
    @DexIgnore
    public static /* final */ int[] FossilButton; // = {2130968907, 2130968908, 2130968909, 2130968910};
    @DexIgnore
    public static /* final */ int FossilButton_button_background; // = 0;
    @DexIgnore
    public static /* final */ int FossilButton_button_icon; // = 1;
    @DexIgnore
    public static /* final */ int FossilButton_button_title; // = 2;
    @DexIgnore
    public static /* final */ int FossilButton_button_title_color; // = 3;
    @DexIgnore
    public static /* final */ int[] FossilCircleImageView; // = {2130968864, 2130968865, 2130968866, 2130969314, 2130969362};
    @DexIgnore
    public static /* final */ int FossilCircleImageView_border_color; // = 0;
    @DexIgnore
    public static /* final */ int FossilCircleImageView_border_overlay; // = 1;
    @DexIgnore
    public static /* final */ int FossilCircleImageView_border_width; // = 2;
    @DexIgnore
    public static /* final */ int FossilCircleImageView_fill_color; // = 3;
    @DexIgnore
    public static /* final */ int FossilCircleImageView_hand_number; // = 4;
    @DexIgnore
    public static /* final */ int[] FossilTopHeaderLayout; // = {2130970048};
    @DexIgnore
    public static /* final */ int[] FossilTopHeaderLayout_Layout; // = {2130968998, 2130968999};
    @DexIgnore
    public static /* final */ int FossilTopHeaderLayout_Layout_collapseMode; // = 0;
    @DexIgnore
    public static /* final */ int FossilTopHeaderLayout_Layout_collapseParallaxMultiplier; // = 1;
    @DexIgnore
    public static /* final */ int FossilTopHeaderLayout_toolbarId; // = 0;
    @DexIgnore
    public static /* final */ int[] Fragment; // = {16842755, 16842960, 16842961};
    @DexIgnore
    public static /* final */ int[] FragmentContainerView; // = {16842755, 16842961};
    @DexIgnore
    public static /* final */ int FragmentContainerView_android_name; // = 0;
    @DexIgnore
    public static /* final */ int FragmentContainerView_android_tag; // = 1;
    @DexIgnore
    public static /* final */ int Fragment_android_id; // = 1;
    @DexIgnore
    public static /* final */ int Fragment_android_name; // = 0;
    @DexIgnore
    public static /* final */ int Fragment_android_tag; // = 2;
    @DexIgnore
    public static /* final */ int[] GradientColor; // = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    @DexIgnore
    public static /* final */ int[] GradientColorItem; // = {16843173, 16844052};
    @DexIgnore
    public static /* final */ int GradientColorItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int GradientColorItem_android_offset; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerColor; // = 7;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerX; // = 3;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerY; // = 4;
    @DexIgnore
    public static /* final */ int GradientColor_android_endColor; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_endX; // = 10;
    @DexIgnore
    public static /* final */ int GradientColor_android_endY; // = 11;
    @DexIgnore
    public static /* final */ int GradientColor_android_gradientRadius; // = 5;
    @DexIgnore
    public static /* final */ int GradientColor_android_startColor; // = 0;
    @DexIgnore
    public static /* final */ int GradientColor_android_startX; // = 8;
    @DexIgnore
    public static /* final */ int GradientColor_android_startY; // = 9;
    @DexIgnore
    public static /* final */ int GradientColor_android_tileMode; // = 6;
    @DexIgnore
    public static /* final */ int GradientColor_android_type; // = 2;
    @DexIgnore
    public static /* final */ int[] HeartRateSleepSessionChart; // = {2130968576, 2130968577, 2130968578, 2130968579, 2130968580, 2130968581, 2130968582, 2130968583, 2130968584};
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_AwakeColor; // = 0;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_AxesColor; // = 1;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_DeepColor; // = 2;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_HeartRateFontSize; // = 3;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_HeartRateTextFont; // = 4;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_LightColor; // = 5;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_TextColor; // = 6;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_TimeFontSize; // = 7;
    @DexIgnore
    public static /* final */ int HeartRateSleepSessionChart_HRSS_TimeTextFont; // = 8;
    @DexIgnore
    public static /* final */ int[] ImageButton; // = {2130969341, 2130969342, 2130969343, 2130969344};
    @DexIgnore
    public static /* final */ int ImageButton_fossil_button_background; // = 0;
    @DexIgnore
    public static /* final */ int ImageButton_fossil_button_icon; // = 1;
    @DexIgnore
    public static /* final */ int ImageButton_fossil_button_title; // = 2;
    @DexIgnore
    public static /* final */ int ImageButton_fossil_button_title_color; // = 3;
    @DexIgnore
    public static /* final */ int[] LetterFastScroller; // = {2130968746, 2130968748};
    @DexIgnore
    public static /* final */ int LetterFastScroller_alphabetColor; // = 0;
    @DexIgnore
    public static /* final */ int LetterFastScroller_alphabetFont; // = 1;
    @DexIgnore
    public static /* final */ int[] LinePageIndicator; // = {16842964, 2130969347, 2130969421, 2130969425, 2130969426, 2130969427, 2130969428, 2130969429, 2130969556};
    @DexIgnore
    public static /* final */ int LinePageIndicator_android_background; // = 0;
    @DexIgnore
    public static /* final */ int LinePageIndicator_gapWidth; // = 1;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_centered; // = 2;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_selectedColor; // = 3;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_strokeWidth; // = 4;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_unselectedBorderColor; // = 5;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_unselectedBorderWidth; // = 6;
    @DexIgnore
    public static /* final */ int LinePageIndicator_indicator_unselectedColor; // = 7;
    @DexIgnore
    public static /* final */ int LinePageIndicator_lineWidth; // = 8;
    @DexIgnore
    public static /* final */ int[] LinearConstraintLayout; // = {16842948};
    @DexIgnore
    public static /* final */ int LinearConstraintLayout_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int[] LinearLayoutCompat; // = {16842927, 16842948, 16843046, 16843047, 16843048, 2130969227, 2130969229, 2130969616, 2130969793};
    @DexIgnore
    public static /* final */ int[] LinearLayoutCompat_Layout; // = {16842931, 16842996, 16842997, 16843137};
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_height; // = 2;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_weight; // = 3;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_width; // = 1;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_baselineAligned; // = 2;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_baselineAlignedChildIndex; // = 3;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_gravity; // = 0;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_orientation; // = 1;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_weightSum; // = 4;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_divider; // = 5;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_dividerPadding; // = 6;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_measureWithLargestChild; // = 7;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_showDividers; // = 8;
    @DexIgnore
    public static /* final */ int[] ListPopupWindow; // = {16843436, 16843437};
    @DexIgnore
    public static /* final */ int ListPopupWindow_android_dropDownHorizontalOffset; // = 0;
    @DexIgnore
    public static /* final */ int ListPopupWindow_android_dropDownVerticalOffset; // = 1;
    @DexIgnore
    public static /* final */ int[] LoadingImageView; // = {2130968975, 2130969413, 2130969414};
    @DexIgnore
    public static /* final */ int LoadingImageView_circleCrop; // = 0;
    @DexIgnore
    public static /* final */ int LoadingImageView_imageAspectRatio; // = 1;
    @DexIgnore
    public static /* final */ int LoadingImageView_imageAspectRatioAdjust; // = 2;
    @DexIgnore
    public static /* final */ int[] MapAttrs; // = {2130968750, 2130968919, 2130968920, 2130968921, 2130968922, 2130968923, 2130968924, 2130968925, 2130969473, 2130969474, 2130969475, 2130969476, 2130969572, 2130969585, 2130970061, 2130970062, 2130970063, 2130970064, 2130970065, 2130970066, 2130970067, 2130970068, 2130970078, 2130970175};
    @DexIgnore
    public static /* final */ int MapAttrs_ambientEnabled; // = 0;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraBearing; // = 1;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraMaxZoomPreference; // = 2;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraMinZoomPreference; // = 3;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTargetLat; // = 4;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTargetLng; // = 5;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraTilt; // = 6;
    @DexIgnore
    public static /* final */ int MapAttrs_cameraZoom; // = 7;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsNorthEastLatitude; // = 8;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsNorthEastLongitude; // = 9;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsSouthWestLatitude; // = 10;
    @DexIgnore
    public static /* final */ int MapAttrs_latLngBoundsSouthWestLongitude; // = 11;
    @DexIgnore
    public static /* final */ int MapAttrs_liteMode; // = 12;
    @DexIgnore
    public static /* final */ int MapAttrs_mapType; // = 13;
    @DexIgnore
    public static /* final */ int MapAttrs_uiCompass; // = 14;
    @DexIgnore
    public static /* final */ int MapAttrs_uiMapToolbar; // = 15;
    @DexIgnore
    public static /* final */ int MapAttrs_uiRotateGestures; // = 16;
    @DexIgnore
    public static /* final */ int MapAttrs_uiScrollGestures; // = 17;
    @DexIgnore
    public static /* final */ int MapAttrs_uiScrollGesturesDuringRotateOrZoom; // = 18;
    @DexIgnore
    public static /* final */ int MapAttrs_uiTiltGestures; // = 19;
    @DexIgnore
    public static /* final */ int MapAttrs_uiZoomControls; // = 20;
    @DexIgnore
    public static /* final */ int MapAttrs_uiZoomGestures; // = 21;
    @DexIgnore
    public static /* final */ int MapAttrs_useViewLifecycle; // = 22;
    @DexIgnore
    public static /* final */ int MapAttrs_zOrderOnTop; // = 23;
    @DexIgnore
    public static /* final */ int[] MaterialAlertDialog; // = {2130968771, 2130968772, 2130968773, 2130968774};
    @DexIgnore
    public static /* final */ int[] MaterialAlertDialogTheme; // = {2130969590, 2130969591, 2130969592, 2130969593, 2130969594};
    @DexIgnore
    public static /* final */ int MaterialAlertDialogTheme_materialAlertDialogBodyTextStyle; // = 0;
    @DexIgnore
    public static /* final */ int MaterialAlertDialogTheme_materialAlertDialogTheme; // = 1;
    @DexIgnore
    public static /* final */ int MaterialAlertDialogTheme_materialAlertDialogTitleIconStyle; // = 2;
    @DexIgnore
    public static /* final */ int MaterialAlertDialogTheme_materialAlertDialogTitlePanelStyle; // = 3;
    @DexIgnore
    public static /* final */ int MaterialAlertDialogTheme_materialAlertDialogTitleTextStyle; // = 4;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_backgroundInsetBottom; // = 0;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_backgroundInsetEnd; // = 1;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_backgroundInsetStart; // = 2;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_backgroundInsetTop; // = 3;
    @DexIgnore
    public static /* final */ int[] MaterialButton; // = {16843191, 16843192, 16843193, 16843194, 16843237, 2130968779, 2130968780, 2130969089, 2130969258, 2130969398, 2130969400, 2130969401, 2130969402, 2130969404, 2130969405, R.attr.rippleColor, 2130969786, 2130969789, 2130969892, 2130969896};
    @DexIgnore
    public static /* final */ int[] MaterialButtonToggleGroup; // = {2130968944, 2130969800};
    @DexIgnore
    public static /* final */ int MaterialButtonToggleGroup_checkedButton; // = 0;
    @DexIgnore
    public static /* final */ int MaterialButtonToggleGroup_singleSelection; // = 1;
    @DexIgnore
    public static /* final */ int MaterialButton_android_checkable; // = 4;
    @DexIgnore
    public static /* final */ int MaterialButton_android_insetBottom; // = 3;
    @DexIgnore
    public static /* final */ int MaterialButton_android_insetLeft; // = 0;
    @DexIgnore
    public static /* final */ int MaterialButton_android_insetRight; // = 1;
    @DexIgnore
    public static /* final */ int MaterialButton_android_insetTop; // = 2;
    @DexIgnore
    public static /* final */ int MaterialButton_backgroundTint; // = 5;
    @DexIgnore
    public static /* final */ int MaterialButton_backgroundTintMode; // = 6;
    @DexIgnore
    public static /* final */ int MaterialButton_cornerRadius; // = 7;
    @DexIgnore
    public static /* final */ int MaterialButton_elevation; // = 8;
    @DexIgnore
    public static /* final */ int MaterialButton_icon; // = 9;
    @DexIgnore
    public static /* final */ int MaterialButton_iconGravity; // = 10;
    @DexIgnore
    public static /* final */ int MaterialButton_iconPadding; // = 11;
    @DexIgnore
    public static /* final */ int MaterialButton_iconSize; // = 12;
    @DexIgnore
    public static /* final */ int MaterialButton_iconTint; // = 13;
    @DexIgnore
    public static /* final */ int MaterialButton_iconTintMode; // = 14;
    @DexIgnore
    public static /* final */ int MaterialButton_rippleColor; // = 15;
    @DexIgnore
    public static /* final */ int MaterialButton_shapeAppearance; // = 16;
    @DexIgnore
    public static /* final */ int MaterialButton_shapeAppearanceOverlay; // = 17;
    @DexIgnore
    public static /* final */ int MaterialButton_strokeColor; // = 18;
    @DexIgnore
    public static /* final */ int MaterialButton_strokeWidth; // = 19;
    @DexIgnore
    public static /* final */ int[] MaterialCalendar; // = {16843277, 2130969190, 2130969191, 2130969192, 2130969193, 2130969707, 2130970172, 2130970173, 2130970174};
    @DexIgnore
    public static /* final */ int[] MaterialCalendarItem; // = {16843191, 16843192, 16843193, 16843194, 2130969447, 2130969456, 2130969457, 2130969464, 2130969465, R.attr.itemTextColor};
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_android_insetBottom; // = 3;
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_android_insetLeft; // = 0;
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_android_insetRight; // = 1;
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_android_insetTop; // = 2;
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_itemFillColor; // = 4;
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_itemShapeAppearance; // = 5;
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_itemShapeAppearanceOverlay; // = 6;
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_itemStrokeColor; // = 7;
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_itemStrokeWidth; // = 8;
    @DexIgnore
    public static /* final */ int MaterialCalendarItem_itemTextColor; // = 9;
    @DexIgnore
    public static /* final */ int MaterialCalendar_android_windowFullscreen; // = 0;
    @DexIgnore
    public static /* final */ int MaterialCalendar_dayInvalidStyle; // = 1;
    @DexIgnore
    public static /* final */ int MaterialCalendar_daySelectedStyle; // = 2;
    @DexIgnore
    public static /* final */ int MaterialCalendar_dayStyle; // = 3;
    @DexIgnore
    public static /* final */ int MaterialCalendar_dayTodayStyle; // = 4;
    @DexIgnore
    public static /* final */ int MaterialCalendar_rangeFillColor; // = 5;
    @DexIgnore
    public static /* final */ int MaterialCalendar_yearSelectedStyle; // = 6;
    @DexIgnore
    public static /* final */ int MaterialCalendar_yearStyle; // = 7;
    @DexIgnore
    public static /* final */ int MaterialCalendar_yearTodayStyle; // = 8;
    @DexIgnore
    public static /* final */ int[] MaterialCardView; // = {16843237, 2130968930, 2130968946, 2130968948, R.attr.rippleColor, 2130969786, 2130969789, 2130969880, 2130969892, 2130969896};
    @DexIgnore
    public static /* final */ int MaterialCardView_android_checkable; // = 0;
    @DexIgnore
    public static /* final */ int MaterialCardView_cardForegroundColor; // = 1;
    @DexIgnore
    public static /* final */ int MaterialCardView_checkedIcon; // = 2;
    @DexIgnore
    public static /* final */ int MaterialCardView_checkedIconTint; // = 3;
    @DexIgnore
    public static /* final */ int MaterialCardView_rippleColor; // = 4;
    @DexIgnore
    public static /* final */ int MaterialCardView_shapeAppearance; // = 5;
    @DexIgnore
    public static /* final */ int MaterialCardView_shapeAppearanceOverlay; // = 6;
    @DexIgnore
    public static /* final */ int MaterialCardView_state_dragged; // = 7;
    @DexIgnore
    public static /* final */ int MaterialCardView_strokeColor; // = 8;
    @DexIgnore
    public static /* final */ int MaterialCardView_strokeWidth; // = 9;
    @DexIgnore
    public static /* final */ int[] MaterialCheckBox; // = {2130968905, 2130970077};
    @DexIgnore
    public static /* final */ int MaterialCheckBox_buttonTint; // = 0;
    @DexIgnore
    public static /* final */ int MaterialCheckBox_useMaterialThemeColors; // = 1;
    @DexIgnore
    public static /* final */ int[] MaterialRadioButton; // = {2130970077};
    @DexIgnore
    public static /* final */ int MaterialRadioButton_useMaterialThemeColors; // = 0;
    @DexIgnore
    public static /* final */ int[] MaterialShape; // = {2130969786, 2130969789};
    @DexIgnore
    public static /* final */ int MaterialShape_shapeAppearance; // = 0;
    @DexIgnore
    public static /* final */ int MaterialShape_shapeAppearanceOverlay; // = 1;
    @DexIgnore
    public static /* final */ int[] MaterialTextAppearance; // = {16844159, 2130969554};
    @DexIgnore
    public static /* final */ int MaterialTextAppearance_android_lineHeight; // = 0;
    @DexIgnore
    public static /* final */ int MaterialTextAppearance_lineHeight; // = 1;
    @DexIgnore
    public static /* final */ int[] MaterialTextView; // = {16842804, 16844159, 2130969554};
    @DexIgnore
    public static /* final */ int MaterialTextView_android_lineHeight; // = 1;
    @DexIgnore
    public static /* final */ int MaterialTextView_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int MaterialTextView_lineHeight; // = 2;
    @DexIgnore
    public static /* final */ int[] MenuGroup; // = {16842766, 16842960, 16843156, 16843230, 16843231, 16843232};
    @DexIgnore
    public static /* final */ int MenuGroup_android_checkableBehavior; // = 5;
    @DexIgnore
    public static /* final */ int MenuGroup_android_enabled; // = 0;
    @DexIgnore
    public static /* final */ int MenuGroup_android_id; // = 1;
    @DexIgnore
    public static /* final */ int MenuGroup_android_menuCategory; // = 3;
    @DexIgnore
    public static /* final */ int MenuGroup_android_orderInCategory; // = 4;
    @DexIgnore
    public static /* final */ int MenuGroup_android_visible; // = 2;
    @DexIgnore
    public static /* final */ int[] MenuItem; // = {16842754, 16842766, 16842960, 16843014, 16843156, 16843230, 16843231, 16843233, 16843234, 16843235, 16843236, 16843237, 16843375, 2130968659, 2130968677, 2130968679, 2130968749, 2130969053, 2130969404, 2130969405, 2130969657, 2130969792, 2130970053};
    @DexIgnore
    public static /* final */ int MenuItem_actionLayout; // = 13;
    @DexIgnore
    public static /* final */ int MenuItem_actionProviderClass; // = 14;
    @DexIgnore
    public static /* final */ int MenuItem_actionViewClass; // = 15;
    @DexIgnore
    public static /* final */ int MenuItem_alphabeticModifiers; // = 16;
    @DexIgnore
    public static /* final */ int MenuItem_android_alphabeticShortcut; // = 9;
    @DexIgnore
    public static /* final */ int MenuItem_android_checkable; // = 11;
    @DexIgnore
    public static /* final */ int MenuItem_android_checked; // = 3;
    @DexIgnore
    public static /* final */ int MenuItem_android_enabled; // = 1;
    @DexIgnore
    public static /* final */ int MenuItem_android_icon; // = 0;
    @DexIgnore
    public static /* final */ int MenuItem_android_id; // = 2;
    @DexIgnore
    public static /* final */ int MenuItem_android_menuCategory; // = 5;
    @DexIgnore
    public static /* final */ int MenuItem_android_numericShortcut; // = 10;
    @DexIgnore
    public static /* final */ int MenuItem_android_onClick; // = 12;
    @DexIgnore
    public static /* final */ int MenuItem_android_orderInCategory; // = 6;
    @DexIgnore
    public static /* final */ int MenuItem_android_title; // = 7;
    @DexIgnore
    public static /* final */ int MenuItem_android_titleCondensed; // = 8;
    @DexIgnore
    public static /* final */ int MenuItem_android_visible; // = 4;
    @DexIgnore
    public static /* final */ int MenuItem_contentDescription; // = 17;
    @DexIgnore
    public static /* final */ int MenuItem_iconTint; // = 18;
    @DexIgnore
    public static /* final */ int MenuItem_iconTintMode; // = 19;
    @DexIgnore
    public static /* final */ int MenuItem_numericModifiers; // = 20;
    @DexIgnore
    public static /* final */ int MenuItem_showAsAction; // = 21;
    @DexIgnore
    public static /* final */ int MenuItem_tooltipText; // = 22;
    @DexIgnore
    public static /* final */ int[] MenuView; // = {16842926, 16843052, 16843053, 16843054, 16843055, 16843056, 16843057, 2130969679, 2130969899};
    @DexIgnore
    public static /* final */ int MenuView_android_headerBackground; // = 4;
    @DexIgnore
    public static /* final */ int MenuView_android_horizontalDivider; // = 2;
    @DexIgnore
    public static /* final */ int MenuView_android_itemBackground; // = 5;
    @DexIgnore
    public static /* final */ int MenuView_android_itemIconDisabledAlpha; // = 6;
    @DexIgnore
    public static /* final */ int MenuView_android_itemTextAppearance; // = 1;
    @DexIgnore
    public static /* final */ int MenuView_android_verticalDivider; // = 3;
    @DexIgnore
    public static /* final */ int MenuView_android_windowAnimationStyle; // = 0;
    @DexIgnore
    public static /* final */ int MenuView_preserveIconSpacing; // = 7;
    @DexIgnore
    public static /* final */ int MenuView_subMenuArrow; // = 8;
    @DexIgnore
    public static /* final */ int[] MessageBlackbgWidget; // = {2130969618, 2130969619, 2130969620, 2130969621, 2130969622};
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_image; // = 0;
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_sub_title; // = 1;
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_subtitle_text_style; // = 2;
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_text_font_color; // = 3;
    @DexIgnore
    public static /* final */ int MessageBlackbgWidget_message_bb_title; // = 4;
    @DexIgnore
    public static /* final */ int[] MockFitnessField; // = {2130969629};
    @DexIgnore
    public static /* final */ int MockFitnessField_mock_fitness_field_label; // = 0;
    @DexIgnore
    public static /* final */ int[] MyLetterListView; // = {2130969330, 2130969987, 2130969999};
    @DexIgnore
    public static /* final */ int MyLetterListView_fontPaths; // = 0;
    @DexIgnore
    public static /* final */ int MyLetterListView_textColors; // = 1;
    @DexIgnore
    public static /* final */ int MyLetterListView_textSizes; // = 2;
    @DexIgnore
    public static /* final */ int[] NavigationView; // = {16842964, 16842973, 16843039, 2130969258, R.attr.headerLayout, R.attr.itemBackground, 2130969448, 2130969450, 2130969451, R.attr.itemIconTint, 2130969453, 2130969456, 2130969457, 2130969458, 2130969459, 2130969460, 2130969461, 2130969462, R.attr.itemTextAppearance, R.attr.itemTextColor, R.attr.menu};
    @DexIgnore
    public static /* final */ int NavigationView_android_background; // = 0;
    @DexIgnore
    public static /* final */ int NavigationView_android_fitsSystemWindows; // = 1;
    @DexIgnore
    public static /* final */ int NavigationView_android_maxWidth; // = 2;
    @DexIgnore
    public static /* final */ int NavigationView_elevation; // = 3;
    @DexIgnore
    public static /* final */ int NavigationView_headerLayout; // = 4;
    @DexIgnore
    public static /* final */ int NavigationView_itemBackground; // = 5;
    @DexIgnore
    public static /* final */ int NavigationView_itemHorizontalPadding; // = 6;
    @DexIgnore
    public static /* final */ int NavigationView_itemIconPadding; // = 7;
    @DexIgnore
    public static /* final */ int NavigationView_itemIconSize; // = 8;
    @DexIgnore
    public static /* final */ int NavigationView_itemIconTint; // = 9;
    @DexIgnore
    public static /* final */ int NavigationView_itemMaxLines; // = 10;
    @DexIgnore
    public static /* final */ int NavigationView_itemShapeAppearance; // = 11;
    @DexIgnore
    public static /* final */ int NavigationView_itemShapeAppearanceOverlay; // = 12;
    @DexIgnore
    public static /* final */ int NavigationView_itemShapeFillColor; // = 13;
    @DexIgnore
    public static /* final */ int NavigationView_itemShapeInsetBottom; // = 14;
    @DexIgnore
    public static /* final */ int NavigationView_itemShapeInsetEnd; // = 15;
    @DexIgnore
    public static /* final */ int NavigationView_itemShapeInsetStart; // = 16;
    @DexIgnore
    public static /* final */ int NavigationView_itemShapeInsetTop; // = 17;
    @DexIgnore
    public static /* final */ int NavigationView_itemTextAppearance; // = 18;
    @DexIgnore
    public static /* final */ int NavigationView_itemTextColor; // = 19;
    @DexIgnore
    public static /* final */ int NavigationView_menu; // = 20;
    @DexIgnore
    public static /* final */ int[] NotificationConfigurationSummaryView; // = {2130968974, 2130969654, 2130969655, 2130969656};
    @DexIgnore
    public static /* final */ int NotificationConfigurationSummaryView_circleColor; // = 0;
    @DexIgnore
    public static /* final */ int NotificationConfigurationSummaryView_numeralColor; // = 1;
    @DexIgnore
    public static /* final */ int NotificationConfigurationSummaryView_numeralFontName; // = 2;
    @DexIgnore
    public static /* final */ int NotificationConfigurationSummaryView_numeralSize; // = 3;
    @DexIgnore
    public static /* final */ int[] NotificationSummaryDialView; // = {2130968585, 2130968586, 2130968587, 2130968588, 2130968589};
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_ClockImageSrc; // = 0;
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_DialItemBackground; // = 1;
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_DialItemPadding; // = 2;
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_DialItemSrc; // = 3;
    @DexIgnore
    public static /* final */ int NotificationSummaryDialView_NSDV_DialItemTintColor; // = 4;
    @DexIgnore
    public static /* final */ int[] NumberPicker; // = {2130969434, 2130969435, 2130969436, 2130969437, 2130969438, 2130969645, 2130969650, 2130969651, 2130969652, 2130969653, 2130969765, 2130969768, 2130969769, 2130969770, 2130969771, 2130969861, 2130970074, 2130970082};
    @DexIgnore
    public static /* final */ int NumberPicker_internalLayout; // = 0;
    @DexIgnore
    public static /* final */ int NumberPicker_internalMaxHeight; // = 1;
    @DexIgnore
    public static /* final */ int NumberPicker_internalMaxWidth; // = 2;
    @DexIgnore
    public static /* final */ int NumberPicker_internalMinHeight; // = 3;
    @DexIgnore
    public static /* final */ int NumberPicker_internalMinWidth; // = 4;
    @DexIgnore
    public static /* final */ int NumberPicker_np_fontFamily; // = 5;
    @DexIgnore
    public static /* final */ int NumberPicker_numberSelectedTextColor; // = 6;
    @DexIgnore
    public static /* final */ int NumberPicker_numberTextColor; // = 7;
    @DexIgnore
    public static /* final */ int NumberPicker_numberTextFont; // = 8;
    @DexIgnore
    public static /* final */ int NumberPicker_number_picker_font; // = 9;
    @DexIgnore
    public static /* final */ int NumberPicker_selectedColor; // = 10;
    @DexIgnore
    public static /* final */ int NumberPicker_selectedTextColor; // = 11;
    @DexIgnore
    public static /* final */ int NumberPicker_selectionDivider; // = 12;
    @DexIgnore
    public static /* final */ int NumberPicker_selectionDividerHeight; // = 13;
    @DexIgnore
    public static /* final */ int NumberPicker_selectionDividersDistance; // = 14;
    @DexIgnore
    public static /* final */ int NumberPicker_solidColor; // = 15;
    @DexIgnore
    public static /* final */ int NumberPicker_unselectedColor; // = 16;
    @DexIgnore
    public static /* final */ int NumberPicker_virtualButtonPressedDrawable; // = 17;
    @DexIgnore
    public static /* final */ int[] OverviewSleepDayChart; // = {2130968766, 2130969194, 2130969552};
    @DexIgnore
    public static /* final */ int OverviewSleepDayChart_awake_bar_height_percent; // = 0;
    @DexIgnore
    public static /* final */ int OverviewSleepDayChart_deep_bar_height_percent; // = 1;
    @DexIgnore
    public static /* final */ int OverviewSleepDayChart_light_bar_height_percent; // = 2;
    @DexIgnore
    public static /* final */ int[] OverviewSleepDaySummary; // = {2130968767, 2130968794, 2130968795, 2130969195, 2130969553};
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_awake_color; // = 0;
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_bar_margin; // = 1;
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_bar_radius; // = 2;
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_deep_color; // = 3;
    @DexIgnore
    public static /* final */ int OverviewSleepDaySummary_light_color; // = 4;
    @DexIgnore
    public static /* final */ int[] PaddingView; // = {2130969380, 2130970161};
    @DexIgnore
    public static /* final */ int PaddingView_heightByPercentageWithScreen; // = 0;
    @DexIgnore
    public static /* final */ int PaddingView_widthByPercentageWithScreen; // = 1;
    @DexIgnore
    public static /* final */ int[] PercentLayout_Layout; // = {2130969481, 2130969535, 2130969538, 2130969539, 2130969540, 2130969541, 2130969542, 2130969543, 2130969544, 2130969548};
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_aspectRatio; // = 0;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_heightPercent; // = 1;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginBottomPercent; // = 2;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginEndPercent; // = 3;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginLeftPercent; // = 4;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginPercent; // = 5;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginRightPercent; // = 6;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginStartPercent; // = 7;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_marginTopPercent; // = 8;
    @DexIgnore
    public static /* final */ int PercentLayout_Layout_layout_widthPercent; // = 9;
    @DexIgnore
    public static /* final */ int[] PopupWindow; // = {16843126, 16843465, 2130969658};
    @DexIgnore
    public static /* final */ int[] PopupWindowBackgroundState; // = {2130969877};
    @DexIgnore
    public static /* final */ int PopupWindowBackgroundState_state_above_anchor; // = 0;
    @DexIgnore
    public static /* final */ int PopupWindow_android_popupAnimationStyle; // = 1;
    @DexIgnore
    public static /* final */ int PopupWindow_android_popupBackground; // = 0;
    @DexIgnore
    public static /* final */ int PopupWindow_overlapAnchor; // = 2;
    @DexIgnore
    public static /* final */ int[] ProfileUnit; // = {2130969445};
    @DexIgnore
    public static /* final */ int ProfileUnit_is_selected; // = 0;
    @DexIgnore
    public static /* final */ int[] ProgressButton; // = {2130968758, 2130968770, 2130968862, 2130969573, 2130969574, 2130969575, 2130969767, 2130969986, 2130969989, 2130970075};
    @DexIgnore
    public static /* final */ int ProgressButton_autoDisableClickable; // = 0;
    @DexIgnore
    public static /* final */ int ProgressButton_backgroundColorStyle; // = 1;
    @DexIgnore
    public static /* final */ int ProgressButton_borderColorStyle; // = 2;
    @DexIgnore
    public static /* final */ int ProgressButton_loading; // = 3;
    @DexIgnore
    public static /* final */ int ProgressButton_loadingDrawable; // = 4;
    @DexIgnore
    public static /* final */ int ProgressButton_loadingText; // = 5;
    @DexIgnore
    public static /* final */ int ProgressButton_selectedText; // = 6;
    @DexIgnore
    public static /* final */ int ProgressButton_textColorStyle; // = 7;
    @DexIgnore
    public static /* final */ int ProgressButton_textFontStyle; // = 8;
    @DexIgnore
    public static /* final */ int ProgressButton_unselectedText; // = 9;
    @DexIgnore
    public static /* final */ int[] ProgressImageView; // = {2130969231, 2130969643, 2130969698};
    @DexIgnore
    public static /* final */ int ProgressImageView_done_color; // = 0;
    @DexIgnore
    public static /* final */ int ProgressImageView_normal_color; // = 1;
    @DexIgnore
    public static /* final */ int ProgressImageView_progress_width; // = 2;
    @DexIgnore
    public static /* final */ int[] ProximityCircleView; // = {2130969893, 2130969894, 2130969895, 2130970060};
    @DexIgnore
    public static /* final */ int ProximityCircleView_strokeColorInRange; // = 0;
    @DexIgnore
    public static /* final */ int ProximityCircleView_strokeColorOutOfRange; // = 1;
    @DexIgnore
    public static /* final */ int ProximityCircleView_strokeSize; // = 2;
    @DexIgnore
    public static /* final */ int ProximityCircleView_type; // = 3;
    @DexIgnore
    public static /* final */ int[] QBaseBarChart; // = {2130968590, 2130968591, 2130968592, 2130968593, 2130968594, 2130968595, 2130968601, 2130968602, 2130968603, 2130968604, 2130968605, 2130968606, 2130968607, 2130968608, 2130968609, 2130968610, 2130968611};
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegBarMargin; // = 0;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegBarTextSize; // = 1;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegBarWidth; // = 2;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegBottomFont; // = 3;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegFixedBarWidth; // = 4;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegFont; // = 5;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineBottomText; // = 6;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineBottomTextColor; // = 7;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineBottomTextSize; // = 8;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineEnable; // = 9;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineMargin; // = 10;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineStrokeWidth; // = 11;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineTextColor; // = 12;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineTextSize; // = 13;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegLineTopText; // = 14;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegSelectedColor; // = 15;
    @DexIgnore
    public static /* final */ int QBaseBarChart_QegnonBrandLineColor; // = 16;
    @DexIgnore
    public static /* final */ int[] QBaseChart; // = {2130968596, 2130968597, 2130968598, 2130968599, 2130968600};
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendColor; // = 0;
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendEnable; // = 1;
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendHeight; // = 2;
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendLetterSpacing; // = 3;
    @DexIgnore
    public static /* final */ int QBaseChart_QegLegendTextSize; // = 4;
    @DexIgnore
    public static /* final */ int[] RTLImageView; // = {2130969416, 2130969443};
    @DexIgnore
    public static /* final */ int RTLImageView_image_color; // = 0;
    @DexIgnore
    public static /* final */ int RTLImageView_is_blend_able; // = 1;
    @DexIgnore
    public static /* final */ int[] RecycleListView; // = {R.attr.paddingBottomNoButtons, R.attr.paddingTopNoTitle};
    @DexIgnore
    public static /* final */ int RecycleListView_paddingBottomNoButtons; // = 0;
    @DexIgnore
    public static /* final */ int RecycleListView_paddingTopNoTitle; // = 1;
    @DexIgnore
    public static /* final */ int[] RecyclerView; // = {16842948, 16842987, 16842993, 2130969306, 2130969307, 2130969308, 2130969309, 2130969310, R.attr.layoutManager, R.attr.reverseLayout, R.attr.spanCount, R.attr.stackFromEnd};
    @DexIgnore
    public static /* final */ int[] RecyclerViewAlphabetIndex; // = {2130969739, 2130969740, 2130969741, 2130969742, 2130969743};
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiCustomizable; // = 0;
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiFontSize; // = 1;
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiItemColor; // = 2;
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiStyle; // = 3;
    @DexIgnore
    public static /* final */ int RecyclerViewAlphabetIndex_rvaiWidth; // = 4;
    @DexIgnore
    public static /* final */ int[] RecyclerViewCalendar; // = {2130969152, 2130969153, 2130969160, 2130969163, 2130969165, 2130969166};
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_backgroundCalendarColor; // = 0;
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_completedTextColor; // = 1;
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_incompleteTextColor; // = 2;
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_noValueTextColor; // = 3;
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_progressColor; // = 4;
    @DexIgnore
    public static /* final */ int RecyclerViewCalendar_cv_selectedTextColor; // = 5;
    @DexIgnore
    public static /* final */ int[] RecyclerViewHeartRateCalendar; // = {2130969744, 2130969745, 2130969746, 2130969747, 2130969748, 2130969749, 2130969750};
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_aboveAverageResting; // = 0;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_averageResting; // = 1;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_backgroundCalendarColor; // = 2;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_circleOutnonBrandLineColor; // = 3;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_hasValueTextColor; // = 4;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_noValueTextColor; // = 5;
    @DexIgnore
    public static /* final */ int RecyclerViewHeartRateCalendar_rvhrdc_selectedTextColor; // = 6;
    @DexIgnore
    public static /* final */ int RecyclerView_android_clipToPadding; // = 1;
    @DexIgnore
    public static /* final */ int RecyclerView_android_descendantFocusability; // = 2;
    @DexIgnore
    public static /* final */ int RecyclerView_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollEnabled; // = 3;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollHorizontalThumbDrawable; // = 4;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollHorizontalTrackDrawable; // = 5;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollVerticalThumbDrawable; // = 6;
    @DexIgnore
    public static /* final */ int RecyclerView_fastScrollVerticalTrackDrawable; // = 7;
    @DexIgnore
    public static /* final */ int RecyclerView_layoutManager; // = 8;
    @DexIgnore
    public static /* final */ int RecyclerView_reverseLayout; // = 9;
    @DexIgnore
    public static /* final */ int RecyclerView_spanCount; // = 10;
    @DexIgnore
    public static /* final */ int RecyclerView_stackFromEnd; // = 11;
    @DexIgnore
    public static /* final */ int[] RingChart; // = {2130969002, 2130969349, 2130970160};
    @DexIgnore
    public static /* final */ int RingChart_color; // = 0;
    @DexIgnore
    public static /* final */ int RingChart_goal; // = 1;
    @DexIgnore
    public static /* final */ int RingChart_width; // = 2;
    @DexIgnore
    public static /* final */ int[] RingProgressBar; // = {2130969406, 2130969408, 2130969409, 2130969410, 2130969614, 2130969688, 2130969728, 2130969729, 2130969730, 2130969862, 2130969897};
    @DexIgnore
    public static /* final */ int RingProgressBar_icon_background; // = 0;
    @DexIgnore
    public static /* final */ int RingProgressBar_icon_filter; // = 1;
    @DexIgnore
    public static /* final */ int RingProgressBar_icon_size; // = 2;
    @DexIgnore
    public static /* final */ int RingProgressBar_icon_source; // = 3;
    @DexIgnore
    public static /* final */ int RingProgressBar_max_progress; // = 4;
    @DexIgnore
    public static /* final */ int RingProgressBar_progress; // = 5;
    @DexIgnore
    public static /* final */ int RingProgressBar_ring_progress_background_color; // = 6;
    @DexIgnore
    public static /* final */ int RingProgressBar_ring_progress_color; // = 7;
    @DexIgnore
    public static /* final */ int RingProgressBar_ring_progress_index; // = 8;
    @DexIgnore
    public static /* final */ int RingProgressBar_space_width; // = 9;
    @DexIgnore
    public static /* final */ int RingProgressBar_stroke_width; // = 10;
    @DexIgnore
    public static /* final */ int[] RoundRectangleSeekBar; // = {2130969712, 2130969713, 2130969714, 2130969715, 2130970020, 2130970021, 2130970022, 2130970023, 2130970073};
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_recommended_line_color; // = 0;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_recommended_text_color; // = 1;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_recommended_text_size; // = 2;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_recommended_text_typeface; // = 3;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_thumb_text_background_color; // = 4;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_thumb_text_color; // = 5;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_thumb_text_size; // = 6;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_thumb_text_typeface; // = 7;
    @DexIgnore
    public static /* final */ int RoundRectangleSeekBar_unit; // = 8;
    @DexIgnore
    public static /* final */ int[] RulerValuePicker; // = {2130969149, 2130969422, 2130969424, 2130969430, 2130969578, 2130969615, 2130969628, 2130969644, 2130969733, 2130969734, 2130969735, 2130969736, 2130969737, 2130969738, 2130969791};
    @DexIgnore
    public static /* final */ int RulerValuePicker_current_value_picker_background; // = 0;
    @DexIgnore
    public static /* final */ int RulerValuePicker_indicator_color; // = 1;
    @DexIgnore
    public static /* final */ int RulerValuePicker_indicator_interval; // = 2;
    @DexIgnore
    public static /* final */ int RulerValuePicker_indicator_width; // = 3;
    @DexIgnore
    public static /* final */ int RulerValuePicker_long_height_height_ratio; // = 4;
    @DexIgnore
    public static /* final */ int RulerValuePicker_max_value; // = 5;
    @DexIgnore
    public static /* final */ int RulerValuePicker_min_value; // = 6;
    @DexIgnore
    public static /* final */ int RulerValuePicker_notch_color; // = 7;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_fontFamily; // = 8;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_selected_text_size; // = 9;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_text_color; // = 10;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_text_size; // = 11;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_text_style; // = 12;
    @DexIgnore
    public static /* final */ int RulerValuePicker_ruler_view_background; // = 13;
    @DexIgnore
    public static /* final */ int RulerValuePicker_short_height_height_ratio; // = 14;
    @DexIgnore
    public static /* final */ int[] RulerView; // = {2130969422, 2130969424, 2130969430, 2130969578, 2130969615, 2130969628, 2130969735, 2130969736, 2130969791};
    @DexIgnore
    public static /* final */ int RulerView_indicator_color; // = 0;
    @DexIgnore
    public static /* final */ int RulerView_indicator_interval; // = 1;
    @DexIgnore
    public static /* final */ int RulerView_indicator_width; // = 2;
    @DexIgnore
    public static /* final */ int RulerView_long_height_height_ratio; // = 3;
    @DexIgnore
    public static /* final */ int RulerView_max_value; // = 4;
    @DexIgnore
    public static /* final */ int RulerView_min_value; // = 5;
    @DexIgnore
    public static /* final */ int RulerView_ruler_text_color; // = 6;
    @DexIgnore
    public static /* final */ int RulerView_ruler_text_size; // = 7;
    @DexIgnore
    public static /* final */ int RulerView_short_height_height_ratio; // = 8;
    @DexIgnore
    public static /* final */ int[] ScrimInsetsFrameLayout; // = {2130969432};
    @DexIgnore
    public static /* final */ int ScrimInsetsFrameLayout_insetForeground; // = 0;
    @DexIgnore
    public static /* final */ int[] ScrollingViewBehavior_Layout; // = {2130968854};
    @DexIgnore
    public static /* final */ int ScrollingViewBehavior_Layout_behavior_overlapTop; // = 0;
    @DexIgnore
    public static /* final */ int[] SearchView; // = {16842970, 16843039, 16843296, 16843364, 2130968987, 2130969038, 2130969198, 2130969348, 2130969411, 2130969477, 2130969703, 2130969704, 2130969758, 2130969759, 2130969901, 2130969906, 2130970095};
    @DexIgnore
    public static /* final */ int SearchView_android_focusable; // = 0;
    @DexIgnore
    public static /* final */ int SearchView_android_imeOptions; // = 3;
    @DexIgnore
    public static /* final */ int SearchView_android_inputType; // = 2;
    @DexIgnore
    public static /* final */ int SearchView_android_maxWidth; // = 1;
    @DexIgnore
    public static /* final */ int SearchView_closeIcon; // = 4;
    @DexIgnore
    public static /* final */ int SearchView_commitIcon; // = 5;
    @DexIgnore
    public static /* final */ int SearchView_defaultQueryHint; // = 6;
    @DexIgnore
    public static /* final */ int SearchView_goIcon; // = 7;
    @DexIgnore
    public static /* final */ int SearchView_iconifiedByDefault; // = 8;
    @DexIgnore
    public static /* final */ int SearchView_layout; // = 9;
    @DexIgnore
    public static /* final */ int SearchView_queryBackground; // = 10;
    @DexIgnore
    public static /* final */ int SearchView_queryHint; // = 11;
    @DexIgnore
    public static /* final */ int SearchView_searchHintIcon; // = 12;
    @DexIgnore
    public static /* final */ int SearchView_searchIcon; // = 13;
    @DexIgnore
    public static /* final */ int SearchView_submitBackground; // = 14;
    @DexIgnore
    public static /* final */ int SearchView_suggestionRowLayout; // = 15;
    @DexIgnore
    public static /* final */ int SearchView_voiceIcon; // = 16;
    @DexIgnore
    public static /* final */ int[] ShapeAppearance; // = {2130969084, 2130969085, 2130969086, 2130969087, 2130969088, 2130969090, 2130969091, 2130969092, 2130969093, 2130969094};
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerFamily; // = 0;
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerFamilyBottomLeft; // = 1;
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerFamilyBottomRight; // = 2;
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerFamilyTopLeft; // = 3;
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerFamilyTopRight; // = 4;
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerSize; // = 5;
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerSizeBottomLeft; // = 6;
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerSizeBottomRight; // = 7;
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerSizeTopLeft; // = 8;
    @DexIgnore
    public static /* final */ int ShapeAppearance_cornerSizeTopRight; // = 9;
    @DexIgnore
    public static /* final */ int[] ShimmerFrameLayout; // = {2130968751, 2130968764, 2130968805, 2130969245, 2130969246, 2130969316, 2130969317, 2130969433, 2130969719, 2130969720, 2130969721, 2130969722, 2130969723, 2130969785, 2130970027};
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_angle; // = 0;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_auto_start; // = 1;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_base_alpha; // = 2;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_dropoff; // = 3;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_duration; // = 4;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_fixed_height; // = 5;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_fixed_width; // = 6;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_intensity; // = 7;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_relative_height; // = 8;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_relative_width; // = 9;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_repeat_count; // = 10;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_repeat_delay; // = 11;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_repeat_mode; // = 12;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_shape; // = 13;
    @DexIgnore
    public static /* final */ int ShimmerFrameLayout_tilt; // = 14;
    @DexIgnore
    public static /* final */ int[] ShimmerView; // = {2130969718};
    @DexIgnore
    public static /* final */ int ShimmerView_reflectionColor; // = 0;
    @DexIgnore
    public static /* final */ int[] SignInButton; // = {2130968902, 2130969021, 2130969752};
    @DexIgnore
    public static /* final */ int SignInButton_buttonSize; // = 0;
    @DexIgnore
    public static /* final */ int SignInButton_colorScheme; // = 1;
    @DexIgnore
    public static /* final */ int SignInButton_scopeUris; // = 2;
    @DexIgnore
    public static /* final */ int[] SleepDayDetailsChart; // = {2130969817, 2130969818, 2130969819, 2130969820, 2130969821, 2130969822, 2130969823};
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_awake_color; // = 0;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_dash_line_color; // = 1;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_light_color; // = 2;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_line_color; // = 3;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_restful_color; // = 4;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_text_color; // = 5;
    @DexIgnore
    public static /* final */ int SleepDayDetailsChart_sleep_day_text_font; // = 6;
    @DexIgnore
    public static /* final */ int[] SleepHorizontalBar; // = {2130969814, 2130969824, 2130969835, 2130969836, 2130969837, 2130969838, 2130969839, 2130969840, 2130969841, 2130969842, 2130969843, 2130969844, 2130969845, 2130969846, 2130969847, 2130969848};
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_background_color; // = 0;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_fontFamily; // = 1;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_awake_color; // = 2;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_color; // = 3;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_deep_color; // = 4;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_margin_end; // = 5;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_radius; // = 6;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_sleep_color; // = 7;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_space; // = 8;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_progress_width; // = 9;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_star_alpha; // = 10;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_star_res; // = 11;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_star_size; // = 12;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_text; // = 13;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_text_color; // = 14;
    @DexIgnore
    public static /* final */ int SleepHorizontalBar_sleep_text_size; // = 15;
    @DexIgnore
    public static /* final */ int[] SleepMonthDetailsChart; // = {2130969825, 2130969826, 2130969827, 2130969828, 2130969829, 2130969830, 2130969831, 2130969832};
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_bar_awake_color; // = 0;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_bar_light_color; // = 1;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_bar_restful_color; // = 2;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_dash_line_color; // = 3;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_line_color; // = 4;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_text_color; // = 5;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_text_font; // = 6;
    @DexIgnore
    public static /* final */ int SleepMonthDetailsChart_sleep_month_text_size; // = 7;
    @DexIgnore
    public static /* final */ int[] SleepQualityChart; // = {2130969815, 2130969816, 2130969833, 2130969834, 2130969857};
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_color_end; // = 0;
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_color_start; // = 1;
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_percent; // = 2;
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_percent_image; // = 3;
    @DexIgnore
    public static /* final */ int SleepQualityChart_sleep_width; // = 4;
    @DexIgnore
    public static /* final */ int[] SleepWeekDetailsChart; // = {2130969849, 2130969850, 2130969851, 2130969852, 2130969853, 2130969854, 2130969855, 2130969856};
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_awake_color; // = 0;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_dash_line_color; // = 1;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_light_color; // = 2;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_line_color; // = 3;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_restful_color; // = 4;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_text_color; // = 5;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_text_font; // = 6;
    @DexIgnore
    public static /* final */ int SleepWeekDetailsChart_sleep_week_text_size; // = 7;
    @DexIgnore
    public static /* final */ int[] Snackbar; // = {2130969858, 2130969859};
    @DexIgnore
    public static /* final */ int[] SnackbarLayout; // = {16843039, 2130968678, 2130968752, 2130968775, 2130969258, R.attr.maxActionInlineWidth};
    @DexIgnore
    public static /* final */ int SnackbarLayout_actionTextColorAlpha; // = 1;
    @DexIgnore
    public static /* final */ int SnackbarLayout_android_maxWidth; // = 0;
    @DexIgnore
    public static /* final */ int SnackbarLayout_animationMode; // = 2;
    @DexIgnore
    public static /* final */ int SnackbarLayout_backgroundOverlayColorAlpha; // = 3;
    @DexIgnore
    public static /* final */ int SnackbarLayout_elevation; // = 4;
    @DexIgnore
    public static /* final */ int SnackbarLayout_maxActionInlineWidth; // = 5;
    @DexIgnore
    public static /* final */ int Snackbar_snackbarButtonStyle; // = 0;
    @DexIgnore
    public static /* final */ int Snackbar_snackbarStyle; // = 1;
    @DexIgnore
    public static /* final */ int[] SnappyHorizontalRecyclerView; // = {2130969646};
    @DexIgnore
    public static /* final */ int SnappyHorizontalRecyclerView_numOfDisplayingItem; // = 0;
    @DexIgnore
    public static /* final */ int[] Spinner; // = {16842930, 16843126, 16843131, 16843362, 2130969677};
    @DexIgnore
    public static /* final */ int Spinner_android_dropDownWidth; // = 3;
    @DexIgnore
    public static /* final */ int Spinner_android_entries; // = 0;
    @DexIgnore
    public static /* final */ int Spinner_android_popupBackground; // = 1;
    @DexIgnore
    public static /* final */ int Spinner_android_prompt; // = 2;
    @DexIgnore
    public static /* final */ int Spinner_popupTheme; // = 4;
    @DexIgnore
    public static /* final */ int[] StateListDrawable; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int[] StateListDrawableItem; // = {16843161};
    @DexIgnore
    public static /* final */ int StateListDrawableItem_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_visible; // = 1;
    @DexIgnore
    public static /* final */ int[] SwitchCompat; // = {16843044, 16843045, 16843074, 2130969795, 2130969867, 2130969910, 2130969911, 2130969913, 2130970017, 2130970018, 2130970019, 2130970055, 2130970056, 2130970057};
    @DexIgnore
    public static /* final */ int SwitchCompat_android_textOff; // = 1;
    @DexIgnore
    public static /* final */ int SwitchCompat_android_textOn; // = 0;
    @DexIgnore
    public static /* final */ int SwitchCompat_android_thumb; // = 2;
    @DexIgnore
    public static /* final */ int SwitchCompat_showText; // = 3;
    @DexIgnore
    public static /* final */ int SwitchCompat_splitTrack; // = 4;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchMinWidth; // = 5;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchPadding; // = 6;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchTextAppearance; // = 7;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTextPadding; // = 8;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTint; // = 9;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTintMode; // = 10;
    @DexIgnore
    public static /* final */ int SwitchCompat_track; // = 11;
    @DexIgnore
    public static /* final */ int SwitchCompat_trackTint; // = 12;
    @DexIgnore
    public static /* final */ int SwitchCompat_trackTintMode; // = 13;
    @DexIgnore
    public static /* final */ int[] SwitchMaterial; // = {2130970077};
    @DexIgnore
    public static /* final */ int SwitchMaterial_useMaterialThemeColors; // = 0;
    @DexIgnore
    public static /* final */ int[] TabItem; // = {16842754, 16842994, 16843087};
    @DexIgnore
    public static /* final */ int TabItem_android_icon; // = 0;
    @DexIgnore
    public static /* final */ int TabItem_android_layout; // = 1;
    @DexIgnore
    public static /* final */ int TabItem_android_text; // = 2;
    @DexIgnore
    public static /* final */ int[] TabLayout; // = {R.attr.tabBackground, R.attr.tabContentStart, R.attr.tabGravity, 2130969920, 2130969921, 2130969922, 2130969923, R.attr.tabIndicatorColor, 2130969925, 2130969926, R.attr.tabIndicatorHeight, 2130969928, R.attr.tabMaxWidth, R.attr.tabMinWidth, R.attr.tabMode, R.attr.tabPadding, R.attr.tabPaddingBottom, R.attr.tabPaddingEnd, R.attr.tabPaddingStart, R.attr.tabPaddingTop, 2130969937, R.attr.tabSelectedTextColor, R.attr.tabTextAppearance, R.attr.tabTextColor, 2130969942};
    @DexIgnore
    public static /* final */ int TabLayout_tabBackground; // = 0;
    @DexIgnore
    public static /* final */ int TabLayout_tabContentStart; // = 1;
    @DexIgnore
    public static /* final */ int TabLayout_tabGravity; // = 2;
    @DexIgnore
    public static /* final */ int TabLayout_tabIconTint; // = 3;
    @DexIgnore
    public static /* final */ int TabLayout_tabIconTintMode; // = 4;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicator; // = 5;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorAnimationDuration; // = 6;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorColor; // = 7;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorFullWidth; // = 8;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorGravity; // = 9;
    @DexIgnore
    public static /* final */ int TabLayout_tabIndicatorHeight; // = 10;
    @DexIgnore
    public static /* final */ int TabLayout_tabInlineLabel; // = 11;
    @DexIgnore
    public static /* final */ int TabLayout_tabMaxWidth; // = 12;
    @DexIgnore
    public static /* final */ int TabLayout_tabMinWidth; // = 13;
    @DexIgnore
    public static /* final */ int TabLayout_tabMode; // = 14;
    @DexIgnore
    public static /* final */ int TabLayout_tabPadding; // = 15;
    @DexIgnore
    public static /* final */ int TabLayout_tabPaddingBottom; // = 16;
    @DexIgnore
    public static /* final */ int TabLayout_tabPaddingEnd; // = 17;
    @DexIgnore
    public static /* final */ int TabLayout_tabPaddingStart; // = 18;
    @DexIgnore
    public static /* final */ int TabLayout_tabPaddingTop; // = 19;
    @DexIgnore
    public static /* final */ int TabLayout_tabRippleColor; // = 20;
    @DexIgnore
    public static /* final */ int TabLayout_tabSelectedTextColor; // = 21;
    @DexIgnore
    public static /* final */ int TabLayout_tabTextAppearance; // = 22;
    @DexIgnore
    public static /* final */ int TabLayout_tabTextColor; // = 23;
    @DexIgnore
    public static /* final */ int TabLayout_tabUnboundedRipple; // = 24;
    @DexIgnore
    public static /* final */ int[] TextAppearance; // = {16842901, 16842902, 16842903, 16842904, 16842906, 16842907, 16843105, 16843106, 16843107, 16843108, 16843692, 16844165, 2130969327, com.misfit.frameworks.buttonservice.R.attr.fontVariationSettings, 2130969952, 2130969997};
    @DexIgnore
    public static /* final */ int TextAppearance_android_fontFamily; // = 10;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowColor; // = 6;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowDx; // = 7;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowDy; // = 8;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowRadius; // = 9;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColor; // = 3;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColorHint; // = 4;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColorLink; // = 5;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textFontWeight; // = 11;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textSize; // = 0;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textStyle; // = 2;
    @DexIgnore
    public static /* final */ int TextAppearance_android_typeface; // = 1;
    @DexIgnore
    public static /* final */ int TextAppearance_fontFamily; // = 12;
    @DexIgnore
    public static /* final */ int TextAppearance_fontVariationSettings; // = 13;
    @DexIgnore
    public static /* final */ int TextAppearance_textAllCaps; // = 14;
    @DexIgnore
    public static /* final */ int TextAppearance_textLocale; // = 15;
    @DexIgnore
    public static /* final */ int[] TextInputLayout; // = {16842906, 16843088, 2130968874, 2130968875, 2130968876, 2130968877, 2130968878, 2130968879, 2130968880, 2130968883, 2130968885, 2130968886, R.attr.counterEnabled, R.attr.counterMaxLength, R.attr.counterOverflowTextAppearance, 2130969098, R.attr.counterTextAppearance, 2130969100, 2130969268, 2130969269, 2130969270, 2130969271, 2130969272, 2130969273, R.attr.errorEnabled, 2130969278, 2130969279, 2130969280, R.attr.errorTextAppearance, 2130969283, 2130969382, 2130969383, 2130969384, 2130969385, R.attr.hintAnimationEnabled, R.attr.hintEnabled, R.attr.hintTextAppearance, 2130969393, R.attr.passwordToggleContentDescription, R.attr.passwordToggleDrawable, R.attr.passwordToggleEnabled, R.attr.passwordToggleTint, R.attr.passwordToggleTintMode, 2130969786, 2130969789, 2130969871, 2130969872, 2130969873, 2130969874, 2130969875};
    @DexIgnore
    public static /* final */ int TextInputLayout_android_hint; // = 1;
    @DexIgnore
    public static /* final */ int TextInputLayout_android_textColorHint; // = 0;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxBackgroundColor; // = 2;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxBackgroundMode; // = 3;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCollapsedPaddingTop; // = 4;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCornerRadiusBottomEnd; // = 5;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCornerRadiusBottomStart; // = 6;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCornerRadiusTopEnd; // = 7;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxCornerRadiusTopStart; // = 8;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxStrokeColor; // = 9;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxStrokeWidth; // = 10;
    @DexIgnore
    public static /* final */ int TextInputLayout_boxStrokeWidthFocused; // = 11;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterEnabled; // = 12;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterMaxLength; // = 13;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterOverflowTextAppearance; // = 14;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterOverflowTextColor; // = 15;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterTextAppearance; // = 16;
    @DexIgnore
    public static /* final */ int TextInputLayout_counterTextColor; // = 17;
    @DexIgnore
    public static /* final */ int TextInputLayout_endIconCheckable; // = 18;
    @DexIgnore
    public static /* final */ int TextInputLayout_endIconContentDescription; // = 19;
    @DexIgnore
    public static /* final */ int TextInputLayout_endIconDrawable; // = 20;
    @DexIgnore
    public static /* final */ int TextInputLayout_endIconMode; // = 21;
    @DexIgnore
    public static /* final */ int TextInputLayout_endIconTint; // = 22;
    @DexIgnore
    public static /* final */ int TextInputLayout_endIconTintMode; // = 23;
    @DexIgnore
    public static /* final */ int TextInputLayout_errorEnabled; // = 24;
    @DexIgnore
    public static /* final */ int TextInputLayout_errorIconDrawable; // = 25;
    @DexIgnore
    public static /* final */ int TextInputLayout_errorIconTint; // = 26;
    @DexIgnore
    public static /* final */ int TextInputLayout_errorIconTintMode; // = 27;
    @DexIgnore
    public static /* final */ int TextInputLayout_errorTextAppearance; // = 28;
    @DexIgnore
    public static /* final */ int TextInputLayout_errorTextColor; // = 29;
    @DexIgnore
    public static /* final */ int TextInputLayout_helperText; // = 30;
    @DexIgnore
    public static /* final */ int TextInputLayout_helperTextEnabled; // = 31;
    @DexIgnore
    public static /* final */ int TextInputLayout_helperTextTextAppearance; // = 32;
    @DexIgnore
    public static /* final */ int TextInputLayout_helperTextTextColor; // = 33;
    @DexIgnore
    public static /* final */ int TextInputLayout_hintAnimationEnabled; // = 34;
    @DexIgnore
    public static /* final */ int TextInputLayout_hintEnabled; // = 35;
    @DexIgnore
    public static /* final */ int TextInputLayout_hintTextAppearance; // = 36;
    @DexIgnore
    public static /* final */ int TextInputLayout_hintTextColor; // = 37;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleContentDescription; // = 38;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleDrawable; // = 39;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleEnabled; // = 40;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleTint; // = 41;
    @DexIgnore
    public static /* final */ int TextInputLayout_passwordToggleTintMode; // = 42;
    @DexIgnore
    public static /* final */ int TextInputLayout_shapeAppearance; // = 43;
    @DexIgnore
    public static /* final */ int TextInputLayout_shapeAppearanceOverlay; // = 44;
    @DexIgnore
    public static /* final */ int TextInputLayout_startIconCheckable; // = 45;
    @DexIgnore
    public static /* final */ int TextInputLayout_startIconContentDescription; // = 46;
    @DexIgnore
    public static /* final */ int TextInputLayout_startIconDrawable; // = 47;
    @DexIgnore
    public static /* final */ int TextInputLayout_startIconTint; // = 48;
    @DexIgnore
    public static /* final */ int TextInputLayout_startIconTintMode; // = 49;
    @DexIgnore
    public static /* final */ int[] ThemeEnforcement; // = {16842804, 2130969274, 2130969275};
    @DexIgnore
    public static /* final */ int ThemeEnforcement_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int ThemeEnforcement_enforceMaterialTheme; // = 1;
    @DexIgnore
    public static /* final */ int ThemeEnforcement_enforceTextAppearance; // = 2;
    @DexIgnore
    public static /* final */ int[] TitleMessageWidget; // = {2130970042, 2130970043, 2130970044, 2130970045, 2130970046, 2130970047};
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_image; // = 0;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_image_max_height; // = 1;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_image_max_width; // = 2;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_image_padding; // = 3;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_message; // = 4;
    @DexIgnore
    public static /* final */ int TitleMessageWidget_titlemessage_title; // = 5;
    @DexIgnore
    public static /* final */ int[] TitlePunchCircleWidget; // = {2130968983, 2130968984};
    @DexIgnore
    public static /* final */ int TitlePunchCircleWidget_circle_text; // = 0;
    @DexIgnore
    public static /* final */ int TitlePunchCircleWidget_circle_text_color; // = 1;
    @DexIgnore
    public static /* final */ int[] TodayHeartRateChart; // = {2130968612, 2130968613, 2130968614, 2130968615, 2130968616, 2130968617, 2130968618, 2130968619, 2130968620};
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_AxesColor; // = 0;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_BackgroundColor; // = 1;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_HeartRateFontSize; // = 2;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_HeartRateTextFont; // = 3;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_LineEndColor; // = 4;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_LineStartColor; // = 5;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_TextColor; // = 6;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_TimeFontSize; // = 7;
    @DexIgnore
    public static /* final */ int TodayHeartRateChart_THR_TimeTextFont; // = 8;
    @DexIgnore
    public static /* final */ int[] Toolbar; // = {16842927, 16843072, 2130968893, 2130968996, 2130968997, 2130969054, 2130969055, 2130969056, 2130969057, 2130969058, 2130969059, 2130969576, 2130969577, 2130969611, R.attr.menu, 2130969635, 2130969636, 2130969677, 2130969902, 2130969903, 2130969904, 2130970031, 2130970033, 2130970034, 2130970035, 2130970036, 2130970037, 2130970038, 2130970039, 2130970040};
    @DexIgnore
    public static /* final */ int Toolbar_android_gravity; // = 0;
    @DexIgnore
    public static /* final */ int Toolbar_android_minHeight; // = 1;
    @DexIgnore
    public static /* final */ int Toolbar_buttonGravity; // = 2;
    @DexIgnore
    public static /* final */ int Toolbar_collapseContentDescription; // = 3;
    @DexIgnore
    public static /* final */ int Toolbar_collapseIcon; // = 4;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetEnd; // = 5;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetEndWithActions; // = 6;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetLeft; // = 7;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetRight; // = 8;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetStart; // = 9;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetStartWithNavigation; // = 10;
    @DexIgnore
    public static /* final */ int Toolbar_logo; // = 11;
    @DexIgnore
    public static /* final */ int Toolbar_logoDescription; // = 12;
    @DexIgnore
    public static /* final */ int Toolbar_maxButtonHeight; // = 13;
    @DexIgnore
    public static /* final */ int Toolbar_menu; // = 14;
    @DexIgnore
    public static /* final */ int Toolbar_navigationContentDescription; // = 15;
    @DexIgnore
    public static /* final */ int Toolbar_navigationIcon; // = 16;
    @DexIgnore
    public static /* final */ int Toolbar_popupTheme; // = 17;
    @DexIgnore
    public static /* final */ int Toolbar_subtitle; // = 18;
    @DexIgnore
    public static /* final */ int Toolbar_subtitleTextAppearance; // = 19;
    @DexIgnore
    public static /* final */ int Toolbar_subtitleTextColor; // = 20;
    @DexIgnore
    public static /* final */ int Toolbar_title; // = 21;
    @DexIgnore
    public static /* final */ int Toolbar_titleMargin; // = 22;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginBottom; // = 23;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginEnd; // = 24;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginStart; // = 25;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginTop; // = 26;
    @DexIgnore
    public static /* final */ int Toolbar_titleMargins; // = 27;
    @DexIgnore
    public static /* final */ int Toolbar_titleTextAppearance; // = 28;
    @DexIgnore
    public static /* final */ int Toolbar_titleTextColor; // = 29;
    @DexIgnore
    public static /* final */ int[] UnderLine; // = {2130969226, 2130969801};
    @DexIgnore
    public static /* final */ int UnderLine_distance; // = 0;
    @DexIgnore
    public static /* final */ int UnderLine_size; // = 1;
    @DexIgnore
    public static /* final */ int[] UnderlinedTextView; // = {2130970069, 2130970070, 2130970071, 2130970072};
    @DexIgnore
    public static /* final */ int UnderlinedTextView_underlineDistance; // = 0;
    @DexIgnore
    public static /* final */ int UnderlinedTextView_underlineStrokeWidth; // = 1;
    @DexIgnore
    public static /* final */ int UnderlinedTextView_underlineWidth; // = 2;
    @DexIgnore
    public static /* final */ int UnderlinedTextView_undernonBrandLineColor; // = 3;
    @DexIgnore
    public static /* final */ int[] View; // = {16842752, 16842970, 2130969660, 2130969662, 2130970013};
    @DexIgnore
    public static /* final */ int[] ViewBackgroundHelper; // = {16842964, 2130968779, 2130968780};
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_android_background; // = 0;
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_backgroundTint; // = 1;
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_backgroundTintMode; // = 2;
    @DexIgnore
    public static /* final */ int[] ViewPager2; // = {16842948};
    @DexIgnore
    public static /* final */ int ViewPager2_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int[] ViewPagerIndicator; // = {2130970096, 2130970097, 2130970098, 2130970099, 2130970100, 2130970101};
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiCirclePageIndicatorStyle; // = 0;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiIconPageIndicatorStyle; // = 1;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiLinePageIndicatorStyle; // = 2;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiTabPageIndicatorStyle; // = 3;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiTitlePageIndicatorStyle; // = 4;
    @DexIgnore
    public static /* final */ int ViewPagerIndicator_vpiUnderlinePageIndicatorStyle; // = 5;
    @DexIgnore
    public static /* final */ int[] ViewStubCompat; // = {16842960, 16842994, 16842995};
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_id; // = 0;
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_inflatedId; // = 2;
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_layout; // = 1;
    @DexIgnore
    public static /* final */ int View_android_focusable; // = 1;
    @DexIgnore
    public static /* final */ int View_android_theme; // = 0;
    @DexIgnore
    public static /* final */ int View_paddingEnd; // = 2;
    @DexIgnore
    public static /* final */ int View_paddingStart; // = 3;
    @DexIgnore
    public static /* final */ int View_theme; // = 4;
    @DexIgnore
    public static /* final */ int[] WLHeartRateChart; // = {2130968634, 2130968635, 2130968636, 2130968637, 2130968638, 2130968639, 2130968640, 2130968641, 2130968642, 2130968643, 2130968644};
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_AxesColor; // = 0;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_BarColor; // = 1;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_GridColor; // = 2;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_HeartRateFontSize; // = 3;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_HeartRateTextFont; // = 4;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_RoundRadius; // = 5;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_TextColor; // = 6;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_TimeFontSize; // = 7;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_TimeTextFont; // = 8;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_Type; // = 9;
    @DexIgnore
    public static /* final */ int WLHeartRateChart_WLHR_nonBrandLineColor; // = 10;
    @DexIgnore
    public static /* final */ int[] WatchAppControl; // = {2130970102, 2130970103, 2130970104, 2130970105, 2130970106, 2130970107, 2130970108, 2130970109, 2130970110, 2130970111, 2130970112, 2130970113, 2130970114};
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_bottom_content; // = 0;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_bottom_content_color; // = 1;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_bottom_content_size; // = 2;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_default_color; // = 3;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_default_color_selected; // = 4;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_end_content; // = 5;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_end_content_color; // = 6;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_end_content_size; // = 7;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_icon_background; // = 8;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_icon_background_selected; // = 9;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_icon_src; // = 10;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_remove_mode; // = 11;
    @DexIgnore
    public static /* final */ int WatchAppControl_watch_app_selected; // = 12;
    @DexIgnore
    public static /* final */ int[] WaveView; // = {2130970120, 2130970121, 2130970122, 2130970123, 2130970124, 2130970125, 2130970126, 2130970127, 2130970128};
    @DexIgnore
    public static /* final */ int WaveView_wave_color; // = 0;
    @DexIgnore
    public static /* final */ int WaveView_wave_number_wave; // = 1;
    @DexIgnore
    public static /* final */ int WaveView_wave_period; // = 2;
    @DexIgnore
    public static /* final */ int WaveView_wave_period_change; // = 3;
    @DexIgnore
    public static /* final */ int WaveView_wave_period_delay; // = 4;
    @DexIgnore
    public static /* final */ int WaveView_wave_radius_end; // = 5;
    @DexIgnore
    public static /* final */ int WaveView_wave_radius_start; // = 6;
    @DexIgnore
    public static /* final */ int WaveView_wave_space_wave; // = 7;
    @DexIgnore
    public static /* final */ int WaveView_wave_stroke_width; // = 8;
    @DexIgnore
    public static /* final */ int[] WeekHeartRateChart; // = {2130968621, 2130968622, 2130968623, 2130968624, 2130968625, 2130968626, 2130968627, 2130968628, 2130968629, 2130968630, 2130968631, 2130968632, 2130968633};
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AboveResting; // = 0;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AverageResting; // = 1;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AxesColor; // = 2;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AxesFontSize; // = 3;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AxesTextColor; // = 4;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_AxesTextFont; // = 5;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_CircleOutnonBrandLineColor; // = 6;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_CircleRadius; // = 7;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_HeartRateTextColor; // = 8;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_HeartRateTextFont; // = 9;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_TimeFontSize; // = 10;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_TimeTextColor; // = 11;
    @DexIgnore
    public static /* final */ int WeekHeartRateChart_WHR_TimeTextFont; // = 12;
    @DexIgnore
    public static /* final */ int[] WidgetControl; // = {2130970136, 2130970137, 2130970138, 2130970139, 2130970140, 2130970141, 2130970142, 2130970143, 2130970144, 2130970145, 2130970146, 2130970147, 2130970148, 2130970149, 2130970150, 2130970151, 2130970152, 2130970153, 2130970154, 2130970155, 2130970156, 2130970157, 2130970158, 2130970159};
    @DexIgnore
    public static /* final */ int WidgetControl_widget_background; // = 0;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_background_color; // = 1;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_background_selected; // = 2;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_background_selected_color; // = 3;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content; // = 4;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_color; // = 5;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_color_selected; // = 6;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_remove; // = 7;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_size; // = 8;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_bottom_content_size_selected; // = 9;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_default_color; // = 10;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_default_color_selected; // = 11;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_default_content_size; // = 12;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_default_content_size_selected; // = 13;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_remove_mode; // = 14;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_selected; // = 15;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content; // = 16;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content_color; // = 17;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content_color_selected; // = 18;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content_size; // = 19;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_content_size_selected; // = 20;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_icon_src; // = 21;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_icon_src_remove; // = 22;
    @DexIgnore
    public static /* final */ int WidgetControl_widget_top_icon_src_selected; // = 23;
    @DexIgnore
    public static /* final */ int[] com_facebook_like_view; // = {2130969026, 2130969028, 2130969029, 2130969033, 2130969034, 2130969036};
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_auxiliary_view_position; // = 0;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_foreground_color; // = 1;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_horizontal_alignment; // = 2;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_object_id; // = 3;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_object_type; // = 4;
    @DexIgnore
    public static /* final */ int com_facebook_like_view_com_facebook_style; // = 5;
    @DexIgnore
    public static /* final */ int[] com_facebook_login_view; // = {2130969027, 2130969031, 2130969032, 2130969037};
    @DexIgnore
    public static /* final */ int com_facebook_login_view_com_facebook_confirm_logout; // = 0;
    @DexIgnore
    public static /* final */ int com_facebook_login_view_com_facebook_login_text; // = 1;
    @DexIgnore
    public static /* final */ int com_facebook_login_view_com_facebook_logout_text; // = 2;
    @DexIgnore
    public static /* final */ int com_facebook_login_view_com_facebook_tooltip_mode; // = 3;
    @DexIgnore
    public static /* final */ int[] com_facebook_profile_picture_view; // = {2130969030, 2130969035};
    @DexIgnore
    public static /* final */ int com_facebook_profile_picture_view_com_facebook_is_cropped; // = 0;
    @DexIgnore
    public static /* final */ int com_facebook_profile_picture_view_com_facebook_preset_size; // = 1;
}
