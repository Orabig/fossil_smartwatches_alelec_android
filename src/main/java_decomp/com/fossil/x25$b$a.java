package com.fossil;

import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$1", f = "NotificationHybridContactPresenter.kt", l = {}, m = "invokeSuspend")
public final class x25$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    public x25$b$a(xe6 xe6) {
        super(2, xe6);
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        x25$b$a x25_b_a = new x25$b$a(xe6);
        x25_b_a.p$ = (il6) obj;
        return x25_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((x25$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            PortfolioApp.get.instance().M();
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
