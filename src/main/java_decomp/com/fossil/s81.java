package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s81 {
    @DexIgnore
    public static /* final */ v61 a; // = new v61();
    @DexIgnore
    public static BluetoothProfile b;
    @DexIgnore
    public static /* final */ BroadcastReceiver c; // = new y41();
    @DexIgnore
    public static /* final */ s81 d; // = new s81();

    @DexIgnore
    public final String a(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? "UNKNOWN" : "DISCONNECTING" : "CONNECTED" : "CONNECTING" : "DISCONNECTED";
    }

    @DexIgnore
    public final void a(BluetoothDevice bluetoothDevice, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        oa1 oa1 = oa1.a;
        Object[] objArr = {bluetoothDevice.getAddress(), a(i3), Integer.valueOf(i), a(i4), Integer.valueOf(i2)};
        qs0 qs0 = qs0.h;
        String a2 = cw0.a((Enum<?>) c31.HID_STATE_CHANGED);
        og0 og0 = og0.DEVICE_EVENT;
        String address = bluetoothDevice.getAddress();
        String str = address != null ? address : "";
        JSONObject jSONObject = new JSONObject();
        bm0 bm0 = bm0.MAC_ADDRESS;
        String address2 = bluetoothDevice.getAddress();
        if (address2 == null) {
            address2 = "";
        }
        JSONObject a3 = cw0.a(cw0.a(cw0.a(jSONObject, bm0, (Object) address2), bm0.PREV_STATE, (Object) a(i3)), bm0.NEW_STATE, (Object) a(i4));
        nn0 nn0 = r4;
        nn0 nn02 = new nn0(a2, og0, str, "", "", true, (String) null, (r40) null, (bw0) null, a3, 448);
        qs0.a(nn0);
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.BLUETOOTH_DEVICE", bluetoothDevice);
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.PREVIOUS_STATE", i3);
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.NEW_STATE", i4);
        Context a4 = gk0.f.a();
        if (a4 != null) {
            ce.a(a4).a(intent);
        }
    }

    @DexIgnore
    public final int b(BluetoothDevice bluetoothDevice) {
        BluetoothProfile bluetoothProfile = b;
        if (bluetoothProfile == null) {
            return ao0.HID_PROXY_NOT_CONNECTED.a;
        }
        try {
            Method method = bluetoothProfile.getClass().getMethod("disconnect", new Class[]{BluetoothDevice.class});
            if (method == null) {
                oa1 oa1 = oa1.a;
                return ao0.HID_UNKNOWN_ERROR.a;
            }
            Object invoke = method.invoke(bluetoothProfile, new Object[]{bluetoothDevice});
            if (invoke != null) {
                boolean booleanValue = ((Boolean) invoke).booleanValue();
                oa1 oa12 = oa1.a;
                new Object[1][0] = Boolean.valueOf(booleanValue);
                if (booleanValue) {
                    return ao0.SUCCESS.a;
                }
                return ao0.HID_UNKNOWN_ERROR.a;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Boolean");
        } catch (NoSuchMethodException e) {
            oa1 oa13 = oa1.a;
            StringBuilder b2 = ze0.b("disconnectDevice got exception: ");
            String localizedMessage = e.getLocalizedMessage();
            if (localizedMessage == null) {
                localizedMessage = e.toString();
            }
            b2.append(localizedMessage);
            b2.toString();
            qs0.h.a(e);
            return ao0.HID_FAIL_TO_INVOKE_PRIVATE_METHOD.a;
        } catch (Exception e2) {
            oa1 oa14 = oa1.a;
            StringBuilder b3 = ze0.b("disconnectDevice got exception: ");
            String localizedMessage2 = e2.getLocalizedMessage();
            if (localizedMessage2 == null) {
                localizedMessage2 = e2.toString();
            }
            b3.append(localizedMessage2);
            b3.toString();
            qs0.h.a(e2);
            return ao0.HID_UNKNOWN_ERROR.a;
        }
    }

    @DexIgnore
    public final int c(BluetoothDevice bluetoothDevice) {
        BluetoothProfile bluetoothProfile = b;
        if (bluetoothProfile != null) {
            return bluetoothProfile.getConnectionState(bluetoothDevice);
        }
        return 0;
    }

    @DexIgnore
    public final int a(BluetoothProfile bluetoothProfile, BluetoothDevice bluetoothDevice) throws Exception {
        Method method = bluetoothProfile.getClass().getMethod("getPriority", new Class[]{BluetoothDevice.class});
        if (method == null) {
            oa1 oa1 = oa1.a;
            return -1;
        }
        Object invoke = method.invoke(bluetoothProfile, new Object[]{bluetoothDevice});
        if (invoke != null) {
            int intValue = ((Integer) invoke).intValue();
            oa1 oa12 = oa1.a;
            Object[] objArr = {bluetoothDevice.getAddress(), Integer.valueOf(intValue)};
            return intValue;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Int");
    }

    @DexIgnore
    public final void a(Context context) {
        int profileConnectionState = BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(4);
        boolean profileProxy = BluetoothAdapter.getDefaultAdapter().getProfileProxy(context, a, 4);
        oa1 oa1 = oa1.a;
        Object[] objArr = {Integer.valueOf(profileConnectionState), Boolean.valueOf(profileProxy)};
        context.registerReceiver(c, new IntentFilter("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"));
    }

    @DexIgnore
    public final int a(BluetoothDevice bluetoothDevice) {
        BluetoothProfile bluetoothProfile = b;
        if (bluetoothProfile == null) {
            return ao0.HID_PROXY_NOT_CONNECTED.a;
        }
        try {
            int a2 = d.a(bluetoothProfile, bluetoothDevice);
            oa1 oa1 = oa1.a;
            Object[] objArr = {bluetoothDevice.getAddress(), Integer.valueOf(a2)};
            if (a2 == 0) {
                return ao0.HID_INPUT_DEVICE_DISABLED.a;
            }
            Method method = bluetoothProfile.getClass().getMethod("connect", new Class[]{BluetoothDevice.class});
            if (method == null) {
                oa1 oa12 = oa1.a;
                return ao0.HID_FAIL_TO_INVOKE_PRIVATE_METHOD.a;
            }
            Object invoke = method.invoke(bluetoothProfile, new Object[]{bluetoothDevice});
            if (invoke != null) {
                boolean booleanValue = ((Boolean) invoke).booleanValue();
                oa1 oa13 = oa1.a;
                new Object[1][0] = Boolean.valueOf(booleanValue);
                if (booleanValue) {
                    return ao0.SUCCESS.a;
                }
                return ao0.HID_UNKNOWN_ERROR.a;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Boolean");
        } catch (NoSuchMethodException e) {
            oa1 oa14 = oa1.a;
            StringBuilder b2 = ze0.b("connectDevice got exception: ");
            String localizedMessage = e.getLocalizedMessage();
            if (localizedMessage == null) {
                localizedMessage = e.toString();
            }
            b2.append(localizedMessage);
            b2.toString();
            qs0.h.a(e);
            return ao0.HID_FAIL_TO_INVOKE_PRIVATE_METHOD.a;
        } catch (Exception e2) {
            oa1 oa15 = oa1.a;
            StringBuilder b3 = ze0.b("connectDevice got exception: ");
            String localizedMessage2 = e2.getLocalizedMessage();
            if (localizedMessage2 == null) {
                localizedMessage2 = e2.toString();
            }
            b3.append(localizedMessage2);
            b3.toString();
            e2.printStackTrace();
            qs0.h.a(e2);
            return ao0.HID_UNKNOWN_ERROR.a;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r0.getConnectedDevices();
     */
    @DexIgnore
    public final List<BluetoothDevice> a() {
        List<BluetoothDevice> connectedDevices;
        BluetoothProfile bluetoothProfile = b;
        return (bluetoothProfile == null || connectedDevices == null) ? new ArrayList() : connectedDevices;
    }
}
