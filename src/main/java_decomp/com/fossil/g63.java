package com.fossil;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g63 implements Callable<List<ab3>> {
    @DexIgnore
    public /* final */ /* synthetic */ ra3 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ d63 d;

    @DexIgnore
    public g63(d63 d63, ra3 ra3, String str, String str2) {
        this.d = d63;
        this.a = ra3;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        this.d.a.t();
        return this.d.a.l().b(this.a.a, this.b, this.c);
    }
}
