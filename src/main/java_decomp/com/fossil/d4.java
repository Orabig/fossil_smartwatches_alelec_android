package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d4 {
    @DexIgnore
    public static /* final */ int cardBackgroundColor; // = 2130968927;
    @DexIgnore
    public static /* final */ int cardCornerRadius; // = 2130968928;
    @DexIgnore
    public static /* final */ int cardElevation; // = 2130968929;
    @DexIgnore
    public static /* final */ int cardMaxElevation; // = 2130968931;
    @DexIgnore
    public static /* final */ int cardPreventCornerOverlap; // = 2130968932;
    @DexIgnore
    public static /* final */ int cardUseCompatPadding; // = 2130968933;
    @DexIgnore
    public static /* final */ int cardViewStyle; // = 2130968934;
    @DexIgnore
    public static /* final */ int contentPadding; // = 2130969060;
    @DexIgnore
    public static /* final */ int contentPaddingBottom; // = 2130969061;
    @DexIgnore
    public static /* final */ int contentPaddingLeft; // = 2130969062;
    @DexIgnore
    public static /* final */ int contentPaddingRight; // = 2130969063;
    @DexIgnore
    public static /* final */ int contentPaddingTop; // = 2130969064;
}
