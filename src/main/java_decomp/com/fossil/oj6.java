package com.fossil;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oj6 {
    @DexIgnore
    public static final kj6 b(Matcher matcher, int i, CharSequence charSequence) {
        if (!matcher.find(i)) {
            return null;
        }
        return new lj6(matcher, charSequence);
    }

    @DexIgnore
    public static final kj6 b(Matcher matcher, CharSequence charSequence) {
        if (!matcher.matches()) {
            return null;
        }
        return new lj6(matcher, charSequence);
    }

    @DexIgnore
    public static final wh6 b(MatchResult matchResult) {
        return ci6.d(matchResult.start(), matchResult.end());
    }

    @DexIgnore
    public static final wh6 b(MatchResult matchResult, int i) {
        return ci6.d(matchResult.start(i), matchResult.end(i));
    }

    @DexIgnore
    public static final int b(Iterable<? extends gj6> iterable) {
        int i = 0;
        for (gj6 value : iterable) {
            i |= value.getValue();
        }
        return i;
    }
}
