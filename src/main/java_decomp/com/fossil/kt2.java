package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kt2 implements ht2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;
    @DexIgnore
    public static /* final */ pk2<Boolean> b;
    @DexIgnore
    public static /* final */ pk2<Boolean> c;
    @DexIgnore
    public static /* final */ pk2<Boolean> d;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.sdk.collection.last_deep_link_referrer", false);
        b = vk2.a("measurement.sdk.collection.last_deep_link_referrer_campaign", false);
        c = vk2.a("measurement.sdk.collection.last_gclid_from_referrer", false);
        d = vk2.a("measurement.sdk.collection.worker_thread_referrer", true);
    }
    */

    @DexIgnore
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    public final boolean zzb() {
        return a.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzc() {
        return b.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzd() {
        return c.b().booleanValue();
    }

    @DexIgnore
    public final boolean zze() {
        return d.b().booleanValue();
    }
}
