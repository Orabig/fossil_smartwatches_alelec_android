package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xt0 {
    @DexIgnore
    public /* final */ /* synthetic */ qv0 a;

    @DexIgnore
    public xt0(qv0 qv0) {
        this.a = qv0;
    }

    @DexIgnore
    public void a(we1 we1) {
        if (we1 instanceof sg1) {
            sg1 sg1 = (sg1) we1;
            if (this.a.a(sg1) > 0) {
                qv0 qv0 = this.a;
                qv0.a(qv0.a(sg1));
                qv0 qv02 = this.a;
                qv02.a(qv02.p);
                qv0 qv03 = this.a;
                qv03.g.add(new ne0(0, sg1.a, sg1.b, cw0.a(new JSONObject(), bm0.PROPOSED_TIMEOUT, (Object) Long.valueOf(this.a.e())), 1));
                return;
            }
        }
        this.a.a(we1);
    }
}
