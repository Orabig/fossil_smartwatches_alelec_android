package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1$2", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
public final class u65$e$a$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CustomizeThemePresenter.e.a this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u65$e$a$a(CustomizeThemePresenter.e.a aVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = aVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        u65$e$a$a u65_e_a_a = new u65$e$a$a(this.this$0, xe6);
        u65_e_a_a.p$ = (il6) obj;
        return u65_e_a_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((u65$e$a$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.this$0.n.b(this.this$0.this$0.$watchFaceWrapper);
            this.this$0.this$0.this$0.n.a();
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
