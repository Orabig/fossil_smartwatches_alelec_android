package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1", f = "HomeAlertsHybridPresenter.kt", l = {61}, m = "invokeSuspend")
public final class h15$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceId;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeAlertsHybridPresenter.c this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1$allAlarms$1", f = "HomeAlertsHybridPresenter.kt", l = {62}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super List<Alarm>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h15$c$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(h15$c$a h15_c_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = h15_c_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                AlarmHelper b = this.this$0.this$0.a.j;
                this.L$0 = il6;
                this.label = 1;
                if (b.a((xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!this.this$0.this$0.a.h) {
                this.this$0.this$0.a.h = true;
                AlarmHelper b2 = this.this$0.this$0.a.j;
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                b2.d(applicationContext);
            }
            return this.this$0.this$0.a.l.getAllAlarmIgnoreDeletePinType();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Integer.valueOf(((Alarm) t).getTotalMinutes()), Integer.valueOf(((Alarm) t2).getTotalMinutes()));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h15$c$a(HomeAlertsHybridPresenter.c cVar, String str, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
        this.$deviceId = str;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        h15$c$a h15_c_a = new h15$c$a(this.this$0, this.$deviceId, xe6);
        h15_c_a.p$ = (il6) obj;
        return h15_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((h15$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            boolean b2 = DeviceHelper.o.e().b(this.$deviceId);
            this.this$0.a.i.r(b2);
            if (b2) {
                dl6 a3 = this.this$0.a.c();
                a aVar = new a(this, (xe6) null);
                this.L$0 = il6;
                this.Z$0 = b2;
                this.label = 1;
                obj2 = gk6.a(a3, aVar, this);
                if (obj2 == a2) {
                    return a2;
                }
            }
            HomeAlertsHybridPresenter homeAlertsHybridPresenter = this.this$0.a;
            homeAlertsHybridPresenter.g = homeAlertsHybridPresenter.m.E();
            this.this$0.a.i.h(this.this$0.a.g);
            this.this$0.a.i.n(this.this$0.a.g);
            return cd6.a;
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            obj2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        List<Alarm> list = (List) obj2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a4 = HomeAlertsHybridPresenter.o.a();
        local.d(a4, "GetAlarms onSuccess: size = " + list.size());
        this.this$0.a.f.clear();
        for (Alarm copy$default : list) {
            this.this$0.a.f.add(Alarm.copy$default(copy$default, (String) null, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 4095, (Object) null));
        }
        ArrayList d = this.this$0.a.f;
        if (d.size() > 1) {
            ud6.a(d, new b());
        }
        this.this$0.a.i.c(this.this$0.a.f);
        HomeAlertsHybridPresenter homeAlertsHybridPresenter2 = this.this$0.a;
        homeAlertsHybridPresenter2.g = homeAlertsHybridPresenter2.m.E();
        this.this$0.a.i.h(this.this$0.a.g);
        this.this$0.a.i.n(this.this$0.a.g);
        return cd6.a;
    }
}
