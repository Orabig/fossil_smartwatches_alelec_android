package com.fossil;

import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it5 implements Factory<ArrayList<Integer>> {
    @DexIgnore
    public static ArrayList<Integer> a(ht5 ht5) {
        ArrayList<Integer> a = ht5.a();
        z76.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
