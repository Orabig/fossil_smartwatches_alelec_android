package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ConnectedAppsFragmentMapping extends ConnectedAppsFragmentBinding {
    @DexIgnore
    public static /* final */ ViewDataBinding.j u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v; // = new SparseIntArray();
    @DexIgnore
    public long t;

    /*
    static {
        v.put(2131362542, 1);
        v.put(2131363218, 2);
        v.put(2131362083, 3);
        v.put(2131362634, 4);
        v.put(2131363222, 5);
        v.put(2131361983, 6);
        v.put(2131362040, 7);
        v.put(2131362604, 8);
        v.put(2131363161, 9);
        v.put(2131361978, 10);
        v.put(2131362037, 11);
        v.put(2131362595, 12);
        v.put(2131363156, 13);
        v.put(2131361977, 14);
    }
    */

    @DexIgnore
    public ConnectedAppsFragmentMapping(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 15, u, v));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public ConnectedAppsFragmentMapping(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[14], objArr[10], objArr[6], objArr[11], objArr[7], objArr[3], objArr[1], objArr[12], objArr[8], objArr[4], objArr[0], objArr[13], objArr[9], objArr[2], objArr[5]);
        this.t = -1;
        this.s.setTag((Object) null);
        a(view);
        f();
    }
}
