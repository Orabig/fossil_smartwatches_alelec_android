package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o81 extends vv0 {
    @DexIgnore
    public rg1 J;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public /* final */ long L;
    @DexIgnore
    public /* final */ long M;
    @DexIgnore
    public /* final */ long N;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ o81(long j, long j2, long j3, short s, ue1 ue1, int i, int i2) {
        super(du0.LEGACY_PUT_FILE, s, lx0.LEGACY_PUT_FILE, ue1, (i2 & 32) != 0 ? 3 : i);
        this.L = j;
        this.M = j2;
        this.N = j3;
        this.J = rg1.UNKNOWN;
        this.K = true;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        this.J = rg1.FTD;
        return cw0.a(jSONObject, bm0.SOCKET_ID, (Object) this.J.a);
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(cw0.a(super.h(), bm0.OFFSET, (Object) Long.valueOf(this.L)), bm0.LENGTH, (Object) Long.valueOf(this.M)), bm0.TOTAL_LENGTH, (Object) Long.valueOf(this.N));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.SOCKET_ID, (Object) this.J.a);
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.L).putInt((int) this.M).putInt((int) this.N).array();
        wg6.a(array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean q() {
        return this.K;
    }
}
