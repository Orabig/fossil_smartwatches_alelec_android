package com.fossil;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class t94 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FrameLayout A;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleButton w;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout x;
    @DexIgnore
    public /* final */ RTLImageView y;
    @DexIgnore
    public /* final */ RTLImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t94(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, View view2, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, FlexibleButton flexibleButton3, FlexibleTextView flexibleTextView2, FlexibleButton flexibleButton4, FlexibleTextView flexibleTextView3, FlexibleTextInputLayout flexibleTextInputLayout, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, FrameLayout frameLayout, ScrollView scrollView, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = flexibleTextInputEditText;
        this.t = flexibleButton;
        this.u = flexibleButton2;
        this.v = flexibleButton3;
        this.w = flexibleButton4;
        this.x = flexibleTextInputLayout;
        this.y = rTLImageView;
        this.z = rTLImageView2;
        this.A = frameLayout;
    }
}
