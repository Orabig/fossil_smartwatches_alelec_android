package com.fossil;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Pair;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.stetho.server.http.HttpStatus;
import com.fossil.fn2;
import com.fossil.mj2;
import com.fossil.oj2;
import com.fossil.pj2;
import com.fossil.qj2;
import com.fossil.uj2;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ea3 implements v63 {
    @DexIgnore
    public static volatile ea3 y;
    @DexIgnore
    public s53 a;
    @DexIgnore
    public x43 b;
    @DexIgnore
    public yz2 c;
    @DexIgnore
    public f53 d;
    @DexIgnore
    public z93 e;
    @DexIgnore
    public sa3 f;
    @DexIgnore
    public /* final */ ia3 g;
    @DexIgnore
    public f83 h;
    @DexIgnore
    public /* final */ x53 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public long m;
    @DexIgnore
    public List<Runnable> n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public FileLock t;
    @DexIgnore
    public FileChannel u;
    @DexIgnore
    public List<Long> v;
    @DexIgnore
    public List<Long> w;
    @DexIgnore
    public long x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements a03 {
        @DexIgnore
        public qj2 a;
        @DexIgnore
        public List<Long> b;
        @DexIgnore
        public List<mj2> c;
        @DexIgnore
        public long d;

        @DexIgnore
        public a(ea3 ea3) {
        }

        @DexIgnore
        public final void a(qj2 qj2) {
            w12.a(qj2);
            this.a = qj2;
        }

        @DexIgnore
        public /* synthetic */ a(ea3 ea3, da3 da3) {
            this(ea3);
        }

        @DexIgnore
        public final boolean a(long j, mj2 mj2) {
            w12.a(mj2);
            if (this.c == null) {
                this.c = new ArrayList();
            }
            if (this.b == null) {
                this.b = new ArrayList();
            }
            if (this.c.size() > 0 && a(this.c.get(0)) != a(mj2)) {
                return false;
            }
            long e = this.d + ((long) mj2.e());
            if (e >= ((long) Math.max(0, l03.n.a(null).intValue()))) {
                return false;
            }
            this.d = e;
            this.c.add(mj2);
            this.b.add(Long.valueOf(j));
            if (this.c.size() >= Math.max(1, l03.o.a(null).intValue())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public static long a(mj2 mj2) {
            return ((mj2.r() / 1000) / 60) / 60;
        }
    }

    @DexIgnore
    public ea3(ja3 ja3) {
        this(ja3, (x53) null);
    }

    @DexIgnore
    public static ea3 a(Context context) {
        w12.a(context);
        w12.a(context.getApplicationContext());
        if (y == null) {
            synchronized (ea3.class) {
                if (y == null) {
                    y = new ea3(new ja3(context));
                }
            }
        }
        return y;
    }

    @DexIgnore
    public final boolean A() {
        y();
        r();
        return l().F() || !TextUtils.isEmpty(l().w());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01c0  */
    public final void B() {
        long j2;
        long j3;
        y();
        r();
        if (h() || this.i.p().a(l03.l0)) {
            if (this.m > 0) {
                long abs = 3600000 - Math.abs(this.i.zzm().c() - this.m);
                if (abs > 0) {
                    this.i.b().B().a("Upload has been suspended. Will update scheduling later in approximately ms", Long.valueOf(abs));
                    w().b();
                    x().u();
                    return;
                }
                this.m = 0;
            }
            if (!this.i.l() || !A()) {
                this.i.b().B().a("Nothing to upload or uploading impossible");
                w().b();
                x().u();
                return;
            }
            long b2 = this.i.zzm().b();
            long max = Math.max(0, l03.F.a(null).longValue());
            boolean z = l().G() || l().B();
            if (z) {
                String s2 = this.i.p().s();
                if (TextUtils.isEmpty(s2) || ".none.".equals(s2)) {
                    j2 = Math.max(0, l03.z.a(null).longValue());
                } else {
                    j2 = Math.max(0, l03.A.a(null).longValue());
                }
            } else {
                j2 = Math.max(0, l03.y.a(null).longValue());
            }
            long a2 = this.i.q().e.a();
            long a3 = this.i.q().f.a();
            long j4 = j2;
            long j5 = max;
            long max2 = Math.max(l().D(), l().E());
            if (max2 != 0) {
                long abs2 = b2 - Math.abs(max2 - b2);
                long abs3 = b2 - Math.abs(a2 - b2);
                long abs4 = b2 - Math.abs(a3 - b2);
                long max3 = Math.max(abs3, abs4);
                long j6 = abs2 + j5;
                if (z && max3 > 0) {
                    j6 = Math.min(abs2, max3) + j4;
                }
                long j7 = j4;
                j3 = !o().a(max3, j7) ? max3 + j7 : j6;
                if (abs4 != 0 && abs4 >= abs2) {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= Math.min(20, Math.max(0, l03.H.a(null).intValue()))) {
                            break;
                        }
                        j3 += Math.max(0, l03.G.a(null).longValue()) * (1 << i2);
                        if (j3 > abs4) {
                            break;
                        }
                        i2++;
                    }
                }
                if (j3 != 0) {
                    this.i.b().B().a("Next upload time is 0");
                    w().b();
                    x().u();
                    return;
                } else if (!k().u()) {
                    this.i.b().B().a("No network");
                    w().a();
                    x().u();
                    return;
                } else {
                    long a4 = this.i.q().g.a();
                    long max4 = Math.max(0, l03.w.a(null).longValue());
                    if (!o().a(a4, max4)) {
                        j3 = Math.max(j3, a4 + max4);
                    }
                    w().b();
                    long b3 = j3 - this.i.zzm().b();
                    if (b3 <= 0) {
                        b3 = Math.max(0, l03.B.a(null).longValue());
                        this.i.q().e.a(this.i.zzm().b());
                    }
                    this.i.b().B().a("Upload scheduled in approximately ms", Long.valueOf(b3));
                    x().a(b3);
                    return;
                }
            }
            j3 = 0;
            if (j3 != 0) {
            }
        }
    }

    @DexIgnore
    public final t43 b() {
        return this.i.b();
    }

    @DexIgnore
    public final Context c() {
        return this.i.c();
    }

    @DexIgnore
    public final bb3 d() {
        return this.i.d();
    }

    @DexIgnore
    public final void e() {
        this.i.a().g();
        l().C();
        if (this.i.q().e.a() == 0) {
            this.i.q().e.a(this.i.zzm().b());
        }
        B();
    }

    @DexIgnore
    public final void f() {
        y();
        if (this.q || this.r || this.s) {
            this.i.b().B().a("Not stopping services. fetch, network, upload", Boolean.valueOf(this.q), Boolean.valueOf(this.r), Boolean.valueOf(this.s));
            return;
        }
        this.i.b().B().a("Stopping uploading service(s)");
        List<Runnable> list = this.n;
        if (list != null) {
            for (Runnable run : list) {
                run.run();
            }
            this.n.clear();
        }
    }

    @DexIgnore
    public final boolean g() {
        FileLock fileLock;
        y();
        if (!this.i.p().a(l03.I0) || (fileLock = this.t) == null || !fileLock.isValid()) {
            try {
                this.u = new RandomAccessFile(new File(this.i.c().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
                this.t = this.u.tryLock();
                if (this.t != null) {
                    this.i.b().B().a("Storage concurrent access okay");
                    return true;
                }
                this.i.b().t().a("Storage concurrent data access panic");
                return false;
            } catch (FileNotFoundException e2) {
                this.i.b().t().a("Failed to acquire storage lock", e2);
                return false;
            } catch (IOException e3) {
                this.i.b().t().a("Failed to access storage lock file", e3);
                return false;
            } catch (OverlappingFileLockException e4) {
                this.i.b().w().a("Storage lock already acquired", e4);
                return false;
            }
        } else {
            this.i.b().B().a("Storage concurrent access okay");
            return true;
        }
    }

    @DexIgnore
    public final boolean h() {
        y();
        r();
        return this.k;
    }

    @DexIgnore
    public final cb3 i() {
        return this.i.p();
    }

    @DexIgnore
    public final s53 j() {
        b((aa3) this.a);
        return this.a;
    }

    @DexIgnore
    public final x43 k() {
        b((aa3) this.b);
        return this.b;
    }

    @DexIgnore
    public final yz2 l() {
        b((aa3) this.c);
        return this.c;
    }

    @DexIgnore
    public final sa3 m() {
        b((aa3) this.f);
        return this.f;
    }

    @DexIgnore
    public final f83 n() {
        b((aa3) this.h);
        return this.h;
    }

    @DexIgnore
    public final ia3 o() {
        b((aa3) this.g);
        return this.g;
    }

    @DexIgnore
    public final r43 p() {
        return this.i.x();
    }

    @DexIgnore
    public final ma3 q() {
        return this.i.w();
    }

    @DexIgnore
    public final void r() {
        if (!this.j) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:95|96) */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        r1.i.b().t().a("Failed to parse upload URL. Not uploading. appId", com.fossil.t43.a(r5), r9);
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:95:0x02c2 */
    public final void s() {
        a63 b2;
        String str;
        y();
        r();
        this.s = true;
        try {
            this.i.d();
            Boolean F = this.i.F().F();
            if (F == null) {
                this.i.b().w().a("Upload data called on the client side before use of service was decided");
            } else if (F.booleanValue()) {
                this.i.b().t().a("Upload called in the client side when service should be used");
                this.s = false;
                f();
            } else if (this.m > 0) {
                B();
                this.s = false;
                f();
            } else {
                y();
                if (this.v != null) {
                    this.i.b().B().a("Uploading requested multiple times");
                    this.s = false;
                    f();
                } else if (!k().u()) {
                    this.i.b().B().a("Network not connected, ignoring upload request");
                    B();
                    this.s = false;
                    f();
                } else {
                    long b3 = this.i.zzm().b();
                    a((String) null, b3 - cb3.x());
                    long a2 = this.i.q().e.a();
                    if (a2 != 0) {
                        this.i.b().A().a("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(b3 - a2)));
                    }
                    String w2 = l().w();
                    if (!TextUtils.isEmpty(w2)) {
                        if (this.x == -1) {
                            this.x = l().x();
                        }
                        List<Pair<qj2, Long>> a3 = l().a(w2, this.i.p().b(w2, l03.l), Math.max(0, this.i.p().b(w2, l03.m)));
                        if (!a3.isEmpty()) {
                            Iterator<Pair<qj2, Long>> it = a3.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    str = null;
                                    break;
                                }
                                qj2 qj2 = (qj2) it.next().first;
                                if (!TextUtils.isEmpty(qj2.r())) {
                                    str = qj2.r();
                                    break;
                                }
                            }
                            if (str != null) {
                                int i2 = 0;
                                while (true) {
                                    if (i2 >= a3.size()) {
                                        break;
                                    }
                                    qj2 qj22 = (qj2) a3.get(i2).first;
                                    if (!TextUtils.isEmpty(qj22.r()) && !qj22.r().equals(str)) {
                                        a3 = a3.subList(0, i2);
                                        break;
                                    }
                                    i2++;
                                }
                            }
                            pj2.a o2 = pj2.o();
                            int size = a3.size();
                            ArrayList arrayList = new ArrayList(a3.size());
                            boolean z = cb3.y() && this.i.p().d(w2);
                            for (int i3 = 0; i3 < size; i3++) {
                                qj2.a aVar = (qj2.a) ((qj2) a3.get(i3).first).j();
                                arrayList.add((Long) a3.get(i3).second);
                                aVar.g(this.i.p().n());
                                aVar.a(b3);
                                this.i.d();
                                aVar.b(false);
                                if (!z) {
                                    aVar.x();
                                }
                                if (this.i.p().e(w2, l03.o0)) {
                                    aVar.l(o().a(((qj2) ((fn2) aVar.i())).f()));
                                }
                                o2.a(aVar);
                            }
                            String a4 = this.i.b().a(2) ? o().a((pj2) ((fn2) o2.i())) : null;
                            o();
                            byte[] f2 = ((pj2) ((fn2) o2.i())).f();
                            String a5 = l03.v.a(null);
                            URL url = new URL(a5);
                            w12.a(!arrayList.isEmpty());
                            if (this.v != null) {
                                this.i.b().t().a("Set uploading progress before finishing the previous upload");
                            } else {
                                this.v = new ArrayList(arrayList);
                            }
                            this.i.q().f.a(b3);
                            String str2 = "?";
                            if (size > 0) {
                                str2 = o2.a(0).w0();
                            }
                            this.i.b().B().a("Uploading data. app, uncompressed size, data", str2, Integer.valueOf(f2.length), a4);
                            this.r = true;
                            x43 k2 = k();
                            ga3 ga3 = new ga3(this, w2);
                            k2.g();
                            k2.r();
                            w12.a(url);
                            w12.a(f2);
                            w12.a(ga3);
                            k2.a().b((Runnable) new c53(k2, w2, url, f2, (Map<String, String>) null, ga3));
                        }
                    } else {
                        this.x = -1;
                        String a6 = l().a(b3 - cb3.x());
                        if (!TextUtils.isEmpty(a6) && (b2 = l().b(a6)) != null) {
                            a(b2);
                        }
                    }
                    this.s = false;
                    f();
                }
            }
        } finally {
            this.s = false;
            f();
        }
    }

    @DexIgnore
    public final void t() {
        y();
        r();
        if (!this.l) {
            this.l = true;
            y();
            r();
            if ((this.i.p().a(l03.l0) || h()) && g()) {
                int a2 = a(this.u);
                int D = this.i.H().D();
                y();
                if (a2 > D) {
                    this.i.b().t().a("Panic: can't downgrade version. Previous, current version", Integer.valueOf(a2), Integer.valueOf(D));
                } else if (a2 < D) {
                    if (a(D, this.u)) {
                        this.i.b().B().a("Storage version upgraded. Previous, current version", Integer.valueOf(a2), Integer.valueOf(D));
                    } else {
                        this.i.b().t().a("Storage version upgrade failed. Previous, current version", Integer.valueOf(a2), Integer.valueOf(D));
                    }
                }
            }
        }
        if (!this.k && !this.i.p().a(l03.l0)) {
            this.i.b().z().a("This instance being marked as an uploader");
            this.k = true;
            B();
        }
    }

    @DexIgnore
    public final void u() {
        this.p++;
    }

    @DexIgnore
    public final x53 v() {
        return this.i;
    }

    @DexIgnore
    public final f53 w() {
        f53 f53 = this.d;
        if (f53 != null) {
            return f53;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    @DexIgnore
    public final z93 x() {
        b((aa3) this.e);
        return this.e;
    }

    @DexIgnore
    public final void y() {
        this.i.a().g();
    }

    @DexIgnore
    public final long z() {
        long b2 = this.i.zzm().b();
        h53 q2 = this.i.q();
        q2.n();
        q2.g();
        long a2 = q2.i.a();
        if (a2 == 0) {
            a2 = 1 + ((long) q2.j().t().nextInt(86400000));
            q2.i.a(a2);
        }
        return ((((b2 + a2) / 1000) / 60) / 60) / 24;
    }

    @DexIgnore
    public final k42 zzm() {
        return this.i.zzm();
    }

    @DexIgnore
    public ea3(ja3 ja3, x53 x53) {
        this.j = false;
        w12.a(ja3);
        this.i = x53.a(ja3.a, (mv2) null);
        this.x = -1;
        ia3 ia3 = new ia3(this);
        ia3.s();
        this.g = ia3;
        x43 x43 = new x43(this);
        x43.s();
        this.b = x43;
        s53 s53 = new s53(this);
        s53.s();
        this.a = s53;
        this.i.a().a((Runnable) new da3(this, ja3));
    }

    @DexIgnore
    public static void b(aa3 aa3) {
        if (aa3 == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!aa3.q()) {
            String valueOf = String.valueOf(aa3.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public final a63 c(ra3 ra3) {
        y();
        r();
        w12.a(ra3);
        w12.b(ra3.a);
        a63 b2 = l().b(ra3.a);
        String b3 = this.i.q().b(ra3.a);
        if (!ps2.a() || !l03.S0.a(null).booleanValue()) {
            return a(ra3, b2, b3);
        }
        if (b2 == null) {
            b2 = new a63(this.i, ra3.a);
            b2.a(this.i.w().v());
            b2.e(b3);
        } else if (!b3.equals(b2.q())) {
            b2.e(b3);
            b2.a(this.i.w().v());
        }
        b2.b(ra3.b);
        b2.c(ra3.v);
        if (bt2.a() && this.i.p().e(b2.l(), l03.K0)) {
            b2.d(ra3.z);
        }
        if (!TextUtils.isEmpty(ra3.o)) {
            b2.f(ra3.o);
        }
        long j2 = ra3.e;
        if (j2 != 0) {
            b2.d(j2);
        }
        if (!TextUtils.isEmpty(ra3.c)) {
            b2.g(ra3.c);
        }
        b2.c(ra3.j);
        String str = ra3.d;
        if (str != null) {
            b2.h(str);
        }
        b2.e(ra3.f);
        b2.a(ra3.h);
        if (!TextUtils.isEmpty(ra3.g)) {
            b2.i(ra3.g);
        }
        b2.p(ra3.p);
        b2.b(ra3.s);
        b2.c(ra3.t);
        if (this.i.p().e(ra3.a, l03.i0)) {
            b2.a(ra3.w);
        }
        b2.f(ra3.x);
        if (b2.a()) {
            l().a(b2);
        }
        return b2;
    }

    @DexIgnore
    public final String d(ra3 ra3) {
        try {
            return (String) this.i.a().a(new ha3(this, ra3)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
            this.i.b().t().a("Failed to get app instance id. appId", t43.a(ra3.a), e2);
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:246:0x0848 A[Catch:{ SQLiteException -> 0x0237, all -> 0x08bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:251:0x0878 A[Catch:{ SQLiteException -> 0x0237, all -> 0x08bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x026f A[Catch:{ SQLiteException -> 0x0237, all -> 0x08bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x02a6 A[Catch:{ SQLiteException -> 0x0237, all -> 0x08bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02f4 A[Catch:{ SQLiteException -> 0x0237, all -> 0x08bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0321  */
    public final void b(j03 j03, ra3 ra3) {
        long j2;
        long intValue;
        String str;
        f03 f03;
        List<Integer> u2;
        na3 c2;
        boolean z;
        long j3;
        na3 na3;
        a63 b2;
        j03 j032 = j03;
        ra3 ra32 = ra3;
        w12.a(ra3);
        w12.b(ra32.a);
        long nanoTime = System.nanoTime();
        y();
        r();
        String str2 = ra32.a;
        if (o().a(j032, ra32)) {
            if (!ra32.h) {
                c(ra32);
            } else if (j().b(str2, j032.a)) {
                this.i.b().w().a("Dropping blacklisted event. appId", t43.a(str2), this.i.x().a(j032.a));
                boolean z2 = j().g(str2) || j().h(str2);
                if (!z2 && !"_err".equals(j032.a)) {
                    this.i.w().a(str2, 11, "_ev", j032.a, 0);
                }
                if (z2 && (b2 = l().b(str2)) != null) {
                    if (Math.abs(this.i.zzm().b() - Math.max(b2.D(), b2.C())) > l03.E.a(null).longValue()) {
                        this.i.b().A().a("Fetching config for blacklisted app");
                        a(b2);
                    }
                }
            } else {
                if (this.i.b().a(2)) {
                    this.i.b().B().a("Logging event", this.i.x().a(j032));
                }
                l().z();
                c(ra32);
                if (!"_iap".equals(j032.a)) {
                    if (!"ecommerce_purchase".equals(j032.a)) {
                        j2 = nanoTime;
                        boolean e2 = ma3.e(j032.a);
                        boolean equals = "_err".equals(j032.a);
                        String str3 = str2;
                        xz2 a2 = l().a(z(), str2, true, e2, false, equals, false);
                        intValue = a2.b - ((long) l03.p.a(null).intValue());
                        if (intValue > 0) {
                            if (intValue % 1000 == 1) {
                                this.i.b().t().a("Data loss. Too many events logged. appId, count", t43.a(str3), Long.valueOf(a2.b));
                            }
                            l().u();
                            l().A();
                            return;
                        }
                        if (e2) {
                            long intValue2 = a2.a - ((long) l03.r.a(null).intValue());
                            if (intValue2 > 0) {
                                if (intValue2 % 1000 == 1) {
                                    this.i.b().t().a("Data loss. Too many public events logged. appId, count", t43.a(str3), Long.valueOf(a2.a));
                                }
                                this.i.w().a(str3, 16, "_ev", j032.a, 0);
                                l().u();
                                l().A();
                                return;
                            }
                        }
                        if (equals) {
                            long max = a2.d - ((long) Math.max(0, Math.min(1000000, this.i.p().b(ra32.a, l03.q))));
                            if (max > 0) {
                                if (max == 1) {
                                    this.i.b().t().a("Too many error events logged. appId, count", t43.a(str3), Long.valueOf(a2.d));
                                }
                                l().u();
                                l().A();
                                return;
                            }
                        }
                        Bundle zzb = j032.b.zzb();
                        this.i.w().a(zzb, "_o", (Object) j032.c);
                        String str4 = str3;
                        if (this.i.w().d(str4)) {
                            this.i.w().a(zzb, "_dbg", (Object) 1L);
                            this.i.w().a(zzb, "_r", (Object) 1L);
                        }
                        if ("_s".equals(j032.a)) {
                            if (this.i.p().m(ra32.a) && (c2 = l().c(ra32.a, "_sno")) != null && (c2.e instanceof Long)) {
                                this.i.w().a(zzb, "_sno", c2.e);
                            }
                        }
                        if (!"_s".equals(j032.a) || !this.i.p().e(ra32.a, l03.c0) || this.i.p().m(ra32.a)) {
                            str = null;
                        } else {
                            str = null;
                            b(new la3("_sno", 0, (String) null), ra32);
                        }
                        long c3 = l().c(str4);
                        if (c3 > 0) {
                            this.i.b().w().a("Data lost. Too many events stored on disk, deleted. appId", t43.a(str4), Long.valueOf(c3));
                        }
                        x53 x53 = this.i;
                        String str5 = j032.c;
                        String str6 = j032.a;
                        long j4 = j032.d;
                        String str7 = "_r";
                        String str8 = str4;
                        String str9 = str;
                        g03 g03 = new g03(x53, str5, str4, str6, j4, 0, zzb);
                        f03 a3 = l().a(str8, g03.b);
                        if (a3 != null) {
                            g03 = g03.a(this.i, a3.f);
                            f03 = a3.a(g03.d);
                        } else if (l().g(str8) < 500 || !e2) {
                            f03 = new f03(str8, g03.b, 0, 0, g03.d, 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                        } else {
                            this.i.b().t().a("Too many event names used, ignoring event. appId, name, supported count", t43.a(str8), this.i.x().a(g03.b), Integer.valueOf(HttpStatus.HTTP_INTERNAL_SERVER_ERROR));
                            this.i.w().a(str8, 8, (String) null, (String) null, 0);
                            l().A();
                            return;
                        }
                        l().a(f03);
                        y();
                        r();
                        w12.a(g03);
                        w12.a(ra3);
                        w12.b(g03.a);
                        w12.a(g03.a.equals(ra32.a));
                        qj2.a z0 = qj2.z0();
                        boolean z3 = true;
                        z0.a(1);
                        z0.a("android");
                        if (!TextUtils.isEmpty(ra32.a)) {
                            z0.f(ra32.a);
                        }
                        if (!TextUtils.isEmpty(ra32.d)) {
                            z0.e(ra32.d);
                        }
                        if (!TextUtils.isEmpty(ra32.c)) {
                            z0.g(ra32.c);
                        }
                        if (ra32.j != -2147483648L) {
                            z0.h((int) ra32.j);
                        }
                        z0.f(ra32.e);
                        if (!TextUtils.isEmpty(ra32.b)) {
                            z0.k(ra32.b);
                        }
                        if (this.i.p().a(l03.p0)) {
                            if (TextUtils.isEmpty(z0.v()) && !TextUtils.isEmpty(ra32.v)) {
                                z0.o(ra32.v);
                            }
                        } else if (!TextUtils.isEmpty(ra32.v)) {
                            z0.o(ra32.v);
                        }
                        if (ra32.f != 0) {
                            z0.h(ra32.f);
                        }
                        z0.k(ra32.x);
                        if (this.i.p().e(ra32.a, l03.m0) && (u2 = o().u()) != null) {
                            z0.c((Iterable<? extends Integer>) u2);
                        }
                        Pair<String, Boolean> a4 = this.i.q().a(ra32.a);
                        if (a4 == null || TextUtils.isEmpty((CharSequence) a4.first)) {
                            if (!this.i.G().a(this.i.c()) && ra32.t) {
                                String string = Settings.Secure.getString(this.i.c().getContentResolver(), "android_id");
                                if (string == null) {
                                    this.i.b().w().a("null secure ID. appId", t43.a(z0.s()));
                                    string = "null";
                                } else if (string.isEmpty()) {
                                    this.i.b().w().a("empty secure ID. appId", t43.a(z0.s()));
                                }
                                z0.m(string);
                            }
                        } else if (ra32.s) {
                            z0.h((String) a4.first);
                            if (a4.second != null) {
                                z0.a(((Boolean) a4.second).booleanValue());
                            }
                        }
                        this.i.G().n();
                        z0.c(Build.MODEL);
                        this.i.G().n();
                        z0.b(Build.VERSION.RELEASE);
                        z0.f((int) this.i.G().s());
                        z0.d(this.i.G().t());
                        z0.j(ra32.p);
                        if (this.i.g() && cb3.y()) {
                            z0.s();
                            if (!TextUtils.isEmpty(str9)) {
                                z0.n(str9);
                            }
                        }
                        a63 b3 = l().b(ra32.a);
                        if (b3 == null) {
                            b3 = new a63(this.i, ra32.a);
                            b3.a(this.i.w().v());
                            b3.f(ra32.o);
                            b3.b(ra32.b);
                            b3.e(this.i.q().b(ra32.a));
                            b3.g(0);
                            b3.a(0);
                            b3.b(0);
                            b3.g(ra32.c);
                            b3.c(ra32.j);
                            b3.h(ra32.d);
                            b3.d(ra32.e);
                            b3.e(ra32.f);
                            b3.a(ra32.h);
                            b3.p(ra32.p);
                            b3.f(ra32.x);
                            l().a(b3);
                        }
                        if (!TextUtils.isEmpty(b3.m())) {
                            z0.i(b3.m());
                        }
                        if (!TextUtils.isEmpty(b3.r())) {
                            z0.l(b3.r());
                        }
                        List<na3> a5 = l().a(ra32.a);
                        for (int i2 = 0; i2 < a5.size(); i2++) {
                            uj2.a A = uj2.A();
                            A.a(a5.get(i2).c);
                            A.a(a5.get(i2).d);
                            o().a(A, a5.get(i2).e);
                            z0.a(A);
                        }
                        try {
                            long a6 = l().a((qj2) ((fn2) z0.i()));
                            yz2 l2 = l();
                            if (g03.f != null) {
                                Iterator<String> it = g03.f.iterator();
                                while (true) {
                                    if (it.hasNext()) {
                                        String str10 = str7;
                                        if (str10.equals(it.next())) {
                                            break;
                                        }
                                        str7 = str10;
                                    } else {
                                        boolean c4 = j().c(g03.a, g03.b);
                                        xz2 a7 = l().a(z(), g03.a, false, false, false, false, false);
                                        if (c4 && a7.e < ((long) this.i.p().a(g03.a))) {
                                        }
                                    }
                                }
                                if (l2.a(g03, a6, z3)) {
                                    this.m = 0;
                                }
                                l().u();
                                if (this.i.b().a(2)) {
                                    this.i.b().B().a("Event recorded", this.i.x().a(g03));
                                }
                                l().A();
                                B();
                                this.i.b().B().a("Background event processing time, ms", Long.valueOf(((System.nanoTime() - j2) + 500000) / 1000000));
                                return;
                            }
                            z3 = false;
                            if (l2.a(g03, a6, z3)) {
                            }
                        } catch (IOException e3) {
                            this.i.b().t().a("Data loss. Failed to insert raw event metadata. appId", t43.a(z0.s()), e3);
                        }
                        l().u();
                        if (this.i.b().a(2)) {
                        }
                        l().A();
                        B();
                        this.i.b().B().a("Background event processing time, ms", Long.valueOf(((System.nanoTime() - j2) + 500000) / 1000000));
                        return;
                    }
                }
                String h2 = j032.b.h("currency");
                if ("ecommerce_purchase".equals(j032.a)) {
                    double doubleValue = j032.b.g("value").doubleValue() * 1000000.0d;
                    if (doubleValue == 0.0d) {
                        doubleValue = ((double) j032.b.f("value").longValue()) * 1000000.0d;
                    }
                    if (doubleValue > 9.223372036854776E18d || doubleValue < -9.223372036854776E18d) {
                        this.i.b().w().a("Data lost. Currency value is too big. appId", t43.a(str2), Double.valueOf(doubleValue));
                        j2 = nanoTime;
                        z = false;
                        if (!z) {
                            l().u();
                            l().A();
                            return;
                        }
                        boolean e22 = ma3.e(j032.a);
                        boolean equals2 = "_err".equals(j032.a);
                        String str32 = str2;
                        xz2 a22 = l().a(z(), str2, true, e22, false, equals2, false);
                        intValue = a22.b - ((long) l03.p.a(null).intValue());
                        if (intValue > 0) {
                        }
                    } else {
                        j3 = Math.round(doubleValue);
                    }
                } else {
                    j3 = j032.b.f("value").longValue();
                }
                if (!TextUtils.isEmpty(h2)) {
                    String upperCase = h2.toUpperCase(Locale.US);
                    if (upperCase.matches("[A-Z]{3}")) {
                        String valueOf = String.valueOf(upperCase);
                        String concat = valueOf.length() != 0 ? "_ltv_".concat(valueOf) : new String("_ltv_");
                        na3 c5 = l().c(str2, concat);
                        if (c5 != null) {
                            if (c5.e instanceof Long) {
                                j2 = nanoTime;
                                na3 = new na3(str2, j032.c, concat, this.i.zzm().b(), Long.valueOf(((Long) c5.e).longValue() + j3));
                                if (!l().a(na3)) {
                                    this.i.b().t().a("Too many unique user properties are set. Ignoring user property. appId", t43.a(str2), this.i.x().c(na3.c), na3.e);
                                    this.i.w().a(str2, 9, (String) null, (String) null, 0);
                                }
                                z = true;
                                if (!z) {
                                }
                                boolean e222 = ma3.e(j032.a);
                                boolean equals22 = "_err".equals(j032.a);
                                String str322 = str2;
                                xz2 a222 = l().a(z(), str2, true, e222, false, equals22, false);
                                intValue = a222.b - ((long) l03.p.a(null).intValue());
                                if (intValue > 0) {
                                }
                            }
                        }
                        j2 = nanoTime;
                        yz2 l3 = l();
                        int b4 = this.i.p().b(str2, l03.J) - 1;
                        w12.b(str2);
                        l3.g();
                        l3.r();
                        try {
                            l3.v().execSQL("delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);", new String[]{str2, str2, String.valueOf(b4)});
                        } catch (SQLiteException e4) {
                            l3.b().t().a("Error pruning currencies. appId", t43.a(str2), e4);
                        } catch (Throwable th) {
                            Throwable th2 = th;
                            l().A();
                            throw th2;
                        }
                        na3 = new na3(str2, j032.c, concat, this.i.zzm().b(), Long.valueOf(j3));
                        if (!l().a(na3)) {
                        }
                        z = true;
                        if (!z) {
                        }
                        boolean e2222 = ma3.e(j032.a);
                        boolean equals222 = "_err".equals(j032.a);
                        String str3222 = str2;
                        xz2 a2222 = l().a(z(), str2, true, e2222, false, equals222, false);
                        intValue = a2222.b - ((long) l03.p.a(null).intValue());
                        if (intValue > 0) {
                        }
                    }
                }
                j2 = nanoTime;
                z = true;
                if (!z) {
                }
                boolean e22222 = ma3.e(j032.a);
                boolean equals2222 = "_err".equals(j032.a);
                String str32222 = str2;
                xz2 a22222 = l().a(z(), str2, true, e22222, false, equals2222, false);
                intValue = a22222.b - ((long) l03.p.a(null).intValue());
                if (intValue > 0) {
                }
            }
        }
    }

    @DexIgnore
    public final void a(ja3 ja3) {
        this.i.a().g();
        yz2 yz2 = new yz2(this);
        yz2.s();
        this.c = yz2;
        this.i.p().a((eb3) this.a);
        sa3 sa3 = new sa3(this);
        sa3.s();
        this.f = sa3;
        f83 f83 = new f83(this);
        f83.s();
        this.h = f83;
        z93 z93 = new z93(this);
        z93.s();
        this.e = z93;
        this.d = new f53(this);
        if (this.o != this.p) {
            this.i.b().t().a("Not all upload components initialized", Integer.valueOf(this.o), Integer.valueOf(this.p));
        }
        this.j = true;
    }

    @DexIgnore
    public final u53 a() {
        return this.i.a();
    }

    @DexIgnore
    public final void a(j03 j03, String str) {
        j03 j032 = j03;
        a63 b2 = l().b(str);
        if (b2 == null || TextUtils.isEmpty(b2.u())) {
            this.i.b().A().a("No app data available; dropping event", str);
            return;
        }
        Boolean b3 = b(b2);
        if (b3 == null) {
            if (!"_ui".equals(j032.a)) {
                this.i.b().w().a("Could not find package. appId", t43.a(str));
            }
        } else if (!b3.booleanValue()) {
            this.i.b().t().a("App version does not match; dropping event. appId", t43.a(str));
            return;
        }
        ra3 ra3 = r2;
        ra3 ra32 = new ra3(str, b2.n(), b2.u(), b2.v(), b2.w(), b2.x(), b2.y(), (String) null, b2.A(), false, b2.r(), b2.f(), 0, 0, b2.g(), b2.h(), false, b2.o(), b2.i(), b2.z(), b2.j(), (!bt2.a() || !this.i.p().e(b2.l(), l03.K0)) ? null : b2.p());
        a(j032, ra3);
    }

    @DexIgnore
    public final void a(j03 j03, ra3 ra3) {
        List<ab3> list;
        List<ab3> list2;
        List<ab3> list3;
        List<String> list4;
        j03 j032 = j03;
        ra3 ra32 = ra3;
        w12.a(ra3);
        w12.b(ra32.a);
        y();
        r();
        String str = ra32.a;
        long j2 = j032.d;
        if (o().a(j032, ra32)) {
            if (!ra32.h) {
                c(ra32);
                return;
            }
            if (this.i.p().e(str, l03.t0) && (list4 = ra32.y) != null) {
                if (list4.contains(j032.a)) {
                    Bundle zzb = j032.b.zzb();
                    zzb.putLong("ga_safelisted", 1);
                    j032 = new j03(j032.a, new i03(zzb), j032.c, j032.d);
                } else {
                    this.i.b().A().a("Dropping non-safelisted event. appId, event name, origin", str, j032.a, j032.c);
                    return;
                }
            }
            l().z();
            try {
                yz2 l2 = l();
                w12.b(str);
                l2.g();
                l2.r();
                int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
                if (i2 < 0) {
                    l2.b().w().a("Invalid time querying timed out conditional properties", t43.a(str), Long.valueOf(j2));
                    list = Collections.emptyList();
                } else {
                    list = l2.a("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j2)});
                }
                for (ab3 next : list) {
                    if (next != null) {
                        this.i.b().A().a("User property timed out", next.a, this.i.x().c(next.c.b), next.c.zza());
                        if (next.g != null) {
                            b(new j03(next.g, j2), ra32);
                        }
                        l().e(str, next.c.b);
                    }
                }
                yz2 l3 = l();
                w12.b(str);
                l3.g();
                l3.r();
                if (i2 < 0) {
                    l3.b().w().a("Invalid time querying expired conditional properties", t43.a(str), Long.valueOf(j2));
                    list2 = Collections.emptyList();
                } else {
                    list2 = l3.a("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j2)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (ab3 next2 : list2) {
                    if (next2 != null) {
                        this.i.b().A().a("User property expired", next2.a, this.i.x().c(next2.c.b), next2.c.zza());
                        l().b(str, next2.c.b);
                        if (next2.o != null) {
                            arrayList.add(next2.o);
                        }
                        l().e(str, next2.c.b);
                    }
                }
                int size = arrayList.size();
                int i3 = 0;
                while (i3 < size) {
                    Object obj = arrayList.get(i3);
                    i3++;
                    b(new j03((j03) obj, j2), ra32);
                }
                yz2 l4 = l();
                String str2 = j032.a;
                w12.b(str);
                w12.b(str2);
                l4.g();
                l4.r();
                if (i2 < 0) {
                    l4.b().w().a("Invalid time querying triggered conditional properties", t43.a(str), l4.i().a(str2), Long.valueOf(j2));
                    list3 = Collections.emptyList();
                } else {
                    list3 = l4.a("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j2)});
                }
                ArrayList arrayList2 = new ArrayList(list3.size());
                for (ab3 next3 : list3) {
                    if (next3 != null) {
                        la3 la3 = next3.c;
                        na3 na3 = r4;
                        na3 na32 = new na3(next3.a, next3.b, la3.b, j2, la3.zza());
                        if (l().a(na3)) {
                            this.i.b().A().a("User property triggered", next3.a, this.i.x().c(na3.c), na3.e);
                        } else {
                            this.i.b().t().a("Too many active user properties, ignoring", t43.a(next3.a), this.i.x().c(na3.c), na3.e);
                        }
                        if (next3.i != null) {
                            arrayList2.add(next3.i);
                        }
                        next3.c = new la3(na3);
                        next3.e = true;
                        l().a(next3);
                    }
                }
                b(j032, ra32);
                int size2 = arrayList2.size();
                int i4 = 0;
                while (i4 < size2) {
                    Object obj2 = arrayList2.get(i4);
                    i4++;
                    b(new j03((j03) obj2, j2), ra32);
                }
                l().u();
            } finally {
                l().A();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0040, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        r2 = r0;
        r22 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0047, code lost:
        r6 = null;
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0098, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0099, code lost:
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:524:0x0cf1, code lost:
        if (r6 != r11) goto L_0x0cf3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040 A[ExcHandler: all (r0v22 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r3 
  PHI: (r3v32 android.database.Cursor) = (r3v27 android.database.Cursor), (r3v27 android.database.Cursor), (r3v27 android.database.Cursor), (r3v35 android.database.Cursor), (r3v35 android.database.Cursor), (r3v35 android.database.Cursor), (r3v35 android.database.Cursor), (r3v1 android.database.Cursor), (r3v1 android.database.Cursor) binds: [B:45:0x00d8, B:51:0x00e5, B:52:?, B:23:0x007b, B:29:0x0088, B:31:0x008c, B:32:?, B:9:0x0031, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0031] */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:150:0x028b A[SYNTHETIC, Splitter:B:150:0x028b] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0292 A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x02a0 A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x03da A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x03dc A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x03df A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x03e0 A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x05bf A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:295:0x069a A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:330:0x079d A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:336:0x07b3 A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:337:0x07cd A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:468:0x0b53 A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:469:0x0b66 A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:471:0x0b69 A[Catch:{ IOException -> 0x0238, all -> 0x0f72 }] */
    /* JADX WARNING: Removed duplicated region for block: B:472:0x0b90 A[SYNTHETIC, Splitter:B:472:0x0b90] */
    /* JADX WARNING: Removed duplicated region for block: B:531:0x0d0e A[Catch:{ all -> 0x0dac }] */
    /* JADX WARNING: Removed duplicated region for block: B:535:0x0d58 A[Catch:{ all -> 0x0dac }] */
    /* JADX WARNING: Removed duplicated region for block: B:610:0x0f58 A[SYNTHETIC, Splitter:B:610:0x0f58] */
    /* JADX WARNING: Removed duplicated region for block: B:617:0x0f6e A[SYNTHETIC, Splitter:B:617:0x0f6e] */
    public final boolean a(String str, long j2) {
        yz2 l2;
        Cursor cursor;
        Throwable th;
        Cursor cursor2;
        boolean z;
        boolean z2;
        long j3;
        long j4;
        a aVar;
        SecureRandom secureRandom;
        a aVar2;
        int i2;
        boolean z3;
        int d2;
        long j5;
        int i3;
        boolean z4;
        long j6;
        f03 f03;
        a63 b2;
        boolean z5;
        boolean z6;
        boolean z7;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        String str2;
        boolean z8;
        int i10;
        char c2;
        boolean z9;
        String str3;
        SQLiteException sQLiteException;
        Cursor cursor3;
        String str4;
        String str5;
        Cursor query;
        String[] strArr;
        String str6;
        String[] strArr2;
        ea3 ea3 = this;
        l().z();
        try {
            Cursor cursor4 = null;
            a aVar3 = new a(ea3, (da3) null);
            l2 = l();
            long j7 = ea3.x;
            w12.a(aVar3);
            l2.g();
            l2.r();
            try {
                SQLiteDatabase v2 = l2.v();
                if (TextUtils.isEmpty((CharSequence) null)) {
                    int i11 = (j7 > -1 ? 1 : (j7 == -1 ? 0 : -1));
                    if (i11 != 0) {
                        try {
                            strArr2 = new String[]{String.valueOf(j7), String.valueOf(j2)};
                        } catch (SQLiteException e2) {
                            e = e2;
                            cursor2 = cursor4;
                            str3 = null;
                        } catch (Throwable th2) {
                        }
                    } else {
                        strArr2 = new String[]{String.valueOf(j2)};
                    }
                    String str7 = i11 != 0 ? "rowid <= ? and " : "";
                    StringBuilder sb = new StringBuilder(str7.length() + 148);
                    sb.append("select app_id, metadata_fingerprint from raw_events where ");
                    sb.append(str7);
                    sb.append("app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;");
                    cursor4 = v2.rawQuery(sb.toString(), strArr2);
                    if (!cursor4.moveToFirst()) {
                        if (cursor4 != null) {
                            cursor4.close();
                        }
                        if (aVar3.c != null) {
                            if (!aVar3.c.isEmpty()) {
                                z = false;
                                if (!z) {
                                    qj2.a aVar4 = (qj2.a) aVar3.a.j();
                                    aVar4.l();
                                    boolean e3 = ea3.i.p().e(aVar3.a.w0(), l03.h0);
                                    int i12 = 0;
                                    int i13 = -1;
                                    int i14 = -1;
                                    boolean z10 = false;
                                    long j8 = 0;
                                    int i15 = 0;
                                    mj2.a aVar5 = null;
                                    mj2.a aVar6 = null;
                                    while (true) {
                                        z2 = z10;
                                        j3 = j8;
                                        if (i12 >= aVar3.c.size()) {
                                            break;
                                        }
                                        mj2.a aVar7 = (mj2.a) aVar3.c.get(i12).j();
                                        if (j().b(aVar3.a.w0(), aVar7.l())) {
                                            ea3.i.b().w().a("Dropping blacklisted raw event. appId", t43.a(aVar3.a.w0()), ea3.i.x().a(aVar7.l()));
                                            if (!j().g(aVar3.a.w0())) {
                                                if (!j().h(aVar3.a.w0())) {
                                                    z9 = false;
                                                    if (!z9 && !"_err".equals(aVar7.l())) {
                                                        ea3.i.w().a(aVar3.a.w0(), 11, "_ev", aVar7.l(), 0);
                                                    }
                                                    z7 = e3;
                                                    z10 = z2;
                                                    j8 = j3;
                                                    int i16 = i14;
                                                    i4 = i12;
                                                    i5 = i16;
                                                }
                                            }
                                            z9 = true;
                                            ea3.i.w().a(aVar3.a.w0(), 11, "_ev", aVar7.l(), 0);
                                            z7 = e3;
                                            z10 = z2;
                                            j8 = j3;
                                            int i162 = i14;
                                            i4 = i12;
                                            i5 = i162;
                                        } else {
                                            boolean c3 = j().c(aVar3.a.w0(), aVar7.l());
                                            if (!c3) {
                                                o();
                                                String l3 = aVar7.l();
                                                w12.b(l3);
                                                i9 = i15;
                                                int hashCode = l3.hashCode();
                                                i8 = i12;
                                                if (hashCode != 94660) {
                                                    if (hashCode != 95025) {
                                                        if (hashCode == 95027) {
                                                            if (l3.equals("_ui")) {
                                                                c2 = 1;
                                                                if (c2 == 0 || c2 == 1 || c2 == 2) {
                                                                    z7 = e3;
                                                                    i7 = i13;
                                                                    i6 = i14;
                                                                    str2 = "_et";
                                                                    if (c3) {
                                                                        ArrayList arrayList = new ArrayList(aVar7.j());
                                                                        int i17 = -1;
                                                                        int i18 = -1;
                                                                        for (int i19 = 0; i19 < arrayList.size(); i19++) {
                                                                            if ("value".equals(((oj2) arrayList.get(i19)).n())) {
                                                                                i17 = i19;
                                                                            } else if ("currency".equals(((oj2) arrayList.get(i19)).n())) {
                                                                                i18 = i19;
                                                                            }
                                                                        }
                                                                        if (i17 != -1) {
                                                                            if (((oj2) arrayList.get(i17)).q() || ((oj2) arrayList.get(i17)).s()) {
                                                                                if (i18 != -1) {
                                                                                    String p2 = ((oj2) arrayList.get(i18)).p();
                                                                                    if (p2.length() == 3) {
                                                                                        int i20 = 0;
                                                                                        while (i20 < p2.length()) {
                                                                                            int codePointAt = p2.codePointAt(i20);
                                                                                            if (Character.isLetter(codePointAt)) {
                                                                                                i20 += Character.charCount(codePointAt);
                                                                                            }
                                                                                        }
                                                                                        z8 = false;
                                                                                    }
                                                                                    z8 = true;
                                                                                    break;
                                                                                }
                                                                                z8 = true;
                                                                                if (z8) {
                                                                                    ea3.i.b().y().a("Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter.");
                                                                                    aVar7.b(i17);
                                                                                    a(aVar7, "_c");
                                                                                    a(aVar7, 19, "currency");
                                                                                }
                                                                                if (!ea3.i.p().e(aVar3.a.w0(), l03.g0)) {
                                                                                    if ("_e".equals(aVar7.l())) {
                                                                                        o();
                                                                                        if (ia3.a((mj2) ((fn2) aVar7.i()), "_fr") == null) {
                                                                                            if (aVar6 != null && Math.abs(aVar6.m() - aVar7.m()) <= 1000) {
                                                                                                mj2.a aVar8 = (mj2.a) ((fn2.b) aVar6.clone());
                                                                                                if (ea3.a(aVar7, aVar8)) {
                                                                                                    i5 = i6;
                                                                                                    aVar4.a(i5, aVar8);
                                                                                                    i13 = i7;
                                                                                                }
                                                                                            }
                                                                                            i5 = i6;
                                                                                            aVar5 = aVar7;
                                                                                            i13 = i9;
                                                                                        } else {
                                                                                            i5 = i6;
                                                                                            i13 = i7;
                                                                                        }
                                                                                    } else {
                                                                                        i5 = i6;
                                                                                        if ("_vs".equals(aVar7.l())) {
                                                                                            o();
                                                                                            if (ia3.a((mj2) ((fn2) aVar7.i()), str2) == null) {
                                                                                                if (aVar5 != null && Math.abs(aVar5.m() - aVar7.m()) <= 1000) {
                                                                                                    mj2.a aVar9 = (mj2.a) ((fn2.b) aVar5.clone());
                                                                                                    if (ea3.a(aVar9, aVar7)) {
                                                                                                        i13 = i7;
                                                                                                        aVar4.a(i13, aVar9);
                                                                                                    }
                                                                                                }
                                                                                                i13 = i7;
                                                                                                aVar6 = aVar7;
                                                                                                i5 = i9;
                                                                                            }
                                                                                            i13 = i7;
                                                                                        } else {
                                                                                            i13 = i7;
                                                                                            if (ea3.i.p().e(aVar3.a.w0(), l03.O0) && "_ab".equals(aVar7.l())) {
                                                                                                o();
                                                                                                if (ia3.a((mj2) ((fn2) aVar7.i()), str2) == null && aVar5 != null && Math.abs(aVar5.m() - aVar7.m()) <= 4000) {
                                                                                                    mj2.a aVar10 = (mj2.a) ((fn2.b) aVar5.clone());
                                                                                                    ea3.b(aVar10, aVar7);
                                                                                                    aVar4.a(i13, aVar10);
                                                                                                    aVar5 = null;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    aVar5 = null;
                                                                                    aVar6 = null;
                                                                                } else {
                                                                                    i13 = i7;
                                                                                    i5 = i6;
                                                                                }
                                                                                if (!z7 && "_e".equals(aVar7.l())) {
                                                                                    if (aVar7.k() != 0) {
                                                                                        ea3.i.b().w().a("Engagement event does not contain any parameters. appId", t43.a(aVar3.a.w0()));
                                                                                    } else {
                                                                                        o();
                                                                                        Long l4 = (Long) ia3.b((mj2) ((fn2) aVar7.i()), str2);
                                                                                        if (l4 == null) {
                                                                                            ea3.i.b().w().a("Engagement event does not include duration. appId", t43.a(aVar3.a.w0()));
                                                                                        } else {
                                                                                            j8 = j3 + l4.longValue();
                                                                                            i4 = i8;
                                                                                            aVar3.c.set(i4, (mj2) ((fn2) aVar7.i()));
                                                                                            i15 = i9 + 1;
                                                                                            aVar4.a(aVar7);
                                                                                            z10 = z2;
                                                                                        }
                                                                                    }
                                                                                }
                                                                                j8 = j3;
                                                                                i4 = i8;
                                                                                aVar3.c.set(i4, (mj2) ((fn2) aVar7.i()));
                                                                                i15 = i9 + 1;
                                                                                aVar4.a(aVar7);
                                                                                z10 = z2;
                                                                            } else {
                                                                                ea3.i.b().y().a("Value must be specified with a numeric type.");
                                                                                aVar7.b(i17);
                                                                                a(aVar7, "_c");
                                                                                a(aVar7, 18, "value");
                                                                            }
                                                                        }
                                                                        if (!ea3.i.p().e(aVar3.a.w0(), l03.g0)) {
                                                                        }
                                                                        if (aVar7.k() != 0) {
                                                                        }
                                                                        j8 = j3;
                                                                        i4 = i8;
                                                                        aVar3.c.set(i4, (mj2) ((fn2) aVar7.i()));
                                                                        i15 = i9 + 1;
                                                                        aVar4.a(aVar7);
                                                                        z10 = z2;
                                                                    }
                                                                    if (!ea3.i.p().e(aVar3.a.w0(), l03.g0)) {
                                                                    }
                                                                    if (aVar7.k() != 0) {
                                                                    }
                                                                    j8 = j3;
                                                                    i4 = i8;
                                                                    aVar3.c.set(i4, (mj2) ((fn2) aVar7.i()));
                                                                    i15 = i9 + 1;
                                                                    aVar4.a(aVar7);
                                                                    z10 = z2;
                                                                }
                                                            }
                                                        }
                                                    } else if (l3.equals("_ug")) {
                                                        c2 = 2;
                                                        if (c2 == 0 || c2 == 1 || c2 == 2) {
                                                        }
                                                    }
                                                } else if (l3.equals("_in")) {
                                                    c2 = 0;
                                                    if (c2 == 0 || c2 == 1 || c2 == 2) {
                                                    }
                                                }
                                                c2 = 65535;
                                                if (c2 == 0 || c2 == 1 || c2 == 2) {
                                                }
                                            } else {
                                                i8 = i12;
                                                i9 = i15;
                                            }
                                            z7 = e3;
                                            int i21 = 0;
                                            boolean z11 = false;
                                            boolean z12 = false;
                                            while (true) {
                                                i7 = i13;
                                                if (i21 >= aVar7.k()) {
                                                    break;
                                                }
                                                if ("_c".equals(aVar7.a(i21).n())) {
                                                    oj2.a aVar11 = (oj2.a) aVar7.a(i21).j();
                                                    i10 = i14;
                                                    aVar11.a(1);
                                                    aVar7.a(i21, (oj2) ((fn2) aVar11.i()));
                                                    z11 = true;
                                                } else {
                                                    i10 = i14;
                                                    if ("_r".equals(aVar7.a(i21).n())) {
                                                        oj2.a aVar12 = (oj2.a) aVar7.a(i21).j();
                                                        aVar12.a(1);
                                                        aVar7.a(i21, (oj2) ((fn2) aVar12.i()));
                                                        z12 = true;
                                                    }
                                                }
                                                i21++;
                                                i13 = i7;
                                                i14 = i10;
                                            }
                                            i6 = i14;
                                            if (z11 || !c3) {
                                                str2 = "_et";
                                            } else {
                                                ea3.i.b().B().a("Marking event as conversion", ea3.i.x().a(aVar7.l()));
                                                oj2.a y2 = oj2.y();
                                                y2.a("_c");
                                                str2 = "_et";
                                                y2.a(1);
                                                aVar7.a(y2);
                                            }
                                            if (!z12) {
                                                ea3.i.b().B().a("Marking event as real-time", ea3.i.x().a(aVar7.l()));
                                                oj2.a y3 = oj2.y();
                                                y3.a("_r");
                                                y3.a(1);
                                                aVar7.a(y3);
                                            }
                                            if (l().a(z(), aVar3.a.w0(), false, false, false, false, true).e > ((long) ea3.i.p().a(aVar3.a.w0()))) {
                                                a(aVar7, "_r");
                                            } else {
                                                z2 = true;
                                            }
                                            if (ma3.e(aVar7.l()) && c3 && l().a(z(), aVar3.a.w0(), false, false, true, false, false).c > ((long) ea3.i.p().b(aVar3.a.w0(), l03.s))) {
                                                ea3.i.b().w().a("Too many conversions. Not logging as conversion. appId", t43.a(aVar3.a.w0()));
                                                boolean z13 = false;
                                                oj2.a aVar13 = null;
                                                int i22 = -1;
                                                for (int i23 = 0; i23 < aVar7.k(); i23++) {
                                                    oj2 a2 = aVar7.a(i23);
                                                    if ("_c".equals(a2.n())) {
                                                        aVar13 = (oj2.a) a2.j();
                                                        i22 = i23;
                                                    } else if ("_err".equals(a2.n())) {
                                                        z13 = true;
                                                    }
                                                }
                                                if (z13 && aVar13 != null) {
                                                    aVar7.b(i22);
                                                } else if (aVar13 != null) {
                                                    oj2.a aVar14 = (oj2.a) ((fn2.b) aVar13.clone());
                                                    aVar14.a("_err");
                                                    aVar14.a(10);
                                                    aVar7.a(i22, (oj2) ((fn2) aVar14.i()));
                                                } else {
                                                    ea3.i.b().t().a("Did not find conversion parameter. appId", t43.a(aVar3.a.w0()));
                                                }
                                            }
                                            if (c3) {
                                            }
                                            if (!ea3.i.p().e(aVar3.a.w0(), l03.g0)) {
                                            }
                                            if (aVar7.k() != 0) {
                                            }
                                            j8 = j3;
                                            i4 = i8;
                                            aVar3.c.set(i4, (mj2) ((fn2) aVar7.i()));
                                            i15 = i9 + 1;
                                            aVar4.a(aVar7);
                                            z10 = z2;
                                        }
                                        int i24 = i4 + 1;
                                        i14 = i5;
                                        i12 = i24;
                                        e3 = z7;
                                    }
                                    String str8 = "_et";
                                    int i25 = i15;
                                    if (e3) {
                                        int i26 = i25;
                                        j4 = j3;
                                        int i27 = 0;
                                        while (i27 < i26) {
                                            mj2 b3 = aVar4.b(i27);
                                            if ("_e".equals(b3.p())) {
                                                o();
                                                if (ia3.a(b3, "_fr") != null) {
                                                    aVar4.c(i27);
                                                    i26--;
                                                    i27--;
                                                    i27++;
                                                }
                                            }
                                            o();
                                            oj2 a3 = ia3.a(b3, str8);
                                            if (a3 != null) {
                                                Long valueOf = a3.q() ? Long.valueOf(a3.r()) : null;
                                                if (valueOf != null && valueOf.longValue() > 0) {
                                                    j4 += valueOf.longValue();
                                                }
                                            }
                                            i27++;
                                        }
                                    } else {
                                        j4 = j3;
                                    }
                                    ea3.a(aVar4, j4, false);
                                    if (ea3.i.p().e(aVar4.s(), l03.w0)) {
                                        Iterator<mj2> it = aVar4.j().iterator();
                                        while (true) {
                                            if (it.hasNext()) {
                                                if ("_s".equals(it.next().p())) {
                                                    z6 = true;
                                                    break;
                                                }
                                            } else {
                                                z6 = false;
                                                break;
                                            }
                                        }
                                        if (z6) {
                                            l().b(aVar4.s(), "_se");
                                        }
                                        if (iu2.a() && ea3.i.p().e(aVar4.s(), l03.x0)) {
                                            if (!(ia3.a(aVar4, "_sid") >= 0)) {
                                                int a4 = ia3.a(aVar4, "_se");
                                                if (a4 >= 0) {
                                                    aVar4.e(a4);
                                                    ea3.i.b().w().a("Session engagement user property is in the bundle without session ID. appId", t43.a(aVar3.a.w0()));
                                                }
                                            }
                                        }
                                        ea3.a(aVar4, j4, true);
                                    } else if (ea3.i.p().e(aVar4.s(), l03.z0)) {
                                        l().b(aVar4.s(), "_se");
                                    }
                                    if (ea3.i.p().e(aVar4.s(), l03.i0)) {
                                        ia3 o2 = o();
                                        o2.b().B().a("Checking account type status for ad personalization signals");
                                        if (o2.p().e(aVar4.s()) && (b2 = o2.o().b(aVar4.s())) != null && b2.g() && o2.h().w()) {
                                            o2.b().A().a("Turning off ad personalization due to account type");
                                            uj2.a A = uj2.A();
                                            A.a("_npa");
                                            A.a(o2.h().u());
                                            A.b(1);
                                            uj2 uj2 = (uj2) ((fn2) A.i());
                                            int i28 = 0;
                                            while (true) {
                                                if (i28 >= aVar4.n()) {
                                                    z5 = false;
                                                    break;
                                                } else if ("_npa".equals(aVar4.d(i28).p())) {
                                                    aVar4.a(i28, uj2);
                                                    z5 = true;
                                                    break;
                                                } else {
                                                    i28++;
                                                }
                                            }
                                            if (!z5) {
                                                aVar4.a(uj2);
                                            }
                                        }
                                    }
                                    if (ea3.i.p().e(aVar4.s(), l03.J0)) {
                                        a(aVar4);
                                    }
                                    aVar4.w();
                                    String s2 = aVar4.s();
                                    List<uj2> m2 = aVar4.m();
                                    List<mj2> j9 = aVar4.j();
                                    long o3 = aVar4.o();
                                    w12.b(s2);
                                    aVar4.b((Iterable<? extends kj2>) m().a(s2, j9, m2, Long.valueOf(o3)));
                                    if (ea3.i.p().e(aVar3.a.w0())) {
                                        try {
                                            HashMap hashMap = new HashMap();
                                            ArrayList arrayList2 = new ArrayList();
                                            SecureRandom t2 = ea3.i.w().t();
                                            int i29 = 0;
                                            while (i29 < aVar4.k()) {
                                                mj2.a aVar15 = (mj2.a) aVar4.b(i29).j();
                                                if (aVar15.l().equals("_ep")) {
                                                    o();
                                                    String str9 = (String) ia3.b((mj2) ((fn2) aVar15.i()), "_en");
                                                    f03 f032 = (f03) hashMap.get(str9);
                                                    if (f032 == null) {
                                                        f032 = l().a(aVar3.a.w0(), str9);
                                                        hashMap.put(str9, f032);
                                                    }
                                                    if (f032.i == null) {
                                                        if (f032.j.longValue() > 1) {
                                                            o();
                                                            ia3.a(aVar15, "_sr", (Object) f032.j);
                                                        }
                                                        if (f032.k != null && f032.k.booleanValue()) {
                                                            o();
                                                            ia3.a(aVar15, "_efs", (Object) 1L);
                                                        }
                                                        arrayList2.add((mj2) ((fn2) aVar15.i()));
                                                    }
                                                    aVar4.a(i29, aVar15);
                                                } else {
                                                    long f2 = j().f(aVar3.a.w0());
                                                    ea3.i.w();
                                                    long a5 = ma3.a(aVar15.m(), f2);
                                                    mj2 mj2 = (mj2) ((fn2) aVar15.i());
                                                    Long l5 = 1L;
                                                    if (!TextUtils.isEmpty("_dbg") && l5 != null) {
                                                        Iterator<oj2> it2 = mj2.n().iterator();
                                                        while (true) {
                                                            if (!it2.hasNext()) {
                                                                break;
                                                            }
                                                            oj2 next = it2.next();
                                                            Iterator<oj2> it3 = it2;
                                                            if (!"_dbg".equals(next.n())) {
                                                                it2 = it3;
                                                            } else if (((l5 instanceof Long) && l5.equals(Long.valueOf(next.r()))) || (((l5 instanceof String) && l5.equals(next.p())) || ((l5 instanceof Double) && l5.equals(Double.valueOf(next.t()))))) {
                                                                z3 = true;
                                                            }
                                                        }
                                                        d2 = z3 ? j().d(aVar3.a.w0(), aVar15.l()) : 1;
                                                        if (d2 > 0) {
                                                            ea3.i.b().w().a("Sample rate must be positive. event, rate", aVar15.l(), Integer.valueOf(d2));
                                                            arrayList2.add((mj2) ((fn2) aVar15.i()));
                                                            aVar4.a(i29, aVar15);
                                                        } else {
                                                            f03 f033 = (f03) hashMap.get(aVar15.l());
                                                            if (f033 == null) {
                                                                j5 = f2;
                                                                f033 = l().a(aVar3.a.w0(), aVar15.l());
                                                                if (f033 == null) {
                                                                    ea3.i.b().w().a("Event being bundled has no eventAggregate. appId, eventName", aVar3.a.w0(), aVar15.l());
                                                                    if (ea3.i.p().e(aVar3.a.w0(), l03.v0)) {
                                                                        f03 = new f03(aVar3.a.w0(), aVar15.l(), 1, 1, 1, aVar15.m(), 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                                                                    } else {
                                                                        f03 = new f03(aVar3.a.w0(), aVar15.l(), 1, 1, aVar15.m(), 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                                                                    }
                                                                    f033 = f03;
                                                                }
                                                            } else {
                                                                j5 = f2;
                                                            }
                                                            o();
                                                            Long l6 = (Long) ia3.b((mj2) ((fn2) aVar15.i()), "_eid");
                                                            Boolean valueOf2 = Boolean.valueOf(l6 != null);
                                                            if (d2 == 1) {
                                                                arrayList2.add((mj2) ((fn2) aVar15.i()));
                                                                if (valueOf2.booleanValue() && !(f033.i == null && f033.j == null && f033.k == null)) {
                                                                    hashMap.put(aVar15.l(), f033.a((Long) null, (Long) null, (Boolean) null));
                                                                }
                                                                aVar4.a(i29, aVar15);
                                                            } else {
                                                                if (t2.nextInt(d2) == 0) {
                                                                    o();
                                                                    secureRandom = t2;
                                                                    i3 = i29;
                                                                    long j10 = (long) d2;
                                                                    ia3.a(aVar15, "_sr", (Object) Long.valueOf(j10));
                                                                    arrayList2.add((mj2) ((fn2) aVar15.i()));
                                                                    if (valueOf2.booleanValue()) {
                                                                        f033 = f033.a((Long) null, Long.valueOf(j10), (Boolean) null);
                                                                    }
                                                                    hashMap.put(aVar15.l(), f033.a(aVar15.m(), a5));
                                                                    aVar2 = aVar3;
                                                                } else {
                                                                    secureRandom = t2;
                                                                    i3 = i29;
                                                                    if (!ea3.i.p().k(aVar3.a.w0())) {
                                                                        aVar2 = aVar3;
                                                                        if (Math.abs(aVar15.m() - f033.g) >= 86400000) {
                                                                        }
                                                                        z4 = false;
                                                                        if (!z4) {
                                                                            o();
                                                                            ia3.a(aVar15, "_efs", (Object) 1L);
                                                                            o();
                                                                            long j11 = (long) d2;
                                                                            ia3.a(aVar15, "_sr", (Object) Long.valueOf(j11));
                                                                            arrayList2.add((mj2) ((fn2) aVar15.i()));
                                                                            if (valueOf2.booleanValue()) {
                                                                                f033 = f033.a((Long) null, Long.valueOf(j11), true);
                                                                            }
                                                                            hashMap.put(aVar15.l(), f033.a(aVar15.m(), a5));
                                                                        } else if (valueOf2.booleanValue()) {
                                                                            hashMap.put(aVar15.l(), f033.a(l6, (Long) null, (Boolean) null));
                                                                        }
                                                                    } else if (f033.h != null) {
                                                                        j6 = f033.h.longValue();
                                                                        aVar2 = aVar3;
                                                                    } else {
                                                                        ea3.i.w();
                                                                        aVar2 = aVar3;
                                                                        j6 = ma3.a(aVar15.n(), j5);
                                                                    }
                                                                    z4 = true;
                                                                    if (!z4) {
                                                                    }
                                                                }
                                                                i2 = i3;
                                                                aVar4.a(i2, aVar15);
                                                                i29 = i2 + 1;
                                                                ea3 = this;
                                                                t2 = secureRandom;
                                                                aVar3 = aVar2;
                                                            }
                                                        }
                                                    }
                                                    z3 = false;
                                                    if (z3) {
                                                    }
                                                    if (d2 > 0) {
                                                    }
                                                }
                                                aVar2 = aVar3;
                                                secureRandom = t2;
                                                i2 = i29;
                                                i29 = i2 + 1;
                                                ea3 = this;
                                                t2 = secureRandom;
                                                aVar3 = aVar2;
                                            }
                                            aVar = aVar3;
                                            if (arrayList2.size() < aVar4.k()) {
                                                aVar4.l();
                                                aVar4.a((Iterable<? extends mj2>) arrayList2);
                                            }
                                            for (Map.Entry value : hashMap.entrySet()) {
                                                l().a((f03) value.getValue());
                                            }
                                        } catch (Throwable th3) {
                                            th = th3;
                                            Throwable th4 = th;
                                            l().A();
                                            throw th4;
                                        }
                                    } else {
                                        aVar = aVar3;
                                    }
                                    if (!this.i.p().e(aVar4.s(), l03.J0)) {
                                        a(aVar4);
                                    }
                                    a aVar16 = aVar;
                                    String w0 = aVar16.a.w0();
                                    a63 b4 = l().b(w0);
                                    if (b4 == null) {
                                        this.i.b().t().a("Bundling raw events w/o app info. appId", t43.a(aVar16.a.w0()));
                                    } else if (aVar4.k() > 0) {
                                        long t3 = b4.t();
                                        if (t3 != 0) {
                                            aVar4.e(t3);
                                        } else {
                                            aVar4.r();
                                        }
                                        long s3 = b4.s();
                                        if (s3 != 0) {
                                            t3 = s3;
                                        }
                                        if (t3 != 0) {
                                            aVar4.d(t3);
                                        } else {
                                            aVar4.q();
                                        }
                                        b4.E();
                                        aVar4.g((int) b4.B());
                                        b4.a(aVar4.o());
                                        b4.b(aVar4.p());
                                        String e4 = b4.e();
                                        if (e4 != null) {
                                            aVar4.j(e4);
                                        } else {
                                            aVar4.t();
                                        }
                                        l().a(b4);
                                    }
                                    if (aVar4.k() > 0) {
                                        this.i.d();
                                        gj2 a6 = j().a(aVar16.a.w0());
                                        if (a6 != null) {
                                            if (a6.n()) {
                                                aVar4.i(a6.o());
                                                l().a((qj2) ((fn2) aVar4.i()), z2);
                                            }
                                        }
                                        if (TextUtils.isEmpty(aVar16.a.B())) {
                                            aVar4.i(-1);
                                        } else {
                                            this.i.b().w().a("Did not find measurement config or missing version info. appId", t43.a(aVar16.a.w0()));
                                        }
                                        l().a((qj2) ((fn2) aVar4.i()), z2);
                                    }
                                    yz2 l7 = l();
                                    List<Long> list = aVar16.b;
                                    w12.a(list);
                                    l7.g();
                                    l7.r();
                                    StringBuilder sb2 = new StringBuilder("rowid in (");
                                    for (int i30 = 0; i30 < list.size(); i30++) {
                                        if (i30 != 0) {
                                            sb2.append(",");
                                        }
                                        sb2.append(list.get(i30).longValue());
                                    }
                                    sb2.append(")");
                                    int delete = l7.v().delete("raw_events", sb2.toString(), (String[]) null);
                                    if (delete != list.size()) {
                                        l7.b().t().a("Deleted fewer rows from raw events table than expected", Integer.valueOf(delete), Integer.valueOf(list.size()));
                                    }
                                    yz2 l8 = l();
                                    try {
                                        l8.v().execSQL("delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)", new String[]{w0, w0});
                                    } catch (SQLiteException e5) {
                                        l8.b().t().a("Failed to remove unused event metadata. appId", t43.a(w0), e5);
                                    }
                                    l().u();
                                    l().A();
                                    return true;
                                }
                                l().u();
                                l().A();
                                return false;
                            }
                        }
                        z = true;
                        if (!z) {
                        }
                    } else {
                        str3 = cursor4.getString(0);
                        String string = cursor4.getString(1);
                        cursor4.close();
                        cursor3 = cursor4;
                        str5 = str3;
                        str4 = string;
                    }
                } else {
                    int i31 = (j7 > -1 ? 1 : (j7 == -1 ? 0 : -1));
                    String[] strArr3 = i31 != 0 ? new String[]{null, String.valueOf(j7)} : new String[]{null};
                    String str10 = i31 != 0 ? " and rowid <= ?" : "";
                    StringBuilder sb3 = new StringBuilder(str10.length() + 84);
                    sb3.append("select metadata_fingerprint from raw_events where app_id = ?");
                    sb3.append(str10);
                    sb3.append(" order by rowid limit 1;");
                    cursor4 = v2.rawQuery(sb3.toString(), strArr3);
                    if (!cursor4.moveToFirst()) {
                        if (cursor4 != null) {
                            cursor4.close();
                        }
                        if (aVar3.c != null) {
                        }
                        z = true;
                        if (!z) {
                        }
                    } else {
                        String string2 = cursor4.getString(0);
                        cursor4.close();
                        cursor3 = cursor4;
                        str4 = string2;
                        str5 = null;
                    }
                }
                try {
                    SQLiteDatabase sQLiteDatabase = v2;
                    query = v2.query("raw_events_metadata", new String[]{"metadata"}, "app_id = ? and metadata_fingerprint = ?", new String[]{str5, str4}, (String) null, (String) null, "rowid", "2");
                    try {
                        if (!query.moveToFirst()) {
                            try {
                                l2.b().t().a("Raw event metadata record is missing. appId", t43.a(str5));
                                if (query != null) {
                                    query.close();
                                }
                            } catch (SQLiteException e6) {
                                e = e6;
                                str3 = str5;
                                cursor2 = query;
                                sQLiteException = e;
                                try {
                                    l2.b().t().a("Data loss. Error selecting raw event. appId", t43.a(str3), sQLiteException);
                                    if (cursor2 != null) {
                                    }
                                    if (aVar3.c != null) {
                                    }
                                    z = true;
                                    if (!z) {
                                    }
                                } catch (Throwable th5) {
                                    th = th5;
                                    th = th;
                                    cursor = cursor2;
                                    if (cursor != null) {
                                    }
                                    throw th;
                                }
                            } catch (Throwable th6) {
                                th = th6;
                                cursor = query;
                                if (cursor != null) {
                                }
                                throw th;
                            }
                            if (aVar3.c != null) {
                            }
                            z = true;
                            if (!z) {
                            }
                        } else {
                            byte[] blob = query.getBlob(0);
                            qj2.a z0 = qj2.z0();
                            ia3.a(z0, blob);
                            qj2 qj2 = (qj2) ((fn2) z0.i());
                            if (query.moveToNext()) {
                                l2.b().w().a("Get multiple raw event metadata records, expected one. appId", t43.a(str5));
                            }
                            query.close();
                            aVar3.a(qj2);
                            if (j7 != -1) {
                                str6 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?";
                                strArr = new String[]{str5, str4, String.valueOf(j7)};
                            } else {
                                str6 = "app_id = ? and metadata_fingerprint = ?";
                                strArr = new String[]{str5, str4};
                            }
                            cursor2 = query;
                            try {
                                Cursor query2 = sQLiteDatabase.query("raw_events", new String[]{"rowid", "name", "timestamp", "data"}, str6, strArr, (String) null, (String) null, "rowid", (String) null);
                                try {
                                    if (!query2.moveToFirst()) {
                                        l2.b().w().a("Raw event data disappeared while in transaction. appId", t43.a(str5));
                                        if (query2 != null) {
                                            query2.close();
                                        }
                                        if (aVar3.c != null) {
                                        }
                                        z = true;
                                        if (!z) {
                                        }
                                    } else {
                                        do {
                                            long j12 = query2.getLong(0);
                                            byte[] blob2 = query2.getBlob(3);
                                            try {
                                                mj2.a y4 = mj2.y();
                                                ia3.a(y4, blob2);
                                                mj2.a aVar17 = y4;
                                                aVar17.a(query2.getString(1));
                                                aVar17.a(query2.getLong(2));
                                                if (!aVar3.a(j12, (mj2) ((fn2) aVar17.i()))) {
                                                    if (query2 != null) {
                                                        query2.close();
                                                    }
                                                    if (aVar3.c != null) {
                                                    }
                                                    z = true;
                                                    if (!z) {
                                                    }
                                                }
                                            } catch (IOException e7) {
                                                l2.b().t().a("Data loss. Failed to merge raw event. appId", t43.a(str5), e7);
                                            }
                                        } while (query2.moveToNext());
                                        if (query2 != null) {
                                            query2.close();
                                        }
                                        if (aVar3.c != null) {
                                        }
                                        z = true;
                                        if (!z) {
                                        }
                                    }
                                } catch (SQLiteException e8) {
                                    e = e8;
                                    str3 = str5;
                                    cursor2 = query2;
                                    sQLiteException = e;
                                    l2.b().t().a("Data loss. Error selecting raw event. appId", t43.a(str3), sQLiteException);
                                    if (cursor2 != null) {
                                    }
                                    if (aVar3.c != null) {
                                    }
                                    z = true;
                                    if (!z) {
                                    }
                                } catch (Throwable th7) {
                                    th = th7;
                                    cursor = query2;
                                    if (cursor != null) {
                                    }
                                    throw th;
                                }
                            } catch (SQLiteException e9) {
                                e = e9;
                                str3 = str5;
                                sQLiteException = e;
                                l2.b().t().a("Data loss. Error selecting raw event. appId", t43.a(str3), sQLiteException);
                                if (cursor2 != null) {
                                }
                                if (aVar3.c != null) {
                                }
                                z = true;
                                if (!z) {
                                }
                            }
                        }
                    } catch (SQLiteException e10) {
                        e = e10;
                        cursor2 = query;
                        str3 = str5;
                        sQLiteException = e;
                        l2.b().t().a("Data loss. Error selecting raw event. appId", t43.a(str3), sQLiteException);
                        if (cursor2 != null) {
                        }
                        if (aVar3.c != null) {
                        }
                        z = true;
                        if (!z) {
                        }
                    } catch (Throwable th8) {
                        th = th8;
                        cursor2 = query;
                        th = th;
                        cursor = cursor2;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (SQLiteException e11) {
                    e = e11;
                    str3 = str5;
                    cursor2 = cursor3;
                    sQLiteException = e;
                    l2.b().t().a("Data loss. Error selecting raw event. appId", t43.a(str3), sQLiteException);
                    if (cursor2 != null) {
                    }
                    if (aVar3.c != null) {
                    }
                    z = true;
                    if (!z) {
                    }
                } catch (Throwable th9) {
                    th = th9;
                    cursor = cursor3;
                    if (cursor != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e12) {
                sQLiteException = e12;
                cursor2 = null;
                str3 = null;
                l2.b().t().a("Data loss. Error selecting raw event. appId", t43.a(str3), sQLiteException);
                if (cursor2 != null) {
                    cursor2.close();
                }
                if (aVar3.c != null) {
                }
                z = true;
                if (!z) {
                }
            } catch (Throwable th10) {
                th = th10;
                cursor = null;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (IOException e13) {
            cursor2 = query;
            l2.b().t().a("Data loss. Failed to merge raw event metadata. appId", t43.a(str5), e13);
            if (cursor2 != null) {
                cursor2.close();
            }
        } catch (Throwable th11) {
            th = th11;
        }
    }

    @DexIgnore
    public final void b(mj2.a aVar, mj2.a aVar2) {
        w12.a("_e".equals(aVar.l()));
        o();
        oj2 a2 = ia3.a((mj2) aVar.i(), "_et");
        if (a2.q() && a2.r() > 0) {
            long r2 = a2.r();
            o();
            oj2 a3 = ia3.a((mj2) aVar2.i(), "_et");
            if (a3 != null && a3.r() > 0) {
                r2 += a3.r();
            }
            o();
            ia3.a(aVar2, "_et", (Object) Long.valueOf(r2));
            o();
            ia3.a(aVar, "_fr", (Object) 1L);
        }
    }

    @DexIgnore
    public final Boolean b(a63 a63) {
        try {
            if (a63.v() != -2147483648L) {
                if (a63.v() == ((long) g52.b(this.i.c()).b(a63.l(), 0).versionCode)) {
                    return true;
                }
            } else {
                String str = g52.b(this.i.c()).b(a63.l(), 0).versionName;
                if (a63.u() != null && a63.u().equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public final void b(la3 la3, ra3 ra3) {
        y();
        r();
        if (TextUtils.isEmpty(ra3.b) && TextUtils.isEmpty(ra3.v)) {
            return;
        }
        if (!ra3.h) {
            c(ra3);
        } else if (!this.i.p().e(ra3.a, l03.i0)) {
            this.i.b().A().a("Removing user property", this.i.x().c(la3.b));
            l().z();
            try {
                c(ra3);
                l().b(ra3.a, la3.b);
                l().u();
                this.i.b().A().a("User property removed", this.i.x().c(la3.b));
            } finally {
                l().A();
            }
        } else if (!"_npa".equals(la3.b) || ra3.w == null) {
            this.i.b().A().a("Removing user property", this.i.x().c(la3.b));
            l().z();
            try {
                c(ra3);
                l().b(ra3.a, la3.b);
                l().u();
                this.i.b().A().a("User property removed", this.i.x().c(la3.b));
            } finally {
                l().A();
            }
        } else {
            this.i.b().A().a("Falling back to manifest metadata value for ad personalization");
            a(new la3("_npa", this.i.zzm().b(), Long.valueOf(ra3.w.booleanValue() ? 1 : 0), "auto"), ra3);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:170:0x04b4 A[Catch:{ NameNotFoundException -> 0x034e, all -> 0x04df }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01f5 A[Catch:{ NameNotFoundException -> 0x034e, all -> 0x04df }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0229 A[Catch:{ NameNotFoundException -> 0x034e, all -> 0x04df }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x022b A[Catch:{ NameNotFoundException -> 0x034e, all -> 0x04df }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x022f A[Catch:{ NameNotFoundException -> 0x034e, all -> 0x04df }] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0252 A[Catch:{ NameNotFoundException -> 0x034e, all -> 0x04df }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0259 A[Catch:{ NameNotFoundException -> 0x034e, all -> 0x04df }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0266 A[Catch:{ NameNotFoundException -> 0x034e, all -> 0x04df }] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0279 A[Catch:{ NameNotFoundException -> 0x034e, all -> 0x04df }] */
    public final void b(ra3 ra3) {
        String str;
        int i2;
        String str2;
        f03 f03;
        String str3;
        PackageInfo packageInfo;
        String str4;
        long j2;
        ApplicationInfo applicationInfo;
        boolean z;
        boolean z2;
        na3 c2;
        ra3 ra32 = ra3;
        y();
        r();
        w12.a(ra3);
        w12.b(ra32.a);
        if (!TextUtils.isEmpty(ra32.b) || !TextUtils.isEmpty(ra32.v)) {
            a63 b2 = l().b(ra32.a);
            if (b2 != null && TextUtils.isEmpty(b2.n()) && !TextUtils.isEmpty(ra32.b)) {
                b2.h(0);
                l().a(b2);
                j().d(ra32.a);
            }
            if (!ra32.h) {
                c(ra3);
                return;
            }
            long j3 = ra32.q;
            if (j3 == 0) {
                j3 = this.i.zzm().b();
            }
            if (this.i.p().e(ra32.a, l03.i0)) {
                this.i.G().v();
            }
            int i3 = ra32.r;
            if (!(i3 == 0 || i3 == 1)) {
                this.i.b().w().a("Incorrect app type, assuming installed app. appId, appType", t43.a(ra32.a), Integer.valueOf(i3));
                i3 = 0;
            }
            l().z();
            try {
                if (!this.i.p().e(ra32.a, l03.i0) || ((c2 = l().c(ra32.a, "_npa")) != null && !"auto".equals(c2.b))) {
                    str = "_sys";
                    i2 = 1;
                } else if (ra32.w != null) {
                    la3 la3 = r11;
                    str = "_sys";
                    na3 na3 = c2;
                    i2 = 1;
                    la3 la32 = new la3("_npa", j3, Long.valueOf(ra32.w.booleanValue() ? 1 : 0), "auto");
                    if (na3 == null || !na3.e.equals(la3.d)) {
                        a(la3, ra32);
                    }
                } else {
                    str = "_sys";
                    i2 = 1;
                    if (c2 != null) {
                        b(new la3("_npa", j3, (Object) null, "auto"), ra32);
                    }
                }
                a63 b3 = l().b(ra32.a);
                if (b3 != null) {
                    this.i.w();
                    if (ma3.a(ra32.b, b3.n(), ra32.v, b3.o())) {
                        this.i.b().w().a("New GMP App Id passed in. Removing cached database data. appId", t43.a(b3.l()));
                        yz2 l2 = l();
                        String l3 = b3.l();
                        l2.r();
                        l2.g();
                        w12.b(l3);
                        try {
                            SQLiteDatabase v2 = l2.v();
                            String[] strArr = new String[i2];
                            try {
                                strArr[0] = l3;
                                int delete = v2.delete("events", "app_id=?", strArr) + 0 + v2.delete("user_attributes", "app_id=?", strArr) + v2.delete("conditional_properties", "app_id=?", strArr) + v2.delete("apps", "app_id=?", strArr) + v2.delete("raw_events", "app_id=?", strArr) + v2.delete("raw_events_metadata", "app_id=?", strArr) + v2.delete("event_filters", "app_id=?", strArr) + v2.delete("property_filters", "app_id=?", strArr) + v2.delete("audience_filter_values", "app_id=?", strArr);
                                if (delete > 0) {
                                    l2.b().B().a("Deleted application data. app, records", l3, Integer.valueOf(delete));
                                }
                            } catch (SQLiteException e2) {
                                e = e2;
                                l2.b().t().a("Error deleting application data. appId, error", t43.a(l3), e);
                                b3 = null;
                                if (b3 == null) {
                                }
                                c(ra3);
                                if (i3 == 0) {
                                }
                                if (f03 == null) {
                                }
                                l().u();
                                l().A();
                            }
                        } catch (SQLiteException e3) {
                            e = e3;
                            l2.b().t().a("Error deleting application data. appId, error", t43.a(l3), e);
                            b3 = null;
                            if (b3 == null) {
                            }
                            c(ra3);
                            if (i3 == 0) {
                            }
                            if (f03 == null) {
                            }
                            l().u();
                            l().A();
                        }
                        b3 = null;
                        if (b3 == null) {
                            if (b3.v() != -2147483648L) {
                                str2 = "_pfo";
                                if (b3.v() != ra32.j) {
                                    z2 = true;
                                    if (z2 || (b3.v() == -2147483648L && b3.u() != null && !b3.u().equals(ra32.c))) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("_pv", b3.u());
                                        a(new j03("_au", new i03(bundle), "auto", j3), ra32);
                                        c(ra3);
                                        if (i3 == 0) {
                                            f03 = l().a(ra32.a, "_f");
                                        } else {
                                            f03 = i3 == 1 ? l().a(ra32.a, "_v") : null;
                                        }
                                        if (f03 == null) {
                                            long j4 = ((j3 / 3600000) + 1) * 3600000;
                                            if (i3 == 0) {
                                                str3 = "_et";
                                                a(new la3("_fot", j3, Long.valueOf(j4), "auto"), ra32);
                                                if (this.i.p().j(ra32.b)) {
                                                    y();
                                                    this.i.t().a(ra32.a);
                                                }
                                                y();
                                                r();
                                                Bundle bundle2 = new Bundle();
                                                bundle2.putLong("_c", 1);
                                                bundle2.putLong("_r", 1);
                                                bundle2.putLong("_uwa", 0);
                                                bundle2.putLong(str2, 0);
                                                String str5 = str;
                                                bundle2.putLong(str5, 0);
                                                bundle2.putLong("_sysu", 0);
                                                if (this.i.p().p(ra32.a)) {
                                                    bundle2.putLong(str3, 1);
                                                }
                                                if (ra32.u) {
                                                    bundle2.putLong("_dac", 1);
                                                }
                                                yz2 l4 = l();
                                                String str6 = ra32.a;
                                                w12.b(str6);
                                                l4.g();
                                                l4.r();
                                                long h2 = l4.h(str6, "first_open_count");
                                                if (this.i.c().getPackageManager() == null) {
                                                    this.i.b().t().a("PackageManager is null, first open report might be inaccurate. appId", t43.a(ra32.a));
                                                    str4 = str2;
                                                    j2 = h2;
                                                } else {
                                                    packageInfo = g52.b(this.i.c()).b(ra32.a, 0);
                                                    if (packageInfo == null || packageInfo.firstInstallTime == 0) {
                                                        str4 = str2;
                                                        j2 = h2;
                                                    } else {
                                                        str4 = str2;
                                                        if (packageInfo.firstInstallTime != packageInfo.lastUpdateTime) {
                                                            if (!this.i.p().a(l03.Q0)) {
                                                                bundle2.putLong("_uwa", 1);
                                                            } else if (h2 == 0) {
                                                                bundle2.putLong("_uwa", 1);
                                                            }
                                                            z = false;
                                                        } else {
                                                            z = true;
                                                        }
                                                        j2 = h2;
                                                        a(new la3("_fi", j3, Long.valueOf(z ? 1 : 0), "auto"), ra32);
                                                    }
                                                    try {
                                                        applicationInfo = g52.b(this.i.c()).a(ra32.a, 0);
                                                    } catch (PackageManager.NameNotFoundException e4) {
                                                        this.i.b().t().a("Application info is null, first open report might be inaccurate. appId", t43.a(ra32.a), e4);
                                                        applicationInfo = null;
                                                    }
                                                    if (applicationInfo != null) {
                                                        if ((applicationInfo.flags & 1) != 0) {
                                                            bundle2.putLong(str5, 1);
                                                        }
                                                        if ((applicationInfo.flags & 128) != 0) {
                                                            bundle2.putLong("_sysu", 1);
                                                        }
                                                    }
                                                }
                                                if (j2 >= 0) {
                                                    bundle2.putLong(str4, j2);
                                                }
                                                a(new j03("_f", new i03(bundle2), "auto", j3), ra32);
                                            } else {
                                                str3 = "_et";
                                                if (i3 == 1) {
                                                    a(new la3("_fvt", j3, Long.valueOf(j4), "auto"), ra32);
                                                    y();
                                                    r();
                                                    Bundle bundle3 = new Bundle();
                                                    bundle3.putLong("_c", 1);
                                                    bundle3.putLong("_r", 1);
                                                    if (this.i.p().p(ra32.a)) {
                                                        bundle3.putLong(str3, 1);
                                                    }
                                                    if (ra32.u) {
                                                        bundle3.putLong("_dac", 1);
                                                    }
                                                    a(new j03("_v", new i03(bundle3), "auto", j3), ra32);
                                                }
                                            }
                                            if (!this.i.p().e(ra32.a, l03.h0)) {
                                                Bundle bundle4 = new Bundle();
                                                bundle4.putLong(str3, 1);
                                                if (this.i.p().p(ra32.a)) {
                                                    bundle4.putLong("_fr", 1);
                                                }
                                                a(new j03("_e", new i03(bundle4), "auto", j3), ra32);
                                            }
                                        } else if (ra32.i) {
                                            a(new j03("_cd", new i03(new Bundle()), "auto", j3), ra32);
                                        }
                                        l().u();
                                        l().A();
                                    }
                                }
                            } else {
                                str2 = "_pfo";
                            }
                            z2 = false;
                            if (z2 || (b3.v() == -2147483648L && b3.u() != null && !b3.u().equals(ra32.c))) {
                            }
                        } else {
                            str2 = "_pfo";
                        }
                        c(ra3);
                        if (i3 == 0) {
                        }
                        if (f03 == null) {
                        }
                        l().u();
                        l().A();
                    }
                }
                if (b3 == null) {
                }
                c(ra3);
                if (i3 == 0) {
                }
                if (f03 == null) {
                }
            } catch (PackageManager.NameNotFoundException e5) {
                this.i.b().t().a("Package info is null, first open report might be inaccurate. appId", t43.a(ra32.a), e5);
                packageInfo = null;
            } catch (Throwable th) {
                l().A();
                throw th;
            }
            l().u();
            l().A();
        }
    }

    @DexIgnore
    public final void b(ab3 ab3) {
        ra3 a2 = a(ab3.a);
        if (a2 != null) {
            b(ab3, a2);
        }
    }

    @DexIgnore
    public final void b(ab3 ab3, ra3 ra3) {
        w12.a(ab3);
        w12.b(ab3.a);
        w12.a(ab3.c);
        w12.b(ab3.c.b);
        y();
        r();
        if (TextUtils.isEmpty(ra3.b) && TextUtils.isEmpty(ra3.v)) {
            return;
        }
        if (!ra3.h) {
            c(ra3);
            return;
        }
        l().z();
        try {
            c(ra3);
            ab3 d2 = l().d(ab3.a, ab3.c.b);
            if (d2 != null) {
                this.i.b().A().a("Removing conditional user property", ab3.a, this.i.x().c(ab3.c.b));
                l().e(ab3.a, ab3.c.b);
                if (d2.e) {
                    l().b(ab3.a, ab3.c.b);
                }
                if (ab3.o != null) {
                    Bundle bundle = null;
                    if (ab3.o.b != null) {
                        bundle = ab3.o.b.zzb();
                    }
                    Bundle bundle2 = bundle;
                    b(this.i.w().a(ab3.a, ab3.o.a, bundle2, d2.b, ab3.o.d, true, false), ra3);
                }
            } else {
                this.i.b().w().a("Conditional user property doesn't exist", t43.a(ab3.a), this.i.x().c(ab3.c.b));
            }
            l().u();
        } finally {
            l().A();
        }
    }

    @DexIgnore
    public static void a(qj2.a aVar) {
        aVar.b((long) RecyclerView.FOREVER_NS);
        aVar.c(Long.MIN_VALUE);
        for (int i2 = 0; i2 < aVar.k(); i2++) {
            mj2 b2 = aVar.b(i2);
            if (b2.r() < aVar.o()) {
                aVar.b(b2.r());
            }
            if (b2.r() > aVar.p()) {
                aVar.c(b2.r());
            }
        }
    }

    @DexIgnore
    public final void a(qj2.a aVar, long j2, boolean z) {
        na3 na3;
        String str = z ? "_se" : "_lte";
        na3 c2 = l().c(aVar.s(), str);
        if (c2 == null || c2.e == null) {
            na3 = new na3(aVar.s(), "auto", str, this.i.zzm().b(), Long.valueOf(j2));
        } else {
            na3 = new na3(aVar.s(), "auto", str, this.i.zzm().b(), Long.valueOf(((Long) c2.e).longValue() + j2));
        }
        uj2.a A = uj2.A();
        A.a(str);
        A.a(this.i.zzm().b());
        A.b(((Long) na3.e).longValue());
        uj2 uj2 = (uj2) A.i();
        boolean z2 = false;
        int a2 = ia3.a(aVar, str);
        if (a2 >= 0) {
            aVar.a(a2, uj2);
            z2 = true;
        }
        if (!z2) {
            aVar.a(uj2);
        }
        if (j2 > 0) {
            l().a(na3);
            this.i.b().A().a("Updated engagement user property. scope, value", z ? "session-scoped" : "lifetime", na3.e);
        }
    }

    @DexIgnore
    public final boolean a(mj2.a aVar, mj2.a aVar2) {
        String str;
        w12.a("_e".equals(aVar.l()));
        o();
        oj2 a2 = ia3.a((mj2) aVar.i(), "_sc");
        String str2 = null;
        if (a2 == null) {
            str = null;
        } else {
            str = a2.p();
        }
        o();
        oj2 a3 = ia3.a((mj2) aVar2.i(), "_pc");
        if (a3 != null) {
            str2 = a3.p();
        }
        if (str2 == null || !str2.equals(str)) {
            return false;
        }
        b(aVar, aVar2);
        return true;
    }

    @DexIgnore
    public static void a(mj2.a aVar, String str) {
        List<oj2> j2 = aVar.j();
        for (int i2 = 0; i2 < j2.size(); i2++) {
            if (str.equals(j2.get(i2).n())) {
                aVar.b(i2);
                return;
            }
        }
    }

    @DexIgnore
    public static void a(mj2.a aVar, int i2, String str) {
        List<oj2> j2 = aVar.j();
        int i3 = 0;
        while (i3 < j2.size()) {
            if (!"_err".equals(j2.get(i3).n())) {
                i3++;
            } else {
                return;
            }
        }
        oj2.a y2 = oj2.y();
        y2.a("_err");
        y2.a(Long.valueOf((long) i2).longValue());
        oj2.a y3 = oj2.y();
        y3.a("_ev");
        y3.b(str);
        aVar.a((oj2) y2.i());
        aVar.a((oj2) y3.i());
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void a(int i2, Throwable th, byte[] bArr, String str) {
        yz2 l2;
        y();
        r();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.r = false;
                f();
                throw th2;
            }
        }
        List<Long> list = this.v;
        this.v = null;
        boolean z = true;
        if ((i2 == 200 || i2 == 204) && th == null) {
            try {
                this.i.q().e.a(this.i.zzm().b());
                this.i.q().f.a(0);
                B();
                this.i.b().B().a("Successful upload. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                l().z();
                try {
                    for (Long next : list) {
                        try {
                            l2 = l();
                            long longValue = next.longValue();
                            l2.g();
                            l2.r();
                            if (l2.v().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e2) {
                            l2.b().t().a("Failed to delete a bundle in a queue table", e2);
                            throw e2;
                        } catch (SQLiteException e3) {
                            if (this.w == null || !this.w.contains(next)) {
                                throw e3;
                            }
                        }
                    }
                    l().u();
                    l().A();
                    this.w = null;
                    if (!k().u() || !A()) {
                        this.x = -1;
                        B();
                    } else {
                        s();
                    }
                    this.m = 0;
                } catch (Throwable th3) {
                    l().A();
                    throw th3;
                }
            } catch (SQLiteException e4) {
                this.i.b().t().a("Database error while trying to delete uploaded bundles", e4);
                this.m = this.i.zzm().c();
                this.i.b().B().a("Disable upload, time", Long.valueOf(this.m));
            }
        } else {
            this.i.b().B().a("Network upload failed. Will retry later. code, error", Integer.valueOf(i2), th);
            this.i.q().f.a(this.i.zzm().b());
            if (i2 != 503) {
                if (i2 != 429) {
                    z = false;
                }
            }
            if (z) {
                this.i.q().g.a(this.i.zzm().b());
            }
            l().a(list);
            B();
        }
        this.r = false;
        f();
    }

    @DexIgnore
    public final void a(a63 a63) {
        y();
        if (!TextUtils.isEmpty(a63.n()) || !TextUtils.isEmpty(a63.o())) {
            cb3 p2 = this.i.p();
            Uri.Builder builder = new Uri.Builder();
            String n2 = a63.n();
            if (TextUtils.isEmpty(n2)) {
                n2 = a63.o();
            }
            p4 p4Var = null;
            Uri.Builder encodedAuthority = builder.scheme(l03.j.a(null)).encodedAuthority(l03.k.a(null));
            String valueOf = String.valueOf(n2);
            encodedAuthority.path(valueOf.length() != 0 ? "config/app/".concat(valueOf) : new String("config/app/")).appendQueryParameter("app_instance_id", a63.m()).appendQueryParameter("platform", "android").appendQueryParameter("gmp_version", String.valueOf(p2.n()));
            String uri = builder.build().toString();
            try {
                URL url = new URL(uri);
                this.i.b().B().a("Fetching remote configuration", a63.l());
                gj2 a2 = j().a(a63.l());
                String b2 = j().b(a63.l());
                if (a2 != null && !TextUtils.isEmpty(b2)) {
                    p4Var = new p4();
                    p4Var.put("If-Modified-Since", b2);
                }
                this.q = true;
                x43 k2 = k();
                String l2 = a63.l();
                fa3 fa3 = new fa3(this);
                k2.g();
                k2.r();
                w12.a(url);
                w12.a(fa3);
                k2.a().b((Runnable) new c53(k2, l2, url, (byte[]) null, p4Var, fa3));
            } catch (MalformedURLException unused) {
                this.i.b().t().a("Failed to parse config URL. Not fetching. appId", t43.a(a63.l()), uri);
            }
        } else {
            a(a63.l(), 204, (Throwable) null, (byte[]) null, (Map<String, List<String>>) null);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:59:0x013a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x014a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0172 A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0176 A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    public final void a(String str, int i2, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        y();
        r();
        w12.b(str);
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.q = false;
                f();
                throw th2;
            }
        }
        this.i.b().B().a("onConfigFetched. Response size", Integer.valueOf(bArr.length));
        l().z();
        a63 b2 = l().b(str);
        boolean z = true;
        boolean z2 = (i2 == 200 || i2 == 204 || i2 == 304) && th == null;
        if (b2 == null) {
            this.i.b().w().a("App does not exist in onConfigFetched. appId", t43.a(str));
        } else {
            if (!z2) {
                if (i2 != 404) {
                    b2.i(this.i.zzm().b());
                    l().a(b2);
                    this.i.b().B().a("Fetching config failed. code, error", Integer.valueOf(i2), th);
                    j().c(str);
                    this.i.q().f.a(this.i.zzm().b());
                    if (i2 != 503) {
                        if (i2 != 429) {
                            z = false;
                        }
                    }
                    if (z) {
                        this.i.q().g.a(this.i.zzm().b());
                    }
                    B();
                }
            }
            List list = map != null ? map.get("Last-Modified") : null;
            String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
            if (i2 != 404) {
                if (i2 != 304) {
                    if (!j().a(str, bArr, str2)) {
                        l().A();
                        this.q = false;
                        f();
                        return;
                    }
                    b2.h(this.i.zzm().b());
                    l().a(b2);
                    if (i2 != 404) {
                        this.i.b().y().a("Config not found. Using empty config. appId", str);
                    } else {
                        this.i.b().B().a("Successfully fetched config. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                    }
                    if (!k().u() || !A()) {
                        B();
                    } else {
                        s();
                    }
                }
            }
            if (j().a(str) == null && !j().a(str, (byte[]) null, (String) null)) {
                l().A();
                this.q = false;
                f();
                return;
            }
            b2.h(this.i.zzm().b());
            l().a(b2);
            if (i2 != 404) {
            }
            if (!k().u() || !A()) {
            }
        }
        l().u();
        l().A();
        this.q = false;
        f();
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        y();
        if (this.n == null) {
            this.n = new ArrayList();
        }
        this.n.add(runnable);
    }

    @DexIgnore
    public final int a(FileChannel fileChannel) {
        y();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.b().t().a("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    this.i.b().w().a("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e2) {
            this.i.b().t().a("Failed to read from channel", e2);
            return 0;
        }
    }

    @DexIgnore
    public final boolean a(int i2, FileChannel fileChannel) {
        y();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.i.b().t().a("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i2);
        allocate.flip();
        try {
            fileChannel.truncate(0);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                this.i.b().t().a("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e2) {
            this.i.b().t().a("Failed to write to channel", e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(ra3 ra3) {
        if (this.v != null) {
            this.w = new ArrayList();
            this.w.addAll(this.v);
        }
        yz2 l2 = l();
        String str = ra3.a;
        w12.b(str);
        l2.g();
        l2.r();
        try {
            SQLiteDatabase v2 = l2.v();
            String[] strArr = {str};
            int delete = v2.delete("apps", "app_id=?", strArr) + 0 + v2.delete("events", "app_id=?", strArr) + v2.delete("user_attributes", "app_id=?", strArr) + v2.delete("conditional_properties", "app_id=?", strArr) + v2.delete("raw_events", "app_id=?", strArr) + v2.delete("raw_events_metadata", "app_id=?", strArr) + v2.delete("queue", "app_id=?", strArr) + v2.delete("audience_filter_values", "app_id=?", strArr) + v2.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                l2.b().B().a("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e2) {
            l2.b().t().a("Error resetting analytics data. appId, error", t43.a(str), e2);
        }
        if (!ks2.a() || !this.i.p().a(l03.P0)) {
            ra3 a2 = a(this.i.c(), ra3.a, ra3.b, ra3.h, ra3.s, ra3.t, ra3.q, ra3.v, ra3.z);
            if (ra3.h) {
                b(a2);
            }
        } else if (ra3.h) {
            b(ra3);
        }
    }

    @DexIgnore
    public final ra3 a(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j2, String str3, String str4) {
        String str5;
        String str6;
        int i2;
        String str7 = str;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            this.i.b().t().a("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str5 = packageManager.getInstallerPackageName(str7);
        } catch (IllegalArgumentException unused) {
            this.i.b().t().a("Error retrieving installer package name. appId", t43.a(str));
            str5 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        if (str5 == null) {
            str5 = "manual_install";
        } else if ("com.android.vending".equals(str5)) {
            str5 = "";
        }
        String str8 = str5;
        try {
            PackageInfo b2 = g52.b(context).b(str7, 0);
            if (b2 != null) {
                CharSequence b3 = g52.b(context).b(str7);
                if (!TextUtils.isEmpty(b3)) {
                    String charSequence = b3.toString();
                }
                str6 = b2.versionName;
                i2 = b2.versionCode;
            } else {
                str6 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                i2 = RecyclerView.UNDEFINED_DURATION;
            }
            this.i.d();
            return new ra3(str, str2, str6, (long) i2, str8, this.i.p().n(), this.i.w().a(context, str7), (String) null, z, false, "", 0, this.i.p().i(str7) ? j2 : 0, 0, z2, z3, false, str3, (Boolean) null, 0, (List<String>) null, (!bt2.a() || !this.i.p().e(str7, l03.K0)) ? null : str4);
        } catch (PackageManager.NameNotFoundException unused2) {
            this.i.b().t().a("Error retrieving newly installed package info. appId, appName", t43.a(str), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            return null;
        }
    }

    @DexIgnore
    public final void a(la3 la3, ra3 ra3) {
        f03 a2;
        y();
        r();
        if (TextUtils.isEmpty(ra3.b) && TextUtils.isEmpty(ra3.v)) {
            return;
        }
        if (!ra3.h) {
            c(ra3);
            return;
        }
        int b2 = this.i.w().b(la3.b);
        if (b2 != 0) {
            this.i.w();
            String a3 = ma3.a(la3.b, 24, true);
            String str = la3.b;
            this.i.w().a(ra3.a, b2, "_ev", a3, str != null ? str.length() : 0);
            return;
        }
        int b3 = this.i.w().b(la3.b, la3.zza());
        if (b3 != 0) {
            this.i.w();
            String a4 = ma3.a(la3.b, 24, true);
            Object zza = la3.zza();
            this.i.w().a(ra3.a, b3, "_ev", a4, (zza == null || (!(zza instanceof String) && !(zza instanceof CharSequence))) ? 0 : String.valueOf(zza).length());
            return;
        }
        Object c2 = this.i.w().c(la3.b, la3.zza());
        if (c2 != null) {
            if ("_sid".equals(la3.b) && this.i.p().m(ra3.a)) {
                long j2 = la3.c;
                String str2 = la3.f;
                long j3 = 0;
                na3 c3 = l().c(ra3.a, "_sno");
                if (c3 != null) {
                    Object obj = c3.e;
                    if (obj instanceof Long) {
                        j3 = ((Long) obj).longValue();
                        a(new la3("_sno", j2, Long.valueOf(j3 + 1), str2), ra3);
                    }
                }
                if (c3 != null) {
                    this.i.b().w().a("Retrieved last session number from database does not contain a valid (long) value", c3.e);
                }
                if (this.i.p().e(ra3.a, l03.b0) && (a2 = l().a(ra3.a, "_s")) != null) {
                    j3 = a2.c;
                    this.i.b().B().a("Backfill the session number. Last used session number", Long.valueOf(j3));
                }
                a(new la3("_sno", j2, Long.valueOf(j3 + 1), str2), ra3);
            }
            na3 na3 = new na3(ra3.a, la3.f, la3.b, la3.c, c2);
            this.i.b().A().a("Setting user property", this.i.x().c(na3.c), c2);
            l().z();
            try {
                c(ra3);
                boolean a5 = l().a(na3);
                l().u();
                if (a5) {
                    this.i.b().A().a("User property set", this.i.x().c(na3.c), na3.e);
                } else {
                    this.i.b().t().a("Too many unique user properties are set. Ignoring user property", this.i.x().c(na3.c), na3.e);
                    this.i.w().a(ra3.a, 9, (String) null, (String) null, 0);
                }
            } finally {
                l().A();
            }
        }
    }

    @DexIgnore
    public final void a(aa3 aa3) {
        this.o++;
    }

    @DexIgnore
    public final ra3 a(String str) {
        String str2 = str;
        a63 b2 = l().b(str2);
        if (b2 == null || TextUtils.isEmpty(b2.u())) {
            this.i.b().A().a("No app data available; dropping", str2);
            return null;
        }
        Boolean b3 = b(b2);
        if (b3 == null || b3.booleanValue()) {
            return new ra3(str, b2.n(), b2.u(), b2.v(), b2.w(), b2.x(), b2.y(), (String) null, b2.A(), false, b2.r(), b2.f(), 0, 0, b2.g(), b2.h(), false, b2.o(), b2.i(), b2.z(), b2.j(), (!bt2.a() || !this.i.p().e(str2, l03.K0)) ? null : b2.p());
        }
        this.i.b().t().a("App version does not match; dropping. appId", t43.a(str));
        return null;
    }

    @DexIgnore
    public final void a(ab3 ab3) {
        ra3 a2 = a(ab3.a);
        if (a2 != null) {
            a(ab3, a2);
        }
    }

    @DexIgnore
    public final void a(ab3 ab3, ra3 ra3) {
        w12.a(ab3);
        w12.b(ab3.a);
        w12.a(ab3.b);
        w12.a(ab3.c);
        w12.b(ab3.c.b);
        y();
        r();
        if (TextUtils.isEmpty(ra3.b) && TextUtils.isEmpty(ra3.v)) {
            return;
        }
        if (!ra3.h) {
            c(ra3);
            return;
        }
        ab3 ab32 = new ab3(ab3);
        boolean z = false;
        ab32.e = false;
        l().z();
        try {
            ab3 d2 = l().d(ab32.a, ab32.c.b);
            if (d2 != null && !d2.b.equals(ab32.b)) {
                this.i.b().w().a("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.i.x().c(ab32.c.b), ab32.b, d2.b);
            }
            if (d2 != null && d2.e) {
                ab32.b = d2.b;
                ab32.d = d2.d;
                ab32.h = d2.h;
                ab32.f = d2.f;
                ab32.i = d2.i;
                ab32.e = d2.e;
                ab32.c = new la3(ab32.c.b, d2.c.c, ab32.c.zza(), d2.c.f);
            } else if (TextUtils.isEmpty(ab32.f)) {
                ab32.c = new la3(ab32.c.b, ab32.d, ab32.c.zza(), ab32.c.f);
                ab32.e = true;
                z = true;
            }
            if (ab32.e) {
                la3 la3 = ab32.c;
                na3 na3 = new na3(ab32.a, ab32.b, la3.b, la3.c, la3.zza());
                if (l().a(na3)) {
                    this.i.b().A().a("User property updated immediately", ab32.a, this.i.x().c(na3.c), na3.e);
                } else {
                    this.i.b().t().a("(2)Too many active user properties, ignoring", t43.a(ab32.a), this.i.x().c(na3.c), na3.e);
                }
                if (z && ab32.i != null) {
                    b(new j03(ab32.i, ab32.d), ra3);
                }
            }
            if (l().a(ab32)) {
                this.i.b().A().a("Conditional property added", ab32.a, this.i.x().c(ab32.c.b), ab32.c.zza());
            } else {
                this.i.b().t().a("Too many conditional properties, ignoring", t43.a(ab32.a), this.i.x().c(ab32.c.b), ab32.c.zza());
            }
            l().u();
        } finally {
            l().A();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x018e  */
    public final a63 a(ra3 ra3, a63 a63, String str) {
        boolean z;
        if (a63 == null) {
            a63 = new a63(this.i, ra3.a);
            a63.a(this.i.w().v());
            a63.e(str);
        } else if (!str.equals(a63.q())) {
            a63.e(str);
            a63.a(this.i.w().v());
        } else {
            z = false;
            if (!TextUtils.equals(ra3.b, a63.n())) {
                a63.b(ra3.b);
                z = true;
            }
            if (!TextUtils.equals(ra3.v, a63.o())) {
                a63.c(ra3.v);
                z = true;
            }
            if (bt2.a() && this.i.p().e(a63.l(), l03.K0) && !TextUtils.equals(ra3.z, a63.p())) {
                a63.d(ra3.z);
                z = true;
            }
            if (!TextUtils.isEmpty(ra3.o) && !ra3.o.equals(a63.r())) {
                a63.f(ra3.o);
                z = true;
            }
            long j2 = ra3.e;
            if (!(j2 == 0 || j2 == a63.x())) {
                a63.d(ra3.e);
                z = true;
            }
            if (!TextUtils.isEmpty(ra3.c) && !ra3.c.equals(a63.u())) {
                a63.g(ra3.c);
                z = true;
            }
            if (ra3.j != a63.v()) {
                a63.c(ra3.j);
                z = true;
            }
            String str2 = ra3.d;
            if (str2 != null && !str2.equals(a63.w())) {
                a63.h(ra3.d);
                z = true;
            }
            if (ra3.f != a63.y()) {
                a63.e(ra3.f);
                z = true;
            }
            if (ra3.h != a63.A()) {
                a63.a(ra3.h);
                z = true;
            }
            if (!TextUtils.isEmpty(ra3.g) && !ra3.g.equals(a63.d())) {
                a63.i(ra3.g);
                z = true;
            }
            if (ra3.p != a63.f()) {
                a63.p(ra3.p);
                z = true;
            }
            if (ra3.s != a63.g()) {
                a63.b(ra3.s);
                z = true;
            }
            if (ra3.t != a63.h()) {
                a63.c(ra3.t);
                z = true;
            }
            if (this.i.p().e(ra3.a, l03.i0) && ra3.w != a63.i()) {
                a63.a(ra3.w);
                z = true;
            }
            long j3 = ra3.x;
            if (!(j3 == 0 || j3 == a63.z())) {
                a63.f(ra3.x);
                z = true;
            }
            if (z) {
                l().a(a63);
            }
            return a63;
        }
        z = true;
        if (!TextUtils.equals(ra3.b, a63.n())) {
        }
        if (!TextUtils.equals(ra3.v, a63.o())) {
        }
        a63.d(ra3.z);
        z = true;
        a63.f(ra3.o);
        z = true;
        long j22 = ra3.e;
        a63.d(ra3.e);
        z = true;
        a63.g(ra3.c);
        z = true;
        if (ra3.j != a63.v()) {
        }
        String str22 = ra3.d;
        a63.h(ra3.d);
        z = true;
        if (ra3.f != a63.y()) {
        }
        if (ra3.h != a63.A()) {
        }
        a63.i(ra3.g);
        z = true;
        if (ra3.p != a63.f()) {
        }
        if (ra3.s != a63.g()) {
        }
        if (ra3.t != a63.h()) {
        }
        a63.a(ra3.w);
        z = true;
        long j32 = ra3.x;
        a63.f(ra3.x);
        z = true;
        if (z) {
        }
        return a63;
    }

    @DexIgnore
    public final void a(boolean z) {
        B();
    }
}
