package com.fossil;

import com.fossil.m24;
import com.fossil.nw5;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr5$d$b implements m24.e<nw5.d, nw5.b> {
    @DexIgnore
    public /* final */ /* synthetic */ ProfileSetupPresenter.d a;

    @DexIgnore
    public lr5$d$b(ProfileSetupPresenter.d dVar) {
        this.a = dVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(GetRecommendedGoalUseCase.d dVar) {
        wg6.b(dVar, "responseValue");
        ProfileSetupPresenter.d dVar2 = this.a;
        dVar2.this$0.e(dVar2.$service);
    }

    @DexIgnore
    public void a(GetRecommendedGoalUseCase.b bVar) {
        wg6.b(bVar, "errorValue");
        ProfileSetupPresenter.d dVar = this.a;
        dVar.this$0.e(dVar.$service);
    }
}
