package com.fossil;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jj4 {
    @DexIgnore
    public static mj4 a(Context context) {
        return wq.d(context);
    }

    @DexIgnore
    public static mj4 a(Fragment fragment) {
        return wq.a(fragment);
    }

    @DexIgnore
    public static mj4 a(View view) {
        return wq.a(view);
    }
}
