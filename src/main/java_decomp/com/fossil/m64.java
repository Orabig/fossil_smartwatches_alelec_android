package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m64 extends l64 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j L; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray M; // = new SparseIntArray();
    @DexIgnore
    public long K;

    /*
    static {
        M.put(2131362561, 1);
        M.put(2131362565, 2);
        M.put(2131362245, 3);
        M.put(2131362443, 4);
        M.put(2131362237, 5);
        M.put(2131362390, 6);
        M.put(2131362617, 7);
        M.put(2131362414, 8);
        M.put(2131362988, 9);
        M.put(2131362029, 10);
        M.put(2131362153, 11);
        M.put(2131362151, 12);
        M.put(2131362155, 13);
        M.put(2131362156, 14);
        M.put(2131362154, 15);
        M.put(2131362150, 16);
        M.put(2131362152, 17);
        M.put(2131363265, 18);
        M.put(2131362756, 19);
        M.put(2131362758, 20);
        M.put(2131362757, 21);
        M.put(2131362223, 22);
    }
    */

    @DexIgnore
    public m64(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 23, L, M));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.K = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.K != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.K = 1;
        }
        g();
    }

    @DexIgnore
    public m64(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[10], objArr[16], objArr[12], objArr[17], objArr[11], objArr[15], objArr[13], objArr[14], objArr[22], objArr[5], objArr[3], objArr[6], objArr[8], objArr[4], objArr[1], objArr[2], objArr[7], objArr[19], objArr[21], objArr[20], objArr[0], objArr[9], objArr[18]);
        this.K = -1;
        this.I.setTag((Object) null);
        a(view);
        f();
    }
}
