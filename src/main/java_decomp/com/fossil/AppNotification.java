package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.NotificationFlag;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppNotification extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ NotificationType a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public long c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ NotificationFlag[] i;
    @DexIgnore
    public /* final */ long j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<p70> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new AppNotification(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new AppNotification[i];
        }
    }

    @DexIgnore
    public AppNotification(NotificationType notificationType, int i2, String str, String str2, String str3, int i3, String str4, NotificationFlag[] notificationFlagArr, long j2) {
        this.a = notificationType;
        this.b = i2;
        this.d = str;
        h51 h51 = h51.a;
        byte[] bytes = str.getBytes(mi0.A.f());
        wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
        this.c = h51.a(md6.a(bytes, (byte) 0), q11.CRC32);
        this.e = cw0.a(str2, 99, (Charset) null, (CodingErrorAction) null, 6);
        this.f = cw0.a(str3, 97, (Charset) null, (CodingErrorAction) null, 6);
        this.g = i3;
        this.h = cw0.a(str4, 249, (Charset) null, (CodingErrorAction) null, 6);
        this.i = notificationFlagArr;
        this.j = j2;
    }

    @DexIgnore
    public JSONObject a() {
        String str = this.h;
        Charset f2 = mi0.A.f();
        if (str != null) {
            byte[] bytes = str.getBytes(f2);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            JSONObject a2 = cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.TYPE, (Object) cw0.a((Enum<?>) this.a)), bm0.UID, (Object) Integer.valueOf(this.b)), bm0.APP_PACKAGE_NAME, (Object) this.d), bm0.APP_BUNDLE_CRC, (Object) cw0.a((int) this.c)), bm0.g, (Object) this.e), bm0.SENDER, (Object) this.f), bm0.SENDER_ID, (Object) Integer.valueOf(this.g)), bm0.MESSAGE_LENGTH, (Object) Integer.valueOf(this.h.length())), bm0.MESSAGE_CRC, (Object) cw0.a((int) h51.a.a(bytes, q11.CRC32)));
            bm0 bm0 = bm0.FLAGS;
            NotificationFlag[] notificationFlagArr = this.i;
            JSONArray jSONArray = new JSONArray();
            for (NotificationFlag a3 : notificationFlagArr) {
                jSONArray.put(cw0.a((Enum<?>) a3));
            }
            return cw0.a(cw0.a(a2, bm0, (Object) jSONArray), bm0.RECEIVED_TIMESTAMP_IN_SECOND, (Object) Float.valueOf(((float) this.j) / 1000.0f));
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte[] b() {
        byte b2 = (byte) 12;
        byte b3 = this.a.b();
        byte b4 = 0;
        for (NotificationFlag a2 : this.i) {
            b4 = (byte) (b4 | a2.a());
        }
        byte b5 = (byte) 4;
        String a3 = cw0.a(this.e);
        Charset f2 = mi0.A.f();
        if (a3 != null) {
            byte[] bytes = a3.getBytes(f2);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            String a4 = cw0.a(this.f);
            Charset f3 = mi0.A.f();
            if (a4 != null) {
                byte[] bytes2 = a4.getBytes(f3);
                wg6.a(bytes2, "(this as java.lang.String).getBytes(charset)");
                String a5 = cw0.a(this.h);
                Charset f4 = mi0.A.f();
                if (a5 != null) {
                    byte[] bytes3 = a5.getBytes(f4);
                    wg6.a(bytes3, "(this as java.lang.String).getBytes(charset)");
                    short length = (short) (b2 + b5 + b5 + bytes.length + bytes2.length + b5 + bytes3.length + b5);
                    ByteBuffer allocate = ByteBuffer.allocate(length);
                    wg6.a(allocate, "ByteBuffer.allocate(totalLen.toInt())");
                    allocate.order(ByteOrder.LITTLE_ENDIAN);
                    allocate.putShort(length);
                    allocate.put(b2);
                    allocate.put(b3);
                    allocate.put(b4);
                    allocate.put(b5);
                    allocate.put(b5);
                    allocate.put((byte) bytes.length);
                    allocate.put((byte) bytes2.length);
                    allocate.put((byte) bytes3.length);
                    allocate.put(b5);
                    allocate.put(b5);
                    allocate.putInt(this.b);
                    allocate.putInt((int) this.c);
                    allocate.put(bytes);
                    allocate.put(bytes2);
                    allocate.put(bytes3);
                    allocate.putInt(this.g);
                    allocate.putInt((int) (this.j / ((long) 1000)));
                    byte[] array = allocate.array();
                    wg6.a(array, "notificationData.array()");
                    return array;
                }
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
            throw new rc6("null cannot be cast to non-null type java.lang.String");
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(AppNotification.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            AppNotification appNotification = (AppNotification) obj;
            return this.a == appNotification.a && this.b == appNotification.b && this.c == appNotification.c && !(wg6.a(this.e, appNotification.e) ^ true) && !(wg6.a(this.f, appNotification.f) ^ true) && this.g == appNotification.g && !(wg6.a(this.h, appNotification.h) ^ true) && Arrays.equals(this.i, appNotification.i) && this.j == appNotification.j;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotification");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.c;
    }

    @DexIgnore
    public final String getAppPackageName() {
        return this.d;
    }

    @DexIgnore
    public final NotificationFlag[] getFlags() {
        return this.i;
    }

    @DexIgnore
    public final String getMessage() {
        return this.h;
    }

    @DexIgnore
    public final long getReceivedTimestampInMilliSecond() {
        return this.j;
    }

    @DexIgnore
    public final String getSender() {
        return this.f;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.g;
    }

    @DexIgnore
    public final String getTitle() {
        return this.e;
    }

    @DexIgnore
    public final NotificationType getType() {
        return this.a;
    }

    @DexIgnore
    public final int getUid() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Long.valueOf(this.c).hashCode();
        int hashCode2 = this.e.hashCode();
        int hashCode3 = this.f.hashCode();
        int hashCode4 = Integer.valueOf(this.g).hashCode();
        int hashCode5 = this.h.hashCode();
        return Long.valueOf(this.j).hashCode() + ((((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (((this.a.hashCode() * 31) + this.b) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + Arrays.hashCode(this.i)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeSerializable(this.a);
        parcel.writeInt(this.b);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
        NotificationFlag[] notificationFlagArr = this.i;
        ArrayList arrayList = new ArrayList(notificationFlagArr.length);
        for (NotificationFlag name : notificationFlagArr) {
            arrayList.add(name.name());
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            parcel.writeStringArray((String[]) array);
            parcel.writeLong(this.j);
            parcel.writeLong(this.c);
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ AppNotification(Parcel parcel, qg6 qg6) {
        this(r1, r2, r3, r4, r5, r6, r7, r0.a(r8), parcel.readLong());
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            NotificationType notificationType = (NotificationType) readSerializable;
            int readInt = parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        wg6.a(readString3, "parcel.readString()!!");
                        int readInt2 = parcel.readInt();
                        String readString4 = parcel.readString();
                        if (readString4 != null) {
                            wg6.a(readString4, "parcel.readString()!!");
                            NotificationFlag.a aVar = NotificationFlag.c;
                            String[] createStringArray = parcel.createStringArray();
                            if (createStringArray != null) {
                                wg6.a(createStringArray, "parcel.createStringArray()!!");
                                this.c = parcel.readLong();
                                return;
                            }
                            wg6.a();
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationType");
    }
}
