package com.fossil;

import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.bw3;
import com.fossil.zv3;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aw3 {
    @DexIgnore
    public static /* final */ vv3 g; // = new a();
    @DexIgnore
    public static /* final */ Logger h; // = Logger.getLogger(aw3.class.getName());
    @DexIgnore
    public static /* final */ Map<Character, Character> i;
    @DexIgnore
    public static /* final */ Map<Character, Character> j;
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Pattern l; // = Pattern.compile("[+\uff0b]+");
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e]+");
    @DexIgnore
    public static /* final */ Pattern n; // = Pattern.compile("(\\p{Nd})");
    @DexIgnore
    public static /* final */ Pattern o; // = Pattern.compile("[+\uff0b\\p{Nd}]");
    @DexIgnore
    public static /* final */ Pattern p; // = Pattern.compile("[\\\\/] *x");
    @DexIgnore
    public static /* final */ Pattern q; // = Pattern.compile("[[\\P{N}&&\\P{L}]&&[^#]]+$");
    @DexIgnore
    public static /* final */ Pattern r; // = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ String t; // = c("x\uff58#\uff03~\uff5e".length() != 0 ? ",".concat("x\uff58#\uff03~\uff5e") : new String(","));
    @DexIgnore
    public static /* final */ Pattern u;
    @DexIgnore
    public static /* final */ Pattern v;
    @DexIgnore
    public static /* final */ Pattern w; // = Pattern.compile("(\\$\\d)");
    @DexIgnore
    public static /* final */ Pattern x; // = Pattern.compile("\\$CC");
    @DexIgnore
    public static aw3 y; // = null;
    @DexIgnore
    public /* final */ xv3 a;
    @DexIgnore
    public /* final */ Map<Integer, List<String>> b;
    @DexIgnore
    public /* final */ Set<String> c; // = new HashSet(35);
    @DexIgnore
    public /* final */ cw3 d; // = new cw3(100);
    @DexIgnore
    public /* final */ Set<String> e; // = new HashSet(320);
    @DexIgnore
    public /* final */ Set<Integer> f; // = new HashSet();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements vv3 {
        @DexIgnore
        public InputStream a(String str) {
            return aw3.class.getResourceAsStream(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class b {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[bw3.a.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[c.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] c; // = new int[d.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(41:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|(2:37|38)|39|41|42|43|44|45|46|47|48|50) */
        /* JADX WARNING: Can't wrap try/catch for region: R(42:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|(2:37|38)|39|41|42|43|44|45|46|47|48|50) */
        /* JADX WARNING: Can't wrap try/catch for region: R(43:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|37|38|39|41|42|43|44|45|46|47|48|50) */
        /* JADX WARNING: Can't wrap try/catch for region: R(44:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|31|32|33|34|35|36|37|38|39|41|42|43|44|45|46|47|48|50) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0035 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0099 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00a3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00ad */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00ca */
        /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00d4 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00de */
        /*
        static {
            try {
                c[d.PREMIUM_RATE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                c[d.TOLL_FREE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                c[d.MOBILE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            c[d.FIXED_LINE.ordinal()] = 4;
            c[d.FIXED_LINE_OR_MOBILE.ordinal()] = 5;
            c[d.SHARED_COST.ordinal()] = 6;
            c[d.VOIP.ordinal()] = 7;
            c[d.PERSONAL_NUMBER.ordinal()] = 8;
            c[d.PAGER.ordinal()] = 9;
            c[d.UAN.ordinal()] = 10;
            try {
                c[d.VOICEMAIL.ordinal()] = 11;
            } catch (NoSuchFieldError unused4) {
            }
            b[c.E164.ordinal()] = 1;
            b[c.INTERNATIONAL.ordinal()] = 2;
            b[c.RFC3966.ordinal()] = 3;
            try {
                b[c.NATIONAL.ordinal()] = 4;
            } catch (NoSuchFieldError unused5) {
            }
            a[bw3.a.FROM_NUMBER_WITH_PLUS_SIGN.ordinal()] = 1;
            a[bw3.a.FROM_NUMBER_WITH_IDD.ordinal()] = 2;
            a[bw3.a.FROM_NUMBER_WITHOUT_PLUS_SIGN.ordinal()] = 3;
            try {
                a[bw3.a.FROM_DEFAULT_COUNTRY.ordinal()] = 4;
            } catch (NoSuchFieldError unused6) {
            }
        }
        */
    }

    @DexIgnore
    public enum c {
        E164,
        INTERNATIONAL,
        NATIONAL,
        RFC3966
    }

    @DexIgnore
    public enum d {
        FIXED_LINE,
        MOBILE,
        FIXED_LINE_OR_MOBILE,
        TOLL_FREE,
        PREMIUM_RATE,
        SHARED_COST,
        VOIP,
        PERSONAL_NUMBER,
        PAGER,
        UAN,
        VOICEMAIL,
        UNKNOWN
    }

    @DexIgnore
    public enum e {
        IS_POSSIBLE,
        INVALID_COUNTRY_CODE,
        TOO_SHORT,
        TOO_LONG
    }

    /*
    static {
        HashMap hashMap = new HashMap();
        hashMap.put(52, "1");
        hashMap.put(54, CrashDumperPlugin.OPTION_KILL_DEFAULT);
        Collections.unmodifiableMap(hashMap);
        HashSet hashSet = new HashSet();
        hashSet.add(52);
        hashSet.add(54);
        hashSet.add(55);
        Collections.unmodifiableSet(hashSet);
        HashMap hashMap2 = new HashMap();
        hashMap2.put('0', '0');
        hashMap2.put('1', '1');
        hashMap2.put('2', '2');
        hashMap2.put('3', '3');
        hashMap2.put('4', '4');
        hashMap2.put('5', '5');
        hashMap2.put('6', '6');
        hashMap2.put('7', '7');
        hashMap2.put('8', '8');
        hashMap2.put('9', '9');
        HashMap hashMap3 = new HashMap(40);
        hashMap3.put('A', '2');
        hashMap3.put('B', '2');
        hashMap3.put('C', '2');
        hashMap3.put('D', '3');
        hashMap3.put('E', '3');
        hashMap3.put('F', '3');
        hashMap3.put('G', '4');
        hashMap3.put('H', '4');
        hashMap3.put('I', '4');
        hashMap3.put('J', '5');
        hashMap3.put('K', '5');
        hashMap3.put('L', '5');
        hashMap3.put('M', '6');
        hashMap3.put('N', '6');
        hashMap3.put('O', '6');
        hashMap3.put('P', '7');
        hashMap3.put('Q', '7');
        hashMap3.put('R', '7');
        hashMap3.put('S', '7');
        hashMap3.put('T', '8');
        hashMap3.put('U', '8');
        hashMap3.put('V', '8');
        hashMap3.put('W', '9');
        hashMap3.put('X', '9');
        hashMap3.put('Y', '9');
        hashMap3.put('Z', '9');
        i = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap(100);
        hashMap4.putAll(i);
        hashMap4.putAll(hashMap2);
        j = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.putAll(hashMap2);
        hashMap5.put('+', '+');
        hashMap5.put('*', '*');
        Collections.unmodifiableMap(hashMap5);
        HashMap hashMap6 = new HashMap();
        for (Character charValue : i.keySet()) {
            char charValue2 = charValue.charValue();
            hashMap6.put(Character.valueOf(Character.toLowerCase(charValue2)), Character.valueOf(charValue2));
            hashMap6.put(Character.valueOf(charValue2), Character.valueOf(charValue2));
        }
        hashMap6.putAll(hashMap2);
        hashMap6.put('-', '-');
        hashMap6.put(65293, '-');
        hashMap6.put(8208, '-');
        hashMap6.put(8209, '-');
        hashMap6.put(8210, '-');
        hashMap6.put(8211, '-');
        hashMap6.put(8212, '-');
        hashMap6.put(8213, '-');
        hashMap6.put(8722, '-');
        hashMap6.put('/', '/');
        hashMap6.put(65295, '/');
        hashMap6.put(' ', ' ');
        hashMap6.put(12288, ' ');
        hashMap6.put(8288, ' ');
        hashMap6.put('.', '.');
        hashMap6.put(65294, '.');
        Collections.unmodifiableMap(hashMap6);
        Pattern.compile("[\\d]+(?:[~\u2053\u223c\uff5e][\\d]+)?");
        String valueOf = String.valueOf(Arrays.toString(i.keySet().toArray()).replaceAll("[, \\[\\]]", ""));
        String valueOf2 = String.valueOf(Arrays.toString(i.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
        k = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
        String valueOf3 = String.valueOf(String.valueOf(k));
        StringBuilder sb = new StringBuilder("\\p{Nd}{2}|[+\uff0b]*+(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*\\p{Nd}){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*".length() + 2 + valueOf3.length() + "\\p{Nd}".length());
        sb.append("\\p{Nd}{2}|[+\uff0b]*+(?:[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*]*\\p{Nd}){3,}[-x\u2010-\u2015\u2212\u30fc\uff0d-\uff0f \u00a0\u00ad\u200b\u2060\u3000()\uff08\uff09\uff3b\uff3d.\\[\\]/~\u2053\u223c\uff5e*");
        sb.append(valueOf3);
        sb.append("\\p{Nd}");
        sb.append("]*");
        s = sb.toString();
        c("x\uff58#\uff03~\uff5e");
        String valueOf4 = String.valueOf(String.valueOf(t));
        StringBuilder sb2 = new StringBuilder(valueOf4.length() + 5);
        sb2.append("(?:");
        sb2.append(valueOf4);
        sb2.append(")$");
        u = Pattern.compile(sb2.toString(), 66);
        String valueOf5 = String.valueOf(String.valueOf(s));
        String valueOf6 = String.valueOf(String.valueOf(t));
        StringBuilder sb3 = new StringBuilder(valueOf5.length() + 5 + valueOf6.length());
        sb3.append(valueOf5);
        sb3.append("(?:");
        sb3.append(valueOf6);
        sb3.append(")?");
        v = Pattern.compile(sb3.toString(), 66);
        Pattern.compile("(\\D+)");
        Pattern.compile("\\$NP");
        Pattern.compile("\\$FG");
        Pattern.compile("\\(?\\$1\\)?");
    }
    */

    @DexIgnore
    public aw3(xv3 xv3, Map<Integer, List<String>> map) {
        this.a = xv3;
        this.b = map;
        for (Map.Entry next : map.entrySet()) {
            List list = (List) next.getValue();
            if (list.size() != 1 || !"001".equals(list.get(0))) {
                this.e.addAll(list);
            } else {
                this.f.add(next.getKey());
            }
        }
        if (this.e.remove("001")) {
            h.log(Level.WARNING, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))");
        }
        this.c.addAll(map.get(1));
    }

    @DexIgnore
    public static StringBuilder a(String str, boolean z) {
        StringBuilder sb = new StringBuilder(str.length());
        for (char c2 : str.toCharArray()) {
            int digit = Character.digit(c2, 10);
            if (digit != -1) {
                sb.append(digit);
            } else if (z) {
                sb.append(c2);
            }
        }
        return sb;
    }

    @DexIgnore
    public static void b(StringBuilder sb) {
        sb.replace(0, sb.length(), f(sb.toString()));
    }

    @DexIgnore
    public static String c(String str) {
        String valueOf = String.valueOf(String.valueOf(str));
        StringBuilder sb = new StringBuilder(";ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[".length() + 48 + valueOf.length() + "(\\p{Nd}{1,7})".length() + "\\p{Nd}".length());
        sb.append(";ext=(\\p{Nd}{1,7})|[ \u00a0\\t,]*(?:e?xt(?:ensi(?:o\u0301?|\u00f3))?n?|\uff45?\uff58\uff54\uff4e?|[");
        sb.append(valueOf);
        sb.append("]|int|anexo|\uff49\uff4e\uff54)");
        sb.append("[:\\.\uff0e]?[ \u00a0\\t,-]*");
        sb.append("(\\p{Nd}{1,7})");
        sb.append("#?|");
        sb.append("[- ]+(");
        sb.append("\\p{Nd}");
        sb.append("{1,5})#");
        return sb.toString();
    }

    @DexIgnore
    public static String d(String str) {
        Matcher matcher = o.matcher(str);
        if (!matcher.find()) {
            return "";
        }
        String substring = str.substring(matcher.start());
        Matcher matcher2 = q.matcher(substring);
        if (matcher2.find()) {
            substring = substring.substring(0, matcher2.start());
            Logger logger = h;
            Level level = Level.FINER;
            String valueOf = String.valueOf(substring);
            logger.log(level, valueOf.length() != 0 ? "Stripped trailing characters: ".concat(valueOf) : new String("Stripped trailing characters: "));
        }
        Matcher matcher3 = p.matcher(substring);
        return matcher3.find() ? substring.substring(0, matcher3.start()) : substring;
    }

    @DexIgnore
    public static boolean e(String str) {
        if (str.length() < 2) {
            return false;
        }
        return v.matcher(str).matches();
    }

    @DexIgnore
    public static String f(String str) {
        if (r.matcher(str).matches()) {
            return a(str, j, true);
        }
        return g(str);
    }

    @DexIgnore
    public static String g(String str) {
        return a(str, false).toString();
    }

    @DexIgnore
    public final boolean c(int i2) {
        return this.b.containsKey(Integer.valueOf(i2));
    }

    @DexIgnore
    public final boolean b(String str) {
        return str != null && this.e.contains(str);
    }

    @DexIgnore
    public String b(int i2) {
        List list = this.b.get(Integer.valueOf(i2));
        if (list == null) {
            return "ZZ";
        }
        return (String) list.get(0);
    }

    @DexIgnore
    public static String a(String str, Map<Character, Character> map, boolean z) {
        StringBuilder sb = new StringBuilder(str.length());
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            Character ch = map.get(Character.valueOf(Character.toUpperCase(charAt)));
            if (ch != null) {
                sb.append(ch);
            } else if (!z) {
                sb.append(charAt);
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public bw3 b(String str, String str2) throws zv3 {
        bw3 bw3 = new bw3();
        a(str, str2, bw3);
        return bw3;
    }

    @DexIgnore
    public static synchronized void a(aw3 aw3) {
        synchronized (aw3.class) {
            y = aw3;
        }
    }

    @DexIgnore
    public static synchronized aw3 a() {
        aw3 aw3;
        synchronized (aw3.class) {
            if (y == null) {
                a(a(g));
            }
            aw3 = y;
        }
        return aw3;
    }

    @DexIgnore
    public static aw3 a(xv3 xv3) {
        if (xv3 != null) {
            return new aw3(xv3, uv3.a());
        }
        throw new IllegalArgumentException("metadataSource could not be null.");
    }

    @DexIgnore
    public static aw3 a(vv3 vv3) {
        if (vv3 != null) {
            return a((xv3) new yv3(vv3));
        }
        throw new IllegalArgumentException("metadataLoader could not be null.");
    }

    @DexIgnore
    public String a(bw3 bw3, c cVar) {
        if (bw3.getNationalNumber() == 0 && bw3.hasRawInput()) {
            String rawInput = bw3.getRawInput();
            if (rawInput.length() > 0) {
                return rawInput;
            }
        }
        StringBuilder sb = new StringBuilder(20);
        a(bw3, cVar, sb);
        return sb.toString();
    }

    @DexIgnore
    public void a(bw3 bw3, c cVar, StringBuilder sb) {
        sb.setLength(0);
        int countryCode = bw3.getCountryCode();
        String a2 = a(bw3);
        if (cVar == c.E164) {
            sb.append(a2);
            a(countryCode, c.E164, sb);
        } else if (!c(countryCode)) {
            sb.append(a2);
        } else {
            fw3 a3 = a(countryCode, b(countryCode));
            sb.append(a(a2, a3, cVar));
            a(bw3, a3, cVar, sb);
            a(countryCode, cVar, sb);
        }
    }

    @DexIgnore
    public final fw3 a(int i2, String str) {
        return "001".equals(str) ? a(i2) : a(str);
    }

    @DexIgnore
    public String a(bw3 bw3) {
        StringBuilder sb = new StringBuilder();
        if (bw3.isItalianLeadingZero()) {
            char[] cArr = new char[bw3.getNumberOfLeadingZeros()];
            Arrays.fill(cArr, '0');
            sb.append(new String(cArr));
        }
        sb.append(bw3.getNationalNumber());
        return sb.toString();
    }

    @DexIgnore
    public final void a(int i2, c cVar, StringBuilder sb) {
        int i3 = b.b[cVar.ordinal()];
        if (i3 == 1) {
            sb.insert(0, i2).insert(0, '+');
        } else if (i3 == 2) {
            sb.insert(0, " ").insert(0, i2).insert(0, '+');
        } else if (i3 == 3) {
            sb.insert(0, "-").insert(0, i2).insert(0, '+').insert(0, "tel:");
        }
    }

    @DexIgnore
    public final String a(String str, fw3 fw3, c cVar) {
        return a(str, fw3, cVar, (String) null);
    }

    @DexIgnore
    public final String a(String str, fw3 fw3, c cVar, String str2) {
        ew3[] ew3Arr = fw3.w;
        if (ew3Arr.length == 0 || cVar == c.NATIONAL) {
            ew3Arr = fw3.v;
        }
        ew3 a2 = a(ew3Arr, str);
        return a2 == null ? str : a(str, a2, cVar, str2);
    }

    @DexIgnore
    public ew3 a(ew3[] ew3Arr, String str) {
        for (ew3 ew3 : ew3Arr) {
            String[] strArr = ew3.c;
            int length = strArr.length;
            if ((length == 0 || this.d.a(strArr[length - 1]).matcher(str).lookingAt()) && this.d.a(ew3.a).matcher(str).matches()) {
                return ew3;
            }
        }
        return null;
    }

    @DexIgnore
    public final String a(String str, ew3 ew3, c cVar, String str2) {
        String str3;
        String str4 = ew3.b;
        Matcher matcher = this.d.a(ew3.a).matcher(str);
        if (cVar != c.NATIONAL || str2 == null || str2.length() <= 0 || ew3.e.length() <= 0) {
            String str5 = ew3.d;
            if (cVar != c.NATIONAL || str5 == null || str5.length() <= 0) {
                str3 = matcher.replaceAll(str4);
            } else {
                str3 = matcher.replaceAll(w.matcher(str4).replaceFirst(str5));
            }
        } else {
            str3 = matcher.replaceAll(w.matcher(str4).replaceFirst(x.matcher(ew3.e).replaceFirst(str2)));
        }
        if (cVar != c.RFC3966) {
            return str3;
        }
        Matcher matcher2 = m.matcher(str3);
        if (matcher2.lookingAt()) {
            str3 = matcher2.replaceFirst("");
        }
        return matcher2.reset(str3).replaceAll("-");
    }

    @DexIgnore
    public final void a(bw3 bw3, fw3 fw3, c cVar, StringBuilder sb) {
        if (bw3.hasExtension() && bw3.getExtension().length() > 0) {
            if (cVar == c.RFC3966) {
                sb.append(";ext=");
                sb.append(bw3.getExtension());
            } else if (!fw3.s.equals("")) {
                sb.append(fw3.s);
                sb.append(bw3.getExtension());
            } else {
                sb.append(" ext. ");
                sb.append(bw3.getExtension());
            }
        }
    }

    @DexIgnore
    public fw3 a(String str) {
        if (!b(str)) {
            return null;
        }
        return this.a.a(str);
    }

    @DexIgnore
    public fw3 a(int i2) {
        if (!this.b.containsKey(Integer.valueOf(i2))) {
            return null;
        }
        return this.a.a(i2);
    }

    @DexIgnore
    public final e a(Pattern pattern, String str) {
        Matcher matcher = pattern.matcher(str);
        if (matcher.matches()) {
            return e.IS_POSSIBLE;
        }
        if (matcher.lookingAt()) {
            return e.TOO_LONG;
        }
        return e.TOO_SHORT;
    }

    @DexIgnore
    public final boolean a(fw3 fw3, String str) {
        return a(this.d.a(fw3.a.b), str) == e.TOO_SHORT;
    }

    @DexIgnore
    public int a(StringBuilder sb, StringBuilder sb2) {
        if (!(sb.length() == 0 || sb.charAt(0) == '0')) {
            int length = sb.length();
            int i2 = 1;
            while (i2 <= 3 && i2 <= length) {
                int parseInt = Integer.parseInt(sb.substring(0, i2));
                if (this.b.containsKey(Integer.valueOf(parseInt))) {
                    sb2.append(sb.substring(i2));
                    return parseInt;
                }
                i2++;
            }
        }
        return 0;
    }

    @DexIgnore
    public int a(String str, fw3 fw3, StringBuilder sb, boolean z, bw3 bw3) throws zv3 {
        if (str.length() == 0) {
            return 0;
        }
        StringBuilder sb2 = new StringBuilder(str);
        bw3.a a2 = a(sb2, fw3 != null ? fw3.r : "NonMatch");
        if (z) {
            bw3.setCountryCodeSource(a2);
        }
        if (a2 == bw3.a.FROM_DEFAULT_COUNTRY) {
            if (fw3 != null) {
                int i2 = fw3.q;
                String valueOf = String.valueOf(i2);
                String sb3 = sb2.toString();
                if (sb3.startsWith(valueOf)) {
                    StringBuilder sb4 = new StringBuilder(sb3.substring(valueOf.length()));
                    hw3 hw3 = fw3.a;
                    Pattern a3 = this.d.a(hw3.a);
                    a(sb4, fw3, (StringBuilder) null);
                    Pattern a4 = this.d.a(hw3.b);
                    if ((!a3.matcher(sb2).matches() && a3.matcher(sb4).matches()) || a(a4, sb2.toString()) == e.TOO_LONG) {
                        sb.append(sb4);
                        if (z) {
                            bw3.setCountryCodeSource(bw3.a.FROM_NUMBER_WITHOUT_PLUS_SIGN);
                        }
                        bw3.setCountryCode(i2);
                        return i2;
                    }
                }
            }
            bw3.setCountryCode(0);
            return 0;
        } else if (sb2.length() > 2) {
            int a5 = a(sb2, sb);
            if (a5 != 0) {
                bw3.setCountryCode(a5);
                return a5;
            }
            throw new zv3(zv3.a.INVALID_COUNTRY_CODE, "Country calling code supplied was not recognised.");
        } else {
            throw new zv3(zv3.a.TOO_SHORT_AFTER_IDD, "Phone number had an IDD, but after this was not long enough to be a viable phone number.");
        }
    }

    @DexIgnore
    public final boolean a(Pattern pattern, StringBuilder sb) {
        Matcher matcher = pattern.matcher(sb);
        if (!matcher.lookingAt()) {
            return false;
        }
        int end = matcher.end();
        Matcher matcher2 = n.matcher(sb.substring(end));
        if (matcher2.find() && g(matcher2.group(1)).equals("0")) {
            return false;
        }
        sb.delete(0, end);
        return true;
    }

    @DexIgnore
    public bw3.a a(StringBuilder sb, String str) {
        if (sb.length() == 0) {
            return bw3.a.FROM_DEFAULT_COUNTRY;
        }
        Matcher matcher = l.matcher(sb);
        if (matcher.lookingAt()) {
            sb.delete(0, matcher.end());
            b(sb);
            return bw3.a.FROM_NUMBER_WITH_PLUS_SIGN;
        }
        Pattern a2 = this.d.a(str);
        b(sb);
        return a(a2, sb) ? bw3.a.FROM_NUMBER_WITH_IDD : bw3.a.FROM_DEFAULT_COUNTRY;
    }

    @DexIgnore
    public boolean a(StringBuilder sb, fw3 fw3, StringBuilder sb2) {
        int length = sb.length();
        String str = fw3.t;
        if (!(length == 0 || str.length() == 0)) {
            Matcher matcher = this.d.a(str).matcher(sb);
            if (matcher.lookingAt()) {
                Pattern a2 = this.d.a(fw3.a.a);
                boolean matches = a2.matcher(sb).matches();
                int groupCount = matcher.groupCount();
                String str2 = fw3.u;
                if (str2 != null && str2.length() != 0 && matcher.group(groupCount) != null) {
                    StringBuilder sb3 = new StringBuilder(sb);
                    sb3.replace(0, length, matcher.replaceFirst(str2));
                    if (matches && !a2.matcher(sb3.toString()).matches()) {
                        return false;
                    }
                    if (sb2 != null && groupCount > 1) {
                        sb2.append(matcher.group(1));
                    }
                    sb.replace(0, sb.length(), sb3.toString());
                    return true;
                } else if (matches && !a2.matcher(sb.substring(matcher.end())).matches()) {
                    return false;
                } else {
                    if (!(sb2 == null || groupCount <= 0 || matcher.group(groupCount) == null)) {
                        sb2.append(matcher.group(1));
                    }
                    sb.delete(0, matcher.end());
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public String a(StringBuilder sb) {
        Matcher matcher = u.matcher(sb);
        if (!matcher.find() || !e(sb.substring(0, matcher.start()))) {
            return "";
        }
        int groupCount = matcher.groupCount();
        for (int i2 = 1; i2 <= groupCount; i2++) {
            if (matcher.group(i2) != null) {
                String group = matcher.group(i2);
                sb.delete(matcher.start(), sb.length());
                return group;
            }
        }
        return "";
    }

    @DexIgnore
    public final boolean a(String str, String str2) {
        if (!b(str2)) {
            return (str == null || str.length() == 0 || !l.matcher(str).lookingAt()) ? false : true;
        }
        return true;
    }

    @DexIgnore
    public void a(String str, String str2, bw3 bw3) throws zv3 {
        a(str, str2, false, true, bw3);
    }

    @DexIgnore
    public static void a(String str, bw3 bw3) {
        if (str.length() > 1 && str.charAt(0) == '0') {
            bw3.setItalianLeadingZero(true);
            int i2 = 1;
            while (i2 < str.length() - 1 && str.charAt(i2) == '0') {
                i2++;
            }
            if (i2 != 1) {
                bw3.setNumberOfLeadingZeros(i2);
            }
        }
    }

    @DexIgnore
    public final void a(String str, String str2, boolean z, boolean z2, bw3 bw3) throws zv3 {
        int i2;
        if (str == null) {
            throw new zv3(zv3.a.NOT_A_NUMBER, "The phone number supplied was null.");
        } else if (str.length() <= 250) {
            StringBuilder sb = new StringBuilder();
            a(str, sb);
            if (!e(sb.toString())) {
                throw new zv3(zv3.a.NOT_A_NUMBER, "The string supplied did not seem to be a phone number.");
            } else if (!z2 || a(sb.toString(), str2)) {
                if (z) {
                    bw3.setRawInput(str);
                }
                String a2 = a(sb);
                if (a2.length() > 0) {
                    bw3.setExtension(a2);
                }
                fw3 a3 = a(str2);
                StringBuilder sb2 = new StringBuilder();
                try {
                    i2 = a(sb.toString(), a3, sb2, z, bw3);
                } catch (zv3 e2) {
                    Matcher matcher = l.matcher(sb.toString());
                    if (e2.getErrorType() != zv3.a.INVALID_COUNTRY_CODE || !matcher.lookingAt()) {
                        throw new zv3(e2.getErrorType(), e2.getMessage());
                    }
                    i2 = a(sb.substring(matcher.end()), a3, sb2, z, bw3);
                    if (i2 == 0) {
                        throw new zv3(zv3.a.INVALID_COUNTRY_CODE, "Could not interpret numbers after plus-sign.");
                    }
                }
                if (i2 != 0) {
                    String b2 = b(i2);
                    if (!b2.equals(str2)) {
                        a3 = a(i2, b2);
                    }
                } else {
                    b(sb);
                    sb2.append(sb);
                    if (str2 != null) {
                        bw3.setCountryCode(a3.q);
                    } else if (z) {
                        bw3.clearCountryCodeSource();
                    }
                }
                if (sb2.length() >= 2) {
                    if (a3 != null) {
                        StringBuilder sb3 = new StringBuilder();
                        StringBuilder sb4 = new StringBuilder(sb2);
                        a(sb4, a3, sb3);
                        if (!a(a3, sb4.toString())) {
                            if (z) {
                                bw3.setPreferredDomesticCarrierCode(sb3.toString());
                            }
                            sb2 = sb4;
                        }
                    }
                    int length = sb2.length();
                    if (length < 2) {
                        throw new zv3(zv3.a.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                    } else if (length <= 17) {
                        a(sb2.toString(), bw3);
                        bw3.setNationalNumber(Long.parseLong(sb2.toString()));
                    } else {
                        throw new zv3(zv3.a.TOO_LONG, "The string supplied is too long to be a phone number.");
                    }
                } else {
                    throw new zv3(zv3.a.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                }
            } else {
                throw new zv3(zv3.a.INVALID_COUNTRY_CODE, "Missing or invalid default region.");
            }
        } else {
            throw new zv3(zv3.a.TOO_LONG, "The string supplied was too long to parse.");
        }
    }

    @DexIgnore
    public final void a(String str, StringBuilder sb) {
        int indexOf = str.indexOf(";phone-context=");
        if (indexOf > 0) {
            int i2 = indexOf + 15;
            if (str.charAt(i2) == '+') {
                int indexOf2 = str.indexOf(59, i2);
                if (indexOf2 > 0) {
                    sb.append(str.substring(i2, indexOf2));
                } else {
                    sb.append(str.substring(i2));
                }
            }
            int indexOf3 = str.indexOf("tel:");
            sb.append(str.substring(indexOf3 >= 0 ? indexOf3 + 4 : 0, indexOf));
        } else {
            sb.append(d(str));
        }
        int indexOf4 = sb.indexOf(";isub=");
        if (indexOf4 > 0) {
            sb.delete(indexOf4, sb.length());
        }
    }
}
