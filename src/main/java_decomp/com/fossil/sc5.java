package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc5 implements Factory<rc5> {
    @DexIgnore
    public static ActivityOverviewMonthPresenter a(qc5 qc5, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new ActivityOverviewMonthPresenter(qc5, userRepository, summariesRepository, portfolioApp);
    }
}
