package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mp6 implements Runnable {
    @DexIgnore
    public long a;
    @DexIgnore
    public np6 b;

    @DexIgnore
    public mp6(long j, np6 np6) {
        wg6.b(np6, "taskContext");
        this.a = j;
        this.b = np6;
    }

    @DexIgnore
    public final pp6 a() {
        return this.b.n();
    }

    @DexIgnore
    public mp6() {
        this(0, lp6.b);
    }
}
