package com.fossil;

import android.animation.TimeInterpolator;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ni3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bi3 {
    @DexIgnore
    public static /* final */ boolean V; // = (Build.VERSION.SDK_INT < 18);
    @DexIgnore
    public static /* final */ Paint W; // = null;
    @DexIgnore
    public boolean A;
    @DexIgnore
    public Bitmap B;
    @DexIgnore
    public Paint C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public int[] H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public /* final */ TextPaint J;
    @DexIgnore
    public /* final */ TextPaint K;
    @DexIgnore
    public TimeInterpolator L;
    @DexIgnore
    public TimeInterpolator M;
    @DexIgnore
    public float N;
    @DexIgnore
    public float O;
    @DexIgnore
    public float P;
    @DexIgnore
    public ColorStateList Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public float S;
    @DexIgnore
    public float T;
    @DexIgnore
    public ColorStateList U;
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public float c;
    @DexIgnore
    public /* final */ Rect d;
    @DexIgnore
    public /* final */ Rect e;
    @DexIgnore
    public /* final */ RectF f;
    @DexIgnore
    public int g; // = 16;
    @DexIgnore
    public int h; // = 16;
    @DexIgnore
    public float i; // = 15.0f;
    @DexIgnore
    public float j; // = 15.0f;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public ColorStateList l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public Typeface s;
    @DexIgnore
    public Typeface t;
    @DexIgnore
    public Typeface u;
    @DexIgnore
    public ni3 v;
    @DexIgnore
    public ni3 w;
    @DexIgnore
    public CharSequence x;
    @DexIgnore
    public CharSequence y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ni3.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(Typeface typeface) {
            bi3.this.a(typeface);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ni3.a {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(Typeface typeface) {
            bi3.this.c(typeface);
        }
    }

    /*
    static {
        Paint paint = W;
        if (paint != null) {
            paint.setAntiAlias(true);
            W.setColor(-65281);
        }
    }
    */

    @DexIgnore
    public bi3(View view) {
        this.a = view;
        this.J = new TextPaint(129);
        this.K = new TextPaint(this.J);
        this.e = new Rect();
        this.d = new Rect();
        this.f = new RectF();
    }

    @DexIgnore
    public void a(TimeInterpolator timeInterpolator) {
        this.L = timeInterpolator;
        s();
    }

    @DexIgnore
    public void b(TimeInterpolator timeInterpolator) {
        this.M = timeInterpolator;
        s();
    }

    @DexIgnore
    public void c(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            s();
        }
    }

    @DexIgnore
    public void d(float f2) {
        if (this.i != f2) {
            this.i = f2;
            s();
        }
    }

    @DexIgnore
    public void e(Typeface typeface) {
        boolean b2 = b(typeface);
        boolean d2 = d(typeface);
        if (b2 || d2) {
            s();
        }
    }

    @DexIgnore
    public final void f(float f2) {
        b(f2);
        this.A = V && this.F != 1.0f;
        if (this.A) {
            e();
        }
        x9.I(this.a);
    }

    @DexIgnore
    public int g() {
        return this.h;
    }

    @DexIgnore
    public float h() {
        a(this.K);
        return -this.K.ascent();
    }

    @DexIgnore
    public Typeface i() {
        Typeface typeface = this.s;
        return typeface != null ? typeface : Typeface.DEFAULT;
    }

    @DexIgnore
    public int j() {
        return a(this.l);
    }

    @DexIgnore
    public final int k() {
        return a(this.k);
    }

    @DexIgnore
    public int l() {
        return this.g;
    }

    @DexIgnore
    public float m() {
        b(this.K);
        return -this.K.ascent();
    }

    @DexIgnore
    public Typeface n() {
        Typeface typeface = this.t;
        return typeface != null ? typeface : Typeface.DEFAULT;
    }

    @DexIgnore
    public float o() {
        return this.c;
    }

    @DexIgnore
    public CharSequence p() {
        return this.x;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000a, code lost:
        r0 = r1.k;
     */
    @DexIgnore
    public final boolean q() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2 = this.l;
        return (colorStateList2 != null && colorStateList2.isStateful()) || (colorStateList != null && colorStateList.isStateful());
    }

    @DexIgnore
    public void r() {
        this.b = this.e.width() > 0 && this.e.height() > 0 && this.d.width() > 0 && this.d.height() > 0;
    }

    @DexIgnore
    public void s() {
        if (this.a.getHeight() > 0 && this.a.getWidth() > 0) {
            a();
            c();
        }
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        if (!a(this.e, i2, i3, i4, i5)) {
            this.e.set(i2, i3, i4, i5);
            this.I = true;
            r();
        }
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            s();
        }
    }

    @DexIgnore
    public void c(int i2) {
        qi3 qi3 = new qi3(this.a.getContext(), i2);
        ColorStateList colorStateList = qi3.b;
        if (colorStateList != null) {
            this.k = colorStateList;
        }
        float f2 = qi3.a;
        if (f2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.i = f2;
        }
        ColorStateList colorStateList2 = qi3.f;
        if (colorStateList2 != null) {
            this.U = colorStateList2;
        }
        this.S = qi3.g;
        this.T = qi3.h;
        this.R = qi3.i;
        ni3 ni3 = this.v;
        if (ni3 != null) {
            ni3.a();
        }
        this.v = new ni3(new b(), qi3.b());
        qi3.a(this.a.getContext(), (si3) this.v);
        s();
    }

    @DexIgnore
    public void d(int i2) {
        if (this.g != i2) {
            this.g = i2;
            s();
        }
    }

    @DexIgnore
    public void e(float f2) {
        float a2 = x7.a(f2, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        if (a2 != this.c) {
            this.c = a2;
            c();
        }
    }

    @DexIgnore
    public void b(int i2, int i3, int i4, int i5) {
        if (!a(this.d, i2, i3, i4, i5)) {
            this.d.set(i2, i3, i4, i5);
            this.I = true;
            r();
        }
    }

    @DexIgnore
    public ColorStateList f() {
        return this.l;
    }

    @DexIgnore
    public void a(Rect rect) {
        a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    public final boolean d(Typeface typeface) {
        ni3 ni3 = this.v;
        if (ni3 != null) {
            ni3.a();
        }
        if (this.t == typeface) {
            return false;
        }
        this.t = typeface;
        return true;
    }

    @DexIgnore
    public void a(RectF rectF) {
        float f2;
        boolean a2 = a(this.x);
        Rect rect = this.e;
        if (!a2) {
            f2 = (float) rect.left;
        } else {
            f2 = ((float) rect.right) - b();
        }
        rectF.left = f2;
        Rect rect2 = this.e;
        rectF.top = (float) rect2.top;
        rectF.right = !a2 ? rectF.left + b() : (float) rect2.right;
        rectF.bottom = ((float) this.e.top) + h();
    }

    @DexIgnore
    public final void e() {
        if (this.B == null && !this.d.isEmpty() && !TextUtils.isEmpty(this.y)) {
            a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.D = this.J.ascent();
            this.E = this.J.descent();
            TextPaint textPaint = this.J;
            CharSequence charSequence = this.y;
            int round = Math.round(textPaint.measureText(charSequence, 0, charSequence.length()));
            int round2 = Math.round(this.E - this.D);
            if (round > 0 && round2 > 0) {
                this.B = Bitmap.createBitmap(round, round2, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(this.B);
                CharSequence charSequence2 = this.y;
                canvas.drawText(charSequence2, 0, charSequence2.length(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((float) round2) - this.J.descent(), this.J);
                if (this.C == null) {
                    this.C = new Paint(3);
                }
            }
        }
    }

    @DexIgnore
    public void b(Rect rect) {
        b(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    public float b() {
        if (this.x == null) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        a(this.K);
        TextPaint textPaint = this.K;
        CharSequence charSequence = this.x;
        return textPaint.measureText(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public final void d() {
        Bitmap bitmap = this.B;
        if (bitmap != null) {
            bitmap.recycle();
            this.B = null;
        }
    }

    @DexIgnore
    public final void a(TextPaint textPaint) {
        textPaint.setTextSize(this.j);
        textPaint.setTypeface(this.s);
    }

    @DexIgnore
    public final void b(TextPaint textPaint) {
        textPaint.setTextSize(this.i);
        textPaint.setTypeface(this.t);
    }

    @DexIgnore
    public void a(int i2) {
        qi3 qi3 = new qi3(this.a.getContext(), i2);
        ColorStateList colorStateList = qi3.b;
        if (colorStateList != null) {
            this.l = colorStateList;
        }
        float f2 = qi3.a;
        if (f2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.j = f2;
        }
        ColorStateList colorStateList2 = qi3.f;
        if (colorStateList2 != null) {
            this.Q = colorStateList2;
        }
        this.O = qi3.g;
        this.P = qi3.h;
        this.N = qi3.i;
        ni3 ni3 = this.w;
        if (ni3 != null) {
            ni3.a();
        }
        this.w = new ni3(new a(), qi3.b());
        qi3.a(this.a.getContext(), (si3) this.w);
        s();
    }

    @DexIgnore
    public void b(int i2) {
        if (this.h != i2) {
            this.h = i2;
            s();
        }
    }

    @DexIgnore
    public final boolean b(Typeface typeface) {
        ni3 ni3 = this.w;
        if (ni3 != null) {
            ni3.a();
        }
        if (this.s == typeface) {
            return false;
        }
        this.s = typeface;
        return true;
    }

    @DexIgnore
    public void c(Typeface typeface) {
        if (d(typeface)) {
            s();
        }
    }

    @DexIgnore
    public final void c() {
        a(this.c);
    }

    @DexIgnore
    public final void b(float f2) {
        boolean z2;
        float f3;
        boolean z3;
        if (this.x != null) {
            float width = (float) this.e.width();
            float width2 = (float) this.d.width();
            boolean z4 = true;
            if (a(f2, this.j)) {
                float f4 = this.j;
                this.F = 1.0f;
                Typeface typeface = this.u;
                Typeface typeface2 = this.s;
                if (typeface != typeface2) {
                    this.u = typeface2;
                    z3 = true;
                } else {
                    z3 = false;
                }
                f3 = f4;
                z2 = z3;
            } else {
                f3 = this.i;
                Typeface typeface3 = this.u;
                Typeface typeface4 = this.t;
                if (typeface3 != typeface4) {
                    this.u = typeface4;
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (a(f2, this.i)) {
                    this.F = 1.0f;
                } else {
                    this.F = f2 / this.i;
                }
                float f5 = this.j / this.i;
                width = width2 * f5 > width ? Math.min(width / f5, width2) : width2;
            }
            if (width > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z2 = this.G != f3 || this.I || z2;
                this.G = f3;
                this.I = false;
            }
            if (this.y == null || z2) {
                this.J.setTextSize(this.G);
                this.J.setTypeface(this.u);
                TextPaint textPaint = this.J;
                if (this.F == 1.0f) {
                    z4 = false;
                }
                textPaint.setLinearText(z4);
                CharSequence ellipsize = TextUtils.ellipsize(this.x, this.J, width, TextUtils.TruncateAt.END);
                if (!TextUtils.equals(ellipsize, this.y)) {
                    this.y = ellipsize;
                    this.z = a(this.y);
                }
            }
        }
    }

    @DexIgnore
    public final void c(float f2) {
        this.f.left = a((float) this.d.left, (float) this.e.left, f2, this.L);
        this.f.top = a(this.m, this.n, f2, this.L);
        this.f.right = a((float) this.d.right, (float) this.e.right, f2, this.L);
        this.f.bottom = a((float) this.d.bottom, (float) this.e.bottom, f2, this.L);
    }

    @DexIgnore
    public void a(Typeface typeface) {
        if (b(typeface)) {
            s();
        }
    }

    @DexIgnore
    public final boolean a(int[] iArr) {
        this.H = iArr;
        if (!q()) {
            return false;
        }
        s();
        return true;
    }

    @DexIgnore
    public final void a(float f2) {
        c(f2);
        this.q = a(this.o, this.p, f2, this.L);
        this.r = a(this.m, this.n, f2, this.L);
        f(a(this.i, this.j, f2, this.M));
        if (this.l != this.k) {
            this.J.setColor(a(k(), j(), f2));
        } else {
            this.J.setColor(j());
        }
        this.J.setShadowLayer(a(this.R, this.N, f2, (TimeInterpolator) null), a(this.S, this.O, f2, (TimeInterpolator) null), a(this.T, this.P, f2, (TimeInterpolator) null), a(a(this.U), a(this.Q), f2));
        x9.I(this.a);
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        if (charSequence == null || !TextUtils.equals(this.x, charSequence)) {
            this.x = charSequence;
            this.y = null;
            d();
            s();
        }
    }

    @DexIgnore
    public final int a(ColorStateList colorStateList) {
        if (colorStateList == null) {
            return 0;
        }
        int[] iArr = this.H;
        if (iArr != null) {
            return colorStateList.getColorForState(iArr, 0);
        }
        return colorStateList.getDefaultColor();
    }

    @DexIgnore
    public final void a() {
        float f2 = this.G;
        b(this.j);
        CharSequence charSequence = this.y;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float measureText = charSequence != null ? this.J.measureText(charSequence, 0, charSequence.length()) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int a2 = e9.a(this.h, this.z ? 1 : 0);
        int i2 = a2 & 112;
        if (i2 == 48) {
            this.n = ((float) this.e.top) - this.J.ascent();
        } else if (i2 != 80) {
            this.n = ((float) this.e.centerY()) + (((this.J.descent() - this.J.ascent()) / 2.0f) - this.J.descent());
        } else {
            this.n = (float) this.e.bottom;
        }
        int i3 = a2 & 8388615;
        if (i3 == 1) {
            this.p = ((float) this.e.centerX()) - (measureText / 2.0f);
        } else if (i3 != 5) {
            this.p = (float) this.e.left;
        } else {
            this.p = ((float) this.e.right) - measureText;
        }
        b(this.i);
        CharSequence charSequence2 = this.y;
        if (charSequence2 != null) {
            f3 = this.J.measureText(charSequence2, 0, charSequence2.length());
        }
        int a3 = e9.a(this.g, this.z ? 1 : 0);
        int i4 = a3 & 112;
        if (i4 == 48) {
            this.m = ((float) this.d.top) - this.J.ascent();
        } else if (i4 != 80) {
            this.m = ((float) this.d.centerY()) + (((this.J.descent() - this.J.ascent()) / 2.0f) - this.J.descent());
        } else {
            this.m = (float) this.d.bottom;
        }
        int i5 = a3 & 8388615;
        if (i5 == 1) {
            this.o = ((float) this.d.centerX()) - (f3 / 2.0f);
        } else if (i5 != 5) {
            this.o = (float) this.d.left;
        } else {
            this.o = ((float) this.d.right) - f3;
        }
        d();
        f(f2);
    }

    @DexIgnore
    public void a(Canvas canvas) {
        float f2;
        int save = canvas.save();
        if (this.y != null && this.b) {
            float f3 = this.q;
            float f4 = this.r;
            boolean z2 = this.A && this.B != null;
            if (z2) {
                f2 = this.D * this.F;
            } else {
                f2 = this.J.ascent() * this.F;
                this.J.descent();
            }
            if (z2) {
                f4 += f2;
            }
            float f5 = f4;
            float f6 = this.F;
            if (f6 != 1.0f) {
                canvas.scale(f6, f6, f3, f5);
            }
            if (z2) {
                canvas.drawBitmap(this.B, f3, f5, this.C);
            } else {
                CharSequence charSequence = this.y;
                canvas.drawText(charSequence, 0, charSequence.length(), f3, f5, this.J);
            }
        }
        canvas.restoreToCount(save);
    }

    @DexIgnore
    public final boolean a(CharSequence charSequence) {
        boolean z2 = true;
        if (x9.o(this.a) != 1) {
            z2 = false;
        }
        return (z2 ? p8.d : p8.c).a(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public static boolean a(float f2, float f3) {
        return Math.abs(f2 - f3) < 0.001f;
    }

    @DexIgnore
    public static int a(int i2, int i3, float f2) {
        float f3 = 1.0f - f2;
        return Color.argb((int) ((((float) Color.alpha(i2)) * f3) + (((float) Color.alpha(i3)) * f2)), (int) ((((float) Color.red(i2)) * f3) + (((float) Color.red(i3)) * f2)), (int) ((((float) Color.green(i2)) * f3) + (((float) Color.green(i3)) * f2)), (int) ((((float) Color.blue(i2)) * f3) + (((float) Color.blue(i3)) * f2)));
    }

    @DexIgnore
    public static float a(float f2, float f3, float f4, TimeInterpolator timeInterpolator) {
        if (timeInterpolator != null) {
            f4 = timeInterpolator.getInterpolation(f4);
        }
        return yf3.a(f2, f3, f4);
    }

    @DexIgnore
    public static boolean a(Rect rect, int i2, int i3, int i4, int i5) {
        return rect.left == i2 && rect.top == i3 && rect.right == i4 && rect.bottom == i5;
    }
}
