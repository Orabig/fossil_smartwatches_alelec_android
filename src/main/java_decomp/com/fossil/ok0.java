package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.facebook.login.LoginStatusClient;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ok0 implements ef0<p51> {
    @DexIgnore
    public /* final */ Handler a;
    @DexIgnore
    public /* final */ li1 b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public ch0 d;
    @DexIgnore
    public hg6<? super ok0, cd6> e;
    @DexIgnore
    public hg6<? super ok0, cd6> f;
    @DexIgnore
    public gg6<cd6> g;
    @DexIgnore
    public /* final */ hm0 h;
    @DexIgnore
    public /* final */ at0 i;

    @DexIgnore
    public ok0(hm0 hm0, at0 at0) {
        this.h = hm0;
        this.i = at0;
        cw0.a((Enum<?>) this.h);
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.a = new Handler(myLooper);
            this.b = li1.NORMAL;
            this.d = new ch0(this.h, lf0.NOT_START, (t31) null, 4);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public abstract void a(ue1 ue1);

    @DexIgnore
    public void a(Object obj) {
        p51 p51 = (p51) obj;
        if (b(p51)) {
            lf0 lf0 = this.d.b;
            if (lf0 == lf0.INTERRUPTED || lf0 == lf0.INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT) {
                this.d = ch0.a(this.d, (hm0) null, (lf0) null, p51.a, 3);
            } else {
                a(p51);
            }
            a();
        }
    }

    @DexIgnore
    public li1 b() {
        return this.b;
    }

    @DexIgnore
    public abstract boolean b(p51 p51);

    @DexIgnore
    public abstract hn1<p51> c();

    @DexIgnore
    public void c(p51 p51) {
    }

    @DexIgnore
    public boolean d() {
        return false;
    }

    @DexIgnore
    public final ok0 a(gg6<cd6> gg6) {
        this.g = gg6;
        return this;
    }

    @DexIgnore
    public final void a(lf0 lf0) {
        if (!this.c) {
            oa1 oa1 = oa1.a;
            new Object[1][0] = lf0;
            this.d = ch0.a(this.d, (hm0) null, lf0, (t31) null, 5);
            if (d()) {
                this.a.postDelayed(new ui0(this), LoginStatusClient.DEFAULT_TOAST_DURATION_MS);
            } else {
                a();
            }
        }
    }

    @DexIgnore
    public void a(p51 p51) {
        c(p51);
        ch0 a2 = ch0.d.a(p51.a);
        this.d = ch0.a(this.d, (hm0) null, a2.b, a2.c, 1);
    }

    @DexIgnore
    public final void a() {
        if (!this.c) {
            this.c = true;
            hn1<p51> c2 = c();
            if (c2 != null) {
                c2.b(this);
            }
            lf0 lf0 = lf0.SUCCESS;
            ch0 ch0 = this.d;
            if (lf0 == ch0.b) {
                oa1 oa1 = oa1.a;
                new Object[1][0] = p40.a(ch0, 0, 1, (Object) null);
                hg6<? super ok0, cd6> hg6 = this.e;
                if (hg6 != null) {
                    cd6 cd6 = (cd6) hg6.invoke(this);
                }
            } else {
                oa1 oa12 = oa1.a;
                new Object[1][0] = p40.a(ch0, 0, 1, (Object) null);
                hg6<? super ok0, cd6> hg62 = this.f;
                if (hg62 != null) {
                    cd6 cd62 = (cd6) hg62.invoke(this);
                }
            }
            gg6<cd6> gg6 = this.g;
            if (gg6 != null) {
                cd6 cd63 = (cd6) gg6.invoke();
            }
        }
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        return new JSONObject();
    }
}
