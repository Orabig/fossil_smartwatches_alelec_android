package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum th4 {
    Jawbone("jawbone"),
    UnderArmour("underarmour"),
    HealthKit("healthkit"),
    GoogleFit("googlefit");
    
    @DexIgnore
    public /* final */ String name;

    @DexIgnore
    public th4(String str) {
        this.name = str;
    }

    @DexIgnore
    public static th4 fromName(String str) {
        for (th4 th4 : values()) {
            if (th4.name.equalsIgnoreCase(str)) {
                return th4;
            }
        }
        return null;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }
}
