package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1", f = "PairingPresenter.kt", l = {308}, m = "invokeSuspend")
public final class qs5$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LinkDeviceUseCase.i $errorValue;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ PairingPresenter.d this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$pairDeviceCallback$1$onError$1$params$1", f = "PairingPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super HashMap<String, String>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qs5$d$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(qs5$d$a qs5_d_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = qs5_d_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                qs5$d$a qs5_d_a = this.this$0;
                return qs5_d_a.this$0.a.d(qs5_d_a.$errorValue.a());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qs5$d$a(PairingPresenter.d dVar, LinkDeviceUseCase.i iVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
        this.$errorValue = iVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        qs5$d$a qs5_d_a = new qs5$d$a(this.this$0, this.$errorValue, xe6);
        qs5_d_a.p$ = (il6) obj;
        return qs5_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((qs5$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = PairingPresenter.y.a();
            local.d(a3, "Inside .startPairClosestDevice pairDevice fail with error=" + this.$errorValue.b());
            dl6 a4 = this.this$0.a.b();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = gk6.a(a4, aVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        HashMap hashMap = (HashMap) obj;
        if (hashMap.containsKey("Style_Number") && hashMap.containsKey("Device_Name")) {
            this.this$0.a.a("pair_fail", (Map<String, String>) hashMap);
        }
        this.this$0.a.s.a();
        LinkDeviceUseCase.i iVar = this.$errorValue;
        if (iVar instanceof LinkDeviceUseCase.d) {
            if (this.this$0.a.p) {
                ns5 g = this.this$0.a.s;
                ShineDevice f = this.this$0.a.e;
                if (f != null) {
                    String serial = f.getSerial();
                    wg6.a((Object) serial, "mPairingDevice!!.serial");
                    g.l(serial);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                ns5 g2 = this.this$0.a.s;
                ShineDevice f2 = this.this$0.a.e;
                if (f2 != null) {
                    String serial2 = f2.getSerial();
                    wg6.a((Object) serial2, "mPairingDevice!!.serial");
                    g2.o(serial2);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else if (iVar instanceof LinkDeviceUseCase.k) {
            this.this$0.a.s.a(this.$errorValue.b(), this.$errorValue.a(), this.$errorValue.c());
        } else if (iVar instanceof LinkDeviceUseCase.e) {
            this.this$0.a.s.y();
        }
        String valueOf = String.valueOf(this.$errorValue.b());
        hl4 a5 = AnalyticsHelper.f.a("setup_device_session_optional_error");
        a5.a("error_code", valueOf);
        jl4 p = this.this$0.a.p();
        if (p != null) {
            p.a(a5);
        }
        jl4 p2 = this.this$0.a.p();
        if (p2 != null) {
            p2.a(valueOf);
        }
        AnalyticsHelper.f.e("setup_device_session");
        return cd6.a;
    }
}
