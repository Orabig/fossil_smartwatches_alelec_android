package com.fossil;

import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1$listDNDScheduledTimeModel$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
public final class a15$c$a extends sf6 implements ig6<il6, xe6<? super List<? extends DNDScheduledTimeModel>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a15$c$a(DoNotDisturbScheduledTimePresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        a15$c$a a15_c_a = new a15$c$a(this.this$0, xe6);
        a15_c_a.p$ = (il6) obj;
        return a15_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((a15$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.j.getDNDScheduledTimeDao().getListDNDScheduledTimeModel();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
