package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kz0 extends qx0 {
    @DexIgnore
    public byte[] O;
    @DexIgnore
    public short P;
    @DexIgnore
    public long Q;
    @DexIgnore
    public /* final */ int R;
    @DexIgnore
    public /* final */ int S;
    @DexIgnore
    public float T;
    @DexIgnore
    public /* final */ boolean U;
    @DexIgnore
    public /* final */ byte[] V;
    @DexIgnore
    public byte[] W;
    @DexIgnore
    public /* final */ long X;
    @DexIgnore
    public /* final */ float Y;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ kz0(short s, long j, ue1 ue1, int i, float f, int i2) {
        super(du0.LEGACY_GET_FILE, s, lx0.LEGACY_GET_ACTIVITY_FILE, ue1, r5);
        int i3 = (i2 & 8) != 0 ? 3 : i;
        f = (i2 & 16) != 0 ? 0.001f : f;
        this.X = j;
        this.Y = f;
        this.O = new byte[0];
        this.S = 65535;
        byte[] array = ByteBuffer.allocate(11).order(ByteOrder.LITTLE_ENDIAN).put(du0.LEGACY_GET_FILE.a).putShort(s).putInt(this.R).putInt(this.S).array();
        wg6.a(array, "ByteBuffer.allocate(11)\n\u2026gth)\n            .array()");
        this.V = array;
        byte[] array2 = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(du0.LEGACY_GET_FILE.a()).array();
        wg6.a(array2, "ByteBuffer.allocate(1)\n \u2026e())\n            .array()");
        this.W = array2;
    }

    @DexIgnore
    public void b(byte[] bArr) {
        this.W = bArr;
    }

    @DexIgnore
    public void c(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        this.P = order.getShort(0);
        this.Q = cw0.b(order.getInt(bArr.length - 4));
        if (!(this.P == s() || this.Q == h51.a.a(bArr, 0, bArr.length - 4, q11.CRC32))) {
            this.v = bn0.a(this.v, (lx0) null, (String) null, il0.INVALID_RESPONSE_DATA, (ch0) null, (sj0) null, 27);
        }
        this.O = bArr;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(cw0.a(super.h(), bm0.TOTAL_FILE_SIZE, (Object) Long.valueOf(this.X)), bm0.OFFSET, (Object) Integer.valueOf(this.R)), bm0.LENGTH, (Object) Integer.valueOf(this.S));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(super.i(), bm0.FILE_CRC, (Object) Long.valueOf(h51.a.a(this.O, q11.CRC32))), bm0.LENGTH, (Object) Integer.valueOf(this.O.length));
    }

    @DexIgnore
    public byte[] o() {
        return this.V;
    }

    @DexIgnore
    public boolean q() {
        return this.U;
    }

    @DexIgnore
    public byte[] r() {
        return this.W;
    }

    @DexIgnore
    public void t() {
        float length = (((float) this.O.length) * 1.0f) / ((float) this.X);
        if (length - this.T > this.Y || length == 1.0f) {
            this.T = length;
            a(length);
        }
    }
}
