package com.fossil;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Pair;
import com.fossil.sj2;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xa3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public Set<Integer> b; // = new HashSet();
    @DexIgnore
    public Map<Integer, ua3> c; // = new p4();
    @DexIgnore
    public /* final */ /* synthetic */ sa3 d;

    @DexIgnore
    public xa3(sa3 sa3, String str) {
        this.d = sa3;
        this.a = str;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v42, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v30, resolved type: java.lang.Long} */
    /* JADX WARNING: Code restructure failed: missing block: B:224:0x0743, code lost:
        if (r11.n() == false) goto L_0x074e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x0745, code lost:
        r11 = java.lang.Integer.valueOf(r11.o());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:226:0x074e, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x074f, code lost:
        r2.a("Invalid property filter ID. appId, id", r10, java.lang.String.valueOf(r11));
        r11 = false;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x03a2  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x03c1  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x03c6  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x045e  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x04bf  */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x053d  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0561  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x0611  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x0786  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x07ac  */
    /* JADX WARNING: Removed duplicated region for block: B:273:0x0255 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x024e  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0278  */
    public final List<kj2> a(List<mj2> list, List<uj2> list2, Long l) {
        boolean z;
        boolean d2;
        Map<Integer, sj2> f;
        boolean d3;
        String str;
        Iterator<mj2> it;
        f03 a2;
        mj2 mj2;
        f03 f03;
        Map map;
        Iterator<mj2> it2;
        Object obj;
        Long l2;
        List<oj2> arrayList;
        Map<Integer, sj2> map2;
        boolean z2;
        yz2 o;
        String str2;
        if (this.d.l().d(this.a, l03.u0) || this.d.l().d(this.a, l03.v0)) {
            Iterator<mj2> it3 = list.iterator();
            while (true) {
                if (it3.hasNext()) {
                    if ("_s".equals(it3.next().p())) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            d2 = this.d.l().d(this.a, l03.u0);
            boolean d4 = this.d.l().d(this.a, l03.v0);
            if (z && d4) {
                o = this.d.o();
                str2 = this.a;
                o.r();
                o.g();
                w12.b(str2);
                ContentValues contentValues = new ContentValues();
                contentValues.put("current_session_count", 0);
                o.v().update("events", contentValues, "app_id = ?", new String[]{str2});
            }
            f = this.d.o().f(this.a);
            if (f != null && !f.isEmpty()) {
                HashSet<Integer> hashSet = new HashSet<>(f.keySet());
                if (d2 || !z) {
                    map2 = f;
                } else {
                    sa3 sa3 = this.d;
                    String str3 = this.a;
                    w12.b(str3);
                    w12.a(f);
                    map2 = new p4<>();
                    if (!f.isEmpty()) {
                        Map<Integer, List<Integer>> e = sa3.o().e(str3);
                        for (Integer intValue : f.keySet()) {
                            int intValue2 = intValue.intValue();
                            sj2 sj2 = f.get(Integer.valueOf(intValue2));
                            List list3 = e.get(Integer.valueOf(intValue2));
                            if (list3 == null || list3.isEmpty()) {
                                map2.put(Integer.valueOf(intValue2), sj2);
                            } else {
                                List<Long> a3 = sa3.n().a(sj2.p(), (List<Integer>) list3);
                                if (!a3.isEmpty()) {
                                    sj2.a aVar = (sj2.a) sj2.j();
                                    aVar.k();
                                    aVar.b((Iterable<? extends Long>) a3);
                                    List<Long> a4 = sa3.n().a(sj2.n(), (List<Integer>) list3);
                                    aVar.j();
                                    aVar.a((Iterable<? extends Long>) a4);
                                    for (int i = 0; i < sj2.s(); i++) {
                                        if (list3.contains(Integer.valueOf(sj2.b(i).o()))) {
                                            aVar.a(i);
                                        }
                                    }
                                    for (int i2 = 0; i2 < sj2.v(); i2++) {
                                        if (list3.contains(Integer.valueOf(sj2.c(i2).o()))) {
                                            aVar.b(i2);
                                        }
                                    }
                                    map2.put(Integer.valueOf(intValue2), (sj2) aVar.i());
                                }
                            }
                        }
                    }
                }
                for (Integer intValue3 : hashSet) {
                    int intValue4 = intValue3.intValue();
                    sj2 sj22 = map2.get(Integer.valueOf(intValue4));
                    BitSet bitSet = new BitSet();
                    BitSet bitSet2 = new BitSet();
                    p4 p4Var = new p4();
                    if (!(sj22 == null || sj22.s() == 0)) {
                        for (lj2 next : sj22.r()) {
                            if (next.n()) {
                                p4Var.put(Integer.valueOf(next.o()), next.p() ? Long.valueOf(next.q()) : null);
                            }
                        }
                    }
                    if (sj22 != null) {
                        for (int i3 = 0; i3 < (sj22.o() << 6); i3++) {
                            if (ia3.a(sj22.n(), i3)) {
                                this.d.b().B().a("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue4), Integer.valueOf(i3));
                                bitSet2.set(i3);
                                if (ia3.a(sj22.p(), i3)) {
                                    bitSet.set(i3);
                                    z2 = true;
                                    if (z2) {
                                        p4Var.remove(Integer.valueOf(i3));
                                    }
                                }
                            }
                            z2 = false;
                            if (z2) {
                            }
                        }
                    }
                    if (d2) {
                        sj22 = f.get(Integer.valueOf(intValue4));
                    }
                    a(intValue4).a(sj22, bitSet, bitSet2, p4Var);
                }
            }
            String str4 = "Skipping failed audience ID";
            if (!list.isEmpty()) {
                p4 p4Var2 = new p4();
                Iterator<mj2> it4 = list.iterator();
                mj2 mj22 = null;
                Long l3 = null;
                long j = 0;
                while (it4.hasNext()) {
                    mj2 next2 = it4.next();
                    String p = next2.p();
                    List<oj2> n = next2.n();
                    this.d.n();
                    Long l4 = (Long) ia3.b(next2, "_eid");
                    boolean z3 = l4 != null;
                    if (z3 && p.equals("_ep")) {
                        this.d.n();
                        String str5 = (String) ia3.b(next2, "_en");
                        if (TextUtils.isEmpty(str5)) {
                            this.d.b().t().a("Extra parameter without an event name. eventId", l4);
                            it2 = it4;
                        } else {
                            if (mj22 == null || l3 == null || l4.longValue() != l3.longValue()) {
                                Pair<mj2, Long> a5 = this.d.o().a(this.a, l4);
                                if (a5 == null || (obj = a5.first) == null) {
                                    it2 = it4;
                                    this.d.b().t().a("Extra parameter without existing main event. eventName, eventId", str5, l4);
                                } else {
                                    mj22 = (mj2) obj;
                                    j = ((Long) a5.second).longValue();
                                    this.d.n();
                                    l3 = ia3.b(mj22, "_eid");
                                }
                            }
                            mj2 mj23 = mj22;
                            j--;
                            if (j <= 0) {
                                yz2 o2 = this.d.o();
                                String str6 = this.a;
                                o2.g();
                                o2.b().B().a("Clearing complex main event info. appId", str6);
                                try {
                                    SQLiteDatabase v = o2.v();
                                    it = it4;
                                    l2 = l3;
                                    try {
                                        String[] strArr = new String[1];
                                        try {
                                            strArr[0] = str6;
                                            v.execSQL("delete from main_event_params where app_id=?", strArr);
                                        } catch (SQLiteException e2) {
                                            e = e2;
                                        }
                                    } catch (SQLiteException e3) {
                                        e = e3;
                                        o2.b().t().a("Error clearing complex main event", e);
                                        arrayList = new ArrayList<>();
                                        while (r8.hasNext()) {
                                        }
                                        if (!arrayList.isEmpty()) {
                                        }
                                        p = str5;
                                        mj22 = mj23;
                                        l3 = l2;
                                        sa3 sa32 = this.d;
                                        String str7 = this.a;
                                        boolean d5 = sa32.l().d(str7, l03.v0);
                                        a2 = sa32.o().a(str7, next2.p());
                                        if (a2 != null) {
                                        }
                                        this.d.o().a(f03);
                                        long j2 = f03.c;
                                        map = (Map) p4Var2.get(p);
                                        if (map == null) {
                                        }
                                        while (r14.hasNext()) {
                                        }
                                        Long l5 = l3;
                                        long j3 = j;
                                        it4 = it;
                                        mj22 = mj2;
                                    }
                                } catch (SQLiteException e4) {
                                    e = e4;
                                    it = it4;
                                    l2 = l3;
                                    o2.b().t().a("Error clearing complex main event", e);
                                    arrayList = new ArrayList<>();
                                    while (r8.hasNext()) {
                                    }
                                    if (!arrayList.isEmpty()) {
                                    }
                                    p = str5;
                                    mj22 = mj23;
                                    l3 = l2;
                                    sa3 sa322 = this.d;
                                    String str72 = this.a;
                                    boolean d52 = sa322.l().d(str72, l03.v0);
                                    a2 = sa322.o().a(str72, next2.p());
                                    if (a2 != null) {
                                    }
                                    this.d.o().a(f03);
                                    long j22 = f03.c;
                                    map = (Map) p4Var2.get(p);
                                    if (map == null) {
                                    }
                                    while (r14.hasNext()) {
                                    }
                                    Long l52 = l3;
                                    long j32 = j;
                                    it4 = it;
                                    mj22 = mj2;
                                }
                            } else {
                                it = it4;
                                l2 = l3;
                                this.d.o().a(this.a, l4, j, mj23);
                            }
                            arrayList = new ArrayList<>();
                            for (oj2 next3 : mj23.n()) {
                                this.d.n();
                                if (ia3.a(next2, next3.n()) == null) {
                                    arrayList.add(next3);
                                }
                            }
                            if (!arrayList.isEmpty()) {
                                arrayList.addAll(n);
                                n = arrayList;
                            } else {
                                this.d.b().w().a("No unique parameters in main event. eventName", str5);
                            }
                            p = str5;
                            mj22 = mj23;
                            l3 = l2;
                        }
                        it4 = it2;
                    } else {
                        it = it4;
                        if (z3) {
                            this.d.n();
                            long j4 = 0L;
                            Object b2 = ia3.b(next2, "_epc");
                            if (b2 != null) {
                                j4 = b2;
                            }
                            long longValue = ((Long) j4).longValue();
                            if (longValue <= 0) {
                                this.d.b().w().a("Complex event with zero extra param count. eventName", p);
                            } else {
                                this.d.o().a(this.a, l4, longValue, next2);
                            }
                            j = longValue;
                            mj22 = next2;
                            l3 = l4;
                            sa3 sa3222 = this.d;
                            String str722 = this.a;
                            boolean d522 = sa3222.l().d(str722, l03.v0);
                            a2 = sa3222.o().a(str722, next2.p());
                            if (a2 != null) {
                                mj2 = mj22;
                                sa3222.b().w().a("Event aggregate wasn't created during raw event logging. appId, event", t43.a(str722), sa3222.i().a(p));
                                if (d522) {
                                    f03 = new f03(str722, next2.p(), 1, 1, 1, next2.r(), 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                                } else {
                                    f03 = new f03(str722, next2.p(), 1, 1, next2.r(), 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                                }
                            } else {
                                mj2 = mj22;
                                if (d522) {
                                    f03 = new f03(a2.a, a2.b, a2.c + 1, a2.d + 1, a2.e + 1, a2.f, a2.g, a2.h, a2.i, a2.j, a2.k);
                                } else {
                                    f03 = new f03(a2.a, a2.b, a2.c + 1, a2.d + 1, a2.e, a2.f, a2.g, a2.h, a2.i, a2.j, a2.k);
                                }
                            }
                            this.d.o().a(f03);
                            long j222 = f03.c;
                            map = (Map) p4Var2.get(p);
                            if (map == null) {
                                map = this.d.o().f(this.a, p);
                                if (map == null) {
                                    map = new p4();
                                }
                                p4Var2.put(p, map);
                            }
                            for (Integer intValue5 : map.keySet()) {
                                int intValue6 = intValue5.intValue();
                                p4 p4Var3 = p4Var2;
                                Long l6 = l3;
                                if (this.b.contains(Integer.valueOf(intValue6))) {
                                    this.d.b().B().a(str4, Integer.valueOf(intValue6));
                                    p4Var2 = p4Var3;
                                    l3 = l6;
                                } else {
                                    Iterator it5 = ((List) map.get(Integer.valueOf(intValue6))).iterator();
                                    boolean z4 = true;
                                    while (it5.hasNext()) {
                                        wi2 wi2 = (wi2) it5.next();
                                        Iterator it6 = it5;
                                        wa3 wa3 = new wa3(this.d, this.a, intValue6, wi2);
                                        z4 = wa3.a(next2, p, n, j222, f03, a(intValue6, wi2.o()));
                                        a(intValue6).a((za3) wa3);
                                        it5 = it6;
                                        map = map;
                                        j = j;
                                    }
                                    Map map3 = map;
                                    long j5 = j;
                                    if (!z4) {
                                        this.b.add(Integer.valueOf(intValue6));
                                    }
                                    p4Var2 = p4Var3;
                                    l3 = l6;
                                    map = map3;
                                    j = j5;
                                }
                            }
                            Long l522 = l3;
                            long j322 = j;
                            it4 = it;
                            mj22 = mj2;
                        }
                    }
                    sa3 sa32222 = this.d;
                    String str7222 = this.a;
                    boolean d5222 = sa32222.l().d(str7222, l03.v0);
                    a2 = sa32222.o().a(str7222, next2.p());
                    if (a2 != null) {
                    }
                    this.d.o().a(f03);
                    long j2222 = f03.c;
                    map = (Map) p4Var2.get(p);
                    if (map == null) {
                    }
                    while (r14.hasNext()) {
                    }
                    Long l5222 = l3;
                    long j3222 = j;
                    it4 = it;
                    mj22 = mj2;
                }
            }
            ArrayList arrayList2 = new ArrayList();
            if (!list2.isEmpty()) {
                p4 p4Var4 = new p4();
                for (uj2 next4 : list2) {
                    arrayList2.add(next4.p());
                    String p2 = next4.p();
                    Map map4 = (Map) p4Var4.get(p2);
                    if (map4 == null) {
                        map4 = this.d.o().g(this.a, p2);
                        if (map4 == null) {
                            map4 = new p4();
                        }
                        p4Var4.put(p2, map4);
                    }
                    Iterator it7 = map4.keySet().iterator();
                    while (true) {
                        if (!it7.hasNext()) {
                            Long l7 = l;
                            break;
                        }
                        int intValue7 = ((Integer) it7.next()).intValue();
                        if (this.b.contains(Integer.valueOf(intValue7))) {
                            this.d.b().B().a(str4, Integer.valueOf(intValue7));
                            break;
                        }
                        Iterator it8 = ((List) map4.get(Integer.valueOf(intValue7))).iterator();
                        boolean z5 = true;
                        while (true) {
                            if (!it8.hasNext()) {
                                Long l8 = l;
                                str = str4;
                                break;
                            }
                            zi2 zi2 = (zi2) it8.next();
                            if (this.d.b().a(2)) {
                                str = str4;
                                this.d.b().B().a("Evaluating filter. audience, filter, property", Integer.valueOf(intValue7), zi2.n() ? Integer.valueOf(zi2.o()) : null, this.d.i().c(zi2.p()));
                                this.d.b().B().a("Filter definition", this.d.n().a(zi2));
                            } else {
                                str = str4;
                            }
                            if (!zi2.n() || zi2.o() > 256) {
                                Long l9 = l;
                                v43 w = this.d.b().w();
                                Object a6 = t43.a(this.a);
                            } else {
                                ya3 ya3 = new ya3(this.d, this.a, intValue7, zi2);
                                z5 = ya3.a(l, next4, a(intValue7, zi2.o()));
                                a(intValue7).a((za3) ya3);
                                str4 = str;
                            }
                        }
                        if (!z5) {
                            this.b.add(Integer.valueOf(intValue7));
                        }
                        str4 = str;
                    }
                }
            }
            d3 = this.d.l().d(this.a, l03.A0);
            Map p4Var5 = new p4();
            if (d3) {
                p4Var5 = this.d.o().b(this.a, (List<String>) arrayList2);
            }
            ArrayList arrayList3 = new ArrayList();
            Set<Integer> keySet = this.c.keySet();
            keySet.removeAll(this.b);
            for (Integer intValue8 : keySet) {
                int intValue9 = intValue8.intValue();
                kj2 a7 = this.c.get(Integer.valueOf(intValue9)).a(intValue9, z, (List<Integer>) (List) p4Var5.get(Integer.valueOf(intValue9)));
                arrayList3.add(a7);
                yz2 o3 = this.d.o();
                String str8 = this.a;
                sj2 p3 = a7.p();
                o3.r();
                o3.g();
                w12.b(str8);
                w12.a(p3);
                byte[] f2 = p3.f();
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("app_id", str8);
                contentValues2.put("audience_id", Integer.valueOf(intValue9));
                contentValues2.put("current_results", f2);
                try {
                    try {
                        if (o3.v().insertWithOnConflict("audience_filter_values", (String) null, contentValues2, 5) == -1) {
                            o3.b().t().a("Failed to insert filter results (got -1). appId", t43.a(str8));
                        }
                    } catch (SQLiteException e5) {
                        e = e5;
                        o3.b().t().a("Error storing filter results. appId", t43.a(str8), e);
                    }
                } catch (SQLiteException e6) {
                    e = e6;
                    o3.b().t().a("Error storing filter results. appId", t43.a(str8), e);
                }
            }
            return arrayList3;
        }
        z = false;
        d2 = this.d.l().d(this.a, l03.u0);
        boolean d42 = this.d.l().d(this.a, l03.v0);
        o = this.d.o();
        str2 = this.a;
        o.r();
        o.g();
        w12.b(str2);
        ContentValues contentValues3 = new ContentValues();
        contentValues3.put("current_session_count", 0);
        try {
            o.v().update("events", contentValues3, "app_id = ?", new String[]{str2});
        } catch (SQLiteException e7) {
            o.b().t().a("Error resetting session-scoped event counts. appId", t43.a(str2), e7);
        }
        f = this.d.o().f(this.a);
        HashSet<Integer> hashSet2 = new HashSet<>(f.keySet());
        if (d2) {
        }
        map2 = f;
        while (r3.hasNext()) {
        }
        String str42 = "Skipping failed audience ID";
        if (!list.isEmpty()) {
        }
        ArrayList arrayList22 = new ArrayList();
        if (!list2.isEmpty()) {
        }
        d3 = this.d.l().d(this.a, l03.A0);
        Map p4Var52 = new p4();
        if (d3) {
        }
        ArrayList arrayList32 = new ArrayList();
        Set<Integer> keySet2 = this.c.keySet();
        keySet2.removeAll(this.b);
        while (r5.hasNext()) {
        }
        return arrayList32;
    }

    @DexIgnore
    public final ua3 a(int i) {
        if (this.c.containsKey(Integer.valueOf(i))) {
            return this.c.get(Integer.valueOf(i));
        }
        ua3 ua3 = new ua3(this.d, this.a, (va3) null);
        this.c.put(Integer.valueOf(i), ua3);
        return ua3;
    }

    @DexIgnore
    public final boolean a(int i, int i2) {
        if (this.c.get(Integer.valueOf(i)) == null) {
            return false;
        }
        return this.c.get(Integer.valueOf(i)).c.get(i2);
    }
}
