package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pn {
    @DexIgnore
    public static pn e;
    @DexIgnore
    public jn a;
    @DexIgnore
    public kn b;
    @DexIgnore
    public nn c;
    @DexIgnore
    public on d;

    @DexIgnore
    public pn(Context context, to toVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = new jn(applicationContext, toVar);
        this.b = new kn(applicationContext, toVar);
        this.c = new nn(applicationContext, toVar);
        this.d = new on(applicationContext, toVar);
    }

    @DexIgnore
    public static synchronized pn a(Context context, to toVar) {
        pn pnVar;
        synchronized (pn.class) {
            if (e == null) {
                e = new pn(context, toVar);
            }
            pnVar = e;
        }
        return pnVar;
    }

    @DexIgnore
    public kn b() {
        return this.b;
    }

    @DexIgnore
    public nn c() {
        return this.c;
    }

    @DexIgnore
    public on d() {
        return this.d;
    }

    @DexIgnore
    public jn a() {
        return this.a;
    }
}
