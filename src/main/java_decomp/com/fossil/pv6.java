package com.fossil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pv6 extends lv6 {
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ Charset c;

    @DexIgnore
    public pv6(String str, String str2, Charset charset) throws UnsupportedEncodingException {
        super(str2);
        if (str != null) {
            charset = charset == null ? Charset.forName("US-ASCII") : charset;
            this.b = str.getBytes(charset.name());
            this.c = charset;
            return;
        }
        throw new IllegalArgumentException("Text may not be null");
    }

    @DexIgnore
    public String a() {
        return "8bit";
    }

    @DexIgnore
    public String b() {
        return this.c.name();
    }

    @DexIgnore
    public String d() {
        return null;
    }

    @DexIgnore
    public long getContentLength() {
        return (long) this.b.length;
    }

    @DexIgnore
    public void writeTo(OutputStream outputStream) throws IOException {
        if (outputStream != null) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.b);
            byte[] bArr = new byte[4096];
            while (true) {
                int read = byteArrayInputStream.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    outputStream.flush();
                    return;
                }
            }
        } else {
            throw new IllegalArgumentException("Output stream may not be null");
        }
    }

    @DexIgnore
    public pv6(String str) throws UnsupportedEncodingException {
        this(str, "text/plain", (Charset) null);
    }
}
