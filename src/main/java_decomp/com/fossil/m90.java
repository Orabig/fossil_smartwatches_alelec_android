package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m90 extends n90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ f90 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<m90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new m90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new m90[i];
        }
    }

    @DexIgnore
    public m90(byte b, bo0 bo0) {
        super(e90.DEVICE_CONFIG_SYNC, b);
        this.c = f90.b.a(bo0);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(m90.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((m90) obj).c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.DeviceConfigSyncNotification");
    }

    @DexIgnore
    public final f90 getAction() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c.ordinal());
        }
    }

    @DexIgnore
    public /* synthetic */ m90(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.c = f90.values()[parcel.readInt()];
    }
}
