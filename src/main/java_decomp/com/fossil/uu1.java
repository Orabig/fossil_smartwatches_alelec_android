package com.fossil;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uu1 implements Comparator<Scope> {
    @DexIgnore
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return ((Scope) obj).p().compareTo(((Scope) obj2).p());
    }
}
