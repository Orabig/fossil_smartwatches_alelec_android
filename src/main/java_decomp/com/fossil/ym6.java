package com.fossil;

import com.fossil.af6;
import com.fossil.jo6;
import com.fossil.rm6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ym6 implements rm6, sk6, gn6, tp6 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(ym6.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _state;
    @DexIgnore
    public volatile qk6 parentHandle;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> extends mk6<T> {
        @DexIgnore
        public /* final */ ym6 h;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xe6<? super T> xe6, ym6 ym6) {
            super(xe6, 1);
            wg6.b(xe6, "delegate");
            wg6.b(ym6, "job");
            this.h = ym6;
        }

        @DexIgnore
        public Throwable a(rm6 rm6) {
            Throwable th;
            wg6.b(rm6, "parent");
            Object d = this.h.d();
            if ((d instanceof c) && (th = ((c) d).rootCause) != null) {
                return th;
            }
            if (d instanceof vk6) {
                return ((vk6) d).a;
            }
            return rm6.k();
        }

        @DexIgnore
        public String i() {
            return "AwaitContinuation";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xm6<rm6> {
        @DexIgnore
        public /* final */ ym6 e;
        @DexIgnore
        public /* final */ c f;
        @DexIgnore
        public /* final */ rk6 g;
        @DexIgnore
        public /* final */ Object h;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ym6 ym6, c cVar, rk6 rk6, Object obj) {
            super(rk6.e);
            wg6.b(ym6, "parent");
            wg6.b(cVar, Constants.STATE);
            wg6.b(rk6, "child");
            this.e = ym6;
            this.f = cVar;
            this.g = rk6;
            this.h = obj;
        }

        @DexIgnore
        public void b(Throwable th) {
            this.e.a(this.f, this.g, this.h);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            b((Throwable) obj);
            return cd6.a;
        }

        @DexIgnore
        public String toString() {
            return "ChildCompletion[" + this.g + ", " + this.h + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements mm6 {
        @DexIgnore
        public volatile Object _exceptionsHolder;
        @DexIgnore
        public /* final */ dn6 a;
        @DexIgnore
        public volatile boolean isCompleting;
        @DexIgnore
        public volatile Throwable rootCause;

        @DexIgnore
        public c(dn6 dn6, boolean z, Throwable th) {
            wg6.b(dn6, "list");
            this.a = dn6;
            this.isCompleting = z;
            this.rootCause = th;
        }

        @DexIgnore
        public dn6 a() {
            return this.a;
        }

        @DexIgnore
        public final List<Throwable> b(Throwable th) {
            ArrayList<Throwable> arrayList;
            Object obj = this._exceptionsHolder;
            if (obj == null) {
                arrayList = b();
            } else if (obj instanceof Throwable) {
                ArrayList<Throwable> b = b();
                b.add(obj);
                arrayList = b;
            } else if (obj instanceof ArrayList) {
                arrayList = (ArrayList) obj;
            } else {
                throw new IllegalStateException(("State is " + obj).toString());
            }
            Throwable th2 = this.rootCause;
            if (th2 != null) {
                arrayList.add(0, th2);
            }
            if (th != null && (!wg6.a((Object) th, (Object) th2))) {
                arrayList.add(th);
            }
            this._exceptionsHolder = zm6.a;
            return arrayList;
        }

        @DexIgnore
        public final boolean c() {
            return this.rootCause != null;
        }

        @DexIgnore
        public final boolean d() {
            return this._exceptionsHolder == zm6.a;
        }

        @DexIgnore
        public boolean isActive() {
            return this.rootCause == null;
        }

        @DexIgnore
        public String toString() {
            return "Finishing[cancelling=" + c() + ", completing=" + this.isCompleting + ", rootCause=" + this.rootCause + ", exceptions=" + this._exceptionsHolder + ", list=" + a() + ']';
        }

        @DexIgnore
        public final void a(Throwable th) {
            wg6.b(th, "exception");
            Throwable th2 = this.rootCause;
            if (th2 == null) {
                this.rootCause = th;
            } else if (th != th2) {
                Object obj = this._exceptionsHolder;
                if (obj == null) {
                    this._exceptionsHolder = th;
                } else if (obj instanceof Throwable) {
                    if (th != obj) {
                        ArrayList<Throwable> b = b();
                        b.add(obj);
                        b.add(th);
                        this._exceptionsHolder = b;
                    }
                } else if (obj instanceof ArrayList) {
                    ((ArrayList) obj).add(th);
                } else {
                    throw new IllegalStateException(("State is " + obj).toString());
                }
            }
        }

        @DexIgnore
        public final ArrayList<Throwable> b() {
            return new ArrayList<>(4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends jo6.a {
        @DexIgnore
        public /* final */ /* synthetic */ ym6 d;
        @DexIgnore
        public /* final */ /* synthetic */ Object e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(jo6 jo6, jo6 jo62, ym6 ym6, Object obj) {
            super(jo62);
            this.d = ym6;
            this.e = obj;
        }

        @DexIgnore
        /* renamed from: a */
        public Object c(jo6 jo6) {
            wg6.b(jo6, "affected");
            if (this.d.d() == this.e) {
                return null;
            }
            return io6.a();
        }
    }

    @DexIgnore
    public ym6(boolean z) {
        this._state = z ? zm6.c : zm6.b;
    }

    @DexIgnore
    public void a(Object obj, int i) {
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public final boolean b(mm6 mm6, Object obj, int i) {
        if (nl6.a()) {
            if (!((mm6 instanceof dm6) || (mm6 instanceof xm6))) {
                throw new AssertionError();
            }
        }
        if (nl6.a() && !(!(obj instanceof vk6))) {
            throw new AssertionError();
        } else if (!a.compareAndSet(this, mm6, zm6.a(obj))) {
            return false;
        } else {
            g((Throwable) null);
            g(obj);
            a(mm6, obj, i);
            return true;
        }
    }

    @DexIgnore
    public boolean c() {
        return false;
    }

    @DexIgnore
    public final boolean c(Throwable th) {
        if (f()) {
            return true;
        }
        boolean z = th instanceof CancellationException;
        qk6 qk6 = this.parentHandle;
        if (qk6 == null || qk6 == en6.a) {
            return z;
        }
        if (qk6.a(th) || z) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public /* synthetic */ void cancel() {
        a((CancellationException) null);
    }

    @DexIgnore
    public boolean d(Throwable th) {
        wg6.b(th, "cause");
        if (th instanceof CancellationException) {
            return true;
        }
        if (!a((Object) th) || !b()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean e() {
        return !(d() instanceof mm6);
    }

    @DexIgnore
    public boolean e(Throwable th) {
        wg6.b(th, "exception");
        return false;
    }

    @DexIgnore
    public void f(Throwable th) {
        wg6.b(th, "exception");
        throw th;
    }

    @DexIgnore
    public boolean f() {
        return false;
    }

    @DexIgnore
    public <R> R fold(R r, ig6<? super R, ? super af6.b, ? extends R> ig6) {
        wg6.b(ig6, "operation");
        return rm6.a.a(this, r, ig6);
    }

    @DexIgnore
    public String g() {
        return ol6.a((Object) this);
    }

    @DexIgnore
    public void g(Object obj) {
    }

    @DexIgnore
    public void g(Throwable th) {
    }

    @DexIgnore
    public <E extends af6.b> E get(af6.c<E> cVar) {
        wg6.b(cVar, "key");
        return rm6.a.a((rm6) this, cVar);
    }

    @DexIgnore
    public final af6.c<?> getKey() {
        return rm6.n;
    }

    @DexIgnore
    public final int h(Object obj) {
        if (obj instanceof dm6) {
            if (((dm6) obj).isActive()) {
                return 0;
            }
            if (!a.compareAndSet(this, obj, zm6.c)) {
                return -1;
            }
            h();
            return 1;
        } else if (!(obj instanceof lm6)) {
            return 0;
        } else {
            if (!a.compareAndSet(this, obj, ((lm6) obj).a())) {
                return -1;
            }
            h();
            return 1;
        }
    }

    @DexIgnore
    public void h() {
    }

    @DexIgnore
    public final String i() {
        return g() + '{' + i(d()) + '}';
    }

    @DexIgnore
    public boolean isActive() {
        Object d2 = d();
        return (d2 instanceof mm6) && ((mm6) d2).isActive();
    }

    @DexIgnore
    public final CancellationException k() {
        Object d2 = d();
        if (d2 instanceof c) {
            Throwable th = ((c) d2).rootCause;
            if (th != null) {
                CancellationException a2 = a(th, ol6.a((Object) this) + " is cancelling");
                if (a2 != null) {
                    return a2;
                }
            }
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (d2 instanceof mm6) {
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (d2 instanceof vk6) {
            return a(this, ((vk6) d2).a, (String) null, 1, (Object) null);
        } else {
            return new sm6(ol6.a((Object) this) + " has completed normally", (Throwable) null, this);
        }
    }

    @DexIgnore
    public CancellationException l() {
        Throwable th;
        Object d2 = d();
        CancellationException cancellationException = null;
        if (d2 instanceof c) {
            th = ((c) d2).rootCause;
        } else if (d2 instanceof vk6) {
            th = ((vk6) d2).a;
        } else if (!(d2 instanceof mm6)) {
            th = null;
        } else {
            throw new IllegalStateException(("Cannot be cancelling child in this state: " + d2).toString());
        }
        if (th instanceof CancellationException) {
            cancellationException = th;
        }
        CancellationException cancellationException2 = cancellationException;
        if (cancellationException2 != null) {
            return cancellationException2;
        }
        return new sm6("Parent job is " + i(d2), th, this);
    }

    @DexIgnore
    public af6 minusKey(af6.c<?> cVar) {
        wg6.b(cVar, "key");
        return rm6.a.b(this, cVar);
    }

    @DexIgnore
    public af6 plus(af6 af6) {
        wg6.b(af6, "context");
        return rm6.a.a((rm6) this, af6);
    }

    @DexIgnore
    public final boolean start() {
        int h;
        do {
            h = h(d());
            if (h == 0) {
                return false;
            }
        } while (h != 1);
        return true;
    }

    @DexIgnore
    public String toString() {
        return i() + '@' + ol6.b(this);
    }

    @DexIgnore
    public final void a(rm6 rm6) {
        if (nl6.a()) {
            if (!(this.parentHandle == null)) {
                throw new AssertionError();
            }
        }
        if (rm6 == null) {
            this.parentHandle = en6.a;
            return;
        }
        rm6.start();
        qk6 a2 = rm6.a((sk6) this);
        this.parentHandle = a2;
        if (e()) {
            a2.dispose();
            this.parentHandle = en6.a;
        }
    }

    @DexIgnore
    public final Throwable e(Object obj) {
        if (!(obj instanceof vk6)) {
            obj = null;
        }
        vk6 vk6 = (vk6) obj;
        if (vk6 != null) {
            return vk6.a;
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003c, code lost:
        if (r8 == null) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003e, code lost:
        a(((com.fossil.ym6.c) r2).a(), r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0047, code lost:
        return true;
     */
    @DexIgnore
    public final boolean f(Object obj) {
        Throwable th = null;
        while (true) {
            Object d2 = d();
            if (d2 instanceof c) {
                synchronized (d2) {
                    if (((c) d2).d()) {
                        return false;
                    }
                    boolean c2 = ((c) d2).c();
                    if (obj != null || !c2) {
                        if (th == null) {
                            th = d(obj);
                        }
                        ((c) d2).a(th);
                    }
                    Throwable th2 = ((c) d2).rootCause;
                    if (!(!c2)) {
                        th2 = null;
                    }
                }
            } else if (!(d2 instanceof mm6)) {
                return false;
            } else {
                if (th == null) {
                    th = d(obj);
                }
                mm6 mm6 = (mm6) d2;
                if (!mm6.isActive()) {
                    int a2 = a(d2, (Object) new vk6(th, false, 2, (qg6) null), 0);
                    if (a2 == 0) {
                        throw new IllegalStateException(("Cannot happen in " + d2).toString());
                    } else if (a2 == 1 || a2 == 2) {
                        return true;
                    } else {
                        if (a2 != 3) {
                            throw new IllegalStateException("unexpected result".toString());
                        }
                    }
                } else if (a(mm6, th)) {
                    return true;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public final String i(Object obj) {
        if (obj instanceof c) {
            c cVar = (c) obj;
            if (cVar.c()) {
                return "Cancelling";
            }
            if (cVar.isCompleting) {
                return "Completing";
            }
            return "Active";
        } else if (!(obj instanceof mm6)) {
            return obj instanceof vk6 ? "Cancelled" : "Completed";
        } else {
            if (((mm6) obj).isActive()) {
                return "Active";
            }
            return "New";
        }
    }

    @DexIgnore
    public final Throwable d(Object obj) {
        if (obj != null ? obj instanceof Throwable : true) {
            return obj != null ? (Throwable) obj : a();
        }
        if (obj != null) {
            return ((gn6) obj).l();
        }
        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.ParentJob");
    }

    @DexIgnore
    public final Object d() {
        while (true) {
            Object obj = this._state;
            if (!(obj instanceof po6)) {
                return obj;
            }
            ((po6) obj).a(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0054, code lost:
        if (r3 == null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0056, code lost:
        a(r0, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0059, code lost:
        r8 = a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x005d, code lost:
        if (r8 == null) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0063, code lost:
        if (b(r2, r8, r9) == false) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0065, code lost:
        return 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x006b, code lost:
        if (a(r2, r9, r10) == false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x006d, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x006e, code lost:
        return 3;
     */
    @DexIgnore
    public final int c(mm6 mm6, Object obj, int i) {
        dn6 b2 = b(mm6);
        if (b2 == null) {
            return 3;
        }
        Throwable th = null;
        c cVar = (c) (!(mm6 instanceof c) ? null : mm6);
        if (cVar == null) {
            cVar = new c(b2, false, (Throwable) null);
        }
        synchronized (cVar) {
            if (cVar.isCompleting) {
                return 0;
            }
            cVar.isCompleting = true;
            if (cVar != mm6 && !a.compareAndSet(this, mm6, cVar)) {
                return 3;
            }
            if (!cVar.d()) {
                boolean c2 = cVar.c();
                vk6 vk6 = (vk6) (!(obj instanceof vk6) ? null : obj);
                if (vk6 != null) {
                    cVar.a(vk6.a);
                }
                Throwable th2 = cVar.rootCause;
                if (!c2) {
                    th = th2;
                }
                cd6 cd6 = cd6.a;
            } else {
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
        }
    }

    @DexIgnore
    public boolean b(Throwable th) {
        return a((Object) th) && b();
    }

    @DexIgnore
    public final dn6 b(mm6 mm6) {
        dn6 a2 = mm6.a();
        if (a2 != null) {
            return a2;
        }
        if (mm6 instanceof dm6) {
            return new dn6();
        }
        if (mm6 instanceof xm6) {
            a((xm6<?>) (xm6) mm6);
            return null;
        }
        throw new IllegalStateException(("State should have list: " + mm6).toString());
    }

    @DexIgnore
    public final boolean a(c cVar, Object obj, int i) {
        boolean c2;
        Throwable a2;
        boolean z = false;
        if (!(d() == cVar)) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        } else if (!(!cVar.d())) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        } else if (cVar.isCompleting) {
            vk6 vk6 = (vk6) (!(obj instanceof vk6) ? null : obj);
            Throwable th = vk6 != null ? vk6.a : null;
            synchronized (cVar) {
                c2 = cVar.c();
                List<Throwable> b2 = cVar.b(th);
                a2 = a(cVar, (List<? extends Throwable>) b2);
                if (a2 != null) {
                    a(a2, (List<? extends Throwable>) b2);
                }
            }
            if (!(a2 == null || a2 == th)) {
                obj = new vk6(a2, false, 2, (qg6) null);
            }
            if (a2 != null) {
                if (c(a2) || e(a2)) {
                    z = true;
                }
                if (z) {
                    if (obj != null) {
                        ((vk6) obj).b();
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally");
                    }
                }
            }
            if (!c2) {
                g(a2);
            }
            g(obj);
            if (a.compareAndSet(this, cVar, zm6.a(obj))) {
                a((mm6) cVar, obj, i);
                return true;
            }
            throw new IllegalArgumentException(("Unexpected state: " + this._state + ", expected: " + cVar + ", update: " + obj).toString());
        } else {
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    @DexIgnore
    public final boolean b(c cVar, rk6 rk6, Object obj) {
        while (rm6.a.a(rk6.e, false, false, new b(this, cVar, rk6, obj), 1, (Object) null) == en6.a) {
            rk6 = a((jo6) rk6);
            if (rk6 == null) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final Object b(xe6<Object> xe6) {
        Object d2;
        do {
            d2 = d();
            if (!(d2 instanceof mm6)) {
                if (!(d2 instanceof vk6)) {
                    return zm6.b(d2);
                }
                Throwable th = ((vk6) d2).a;
                if (nl6.d()) {
                    ug6.a(0);
                    if (!(xe6 instanceof kf6)) {
                        throw th;
                    }
                    throw to6.b(th, (kf6) xe6);
                }
                throw th;
            }
        } while (h(d2) < 0);
        return c(xe6);
    }

    @DexIgnore
    public final /* synthetic */ Object c(xe6<Object> xe6) {
        a aVar = new a(ef6.a(xe6), this);
        nk6.a((lk6<?>) aVar, a((hg6<? super Throwable, cd6>) new in6(this, aVar)));
        Object e = aVar.e();
        if (e == ff6.a()) {
            nf6.c(xe6);
        }
        return e;
    }

    @DexIgnore
    public final boolean c(Object obj) {
        int a2;
        do {
            Object d2 = d();
            if (!(d2 instanceof mm6) || (((d2 instanceof c) && ((c) d2).isCompleting) || (a2 = a(d2, (Object) new vk6(d(obj), false, 2, (qg6) null), 0)) == 0)) {
                return false;
            }
            if (a2 == 1 || a2 == 2) {
                return true;
            }
        } while (a2 == 3);
        throw new IllegalStateException("unexpected result".toString());
    }

    @DexIgnore
    public final void b(dn6 dn6, Throwable th) {
        Object c2 = dn6.c();
        if (c2 != null) {
            al6 al6 = null;
            for (jo6 jo6 = (jo6) c2; !wg6.a((Object) jo6, (Object) dn6); jo6 = jo6.d()) {
                if (jo6 instanceof xm6) {
                    xm6 xm6 = (xm6) jo6;
                    try {
                        xm6.b(th);
                    } catch (Throwable th2) {
                        if (al6 != null) {
                            ec6.a(al6, th2);
                            if (al6 != null) {
                            }
                        }
                        al6 = new al6("Exception in completion handler " + xm6 + " for " + this, th2);
                        cd6 cd6 = cd6.a;
                    }
                }
            }
            if (al6 != null) {
                f((Throwable) al6);
                return;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public final Throwable a(c cVar, List<? extends Throwable> list) {
        T t;
        if (!list.isEmpty()) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (!(((Throwable) t) instanceof CancellationException)) {
                    break;
                }
            }
            Throwable th = (Throwable) t;
            return th != null ? th : (Throwable) list.get(0);
        } else if (cVar.c()) {
            return a();
        } else {
            return null;
        }
    }

    @DexIgnore
    public final void a(Throwable th, List<? extends Throwable> list) {
        if (list.size() > 1) {
            Set a2 = do6.a(list.size());
            Throwable b2 = to6.b(th);
            for (Throwable b3 : list) {
                Throwable b4 = to6.b(b3);
                if (b4 != th && b4 != b2 && !(b4 instanceof CancellationException) && a2.add(b4)) {
                    ec6.a(th, b4);
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x001d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x000d A[ADDED_TO_REGION] */
    public final void b(xm6<?> xm6) {
        Object d2;
        wg6.b(xm6, "node");
        do {
            d2 = d();
            if (!(d2 instanceof xm6)) {
                if (d2 != xm6 || a.compareAndSet(this, d2, zm6.c)) {
                }
                d2 = d();
                if (!(d2 instanceof xm6)) {
                    if ((d2 instanceof mm6) && ((mm6) d2).a() != null) {
                        xm6.j();
                        return;
                    }
                    return;
                }
            }
        } while (a.compareAndSet(this, d2, zm6.c));
    }

    @DexIgnore
    public final void a(mm6 mm6, Object obj, int i) {
        qk6 qk6 = this.parentHandle;
        if (qk6 != null) {
            qk6.dispose();
            this.parentHandle = en6.a;
        }
        Throwable th = null;
        vk6 vk6 = (vk6) (!(obj instanceof vk6) ? null : obj);
        if (vk6 != null) {
            th = vk6.a;
        }
        if (mm6 instanceof xm6) {
            try {
                ((xm6) mm6).b(th);
            } catch (Throwable th2) {
                f((Throwable) new al6("Exception in completion handler " + mm6 + " for " + this, th2));
            }
        } else {
            dn6 a2 = mm6.a();
            if (a2 != null) {
                b(a2, th);
            }
        }
        a(obj, i);
    }

    @DexIgnore
    public final boolean b(Object obj, int i) {
        int a2;
        do {
            a2 = a(d(), obj, i);
            if (a2 == 0) {
                throw new IllegalStateException("Job " + this + " is already complete or completing, " + "but is being completed with " + obj, e(obj));
            } else if (a2 == 1) {
                return true;
            } else {
                if (a2 == 2) {
                    return false;
                }
            }
        } while (a2 == 3);
        throw new IllegalStateException("unexpected result".toString());
    }

    @DexIgnore
    public final void a(dn6 dn6, Throwable th) {
        g(th);
        Object c2 = dn6.c();
        if (c2 != null) {
            al6 al6 = null;
            for (jo6 jo6 = (jo6) c2; !wg6.a((Object) jo6, (Object) dn6); jo6 = jo6.d()) {
                if (jo6 instanceof tm6) {
                    xm6 xm6 = (xm6) jo6;
                    try {
                        xm6.b(th);
                    } catch (Throwable th2) {
                        if (al6 != null) {
                            ec6.a(al6, th2);
                            if (al6 != null) {
                            }
                        }
                        al6 = new al6("Exception in completion handler " + xm6 + " for " + this, th2);
                        cd6 cd6 = cd6.a;
                    }
                }
            }
            if (al6 != null) {
                f((Throwable) al6);
            }
            c(th);
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public static /* synthetic */ CancellationException a(ym6 ym6, Throwable th, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                str = null;
            }
            return ym6.a(th, str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toCancellationException");
    }

    @DexIgnore
    public final CancellationException a(Throwable th, String str) {
        wg6.b(th, "$this$toCancellationException");
        CancellationException cancellationException = (CancellationException) (!(th instanceof CancellationException) ? null : th);
        if (cancellationException == null) {
            if (str == null) {
                str = ol6.a((Object) th) + " was cancelled";
            }
            cancellationException = new sm6(str, th, this);
        }
        return cancellationException;
    }

    @DexIgnore
    public final am6 a(hg6<? super Throwable, cd6> hg6) {
        wg6.b(hg6, "handler");
        return a(false, true, hg6);
    }

    @DexIgnore
    public final xm6<?> a(hg6<? super Throwable, cd6> hg6, boolean z) {
        boolean z2 = true;
        tm6 tm6 = null;
        if (z) {
            if (hg6 instanceof tm6) {
                tm6 = hg6;
            }
            tm6 tm62 = tm6;
            if (tm62 != null) {
                if (tm62.d != this) {
                    z2 = false;
                }
                if (!z2) {
                    throw new IllegalArgumentException("Failed requirement.".toString());
                } else if (tm62 != null) {
                    return tm62;
                }
            }
            return new pm6(this, hg6);
        }
        if (hg6 instanceof xm6) {
            tm6 = hg6;
        }
        xm6<?> xm6 = tm6;
        if (xm6 != null) {
            if (xm6.d != this || (xm6 instanceof tm6)) {
                z2 = false;
            }
            if (!z2) {
                throw new IllegalArgumentException("Failed requirement.".toString());
            } else if (xm6 != null) {
                return xm6;
            }
        }
        return new qm6(this, hg6);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.fossil.lm6] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void a(dm6 dm6) {
        dn6 dn6 = new dn6();
        if (!dm6.isActive()) {
            dn6 = new lm6(dn6);
        }
        a.compareAndSet(this, dm6, dn6);
    }

    @DexIgnore
    public final void a(xm6<?> xm6) {
        xm6.a(new dn6());
        a.compareAndSet(this, xm6, xm6.d());
    }

    @DexIgnore
    public void a(CancellationException cancellationException) {
        b((Throwable) cancellationException);
    }

    @DexIgnore
    public final void a(gn6 gn6) {
        wg6.b(gn6, "parentJob");
        a((Object) gn6);
    }

    @DexIgnore
    public final boolean a(Throwable th) {
        return a((Object) th);
    }

    @DexIgnore
    public final boolean a(Object obj) {
        if (!c() || !c(obj)) {
            return f(obj);
        }
        return true;
    }

    @DexIgnore
    public final sm6 a() {
        return new sm6("Job was cancelled", (Throwable) null, this);
    }

    @DexIgnore
    public final boolean a(mm6 mm6, Throwable th) {
        if (nl6.a() && !(!(mm6 instanceof c))) {
            throw new AssertionError();
        } else if (!nl6.a() || mm6.isActive()) {
            dn6 b2 = b(mm6);
            if (b2 == null) {
                return false;
            }
            if (!a.compareAndSet(this, mm6, new c(b2, false, th))) {
                return false;
            }
            a(b2, th);
            return true;
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final int a(Object obj, Object obj2, int i) {
        if (!(obj instanceof mm6)) {
            return 0;
        }
        if (((obj instanceof dm6) || (obj instanceof xm6)) && !(obj instanceof rk6) && !(obj2 instanceof vk6)) {
            return !b((mm6) obj, obj2, i) ? 3 : 1;
        }
        return c((mm6) obj, obj2, i);
    }

    @DexIgnore
    public final rk6 a(mm6 mm6) {
        rk6 rk6 = (rk6) (!(mm6 instanceof rk6) ? null : mm6);
        if (rk6 != null) {
            return rk6;
        }
        dn6 a2 = mm6.a();
        if (a2 != null) {
            return a((jo6) a2);
        }
        return null;
    }

    @DexIgnore
    public final void a(c cVar, rk6 rk6, Object obj) {
        if (d() == cVar) {
            rk6 a2 = a((jo6) rk6);
            if ((a2 == null || !b(cVar, a2, obj)) && a(cVar, obj, 0)) {
            }
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @DexIgnore
    public final rk6 a(jo6 jo6) {
        while (jo6.h()) {
            jo6 = jo6.f();
        }
        while (true) {
            jo6 = jo6.d();
            if (!jo6.h()) {
                if (jo6 instanceof rk6) {
                    return (rk6) jo6;
                }
                if (jo6 instanceof dn6) {
                    return null;
                }
            }
        }
    }

    @DexIgnore
    public final qk6 a(sk6 sk6) {
        wg6.b(sk6, "child");
        am6 a2 = rm6.a.a(this, true, false, new rk6(this, sk6), 2, (Object) null);
        if (a2 != null) {
            return (qk6) a2;
        }
        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.ChildHandle");
    }

    @DexIgnore
    public final am6 a(boolean z, boolean z2, hg6<? super Throwable, cd6> hg6) {
        Throwable th;
        wg6.b(hg6, "handler");
        Throwable th2 = null;
        xm6<?> xm6 = null;
        while (true) {
            Object d2 = d();
            if (d2 instanceof dm6) {
                dm6 dm6 = (dm6) d2;
                if (dm6.isActive()) {
                    if (xm6 == null) {
                        xm6 = a(hg6, z);
                    }
                    if (a.compareAndSet(this, d2, xm6)) {
                        return xm6;
                    }
                } else {
                    a(dm6);
                }
            } else if (d2 instanceof mm6) {
                dn6 a2 = ((mm6) d2).a();
                if (a2 != null) {
                    am6 am6 = en6.a;
                    if (!z || !(d2 instanceof c)) {
                        th = null;
                    } else {
                        synchronized (d2) {
                            th = ((c) d2).rootCause;
                            if (th == null || ((hg6 instanceof rk6) && !((c) d2).isCompleting)) {
                                if (xm6 == null) {
                                    xm6 = a(hg6, z);
                                }
                                if (a(d2, a2, xm6)) {
                                    if (th == null) {
                                        return xm6;
                                    }
                                    am6 = xm6;
                                }
                            }
                            cd6 cd6 = cd6.a;
                        }
                    }
                    if (th != null) {
                        if (z2) {
                            hg6.invoke(th);
                        }
                        return am6;
                    }
                    if (xm6 == null) {
                        xm6 = a(hg6, z);
                    }
                    if (a(d2, a2, xm6)) {
                        return xm6;
                    }
                } else if (d2 != null) {
                    a((xm6<?>) (xm6) d2);
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.JobNode<*>");
                }
            } else {
                if (z2) {
                    if (!(d2 instanceof vk6)) {
                        d2 = null;
                    }
                    vk6 vk6 = (vk6) d2;
                    if (vk6 != null) {
                        th2 = vk6.a;
                    }
                    hg6.invoke(th2);
                }
                return en6.a;
            }
        }
    }

    @DexIgnore
    public final boolean a(Object obj, dn6 dn6, xm6<?> xm6) {
        int a2;
        d dVar = new d(xm6, xm6, this, obj);
        do {
            Object e = dn6.e();
            if (e != null) {
                a2 = ((jo6) e).a(xm6, dn6, dVar);
                if (a2 == 1) {
                    return true;
                }
            } else {
                throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (a2 != 2);
        return false;
    }
}
