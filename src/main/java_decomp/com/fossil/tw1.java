package com.fossil;

import android.app.Activity;
import android.content.Intent;
import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface tw1 {
    @DexIgnore
    <T extends LifecycleCallback> T a(String str, Class<T> cls);

    @DexIgnore
    void a(String str, LifecycleCallback lifecycleCallback);

    @DexIgnore
    void startActivityForResult(Intent intent, int i);

    @DexIgnore
    Activity w0();
}
