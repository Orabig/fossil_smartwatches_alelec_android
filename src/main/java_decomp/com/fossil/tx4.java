package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tx4 implements Factory<sx4> {
    @DexIgnore
    public static HomeAlertsPresenter a(ox4 ox4, z24 z24, AlarmHelper alarmHelper, d15 d15, mz4 mz4, gy4 gy4, NotificationSettingsDatabase notificationSettingsDatabase, SetAlarms setAlarms, AlarmsRepository alarmsRepository, an4 an4, DNDSettingsDatabase dNDSettingsDatabase) {
        return new HomeAlertsPresenter(ox4, z24, alarmHelper, d15, mz4, gy4, notificationSettingsDatabase, setAlarms, alarmsRepository, an4, dNDSettingsDatabase);
    }
}
