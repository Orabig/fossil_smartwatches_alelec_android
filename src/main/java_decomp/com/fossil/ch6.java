package com.fossil;

import com.fossil.ni6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ch6 extends eh6 implements ni6 {
    @DexIgnore
    public ch6() {
    }

    @DexIgnore
    public ei6 computeReflected() {
        kh6.a(this);
        return this;
    }

    @DexIgnore
    public Object getDelegate(Object obj) {
        return ((ni6) getReflected()).getDelegate(obj);
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return get(obj);
    }

    @DexIgnore
    public ch6(Object obj) {
        super(obj);
    }

    @DexIgnore
    public ni6.a getGetter() {
        return ((ni6) getReflected()).getGetter();
    }
}
