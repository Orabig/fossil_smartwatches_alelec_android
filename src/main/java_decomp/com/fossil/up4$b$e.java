package com.fossil;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.Window;
import com.fossil.xx5;
import com.portfolio.platform.service.ShakeFeedbackService;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up4$b$e implements View.OnClickListener {
    @DexIgnore
    public /* final */ /* synthetic */ ShakeFeedbackService.b a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$initShakeFeedbackService$1$5$1", f = "ShakeFeedbackService.kt", l = {178}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ up4$b$e this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(up4$b$e up4_b_e, Bitmap bitmap, xe6 xe6) {
            super(2, xe6);
            this.this$0 = up4_b_e;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$bitmap, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ShakeFeedbackService shakeFeedbackService = this.this$0.a.a;
                Bitmap bitmap = this.$bitmap;
                this.L$0 = il6;
                this.label = 1;
                if (shakeFeedbackService.a(bitmap, (xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore
    public up4$b$e(ShakeFeedbackService.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public final void onClick(View view) {
        this.a.a.f = 2;
        xx5.a aVar = xx5.a;
        WeakReference b = this.a.a.a;
        if (b != null) {
            Object obj = b.get();
            if (obj == null) {
                throw new rc6("null cannot be cast to non-null type android.app.Activity");
            } else if (aVar.c((Activity) obj, 123)) {
                WeakReference b2 = this.a.a.a;
                if (b2 != null) {
                    Object obj2 = b2.get();
                    if (obj2 != null) {
                        Window window = ((Activity) obj2).getWindow();
                        wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                        View decorView = window.getDecorView();
                        wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                        View rootView = decorView.getRootView();
                        wg6.a((Object) rootView, "v1");
                        rootView.setDrawingCacheEnabled(true);
                        if (rootView.getDrawingCache() != null) {
                            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                            wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new a(this, createBitmap, (xe6) null), 3, (Object) null);
                        }
                        qg3 a2 = this.a.a.c;
                        if (a2 != null) {
                            a2.dismiss();
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type android.app.Activity");
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
