package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum qk0 {
    FIRE_TIME((byte) 0),
    c((byte) 1),
    MESSAGE((byte) 2);
    
    @DexIgnore
    public static /* final */ wi0 f; // = null;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        f = new wi0((qg6) null);
    }
    */

    @DexIgnore
    public qk0(byte b) {
        this.a = b;
    }
}
