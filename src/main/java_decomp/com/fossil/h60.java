package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.Gender;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h60 extends r60 {
    @DexIgnore
    public static /* final */ c CREATOR; // = new c((qg6) null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ short d;
    @DexIgnore
    public /* final */ short e;
    @DexIgnore
    public /* final */ b f;

    @DexIgnore
    public enum a {
        UNSPECIFIED((byte) 0),
        MALE((byte) 1),
        FEMALE((byte) 2);
        
        @DexIgnore
        public static /* final */ C0019a c; // = null;
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.h60$a$a")
        /* renamed from: com.fossil.h60$a$a  reason: collision with other inner class name */
        public static final class C0019a {
            @DexIgnore
            public /* synthetic */ C0019a(qg6 qg6) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }
        }

        /*
        static {
            c = new C0019a((qg6) null);
        }
        */

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }

        @DexIgnore
        public final Gender b() {
            int i = ky0.a[ordinal()];
            if (i == 1) {
                return Gender.UNSPECIFIED;
            }
            if (i == 2) {
                return Gender.MALE;
            }
            if (i == 3) {
                return Gender.FEMALE;
            }
            throw new kc6();
        }
    }

    @DexIgnore
    public enum b {
        UNSPECIFIED((byte) 0),
        LEFT_WRIST((byte) 1),
        RIGHT_WRIST((byte) 2),
        UNSPECIFIED_WRIST((byte) 3);
        
        @DexIgnore
        public static /* final */ a c; // = null;
        @DexIgnore
        public /* final */ byte a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(qg6 qg6) {
            }

            @DexIgnore
            public final b a(byte b) throws IllegalArgumentException {
                b bVar;
                b[] values = b.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bVar = null;
                        break;
                    }
                    bVar = values[i];
                    if (bVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (bVar != null) {
                    return bVar;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }
        }

        /*
        static {
            c = new a((qg6) null);
        }
        */

        @DexIgnore
        public b(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Parcelable.Creator<h60> {
        @DexIgnore
        public /* synthetic */ c(qg6 qg6) {
        }

        @DexIgnore
        public final h60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 7) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new h60(order.get(0), a.c.a(order.get(1)), order.getShort(2), order.getShort(4), b.c.a(order.get(6)));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 7"));
        }

        @DexIgnore
        public h60 createFromParcel(Parcel parcel) {
            return new h60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new h60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m21createFromParcel(Parcel parcel) {
            return new h60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public h60(byte b2, a aVar, short s, short s2, b bVar) throws IllegalArgumentException {
        super(s60.BIOMETRIC_PROFILE);
        this.b = b2;
        this.c = aVar;
        this.d = s;
        this.e = s2;
        this.f = bVar;
        e();
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(7).order(ByteOrder.LITTLE_ENDIAN).put(this.b).put(this.c.a()).putShort(this.d).putShort(this.e).put(this.f.a()).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        byte b2 = this.b;
        boolean z = true;
        if (8 <= b2 && 110 >= b2) {
            short s = this.d;
            if (100 <= s && 250 >= s) {
                short s2 = this.e;
                if (35 > s2 || 250 < s2) {
                    z = false;
                }
                if (!z) {
                    StringBuilder b3 = ze0.b("weightInKilogram (");
                    b3.append(this.e);
                    b3.append(") is out of range ");
                    b3.append("[35, 250]");
                    b3.append(" (in kilogram).");
                    throw new IllegalArgumentException(b3.toString());
                }
                return;
            }
            StringBuilder b4 = ze0.b("heightInCentimeter (");
            b4.append(this.d);
            b4.append(") is out of range ");
            b4.append("[100, 250]");
            b4.append(" (in centimeter.");
            throw new IllegalArgumentException(b4.toString());
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("age ("), this.b, ") is out of range [8, ", "110]."));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(h60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            h60 h60 = (h60) obj;
            return this.b == h60.b && this.c == h60.c && this.d == h60.d && this.e == h60.e && this.f == h60.f;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BiometricProfile");
    }

    @DexIgnore
    public final byte getAge() {
        return this.b;
    }

    @DexIgnore
    public final a getGender() {
        return this.c;
    }

    @DexIgnore
    public final short getHeightInCentimeter() {
        return this.d;
    }

    @DexIgnore
    public final b getWearingPosition() {
        return this.f;
    }

    @DexIgnore
    public final short getWeightInKilogram() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return this.f.hashCode() + ((((((hashCode + (this.b * 31)) * 31) + this.d) * 31) + this.e) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.d));
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.e));
        }
        if (parcel != null) {
            parcel.writeString(this.f.name());
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("age", Byte.valueOf(this.b));
            jSONObject.put("gender", cw0.a((Enum<?>) this.c));
            jSONObject.put("height_in_centimeter", Short.valueOf(this.d));
            jSONObject.put("weight_in_kilogram", Short.valueOf(this.e));
            jSONObject.put("wearing_position", cw0.a((Enum<?>) this.f));
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ h60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readByte();
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            this.c = a.valueOf(readString);
            this.d = (short) parcel.readInt();
            this.e = (short) parcel.readInt();
            String readString2 = parcel.readString();
            if (readString2 != null) {
                wg6.a(readString2, "parcel.readString()!!");
                this.f = b.valueOf(readString2);
                e();
                return;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }
}
