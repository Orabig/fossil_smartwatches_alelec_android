package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class h54 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon7.Anon1_Level2 a;
    @DexIgnore
    private /* final */ /* synthetic */ List b;
    @DexIgnore
    private /* final */ /* synthetic */ String c;
    @DexIgnore
    private /* final */ /* synthetic */ PresetDataSource.GetActivePresetCallback d;

    @DexIgnore
    public /* synthetic */ h54(PresetRepository.Anon7.Anon1_Level2 anon1_Level2, List list, String str, PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
        this.a = anon1_Level2;
        this.b = list;
        this.c = str;
        this.d = getActivePresetCallback;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c, this.d);
    }
}
