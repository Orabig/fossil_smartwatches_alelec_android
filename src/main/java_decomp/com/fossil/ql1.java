package com.fossil;

import com.facebook.internal.Utility;
import com.fossil.yq6;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ql1 implements Interceptor {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public ql1(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) {
        String str;
        String valueOf = String.valueOf(System.currentTimeMillis() / ((long) 1000));
        yq6.a f = chain.t().f();
        f.b("Content-Type", "application/json");
        f.b("X-Cyc-Auth-Method", "signature");
        f.b("X-Cyc-Access-Key-Id", this.a);
        f.b("X-Cyc-Timestamp", valueOf);
        StringBuilder b2 = ze0.b("Signature=");
        String str2 = this.b;
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1);
            String str3 = valueOf + str2;
            Charset f2 = mi0.A.f();
            if (str3 != null) {
                byte[] bytes = str3.getBytes(f2);
                wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
                instance.update(bytes);
                byte[] digest = instance.digest();
                StringBuilder sb = new StringBuilder();
                for (byte b3 : digest) {
                    nh6 nh6 = nh6.a;
                    Object[] objArr = {Byte.valueOf(b3)};
                    String format = String.format("%02X", Arrays.copyOf(objArr, objArr.length));
                    wg6.a(format, "java.lang.String.format(format, *args)");
                    sb.append(format);
                }
                str = sb.toString();
                wg6.a(str, "stringBuilder.toString()");
                b2.append(str);
                f.b("Authorization", b2.toString());
                Response a2 = chain.a(f.a());
                wg6.a(a2, "chain.proceed(request)");
                return a2;
            }
            throw new rc6("null cannot be cast to non-null type java.lang.String");
        } catch (Exception e) {
            qs0.h.a(e);
            str = "";
        }
    }
}
