package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j73 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle a;
    @DexIgnore
    public /* final */ /* synthetic */ e73 b;

    @DexIgnore
    public j73(e73 e73, Bundle bundle) {
        this.b = e73;
        this.a = bundle;
    }

    @DexIgnore
    public final void run() {
        this.b.c(this.a);
    }
}
