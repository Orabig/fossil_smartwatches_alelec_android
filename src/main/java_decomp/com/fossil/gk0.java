package com.fossil;

import android.content.Context;
import android.os.Build;
import com.facebook.internal.AnalyticsEvents;
import java.lang.ref.WeakReference;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk0 {
    @DexIgnore
    public static WeakReference<Context> a;
    @DexIgnore
    public static String b; // = new String();
    @DexIgnore
    public static ge0 c; // = new ge0("", "", "");
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ gk0 f; // = new gk0();

    /*
    static {
        new ge0("", "", "");
        String str = Build.VERSION.RELEASE;
        if (str == null) {
            str = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        d = str;
        String str2 = Build.MODEL;
        if (str2 == null) {
            str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        e = str2;
    }
    */

    @DexIgnore
    public final void a(String str) {
        b = str;
    }

    @DexIgnore
    public final String b() {
        return d;
    }

    @DexIgnore
    public final String c() {
        return e;
    }

    @DexIgnore
    public final int d() {
        return (TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 1000) / 60;
    }

    @DexIgnore
    public final String e() {
        return b;
    }

    @DexIgnore
    public final void a(ge0 ge0) {
        c = ge0;
        qs0 qs0 = qs0.h;
        qs0.c.a(new ge0(c.c() + "/sdk_log", c.a(), c.b()));
        zj0 zj0 = zj0.h;
        zj0.c.a(new ge0(c.c() + "/raw_minute_data", c.a(), c.b()));
        fi0 fi0 = fi0.h;
        fi0.c.a(new ge0(c.c() + "/raw_hardware_log", c.a(), c.b()));
    }

    @DexIgnore
    public final void a(Context context) {
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            a = new WeakReference<>(applicationContext);
        }
    }

    @DexIgnore
    public final Context a() {
        WeakReference<Context> weakReference = a;
        if (weakReference != null) {
            return (Context) weakReference.get();
        }
        return null;
    }
}
