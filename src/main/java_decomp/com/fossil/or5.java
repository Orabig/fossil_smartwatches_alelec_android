package com.fossil;

import com.portfolio.platform.uirenew.ProfileSetupFragment;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface or5 {
    @DexIgnore
    void a(ProfileSetupFragment profileSetupFragment);

    @DexIgnore
    void a(ProfileEditFragment profileEditFragment);
}
