package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s64 extends r64 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public long z;

    /*
    static {
        B.put(2131362081, 1);
        B.put(2131361851, 2);
        B.put(2131362820, 3);
        B.put(2131362442, 4);
        B.put(2131362394, 5);
        B.put(2131362324, 6);
        B.put(2131362043, 7);
        B.put(2131361854, 8);
        B.put(2131362067, 9);
        B.put(2131361855, 10);
        B.put(2131361852, 11);
    }
    */

    @DexIgnore
    public s64(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 12, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public s64(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[2], objArr[11], objArr[8], objArr[10], objArr[7], objArr[9], objArr[1], objArr[6], objArr[5], objArr[4], objArr[3], objArr[0]);
        this.z = -1;
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
