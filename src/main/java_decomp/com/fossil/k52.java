package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class k52 implements Callable {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ l52 c;

    @DexIgnore
    public k52(boolean z, String str, l52 l52) {
        this.a = z;
        this.b = str;
        this.c = l52;
    }

    @DexIgnore
    public final Object call() {
        return j52.a(this.a, this.b, this.c);
    }
}
