package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class om5 implements Factory<nm5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public om5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static om5 a(Provider<ThemeRepository> provider) {
        return new om5(provider);
    }

    @DexIgnore
    public static nm5 b(Provider<ThemeRepository> provider) {
        return new ThemesViewModel(provider.get());
    }

    @DexIgnore
    public ThemesViewModel get() {
        return b(this.a);
    }
}
