package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class b54 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon14 a;
    @DexIgnore
    private /* final */ /* synthetic */ RecommendedPreset b;

    @DexIgnore
    public /* synthetic */ b54(PresetRepository.Anon14 anon14, RecommendedPreset recommendedPreset) {
        this.a = anon14;
        this.b = recommendedPreset;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b);
    }
}
