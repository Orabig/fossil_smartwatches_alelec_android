package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class re3 extends e22 implements be3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<re3> CREATOR; // = new se3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public re3(int i, String str, byte[] bArr, String str2) {
        this.a = i;
        this.b = str;
        this.c = bArr;
        this.d = str2;
    }

    @DexIgnore
    public final int B() {
        return this.a;
    }

    @DexIgnore
    public final String C() {
        return this.d;
    }

    @DexIgnore
    public final byte[] p() {
        return this.c;
    }

    @DexIgnore
    public final String toString() {
        int i = this.a;
        String str = this.b;
        byte[] bArr = this.c;
        String valueOf = String.valueOf(bArr == null ? "null" : Integer.valueOf(bArr.length));
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 43 + String.valueOf(valueOf).length());
        sb.append("MessageEventParcelable[");
        sb.append(i);
        sb.append(",");
        sb.append(str);
        sb.append(", size=");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, B());
        g22.a(parcel, 3, z(), false);
        g22.a(parcel, 4, p(), false);
        g22.a(parcel, 5, C(), false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public final String z() {
        return this.b;
    }
}
