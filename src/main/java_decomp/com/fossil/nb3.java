package com.fossil;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nb3 implements Parcelable.Creator<ob3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        int i = 0;
        Intent intent = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i = f22.q(parcel, a);
            } else if (a2 == 2) {
                i2 = f22.q(parcel, a);
            } else if (a2 != 3) {
                f22.v(parcel, a);
            } else {
                intent = (Intent) f22.a(parcel, a, Intent.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new ob3(i, i2, intent);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ob3[i];
    }
}
