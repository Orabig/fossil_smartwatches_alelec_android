package com.fossil;

import android.os.Bundle;
import com.fossil.w52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d62 implements w52.a {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle a;
    @DexIgnore
    public /* final */ /* synthetic */ w52 b;

    @DexIgnore
    public d62(w52 w52, Bundle bundle) {
        this.b = w52;
        this.a = bundle;
    }

    @DexIgnore
    public final void a(y52 y52) {
        this.b.a.b(this.a);
    }

    @DexIgnore
    public final int getState() {
        return 1;
    }
}
