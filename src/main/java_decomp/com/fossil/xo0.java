package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xo0 extends ml0 {
    @DexIgnore
    public static /* final */ fn0 L; // = new fn0((qg6) null);
    @DexIgnore
    public /* final */ r50 J;
    @DexIgnore
    public /* final */ HandMovingConfig[] K;

    @DexIgnore
    public xo0(ue1 ue1, r50 r50, HandMovingConfig[] handMovingConfigArr) {
        super(tf1.MOVE_HANDS, lx0.MOVE_HANDS, ue1, 0, 8);
        this.J = r50;
        this.K = handMovingConfigArr;
        this.C = true;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.MOVING_TYPE, (Object) cw0.a((Enum<?>) this.J)), bm0.HAND_CONFIGS, (Object) cw0.a((p40[]) this.K));
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate((this.K.length * 5) + 2).order(ByteOrder.LITTLE_ENDIAN).put(this.J.a()).put((byte) this.K.length).put(L.a(this.K)).array();
        wg6.a(array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }
}
