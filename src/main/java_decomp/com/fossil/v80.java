package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v80 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ h70 b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ j70 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<v80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                h70 valueOf = h70.valueOf(readString);
                float readFloat = parcel.readFloat();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    return new v80(readLong, valueOf, readFloat, j70.valueOf(readString2));
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new v80[i];
        }
    }

    @DexIgnore
    public v80(long j, h70 h70, float f, j70 j70) {
        this.a = j;
        this.b = h70;
        this.c = cw0.a(f, 2);
        this.d = j70;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.EXPIRED_TIMESTAMP_IN_SECOND, (Object) Long.valueOf(this.a)), bm0.TEMP_UNIT, (Object) cw0.a((Enum<?>) this.b)), bm0.TEMPERATURE, (Object) Float.valueOf(this.c)), bm0.WEATHER_CONDITION, (Object) cw0.a((Enum<?>) this.d));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alive", this.a);
            jSONObject.put("unit", cw0.a((Enum<?>) this.b));
            jSONObject.put("temp", Float.valueOf(this.c));
            jSONObject.put("cond_id", this.d.a());
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(v80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            v80 v80 = (v80) obj;
            return this.a == v80.a && this.b == v80.b && this.c == v80.c && this.d == v80.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.CompactWeatherInfo");
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.a;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.c;
    }

    @DexIgnore
    public final h70 getTemperatureUnit() {
        return this.b;
    }

    @DexIgnore
    public final j70 getWeatherCondition() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = Float.valueOf(this.c).hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + (((int) this.a) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
