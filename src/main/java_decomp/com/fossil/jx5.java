package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.util.DeviceUtils;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jx5 implements MembersInjector<ix5> {
    @DexIgnore
    public static void a(DeviceUtils deviceUtils, DeviceRepository deviceRepository) {
        deviceUtils.a = deviceRepository;
    }

    @DexIgnore
    public static void a(DeviceUtils deviceUtils, an4 an4) {
        deviceUtils.b = an4;
    }

    @DexIgnore
    public static void a(DeviceUtils deviceUtils, GuestApiService guestApiService) {
        deviceUtils.c = guestApiService;
    }

    @DexIgnore
    public static void a(DeviceUtils deviceUtils, FirmwareFileRepository firmwareFileRepository) {
        deviceUtils.d = firmwareFileRepository;
    }
}
