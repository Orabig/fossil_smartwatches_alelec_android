package com.fossil.wearables.fsl.fitness;

import android.util.Pair;
import com.fossil.wearables.fsl.BaseProvider;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface FitnessProvider extends BaseProvider {
    @DexIgnore
    void addDownloadedSampleRaw(SampleRaw sampleRaw);

    @DexIgnore
    void addSample(SampleRaw sampleRaw);

    @DexIgnore
    void clearActivitySettings();

    @DexIgnore
    boolean createOrUpdateActivitySettings(ActivitySettings activitySettings);

    @DexIgnore
    ActivitySettings getActivitySettings();

    @DexIgnore
    List<DailyGoal> getAllDailyGoals();

    @DexIgnore
    List<SampleDay> getAllSampleDays();

    @DexIgnore
    DailyGoal getDailyGoal(int i, int i2, int i3);

    @DexIgnore
    DailyGoal getDailyGoal(Calendar calendar);

    @DexIgnore
    List<DailyGoal> getDailyGoalInRange(Date date, Date date2);

    @DexIgnore
    Date getEndDateOfSampleDay();

    @DexIgnore
    Date getEndDateOfSampleRaw();

    @DexIgnore
    int getLastDailyGoal();

    @DexIgnore
    int getLastDailyGoalFromDate(Date date);

    @DexIgnore
    SampleRaw getLastSampleRaw();

    @DexIgnore
    List<SampleRaw> getListSampleRawByType(int i);

    @DexIgnore
    SampleDay getNearestSampleDayFromDate(Date date);

    @DexIgnore
    List<SampleRaw> getPendingSampleRaws();

    @DexIgnore
    List<SampleRaw> getRawSamples(Date date, Date date2);

    @DexIgnore
    SampleDay getSampleDayByDate(int i, int i2, int i3);

    @DexIgnore
    List<SampleDay> getSampleDays(Calendar calendar, Calendar calendar2);

    @DexIgnore
    List<SampleRaw> getSampleRawsByIds(List<String> list);

    @DexIgnore
    SampleDay getSamplesForDay(int i, int i2, int i3);

    @DexIgnore
    SampleDay getSamplesForDay(Calendar calendar);

    @DexIgnore
    Date getStartDateOfSampleDay();

    @DexIgnore
    Date getStartDateOfSampleRaw();

    @DexIgnore
    Pair<Integer, Integer> getTodayStepCountAndGoal();

    @DexIgnore
    void updateActiveTimeGoalSettings(int i);

    @DexIgnore
    void updateCaloriesGoalSettings(int i);

    @DexIgnore
    void updateSampleDay(SampleDay sampleDay);

    @DexIgnore
    void updateSampleDays(List<SampleDay> list);

    @DexIgnore
    void updateSampleRawPinType(String str, int i);

    @DexIgnore
    void updateSampleRawUAPinType(String str, int i);

    @DexIgnore
    void updateStepGoal(int i, int i2, int i3, int i4);

    @DexIgnore
    void updateStepGoalSettings(int i);
}
