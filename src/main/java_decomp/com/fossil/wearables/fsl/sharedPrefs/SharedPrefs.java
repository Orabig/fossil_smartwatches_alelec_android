package com.fossil.wearables.fsl.sharedPrefs;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface SharedPrefs {
    @DexIgnore
    boolean getSharedPreferenceBoolean(Context context, String str, boolean z);

    @DexIgnore
    int getSharedPreferenceInt(Context context, String str, int i);

    @DexIgnore
    String getSharedPreferenceString(Context context, String str, String str2);

    @DexIgnore
    void setSharedPreferenceBoolean(Context context, String str, boolean z);

    @DexIgnore
    void setSharedPreferenceInt(Context context, String str, int i);

    @DexIgnore
    void setSharedPreferenceString(Context context, String str, String str2);
}
