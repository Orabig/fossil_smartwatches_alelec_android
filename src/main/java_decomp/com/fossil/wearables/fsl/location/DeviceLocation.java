package com.fossil.wearables.fsl.location;

import com.j256.ormlite.field.DatabaseField;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DeviceLocation implements Serializable {
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_SERIAL; // = "deviceSerial";
    @DexIgnore
    public static /* final */ String COLUMN_LATITUDE; // = "latitude";
    @DexIgnore
    public static /* final */ String COLUMN_LONGITUDE; // = "longitude";
    @DexIgnore
    public static /* final */ String COLUMN_TIMESTAMP; // = "timestamp";
    @DexIgnore
    @DatabaseField(columnName = "deviceSerial", id = true)
    public String deviceSerial;
    @DexIgnore
    @DatabaseField(columnName = "latitude")
    public double latitude;
    @DexIgnore
    @DatabaseField(columnName = "longitude")
    public double longitude;
    @DexIgnore
    @DatabaseField(columnName = "timestamp")
    public long timeStamp;

    @DexIgnore
    public DeviceLocation() {
    }

    @DexIgnore
    public String getDeviceSerial() {
        return this.deviceSerial;
    }

    @DexIgnore
    public double getLatitude() {
        return this.latitude;
    }

    @DexIgnore
    public double getLongitude() {
        return this.longitude;
    }

    @DexIgnore
    public long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public void setDeviceSerial(String str) {
        this.deviceSerial = str;
    }

    @DexIgnore
    public void setLatitude(double d) {
        this.latitude = d;
    }

    @DexIgnore
    public void setLongitude(double d) {
        this.longitude = d;
    }

    @DexIgnore
    public void setTimeStamp(long j) {
        this.timeStamp = j;
    }

    @DexIgnore
    public String toString() {
        return "[DeviceLocation: serial=" + this.deviceSerial + ", lat=" + this.latitude + ", lon=" + this.longitude + ", timestamp=" + this.timeStamp + "]";
    }

    @DexIgnore
    public DeviceLocation(String str, double d, double d2, long j) {
        this.deviceSerial = str;
        this.latitude = d;
        this.longitude = d2;
        this.timeStamp = j;
    }
}
