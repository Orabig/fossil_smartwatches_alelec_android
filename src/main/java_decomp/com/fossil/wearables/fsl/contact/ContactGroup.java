package com.fossil.wearables.fsl.contact;

import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ContactGroup extends BaseFeatureModel {
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_FAMILY; // = "deviceFamily";
    @DexIgnore
    @ForeignCollectionField(eager = true)
    public ForeignCollection<Contact> contacts;
    @DexIgnore
    @DatabaseField
    public int deviceFamily;

    @DexIgnore
    public Contact getContactWithEmail(String str) {
        if (getContacts() != null && getContacts().size() > 0) {
            for (Contact next : getContacts()) {
                if (next != null && next.containsEmail(str)) {
                    return next;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public Contact getContactWithPhoneNumber(String str) {
        if (getContacts() != null && getContacts().size() > 0) {
            for (Contact next : getContacts()) {
                if (next != null && next.containsPhoneNumber(str)) {
                    return next;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public List<Contact> getContacts() {
        ArrayList arrayList = new ArrayList();
        ForeignCollection<Contact> foreignCollection = this.contacts;
        return (foreignCollection == null || foreignCollection.size() <= 0) ? arrayList : new ArrayList(this.contacts);
    }

    @DexIgnore
    public int getDeviceFamily() {
        return this.deviceFamily;
    }

    @DexIgnore
    public void setDeviceFamily(int i) {
        this.deviceFamily = i;
    }
}
