package com.fossil;

import com.portfolio.platform.CoroutineUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws5 implements CoroutineUseCase.a {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;

    @DexIgnore
    public ws5(int i, String str) {
        wg6.b(str, "errorMessage");
        this.a = i;
        this.b = str;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }
}
