package com.fossil;

import android.content.ComponentName;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h93 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ d93 a;

    @DexIgnore
    public h93(d93 d93) {
        this.a = d93;
    }

    @DexIgnore
    public final void run() {
        l83 l83 = this.a.c;
        Context c = l83.c();
        this.a.c.d();
        l83.a(new ComponentName(c, "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
