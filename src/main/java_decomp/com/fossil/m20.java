package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import com.facebook.internal.FetchedAppSettings;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m20 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        String a(File file) throws IOException;
    }

    @DexIgnore
    public m20(Context context, a aVar) {
        this.a = context;
        this.b = aVar;
    }

    @DexIgnore
    public byte[] a(String str) throws IOException {
        return a(d(str));
    }

    @DexIgnore
    public final JSONArray b(BufferedReader bufferedReader) throws IOException {
        JSONArray jSONArray = new JSONArray();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return jSONArray;
            }
            JSONObject c = c(readLine);
            if (c != null) {
                jSONArray.put(c);
            }
        }
    }

    @DexIgnore
    public final JSONObject c(String str) {
        u30 a2 = v30.a(str);
        if (a2 != null && a(a2)) {
            try {
                try {
                    return a(this.b.a(b(a2.d)), a2);
                } catch (JSONException e) {
                    c86.g().b("CrashlyticsCore", "Could not create a binary image json string", e);
                    return null;
                }
            } catch (IOException e2) {
                l86 g = c86.g();
                g.b("CrashlyticsCore", "Could not generate ID for file " + a2.d, e2);
            }
        }
        return null;
    }

    @DexIgnore
    public final JSONArray d(String str) {
        JSONArray jSONArray = new JSONArray();
        try {
            String[] split = b(new JSONObject(str).getJSONArray("maps")).split(FetchedAppSettings.DialogFeatureConfig.DIALOG_CONFIG_DIALOG_NAME_FEATURE_NAME_SEPARATOR);
            for (String c : split) {
                JSONObject c2 = c(c);
                if (c2 != null) {
                    jSONArray.put(c2);
                }
            }
            return jSONArray;
        } catch (JSONException e) {
            c86.g().a("CrashlyticsCore", "Unable to parse proc maps string", e);
            return jSONArray;
        }
    }

    @DexIgnore
    public byte[] a(BufferedReader bufferedReader) throws IOException {
        return a(b(bufferedReader));
    }

    @DexIgnore
    public final File a(File file) {
        if (Build.VERSION.SDK_INT < 9 || !file.getAbsolutePath().startsWith("/data")) {
            return file;
        }
        try {
            return new File(this.a.getPackageManager().getApplicationInfo(this.a.getPackageName(), 0).nativeLibraryDir, file.getName());
        } catch (PackageManager.NameNotFoundException e) {
            c86.g().e("CrashlyticsCore", "Error getting ApplicationInfo", e);
            return file;
        }
    }

    @DexIgnore
    public final File b(String str) {
        File file = new File(str);
        return !file.exists() ? a(file) : file;
    }

    @DexIgnore
    public static String b(JSONArray jSONArray) throws JSONException {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < jSONArray.length(); i++) {
            sb.append(jSONArray.getString(i));
        }
        return sb.toString();
    }

    @DexIgnore
    public static byte[] a(JSONArray jSONArray) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("binary_images", jSONArray);
            return jSONObject.toString().getBytes();
        } catch (JSONException e) {
            c86.g().a("CrashlyticsCore", "Binary images string is null", e);
            return new byte[0];
        }
    }

    @DexIgnore
    public static JSONObject a(String str, u30 u30) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("base_address", u30.a);
        jSONObject.put("size", u30.b);
        jSONObject.put("name", u30.d);
        jSONObject.put("uuid", str);
        return jSONObject;
    }

    @DexIgnore
    public static boolean a(u30 u30) {
        return (u30.c.indexOf(120) == -1 || u30.d.indexOf(47) == -1) ? false : true;
    }
}
