package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v46 implements x56 {
    @DexIgnore
    public /* final */ /* synthetic */ List a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ o46 c;

    @DexIgnore
    public v46(o46 o46, List list, boolean z) {
        this.c = o46;
        this.a = list;
        this.b = z;
    }

    @DexIgnore
    public void a() {
        q36.c();
        this.c.a((List<y46>) this.a, this.b, true);
    }

    @DexIgnore
    public void b() {
        q36.d();
        this.c.a((List<y46>) this.a, 1, this.b, true);
    }
}
