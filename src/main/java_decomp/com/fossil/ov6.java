package com.fossil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ov6 extends lv6 {
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public ov6(File file, String str, String str2, String str3) {
        super(str2);
        if (file != null) {
            this.b = file;
            if (str != null) {
                this.c = str;
            } else {
                this.c = file.getName();
            }
            this.d = str3;
            return;
        }
        throw new IllegalArgumentException("File may not be null");
    }

    @DexIgnore
    public String a() {
        return "binary";
    }

    @DexIgnore
    public String b() {
        return this.d;
    }

    @DexIgnore
    public String d() {
        return this.c;
    }

    @DexIgnore
    public long getContentLength() {
        return this.b.length();
    }

    @DexIgnore
    public void writeTo(OutputStream outputStream) throws IOException {
        if (outputStream != null) {
            FileInputStream fileInputStream = new FileInputStream(this.b);
            try {
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read != -1) {
                        outputStream.write(bArr, 0, read);
                    } else {
                        outputStream.flush();
                        return;
                    }
                }
            } finally {
                fileInputStream.close();
            }
        } else {
            throw new IllegalArgumentException("Output stream may not be null");
        }
    }

    @DexIgnore
    public ov6(File file, String str, String str2) {
        this(file, (String) null, str, str2);
    }

    @DexIgnore
    public ov6(File file, String str) {
        this(file, str, (String) null);
    }
}
