package com.fossil;

import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.rv1;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lz1 {
    @DexIgnore
    public static /* final */ Status d; // = new Status(8, "The connection to Google Play services was lost");
    @DexIgnore
    public static /* final */ BasePendingResult<?>[] e; // = new BasePendingResult[0];
    @DexIgnore
    public /* final */ Set<BasePendingResult<?>> a; // = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    @DexIgnore
    public /* final */ mz1 b; // = new kz1(this);
    @DexIgnore
    public /* final */ Map<rv1.c<?>, rv1.f> c;

    @DexIgnore
    public lz1(Map<rv1.c<?>, rv1.f> map) {
        this.c = map;
    }

    @DexIgnore
    public final void a(BasePendingResult<? extends ew1> basePendingResult) {
        this.a.add(basePendingResult);
        basePendingResult.a(this.b);
    }

    @DexIgnore
    public final void b() {
        for (BasePendingResult b2 : (BasePendingResult[]) this.a.toArray(e)) {
            b2.b(d);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.fossil.fw1, com.fossil.p02, com.fossil.kz1, com.fossil.mz1] */
    public final void a() {
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.a.toArray(e)) {
            Object r5 = 0;
            basePendingResult.a((mz1) r5);
            if (basePendingResult.e() != null) {
                basePendingResult.a(r5);
                IBinder n = this.c.get(((nw1) basePendingResult).i()).n();
                if (basePendingResult.d()) {
                    basePendingResult.a((mz1) new nz1(basePendingResult, r5, n, r5));
                } else if (n == null || !n.isBinderAlive()) {
                    basePendingResult.a((mz1) r5);
                    basePendingResult.a();
                    r5.a(basePendingResult.e().intValue());
                } else {
                    nz1 nz1 = new nz1(basePendingResult, r5, n, r5);
                    basePendingResult.a((mz1) nz1);
                    try {
                        n.linkToDeath(nz1, 0);
                    } catch (RemoteException unused) {
                        basePendingResult.a();
                        r5.a(basePendingResult.e().intValue());
                    }
                }
                this.a.remove(basePendingResult);
            } else if (basePendingResult.f()) {
                this.a.remove(basePendingResult);
            }
        }
    }
}
