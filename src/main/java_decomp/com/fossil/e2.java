package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ActionMenuView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e2 extends ViewGroup {
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public ActionMenuView c;
    @DexIgnore
    public g2 d;
    @DexIgnore
    public int e;
    @DexIgnore
    public ba f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;

    @DexIgnore
    public e2(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public static int a(int i, int i2, boolean z) {
        return z ? i - i2 : i + i2;
    }

    @DexIgnore
    public int getAnimatedVisibility() {
        if (this.f != null) {
            return this.a.b;
        }
        return getVisibility();
    }

    @DexIgnore
    public int getContentHeight() {
        return this.e;
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes((AttributeSet) null, j0.ActionBar, a0.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(j0.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        g2 g2Var = this.d;
        if (g2Var != null) {
            g2Var.a(configuration);
        }
    }

    @DexIgnore
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.h = false;
        }
        if (!this.h) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.h = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.h = false;
        }
        return true;
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.g = false;
        }
        if (!this.g) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.g = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.g = false;
        }
        return true;
    }

    @DexIgnore
    public abstract void setContentHeight(int i);

    @DexIgnore
    public void setVisibility(int i) {
        if (i != getVisibility()) {
            ba baVar = this.f;
            if (baVar != null) {
                baVar.a();
            }
            super.setVisibility(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ca {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public a a(ba baVar, int i) {
            e2.this.f = baVar;
            this.b = i;
            return this;
        }

        @DexIgnore
        public void b(View view) {
            if (!this.a) {
                e2 e2Var = e2.this;
                e2Var.f = null;
                e2.super.setVisibility(this.b);
            }
        }

        @DexIgnore
        public void c(View view) {
            e2.super.setVisibility(0);
            this.a = false;
        }

        @DexIgnore
        public void a(View view) {
            this.a = true;
        }
    }

    @DexIgnore
    public e2(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ba a(int i, long j) {
        ba baVar = this.f;
        if (baVar != null) {
            baVar.a();
        }
        if (i == 0) {
            if (getVisibility() != 0) {
                setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            ba a2 = x9.a(this);
            a2.a(1.0f);
            a2.a(j);
            a aVar = this.a;
            aVar.a(a2, i);
            a2.a((ca) aVar);
            return a2;
        }
        ba a3 = x9.a(this);
        a3.a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        a3.a(j);
        a aVar2 = this.a;
        aVar2.a(a3, i);
        a3.a((ca) aVar2);
        return a3;
    }

    @DexIgnore
    public e2(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2;
        this.a = new a();
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(a0.actionBarPopupTheme, typedValue, true) || (i2 = typedValue.resourceId) == 0) {
            this.b = context;
        } else {
            this.b = new ContextThemeWrapper(context, i2);
        }
    }

    @DexIgnore
    public int a(View view, int i, int i2, int i3) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i, RecyclerView.UNDEFINED_DURATION), i2);
        return Math.max(0, (i - view.getMeasuredWidth()) - i3);
    }

    @DexIgnore
    public int a(View view, int i, int i2, int i3, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = i2 + ((i3 - measuredHeight) / 2);
        if (z) {
            view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
        } else {
            view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        }
        return z ? -measuredWidth : measuredWidth;
    }
}
