package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pv2 implements Parcelable.Creator<mv2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        Bundle bundle = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    j = f22.s(parcel2, a);
                    break;
                case 2:
                    j2 = f22.s(parcel2, a);
                    break;
                case 3:
                    z = f22.i(parcel2, a);
                    break;
                case 4:
                    str = f22.e(parcel2, a);
                    break;
                case 5:
                    str2 = f22.e(parcel2, a);
                    break;
                case 6:
                    str3 = f22.e(parcel2, a);
                    break;
                case 7:
                    bundle = f22.a(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new mv2(j, j2, z, str, str2, str3, bundle);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new mv2[i];
    }
}
