package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ic0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ pz0 a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ic0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            pz0 pz0 = pz0.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = wx0.a[pz0.ordinal()];
            if (i == 1) {
                return e31.CREATOR.createFromParcel(parcel);
            }
            if (i == 2) {
                return kc0.CREATOR.createFromParcel(parcel);
            }
            if (i == 3) {
                return jc0.CREATOR.createFromParcel(parcel);
            }
            throw new kc6();
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new ic0[i];
        }
    }

    @DexIgnore
    public ic0(pz0 pz0, boolean z) {
        this.a = pz0;
        this.b = z;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("enable_goal_ring", this.b);
        wg6.a(put, "JSONObject()\n           \u2026 mEnablePercentageCircle)");
        return put;
    }

    @DexIgnore
    public boolean b() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ic0 ic0 = (ic0) obj;
            return this.a == ic0.a && this.b == ic0.b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.b).hashCode() + this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a.ordinal());
        }
        if (parcel != null) {
            parcel.writeInt(this.b ? 1 : 0);
        }
    }

    @DexIgnore
    public /* synthetic */ ic0(pz0 pz0, boolean z, int i) {
        z = (i & 2) != 0 ? false : z;
        this.a = pz0;
        this.b = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ic0(Parcel parcel) {
        this(pz0.values()[parcel.readInt()], parcel.readInt() != 1 ? false : true);
    }
}
