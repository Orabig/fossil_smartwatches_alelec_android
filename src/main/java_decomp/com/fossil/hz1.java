package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz1 implements ic3<Boolean, Void> {
    @DexIgnore
    public final /* synthetic */ Object then(qc3 qc3) throws Exception {
        if (((Boolean) qc3.b()).booleanValue()) {
            return null;
        }
        throw new sv1(new Status(13, "listener already unregistered"));
    }
}
