package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mj4 extends fr {
    @DexIgnore
    public mj4(wq wqVar, my myVar, ry ryVar, Context context) {
        super(wqVar, myVar, ryVar, context);
    }

    @DexIgnore
    public lj4<Bitmap> e() {
        return mj4.super.e();
    }

    @DexIgnore
    public lj4<Drawable> f() {
        return mj4.super.f();
    }

    @DexIgnore
    public <ResourceType> lj4<ResourceType> a(Class<ResourceType> cls) {
        return new lj4<>(this.a, this, cls, this.b);
    }

    @DexIgnore
    public lj4<Drawable> a(String str) {
        return mj4.super.a(str);
    }

    @DexIgnore
    public lj4<Drawable> a(Uri uri) {
        return mj4.super.a(uri);
    }

    @DexIgnore
    public lj4<Drawable> a(Integer num) {
        return mj4.super.a(num);
    }

    @DexIgnore
    public lj4<Drawable> a(byte[] bArr) {
        return mj4.super.a(bArr);
    }

    @DexIgnore
    public lj4<Drawable> a(Object obj) {
        return mj4.super.a(obj);
    }

    @DexIgnore
    public void a(nz nzVar) {
        if (nzVar instanceof kj4) {
            mj4.super.a(nzVar);
        } else {
            mj4.super.a(new kj4().a((gz) nzVar));
        }
    }
}
