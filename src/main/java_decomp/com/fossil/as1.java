package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rs1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class as1 implements rs1.b {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ rp1 b;

    @DexIgnore
    public as1(long j, rp1 rp1) {
        this.a = j;
        this.b = rp1;
    }

    @DexIgnore
    public static rs1.b a(long j, rp1 rp1) {
        return new as1(j, rp1);
    }

    @DexIgnore
    public Object apply(Object obj) {
        return rs1.a(this.a, this.b, (SQLiteDatabase) obj);
    }
}
