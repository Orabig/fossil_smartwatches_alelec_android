package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ai5$g$c<T> implements ld<yx5<? extends List<ActivitySample>>> {
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesDetailPresenter.g a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3$1", f = "CaloriesDetailPresenter.kt", l = {158}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ai5$g$c this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ai5$g$c$a$a")
        @lf6(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3$1$samples$1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.ai5$g$c$a$a  reason: collision with other inner class name */
        public static final class C0004a extends sf6 implements ig6<il6, xe6<? super List<ActivitySample>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0004a(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                C0004a aVar = new C0004a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0004a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    CaloriesDetailPresenter caloriesDetailPresenter = this.this$0.this$0.a.this$0;
                    return caloriesDetailPresenter.a(caloriesDetailPresenter.g, (List<ActivitySample>) this.this$0.this$0.a.this$0.l);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ai5$g$c ai5_g_c, xe6 xe6) {
            super(2, xe6);
            this.this$0 = ai5_g_c;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.a.this$0.b();
                C0004a aVar = new C0004a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, aVar, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) obj;
            if (this.this$0.a.this$0.n == null || (!wg6.a((Object) this.this$0.a.this$0.n, (Object) list))) {
                this.this$0.a.this$0.n = list;
                if (this.this$0.a.this$0.i && this.this$0.a.this$0.j) {
                    rm6 unused = this.this$0.a.this$0.m();
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore
    public ai5$g$c(CaloriesDetailPresenter.g gVar) {
        this.a = gVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(yx5<? extends List<ActivitySample>> yx5) {
        wh4 a2 = yx5.a();
        List list = (List) yx5.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("start - sampleTransformations -- activitySamples=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        sb.append(", status=");
        sb.append(a2);
        local.d("CaloriesDetailPresenter", sb.toString());
        if (a2 == wh4.NETWORK_LOADING || a2 == wh4.SUCCESS) {
            this.a.this$0.l = list;
            this.a.this$0.j = true;
            rm6 unused = ik6.b(this.a.this$0.e(), (af6) null, (ll6) null, new a(this, (xe6) null), 3, (Object) null);
        }
    }
}
