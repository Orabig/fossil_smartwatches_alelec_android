package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qc3<TResult> {
    @DexIgnore
    public qc3<TResult> a(kc3<TResult> kc3) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public abstract qc3<TResult> a(lc3 lc3);

    @DexIgnore
    public abstract qc3<TResult> a(mc3<? super TResult> mc3);

    @DexIgnore
    public abstract qc3<TResult> a(Executor executor, lc3 lc3);

    @DexIgnore
    public abstract qc3<TResult> a(Executor executor, mc3<? super TResult> mc3);

    @DexIgnore
    public abstract Exception a();

    @DexIgnore
    public abstract <X extends Throwable> TResult a(Class<X> cls) throws Throwable;

    @DexIgnore
    public <TContinuationResult> qc3<TContinuationResult> b(ic3<TResult, qc3<TContinuationResult>> ic3) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public abstract TResult b();

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract boolean d();

    @DexIgnore
    public abstract boolean e();

    @DexIgnore
    public qc3<TResult> a(Executor executor, kc3<TResult> kc3) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> qc3<TContinuationResult> b(Executor executor, ic3<TResult, qc3<TContinuationResult>> ic3) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public qc3<TResult> a(Executor executor, jc3 jc3) {
        throw new UnsupportedOperationException("addOnCanceledListener is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> qc3<TContinuationResult> a(ic3<TResult, TContinuationResult> ic3) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> qc3<TContinuationResult> a(Executor executor, ic3<TResult, TContinuationResult> ic3) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> qc3<TContinuationResult> a(pc3<TResult, TContinuationResult> pc3) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> qc3<TContinuationResult> a(Executor executor, pc3<TResult, TContinuationResult> pc3) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }
}
