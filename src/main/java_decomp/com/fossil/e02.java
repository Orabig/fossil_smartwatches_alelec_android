package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e02 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ b02 a;

    @DexIgnore
    public e02(b02 b02) {
        this.a = b02;
    }

    @DexIgnore
    public final void run() {
        this.a.q.lock();
        try {
            this.a.h();
        } finally {
            this.a.q.unlock();
        }
    }
}
