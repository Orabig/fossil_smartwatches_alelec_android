package com.fossil;

import com.fossil.gs4;
import com.fossil.m24;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br5$g$b implements m24.e<gs4.d, gs4.c> {
    @DexIgnore
    public void a(UpdateFirmwareUsecase.c cVar) {
        wg6.b(cVar, "errorValue");
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(UpdateFirmwareUsecase.d dVar) {
        wg6.b(dVar, "responseValue");
    }
}
