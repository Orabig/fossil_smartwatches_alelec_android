package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class se1 extends em0 {
    @DexIgnore
    public static /* final */ xc1 CREATOR; // = new xc1((qg6) null);
    @DexIgnore
    public /* final */ vd0 c;

    @DexIgnore
    public se1(byte b, vd0 vd0) {
        super(xh0.MICRO_APP_EVENT, b);
        this.c = vd0;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(super.a(), bm0.MICRO_APP_EVENT, (Object) this.c.a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(se1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            se1 se1 = (se1) obj;
            return this.a == se1.a && this.b == se1.b && !(wg6.a(this.c, se1.c) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.MicroAppAsyncEvent");
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode() + (((this.a.hashCode() * 31) + this.b) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public /* synthetic */ se1(Parcel parcel, qg6 qg6) {
        super(parcel);
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            wg6.a(createByteArray, "parcel.createByteArray()!!");
            this.c = new vd0(createByteArray);
            return;
        }
        wg6.a();
        throw null;
    }
}
