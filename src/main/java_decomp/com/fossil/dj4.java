package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dj4 implements Factory<cj4> {
    @DexIgnore
    public /* final */ Provider<zs5> a;
    @DexIgnore
    public /* final */ Provider<bt5> b;
    @DexIgnore
    public /* final */ Provider<nr4> c;
    @DexIgnore
    public /* final */ Provider<kr4> d;

    @DexIgnore
    public dj4(Provider<zs5> provider, Provider<bt5> provider2, Provider<nr4> provider3, Provider<kr4> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static dj4 a(Provider<zs5> provider, Provider<bt5> provider2, Provider<nr4> provider3, Provider<kr4> provider4) {
        return new dj4(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static cj4 b(Provider<zs5> provider, Provider<bt5> provider2, Provider<nr4> provider3, Provider<kr4> provider4) {
        return new cj4(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public cj4 get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
