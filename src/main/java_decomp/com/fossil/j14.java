package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiUserService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j14 implements Factory<AuthApiUserService> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<ip4> b;
    @DexIgnore
    public /* final */ Provider<mp4> c;

    @DexIgnore
    public j14(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static j14 a(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        return new j14(b14, provider, provider2);
    }

    @DexIgnore
    public static AuthApiUserService b(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        return a(b14, provider.get(), provider2.get());
    }

    @DexIgnore
    public static AuthApiUserService a(b14 b14, ip4 ip4, mp4 mp4) {
        AuthApiUserService b2 = b14.b(ip4, mp4);
        z76.a(b2, "Cannot return null from a non-@Nullable @Provides method");
        return b2;
    }

    @DexIgnore
    public AuthApiUserService get() {
        return b(this.a, this.b, this.c);
    }
}
