package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l50 extends jd0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public dc0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<l50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    wg6.a(createByteArray, "parcel.createByteArray()!!");
                    Parcelable readParcelable = parcel.readParcelable(dc0.class.getClassLoader());
                    if (readParcelable != null) {
                        return new l50(readString, createByteArray, (dc0) readParcelable);
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new l50[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ l50(String str, byte[] bArr, dc0 dc0, int i, qg6 qg6) {
        this(str, bArr, (i & 4) != 0 ? new dc0(0, 62, 0) : dc0);
    }

    @DexIgnore
    public final void a(dc0 dc0) {
        this.d = dc0;
    }

    @DexIgnore
    public final JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("image_name", d()).put("pos", this.d.a());
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final dc0 getPositionConfig() {
        return this.d;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }

    @DexIgnore
    public l50(String str, byte[] bArr, dc0 dc0) {
        super(str, bArr);
        this.d = dc0;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = super.a().put("pos", this.d.a());
        wg6.a(put, "super.toJSONObject()\n   \u2026ionConfig.toJSONObject())");
        return put;
    }
}
