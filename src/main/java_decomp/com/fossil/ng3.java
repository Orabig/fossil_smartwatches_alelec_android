package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ng3 extends cj3 implements Cloneable {
    @DexIgnore
    public float a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;

    @DexIgnore
    public ng3(float f, float f2, float f3) {
        this.b = f;
        this.a = f2;
        this.d = f3;
        if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            return;
        }
        throw new IllegalArgumentException("cradleVerticalOffset must be positive.");
    }

    @DexIgnore
    public void a(float f, float f2, float f3, jj3 jj3) {
        float f4 = f;
        jj3 jj32 = jj3;
        float f5 = this.c;
        if (f5 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            jj32.a(f4, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            return;
        }
        float f6 = ((this.b * 2.0f) + f5) / 2.0f;
        float f7 = f3 * this.a;
        float f8 = f2 + this.e;
        float f9 = (this.d * f3) + ((1.0f - f3) * f6);
        if (f9 / f6 >= 1.0f) {
            jj32.a(f4, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            return;
        }
        float f10 = f6 + f7;
        float f11 = f9 + f7;
        float sqrt = (float) Math.sqrt((double) ((f10 * f10) - (f11 * f11)));
        float f12 = f8 - sqrt;
        float f13 = f8 + sqrt;
        float degrees = (float) Math.toDegrees(Math.atan((double) (sqrt / f11)));
        float f14 = 90.0f - degrees;
        jj32.a(f12, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float f15 = f7 * 2.0f;
        float f16 = degrees;
        jj3.a(f12 - f7, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f12 + f7, f15, 270.0f, degrees);
        jj3.a(f8 - f6, (-f6) - f9, f8 + f6, f6 - f9, 180.0f - f14, (f14 * 2.0f) - 180.0f);
        jj3.a(f13 - f7, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f13 + f7, f15, 270.0f - f16, f16);
        jj32.a(f4, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public float b() {
        return this.b;
    }

    @DexIgnore
    public float c() {
        return this.a;
    }

    @DexIgnore
    public float d() {
        return this.c;
    }

    @DexIgnore
    public void e(float f) {
        this.e = f;
    }

    @DexIgnore
    public void b(float f) {
        this.b = f;
    }

    @DexIgnore
    public void c(float f) {
        this.a = f;
    }

    @DexIgnore
    public void d(float f) {
        this.c = f;
    }

    @DexIgnore
    public float e() {
        return this.e;
    }

    @DexIgnore
    public float a() {
        return this.d;
    }

    @DexIgnore
    public void a(float f) {
        this.d = f;
    }
}
