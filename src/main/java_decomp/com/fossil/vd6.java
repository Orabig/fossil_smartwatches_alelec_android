package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vd6 extends ud6 {
    @DexIgnore
    public static final <T> boolean a(Collection<? super T> collection, Iterable<? extends T> iterable) {
        wg6.b(collection, "$this$addAll");
        wg6.b(iterable, "elements");
        if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        }
        boolean z = false;
        for (Object add : iterable) {
            if (collection.add(add)) {
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public static final <T> boolean b(Collection<? super T> collection, Iterable<? extends T> iterable) {
        wg6.b(collection, "$this$retainAll");
        wg6.b(iterable, "elements");
        return oh6.a((Object) collection).retainAll(rd6.a(iterable, (Iterable<? extends T>) collection));
    }

    @DexIgnore
    public static final <T> boolean a(Collection<? super T> collection, T[] tArr) {
        wg6.b(collection, "$this$addAll");
        wg6.b(tArr, "elements");
        return collection.addAll(md6.b(tArr));
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, hg6<? super T, Boolean> hg6) {
        wg6.b(iterable, "$this$retainAll");
        wg6.b(hg6, "predicate");
        return a(iterable, hg6, false);
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, hg6<? super T, Boolean> hg6, boolean z) {
        Iterator<? extends T> it = iterable.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            if (hg6.invoke(it.next()).booleanValue() == z) {
                it.remove();
                z2 = true;
            }
        }
        return z2;
    }

    @DexIgnore
    public static final <T> boolean a(List<T> list, hg6<? super T, Boolean> hg6) {
        wg6.b(list, "$this$removeAll");
        wg6.b(hg6, "predicate");
        return a(list, hg6, true);
    }

    @DexIgnore
    public static final <T> boolean a(List<T> list, hg6<? super T, Boolean> hg6, boolean z) {
        int i;
        if (list instanceof RandomAccess) {
            int a = qd6.a(list);
            if (a >= 0) {
                int i2 = 0;
                i = 0;
                while (true) {
                    T t = list.get(i2);
                    if (hg6.invoke(t).booleanValue() != z) {
                        if (i != i2) {
                            list.set(i, t);
                        }
                        i++;
                    }
                    if (i2 == a) {
                        break;
                    }
                    i2++;
                }
            } else {
                i = 0;
            }
            if (i >= list.size()) {
                return false;
            }
            int a2 = qd6.a(list);
            if (a2 < i) {
                return true;
            }
            while (true) {
                list.remove(a2);
                if (a2 == i) {
                    return true;
                }
                a2--;
            }
        } else if (list != null) {
            return a(oh6.b(list), hg6, z);
        } else {
            throw new rc6("null cannot be cast to non-null type kotlin.collections.MutableIterable<T>");
        }
    }
}
