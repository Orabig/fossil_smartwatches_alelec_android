package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj2 extends fn2<nj2, a> implements to2 {
    @DexIgnore
    public static /* final */ nj2 zzf;
    @DexIgnore
    public static volatile yo2<nj2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public long zze;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<nj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(nj2.zzf);
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }
    }

    /*
    static {
        nj2 nj2 = new nj2();
        zzf = nj2;
        fn2.a(nj2.class, nj2);
    }
    */

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new nj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\u0002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                yo2<nj2> yo2 = zzg;
                if (yo2 == null) {
                    synchronized (nj2.class) {
                        yo2 = zzg;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzf);
                            zzg = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
