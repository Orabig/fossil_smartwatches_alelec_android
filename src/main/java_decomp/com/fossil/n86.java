package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n86<T> implements p86<T> {
    @DexIgnore
    public /* final */ p86<T> a;

    @DexIgnore
    public n86(p86<T> p86) {
        this.a = p86;
    }

    @DexIgnore
    public abstract T a(Context context);

    @DexIgnore
    public final synchronized T a(Context context, q86<T> q86) throws Exception {
        T a2;
        a2 = a(context);
        if (a2 == null) {
            a2 = this.a != null ? this.a.a(context, q86) : q86.a(context);
            a(context, a2);
        }
        return a2;
    }

    @DexIgnore
    public abstract void b(Context context, T t);

    @DexIgnore
    public final void a(Context context, T t) {
        if (t != null) {
            b(context, t);
            return;
        }
        throw new NullPointerException();
    }
}
