package com.fossil;

import java.util.regex.Pattern;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss0 extends n40 {
    @DexIgnore
    public Pattern a;

    @DexIgnore
    public ss0(String str) throws IllegalArgumentException {
        Pattern compile = Pattern.compile(str);
        wg6.a(compile, "Pattern.compile(serialNumberRegex)");
        this.a = compile;
    }

    @DexIgnore
    public boolean a(ii1 ii1) {
        return this.a.matcher(ii1.r.getSerialNumber()).matches();
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.FILTER_TYPE, (Object) "serial_number_pattern"), bm0.SERIAL_NUMBER_REGEX, (Object) this.a.toString());
    }
}
