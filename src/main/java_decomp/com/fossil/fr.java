package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.fossil.hy;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fr implements ComponentCallbacks2, ny, ar<er<Drawable>> {
    @DexIgnore
    public static /* final */ nz q; // = ((nz) nz.b((Class<?>) Bitmap.class).J());
    @DexIgnore
    public /* final */ wq a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ my c;
    @DexIgnore
    public /* final */ sy d;
    @DexIgnore
    public /* final */ ry e;
    @DexIgnore
    public /* final */ ty f;
    @DexIgnore
    public /* final */ Runnable g;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ hy i;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<mz<Object>> j;
    @DexIgnore
    public nz o;
    @DexIgnore
    public boolean p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            fr frVar = fr.this;
            frVar.c.a(frVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements hy.a {
        @DexIgnore
        public /* final */ sy a;

        @DexIgnore
        public b(sy syVar) {
            this.a = syVar;
        }

        @DexIgnore
        public void a(boolean z) {
            if (z) {
                synchronized (fr.this) {
                    this.a.d();
                }
            }
        }
    }

    /*
    static {
        nz nzVar = (nz) nz.b((Class<?>) qx.class).J();
        nz nzVar2 = (nz) ((nz) nz.b(ft.b).a(br.LOW)).a(true);
    }
    */

    @DexIgnore
    public fr(wq wqVar, my myVar, ry ryVar, Context context) {
        this(wqVar, myVar, ryVar, new sy(), wqVar.d(), context);
    }

    @DexIgnore
    public synchronized void a(nz nzVar) {
        this.o = (nz) ((nz) nzVar.clone()).a();
    }

    @DexIgnore
    public synchronized void b() {
        this.f.b();
        for (yz<?> a2 : this.f.f()) {
            a(a2);
        }
        this.f.e();
        this.d.a();
        this.c.b(this);
        this.c.b(this.i);
        this.h.removeCallbacks(this.g);
        this.a.b(this);
    }

    @DexIgnore
    public synchronized void c() {
        k();
        this.f.c();
    }

    @DexIgnore
    public er<Bitmap> e() {
        return a(Bitmap.class).a((gz<?>) q);
    }

    @DexIgnore
    public er<Drawable> f() {
        return a(Drawable.class);
    }

    @DexIgnore
    public List<mz<Object>> g() {
        return this.j;
    }

    @DexIgnore
    public synchronized nz h() {
        return this.o;
    }

    @DexIgnore
    public synchronized void i() {
        this.d.b();
    }

    @DexIgnore
    public synchronized void j() {
        i();
        for (fr i2 : this.e.a()) {
            i2.i();
        }
    }

    @DexIgnore
    public synchronized void k() {
        this.d.c();
    }

    @DexIgnore
    public synchronized void l() {
        this.d.e();
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        if (i2 == 60 && this.p) {
            j();
        }
    }

    @DexIgnore
    public synchronized String toString() {
        return super.toString() + "{tracker=" + this.d + ", treeNode=" + this.e + "}";
    }

    @DexIgnore
    public synchronized void a() {
        l();
        this.f.a();
    }

    @DexIgnore
    public fr(wq wqVar, my myVar, ry ryVar, sy syVar, iy iyVar, Context context) {
        this.f = new ty();
        this.g = new a();
        this.h = new Handler(Looper.getMainLooper());
        this.a = wqVar;
        this.c = myVar;
        this.e = ryVar;
        this.d = syVar;
        this.b = context;
        this.i = iyVar.a(context.getApplicationContext(), new b(syVar));
        if (r00.c()) {
            this.h.post(this.g);
        } else {
            myVar.a(this);
        }
        myVar.a(this.i);
        this.j = new CopyOnWriteArrayList<>(wqVar.f().b());
        a(wqVar.f().c());
        wqVar.a(this);
    }

    @DexIgnore
    public final void c(yz<?> yzVar) {
        boolean b2 = b(yzVar);
        jz d2 = yzVar.d();
        if (!b2 && !this.a.a(yzVar) && d2 != null) {
            yzVar.a((jz) null);
            d2.clear();
        }
    }

    @DexIgnore
    public er<Drawable> a(String str) {
        return f().a(str);
    }

    @DexIgnore
    public er<Drawable> a(Uri uri) {
        return f().a(uri);
    }

    @DexIgnore
    public er<Drawable> a(Integer num) {
        return f().a(num);
    }

    @DexIgnore
    public er<Drawable> a(byte[] bArr) {
        return f().b(bArr);
    }

    @DexIgnore
    public er<Drawable> a(Object obj) {
        return f().a(obj);
    }

    @DexIgnore
    public <ResourceType> er<ResourceType> a(Class<ResourceType> cls) {
        return new er<>(this.a, this, cls, this.b);
    }

    @DexIgnore
    public synchronized boolean b(yz<?> yzVar) {
        jz d2 = yzVar.d();
        if (d2 == null) {
            return true;
        }
        if (!this.d.a(d2)) {
            return false;
        }
        this.f.b(yzVar);
        yzVar.a((jz) null);
        return true;
    }

    @DexIgnore
    public void a(yz<?> yzVar) {
        if (yzVar != null) {
            c(yzVar);
        }
    }

    @DexIgnore
    public synchronized void a(yz<?> yzVar, jz jzVar) {
        this.f.a(yzVar);
        this.d.b(jzVar);
    }

    @DexIgnore
    public <T> gr<?, T> b(Class<T> cls) {
        return this.a.f().a(cls);
    }
}
