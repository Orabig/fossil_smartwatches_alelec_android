package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rg2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<rg2> CREATOR; // = new sg2();
    @DexIgnore
    public static /* final */ List<d12> h; // = Collections.emptyList();
    @DexIgnore
    public LocationRequest a;
    @DexIgnore
    public List<d12> b;
    @DexIgnore
    public String c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public String g;

    @DexIgnore
    public rg2(LocationRequest locationRequest, List<d12> list, String str, boolean z, boolean z2, boolean z3, String str2) {
        this.a = locationRequest;
        this.b = list;
        this.c = str;
        this.d = z;
        this.e = z2;
        this.f = z3;
        this.g = str2;
    }

    @DexIgnore
    @Deprecated
    public static rg2 a(LocationRequest locationRequest) {
        return new rg2(locationRequest, h, (String) null, false, false, false, (String) null);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof rg2)) {
            return false;
        }
        rg2 rg2 = (rg2) obj;
        return u12.a(this.a, rg2.a) && u12.a(this.b, rg2.b) && u12.a(this.c, rg2.c) && this.d == rg2.d && this.e == rg2.e && this.f == rg2.f && u12.a(this.g, rg2.g);
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        if (this.c != null) {
            sb.append(" tag=");
            sb.append(this.c);
        }
        if (this.g != null) {
            sb.append(" moduleId=");
            sb.append(this.g);
        }
        sb.append(" hideAppOps=");
        sb.append(this.d);
        sb.append(" clients=");
        sb.append(this.b);
        sb.append(" forceCoarseLocation=");
        sb.append(this.e);
        if (this.f) {
            sb.append(" exemptFromBackgroundThrottle");
        }
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, (Parcelable) this.a, i, false);
        g22.c(parcel, 5, this.b, false);
        g22.a(parcel, 6, this.c, false);
        g22.a(parcel, 7, this.d);
        g22.a(parcel, 8, this.e);
        g22.a(parcel, 9, this.f);
        g22.a(parcel, 10, this.g, false);
        g22.a(parcel, a2);
    }
}
