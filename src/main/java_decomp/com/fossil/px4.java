package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px4 implements MembersInjector<HomeAlertsFragment> {
    @DexIgnore
    public static void a(HomeAlertsFragment homeAlertsFragment, DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter) {
        homeAlertsFragment.o = doNotDisturbScheduledTimePresenter;
    }
}
