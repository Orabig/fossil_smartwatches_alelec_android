package com.fossil;

import com.portfolio.platform.ApplicationEventListener;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class z04 extends ch6 {
    @DexIgnore
    public static /* final */ ni6 INSTANCE; // = new z04();

    @DexIgnore
    public Object get(Object obj) {
        return eg6.a((ApplicationEventListener) obj);
    }

    @DexIgnore
    public String getName() {
        return "javaClass";
    }

    @DexIgnore
    public hi6 getOwner() {
        return kh6.a(eg6.class, "app_fossilRelease");
    }

    @DexIgnore
    public String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
