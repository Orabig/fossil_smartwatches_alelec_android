package com.fossil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.concurrent.CountDownLatch;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s20 {
    @DexIgnore
    public /* final */ e a;
    @DexIgnore
    public /* final */ AlertDialog.Builder b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ e a;

        @DexIgnore
        public a(e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            this.a.a(true);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ e a;

        @DexIgnore
        public b(e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            this.a.a(false);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ d a;
        @DexIgnore
        public /* final */ /* synthetic */ e b;

        @DexIgnore
        public c(d dVar, e eVar) {
            this.a = dVar;
            this.b = eVar;
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            this.a.a(true);
            this.b.a(true);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    public s20(AlertDialog.Builder builder, e eVar) {
        this.a = eVar;
        this.b = builder;
    }

    @DexIgnore
    public static int a(float f, int i) {
        return (int) (f * ((float) i));
    }

    @DexIgnore
    public static s20 a(Activity activity, tb6 tb6, d dVar) {
        e eVar = new e((a) null);
        h30 h30 = new h30(activity, tb6);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        ScrollView a2 = a(activity, h30.c());
        builder.setView(a2).setTitle(h30.e()).setCancelable(false).setNeutralButton(h30.d(), new a(eVar));
        if (tb6.d) {
            builder.setNegativeButton(h30.b(), new b(eVar));
        }
        if (tb6.f) {
            builder.setPositiveButton(h30.a(), new c(dVar, eVar));
        }
        return new s20(builder, eVar);
    }

    @DexIgnore
    public boolean b() {
        return this.a.b();
    }

    @DexIgnore
    public void c() {
        this.b.show();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ CountDownLatch b;

        @DexIgnore
        public e() {
            this.a = false;
            this.b = new CountDownLatch(1);
        }

        @DexIgnore
        public void a(boolean z) {
            this.a = z;
            this.b.countDown();
        }

        @DexIgnore
        public boolean b() {
            return this.a;
        }

        @DexIgnore
        public void a() {
            try {
                this.b.await();
            } catch (InterruptedException unused) {
            }
        }

        @DexIgnore
        public /* synthetic */ e(a aVar) {
            this();
        }
    }

    @DexIgnore
    public static ScrollView a(Activity activity, String str) {
        float f = activity.getResources().getDisplayMetrics().density;
        int a2 = a(f, 5);
        TextView textView = new TextView(activity);
        textView.setAutoLinkMask(15);
        textView.setText(str);
        textView.setTextAppearance(activity, 16973892);
        textView.setPadding(a2, a2, a2, a2);
        textView.setFocusable(false);
        ScrollView scrollView = new ScrollView(activity);
        scrollView.setPadding(a(f, 14), a(f, 2), a(f, 10), a(f, 12));
        scrollView.addView(textView);
        return scrollView;
    }

    @DexIgnore
    public void a() {
        this.a.a();
    }
}
