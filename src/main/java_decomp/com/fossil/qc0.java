package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qc0 extends tc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ w40 c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(u90.class.getClassLoader());
            if (readParcelable != null) {
                wg6.a(readParcelable, "parcel.readParcelable<Co\u2026class.java.classLoader)!!");
                u90 u90 = (u90) readParcelable;
                uc0 uc0 = (uc0) parcel.readParcelable(uc0.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(w40.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new qc0(u90, uc0, (w40) readParcelable2, parcel.readInt(), parcel.readInt());
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new qc0[i];
        }
    }

    @DexIgnore
    public qc0(u90 u90, w40 w40, int i, int i2) {
        super(u90, (uc0) null);
        this.d = i;
        this.e = i2;
        this.c = w40;
    }

    @DexIgnore
    public byte[] a(short s, w40 w40) {
        try {
            kk1 kk1 = kk1.d;
            x90 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return kk1.a(s, w40, new ay0(((z90) deviceRequest).d(), new w40(this.c.getMajor(), this.c.getMinor()), this.d, this.e).getData());
            }
            throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (sw0 e2) {
            qs0.h.a(e2);
            return new byte[0];
        }
    }

    @DexIgnore
    public JSONObject b() {
        return cw0.a(cw0.a(cw0.a(super.b(), bm0.MICRO_APP_VERSION, (Object) this.c.toString()), bm0.HOUR, (Object) Integer.valueOf(this.d)), bm0.MINUTE, (Object) Integer.valueOf(this.e));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(qc0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return true;
        }
        if (obj != null) {
            qc0 qc0 = (qc0) obj;
            return !(wg6.a(this.c, qc0.c) ^ true) && this.d == qc0.d && this.e == qc0.e;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeETAMicroAppData");
    }

    @DexIgnore
    public final int getHour() {
        return this.d;
    }

    @DexIgnore
    public final w40 getMicroAppVersion() {
        return this.c;
    }

    @DexIgnore
    public final int getMinute() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = Integer.valueOf(this.d).hashCode();
        return Integer.valueOf(this.e).hashCode() + ((hashCode2 + ((hashCode + (super.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }

    @DexIgnore
    public qc0(u90 u90, uc0 uc0, w40 w40, int i, int i2) {
        super(u90, uc0);
        this.d = i;
        this.e = i2;
        this.c = w40;
    }
}
