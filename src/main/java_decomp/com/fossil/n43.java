package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n43 extends sh2 implements l43 {
    @DexIgnore
    public n43(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    public final void a(j03 j03, ra3 ra3) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) j03);
        li2.a(q, (Parcelable) ra3);
        b(1, q);
    }

    @DexIgnore
    public final void b(ra3 ra3) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) ra3);
        b(6, q);
    }

    @DexIgnore
    public final String c(ra3 ra3) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) ra3);
        Parcel a = a(11, q);
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    @DexIgnore
    public final void d(ra3 ra3) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) ra3);
        b(4, q);
    }

    @DexIgnore
    public final void a(la3 la3, ra3 ra3) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) la3);
        li2.a(q, (Parcelable) ra3);
        b(2, q);
    }

    @DexIgnore
    public final void a(j03 j03, String str, String str2) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) j03);
        q.writeString(str);
        q.writeString(str2);
        b(5, q);
    }

    @DexIgnore
    public final byte[] a(j03 j03, String str) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) j03);
        q.writeString(str);
        Parcel a = a(9, q);
        byte[] createByteArray = a.createByteArray();
        a.recycle();
        return createByteArray;
    }

    @DexIgnore
    public final void a(long j, String str, String str2, String str3) throws RemoteException {
        Parcel q = q();
        q.writeLong(j);
        q.writeString(str);
        q.writeString(str2);
        q.writeString(str3);
        b(10, q);
    }

    @DexIgnore
    public final void a(ab3 ab3, ra3 ra3) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) ab3);
        li2.a(q, (Parcelable) ra3);
        b(12, q);
    }

    @DexIgnore
    public final void a(ab3 ab3) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) ab3);
        b(13, q);
    }

    @DexIgnore
    public final List<la3> a(String str, String str2, boolean z, ra3 ra3) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        li2.a(q, z);
        li2.a(q, (Parcelable) ra3);
        Parcel a = a(14, q);
        ArrayList<la3> createTypedArrayList = a.createTypedArrayList(la3.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final List<la3> a(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        q.writeString(str3);
        li2.a(q, z);
        Parcel a = a(15, q);
        ArrayList<la3> createTypedArrayList = a.createTypedArrayList(la3.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final List<ab3> a(String str, String str2, ra3 ra3) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        li2.a(q, (Parcelable) ra3);
        Parcel a = a(16, q);
        ArrayList<ab3> createTypedArrayList = a.createTypedArrayList(ab3.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final List<ab3> a(String str, String str2, String str3) throws RemoteException {
        Parcel q = q();
        q.writeString(str);
        q.writeString(str2);
        q.writeString(str3);
        Parcel a = a(17, q);
        ArrayList<ab3> createTypedArrayList = a.createTypedArrayList(ab3.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final void a(ra3 ra3) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) ra3);
        b(18, q);
    }
}
