package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xh6 implements Iterable<Long>, ph6 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public xh6(long j, long j2, long j3) {
        if (j3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (j3 != Long.MIN_VALUE) {
            this.a = j;
            this.b = vf6.b(j, j2, j3);
            this.c = j3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Long.MIN_VALUE to avoid overflow on negation.");
        }
    }

    @DexIgnore
    public final long a() {
        return this.a;
    }

    @DexIgnore
    public final long b() {
        return this.b;
    }

    @DexIgnore
    public de6 iterator() {
        return new yh6(this.a, this.b, this.c);
    }
}
