package com.fossil;

import android.content.Context;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vk2 {
    @DexIgnore
    public /* final */ Uri a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public vk2(Uri uri) {
        this((String) null, uri, "", "", false, false, false, false, (zk2<Context, Boolean>) null);
    }

    @DexIgnore
    public final pk2<Long> a(String str, long j) {
        return pk2.b(this, str, j);
    }

    @DexIgnore
    public vk2(String str, Uri uri, String str2, String str3, boolean z, boolean z2, boolean z3, boolean z4, zk2<Context, Boolean> zk2) {
        this.a = uri;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final pk2<Boolean> a(String str, boolean z) {
        return pk2.b(this, str, z);
    }

    @DexIgnore
    public final pk2<Double> a(String str, double d) {
        return pk2.b(this, str, -3.0d);
    }

    @DexIgnore
    public final pk2<String> a(String str, String str2) {
        return pk2.b(this, str, str2);
    }
}
