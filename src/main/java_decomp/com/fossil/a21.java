package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a21 {
    @DexIgnore
    public /* synthetic */ a21(qg6 qg6) {
    }

    @DexIgnore
    public final w31 a(short s) {
        for (w31 w31 : w31.values()) {
            short s2 = w31.a;
            if (((short) (s | s2)) == s2) {
                return w31;
            }
        }
        return null;
    }

    @DexIgnore
    public final w31 a(byte b) {
        for (w31 w31 : w31.values()) {
            if (w31.b == b) {
                return w31;
            }
        }
        return null;
    }
}
