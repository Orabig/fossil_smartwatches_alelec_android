package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.d26;
import com.fossil.e26;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g36 implements b36 {
    @DexIgnore
    public static b e;
    @DexIgnore
    public static String f;
    @DexIgnore
    public Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public Handler b;
        @DexIgnore
        public Context c;
        @DexIgnore
        public Runnable d;
        @DexIgnore
        public Runnable e;

        @DexIgnore
        public b(Context context) {
            this.a = false;
            this.b = new Handler(Looper.getMainLooper());
            this.d = new h36(this);
            this.e = new i36(this);
            this.c = context;
        }

        @DexIgnore
        public final void onActivityCreated(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public final void onActivityDestroyed(Activity activity) {
        }

        @DexIgnore
        public final void onActivityPaused(Activity activity) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", activity.getComponentName().getClassName() + "  onActivityPaused");
            this.b.removeCallbacks(this.e);
            this.b.postDelayed(this.d, 800);
        }

        @DexIgnore
        public final void onActivityResumed(Activity activity) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", activity.getComponentName().getClassName() + "  onActivityResumed");
            this.b.removeCallbacks(this.d);
            this.b.postDelayed(this.e, 800);
        }

        @DexIgnore
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public final void onActivityStarted(Activity activity) {
        }

        @DexIgnore
        public final void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public g36(Context context, String str, boolean z) {
        h26.d("MicroMsg.SDK.WXApiImplV10", "<init>, appId = " + str + ", checkSignature = " + z);
        this.a = context;
        this.b = str;
        this.c = z;
    }

    @DexIgnore
    public final void a(Context context, String str) {
        String str2 = "AWXOP" + str;
        n36.b(context, str2);
        n36.a(true);
        n36.a(o36.PERIOD);
        n36.c(60);
        n36.c(context, "Wechat_Sdk");
        try {
            p36.a(context, str2, "2.0.3");
        } catch (l36 e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final boolean a() {
        if (!this.d) {
            try {
                PackageInfo packageInfo = this.a.getPackageManager().getPackageInfo("com.tencent.mm", 64);
                if (packageInfo == null) {
                    return false;
                }
                return f36.a(this.a, packageInfo.signatures, this.c);
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            }
        } else {
            throw new IllegalStateException("isWXAppInstalled fail, WXMsgImpl has been detached");
        }
    }

    @DexIgnore
    public final boolean a(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/createChatroom"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_basereq_transaction"), bundle.getString("_wxapi_create_chatroom_group_id"), bundle.getString("_wxapi_create_chatroom_chatroom_name"), bundle.getString("_wxapi_create_chatroom_chatroom_nickname"), bundle.getString("_wxapi_create_chatroom_ext_msg")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean a(Intent intent, c36 c36) {
        try {
            if (!f36.a(intent, "com.tencent.mm.openapi.token")) {
                h26.c("MicroMsg.SDK.WXApiImplV10", "handleIntent fail, intent not from weixin msg");
                return false;
            } else if (!this.d) {
                String stringExtra = intent.getStringExtra("_mmessage_content");
                int intExtra = intent.getIntExtra("_mmessage_sdkVersion", 0);
                String stringExtra2 = intent.getStringExtra("_mmessage_appPackage");
                if (stringExtra2 != null) {
                    if (stringExtra2.length() != 0) {
                        if (!a(intent.getByteArrayExtra("_mmessage_checksum"), f26.a(stringExtra, intExtra, stringExtra2))) {
                            h26.a("MicroMsg.SDK.WXApiImplV10", "checksum fail");
                            return false;
                        }
                        int intExtra2 = intent.getIntExtra("_wxapi_command_type", 0);
                        if (intExtra2 == 9) {
                            c36.a((n26) new o26(intent.getExtras()));
                            return true;
                        } else if (intExtra2 == 12) {
                            c36.a((n26) new s26(intent.getExtras()));
                            return true;
                        } else if (intExtra2 == 14) {
                            c36.a((n26) new q26(intent.getExtras()));
                            return true;
                        } else if (intExtra2 != 15) {
                            switch (intExtra2) {
                                case 1:
                                    c36.a((n26) new w26(intent.getExtras()));
                                    return true;
                                case 2:
                                    c36.a((n26) new x26(intent.getExtras()));
                                    return true;
                                case 3:
                                    c36.a((m26) new t26(intent.getExtras()));
                                    return true;
                                case 4:
                                    c36.a((m26) new y26(intent.getExtras()));
                                    return true;
                                case 5:
                                    c36.a((n26) new a36(intent.getExtras()));
                                    return true;
                                case 6:
                                    c36.a((m26) new u26(intent.getExtras()));
                                    return true;
                                default:
                                    h26.a("MicroMsg.SDK.WXApiImplV10", "unknown cmd = " + intExtra2);
                                    break;
                            }
                            return false;
                        } else {
                            c36.a((n26) new r26(intent.getExtras()));
                            return true;
                        }
                    }
                }
                h26.a("MicroMsg.SDK.WXApiImplV10", "invalid argument");
                return false;
            } else {
                throw new IllegalStateException("handleIntent fail, WXMsgImpl has been detached");
            }
        } catch (Exception e2) {
            h26.a("MicroMsg.SDK.WXApiImplV10", "handleIntent fail, ex = %s", e2.getMessage());
        }
    }

    @DexIgnore
    public final boolean a(m26 m26) {
        String str;
        if (!this.d) {
            if (!f36.a(this.a, "com.tencent.mm", this.c)) {
                str = "sendReq failed for wechat app signature check failed";
            } else if (!m26.a()) {
                str = "sendReq checkArgs fail";
            } else {
                h26.d("MicroMsg.SDK.WXApiImplV10", "sendReq, req type = " + m26.b());
                Bundle bundle = new Bundle();
                m26.b(bundle);
                if (m26.b() == 5) {
                    return j(this.a, bundle);
                }
                if (m26.b() == 7) {
                    return d(this.a, bundle);
                }
                if (m26.b() == 8) {
                    return f(this.a, bundle);
                }
                if (m26.b() == 10) {
                    return e(this.a, bundle);
                }
                if (m26.b() == 9) {
                    return c(this.a, bundle);
                }
                if (m26.b() == 11) {
                    return h(this.a, bundle);
                }
                if (m26.b() == 12) {
                    return i(this.a, bundle);
                }
                if (m26.b() == 13) {
                    return g(this.a, bundle);
                }
                if (m26.b() == 14) {
                    return a(this.a, bundle);
                }
                if (m26.b() == 15) {
                    return b(this.a, bundle);
                }
                d26.a aVar = new d26.a();
                aVar.e = bundle;
                aVar.c = "weixin://sendreq?appid=" + this.b;
                aVar.a = "com.tencent.mm";
                aVar.b = "com.tencent.mm.plugin.base.stub.WXEntryActivity";
                return d26.a(this.a, aVar);
            }
            h26.a("MicroMsg.SDK.WXApiImplV10", str);
            return false;
        }
        throw new IllegalStateException("sendReq fail, WXMsgImpl has been detached");
    }

    @DexIgnore
    public final boolean a(String str) {
        Application application;
        if (this.d) {
            throw new IllegalStateException("registerApp fail, WXMsgImpl has been detached");
        } else if (!f36.a(this.a, "com.tencent.mm", this.c)) {
            h26.a("MicroMsg.SDK.WXApiImplV10", "register app failed for wechat app signature check failed");
            return false;
        } else {
            if (e == null && Build.VERSION.SDK_INT >= 14) {
                Context context = this.a;
                if (context instanceof Activity) {
                    a(context, str);
                    e = new b(this.a);
                    application = ((Activity) this.a).getApplication();
                } else if (context instanceof Service) {
                    a(context, str);
                    e = new b(this.a);
                    application = ((Service) this.a).getApplication();
                } else {
                    h26.b("MicroMsg.SDK.WXApiImplV10", "context is not instanceof Activity or Service, disable WXStat");
                }
                application.registerActivityLifecycleCallbacks(e);
            }
            h26.d("MicroMsg.SDK.WXApiImplV10", "registerApp, appId = " + str);
            if (str != null) {
                this.b = str;
            }
            h26.d("MicroMsg.SDK.WXApiImplV10", "register app " + this.a.getPackageName());
            e26.a aVar = new e26.a();
            aVar.a = "com.tencent.mm";
            aVar.b = "com.tencent.mm.plugin.openapi.Intent.ACTION_HANDLE_APP_REGISTER";
            aVar.c = "weixin://registerapp?appid=" + this.b;
            return e26.a(this.a, aVar);
        }
    }

    @DexIgnore
    public final boolean a(byte[] bArr, byte[] bArr2) {
        String str;
        if (bArr == null || bArr.length == 0 || bArr2 == null || bArr2.length == 0) {
            str = "checkSumConsistent fail, invalid arguments";
        } else if (bArr.length != bArr2.length) {
            str = "checkSumConsistent fail, length is different";
        } else {
            for (int i = 0; i < bArr.length; i++) {
                if (bArr[i] != bArr2[i]) {
                    return false;
                }
            }
            return true;
        }
        h26.a("MicroMsg.SDK.WXApiImplV10", str);
        return false;
    }

    @DexIgnore
    public final boolean b(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/joinChatroom"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_basereq_transaction"), bundle.getString("_wxapi_join_chatroom_group_id"), bundle.getString("_wxapi_join_chatroom_chatroom_nickname"), bundle.getString("_wxapi_join_chatroom_ext_msg")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean c(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/addCardToWX"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_add_card_to_wx_card_list"), bundle.getString("_wxapi_basereq_transaction")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean d(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_profile_req_scene"));
        StringBuilder sb2 = new StringBuilder();
        sb2.append(bundle.getInt("_wxapi_jump_to_biz_profile_req_profile_type"));
        Cursor query = contentResolver.query(parse, (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_jump_to_biz_profile_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_profile_req_ext_msg"), sb.toString(), sb2.toString()}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean e(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizTempSession");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_webview_req_show_type"));
        Cursor query = contentResolver.query(parse, (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_webview_req_session_from"), sb.toString()}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean f(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_webview_req_scene"));
        Cursor query = contentResolver.query(parse, (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_webview_req_ext_msg"), sb.toString()}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean g(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openBusiLuckyMoney"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_open_busi_lucky_money_timeStamp"), bundle.getString("_wxapi_open_busi_lucky_money_nonceStr"), bundle.getString("_wxapi_open_busi_lucky_money_signType"), bundle.getString("_wxapi_open_busi_lucky_money_signature"), bundle.getString("_wxapi_open_busi_lucky_money_package")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean h(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openRankList"), (String[]) null, (String) null, new String[0], (String) null);
        if (query == null) {
            return true;
        }
        query.close();
        return true;
    }

    @DexIgnore
    public final boolean i(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openWebview"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_jump_to_webview_url"), bundle.getString("_wxapi_basereq_transaction")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean j(Context context, Bundle bundle) {
        if (f == null) {
            f = new d36(context).getString("_wxapp_pay_entry_classname_", (String) null);
            h26.d("MicroMsg.SDK.WXApiImplV10", "pay, set wxappPayEntryClassname = " + f);
            if (f == null) {
                h26.a("MicroMsg.SDK.WXApiImplV10", "pay fail, wxappPayEntryClassname is null");
                return false;
            }
        }
        d26.a aVar = new d26.a();
        aVar.e = bundle;
        aVar.a = "com.tencent.mm";
        aVar.b = f;
        return d26.a(context, aVar);
    }
}
