package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cf0 implements Parcelable.Creator<tg0> {
    @DexIgnore
    public /* synthetic */ cf0(qg6 qg6) {
    }

    @DexIgnore
    public tg0 createFromParcel(Parcel parcel) {
        return new tg0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new tg0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m7createFromParcel(Parcel parcel) {
        return new tg0(parcel, (qg6) null);
    }
}
