package com.fossil;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class la5 extends RecyclerView.l {
    @DexIgnore
    public Drawable a;
    @DexIgnore
    public /* final */ Rect b; // = new Rect();
    @DexIgnore
    public int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public la5(int i) {
        this.c = i;
    }

    @DexIgnore
    public final void a(Drawable drawable) {
        wg6.b(drawable, ResourceManager.DRAWABLE);
        this.a = drawable;
    }

    @DexIgnore
    public final void b(Canvas canvas, RecyclerView recyclerView) {
        int i;
        int i2;
        canvas.save();
        if (recyclerView.getClipToPadding()) {
            i2 = recyclerView.getPaddingLeft();
            i = recyclerView.getWidth() - recyclerView.getPaddingRight();
            canvas.clipRect(i2, recyclerView.getPaddingTop(), i, recyclerView.getHeight() - recyclerView.getPaddingBottom());
        } else {
            i = recyclerView.getWidth();
            i2 = 0;
        }
        Drawable drawable = this.a;
        if (drawable != null) {
            int childCount = recyclerView.getChildCount();
            for (int i3 = 0; i3 < childCount; i3++) {
                View childAt = recyclerView.getChildAt(i3);
                recyclerView.getDecoratedBoundsWithMargins(childAt, this.b);
                int i4 = this.b.bottom;
                wg6.a((Object) childAt, "child");
                int round = i4 + Math.round(childAt.getTranslationY());
                drawable.setBounds(i2, round - drawable.getIntrinsicHeight(), i, round);
                drawable.draw(canvas);
            }
        }
        canvas.restore();
    }

    @DexIgnore
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        int i;
        wg6.b(rect, "outRect");
        wg6.b(view, "view");
        wg6.b(recyclerView, "parent");
        wg6.b(state, Constants.STATE);
        Drawable drawable = this.a;
        if (drawable != null) {
            RecyclerView.g adapter = recyclerView.getAdapter();
            if (adapter != null) {
                int itemCount = adapter.getItemCount();
                int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
                if (childAdapterPosition != 0 && (i = childAdapterPosition + 1) < itemCount) {
                    RecyclerView.g adapter2 = recyclerView.getAdapter();
                    if (adapter2 == null) {
                        wg6.a();
                        throw null;
                    } else if (adapter2.getItemViewType(i) == 1) {
                        if (this.c == 1) {
                            rect.set(0, 0, 0, drawable.getIntrinsicHeight());
                            return;
                        } else {
                            rect.set(0, 0, drawable.getIntrinsicWidth(), 0);
                            return;
                        }
                    }
                }
                rect.set(0, 0, 0, 0);
                return;
            }
            wg6.a();
            throw null;
        }
        rect.set(0, 0, 0, 0);
    }

    @DexIgnore
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        wg6.b(canvas, "c");
        wg6.b(recyclerView, "parent");
        wg6.b(state, Constants.STATE);
        if (recyclerView.getLayoutManager() != null && this.a != null) {
            if (this.c == 1) {
                b(canvas, recyclerView);
            } else {
                a(canvas, recyclerView);
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, RecyclerView recyclerView) {
        int i;
        int i2;
        canvas.save();
        int i3 = 0;
        if (recyclerView.getClipToPadding()) {
            i2 = recyclerView.getPaddingTop();
            i = recyclerView.getHeight() - recyclerView.getPaddingBottom();
            canvas.clipRect(recyclerView.getPaddingLeft(), i2, recyclerView.getWidth() - recyclerView.getPaddingRight(), i);
        } else {
            i = recyclerView.getHeight();
            i2 = 0;
        }
        Drawable drawable = this.a;
        if (drawable != null) {
            int childCount = recyclerView.getChildCount();
            while (i3 < childCount) {
                View childAt = recyclerView.getChildAt(i3);
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    layoutManager.c(childAt, this.b);
                    int i4 = this.b.right;
                    wg6.a((Object) childAt, "child");
                    int round = i4 + Math.round(childAt.getTranslationX());
                    drawable.setBounds(round - drawable.getIntrinsicWidth(), i2, round, i);
                    drawable.draw(canvas);
                    i3++;
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        canvas.restore();
    }
}
