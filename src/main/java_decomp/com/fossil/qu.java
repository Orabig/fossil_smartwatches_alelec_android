package com.fossil;

import android.annotation.SuppressLint;
import com.fossil.ru;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qu extends n00<vr, rt<?>> implements ru {
    @DexIgnore
    public ru.a d;

    @DexIgnore
    public qu(long j) {
        super(j);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ rt a(vr vrVar, rt rtVar) {
        return (rt) super.b(vrVar, rtVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void a(vr vrVar, rt<?> rtVar) {
        ru.a aVar = this.d;
        if (aVar != null && rtVar != null) {
            aVar.a(rtVar);
        }
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ rt a(vr vrVar) {
        return (rt) super.c(vrVar);
    }

    @DexIgnore
    public void a(ru.a aVar) {
        this.d = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public int b(rt<?> rtVar) {
        if (rtVar == null) {
            return super.b(null);
        }
        return rtVar.b();
    }

    @DexIgnore
    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 40) {
            a();
        } else if (i >= 20 || i == 15) {
            a(c() / 2);
        }
    }
}
