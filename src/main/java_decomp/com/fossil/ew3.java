package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ew3 extends lw3 {
    @DexIgnore
    public static volatile ew3[] f;
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String[] c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public ew3() {
        a();
    }

    @DexIgnore
    public static ew3[] b() {
        if (f == null) {
            synchronized (jw3.a) {
                if (f == null) {
                    f = new ew3[0];
                }
            }
        }
        return f;
    }

    @DexIgnore
    public ew3 a() {
        this.a = "";
        this.b = "";
        this.c = nw3.a;
        this.d = "";
        this.e = "";
        return this;
    }

    @DexIgnore
    public ew3 a(iw3 iw3) throws IOException {
        while (true) {
            int j = iw3.j();
            if (j == 0) {
                return this;
            }
            if (j == 10) {
                this.a = iw3.i();
            } else if (j == 18) {
                this.b = iw3.i();
            } else if (j == 26) {
                int a2 = nw3.a(iw3, 26);
                String[] strArr = this.c;
                int length = strArr == null ? 0 : strArr.length;
                String[] strArr2 = new String[(a2 + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, strArr2, 0, length);
                }
                while (length < strArr2.length - 1) {
                    strArr2[length] = iw3.i();
                    iw3.j();
                    length++;
                }
                strArr2[length] = iw3.i();
                this.c = strArr2;
            } else if (j == 34) {
                this.d = iw3.i();
            } else if (j == 42) {
                this.e = iw3.i();
            } else if (j == 48) {
                iw3.c();
            } else if (!nw3.b(iw3, j)) {
                return this;
            }
        }
    }
}
