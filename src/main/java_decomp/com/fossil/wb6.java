package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum wb6 {
    USE_CACHE,
    SKIP_CACHE_LOOKUP,
    IGNORE_CACHE_EXPIRATION
}
