package com.fossil;

import android.app.PendingIntent;
import android.content.Intent;
import com.fossil.p6;
import com.fossil.sk4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp5 {
    @DexIgnore
    public b a;
    @DexIgnore
    public /* final */ InAppNotificationRepository b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(InAppNotification inAppNotification);
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public dp5(InAppNotificationRepository inAppNotificationRepository) {
        wg6.b(inAppNotificationRepository, "repository");
        this.b = inAppNotificationRepository;
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "listener");
        this.a = bVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(InAppNotification inAppNotification) {
        FLogger.INSTANCE.getLocal().d("InAppNotificationManager", "sendNotification()");
        Object instance = PortfolioApp.get.instance();
        PendingIntent activity = PendingIntent.getActivity(instance, 0, new Intent(instance, SplashScreenActivity.class), 134217728);
        sk4.a aVar = sk4.a;
        String title = inAppNotification.getTitle();
        String content = inAppNotification.getContent();
        wg6.a((Object) activity, "pendingIntent");
        aVar.a(instance, 1, title, content, activity, (List<? extends p6.a>) null);
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wg6.b(str, Explore.COLUMN_TITLE);
        wg6.b(str2, "messageBody");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("InAppNotificationManager", "handleNewNotification() - title = " + str + " - messageBody = " + str2);
        String uuid = UUID.randomUUID().toString();
        wg6.a((Object) uuid, "UUID.randomUUID().toString()");
        InAppNotification inAppNotification = new InAppNotification(uuid, str, str2);
        this.b.handleReceivingNotification(inAppNotification);
        if (!PortfolioApp.get.instance().B()) {
            a(inAppNotification);
            b(inAppNotification);
            return;
        }
        b bVar = this.a;
        if (bVar != null) {
            bVar.a(inAppNotification);
        }
    }

    @DexIgnore
    public final void a(InAppNotification inAppNotification) {
        wg6.b(inAppNotification, "inAppNotification");
        this.b.removeNotificationAfterSending(inAppNotification);
    }

    @DexIgnore
    public final void a() {
        this.a = null;
    }
}
