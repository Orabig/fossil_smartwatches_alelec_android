package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum nq2 {
    DOUBLE(qq2.DOUBLE, 1),
    FLOAT(qq2.FLOAT, 5),
    INT64(qq2.LONG, 0),
    UINT64(qq2.LONG, 0),
    INT32(qq2.INT, 0),
    FIXED64(qq2.LONG, 1),
    FIXED32(qq2.INT, 5),
    BOOL(qq2.BOOLEAN, 0),
    STRING(qq2.STRING, 2),
    GROUP(qq2.MESSAGE, 3),
    MESSAGE(qq2.MESSAGE, 2),
    BYTES(qq2.BYTE_STRING, 2),
    UINT32(qq2.INT, 0),
    ENUM(qq2.ENUM, 0),
    SFIXED32(qq2.INT, 5),
    SFIXED64(qq2.LONG, 1),
    SINT32(qq2.INT, 0),
    SINT64(qq2.LONG, 0);
    
    @DexIgnore
    public /* final */ qq2 zzs;
    @DexIgnore
    public /* final */ int zzt;

    @DexIgnore
    public nq2(qq2 qq2, int i) {
        this.zzs = qq2;
        this.zzt = i;
    }

    @DexIgnore
    public final qq2 zza() {
        return this.zzs;
    }

    @DexIgnore
    public final int zzb() {
        return this.zzt;
    }
}
