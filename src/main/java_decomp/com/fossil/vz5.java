package com.fossil;

import android.content.res.Resources;
import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vz5 extends RecyclerView.l {
    @DexIgnore
    public int a;

    @DexIgnore
    public vz5() {
        Resources system = Resources.getSystem();
        wg6.a((Object) system, "Resources.getSystem()");
        this.a = system.getDisplayMetrics().widthPixels;
    }

    @DexIgnore
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        wg6.b(rect, "outRect");
        wg6.b(view, "view");
        wg6.b(recyclerView, "parent");
        wg6.b(state, Constants.STATE);
        vz5.super.getItemOffsets(rect, view, recyclerView, state);
        RecyclerView.g adapter = recyclerView.getAdapter();
        if (adapter != null) {
            wg6.a((Object) adapter, "parent.adapter!!");
            int itemCount = adapter.getItemCount();
            int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
            if (itemCount > 0) {
                int width = recyclerView.getWidth();
                int width2 = view.getWidth();
                if (width == 0) {
                    width = this.a;
                }
                if (width2 == 0) {
                    width2 = this.a / 2;
                }
                if (itemCount == 1 || childAdapterPosition == 0) {
                    rect.set(Math.max(0, (width - width2) / 2), 0, 0, 0);
                } else if (childAdapterPosition == itemCount - 1) {
                    rect.set(0, 0, Math.max(0, (width - width2) / 2), 0);
                }
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
