package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum z50 {
    WEATHER("weatherSSE"),
    HEART_RATE("hrSSE"),
    STEPS("stepsSSE"),
    DATE("dateSSE"),
    CHANCE_OF_RAIN("chanceOfRainSSE"),
    SECOND_TIMEZONE("timeZone2SSE"),
    ACTIVE_MINUTES("activeMinutesSSE"),
    CALORIES("caloriesSSE"),
    BATTERY("batterySSE"),
    EMPTY("empty");
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }
    }

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public z50(String str) {
        this.a = str;
    }

    @DexIgnore
    public final Object a() {
        Object obj;
        if (qw0.a[ordinal()] != 1) {
            obj = this.a;
        } else {
            obj = JSONObject.NULL;
        }
        wg6.a(obj, "when (this) {\n          \u2026 -> rawName\n            }");
        return obj;
    }
}
