package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fb1 extends qg1 {
    @DexIgnore
    public /* final */ byte[] l;

    @DexIgnore
    public fb1(rg1 rg1, byte[] bArr, at0 at0) {
        super(hm0.WRITE_CHARACTERISTIC, rg1, at0);
        this.l = bArr;
    }

    @DexIgnore
    public void a(ue1 ue1) {
        ue1.b(this.k, this.l);
        this.j = true;
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return (p51 instanceof bd1) && ((bd1) p51).b == this.k;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.a;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        JSONObject a = super.a(z);
        if (z) {
            byte[] bArr = this.l;
            if (bArr.length < 100) {
                cw0.a(a, bm0.RAW_DATA, (Object) cw0.a(bArr, (String) null, 1));
                return a;
            }
        }
        cw0.a(a, bm0.RAW_DATA_LENGTH, (Object) Integer.valueOf(this.l.length));
        return a;
    }
}
