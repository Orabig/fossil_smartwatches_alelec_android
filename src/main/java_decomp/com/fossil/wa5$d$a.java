package com.fossil;

import com.fossil.m24;
import com.fossil.ur4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wa5$d$a implements m24.e<ur4.d, ur4.c> {
    @DexIgnore
    /* renamed from: a */
    public void onSuccess(SetReplyMessageMappingUseCase.d dVar) {
        wg6.b(dVar, "responseValue");
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "Set reply message to device success");
    }

    @DexIgnore
    public void a(SetReplyMessageMappingUseCase.c cVar) {
        wg6.b(cVar, "errorValue");
        FLogger.INSTANCE.getLocal().e("HomeDashboardPresenter", "Set reply message to device fail");
    }
}
