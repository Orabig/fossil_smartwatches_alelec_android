package com.fossil;

import android.util.Base64;
import com.fossil.fs;
import com.fossil.jv;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class av<Model, Data> implements jv<Model, Data> {
    @DexIgnore
    public /* final */ a<Data> a;

    @DexIgnore
    public interface a<Data> {
        @DexIgnore
        Data a(String str) throws IllegalArgumentException;

        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<Model> implements kv<Model, InputStream> {
        @DexIgnore
        public /* final */ a<InputStream> a; // = new a(this);

        @DexIgnore
        public jv<Model, InputStream> a(nv nvVar) {
            return new av(this.a);
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements a<InputStream> {
            @DexIgnore
            public a(c cVar) {
            }

            @DexIgnore
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }

            @DexIgnore
            public InputStream a(String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf == -1) {
                        throw new IllegalArgumentException("Missing comma in data URL.");
                    } else if (str.substring(0, indexOf).endsWith(";base64")) {
                        return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                    } else {
                        throw new IllegalArgumentException("Not a base64 image data URL.");
                    }
                } else {
                    throw new IllegalArgumentException("Not a valid image data URL.");
                }
            }

            @DexIgnore
            public void a(InputStream inputStream) throws IOException {
                inputStream.close();
            }
        }
    }

    @DexIgnore
    public av(a<Data> aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public jv.a<Data> a(Model model, int i, int i2, xr xrVar) {
        return new jv.a<>(new g00(model), new b(model.toString(), this.a));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<Data> implements fs<Data> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ a<Data> b;
        @DexIgnore
        public Data c;

        @DexIgnore
        public b(String str, a<Data> aVar) {
            this.a = str;
            this.b = aVar;
        }

        @DexIgnore
        public void a(br brVar, fs.a<? super Data> aVar) {
            try {
                this.c = this.b.a(this.a);
                aVar.a(this.c);
            } catch (IllegalArgumentException e) {
                aVar.a((Exception) e);
            }
        }

        @DexIgnore
        public pr b() {
            return pr.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<Data> getDataClass() {
            return this.b.getDataClass();
        }

        @DexIgnore
        public void a() {
            try {
                this.b.a(this.c);
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore
    public boolean a(Model model) {
        return model.toString().startsWith("data:image");
    }
}
