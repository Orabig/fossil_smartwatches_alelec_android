package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WatchSettingFragmentMapping extends WatchSettingFragmentBinding {
    @DexIgnore
    public static /* final */ ViewDataBinding.j J; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray K; // = new SparseIntArray();
    @DexIgnore
    public long I;

    /*
    static {
        K.put(2131361929, 1);
        K.put(2131363218, 2);
        K.put(2131362566, 3);
        K.put(2131363139, 4);
        K.put(2131363111, 5);
        K.put(2131361928, 6);
        K.put(2131361932, 7);
        K.put(2131362136, 8);
        K.put(2131362077, 9);
        K.put(2131363162, 10);
        K.put(2131363163, 11);
        K.put(2131362958, 12);
        K.put(2131363154, 13);
        K.put(2131363155, 14);
        K.put(2131362959, 15);
        K.put(2131363208, 16);
        K.put(2131363209, 17);
        K.put(2131363228, 18);
        K.put(2131362230, 19);
        K.put(2131362231, 20);
        K.put(2131362229, 21);
        K.put(2131363152, 22);
        K.put(2131362960, 23);
        K.put(2131363102, 24);
        K.put(2131363103, 25);
        K.put(2131363201, 26);
    }
    */

    @DexIgnore
    public WatchSettingFragmentMapping(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 27, J, K));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.I = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.I != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.I = 1;
        }
        g();
    }

    @DexIgnore
    public WatchSettingFragmentMapping(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[6], objArr[1], objArr[7], objArr[9], objArr[8], objArr[21], objArr[19], objArr[20], objArr[3], objArr[0], objArr[12], objArr[15], objArr[23], objArr[24], objArr[25], objArr[5], objArr[4], objArr[22], objArr[13], objArr[14], objArr[10], objArr[11], objArr[26], objArr[16], objArr[17], objArr[2], objArr[18]);
        this.I = -1;
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
