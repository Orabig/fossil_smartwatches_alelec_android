package com.fossil;

import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ji5$f$b extends sf6 implements ig6<il6, xe6<? super lc6<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ji5$f$b(xe6 xe6, GoalTrackingDetailPresenter.f fVar) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ji5$f$b ji5_f_b = new ji5$f$b(xe6, this.this$0);
        ji5_f_b.p$ = (il6) obj;
        return ji5_f_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ji5$f$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return gj5.a.a(this.this$0.this$0.f, this.this$0.this$0.m);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
