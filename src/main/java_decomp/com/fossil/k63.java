package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k63 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ j03 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ d63 c;

    @DexIgnore
    public k63(d63 d63, j03 j03, String str) {
        this.c = d63;
        this.a = j03;
        this.b = str;
    }

    @DexIgnore
    public final void run() {
        this.c.a.t();
        this.c.a.a(this.a, this.b);
    }
}
