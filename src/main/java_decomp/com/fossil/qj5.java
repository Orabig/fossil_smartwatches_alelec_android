package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qj5 implements Factory<pj5> {
    @DexIgnore
    public static HomeProfilePresenter a(oj5 oj5, PortfolioApp portfolioApp, dt4 dt4, UpdateUser updateUser, DeviceRepository deviceRepository, UserRepository userRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        return new HomeProfilePresenter(oj5, portfolioApp, dt4, updateUser, deviceRepository, userRepository, summariesRepository, sleepSummariesRepository, deleteLogoutUserUseCase);
    }
}
