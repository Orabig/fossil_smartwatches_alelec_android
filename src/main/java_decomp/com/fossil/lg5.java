package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lg5 {
    @DexIgnore
    public /* final */ gg5 a;
    @DexIgnore
    public /* final */ vg5 b;
    @DexIgnore
    public /* final */ qg5 c;

    @DexIgnore
    public lg5(gg5 gg5, vg5 vg5, qg5 qg5) {
        wg6.b(gg5, "mSleepOverviewDayView");
        wg6.b(vg5, "mSleepOverviewWeekView");
        wg6.b(qg5, "mSleepOverviewMonthView");
        this.a = gg5;
        this.b = vg5;
        this.c = qg5;
    }

    @DexIgnore
    public final gg5 a() {
        return this.a;
    }

    @DexIgnore
    public final qg5 b() {
        return this.c;
    }

    @DexIgnore
    public final vg5 c() {
        return this.b;
    }
}
