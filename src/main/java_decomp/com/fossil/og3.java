package com.fossil;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class og3 extends q1 {
    @DexIgnore
    public og3(Context context) {
        super(context);
    }

    @DexIgnore
    public MenuItem a(int i, int i2, int i3, CharSequence charSequence) {
        if (size() + 1 <= 5) {
            s();
            MenuItem a = super.a(i, i2, i3, charSequence);
            if (a instanceof t1) {
                ((t1) a).c(true);
            }
            r();
            return a;
        }
        throw new IllegalArgumentException("Maximum number of items supported by BottomNavigationView is 5. Limit can be checked with BottomNavigationView#getMaxItemCount()");
    }

    @DexIgnore
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        throw new UnsupportedOperationException("BottomNavigationView does not support submenus");
    }
}
