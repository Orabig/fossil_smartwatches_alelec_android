package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.fossil.h16;
import com.misfit.frameworks.common.constants.Constants;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z06 {
    @DexIgnore
    public /* final */ b a; // = new b();
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ Downloader d;
    @DexIgnore
    public /* final */ Map<String, t06> e;
    @DexIgnore
    public /* final */ Map<Object, r06> f;
    @DexIgnore
    public /* final */ Map<Object, r06> g;
    @DexIgnore
    public /* final */ Set<Object> h;
    @DexIgnore
    public /* final */ Handler i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ u06 k;
    @DexIgnore
    public /* final */ p16 l;
    @DexIgnore
    public /* final */ List<t06> m;
    @DexIgnore
    public /* final */ c n;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public boolean p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Handler {
        @DexIgnore
        public /* final */ z06 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.z06$a$a")
        /* renamed from: com.fossil.z06$a$a  reason: collision with other inner class name */
        public class C0059a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Message a;

            @DexIgnore
            public C0059a(a aVar, Message message) {
                this.a = message;
            }

            @DexIgnore
            public void run() {
                throw new AssertionError("Unknown handler message received: " + this.a.what);
            }
        }

        @DexIgnore
        public a(Looper looper, z06 z06) {
            super(looper);
            this.a = z06;
        }

        @DexIgnore
        public void handleMessage(Message message) {
            boolean z = false;
            switch (message.what) {
                case 1:
                    this.a.e((r06) message.obj);
                    return;
                case 2:
                    this.a.d((r06) message.obj);
                    return;
                case 4:
                    this.a.f((t06) message.obj);
                    return;
                case 5:
                    this.a.g((t06) message.obj);
                    return;
                case 6:
                    this.a.a((t06) message.obj, false);
                    return;
                case 7:
                    this.a.b();
                    return;
                case 9:
                    this.a.b((NetworkInfo) message.obj);
                    return;
                case 10:
                    z06 z06 = this.a;
                    if (message.arg1 == 1) {
                        z = true;
                    }
                    z06.b(z);
                    return;
                case 11:
                    this.a.a(message.obj);
                    return;
                case 12:
                    this.a.b(message.obj);
                    return;
                default:
                    Picasso.p.post(new C0059a(this, message));
                    return;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends HandlerThread {
        @DexIgnore
        public b() {
            super("Picasso-Dispatcher", 10);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ z06 a;

        @DexIgnore
        public c(z06 z06) {
            this.a = z06;
        }

        @DexIgnore
        public void a() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
            if (this.a.o) {
                intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            }
            this.a.b.registerReceiver(this, intentFilter);
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                    if (intent.hasExtra(Constants.STATE)) {
                        this.a.a(intent.getBooleanExtra(Constants.STATE, false));
                    }
                } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                    this.a.a(((ConnectivityManager) t16.a(context, "connectivity")).getActiveNetworkInfo());
                }
            }
        }
    }

    @DexIgnore
    public z06(Context context, ExecutorService executorService, Handler handler, Downloader downloader, u06 u06, p16 p16) {
        this.a.start();
        t16.a(this.a.getLooper());
        this.b = context;
        this.c = executorService;
        this.e = new LinkedHashMap();
        this.f = new WeakHashMap();
        this.g = new WeakHashMap();
        this.h = new HashSet();
        this.i = new a(this.a.getLooper(), this);
        this.d = downloader;
        this.j = handler;
        this.k = u06;
        this.l = p16;
        this.m = new ArrayList(4);
        this.p = t16.d(this.b);
        this.o = t16.b(context, "android.permission.ACCESS_NETWORK_STATE");
        this.n = new c(this);
        this.n.a();
    }

    @DexIgnore
    public void a(r06 r06) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(2, r06));
    }

    @DexIgnore
    public void b(r06 r06) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(1, r06));
    }

    @DexIgnore
    public void c(t06 t06) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(6, t06));
    }

    @DexIgnore
    public void d(t06 t06) {
        Handler handler = this.i;
        handler.sendMessageDelayed(handler.obtainMessage(5, t06), 500);
    }

    @DexIgnore
    public void e(r06 r06) {
        a(r06, true);
    }

    @DexIgnore
    public void f(t06 t06) {
        if (f16.shouldWriteToMemoryCache(t06.i())) {
            this.k.a(t06.g(), t06.l());
        }
        this.e.remove(t06.g());
        a(t06);
        if (t06.j().n) {
            t16.a("Dispatcher", "batched", t16.a(t06), "for completion");
        }
    }

    @DexIgnore
    public void g(t06 t06) {
        if (!t06.n()) {
            boolean z = false;
            if (this.c.isShutdown()) {
                a(t06, false);
                return;
            }
            NetworkInfo networkInfo = null;
            if (this.o) {
                networkInfo = ((ConnectivityManager) t16.a(this.b, "connectivity")).getActiveNetworkInfo();
            }
            boolean z2 = networkInfo != null && networkInfo.isConnected();
            boolean a2 = t06.a(this.p, networkInfo);
            boolean o2 = t06.o();
            if (!a2) {
                if (this.o && o2) {
                    z = true;
                }
                a(t06, z);
                if (z) {
                    e(t06);
                }
            } else if (!this.o || z2) {
                if (t06.j().n) {
                    t16.a("Dispatcher", "retrying", t16.a(t06));
                }
                if (t06.f() instanceof h16.a) {
                    t06.i |= g16.NO_CACHE.index;
                }
                t06.r = this.c.submit(t06);
            } else {
                a(t06, o2);
                if (o2) {
                    e(t06);
                }
            }
        }
    }

    @DexIgnore
    public void a(NetworkInfo networkInfo) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(9, networkInfo));
    }

    @DexIgnore
    public void b(t06 t06) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(4, t06));
    }

    @DexIgnore
    public final void c(r06 r06) {
        Object j2 = r06.j();
        if (j2 != null) {
            r06.k = true;
            this.f.put(j2, r06);
        }
    }

    @DexIgnore
    public void d(r06 r06) {
        String c2 = r06.c();
        t06 t06 = this.e.get(c2);
        if (t06 != null) {
            t06.b(r06);
            if (t06.a()) {
                this.e.remove(c2);
                if (r06.f().n) {
                    t16.a("Dispatcher", "canceled", r06.h().d());
                }
            }
        }
        if (this.h.contains(r06.i())) {
            this.g.remove(r06.j());
            if (r06.f().n) {
                t16.a("Dispatcher", "canceled", r06.h().d(), "because paused request got canceled");
            }
        }
        r06 remove = this.f.remove(r06.j());
        if (remove != null && remove.f().n) {
            t16.a("Dispatcher", "canceled", remove.h().d(), "from replaying");
        }
    }

    @DexIgnore
    public final void e(t06 t06) {
        r06 c2 = t06.c();
        if (c2 != null) {
            c(c2);
        }
        List<r06> d2 = t06.d();
        if (d2 != null) {
            int size = d2.size();
            for (int i2 = 0; i2 < size; i2++) {
                c(d2.get(i2));
            }
        }
    }

    @DexIgnore
    public void a(boolean z) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(10, z ? 1 : 0, 0));
    }

    @DexIgnore
    public void b(Object obj) {
        if (this.h.remove(obj)) {
            ArrayList arrayList = null;
            Iterator<r06> it = this.g.values().iterator();
            while (it.hasNext()) {
                r06 next = it.next();
                if (next.i().equals(obj)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(next);
                    it.remove();
                }
            }
            if (arrayList != null) {
                Handler handler = this.j;
                handler.sendMessage(handler.obtainMessage(13, arrayList));
            }
        }
    }

    @DexIgnore
    public void a(r06 r06, boolean z) {
        if (this.h.contains(r06.i())) {
            this.g.put(r06.j(), r06);
            if (r06.f().n) {
                String d2 = r06.b.d();
                t16.a("Dispatcher", "paused", d2, "because tag '" + r06.i() + "' is paused");
                return;
            }
            return;
        }
        t06 t06 = this.e.get(r06.c());
        if (t06 != null) {
            t06.a(r06);
        } else if (!this.c.isShutdown()) {
            t06 a2 = t06.a(r06.f(), this, this.k, this.l, r06);
            a2.r = this.c.submit(a2);
            this.e.put(r06.c(), a2);
            if (z) {
                this.f.remove(r06.j());
            }
            if (r06.f().n) {
                t16.a("Dispatcher", "enqueued", r06.b.d());
            }
        } else if (r06.f().n) {
            t16.a("Dispatcher", "ignored", r06.b.d(), "because shut down");
        }
    }

    @DexIgnore
    public void b() {
        ArrayList arrayList = new ArrayList(this.m);
        this.m.clear();
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(8, arrayList));
        a((List<t06>) arrayList);
    }

    @DexIgnore
    public void b(boolean z) {
        this.p = z;
    }

    @DexIgnore
    public void b(NetworkInfo networkInfo) {
        ExecutorService executorService = this.c;
        if (executorService instanceof k16) {
            ((k16) executorService).a(networkInfo);
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            a();
        }
    }

    @DexIgnore
    public void a(Object obj) {
        if (this.h.add(obj)) {
            Iterator<t06> it = this.e.values().iterator();
            while (it.hasNext()) {
                t06 next = it.next();
                boolean z = next.j().n;
                r06 c2 = next.c();
                List<r06> d2 = next.d();
                boolean z2 = d2 != null && !d2.isEmpty();
                if (c2 != null || z2) {
                    if (c2 != null && c2.i().equals(obj)) {
                        next.b(c2);
                        this.g.put(c2.j(), c2);
                        if (z) {
                            t16.a("Dispatcher", "paused", c2.b.d(), "because tag '" + obj + "' was paused");
                        }
                    }
                    if (z2) {
                        for (int size = d2.size() - 1; size >= 0; size--) {
                            r06 r06 = d2.get(size);
                            if (r06.i().equals(obj)) {
                                next.b(r06);
                                this.g.put(r06.j(), r06);
                                if (z) {
                                    t16.a("Dispatcher", "paused", r06.b.d(), "because tag '" + obj + "' was paused");
                                }
                            }
                        }
                    }
                    if (next.a()) {
                        it.remove();
                        if (z) {
                            t16.a("Dispatcher", "canceled", t16.a(next), "all actions paused");
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(t06 t06, boolean z) {
        if (t06.j().n) {
            String a2 = t16.a(t06);
            StringBuilder sb = new StringBuilder();
            sb.append("for error");
            sb.append(z ? " (will replay)" : "");
            t16.a("Dispatcher", "batched", a2, sb.toString());
        }
        this.e.remove(t06.g());
        a(t06);
    }

    @DexIgnore
    public final void a() {
        if (!this.f.isEmpty()) {
            Iterator<r06> it = this.f.values().iterator();
            while (it.hasNext()) {
                r06 next = it.next();
                it.remove();
                if (next.f().n) {
                    t16.a("Dispatcher", "replaying", next.h().d());
                }
                a(next, false);
            }
        }
    }

    @DexIgnore
    public final void a(t06 t06) {
        if (!t06.n()) {
            this.m.add(t06);
            if (!this.i.hasMessages(7)) {
                this.i.sendEmptyMessageDelayed(7, 200);
            }
        }
    }

    @DexIgnore
    public final void a(List<t06> list) {
        if (list != null && !list.isEmpty() && list.get(0).j().n) {
            StringBuilder sb = new StringBuilder();
            for (t06 next : list) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(t16.a(next));
            }
            t16.a("Dispatcher", "delivered", sb.toString());
        }
    }
}
