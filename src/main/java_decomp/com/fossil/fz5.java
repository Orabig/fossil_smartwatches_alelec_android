package com.fossil;

import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum fz5 {
    Left,
    Right,
    Top,
    Bottom;
    
    @DexIgnore
    public static /* final */ List<fz5> FREEDOM; // = null;
    @DexIgnore
    public static /* final */ List<fz5> FREEDOM_NO_BOTTOM; // = null;
    @DexIgnore
    public static /* final */ List<fz5> HORIZONTAL; // = null;
    @DexIgnore
    public static /* final */ List<fz5> VERTICAL; // = null;

    /*
    static {
        FREEDOM = Arrays.asList(values());
        FREEDOM_NO_BOTTOM = Arrays.asList(new fz5[]{Top, Left, Right});
        HORIZONTAL = Arrays.asList(new fz5[]{Left, Right});
        VERTICAL = Arrays.asList(new fz5[]{Top, Bottom});
    }
    */

    @DexIgnore
    public static List<fz5> from(int i) {
        if (i == 0) {
            return FREEDOM;
        }
        if (i == 1) {
            return FREEDOM_NO_BOTTOM;
        }
        if (i == 2) {
            return HORIZONTAL;
        }
        if (i != 3) {
            return FREEDOM;
        }
        return VERTICAL;
    }
}
