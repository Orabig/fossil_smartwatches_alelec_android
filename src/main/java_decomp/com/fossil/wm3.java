package com.fossil;

import com.fossil.wm3.i;
import com.fossil.wm3.n;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wm3<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    @DexIgnore
    public static /* final */ long CLEANUP_EXECUTOR_DELAY_SECS; // = 60;
    @DexIgnore
    public static /* final */ int CONTAINS_VALUE_RETRIES; // = 3;
    @DexIgnore
    public static /* final */ int DRAIN_MAX; // = 16;
    @DexIgnore
    public static /* final */ int DRAIN_THRESHOLD; // = 63;
    @DexIgnore
    public static /* final */ int MAXIMUM_CAPACITY; // = 1073741824;
    @DexIgnore
    public static /* final */ int MAX_SEGMENTS; // = 65536;
    @DexIgnore
    public static /* final */ c0<Object, Object, e> UNSET_WEAK_VALUE_REFERENCE; // = new a();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 5;
    @DexIgnore
    public /* final */ int concurrencyLevel;
    @DexIgnore
    public /* final */ transient j<K, V, E, S> entryHelper;
    @DexIgnore
    public transient Set<Map.Entry<K, V>> entrySet;
    @DexIgnore
    public /* final */ ak3<Object> keyEquivalence;
    @DexIgnore
    public transient Set<K> keySet;
    @DexIgnore
    public /* final */ transient int segmentMask;
    @DexIgnore
    public /* final */ transient int segmentShift;
    @DexIgnore
    public /* final */ transient n<K, V, E, S>[] segments;
    @DexIgnore
    public transient Collection<V> values;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements c0<Object, Object, e> {
        @DexIgnore
        public c0<Object, Object, e> a(ReferenceQueue<Object> referenceQueue, e eVar) {
            return this;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ c0 a(ReferenceQueue referenceQueue, i iVar) {
            a((ReferenceQueue<Object>) referenceQueue, (e) iVar);
            return this;
        }

        @DexIgnore
        public e a() {
            return null;
        }

        @DexIgnore
        public void clear() {
        }

        @DexIgnore
        public Object get() {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a0<K, V> extends n<K, V, z<K, V>, a0<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<K> queueForKeys; // = new ReferenceQueue<>();
        @DexIgnore
        public /* final */ ReferenceQueue<V> queueForValues; // = new ReferenceQueue<>();

        @DexIgnore
        public a0(wm3<K, V, z<K, V>, a0<K, V>> wm3, int i, int i2) {
            super(wm3, i, i2);
        }

        @DexIgnore
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }

        @DexIgnore
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }

        @DexIgnore
        public c0<K, V, z<K, V>> getWeakValueReferenceForTesting(i<K, V, ?> iVar) {
            return castForTesting((i) iVar).c();
        }

        @DexIgnore
        public void maybeClearReferenceQueues() {
            clearReferenceQueue(this.queueForKeys);
        }

        @DexIgnore
        public void maybeDrainReferenceQueues() {
            drainKeyReferenceQueue(this.queueForKeys);
            drainValueReferenceQueue(this.queueForValues);
        }

        @DexIgnore
        public c0<K, V, z<K, V>> newWeakValueReferenceForTesting(i<K, V, ?> iVar, V v) {
            return new d0(this.queueForValues, v, castForTesting((i) iVar));
        }

        @DexIgnore
        public a0<K, V> self() {
            return this;
        }

        @DexIgnore
        public void setWeakValueReferenceForTesting(i<K, V, ?> iVar, c0<K, V, ? extends i<K, V, ?>> c0Var) {
            z castForTesting = castForTesting((i) iVar);
            c0 a = castForTesting.c;
            c0 unused = castForTesting.c = c0Var;
            a.clear();
        }

        @DexIgnore
        public z<K, V> castForTesting(i<K, V, ?> iVar) {
            return (z) iVar;
        }
    }

    @DexIgnore
    public interface b0<K, V, E extends i<K, V, E>> extends i<K, V, E> {
        @DexIgnore
        c0<K, V, E> c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<K, V, E extends i<K, V, E>> implements i<K, V, E> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ E c;

        @DexIgnore
        public c(K k, int i, E e) {
            this.a = k;
            this.b = i;
            this.c = e;
        }

        @DexIgnore
        public E a() {
            return this.c;
        }

        @DexIgnore
        public int b() {
            return this.b;
        }

        @DexIgnore
        public K getKey() {
            return this.a;
        }
    }

    @DexIgnore
    public interface c0<K, V, E extends i<K, V, E>> {
        @DexIgnore
        c0<K, V, E> a(ReferenceQueue<V> referenceQueue, E e);

        @DexIgnore
        E a();

        @DexIgnore
        void clear();

        @DexIgnore
        V get();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<K, V, E extends i<K, V, E>> extends WeakReference<K> implements i<K, V, E> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ E b;

        @DexIgnore
        public d(ReferenceQueue<K> referenceQueue, K k, int i, E e) {
            super(k, referenceQueue);
            this.a = i;
            this.b = e;
        }

        @DexIgnore
        public E a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.a;
        }

        @DexIgnore
        public K getKey() {
            return get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d0<K, V, E extends i<K, V, E>> extends WeakReference<V> implements c0<K, V, E> {
        @DexIgnore
        public /* final */ E a;

        @DexIgnore
        public d0(ReferenceQueue<V> referenceQueue, V v, E e) {
            super(v, referenceQueue);
            this.a = e;
        }

        @DexIgnore
        public E a() {
            return this.a;
        }

        @DexIgnore
        public c0<K, V, E> a(ReferenceQueue<V> referenceQueue, E e) {
            return new d0(referenceQueue, get(), e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements i<Object, Object, e> {
        @DexIgnore
        public e() {
            throw new AssertionError();
        }

        @DexIgnore
        public int b() {
            throw new AssertionError();
        }

        @DexIgnore
        public Object getKey() {
            throw new AssertionError();
        }

        @DexIgnore
        public Object getValue() {
            throw new AssertionError();
        }

        @DexIgnore
        public e a() {
            throw new AssertionError();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e0 extends uk3<K, V> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public V b;

        @DexIgnore
        public e0(K k, V v) {
            this.a = k;
            this.b = v;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            if (!this.a.equals(entry.getKey()) || !this.b.equals(entry.getValue())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public K getKey() {
            return this.a;
        }

        @DexIgnore
        public V getValue() {
            return this.b;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode() ^ this.b.hashCode();
        }

        @DexIgnore
        public V setValue(V v) {
            V put = wm3.this.put(this.a, v);
            this.b = v;
            return put;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends wm3<K, V, E, S>.h<Map.Entry<K, V>> {
        @DexIgnore
        public f(wm3 wm3) {
            super();
        }

        @DexIgnore
        public Map.Entry<K, V> next() {
            return b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends m<Map.Entry<K, V>> {
        @DexIgnore
        public g() {
            super((a) null);
        }

        @DexIgnore
        public void clear() {
            wm3.this.clear();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
            r4 = (java.util.Map.Entry) r4;
         */
        @DexIgnore
        public boolean contains(Object obj) {
            Map.Entry entry;
            Object key;
            Object obj2;
            if ((obj instanceof Map.Entry) && (key = entry.getKey()) != null && (obj2 = wm3.this.get(key)) != null && wm3.this.valueEquivalence().equivalent(entry.getValue(), obj2)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public boolean isEmpty() {
            return wm3.this.isEmpty();
        }

        @DexIgnore
        public Iterator<Map.Entry<K, V>> iterator() {
            return new f(wm3.this);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x0006, code lost:
            r4 = (java.util.Map.Entry) r4;
         */
        @DexIgnore
        public boolean remove(Object obj) {
            Map.Entry entry;
            Object key;
            if ((obj instanceof Map.Entry) && (key = entry.getKey()) != null && wm3.this.remove(key, entry.getValue())) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int size() {
            return wm3.this.size();
        }
    }

    @DexIgnore
    public interface i<K, V, E extends i<K, V, E>> {
        @DexIgnore
        E a();

        @DexIgnore
        int b();

        @DexIgnore
        K getKey();

        @DexIgnore
        V getValue();
    }

    @DexIgnore
    public interface j<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> {
        @DexIgnore
        E a(S s, E e, E e2);

        @DexIgnore
        E a(S s, K k, int i, E e);

        @DexIgnore
        S a(wm3<K, V, E, S> wm3, int i, int i2);

        @DexIgnore
        p a();

        @DexIgnore
        void a(S s, E e, V v);

        @DexIgnore
        p b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k extends wm3<K, V, E, S>.h<K> {
        @DexIgnore
        public k(wm3 wm3) {
            super();
        }

        @DexIgnore
        public K next() {
            return b().getKey();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l extends m<K> {
        @DexIgnore
        public l() {
            super((a) null);
        }

        @DexIgnore
        public void clear() {
            wm3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return wm3.this.containsKey(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return wm3.this.isEmpty();
        }

        @DexIgnore
        public Iterator<K> iterator() {
            return new k(wm3.this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return wm3.this.remove(obj) != null;
        }

        @DexIgnore
        public int size() {
            return wm3.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class m<E> extends AbstractSet<E> {
        @DexIgnore
        public m() {
        }

        @DexIgnore
        public Object[] toArray() {
            return wm3.a(this).toArray();
        }

        @DexIgnore
        public /* synthetic */ m(a aVar) {
            this();
        }

        @DexIgnore
        public <E> E[] toArray(E[] eArr) {
            return wm3.a(this).toArray(eArr);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<K, V> extends b<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 3;

        @DexIgnore
        public o(p pVar, p pVar2, ak3<Object> ak3, ak3<Object> ak32, int i, ConcurrentMap<K, V> concurrentMap) {
            super(pVar, pVar2, ak3, ak32, i, concurrentMap);
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.delegate = readMapMaker(objectInputStream).f();
            readEntries(objectInputStream);
        }

        @DexIgnore
        private Object readResolve() {
            return this.delegate;
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            writeMapTo(objectOutputStream);
        }
    }

    @DexIgnore
    public enum p {
        STRONG {
            @DexIgnore
            public ak3<Object> defaultEquivalence() {
                return ak3.equals();
            }
        },
        WEAK {
            @DexIgnore
            public ak3<Object> defaultEquivalence() {
                return ak3.identity();
            }
        };

        @DexIgnore
        public abstract ak3<Object> defaultEquivalence();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q<K, V> extends c<K, V, q<K, V>> implements u<K, V, q<K, V>> {
        @DexIgnore
        public volatile V d; // = null;

        @DexIgnore
        public q(K k, int i, q<K, V> qVar) {
            super(k, i, qVar);
        }

        @DexIgnore
        public void a(V v) {
            this.d = v;
        }

        @DexIgnore
        public V getValue() {
            return this.d;
        }

        @DexIgnore
        public q<K, V> a(q<K, V> qVar) {
            q<K, V> qVar2 = new q<>(this.a, this.b, qVar);
            qVar2.d = this.d;
            return qVar2;
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, q<K, V>, r<K, V>> {
            @DexIgnore
            public static /* final */ a<?, ?> a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> c() {
                return a;
            }

            @DexIgnore
            public p b() {
                return p.STRONG;
            }

            @DexIgnore
            public p a() {
                return p.STRONG;
            }

            @DexIgnore
            public r<K, V> a(wm3<K, V, q<K, V>, r<K, V>> wm3, int i, int i2) {
                return new r<>(wm3, i, i2);
            }

            @DexIgnore
            public q<K, V> a(r<K, V> rVar, q<K, V> qVar, q<K, V> qVar2) {
                return qVar.a(qVar2);
            }

            @DexIgnore
            public void a(r<K, V> rVar, q<K, V> qVar, V v) {
                qVar.a(v);
            }

            @DexIgnore
            public q<K, V> a(r<K, V> rVar, K k, int i, q<K, V> qVar) {
                return new q<>(k, i, qVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r<K, V> extends n<K, V, q<K, V>, r<K, V>> {
        @DexIgnore
        public r(wm3<K, V, q<K, V>, r<K, V>> wm3, int i, int i2) {
            super(wm3, i, i2);
        }

        @DexIgnore
        public r<K, V> self() {
            return this;
        }

        @DexIgnore
        public q<K, V> castForTesting(i<K, V, ?> iVar) {
            return (q) iVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t<K, V> extends n<K, V, s<K, V>, t<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<V> queueForValues; // = new ReferenceQueue<>();

        @DexIgnore
        public t(wm3<K, V, s<K, V>, t<K, V>> wm3, int i, int i2) {
            super(wm3, i, i2);
        }

        @DexIgnore
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }

        @DexIgnore
        public c0<K, V, s<K, V>> getWeakValueReferenceForTesting(i<K, V, ?> iVar) {
            return castForTesting((i) iVar).c();
        }

        @DexIgnore
        public void maybeClearReferenceQueues() {
            clearReferenceQueue(this.queueForValues);
        }

        @DexIgnore
        public void maybeDrainReferenceQueues() {
            drainValueReferenceQueue(this.queueForValues);
        }

        @DexIgnore
        public c0<K, V, s<K, V>> newWeakValueReferenceForTesting(i<K, V, ?> iVar, V v) {
            return new d0(this.queueForValues, v, castForTesting((i) iVar));
        }

        @DexIgnore
        public t<K, V> self() {
            return this;
        }

        @DexIgnore
        public void setWeakValueReferenceForTesting(i<K, V, ?> iVar, c0<K, V, ? extends i<K, V, ?>> c0Var) {
            s castForTesting = castForTesting((i) iVar);
            c0 a = castForTesting.d;
            c0 unused = castForTesting.d = c0Var;
            a.clear();
        }

        @DexIgnore
        public s<K, V> castForTesting(i<K, V, ?> iVar) {
            return (s) iVar;
        }
    }

    @DexIgnore
    public interface u<K, V, E extends i<K, V, E>> extends i<K, V, E> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v extends wm3<K, V, E, S>.h<V> {
        @DexIgnore
        public v(wm3 wm3) {
            super();
        }

        @DexIgnore
        public V next() {
            return b().getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w extends AbstractCollection<V> {
        @DexIgnore
        public w() {
        }

        @DexIgnore
        public void clear() {
            wm3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return wm3.this.containsValue(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return wm3.this.isEmpty();
        }

        @DexIgnore
        public Iterator<V> iterator() {
            return new v(wm3.this);
        }

        @DexIgnore
        public int size() {
            return wm3.this.size();
        }

        @DexIgnore
        public Object[] toArray() {
            return wm3.a(this).toArray();
        }

        @DexIgnore
        public <E> E[] toArray(E[] eArr) {
            return wm3.a(this).toArray(eArr);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class x<K, V> extends d<K, V, x<K, V>> implements u<K, V, x<K, V>> {
        @DexIgnore
        public volatile V c; // = null;

        @DexIgnore
        public x(ReferenceQueue<K> referenceQueue, K k, int i, x<K, V> xVar) {
            super(referenceQueue, k, i, xVar);
        }

        @DexIgnore
        public void a(V v) {
            this.c = v;
        }

        @DexIgnore
        public V getValue() {
            return this.c;
        }

        @DexIgnore
        public x<K, V> a(ReferenceQueue<K> referenceQueue, x<K, V> xVar) {
            x<K, V> xVar2 = new x<>(referenceQueue, getKey(), this.a, xVar);
            xVar2.a(this.c);
            return xVar2;
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, x<K, V>, y<K, V>> {
            @DexIgnore
            public static /* final */ a<?, ?> a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> c() {
                return a;
            }

            @DexIgnore
            public p b() {
                return p.STRONG;
            }

            @DexIgnore
            public p a() {
                return p.WEAK;
            }

            @DexIgnore
            public y<K, V> a(wm3<K, V, x<K, V>, y<K, V>> wm3, int i, int i2) {
                return new y<>(wm3, i, i2);
            }

            @DexIgnore
            public x<K, V> a(y<K, V> yVar, x<K, V> xVar, x<K, V> xVar2) {
                if (xVar.getKey() == null) {
                    return null;
                }
                return xVar.a(yVar.queueForKeys, xVar2);
            }

            @DexIgnore
            public void a(y<K, V> yVar, x<K, V> xVar, V v) {
                xVar.a(v);
            }

            @DexIgnore
            public x<K, V> a(y<K, V> yVar, K k, int i, x<K, V> xVar) {
                return new x<>(yVar.queueForKeys, k, i, xVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class y<K, V> extends n<K, V, x<K, V>, y<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<K> queueForKeys; // = new ReferenceQueue<>();

        @DexIgnore
        public y(wm3<K, V, x<K, V>, y<K, V>> wm3, int i, int i2) {
            super(wm3, i, i2);
        }

        @DexIgnore
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }

        @DexIgnore
        public void maybeClearReferenceQueues() {
            clearReferenceQueue(this.queueForKeys);
        }

        @DexIgnore
        public void maybeDrainReferenceQueues() {
            drainKeyReferenceQueue(this.queueForKeys);
        }

        @DexIgnore
        public y<K, V> self() {
            return this;
        }

        @DexIgnore
        public x<K, V> castForTesting(i<K, V, ?> iVar) {
            return (x) iVar;
        }
    }

    @DexIgnore
    public wm3(vm3 vm3, j<K, V, E, S> jVar) {
        this.concurrencyLevel = Math.min(vm3.a(), 65536);
        this.keyEquivalence = vm3.c();
        this.entryHelper = jVar;
        int min = Math.min(vm3.b(), 1073741824);
        int i2 = 0;
        int i3 = 1;
        int i4 = 1;
        int i5 = 0;
        while (i4 < this.concurrencyLevel) {
            i5++;
            i4 <<= 1;
        }
        this.segmentShift = 32 - i5;
        this.segmentMask = i4 - 1;
        this.segments = newSegmentArray(i4);
        int i6 = min / i4;
        while (i3 < (i4 * i6 < min ? i6 + 1 : i6)) {
            i3 <<= 1;
        }
        while (true) {
            n<K, V, E, S>[] nVarArr = this.segments;
            if (i2 < nVarArr.length) {
                nVarArr[i2] = createSegment(i3, -1);
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public static <E> ArrayList<E> a(Collection<E> collection) {
        ArrayList<E> arrayList = new ArrayList<>(collection.size());
        qm3.a(arrayList, collection.iterator());
        return arrayList;
    }

    @DexIgnore
    public static <K, V> wm3<K, V, ? extends i<K, V, ?>, ?> create(vm3 vm3) {
        if (vm3.d() == p.STRONG && vm3.e() == p.STRONG) {
            return new wm3<>(vm3, q.a.c());
        }
        if (vm3.d() == p.STRONG && vm3.e() == p.WEAK) {
            return new wm3<>(vm3, s.a.c());
        }
        if (vm3.d() == p.WEAK && vm3.e() == p.STRONG) {
            return new wm3<>(vm3, x.a.c());
        }
        if (vm3.d() == p.WEAK && vm3.e() == p.WEAK) {
            return new wm3<>(vm3, z.a.c());
        }
        throw new AssertionError();
    }

    @DexIgnore
    public static int rehash(int i2) {
        int i3 = i2 + ((i2 << 15) ^ -12931);
        int i4 = i3 ^ (i3 >>> 10);
        int i5 = i4 + (i4 << 3);
        int i6 = i5 ^ (i5 >>> 6);
        int i7 = i6 + (i6 << 2) + (i6 << 14);
        return i7 ^ (i7 >>> 16);
    }

    @DexIgnore
    public static <K, V, E extends i<K, V, E>> c0<K, V, E> unsetWeakValueReference() {
        return UNSET_WEAK_VALUE_REFERENCE;
    }

    @DexIgnore
    public void clear() {
        for (n<K, V, E, S> clear : this.segments) {
            clear.clear();
        }
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        if (obj == null) {
            return false;
        }
        int hash = hash(obj);
        return segmentFor(hash).containsKey(obj, hash);
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        Object obj2 = obj;
        if (obj2 == null) {
            return false;
        }
        n<K, V, E, S>[] nVarArr = this.segments;
        long j2 = -1;
        int i2 = 0;
        while (i2 < 3) {
            long j3 = 0;
            for (n<K, V, E, S> nVar : nVarArr) {
                int i3 = nVar.count;
                AtomicReferenceArray<E> atomicReferenceArray = nVar.table;
                for (int i4 = 0; i4 < atomicReferenceArray.length(); i4++) {
                    for (i iVar = (i) atomicReferenceArray.get(i4); iVar != null; iVar = iVar.a()) {
                        V liveValue = nVar.getLiveValue(iVar);
                        if (liveValue != null && valueEquivalence().equivalent(obj2, liveValue)) {
                            return true;
                        }
                    }
                }
                j3 += (long) nVar.modCount;
            }
            if (j3 == j2) {
                return false;
            }
            i2++;
            j2 = j3;
        }
        return false;
    }

    @DexIgnore
    public E copyEntry(E e2, E e3) {
        return segmentFor(e2.b()).copyEntry(e2, e3);
    }

    @DexIgnore
    public n<K, V, E, S> createSegment(int i2, int i3) {
        return this.entryHelper.a(this, i2, i3);
    }

    @DexIgnore
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.entrySet;
        if (set != null) {
            return set;
        }
        g gVar = new g();
        this.entrySet = gVar;
        return gVar;
    }

    @DexIgnore
    public V get(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).get(obj, hash);
    }

    @DexIgnore
    public E getEntry(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).getEntry(obj, hash);
    }

    @DexIgnore
    public V getLiveValue(E e2) {
        V value;
        if (e2.getKey() == null || (value = e2.getValue()) == null) {
            return null;
        }
        return value;
    }

    @DexIgnore
    public int hash(Object obj) {
        return rehash(this.keyEquivalence.hash(obj));
    }

    @DexIgnore
    public boolean isEmpty() {
        n<K, V, E, S>[] nVarArr = this.segments;
        long j2 = 0;
        for (int i2 = 0; i2 < nVarArr.length; i2++) {
            if (nVarArr[i2].count != 0) {
                return false;
            }
            j2 += (long) nVarArr[i2].modCount;
        }
        if (j2 == 0) {
            return true;
        }
        for (int i3 = 0; i3 < nVarArr.length; i3++) {
            if (nVarArr[i3].count != 0) {
                return false;
            }
            j2 -= (long) nVarArr[i3].modCount;
        }
        if (j2 != 0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean isLiveForTesting(i<K, V, ?> iVar) {
        return segmentFor(iVar.b()).getLiveValueForTesting(iVar) != null;
    }

    @DexIgnore
    public Set<K> keySet() {
        Set<K> set = this.keySet;
        if (set != null) {
            return set;
        }
        l lVar = new l();
        this.keySet = lVar;
        return lVar;
    }

    @DexIgnore
    public p keyStrength() {
        return this.entryHelper.a();
    }

    @DexIgnore
    public final n<K, V, E, S>[] newSegmentArray(int i2) {
        return new n[i2];
    }

    @DexIgnore
    public V put(K k2, V v2) {
        jk3.a(k2);
        jk3.a(v2);
        int hash = hash(k2);
        return segmentFor(hash).put(k2, hash, v2, false);
    }

    @DexIgnore
    public void putAll(Map<? extends K, ? extends V> map) {
        for (Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    @DexIgnore
    public V putIfAbsent(K k2, V v2) {
        jk3.a(k2);
        jk3.a(v2);
        int hash = hash(k2);
        return segmentFor(hash).put(k2, hash, v2, true);
    }

    @DexIgnore
    public void reclaimKey(E e2) {
        int b2 = e2.b();
        segmentFor(b2).reclaimKey(e2, b2);
    }

    @DexIgnore
    public void reclaimValue(c0<K, V, E> c0Var) {
        E a2 = c0Var.a();
        int b2 = a2.b();
        segmentFor(b2).reclaimValue(a2.getKey(), b2, c0Var);
    }

    @DexIgnore
    public V remove(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).remove(obj, hash);
    }

    @DexIgnore
    public boolean replace(K k2, V v2, V v3) {
        jk3.a(k2);
        jk3.a(v3);
        if (v2 == null) {
            return false;
        }
        int hash = hash(k2);
        return segmentFor(hash).replace(k2, hash, v2, v3);
    }

    @DexIgnore
    public n<K, V, E, S> segmentFor(int i2) {
        return this.segments[(i2 >>> this.segmentShift) & this.segmentMask];
    }

    @DexIgnore
    public int size() {
        n<K, V, E, S>[] nVarArr = this.segments;
        long j2 = 0;
        for (n<K, V, E, S> nVar : nVarArr) {
            j2 += (long) nVar.count;
        }
        return xo3.a(j2);
    }

    @DexIgnore
    public ak3<Object> valueEquivalence() {
        return this.entryHelper.b().defaultEquivalence();
    }

    @DexIgnore
    public p valueStrength() {
        return this.entryHelper.b();
    }

    @DexIgnore
    public Collection<V> values() {
        Collection<V> collection = this.values;
        if (collection != null) {
            return collection;
        }
        w wVar = new w();
        this.values = wVar;
        return wVar;
    }

    @DexIgnore
    public Object writeReplace() {
        return new o(this.entryHelper.a(), this.entryHelper.b(), this.keyEquivalence, this.entryHelper.b().defaultEquivalence(), this.concurrencyLevel, this);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<K, V> extends nl3<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 3;
        @DexIgnore
        public /* final */ int concurrencyLevel;
        @DexIgnore
        public transient ConcurrentMap<K, V> delegate;
        @DexIgnore
        public /* final */ ak3<Object> keyEquivalence;
        @DexIgnore
        public /* final */ p keyStrength;
        @DexIgnore
        public /* final */ ak3<Object> valueEquivalence;
        @DexIgnore
        public /* final */ p valueStrength;

        @DexIgnore
        public b(p pVar, p pVar2, ak3<Object> ak3, ak3<Object> ak32, int i, ConcurrentMap<K, V> concurrentMap) {
            this.keyStrength = pVar;
            this.valueStrength = pVar2;
            this.keyEquivalence = ak3;
            this.valueEquivalence = ak32;
            this.concurrencyLevel = i;
            this.delegate = concurrentMap;
        }

        @DexIgnore
        public void readEntries(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            while (true) {
                Object readObject = objectInputStream.readObject();
                if (readObject != null) {
                    this.delegate.put(readObject, objectInputStream.readObject());
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        public vm3 readMapMaker(ObjectInputStream objectInputStream) throws IOException {
            int readInt = objectInputStream.readInt();
            vm3 vm3 = new vm3();
            vm3.b(readInt);
            vm3.a(this.keyStrength);
            vm3.b(this.valueStrength);
            vm3.a(this.keyEquivalence);
            vm3.a(this.concurrencyLevel);
            return vm3;
        }

        @DexIgnore
        public void writeMapTo(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeInt(this.delegate.size());
            for (Map.Entry entry : this.delegate.entrySet()) {
                objectOutputStream.writeObject(entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.writeObject((Object) null);
        }

        @DexIgnore
        public ConcurrentMap<K, V> delegate() {
            return this.delegate;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s<K, V> extends c<K, V, s<K, V>> implements b0<K, V, s<K, V>> {
        @DexIgnore
        public volatile c0<K, V, s<K, V>> d; // = wm3.unsetWeakValueReference();

        @DexIgnore
        public s(K k, int i, s<K, V> sVar) {
            super(k, i, sVar);
        }

        @DexIgnore
        public c0<K, V, s<K, V>> c() {
            return this.d;
        }

        @DexIgnore
        public V getValue() {
            return this.d.get();
        }

        @DexIgnore
        public void a(V v, ReferenceQueue<V> referenceQueue) {
            c0<K, V, s<K, V>> c0Var = this.d;
            this.d = new d0(referenceQueue, v, this);
            c0Var.clear();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, s<K, V>, t<K, V>> {
            @DexIgnore
            public static /* final */ a<?, ?> a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> c() {
                return a;
            }

            @DexIgnore
            public p b() {
                return p.WEAK;
            }

            @DexIgnore
            public p a() {
                return p.STRONG;
            }

            @DexIgnore
            public t<K, V> a(wm3<K, V, s<K, V>, t<K, V>> wm3, int i, int i2) {
                return new t<>(wm3, i, i2);
            }

            @DexIgnore
            public s<K, V> a(t<K, V> tVar, s<K, V> sVar, s<K, V> sVar2) {
                if (n.isCollected(sVar)) {
                    return null;
                }
                return sVar.a((ReferenceQueue<V>) tVar.queueForValues, sVar2);
            }

            @DexIgnore
            public void a(t<K, V> tVar, s<K, V> sVar, V v) {
                sVar.a(v, (ReferenceQueue<V>) tVar.queueForValues);
            }

            @DexIgnore
            public s<K, V> a(t<K, V> tVar, K k, int i, s<K, V> sVar) {
                return new s<>(k, i, sVar);
            }
        }

        @DexIgnore
        public s<K, V> a(ReferenceQueue<V> referenceQueue, s<K, V> sVar) {
            s<K, V> sVar2 = new s<>(this.a, this.b, sVar);
            sVar2.d = this.d.a(referenceQueue, sVar2);
            return sVar2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class z<K, V> extends d<K, V, z<K, V>> implements b0<K, V, z<K, V>> {
        @DexIgnore
        public volatile c0<K, V, z<K, V>> c; // = wm3.unsetWeakValueReference();

        @DexIgnore
        public z(ReferenceQueue<K> referenceQueue, K k, int i, z<K, V> zVar) {
            super(referenceQueue, k, i, zVar);
        }

        @DexIgnore
        public c0<K, V, z<K, V>> c() {
            return this.c;
        }

        @DexIgnore
        public V getValue() {
            return this.c.get();
        }

        @DexIgnore
        public z<K, V> a(ReferenceQueue<K> referenceQueue, ReferenceQueue<V> referenceQueue2, z<K, V> zVar) {
            z<K, V> zVar2 = new z<>(referenceQueue, getKey(), this.a, zVar);
            zVar2.c = this.c.a(referenceQueue2, zVar2);
            return zVar2;
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<K, V> implements j<K, V, z<K, V>, a0<K, V>> {
            @DexIgnore
            public static /* final */ a<?, ?> a; // = new a<>();

            @DexIgnore
            public static <K, V> a<K, V> c() {
                return a;
            }

            @DexIgnore
            public p b() {
                return p.WEAK;
            }

            @DexIgnore
            public p a() {
                return p.WEAK;
            }

            @DexIgnore
            public a0<K, V> a(wm3<K, V, z<K, V>, a0<K, V>> wm3, int i, int i2) {
                return new a0<>(wm3, i, i2);
            }

            @DexIgnore
            public z<K, V> a(a0<K, V> a0Var, z<K, V> zVar, z<K, V> zVar2) {
                if (zVar.getKey() != null && !n.isCollected(zVar)) {
                    return zVar.a(a0Var.queueForKeys, a0Var.queueForValues, zVar2);
                }
                return null;
            }

            @DexIgnore
            public void a(a0<K, V> a0Var, z<K, V> zVar, V v) {
                zVar.a(v, (ReferenceQueue<V>) a0Var.queueForValues);
            }

            @DexIgnore
            public z<K, V> a(a0<K, V> a0Var, K k, int i, z<K, V> zVar) {
                return new z<>(a0Var.queueForKeys, k, i, zVar);
            }
        }

        @DexIgnore
        public void a(V v, ReferenceQueue<V> referenceQueue) {
            c0<K, V, z<K, V>> c0Var = this.c;
            this.c = new d0(referenceQueue, v, this);
            c0Var.clear();
        }
    }

    @DexIgnore
    public boolean remove(Object obj, Object obj2) {
        if (obj == null || obj2 == null) {
            return false;
        }
        int hash = hash(obj);
        return segmentFor(hash).remove(obj, hash, obj2);
    }

    @DexIgnore
    public V replace(K k2, V v2) {
        jk3.a(k2);
        jk3.a(v2);
        int hash = hash(k2);
        return segmentFor(hash).replace(k2, hash, v2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class h<T> implements Iterator<T> {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public n<K, V, E, S> c;
        @DexIgnore
        public AtomicReferenceArray<E> d;
        @DexIgnore
        public E e;
        @DexIgnore
        public wm3<K, V, E, S>.e0 f;
        @DexIgnore
        public wm3<K, V, E, S>.e0 g;

        @DexIgnore
        public h() {
            this.a = wm3.this.segments.length - 1;
            a();
        }

        @DexIgnore
        public final void a() {
            this.f = null;
            if (!c() && !d()) {
                while (true) {
                    int i = this.a;
                    if (i >= 0) {
                        n<K, V, E, S>[] nVarArr = wm3.this.segments;
                        this.a = i - 1;
                        this.c = nVarArr[i];
                        if (this.c.count != 0) {
                            this.d = this.c.table;
                            this.b = this.d.length() - 1;
                            if (d()) {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        @DexIgnore
        public wm3<K, V, E, S>.e0 b() {
            wm3<K, V, E, S>.e0 e0Var = this.f;
            if (e0Var != null) {
                this.g = e0Var;
                a();
                return this.g;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public boolean c() {
            E e2 = this.e;
            if (e2 == null) {
                return false;
            }
            while (true) {
                this.e = e2.a();
                E e3 = this.e;
                if (e3 == null) {
                    return false;
                }
                if (a(e3)) {
                    return true;
                }
                e2 = this.e;
            }
        }

        @DexIgnore
        public boolean d() {
            while (true) {
                int i = this.b;
                if (i < 0) {
                    return false;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.d;
                this.b = i - 1;
                E e2 = (i) atomicReferenceArray.get(i);
                this.e = e2;
                if (e2 != null && (a(this.e) || c())) {
                    return true;
                }
            }
        }

        @DexIgnore
        public boolean hasNext() {
            return this.f != null;
        }

        @DexIgnore
        public void remove() {
            bl3.a(this.g != null);
            wm3.this.remove(this.g.getKey());
            this.g = null;
        }

        @DexIgnore
        public boolean a(E e2) {
            boolean z;
            try {
                Object key = e2.getKey();
                Object liveValue = wm3.this.getLiveValue(e2);
                if (liveValue != null) {
                    this.f = new e0(key, liveValue);
                    z = true;
                } else {
                    z = false;
                }
                return z;
            } finally {
                this.c.postReadCleanup();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class n<K, V, E extends i<K, V, E>, S extends n<K, V, E, S>> extends ReentrantLock {
        @DexIgnore
        public volatile int count;
        @DexIgnore
        public /* final */ wm3<K, V, E, S> map;
        @DexIgnore
        public /* final */ int maxSegmentSize;
        @DexIgnore
        public int modCount;
        @DexIgnore
        public /* final */ AtomicInteger readCount; // = new AtomicInteger();
        @DexIgnore
        public volatile AtomicReferenceArray<E> table;
        @DexIgnore
        public int threshold;

        @DexIgnore
        public n(wm3<K, V, E, S> wm3, int i, int i2) {
            this.map = wm3;
            this.maxSegmentSize = i2;
            initTable(newEntryArray(i));
        }

        @DexIgnore
        public static <K, V, E extends i<K, V, E>> boolean isCollected(E e) {
            return e.getValue() == null;
        }

        @DexIgnore
        public abstract E castForTesting(i<K, V, ?> iVar);

        @DexIgnore
        public void clear() {
            if (this.count != 0) {
                lock();
                try {
                    AtomicReferenceArray<E> atomicReferenceArray = this.table;
                    for (int i = 0; i < atomicReferenceArray.length(); i++) {
                        atomicReferenceArray.set(i, (Object) null);
                    }
                    maybeClearReferenceQueues();
                    this.readCount.set(0);
                    this.modCount++;
                    this.count = 0;
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        public <T> void clearReferenceQueue(ReferenceQueue<T> referenceQueue) {
            do {
            } while (referenceQueue.poll() != null);
        }

        @DexIgnore
        public boolean clearValueForTesting(K k, int i, c0<K, V, ? extends i<K, V, ?>> c0Var) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.b() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else if (((b0) iVar2).c() == c0Var) {
                        atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                        return true;
                    } else {
                        unlock();
                        return false;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public boolean containsKey(Object obj, int i) {
            try {
                boolean z = false;
                if (this.count != 0) {
                    i liveEntry = getLiveEntry(obj, i);
                    if (!(liveEntry == null || liveEntry.getValue() == null)) {
                        z = true;
                    }
                    return z;
                }
                postReadCleanup();
                return false;
            } finally {
                postReadCleanup();
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public boolean containsValue(Object obj) {
            try {
                if (this.count != 0) {
                    AtomicReferenceArray<E> atomicReferenceArray = this.table;
                    int length = atomicReferenceArray.length();
                    for (int i = 0; i < length; i++) {
                        for (i iVar = (i) atomicReferenceArray.get(i); iVar != null; iVar = iVar.a()) {
                            Object liveValue = getLiveValue(iVar);
                            if (liveValue != null) {
                                if (this.map.valueEquivalence().equivalent(obj, liveValue)) {
                                    postReadCleanup();
                                    return true;
                                }
                            }
                        }
                    }
                }
                postReadCleanup();
                return false;
            } catch (Throwable th) {
                postReadCleanup();
                throw th;
            }
        }

        @DexIgnore
        public E copyEntry(E e, E e2) {
            return this.map.entryHelper.a(self(), e, e2);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.wm3$i, com.fossil.wm3$i<K, V, ?>] */
        /* JADX WARNING: type inference failed for: r4v0, types: [com.fossil.wm3$i, com.fossil.wm3$i<K, V, ?>] */
        /* JADX WARNING: Unknown variable types count: 2 */
        public E copyForTesting(i<K, V, ?> r3, i<K, V, ?> r4) {
            return this.map.entryHelper.a(self(), castForTesting(r3), castForTesting(r4));
        }

        @DexIgnore
        public void drainKeyReferenceQueue(ReferenceQueue<K> referenceQueue) {
            int i = 0;
            do {
                Reference<? extends K> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.reclaimKey((i) poll);
                    i++;
                } else {
                    return;
                }
            } while (i != 16);
        }

        @DexIgnore
        public void drainValueReferenceQueue(ReferenceQueue<V> referenceQueue) {
            int i = 0;
            do {
                Reference<? extends V> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.reclaimValue((c0) poll);
                    i++;
                } else {
                    return;
                }
            } while (i != 16);
        }

        @DexIgnore
        public void expand() {
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            int length = atomicReferenceArray.length();
            if (length < 1073741824) {
                int i = this.count;
                AtomicReferenceArray<E> newEntryArray = newEntryArray(length << 1);
                this.threshold = (newEntryArray.length() * 3) / 4;
                int length2 = newEntryArray.length() - 1;
                for (int i2 = 0; i2 < length; i2++) {
                    i iVar = (i) atomicReferenceArray.get(i2);
                    if (iVar != null) {
                        i a = iVar.a();
                        int b = iVar.b() & length2;
                        if (a == null) {
                            newEntryArray.set(b, iVar);
                        } else {
                            i iVar2 = iVar;
                            while (a != null) {
                                int b2 = a.b() & length2;
                                if (b2 != b) {
                                    iVar2 = a;
                                    b = b2;
                                }
                                a = a.a();
                            }
                            newEntryArray.set(b, iVar2);
                            while (iVar != iVar2) {
                                int b3 = iVar.b() & length2;
                                i copyEntry = copyEntry(iVar, (i) newEntryArray.get(b3));
                                if (copyEntry != null) {
                                    newEntryArray.set(b3, copyEntry);
                                } else {
                                    i--;
                                }
                                iVar = iVar.a();
                            }
                        }
                    }
                }
                this.table = newEntryArray;
                this.count = i;
            }
        }

        @DexIgnore
        public V get(Object obj, int i) {
            try {
                i liveEntry = getLiveEntry(obj, i);
                if (liveEntry == null) {
                    return null;
                }
                V value = liveEntry.getValue();
                if (value == null) {
                    tryDrainReferenceQueues();
                }
                postReadCleanup();
                return value;
            } finally {
                postReadCleanup();
            }
        }

        @DexIgnore
        public E getEntry(Object obj, int i) {
            if (this.count == 0) {
                return null;
            }
            for (E first = getFirst(i); first != null; first = first.a()) {
                if (first.b() == i) {
                    Object key = first.getKey();
                    if (key == null) {
                        tryDrainReferenceQueues();
                    } else if (this.map.keyEquivalence.equivalent(obj, key)) {
                        return first;
                    }
                }
            }
            return null;
        }

        @DexIgnore
        public E getFirst(int i) {
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            return (i) atomicReferenceArray.get(i & (atomicReferenceArray.length() - 1));
        }

        @DexIgnore
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            throw new AssertionError();
        }

        @DexIgnore
        public E getLiveEntry(Object obj, int i) {
            return getEntry(obj, i);
        }

        @DexIgnore
        public V getLiveValue(E e) {
            if (e.getKey() == null) {
                tryDrainReferenceQueues();
                return null;
            }
            V value = e.getValue();
            if (value != null) {
                return value;
            }
            tryDrainReferenceQueues();
            return null;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [com.fossil.wm3$i, com.fossil.wm3$i<K, V, ?>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        public V getLiveValueForTesting(i<K, V, ?> r1) {
            return getLiveValue(castForTesting(r1));
        }

        @DexIgnore
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            throw new AssertionError();
        }

        @DexIgnore
        public c0<K, V, E> getWeakValueReferenceForTesting(i<K, V, ?> iVar) {
            throw new AssertionError();
        }

        @DexIgnore
        public void initTable(AtomicReferenceArray<E> atomicReferenceArray) {
            this.threshold = (atomicReferenceArray.length() * 3) / 4;
            int i = this.threshold;
            if (i == this.maxSegmentSize) {
                this.threshold = i + 1;
            }
            this.table = atomicReferenceArray;
        }

        @DexIgnore
        public void maybeClearReferenceQueues() {
        }

        @DexIgnore
        public void maybeDrainReferenceQueues() {
        }

        @DexIgnore
        public AtomicReferenceArray<E> newEntryArray(int i) {
            return new AtomicReferenceArray<>(i);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r5v0, types: [com.fossil.wm3$i, com.fossil.wm3$i<K, V, ?>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        public E newEntryForTesting(K k, int i, i<K, V, ?> r5) {
            return this.map.entryHelper.a(self(), k, i, castForTesting(r5));
        }

        @DexIgnore
        public c0<K, V, E> newWeakValueReferenceForTesting(i<K, V, ?> iVar, V v) {
            throw new AssertionError();
        }

        @DexIgnore
        public void postReadCleanup() {
            if ((this.readCount.incrementAndGet() & 63) == 0) {
                runCleanup();
            }
        }

        @DexIgnore
        public void preWriteCleanup() {
            runLockedCleanup();
        }

        @DexIgnore
        public V put(K k, int i, V v, boolean z) {
            lock();
            try {
                preWriteCleanup();
                int i2 = this.count + 1;
                if (i2 > this.threshold) {
                    expand();
                    i2 = this.count + 1;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.b() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else {
                        V value = iVar2.getValue();
                        if (value == null) {
                            this.modCount++;
                            setValue(iVar2, v);
                            this.count = this.count;
                            return null;
                        } else if (z) {
                            unlock();
                            return value;
                        } else {
                            this.modCount++;
                            setValue(iVar2, v);
                            unlock();
                            return value;
                        }
                    }
                }
                this.modCount++;
                E a = this.map.entryHelper.a(self(), k, i, iVar);
                setValue(a, v);
                atomicReferenceArray.set(length, a);
                this.count = i2;
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public boolean reclaimKey(E e, int i) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = i & (atomicReferenceArray.length() - 1);
                E e2 = (i) atomicReferenceArray.get(length);
                for (E e3 = e2; e3 != null; e3 = e3.a()) {
                    if (e3 == e) {
                        this.modCount++;
                        atomicReferenceArray.set(length, removeFromChain(e2, e3));
                        this.count--;
                        return true;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public boolean reclaimValue(K k, int i, c0<K, V, E> c0Var) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.b() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else if (((b0) iVar2).c() == c0Var) {
                        this.modCount++;
                        atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                        this.count--;
                        return true;
                    } else {
                        unlock();
                        return false;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public V remove(Object obj, int i) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.b() != i || key == null || !this.map.keyEquivalence.equivalent(obj, key)) {
                        iVar2 = iVar2.a();
                    } else {
                        V value = iVar2.getValue();
                        if (value == null) {
                            if (!isCollected(iVar2)) {
                                unlock();
                                return null;
                            }
                        }
                        this.modCount++;
                        atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                        this.count--;
                        return value;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public boolean removeEntryForTesting(E e) {
            int b = e.b();
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            int length = b & (atomicReferenceArray.length() - 1);
            E e2 = (i) atomicReferenceArray.get(length);
            for (E e3 = e2; e3 != null; e3 = e3.a()) {
                if (e3 == e) {
                    this.modCount++;
                    atomicReferenceArray.set(length, removeFromChain(e2, e3));
                    this.count--;
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public E removeFromChain(E e, E e2) {
            int i = this.count;
            E a = e2.a();
            while (e != e2) {
                E copyEntry = copyEntry(e, a);
                if (copyEntry != null) {
                    a = copyEntry;
                } else {
                    i--;
                }
                e = e.a();
            }
            this.count = i;
            return a;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [com.fossil.wm3$i, com.fossil.wm3$i<K, V, ?>] */
        /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.wm3$i, com.fossil.wm3$i<K, V, ?>] */
        /* JADX WARNING: Unknown variable types count: 2 */
        public E removeFromChainForTesting(i<K, V, ?> r1, i<K, V, ?> r2) {
            return removeFromChain(castForTesting(r1), castForTesting(r2));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [com.fossil.wm3$i, com.fossil.wm3$i<K, V, ?>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        public boolean removeTableEntryForTesting(i<K, V, ?> r1) {
            return removeEntryForTesting(castForTesting(r1));
        }

        @DexIgnore
        public boolean replace(K k, int i, V v, V v2) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.b() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else {
                        Object value = iVar2.getValue();
                        if (value == null) {
                            if (isCollected(iVar2)) {
                                this.modCount++;
                                atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                                this.count--;
                            }
                            return false;
                        } else if (this.map.valueEquivalence().equivalent(v, value)) {
                            this.modCount++;
                            setValue(iVar2, v2);
                            unlock();
                            return true;
                        } else {
                            unlock();
                            return false;
                        }
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public void runCleanup() {
            runLockedCleanup();
        }

        @DexIgnore
        public void runLockedCleanup() {
            if (tryLock()) {
                try {
                    maybeDrainReferenceQueues();
                    this.readCount.set(0);
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        public abstract S self();

        @DexIgnore
        public void setTableEntryForTesting(int i, i<K, V, ?> iVar) {
            this.table.set(i, castForTesting(iVar));
        }

        @DexIgnore
        public void setValue(E e, V v) {
            this.map.entryHelper.a(self(), e, v);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.wm3$i, com.fossil.wm3$i<K, V, ?>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        public void setValueForTesting(i<K, V, ?> r3, V v) {
            this.map.entryHelper.a(self(), castForTesting(r3), v);
        }

        @DexIgnore
        public void setWeakValueReferenceForTesting(i<K, V, ?> iVar, c0<K, V, ? extends i<K, V, ?>> c0Var) {
            throw new AssertionError();
        }

        @DexIgnore
        public void tryDrainReferenceQueues() {
            if (tryLock()) {
                try {
                    maybeDrainReferenceQueues();
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        public boolean remove(Object obj, int i, Object obj2) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (true) {
                    boolean z = false;
                    if (iVar2 != null) {
                        Object key = iVar2.getKey();
                        if (iVar2.b() != i || key == null || !this.map.keyEquivalence.equivalent(obj, key)) {
                            iVar2 = iVar2.a();
                        } else {
                            if (this.map.valueEquivalence().equivalent(obj2, iVar2.getValue())) {
                                z = true;
                            } else if (!isCollected(iVar2)) {
                                unlock();
                                return false;
                            }
                            this.modCount++;
                            atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                            this.count--;
                            return z;
                        }
                    } else {
                        unlock();
                        return false;
                    }
                }
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public V replace(K k, int i, V v) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                i iVar = (i) atomicReferenceArray.get(length);
                i iVar2 = iVar;
                while (iVar2 != null) {
                    Object key = iVar2.getKey();
                    if (iVar2.b() != i || key == null || !this.map.keyEquivalence.equivalent(k, key)) {
                        iVar2 = iVar2.a();
                    } else {
                        V value = iVar2.getValue();
                        if (value == null) {
                            if (isCollected(iVar2)) {
                                this.modCount++;
                                atomicReferenceArray.set(length, removeFromChain(iVar, iVar2));
                                this.count--;
                            }
                            return null;
                        }
                        this.modCount++;
                        setValue(iVar2, v);
                        unlock();
                        return value;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }
    }
}
