package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fg {
    @DexIgnore
    public boolean a; // = true;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;

    @DexIgnore
    public boolean a(RecyclerView.State state) {
        int i2 = this.c;
        return i2 >= 0 && i2 < state.a();
    }

    @DexIgnore
    public String toString() {
        return "LayoutState{mAvailable=" + this.b + ", mCurrentPosition=" + this.c + ", mItemDirection=" + this.d + ", mLayoutDirection=" + this.e + ", mStartLine=" + this.f + ", mEndLine=" + this.g + '}';
    }

    @DexIgnore
    public View a(RecyclerView.Recycler recycler) {
        View d2 = recycler.d(this.c);
        this.c += this.d;
        return d2;
    }
}
