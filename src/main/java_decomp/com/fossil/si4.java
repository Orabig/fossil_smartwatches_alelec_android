package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class si4 extends ii4 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public si4(String str, int i) {
        this.a = i;
        this.b = str;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.b;
    }
}
