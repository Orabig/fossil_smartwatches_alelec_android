package com.fossil;

import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ki2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Object g;
    @DexIgnore
    public /* final */ /* synthetic */ boolean h;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ki2(ov2 ov2, String str, String str2, Object obj, boolean z) {
        super(ov2);
        this.i = ov2;
        this.e = str;
        this.f = str2;
        this.g = obj;
        this.h = z;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        this.i.g.setUserProperty(this.e, this.f, z52.a(this.g), this.h, this.a);
    }
}
