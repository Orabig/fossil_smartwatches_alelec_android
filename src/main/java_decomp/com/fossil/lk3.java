package com.fossil;

import java.io.Serializable;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T> implements kk3<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Collection<?> target;

        @DexIgnore
        public boolean apply(T t) {
            try {
                return this.target.contains(t);
            } catch (ClassCastException | NullPointerException unused) {
                return false;
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof b) {
                return this.target.equals(((b) obj).target);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.target.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.in(" + this.target + ")";
        }

        @DexIgnore
        public b(Collection<?> collection) {
            jk3.a(collection);
            this.target = collection;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T> implements kk3<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ T target;

        @DexIgnore
        public boolean apply(T t) {
            return this.target.equals(t);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof c) {
                return this.target.equals(((c) obj).target);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.target.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.equalTo(" + this.target + ")";
        }

        @DexIgnore
        public c(T t) {
            this.target = t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<T> implements kk3<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ kk3<T> predicate;

        @DexIgnore
        public d(kk3<T> kk3) {
            jk3.a(kk3);
            this.predicate = kk3;
        }

        @DexIgnore
        public boolean apply(T t) {
            return !this.predicate.apply(t);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return this.predicate.equals(((d) obj).predicate);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return ~this.predicate.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.not(" + this.predicate + ")";
        }
    }

    @DexIgnore
    public enum e implements kk3<Object> {
        ALWAYS_TRUE {
            @DexIgnore
            public boolean apply(Object obj) {
                return true;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.alwaysTrue()";
            }
        },
        ALWAYS_FALSE {
            @DexIgnore
            public boolean apply(Object obj) {
                return false;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.alwaysFalse()";
            }
        },
        IS_NULL {
            @DexIgnore
            public boolean apply(Object obj) {
                return obj == null;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.isNull()";
            }
        },
        NOT_NULL {
            @DexIgnore
            public boolean apply(Object obj) {
                return obj != null;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.notNull()";
            }
        };

        @DexIgnore
        public <T> kk3<T> withNarrowedType() {
            return this;
        }
    }

    /*
    static {
        ek3.a(',');
    }
    */

    @DexIgnore
    public static <T> kk3<T> a() {
        return e.IS_NULL.withNarrowedType();
    }

    @DexIgnore
    public static <T> kk3<T> a(kk3<T> kk3) {
        return new d(kk3);
    }

    @DexIgnore
    public static <T> kk3<T> a(T t) {
        return t == null ? a() : new c(t);
    }

    @DexIgnore
    public static <T> kk3<T> a(Collection<? extends T> collection) {
        return new b(collection);
    }
}
