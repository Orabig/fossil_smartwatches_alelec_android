package com.fossil;

import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ji5$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ cf $dataList;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDetailPresenter.d this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ji5$d$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xe6 xe6, ji5$d$a ji5_d_a) {
            super(2, xe6);
            this.this$0 = ji5_d_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(xe6, this.this$0);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                GoalTrackingDetailPresenter goalTrackingDetailPresenter = this.this$0.this$0.a;
                goalTrackingDetailPresenter.m = goalTrackingDetailPresenter.q.getGoalTrackingDataInDate(this.this$0.this$0.b);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ji5$d$a(cf cfVar, xe6 xe6, GoalTrackingDetailPresenter.d dVar) {
        super(2, xe6);
        this.$dataList = cfVar;
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ji5$d$a ji5_d_a = new ji5$d$a(this.$dataList, xe6, this.this$0);
        ji5_d_a.p$ = (il6) obj;
        return ji5_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ji5$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        xp6 xp6;
        il6 il6;
        xp6 xp62;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            xp62 = this.this$0.a.j;
            this.L$0 = il6;
            this.L$1 = xp62;
            this.label = 1;
            if (xp62.a((Object) null, this) == a2) {
                return a2;
            }
        } else if (i == 1) {
            xp62 = (xp6) this.L$1;
            nc6.a(obj);
            il6 = (il6) this.L$0;
        } else if (i == 2) {
            xp6 = (xp6) this.L$1;
            il6 il62 = (il6) this.L$0;
            try {
                nc6.a(obj);
                this.this$0.a.p.c(this.$dataList);
                if (this.this$0.a.h && this.this$0.a.i) {
                    rm6 unused = this.this$0.a.m();
                }
                cd6 cd6 = cd6.a;
                xp6.a((Object) null);
                return cd6.a;
            } catch (Throwable th) {
                th = th;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            dl6 b = zl6.b();
            a aVar = new a((xe6) null, this);
            this.L$0 = il6;
            this.L$1 = xp62;
            this.label = 2;
            if (gk6.a(b, aVar, this) == a2) {
                return a2;
            }
            xp6 = xp62;
            this.this$0.a.p.c(this.$dataList);
            rm6 unused2 = this.this$0.a.m();
            cd6 cd62 = cd6.a;
            xp6.a((Object) null);
            return cd6.a;
        } catch (Throwable th2) {
            th = th2;
            xp6 = xp62;
            xp6.a((Object) null);
            throw th;
        }
    }
}
