package com.fossil;

import android.annotation.SuppressLint;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bg implements Runnable {
    @DexIgnore
    public static /* final */ ThreadLocal<bg> e; // = new ThreadLocal<>();
    @DexIgnore
    public static Comparator<c> f; // = new a();
    @DexIgnore
    public ArrayList<RecyclerView> a; // = new ArrayList<>();
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;
    @DexIgnore
    public ArrayList<c> d; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Comparator<c> {
        @DexIgnore
        /* renamed from: a */
        public int compare(c cVar, c cVar2) {
            if ((cVar.d == null) == (cVar2.d == null)) {
                boolean z = cVar.a;
                if (z == cVar2.a) {
                    int i = cVar2.b - cVar.b;
                    if (i != 0) {
                        return i;
                    }
                    int i2 = cVar.c - cVar2.c;
                    if (i2 != 0) {
                        return i2;
                    }
                    return 0;
                } else if (z) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (cVar.d == null) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public RecyclerView d;
        @DexIgnore
        public int e;

        @DexIgnore
        public void a() {
            this.a = false;
            this.b = 0;
            this.c = 0;
            this.d = null;
            this.e = 0;
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) {
        this.a.add(recyclerView);
    }

    @DexIgnore
    public void b(RecyclerView recyclerView) {
        this.a.remove(recyclerView);
    }

    @DexIgnore
    public void run() {
        try {
            g8.a(RecyclerView.TRACE_PREFETCH_TAG);
            if (!this.a.isEmpty()) {
                int size = this.a.size();
                long j = 0;
                for (int i = 0; i < size; i++) {
                    RecyclerView recyclerView = this.a.get(i);
                    if (recyclerView.getWindowVisibility() == 0) {
                        j = Math.max(recyclerView.getDrawingTime(), j);
                    }
                }
                if (j != 0) {
                    b(TimeUnit.MILLISECONDS.toNanos(j) + this.c);
                    this.b = 0;
                    g8.a();
                }
            }
        } finally {
            this.b = 0;
            g8.a();
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView, int i, int i2) {
        if (recyclerView.isAttachedToWindow() && this.b == 0) {
            this.b = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.mPrefetchRegistry.b(i, i2);
    }

    @DexIgnore
    public void b(long j) {
        a();
        a(j);
    }

    @DexIgnore
    public final void a() {
        c cVar;
        int size = this.a.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            RecyclerView recyclerView = this.a.get(i2);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.mPrefetchRegistry.a(recyclerView, false);
                i += recyclerView.mPrefetchRegistry.d;
            }
        }
        this.d.ensureCapacity(i);
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            RecyclerView recyclerView2 = this.a.get(i4);
            if (recyclerView2.getWindowVisibility() == 0) {
                b bVar = recyclerView2.mPrefetchRegistry;
                int abs = Math.abs(bVar.a) + Math.abs(bVar.b);
                int i5 = i3;
                for (int i6 = 0; i6 < bVar.d * 2; i6 += 2) {
                    if (i5 >= this.d.size()) {
                        cVar = new c();
                        this.d.add(cVar);
                    } else {
                        cVar = this.d.get(i5);
                    }
                    int i7 = bVar.c[i6 + 1];
                    cVar.a = i7 <= abs;
                    cVar.b = abs;
                    cVar.c = i7;
                    cVar.d = recyclerView2;
                    cVar.e = bVar.c[i6];
                    i5++;
                }
                i3 = i5;
            }
        }
        Collections.sort(this.d, f);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"VisibleForTests"})
    public static class b implements RecyclerView.m.c {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int[] c;
        @DexIgnore
        public int d;

        @DexIgnore
        public void a(RecyclerView recyclerView, boolean z) {
            this.d = 0;
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            RecyclerView.m mVar = recyclerView.mLayout;
            if (recyclerView.mAdapter != null && mVar != null && mVar.w()) {
                if (z) {
                    if (!recyclerView.mAdapterHelper.c()) {
                        mVar.a(recyclerView.mAdapter.getItemCount(), (RecyclerView.m.c) this);
                    }
                } else if (!recyclerView.hasPendingAdapterUpdates()) {
                    mVar.a(this.a, this.b, recyclerView.mState, (RecyclerView.m.c) this);
                }
                int i = this.d;
                if (i > mVar.m) {
                    mVar.m = i;
                    mVar.n = z;
                    recyclerView.mRecycler.j();
                }
            }
        }

        @DexIgnore
        public void b(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public void a(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i2 >= 0) {
                int i3 = this.d * 2;
                int[] iArr = this.c;
                if (iArr == null) {
                    this.c = new int[4];
                    Arrays.fill(this.c, -1);
                } else if (i3 >= iArr.length) {
                    this.c = new int[(i3 * 2)];
                    System.arraycopy(iArr, 0, this.c, 0, iArr.length);
                }
                int[] iArr2 = this.c;
                iArr2[i3] = i;
                iArr2[i3 + 1] = i2;
                this.d++;
            } else {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            }
        }

        @DexIgnore
        public boolean a(int i) {
            if (this.c != null) {
                int i2 = this.d * 2;
                for (int i3 = 0; i3 < i2; i3 += 2) {
                    if (this.c[i3] == i) {
                        return true;
                    }
                }
            }
            return false;
        }

        @DexIgnore
        public void a() {
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.d = 0;
        }
    }

    @DexIgnore
    public static boolean a(RecyclerView recyclerView, int i) {
        int b2 = recyclerView.mChildHelper.b();
        for (int i2 = 0; i2 < b2; i2++) {
            RecyclerView.ViewHolder childViewHolderInt = RecyclerView.getChildViewHolderInt(recyclerView.mChildHelper.e(i2));
            if (childViewHolderInt.mPosition == i && !childViewHolderInt.isInvalid()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final RecyclerView.ViewHolder a(RecyclerView recyclerView, int i, long j) {
        if (a(recyclerView, i)) {
            return null;
        }
        RecyclerView.Recycler recycler = recyclerView.mRecycler;
        try {
            recyclerView.onEnterLayoutOrScroll();
            RecyclerView.ViewHolder a2 = recycler.a(i, false, j);
            if (a2 != null) {
                if (!a2.isBound() || a2.isInvalid()) {
                    recycler.a(a2, false);
                } else {
                    recycler.b(a2.itemView);
                }
            }
            return a2;
        } finally {
            recyclerView.onExitLayoutOrScroll(false);
        }
    }

    @DexIgnore
    public final void a(RecyclerView recyclerView, long j) {
        if (recyclerView != null) {
            if (recyclerView.mDataSetHasChangedAfterLayout && recyclerView.mChildHelper.b() != 0) {
                recyclerView.removeAndRecycleViews();
            }
            b bVar = recyclerView.mPrefetchRegistry;
            bVar.a(recyclerView, true);
            if (bVar.d != 0) {
                try {
                    g8.a(RecyclerView.TRACE_NESTED_PREFETCH_TAG);
                    recyclerView.mState.a(recyclerView.mAdapter);
                    for (int i = 0; i < bVar.d * 2; i += 2) {
                        a(recyclerView, bVar.c[i], j);
                    }
                } finally {
                    g8.a();
                }
            }
        }
    }

    @DexIgnore
    public final void a(c cVar, long j) {
        RecyclerView.ViewHolder a2 = a(cVar.d, cVar.e, cVar.a ? RecyclerView.FOREVER_NS : j);
        if (a2 != null && a2.mNestedRecyclerView != null && a2.isBound() && !a2.isInvalid()) {
            a((RecyclerView) a2.mNestedRecyclerView.get(), j);
        }
    }

    @DexIgnore
    public final void a(long j) {
        int i = 0;
        while (i < this.d.size()) {
            c cVar = this.d.get(i);
            if (cVar.d != null) {
                a(cVar, j);
                cVar.a();
                i++;
            } else {
                return;
            }
        }
    }
}
