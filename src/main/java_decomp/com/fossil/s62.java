package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.x52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s62 extends cb2 implements r62 {
    @DexIgnore
    public s62(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    @DexIgnore
    public final x52 a(x52 x52, String str, int i) throws RemoteException {
        Parcel zza = zza();
        eb2.a(zza, (IInterface) x52);
        zza.writeString(str);
        zza.writeInt(i);
        Parcel a = a(2, zza);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final int b(x52 x52, String str, boolean z) throws RemoteException {
        Parcel zza = zza();
        eb2.a(zza, (IInterface) x52);
        zza.writeString(str);
        eb2.a(zza, z);
        Parcel a = a(3, zza);
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    public final int p() throws RemoteException {
        Parcel a = a(6, zza());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    public final int a(x52 x52, String str, boolean z) throws RemoteException {
        Parcel zza = zza();
        eb2.a(zza, (IInterface) x52);
        zza.writeString(str);
        eb2.a(zza, z);
        Parcel a = a(5, zza);
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    public final x52 b(x52 x52, String str, int i) throws RemoteException {
        Parcel zza = zza();
        eb2.a(zza, (IInterface) x52);
        zza.writeString(str);
        zza.writeInt(i);
        Parcel a = a(4, zza);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
