package com.fossil;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import com.google.android.gms.dynamite.DynamiteModule;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j52 {
    @DexIgnore
    public static volatile t32 a;
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static Context c;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        return;
     */
    @DexIgnore
    public static synchronized void a(Context context) {
        synchronized (j52.class) {
            if (c != null) {
                Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
            } else if (context != null) {
                c = context.getApplicationContext();
            }
        }
    }

    @DexIgnore
    public static t52 b(String str, l52 l52, boolean z, boolean z2) {
        try {
            if (a == null) {
                w12.a(c);
                synchronized (b) {
                    if (a == null) {
                        a = u32.a(DynamiteModule.a(c, DynamiteModule.k, "com.google.android.gms.googlecertificates").a("com.google.android.gms.common.GoogleCertificatesImpl"));
                    }
                }
            }
            w12.a(c);
            try {
                if (a.a(new r52(str, l52, z, z2), z52.a(c.getPackageManager()))) {
                    return t52.c();
                }
                return t52.a((Callable<String>) new k52(z, str, l52));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
                return t52.a("module call", e);
            }
        } catch (DynamiteModule.a e2) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            String valueOf = String.valueOf(e2.getMessage());
            return t52.a(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e2);
        }
    }

    @DexIgnore
    public static t52 a(String str, l52 l52, boolean z, boolean z2) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return b(str, l52, z, z2);
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    @DexIgnore
    public static final /* synthetic */ String a(boolean z, String str, l52 l52) throws Exception {
        boolean z2 = true;
        if (z || !b(str, l52, true, false).a) {
            z2 = false;
        }
        return t52.a(str, l52, z, z2);
    }
}
