package com.fossil;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wu6 extends OutputStream {
    @DexIgnore
    public static /* final */ byte[] f; // = new byte[0];
    @DexIgnore
    public /* final */ List<byte[]> a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public byte[] d;
    @DexIgnore
    public int e;

    @DexIgnore
    public wu6() {
        this(1024);
    }

    @DexIgnore
    public final void b(int i) {
        if (this.b < this.a.size() - 1) {
            this.c += this.d.length;
            this.b++;
            this.d = this.a.get(this.b);
            return;
        }
        byte[] bArr = this.d;
        if (bArr == null) {
            this.c = 0;
        } else {
            i = Math.max(bArr.length << 1, i - this.c);
            this.c += this.d.length;
        }
        this.b++;
        this.d = new byte[i];
        this.a.add(this.d);
    }

    @DexIgnore
    public void close() throws IOException {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        return r1;
     */
    @DexIgnore
    public synchronized byte[] k() {
        int i = this.e;
        if (i != 0) {
            byte[] bArr = new byte[i];
            int i2 = 0;
            for (byte[] next : this.a) {
                int min = Math.min(next.length, i);
                System.arraycopy(next, 0, bArr, i2, min);
                i2 += min;
                i -= min;
                if (i == 0) {
                    break;
                }
            }
        } else {
            return f;
        }
    }

    @DexIgnore
    public String toString() {
        return new String(k());
    }

    @DexIgnore
    public void write(byte[] bArr, int i, int i2) {
        int i3;
        if (i < 0 || i > bArr.length || i2 < 0 || (i3 = i + i2) > bArr.length || i3 < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 != 0) {
            synchronized (this) {
                int i4 = this.e + i2;
                int i5 = this.e - this.c;
                while (i2 > 0) {
                    int min = Math.min(i2, this.d.length - i5);
                    System.arraycopy(bArr, i3 - i2, this.d, i5, min);
                    i2 -= min;
                    if (i2 > 0) {
                        b(i4);
                        i5 = 0;
                    }
                }
                this.e = i4;
            }
        }
    }

    @DexIgnore
    public wu6(int i) {
        this.a = new ArrayList();
        if (i >= 0) {
            synchronized (this) {
                b(i);
            }
            return;
        }
        throw new IllegalArgumentException("Negative initial size: " + i);
    }

    @DexIgnore
    public synchronized void write(int i) {
        int i2 = this.e - this.c;
        if (i2 == this.d.length) {
            b(this.e + 1);
            i2 = 0;
        }
        this.d[i2] = (byte) i;
        this.e++;
    }
}
