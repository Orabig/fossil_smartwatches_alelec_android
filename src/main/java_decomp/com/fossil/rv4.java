package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rv4 {
    @DexIgnore
    public /* final */ qv4 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ArrayList<Alarm> c;
    @DexIgnore
    public /* final */ Alarm d;

    @DexIgnore
    public rv4(qv4 qv4, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        wg6.b(qv4, "mView");
        wg6.b(str, "mDeviceId");
        wg6.b(arrayList, "mAlarms");
        this.a = qv4;
        this.b = str;
        this.c = arrayList;
        this.d = alarm;
    }

    @DexIgnore
    public final Alarm a() {
        return this.d;
    }

    @DexIgnore
    public final ArrayList<Alarm> b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final qv4 d() {
        return this.a;
    }
}
