package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface b05 extends k24<a05> {
    @DexIgnore
    void M(boolean z);

    @DexIgnore
    void a(int i, boolean z);

    @DexIgnore
    void a(String str);

    @DexIgnore
    void close();

    @DexIgnore
    void x(String str);
}
