package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.view.FlexibleEditText;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xk4 {
    @DexIgnore
    public static /* final */ String a; // = xk4.class.toString();

    @DexIgnore
    public static boolean a(Context context) {
        return context.getResources().getConfiguration().getLayoutDirection() == 1;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    @SuppressLint({"RtlHardcoded"})
    public static void a(View view, Context context) {
        try {
            if (a(context) && (view instanceof FlexibleEditText)) {
                ((FlexibleEditText) view).setGravity(8388629);
                view.setTextDirection(4);
            }
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d(a, e.toString());
        }
    }
}
