package com.fossil;

import android.graphics.Bitmap;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.OutputSettings;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$buildFilterList$1$filters$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
public final class b75$g$a extends sf6 implements ig6<il6, xe6<? super ArrayList<FilterResult>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ EditPhotoViewModel.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b75$g$a(EditPhotoViewModel.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        b75$g$a b75_g_a = new b75$g$a(this.this$0, xe6);
        b75_g_a.p$ = (il6) obj;
        return b75_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((b75$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            EditPhotoViewModel.d dVar = this.this$0.$cropImageInfo;
            Bitmap a = lw4.a(lw4.a(dVar.c(), dVar.h(), dVar.d(), dVar.e(), dVar.a(), dVar.b(), dVar.f(), dVar.g()).a, 480, 480);
            EInkImageFilter create = EInkImageFilter.create();
            wg6.a((Object) create, "EInkImageFilter.create()");
            return create.applyFilters(a, this.this$0.$filterList, false, false, OutputSettings.BACKGROUND);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
