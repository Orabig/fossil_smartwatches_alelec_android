package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xe5 implements Factory<we5> {
    @DexIgnore
    public static GoalTrackingOverviewWeekPresenter a(ve5 ve5, UserRepository userRepository, an4 an4, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewWeekPresenter(ve5, userRepository, an4, goalTrackingRepository);
    }
}
