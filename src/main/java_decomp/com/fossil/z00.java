package com.fossil;

import com.facebook.internal.FileLruCache;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z00 {
    @DexIgnore
    public /* final */ b10 a;
    @DexIgnore
    public /* final */ Map<String, Object> b; // = new ConcurrentHashMap();

    @DexIgnore
    public z00(b10 b10) {
        this.a = b10;
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (!this.a.a((Object) str, FileLruCache.HEADER_CACHEKEY_KEY) && !this.a.a((Object) str2, "value")) {
            a(this.a.a(str), (Object) this.a.a(str2));
        }
    }

    @DexIgnore
    public String toString() {
        return new JSONObject(this.b).toString();
    }

    @DexIgnore
    public void a(String str, Number number) {
        if (!this.a.a((Object) str, FileLruCache.HEADER_CACHEKEY_KEY) && !this.a.a((Object) number, "value")) {
            a(this.a.a(str), (Object) number);
        }
    }

    @DexIgnore
    public void a(String str, Object obj) {
        if (!this.a.a(this.b, str)) {
            this.b.put(str, obj);
        }
    }
}
