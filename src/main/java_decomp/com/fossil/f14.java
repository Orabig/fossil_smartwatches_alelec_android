package com.fossil;

import android.content.Context;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f14 implements Factory<Context> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public f14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static f14 a(b14 b14) {
        return new f14(b14);
    }

    @DexIgnore
    public static Context b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static Context c(b14 b14) {
        Context c = b14.c();
        z76.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }

    @DexIgnore
    public Context get() {
        return b(this.a);
    }
}
