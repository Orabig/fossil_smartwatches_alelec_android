package com.fossil;

import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$showPresets$1$1", f = "HomeDianaCustomizePresenter.kt", l = {}, m = "invokeSuspend")
public final class l45$g$a extends sf6 implements ig6<il6, xe6<? super DianaComplicationRingStyle>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l45$g$a(HomeDianaCustomizePresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        l45$g$a l45_g_a = new l45$g$a(this.this$0, xe6);
        l45_g_a.p$ = (il6) obj;
        return l45_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((l45$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            RingStyleRepository p = this.this$0.this$0.w;
            Object a = this.this$0.this$0.k.a();
            if (a != null) {
                wg6.a(a, "mSerialLiveData.value!!");
                return p.getRingStylesBySerial((String) a);
            }
            wg6.a();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
