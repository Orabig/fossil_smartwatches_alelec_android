package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oh5 implements Factory<mh5> {
    @DexIgnore
    public static mh5 a(nh5 nh5) {
        mh5 a = nh5.a();
        z76.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
