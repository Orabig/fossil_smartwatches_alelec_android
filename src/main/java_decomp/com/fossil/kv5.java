package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import com.fossil.dk4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv5 extends gv5 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ long t; // = TimeUnit.SECONDS.toMillis(15);
    @DexIgnore
    public static /* final */ long u; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public static /* final */ a v; // = new a((qg6) null);
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 3;
    @DexIgnore
    public /* final */ d h; // = new d(this, t, u);
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ Runnable k; // = new e(this);
    @DexIgnore
    public /* final */ f l; // = new f(this);
    @DexIgnore
    public /* final */ c m; // = new c(this);
    @DexIgnore
    public /* final */ b n; // = new b(this);
    @DexIgnore
    public /* final */ PortfolioApp o;
    @DexIgnore
    public /* final */ hv5 p;
    @DexIgnore
    public /* final */ ce q;
    @DexIgnore
    public /* final */ el4 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return kv5.s;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ kv5 a;

        @DexIgnore
        public b(kv5 kv5) {
            this.a = kv5;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            if (wg6.a((Object) "android.bluetooth.adapter.action.STATE_CHANGED", (Object) intent.getAction())) {
                int intExtra = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = kv5.v.a();
                local.d(a2, "mBluetoothStateChangeReceiver - onReceive() - state = " + intExtra);
                if (intExtra == 10) {
                    FLogger.INSTANCE.getLocal().d(kv5.v.a(), "Bluetooth off");
                    this.a.g();
                    this.a.p.a();
                    this.a.p.D();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ kv5 a;

        @DexIgnore
        public c(kv5 kv5) {
            this.a = kv5;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = kv5.v.a();
            local.d(a2, "mConnectionStateChangeReceiver onReceive: status = " + intExtra);
            if (intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
                wg6.a((Object) defaultAdapter, "BluetoothAdapter.getDefaultAdapter()");
                if (!defaultAdapter.isEnabled()) {
                    this.a.p.a();
                    this.a.p.D();
                    return;
                }
                this.a.g();
                this.a.d(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends CountDownTimer {
        @DexIgnore
        public /* final */ /* synthetic */ kv5 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kv5 kv5, long j, long j2) {
            super(j, j2);
            this.a = kv5;
        }

        @DexIgnore
        public void onFinish() {
            FLogger.INSTANCE.getLocal().d(kv5.v.a(), "CountDownTimer onFinish");
            this.a.r.a(true, this.a.o.e(), 0, CalibrationEnums.HandId.HOUR);
            cancel();
            start();
        }

        @DexIgnore
        public void onTick(long j) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ kv5 a;

        @DexIgnore
        public e(kv5 kv5) {
            this.a = kv5;
        }

        @DexIgnore
        public final void run() {
            if (this.a.j) {
                this.a.p.a();
                this.a.o();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ kv5 a;

        @DexIgnore
        public f(kv5 kv5) {
            this.a = kv5;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            boolean z = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal();
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = kv5.v.a();
            local.d(a2, "receiver mode=" + communicateMode + ", isSuccess=" + z);
            int i = lv5.a[communicateMode.ordinal()];
            if (i == 1) {
                this.a.f(z);
            } else if (i == 2) {
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                this.a.a(z, integerArrayListExtra, intExtra);
            } else if (i == 3) {
                this.a.a(z, intExtra);
            } else if (i == 4) {
                this.a.e(z);
            } else if (i == 5) {
                this.a.d(z);
            }
        }
    }

    /*
    static {
        String simpleName = kv5.class.getSimpleName();
        wg6.a((Object) simpleName, "CalibrationPresenter::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public kv5(PortfolioApp portfolioApp, hv5 hv5, ce ceVar, el4 el4) {
        wg6.b(portfolioApp, "mApp");
        wg6.b(hv5, "mView");
        wg6.b(ceVar, "mLocalBroadcastManager");
        wg6.b(el4, "mWatchHelper");
        this.o = portfolioApp;
        this.p = hv5;
        this.q = ceVar;
        this.r = el4;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(s, "start()");
        r();
        m();
        p();
        BleCommandResultManager.d.a(CommunicateMode.ENTER_CALIBRATION);
        this.g = DeviceHelper.o.a(this.o.e()) == MFDeviceFamily.DEVICE_FAMILY_SAM ? 3 : 2;
        this.p.a(this.f, this.g);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(s, "stop");
        if (this.f != 3) {
            this.r.a(this.o.e());
        }
        r();
        this.h.cancel();
        q();
    }

    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d(s, "back");
        int i2 = this.f;
        if (i2 == 0) {
            this.p.o();
        } else if (i2 == 1) {
            this.f = 0;
            this.p.k(this.o.e());
        } else if (i2 == 2) {
            this.f = 1;
            this.p.r(this.o.e());
        } else if (i2 == 3) {
            if (DeviceHelper.o.a(this.o.e()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.f = 2;
                this.p.j(this.o.e());
            } else {
                this.f = 1;
                this.p.r(this.o.e());
            }
        }
        this.p.a(this.f, this.g);
    }

    @DexIgnore
    public List<dk4.b> i() {
        ArrayList arrayList = new ArrayList();
        MFDeviceFamily a2 = DeviceHelper.o.a(this.o.e());
        if (a2 == MFDeviceFamily.DEVICE_FAMILY_SAM || a2 == MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM || a2 == MFDeviceFamily.DEVICE_FAMILY_SAM_MINI) {
            arrayList.add(DeviceHelper.ImageStyle.HYBRID_WATCH_HOUR);
            arrayList.add(DeviceHelper.ImageStyle.HYBRID_WATCH_MINUTE);
            arrayList.add(DeviceHelper.ImageStyle.HYBRID_WATCH_SUBEYE);
        } else {
            arrayList.add(DeviceHelper.ImageStyle.DIANA_WATCH_HOUR);
            arrayList.add(DeviceHelper.ImageStyle.DIANA_WATCH_MINUTE);
        }
        return arrayList;
    }

    @DexIgnore
    public void j() {
        FLogger.INSTANCE.getLocal().d(s, "next");
        int i2 = this.f;
        if (i2 == 0) {
            a("device_calibrate", "Step", "hour");
            this.f = 1;
            this.p.r(this.o.e());
        } else if (i2 == 1) {
            a("device_calibrate", "Step", "minute");
            if (DeviceHelper.o.a(this.o.e()) == MFDeviceFamily.DEVICE_FAMILY_SAM) {
                this.f = 2;
                this.p.j(this.o.e());
            } else {
                this.f = 3;
                l();
            }
        } else if (i2 == 2) {
            a("device_calibrate", "Step", "subeye");
            this.f = 3;
            l();
        } else if (i2 == 3) {
            l();
        }
        this.p.a(this.f, this.g);
    }

    @DexIgnore
    public void k() {
        FLogger.INSTANCE.getLocal().d(s, "stopSmartMove");
        this.r.a();
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d(s, "completeCalibration");
        this.h.cancel();
        this.p.b();
        this.r.b(this.o.e());
        this.f = 4;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void m() {
        FLogger.INSTANCE.getLocal().d(s, "registerBroadcastReceiver()");
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.l, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
        ce ceVar = this.q;
        c cVar = this.m;
        ceVar.a(cVar, new IntentFilter(this.o.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        this.o.registerReceiver(this.n, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }

    @DexIgnore
    public void n() {
        this.p.a(this);
    }

    @DexIgnore
    public final void o() {
        if (!this.e) {
            this.p.R();
        }
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d(s, "startCalibration");
        int i2 = this.f;
        if (i2 == 0) {
            this.p.k(this.o.e());
        } else if (i2 == 1) {
            this.p.r(this.o.e());
        } else if (i2 == 2) {
            this.p.j(this.o.e());
        }
        if (!this.e) {
            this.p.b();
            FLogger.INSTANCE.getLocal().e(s, "mStartCalibration");
            this.r.c(this.o.e());
            a(30);
        }
    }

    @DexIgnore
    public final void q() {
        FLogger.INSTANCE.getLocal().d(s, "stopSetConfigTimeOutTimer");
        this.j = false;
        this.i.removeCallbacks(this.k);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void r() {
        FLogger.INSTANCE.getLocal().d(s, "unregisterBroadcastReceiver()");
        try {
            BleCommandResultManager.d.b((BleCommandResultManager.b) this.l, CommunicateMode.ENTER_CALIBRATION, CommunicateMode.RESET_HAND, CommunicateMode.MOVE_HAND, CommunicateMode.APPLY_HAND_POSITION, CommunicateMode.EXIT_CALIBRATION);
            this.q.a(this.m);
            this.o.unregisterReceiver(this.n);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = s;
            local.d(str, "unregisterBroadcastReceiver() - ex = " + e2);
        }
    }

    @DexIgnore
    public final void a(boolean z, ArrayList<Integer> arrayList, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.e(str, "onEnterCalibrationComplete isSuccess=" + z + ", lastErrorCode=" + i2);
        if (z) {
            this.r.d(this.o.e());
            return;
        }
        this.p.a();
        if (i2 != 1101 && i2 != 1112 && i2 != 1113) {
            o();
        } else if (arrayList != null) {
            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(arrayList);
            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026onErrorCode(errorCodes!!)");
            hv5 hv5 = this.p;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
            if (array != null) {
                uh4[] uh4Arr = (uh4[]) array;
                hv5.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void b(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "startMove: clockwise = " + z);
        int i2 = FossilDeviceSerialPatternUtil.isDianaDevice(this.o.e()) ? 1 : 2;
        int i3 = this.f;
        if (i3 == 0) {
            this.r.a(z, this.o.e(), i2, CalibrationEnums.HandId.HOUR);
        } else if (i3 == 1) {
            this.r.a(z, this.o.e(), i2, CalibrationEnums.HandId.MINUTE);
        } else if (i3 == 2) {
            this.r.a(z, this.o.e(), i2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    public void c(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "startSmartMove: clockwise = " + z + ", mCalibrationStep = " + this.f);
        int i2 = this.f;
        if (i2 == 0) {
            this.r.b(z, this.o.e(), 2, CalibrationEnums.HandId.HOUR);
        } else if (i2 == 1) {
            this.r.b(z, this.o.e(), 2, CalibrationEnums.HandId.MINUTE);
        } else if (i2 == 2) {
            this.r.b(z, this.o.e(), 2, CalibrationEnums.HandId.SUB_EYE);
        }
    }

    @DexIgnore
    public final void d(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "onExitCalibrationComplete success=" + z);
        this.p.a();
        if (z) {
            this.p.o();
        } else {
            o();
        }
    }

    @DexIgnore
    public final void e(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "onMoveHandComplete isSuccess=" + z);
        if (!z) {
            this.r.a(this.o.e());
            o();
        }
    }

    @DexIgnore
    public final void f(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.e(str, "onResetHandComplete isSuccess=" + z);
        this.p.a();
        if (z) {
            q();
            this.h.start();
            return;
        }
        o();
    }

    @DexIgnore
    public final void a(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "onApplyHandsComplete isSuccess=" + z);
        if (z) {
            a("device_calibrate_result", "errorCode", "N/A");
            return;
        }
        a("device_calibrate_result", "errorCode", String.valueOf(i2));
        this.p.a();
        o();
    }

    @DexIgnore
    public final void a(int i2) {
        q();
        this.j = true;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "startSetConfigTimeOutTimer:  timeout = " + i2);
        this.i.postDelayed(this.k, ((long) i2) * 1000);
    }

    @DexIgnore
    public void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        AnalyticsHelper.f.c().b(str, str2, str3);
    }
}
