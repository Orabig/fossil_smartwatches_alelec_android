package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o35 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator CREATOR; // = new a();
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator {
        @DexIgnore
        public final Object createFromParcel(Parcel parcel) {
            wg6.b(parcel, "in");
            return new o35(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
        }

        @DexIgnore
        public final Object[] newArray(int i) {
            return new o35[i];
        }
    }

    @DexIgnore
    public o35() {
        this((String) null, (String) null, (String) null, (String) null, (String) null, 31, (qg6) null);
    }

    @DexIgnore
    public o35(String str, String str2, String str3, String str4, String str5) {
        wg6.b(str, "id");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof o35)) {
            return false;
        }
        o35 o35 = (o35) obj;
        return wg6.a((Object) this.a, (Object) o35.a) && wg6.a((Object) this.b, (Object) o35.b) && wg6.a((Object) this.c, (Object) o35.c) && wg6.a((Object) this.d, (Object) o35.d) && wg6.a((Object) this.e, (Object) o35.e);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "PresetConfig(id=" + this.a + ", icon=" + this.b + ", name=" + this.c + ", position=" + this.d + ", value=" + this.e + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ o35(String str, String str2, String str3, String str4, String str5, int i, qg6 qg6) {
        this(r11, (i & 2) != 0 ? "" : str2, (i & 4) != 0 ? "" : str3, (i & 8) != 0 ? "" : str4, (i & 16) != 0 ? "" : str5);
        String str6 = (i & 1) != 0 ? "" : str;
    }
}
