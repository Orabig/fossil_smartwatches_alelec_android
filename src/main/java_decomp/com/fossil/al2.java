package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class al2<T> extends cl2<T> {
    @DexIgnore
    public static /* final */ al2<Object> zza; // = new al2<>();

    @DexIgnore
    public final boolean equals(Object obj) {
        return obj == this;
    }

    @DexIgnore
    public final int hashCode() {
        return 2040732332;
    }

    @DexIgnore
    public final String toString() {
        return "Optional.absent()";
    }

    @DexIgnore
    public final boolean zza() {
        return false;
    }

    @DexIgnore
    public final T zzb() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }
}
