package com.fossil;

import java.io.IOException;
import java.io.Reader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class no3 {
    @DexIgnore
    public abstract Reader a() throws IOException;

    @DexIgnore
    public <T> T a(to3<T> to3) throws IOException {
        jk3.a(to3);
        qo3 k = qo3.k();
        try {
            Reader a = a();
            k.a(a);
            T a2 = oo3.a(a, to3);
            k.close();
            return a2;
        } catch (Throwable th) {
            k.close();
            throw th;
        }
    }
}
