package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.h26;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i26 implements h26.a {
    @DexIgnore
    public i26() {
        new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    public final int a() {
        return h26.a;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        if (h26.a <= 1) {
            Log.d(str, str2);
        }
    }

    @DexIgnore
    public final void b(String str, String str2) {
        if (h26.a <= 2) {
            Log.i(str, str2);
        }
    }

    @DexIgnore
    public final void c(String str, String str2) {
        if (h26.a <= 3) {
            Log.w(str, str2);
        }
    }

    @DexIgnore
    public final void i(String str, String str2) {
        if (h26.a <= 4) {
            Log.e(str, str2);
        }
    }
}
