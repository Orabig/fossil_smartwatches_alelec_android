package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xx6<T> implements fx6<zq6, T> {
    @DexIgnore
    public /* final */ Gson a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public xx6(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    public T a(zq6 zq6) throws IOException {
        JsonReader a2 = this.a.a(zq6.charStream());
        try {
            T read = this.b.read(a2);
            if (a2.N() == rv3.END_DOCUMENT) {
                return read;
            }
            throw new iu3("JSON document was not fully consumed.");
        } finally {
            zq6.close();
        }
    }
}
