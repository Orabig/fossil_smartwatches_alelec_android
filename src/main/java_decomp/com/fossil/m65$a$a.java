package com.fossil;

import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1$1", f = "ComplicationSearchPresenter.kt", l = {}, m = "invokeSuspend")
public final class m65$a$a extends sf6 implements ig6<il6, xe6<? super List<Complication>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationSearchPresenter.a this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m65$a$a(ComplicationSearchPresenter.a aVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = aVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        m65$a$a m65_a_a = new m65$a$a(this.this$0, xe6);
        m65_a_a.p$ = (il6) obj;
        return m65_a_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((m65$a$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return yd6.d(this.this$0.this$0.m.queryComplicationByName(this.this$0.$query));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
