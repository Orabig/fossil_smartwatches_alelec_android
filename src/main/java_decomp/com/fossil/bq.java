package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bq extends Exception {
    @DexIgnore
    public /* final */ rp networkResponse;
    @DexIgnore
    public long networkTimeMs;

    @DexIgnore
    public bq() {
        this.networkResponse = null;
    }

    @DexIgnore
    public long getNetworkTimeMs() {
        return this.networkTimeMs;
    }

    @DexIgnore
    public void setNetworkTimeMs(long j) {
        this.networkTimeMs = j;
    }

    @DexIgnore
    public bq(rp rpVar) {
        this.networkResponse = rpVar;
    }

    @DexIgnore
    public bq(String str) {
        super(str);
        this.networkResponse = null;
    }

    @DexIgnore
    public bq(String str, Throwable th) {
        super(str, th);
        this.networkResponse = null;
    }

    @DexIgnore
    public bq(Throwable th) {
        super(th);
        this.networkResponse = null;
    }
}
