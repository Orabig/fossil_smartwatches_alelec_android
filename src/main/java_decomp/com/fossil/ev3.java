package com.fossil;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ev3<K, V> extends AbstractMap<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
    @DexIgnore
    public static /* final */ Comparator<Comparable> a; // = new a();
    @DexIgnore
    public Comparator<? super K> comparator;
    @DexIgnore
    public ev3<K, V>.b entrySet;
    @DexIgnore
    public /* final */ e<K, V> header;
    @DexIgnore
    public ev3<K, V>.c keySet;
    @DexIgnore
    public int modCount;
    @DexIgnore
    public e<K, V> root;
    @DexIgnore
    public int size;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Comparator<Comparable> {
        @DexIgnore
        /* renamed from: a */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends AbstractSet<Map.Entry<K, V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ev3<K, V>.d<Map.Entry<K, V>> {
            @DexIgnore
            public a(b bVar) {
                super();
            }

            @DexIgnore
            public Map.Entry<K, V> next() {
                return a();
            }
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void clear() {
            ev3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && ev3.this.findByEntry((Map.Entry) obj) != null;
        }

        @DexIgnore
        public Iterator<Map.Entry<K, V>> iterator() {
            return new a(this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            e findByEntry;
            if (!(obj instanceof Map.Entry) || (findByEntry = ev3.this.findByEntry((Map.Entry) obj)) == null) {
                return false;
            }
            ev3.this.removeInternal(findByEntry, true);
            return true;
        }

        @DexIgnore
        public int size() {
            return ev3.this.size;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends AbstractSet<K> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ev3<K, V>.d<K> {
            @DexIgnore
            public a(c cVar) {
                super();
            }

            @DexIgnore
            public K next() {
                return a().f;
            }
        }

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void clear() {
            ev3.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return ev3.this.containsKey(obj);
        }

        @DexIgnore
        public Iterator<K> iterator() {
            return new a(this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return ev3.this.removeInternalByKey(obj) != null;
        }

        @DexIgnore
        public int size() {
            return ev3.this.size;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class d<T> implements Iterator<T> {
        @DexIgnore
        public e<K, V> a;
        @DexIgnore
        public e<K, V> b; // = null;
        @DexIgnore
        public int c;

        @DexIgnore
        public d() {
            ev3 ev3 = ev3.this;
            this.a = ev3.header.d;
            this.c = ev3.modCount;
        }

        @DexIgnore
        public final e<K, V> a() {
            e<K, V> eVar = this.a;
            ev3 ev3 = ev3.this;
            if (eVar == ev3.header) {
                throw new NoSuchElementException();
            } else if (ev3.modCount == this.c) {
                this.a = eVar.d;
                this.b = eVar;
                return eVar;
            } else {
                throw new ConcurrentModificationException();
            }
        }

        @DexIgnore
        public final boolean hasNext() {
            return this.a != ev3.this.header;
        }

        @DexIgnore
        public final void remove() {
            e<K, V> eVar = this.b;
            if (eVar != null) {
                ev3.this.removeInternal(eVar, true);
                this.b = null;
                this.c = ev3.this.modCount;
                return;
            }
            throw new IllegalStateException();
        }
    }

    /*
    static {
        Class<ev3> cls = ev3.class;
    }
    */

    @DexIgnore
    public ev3() {
        this(a);
    }

    @DexIgnore
    private Object writeReplace() throws ObjectStreamException {
        return new LinkedHashMap(this);
    }

    @DexIgnore
    public final boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    public final void b(e<K, V> eVar) {
        e<K, V> eVar2 = eVar.b;
        e<K, V> eVar3 = eVar.c;
        e<K, V> eVar4 = eVar2.b;
        e<K, V> eVar5 = eVar2.c;
        eVar.b = eVar5;
        if (eVar5 != null) {
            eVar5.a = eVar;
        }
        a(eVar, eVar2);
        eVar2.c = eVar;
        eVar.a = eVar2;
        int i = 0;
        eVar.h = Math.max(eVar3 != null ? eVar3.h : 0, eVar5 != null ? eVar5.h : 0) + 1;
        int i2 = eVar.h;
        if (eVar4 != null) {
            i = eVar4.h;
        }
        eVar2.h = Math.max(i2, i) + 1;
    }

    @DexIgnore
    public void clear() {
        this.root = null;
        this.size = 0;
        this.modCount++;
        e<K, V> eVar = this.header;
        eVar.e = eVar;
        eVar.d = eVar;
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return findByObject(obj) != null;
    }

    @DexIgnore
    public Set<Map.Entry<K, V>> entrySet() {
        ev3<K, V>.b bVar = this.entrySet;
        if (bVar != null) {
            return bVar;
        }
        ev3<K, V>.b bVar2 = new b();
        this.entrySet = bVar2;
        return bVar2;
    }

    @DexIgnore
    public e<K, V> find(K k, boolean z) {
        int i;
        e<K, V> eVar;
        Comparator<? super K> comparator2 = this.comparator;
        e<K, V> eVar2 = this.root;
        if (eVar2 != null) {
            Comparable comparable = comparator2 == a ? (Comparable) k : null;
            while (true) {
                if (comparable != null) {
                    i = comparable.compareTo(eVar2.f);
                } else {
                    i = comparator2.compare(k, eVar2.f);
                }
                if (i == 0) {
                    return eVar2;
                }
                e<K, V> eVar3 = i < 0 ? eVar2.b : eVar2.c;
                if (eVar3 == null) {
                    break;
                }
                eVar2 = eVar3;
            }
        } else {
            i = 0;
        }
        if (!z) {
            return null;
        }
        e<K, V> eVar4 = this.header;
        if (eVar2 != null) {
            eVar = new e<>(eVar2, k, eVar4, eVar4.e);
            if (i < 0) {
                eVar2.b = eVar;
            } else {
                eVar2.c = eVar;
            }
            a(eVar2, true);
        } else if (comparator2 != a || (k instanceof Comparable)) {
            eVar = new e<>(eVar2, k, eVar4, eVar4.e);
            this.root = eVar;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.size++;
        this.modCount++;
        return eVar;
    }

    @DexIgnore
    public e<K, V> findByEntry(Map.Entry<?, ?> entry) {
        e<K, V> findByObject = findByObject(entry.getKey());
        if (findByObject != null && a((Object) findByObject.g, (Object) entry.getValue())) {
            return findByObject;
        }
        return null;
    }

    @DexIgnore
    public e<K, V> findByObject(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return find(obj, false);
        } catch (ClassCastException unused) {
            return null;
        }
    }

    @DexIgnore
    public V get(Object obj) {
        e findByObject = findByObject(obj);
        if (findByObject != null) {
            return findByObject.g;
        }
        return null;
    }

    @DexIgnore
    public Set<K> keySet() {
        ev3<K, V>.c cVar = this.keySet;
        if (cVar != null) {
            return cVar;
        }
        ev3<K, V>.c cVar2 = new c();
        this.keySet = cVar2;
        return cVar2;
    }

    @DexIgnore
    public V put(K k, V v) {
        if (k != null) {
            e find = find(k, true);
            V v2 = find.g;
            find.g = v;
            return v2;
        }
        throw new NullPointerException("key == null");
    }

    @DexIgnore
    public V remove(Object obj) {
        e removeInternalByKey = removeInternalByKey(obj);
        if (removeInternalByKey != null) {
            return removeInternalByKey.g;
        }
        return null;
    }

    @DexIgnore
    public void removeInternal(e<K, V> eVar, boolean z) {
        int i;
        if (z) {
            e<K, V> eVar2 = eVar.e;
            eVar2.d = eVar.d;
            eVar.d.e = eVar2;
        }
        e<K, V> eVar3 = eVar.b;
        e<K, V> eVar4 = eVar.c;
        e<K, V> eVar5 = eVar.a;
        int i2 = 0;
        if (eVar3 == null || eVar4 == null) {
            if (eVar3 != null) {
                a(eVar, eVar3);
                eVar.b = null;
            } else if (eVar4 != null) {
                a(eVar, eVar4);
                eVar.c = null;
            } else {
                a(eVar, (e<K, V>) null);
            }
            a(eVar5, false);
            this.size--;
            this.modCount++;
            return;
        }
        e<K, V> b2 = eVar3.h > eVar4.h ? eVar3.b() : eVar4.a();
        removeInternal(b2, false);
        e<K, V> eVar6 = eVar.b;
        if (eVar6 != null) {
            i = eVar6.h;
            b2.b = eVar6;
            eVar6.a = b2;
            eVar.b = null;
        } else {
            i = 0;
        }
        e<K, V> eVar7 = eVar.c;
        if (eVar7 != null) {
            i2 = eVar7.h;
            b2.c = eVar7;
            eVar7.a = b2;
            eVar.c = null;
        }
        b2.h = Math.max(i, i2) + 1;
        a(eVar, b2);
    }

    @DexIgnore
    public e<K, V> removeInternalByKey(Object obj) {
        e<K, V> findByObject = findByObject(obj);
        if (findByObject != null) {
            removeInternal(findByObject, true);
        }
        return findByObject;
    }

    @DexIgnore
    public int size() {
        return this.size;
    }

    @DexIgnore
    public ev3(Comparator<? super K> comparator2) {
        this.size = 0;
        this.modCount = 0;
        this.header = new e<>();
        this.comparator = comparator2 == null ? a : comparator2;
    }

    @DexIgnore
    public final void a(e<K, V> eVar, e<K, V> eVar2) {
        e<K, V> eVar3 = eVar.a;
        eVar.a = null;
        if (eVar2 != null) {
            eVar2.a = eVar3;
        }
        if (eVar3 == null) {
            this.root = eVar2;
        } else if (eVar3.b == eVar) {
            eVar3.b = eVar2;
        } else {
            eVar3.c = eVar2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<K, V> implements Map.Entry<K, V> {
        @DexIgnore
        public e<K, V> a;
        @DexIgnore
        public e<K, V> b;
        @DexIgnore
        public e<K, V> c;
        @DexIgnore
        public e<K, V> d;
        @DexIgnore
        public e<K, V> e;
        @DexIgnore
        public /* final */ K f;
        @DexIgnore
        public V g;
        @DexIgnore
        public int h;

        @DexIgnore
        public e() {
            this.f = null;
            this.e = this;
            this.d = this;
        }

        @DexIgnore
        public e<K, V> a() {
            e<K, V> eVar = this;
            for (e<K, V> eVar2 = this.b; eVar2 != null; eVar2 = eVar2.b) {
                eVar = eVar2;
            }
            return eVar;
        }

        @DexIgnore
        public e<K, V> b() {
            e<K, V> eVar = this;
            for (e<K, V> eVar2 = this.c; eVar2 != null; eVar2 = eVar2.c) {
                eVar = eVar2;
            }
            return eVar;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0031 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        public boolean equals(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            K k = this.f;
            if (k == null) {
                if (entry.getKey() != null) {
                    return false;
                }
            } else if (!k.equals(entry.getKey())) {
                return false;
            }
            V v = this.g;
            if (v == null) {
                if (entry.getValue() == null) {
                    return true;
                }
                return false;
            } else if (!v.equals(entry.getValue())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public K getKey() {
            return this.f;
        }

        @DexIgnore
        public V getValue() {
            return this.g;
        }

        @DexIgnore
        public int hashCode() {
            K k = this.f;
            int i = 0;
            int hashCode = k == null ? 0 : k.hashCode();
            V v = this.g;
            if (v != null) {
                i = v.hashCode();
            }
            return hashCode ^ i;
        }

        @DexIgnore
        public V setValue(V v) {
            V v2 = this.g;
            this.g = v;
            return v2;
        }

        @DexIgnore
        public String toString() {
            return this.f + "=" + this.g;
        }

        @DexIgnore
        public e(e<K, V> eVar, K k, e<K, V> eVar2, e<K, V> eVar3) {
            this.a = eVar;
            this.f = k;
            this.h = 1;
            this.d = eVar2;
            this.e = eVar3;
            eVar3.d = this;
            eVar2.e = this;
        }
    }

    @DexIgnore
    public final void a(e<K, V> eVar, boolean z) {
        while (eVar != null) {
            e<K, V> eVar2 = eVar.b;
            e<K, V> eVar3 = eVar.c;
            int i = 0;
            int i2 = eVar2 != null ? eVar2.h : 0;
            int i3 = eVar3 != null ? eVar3.h : 0;
            int i4 = i2 - i3;
            if (i4 == -2) {
                e<K, V> eVar4 = eVar3.b;
                e<K, V> eVar5 = eVar3.c;
                int i5 = eVar5 != null ? eVar5.h : 0;
                if (eVar4 != null) {
                    i = eVar4.h;
                }
                int i6 = i - i5;
                if (i6 == -1 || (i6 == 0 && !z)) {
                    a(eVar);
                } else {
                    b(eVar3);
                    a(eVar);
                }
                if (z) {
                    return;
                }
            } else if (i4 == 2) {
                e<K, V> eVar6 = eVar2.b;
                e<K, V> eVar7 = eVar2.c;
                int i7 = eVar7 != null ? eVar7.h : 0;
                if (eVar6 != null) {
                    i = eVar6.h;
                }
                int i8 = i - i7;
                if (i8 == 1 || (i8 == 0 && !z)) {
                    b(eVar);
                } else {
                    a(eVar2);
                    b(eVar);
                }
                if (z) {
                    return;
                }
            } else if (i4 == 0) {
                eVar.h = i2 + 1;
                if (z) {
                    return;
                }
            } else {
                eVar.h = Math.max(i2, i3) + 1;
                if (!z) {
                    return;
                }
            }
            eVar = eVar.a;
        }
    }

    @DexIgnore
    public final void a(e<K, V> eVar) {
        e<K, V> eVar2 = eVar.b;
        e<K, V> eVar3 = eVar.c;
        e<K, V> eVar4 = eVar3.b;
        e<K, V> eVar5 = eVar3.c;
        eVar.c = eVar4;
        if (eVar4 != null) {
            eVar4.a = eVar;
        }
        a(eVar, eVar3);
        eVar3.b = eVar;
        eVar.a = eVar3;
        int i = 0;
        eVar.h = Math.max(eVar2 != null ? eVar2.h : 0, eVar4 != null ? eVar4.h : 0) + 1;
        int i2 = eVar.h;
        if (eVar5 != null) {
            i = eVar5.h;
        }
        eVar3.h = Math.max(i2, i) + 1;
    }
}
