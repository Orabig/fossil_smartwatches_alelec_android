package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class j92 {
    @DexIgnore
    public static Map<String, j92> b; // = new p4();
    @DexIgnore
    public static r92 c;
    @DexIgnore
    public String a; // = "";

    /*
    static {
        p92.a().a("gcm_check_for_different_iid_in_token", true);
        TimeUnit.DAYS.toMillis(7);
    }
    */

    @DexIgnore
    public j92(Context context, String str) {
        context.getApplicationContext();
        this.a = str;
    }

    @DexIgnore
    public static int a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 38);
            sb.append("Never happens: can't find own package ");
            sb.append(valueOf);
            Log.w("InstanceID", sb.toString());
            return 0;
        }
    }

    @DexIgnore
    public static r92 b() {
        return c;
    }

    @DexIgnore
    public static synchronized j92 a(Context context, Bundle bundle) {
        j92 j92;
        synchronized (j92.class) {
            String string = bundle == null ? "" : bundle.getString("subtype");
            if (string == null) {
                string = "";
            }
            Context applicationContext = context.getApplicationContext();
            if (c == null) {
                String packageName = applicationContext.getPackageName();
                StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 73);
                sb.append("Instance ID SDK is deprecated, ");
                sb.append(packageName);
                sb.append(" should update to use Firebase Instance ID");
                Log.w("InstanceID", sb.toString());
                c = new r92(applicationContext);
                new o92(applicationContext);
            }
            Integer.toString(a(applicationContext));
            j92 = b.get(string);
            if (j92 == null) {
                j92 = new j92(applicationContext, string);
                b.put(string, j92);
            }
        }
        return j92;
    }

    @DexIgnore
    public final void a() {
        c.b(this.a);
    }
}
