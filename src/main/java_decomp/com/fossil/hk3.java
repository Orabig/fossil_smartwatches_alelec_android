package com.fossil;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hk3<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Iterable<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hk3$a$a")
        /* renamed from: com.fossil.hk3$a$a  reason: collision with other inner class name */
        public class C0020a extends yj3<T> {
            @DexIgnore
            public /* final */ Iterator<? extends hk3<? extends T>> c;

            @DexIgnore
            public C0020a() {
                Iterator<? extends hk3<? extends T>> it = a.this.a.iterator();
                jk3.a(it);
                this.c = it;
            }

            @DexIgnore
            public T a() {
                while (this.c.hasNext()) {
                    hk3 hk3 = (hk3) this.c.next();
                    if (hk3.isPresent()) {
                        return hk3.get();
                    }
                }
                return b();
            }
        }

        @DexIgnore
        public a(Iterable iterable) {
            this.a = iterable;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return new C0020a();
        }
    }

    @DexIgnore
    public static <T> hk3<T> absent() {
        return xj3.withType();
    }

    @DexIgnore
    public static <T> hk3<T> fromNullable(T t) {
        return t == null ? absent() : new mk3(t);
    }

    @DexIgnore
    public static <T> hk3<T> of(T t) {
        jk3.a(t);
        return new mk3(t);
    }

    @DexIgnore
    public static <T> Iterable<T> presentInstances(Iterable<? extends hk3<? extends T>> iterable) {
        jk3.a(iterable);
        return new a(iterable);
    }

    @DexIgnore
    public abstract Set<T> asSet();

    @DexIgnore
    public abstract boolean equals(Object obj);

    @DexIgnore
    public abstract T get();

    @DexIgnore
    public abstract int hashCode();

    @DexIgnore
    public abstract boolean isPresent();

    @DexIgnore
    public abstract hk3<T> or(hk3<? extends T> hk3);

    @DexIgnore
    public abstract T or(nk3<? extends T> nk3);

    @DexIgnore
    public abstract T or(T t);

    @DexIgnore
    public abstract T orNull();

    @DexIgnore
    public abstract String toString();

    @DexIgnore
    public abstract <V> hk3<V> transform(ck3<? super T, V> ck3);
}
