package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.rv1;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b02 implements wy1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ay1 b;
    @DexIgnore
    public /* final */ Looper c;
    @DexIgnore
    public /* final */ jy1 d;
    @DexIgnore
    public /* final */ jy1 e;
    @DexIgnore
    public /* final */ Map<rv1.c<?>, jy1> f;
    @DexIgnore
    public /* final */ Set<yw1> g; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ rv1.f h;
    @DexIgnore
    public Bundle i;
    @DexIgnore
    public gv1 j; // = null;
    @DexIgnore
    public gv1 o; // = null;
    @DexIgnore
    public boolean p; // = false;
    @DexIgnore
    public /* final */ Lock q;
    @DexIgnore
    public int r; // = 0;

    @DexIgnore
    public b02(Context context, ay1 ay1, Lock lock, Looper looper, kv1 kv1, Map<rv1.c<?>, rv1.f> map, Map<rv1.c<?>, rv1.f> map2, e12 e12, rv1.a<? extends ac3, lb3> aVar, rv1.f fVar, ArrayList<a02> arrayList, ArrayList<a02> arrayList2, Map<rv1<?>, Boolean> map3, Map<rv1<?>, Boolean> map4) {
        this.a = context;
        this.b = ay1;
        this.q = lock;
        this.c = looper;
        this.h = fVar;
        Context context2 = context;
        Lock lock2 = lock;
        Looper looper2 = looper;
        kv1 kv12 = kv1;
        jy1 jy1 = r3;
        jy1 jy12 = new jy1(context2, this.b, lock2, looper2, kv12, map2, (e12) null, map4, (rv1.a<? extends ac3, lb3>) null, arrayList2, new d02(this, (e02) null));
        this.d = jy1;
        this.e = new jy1(context2, this.b, lock2, looper2, kv12, map, e12, map3, aVar, arrayList, new f02(this, (e02) null));
        p4 p4Var = new p4();
        for (rv1.c<?> put : map2.keySet()) {
            p4Var.put(put, this.d);
        }
        for (rv1.c<?> put2 : map.keySet()) {
            p4Var.put(put2, this.e);
        }
        this.f = Collections.unmodifiableMap(p4Var);
    }

    @DexIgnore
    public static b02 a(Context context, ay1 ay1, Lock lock, Looper looper, kv1 kv1, Map<rv1.c<?>, rv1.f> map, e12 e12, Map<rv1<?>, Boolean> map2, rv1.a<? extends ac3, lb3> aVar, ArrayList<a02> arrayList) {
        Map<rv1<?>, Boolean> map3 = map2;
        p4 p4Var = new p4();
        p4 p4Var2 = new p4();
        rv1.f fVar = null;
        for (Map.Entry next : map.entrySet()) {
            rv1.f fVar2 = (rv1.f) next.getValue();
            if (fVar2.d()) {
                fVar = fVar2;
            }
            if (fVar2.m()) {
                p4Var.put((rv1.c) next.getKey(), fVar2);
            } else {
                p4Var2.put((rv1.c) next.getKey(), fVar2);
            }
        }
        w12.b(!p4Var.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        p4 p4Var3 = new p4();
        p4 p4Var4 = new p4();
        for (rv1 next2 : map2.keySet()) {
            rv1.c<?> a2 = next2.a();
            if (p4Var.containsKey(a2)) {
                p4Var3.put(next2, map3.get(next2));
            } else if (p4Var2.containsKey(a2)) {
                p4Var4.put(next2, map3.get(next2));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            a02 a02 = arrayList.get(i2);
            i2++;
            a02 a022 = a02;
            if (p4Var3.containsKey(a022.a)) {
                arrayList2.add(a022);
            } else if (p4Var4.containsKey(a022.a)) {
                arrayList3.add(a022);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new b02(context, ay1, lock, looper, kv1, p4Var, p4Var2, e12, aVar, fVar, arrayList2, arrayList3, p4Var3, p4Var4);
    }

    @DexIgnore
    public final <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T b(T t) {
        if (!c((nw1<? extends ew1, ? extends rv1.b>) t)) {
            return this.d.b(t);
        }
        if (!j()) {
            return this.e.b(t);
        }
        t.c(new Status(4, (String) null, k()));
        return t;
    }

    @DexIgnore
    public final boolean c() {
        this.q.lock();
        try {
            boolean z = true;
            if (!this.d.c() || (!this.e.c() && !j() && this.r != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final void d() {
        this.q.lock();
        try {
            boolean g2 = g();
            this.e.a();
            this.o = new gv1(4);
            if (g2) {
                new bb2(this.c).post(new e02(this));
            } else {
                i();
            }
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final void e() {
        this.d.e();
        this.e.e();
    }

    @DexIgnore
    public final gv1 f() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final boolean g() {
        this.q.lock();
        try {
            return this.r == 2;
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final void h() {
        gv1 gv1;
        if (b(this.j)) {
            if (b(this.o) || j()) {
                int i2 = this.r;
                if (i2 != 1) {
                    if (i2 != 2) {
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        this.r = 0;
                        return;
                    }
                    this.b.a(this.i);
                }
                i();
                this.r = 0;
                return;
            }
            gv1 gv12 = this.o;
            if (gv12 == null) {
                return;
            }
            if (this.r == 1) {
                i();
                return;
            }
            a(gv12);
            this.d.a();
        } else if (this.j == null || !b(this.o)) {
            gv1 gv13 = this.j;
            if (gv13 != null && (gv1 = this.o) != null) {
                if (this.e.q < this.d.q) {
                    gv13 = gv1;
                }
                a(gv13);
            }
        } else {
            this.e.a();
            a(this.j);
        }
    }

    @DexIgnore
    public final void i() {
        for (yw1 onComplete : this.g) {
            onComplete.onComplete();
        }
        this.g.clear();
    }

    @DexIgnore
    public final boolean j() {
        gv1 gv1 = this.o;
        return gv1 != null && gv1.p() == 4;
    }

    @DexIgnore
    public final PendingIntent k() {
        if (this.h == null) {
            return null;
        }
        return PendingIntent.getActivity(this.a, System.identityHashCode(this.b), this.h.l(), 134217728);
    }

    @DexIgnore
    public final boolean c(nw1<? extends ew1, ? extends rv1.b> nw1) {
        rv1.c<? extends rv1.b> i2 = nw1.i();
        w12.a(this.f.containsKey(i2), (Object) "GoogleApiClient is not configured to use the API required for this call.");
        return this.f.get(i2).equals(this.e);
    }

    @DexIgnore
    public final void b() {
        this.r = 2;
        this.p = false;
        this.o = null;
        this.j = null;
        this.d.b();
        this.e.b();
    }

    @DexIgnore
    public static boolean b(gv1 gv1) {
        return gv1 != null && gv1.E();
    }

    @DexIgnore
    public final <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(T t) {
        if (!c((nw1<? extends ew1, ? extends rv1.b>) t)) {
            return this.d.a(t);
        }
        if (!j()) {
            return this.e.a(t);
        }
        t.c(new Status(4, (String) null, k()));
        return t;
    }

    @DexIgnore
    public final void a() {
        this.o = null;
        this.j = null;
        this.r = 0;
        this.d.a();
        this.e.a();
        i();
    }

    @DexIgnore
    public final boolean a(yw1 yw1) {
        this.q.lock();
        try {
            if ((g() || c()) && !this.e.c()) {
                this.g.add(yw1);
                if (this.r == 0) {
                    this.r = 1;
                }
                this.o = null;
                this.e.b();
                return true;
            }
            this.q.unlock();
            return false;
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        int i2 = this.r;
        if (i2 != 1) {
            if (i2 != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.r = 0;
            }
            this.b.a(gv1);
        }
        i();
        this.r = 0;
    }

    @DexIgnore
    public final void a(int i2, boolean z) {
        this.b.a(i2, z);
        this.o = null;
        this.j = null;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        Bundle bundle2 = this.i;
        if (bundle2 == null) {
            this.i = bundle;
        } else if (bundle != null) {
            bundle2.putAll(bundle);
        }
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("authClient").println(":");
        this.e.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append(str).append("anonClient").println(":");
        this.d.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }
}
