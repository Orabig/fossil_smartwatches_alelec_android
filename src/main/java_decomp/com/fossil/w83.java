package com.fossil;

import android.os.RemoteException;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ j03 c;
    @DexIgnore
    public /* final */ /* synthetic */ ra3 d;
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ l83 f;

    @DexIgnore
    public w83(l83 l83, boolean z, boolean z2, j03 j03, ra3 ra3, String str) {
        this.f = l83;
        this.a = z;
        this.b = z2;
        this.c = j03;
        this.d = ra3;
        this.e = str;
    }

    @DexIgnore
    public final void run() {
        l43 d2 = this.f.d;
        if (d2 == null) {
            this.f.b().t().a("Discarding data. Failed to send event to service");
            return;
        }
        if (this.a) {
            this.f.a(d2, (e22) this.b ? null : this.c, this.d);
        } else {
            try {
                if (TextUtils.isEmpty(this.e)) {
                    d2.a(this.c, this.d);
                } else {
                    d2.a(this.c, this.e, this.f.b().C());
                }
            } catch (RemoteException e2) {
                this.f.b().t().a("Failed to send event to the service", e2);
            }
        }
        this.f.I();
    }
}
