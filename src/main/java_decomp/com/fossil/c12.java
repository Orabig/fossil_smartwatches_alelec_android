package com.fossil;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.fossil.k12;
import com.fossil.p12;
import com.google.android.gms.common.api.Scope;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class c12<T extends IInterface> {
    @DexIgnore
    public static /* final */ iv1[] A; // = new iv1[0];
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;
    @DexIgnore
    public int d;
    @DexIgnore
    public long e;
    @DexIgnore
    public p32 f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ k12 h;
    @DexIgnore
    public /* final */ kv1 i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ Object k;
    @DexIgnore
    public /* final */ Object l;
    @DexIgnore
    public r12 m;
    @DexIgnore
    public c n;
    @DexIgnore
    public T o;
    @DexIgnore
    public /* final */ ArrayList<h<?>> p;
    @DexIgnore
    public j q;
    @DexIgnore
    public int r;
    @DexIgnore
    public /* final */ a s;
    @DexIgnore
    public /* final */ b t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ String v;
    @DexIgnore
    public gv1 w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public volatile j32 y;
    @DexIgnore
    public AtomicInteger z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void f(Bundle bundle);

        @DexIgnore
        void g(int i);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(gv1 gv1);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(gv1 gv1);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements c {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void a(gv1 gv1) {
            if (gv1.E()) {
                c12 c12 = c12.this;
                c12.a((n12) null, c12.x());
            } else if (c12.this.t != null) {
                c12.this.t.a(gv1);
            }
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class f extends h<Boolean> {
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ Bundle e;

        @DexIgnore
        public f(int i, Bundle bundle) {
            super(true);
            this.d = i;
            this.e = bundle;
        }

        @DexIgnore
        public abstract void a(gv1 gv1);

        @DexIgnore
        /* JADX WARNING: type inference failed for: r5v11, types: [android.os.Parcelable] */
        /* JADX WARNING: Multi-variable type inference failed */
        public final /* synthetic */ void a(Object obj) {
            PendingIntent pendingIntent = null;
            if (((Boolean) obj) == null) {
                c12.this.b(1, null);
                return;
            }
            int i = this.d;
            if (i != 0) {
                if (i != 10) {
                    c12.this.b(1, null);
                    Bundle bundle = this.e;
                    if (bundle != null) {
                        pendingIntent = bundle.getParcelable("pendingIntent");
                    }
                    a(new gv1(this.d, pendingIntent));
                    return;
                }
                c12.this.b(1, null);
                throw new IllegalStateException(String.format("A fatal developer error has occurred. Class name: %s. Start service action: %s. Service Descriptor: %s. ", new Object[]{getClass().getSimpleName(), c12.this.A(), c12.this.z()}));
            } else if (!e()) {
                c12.this.b(1, null);
                a(new gv1(8, (PendingIntent) null));
            }
        }

        @DexIgnore
        public final void c() {
        }

        @DexIgnore
        public abstract boolean e();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends fb2 {
        @DexIgnore
        public g(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public static void a(Message message) {
            h hVar = (h) message.obj;
            hVar.c();
            hVar.b();
        }

        @DexIgnore
        public static boolean b(Message message) {
            int i = message.what;
            return i == 2 || i == 1 || i == 7;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: android.app.PendingIntent} */
        /* JADX WARNING: Multi-variable type inference failed */
        public final void handleMessage(Message message) {
            gv1 gv1;
            gv1 gv12;
            if (c12.this.z.get() == message.arg1) {
                int i = message.what;
                if ((i == 1 || i == 7 || ((i == 4 && !c12.this.r()) || message.what == 5)) && !c12.this.f()) {
                    a(message);
                    return;
                }
                int i2 = message.what;
                PendingIntent pendingIntent = null;
                if (i2 == 4) {
                    gv1 unused = c12.this.w = new gv1(message.arg2);
                    if (!c12.this.F() || c12.this.x) {
                        if (c12.this.w != null) {
                            gv12 = c12.this.w;
                        } else {
                            gv12 = new gv1(8);
                        }
                        c12.this.n.a(gv12);
                        c12.this.a(gv12);
                        return;
                    }
                    c12.this.b(3, null);
                } else if (i2 == 5) {
                    if (c12.this.w != null) {
                        gv1 = c12.this.w;
                    } else {
                        gv1 = new gv1(8);
                    }
                    c12.this.n.a(gv1);
                    c12.this.a(gv1);
                } else if (i2 == 3) {
                    Object obj = message.obj;
                    if (obj instanceof PendingIntent) {
                        pendingIntent = obj;
                    }
                    gv1 gv13 = new gv1(message.arg2, pendingIntent);
                    c12.this.n.a(gv13);
                    c12.this.a(gv13);
                } else if (i2 == 6) {
                    c12.this.b(5, null);
                    if (c12.this.s != null) {
                        c12.this.s.g(message.arg2);
                    }
                    c12.this.a(message.arg2);
                    boolean unused2 = c12.this.a(5, 1, null);
                } else if (i2 == 2 && !c12.this.c()) {
                    a(message);
                } else if (b(message)) {
                    ((h) message.obj).d();
                } else {
                    int i3 = message.what;
                    StringBuilder sb = new StringBuilder(45);
                    sb.append("Don't know how to handle message: ");
                    sb.append(i3);
                    Log.wtf("GmsClient", sb.toString(), new Exception());
                }
            } else if (b(message)) {
                a(message);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class h<TListener> {
        @DexIgnore
        public TListener a;
        @DexIgnore
        public boolean b; // = false;

        @DexIgnore
        public h(TListener tlistener) {
            this.a = tlistener;
        }

        @DexIgnore
        public final void a() {
            synchronized (this) {
                this.a = null;
            }
        }

        @DexIgnore
        public abstract void a(TListener tlistener);

        @DexIgnore
        public final void b() {
            a();
            synchronized (c12.this.p) {
                c12.this.p.remove(this);
            }
        }

        @DexIgnore
        public abstract void c();

        @DexIgnore
        public final void d() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.a;
                if (this.b) {
                    String valueOf = String.valueOf(this);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Callback proxy ");
                    sb.append(valueOf);
                    sb.append(" being reused. This is not safe.");
                    Log.w("GmsClient", sb.toString());
                }
            }
            if (tlistener != null) {
                try {
                    a(tlistener);
                } catch (RuntimeException e) {
                    c();
                    throw e;
                }
            } else {
                c();
            }
            synchronized (this) {
                this.b = true;
            }
            b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j implements ServiceConnection {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public j(int i) {
            this.a = i;
        }

        @DexIgnore
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            r12 r12;
            if (iBinder == null) {
                c12.this.c(16);
                return;
            }
            synchronized (c12.this.l) {
                c12 c12 = c12.this;
                if (iBinder == null) {
                    r12 = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    if (queryLocalInterface == null || !(queryLocalInterface instanceof r12)) {
                        r12 = new q12(iBinder);
                    } else {
                        r12 = (r12) queryLocalInterface;
                    }
                }
                r12 unused = c12.m = r12;
            }
            c12.this.a(0, (Bundle) null, this.a);
        }

        @DexIgnore
        public final void onServiceDisconnected(ComponentName componentName) {
            synchronized (c12.this.l) {
                r12 unused = c12.this.m = null;
            }
            Handler handler = c12.this.j;
            handler.sendMessage(handler.obtainMessage(6, this.a, 1));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k extends f {
        @DexIgnore
        public /* final */ IBinder g;

        @DexIgnore
        public k(int i, IBinder iBinder, Bundle bundle) {
            super(i, bundle);
            this.g = iBinder;
        }

        @DexIgnore
        public final void a(gv1 gv1) {
            if (c12.this.t != null) {
                c12.this.t.a(gv1);
            }
            c12.this.a(gv1);
        }

        @DexIgnore
        public final boolean e() {
            try {
                String interfaceDescriptor = this.g.getInterfaceDescriptor();
                if (!c12.this.z().equals(interfaceDescriptor)) {
                    String z = c12.this.z();
                    StringBuilder sb = new StringBuilder(String.valueOf(z).length() + 34 + String.valueOf(interfaceDescriptor).length());
                    sb.append("service descriptor mismatch: ");
                    sb.append(z);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    Log.e("GmsClient", sb.toString());
                    return false;
                }
                IInterface a = c12.this.a(this.g);
                if (a == null || (!c12.this.a(2, 4, a) && !c12.this.a(3, 4, a))) {
                    return false;
                }
                gv1 unused = c12.this.w = null;
                Bundle o = c12.this.o();
                if (c12.this.s == null) {
                    return true;
                }
                c12.this.s.f(o);
                return true;
            } catch (RemoteException unused2) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l extends f {
        @DexIgnore
        public l(int i, Bundle bundle) {
            super(i, (Bundle) null);
        }

        @DexIgnore
        public final void a(gv1 gv1) {
            if (!c12.this.r() || !c12.this.F()) {
                c12.this.n.a(gv1);
                c12.this.a(gv1);
                return;
            }
            c12.this.c(16);
        }

        @DexIgnore
        public final boolean e() {
            c12.this.n.a(gv1.e);
            return true;
        }
    }

    /*
    static {
        new String[]{"service_esmobile", "service_googleme"};
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public c12(Context context, Looper looper, int i2, a aVar, b bVar, String str) {
        this(context, looper, r3, r4, i2, aVar, bVar, str);
        k12 a2 = k12.a(context);
        kv1 a3 = kv1.a();
        w12.a(aVar);
        w12.a(bVar);
    }

    @DexIgnore
    public abstract String A();

    @DexIgnore
    public String B() {
        return "com.google.android.gms";
    }

    @DexIgnore
    public boolean C() {
        return false;
    }

    @DexIgnore
    public final String D() {
        String str = this.v;
        return str == null ? this.g.getClass().getName() : str;
    }

    @DexIgnore
    public final boolean E() {
        boolean z2;
        synchronized (this.k) {
            z2 = this.r == 3;
        }
        return z2;
    }

    @DexIgnore
    public final boolean F() {
        if (this.x || TextUtils.isEmpty(z()) || TextUtils.isEmpty(w())) {
            return false;
        }
        try {
            Class.forName(z());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public abstract T a(IBinder iBinder);

    @DexIgnore
    public void a(int i2, T t2) {
    }

    @DexIgnore
    public final void a(j32 j32) {
        this.y = j32;
    }

    @DexIgnore
    public final void b(int i2, T t2) {
        p32 p32;
        w12.a((i2 == 4) == (t2 != null));
        synchronized (this.k) {
            this.r = i2;
            this.o = t2;
            a(i2, t2);
            if (i2 != 1) {
                if (i2 == 2 || i2 == 3) {
                    if (!(this.q == null || this.f == null)) {
                        String c2 = this.f.c();
                        String a2 = this.f.a();
                        StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 70 + String.valueOf(a2).length());
                        sb.append("Calling connect() while still connected, missing disconnect() for ");
                        sb.append(c2);
                        sb.append(" on ");
                        sb.append(a2);
                        Log.e("GmsClient", sb.toString());
                        this.h.a(this.f.c(), this.f.a(), this.f.b(), this.q, D());
                        this.z.incrementAndGet();
                    }
                    this.q = new j(this.z.get());
                    if (this.r != 3 || w() == null) {
                        p32 = new p32(B(), A(), false, 129);
                    } else {
                        p32 = new p32(u().getPackageName(), w(), true, 129);
                    }
                    this.f = p32;
                    if (!this.h.a(new k12.a(this.f.c(), this.f.a(), this.f.b()), this.q, D())) {
                        String c3 = this.f.c();
                        String a3 = this.f.a();
                        StringBuilder sb2 = new StringBuilder(String.valueOf(c3).length() + 34 + String.valueOf(a3).length());
                        sb2.append("unable to connect to service: ");
                        sb2.append(c3);
                        sb2.append(" on ");
                        sb2.append(a3);
                        Log.e("GmsClient", sb2.toString());
                        a(16, (Bundle) null, this.z.get());
                    }
                } else if (i2 == 4) {
                    a(t2);
                }
            } else if (this.q != null) {
                this.h.a(this.f.c(), this.f.a(), this.f.b(), this.q, D());
                this.q = null;
            }
        }
    }

    @DexIgnore
    public boolean c() {
        boolean z2;
        synchronized (this.k) {
            z2 = this.r == 4;
        }
        return z2;
    }

    @DexIgnore
    public boolean d() {
        return false;
    }

    @DexIgnore
    public boolean f() {
        boolean z2;
        synchronized (this.k) {
            if (this.r != 2) {
                if (this.r != 3) {
                    z2 = false;
                }
            }
            z2 = true;
        }
        return z2;
    }

    @DexIgnore
    public String g() {
        p32 p32;
        if (c() && (p32 = this.f) != null) {
            return p32.a();
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    @DexIgnore
    public boolean i() {
        return true;
    }

    @DexIgnore
    public int j() {
        return kv1.a;
    }

    @DexIgnore
    public final iv1[] k() {
        j32 j32 = this.y;
        if (j32 == null) {
            return null;
        }
        return j32.b;
    }

    @DexIgnore
    public Intent l() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    @DexIgnore
    public boolean m() {
        return false;
    }

    @DexIgnore
    public IBinder n() {
        synchronized (this.l) {
            if (this.m == null) {
                return null;
            }
            IBinder asBinder = this.m.asBinder();
            return asBinder;
        }
    }

    @DexIgnore
    public Bundle o() {
        return null;
    }

    @DexIgnore
    public void p() {
        int a2 = this.i.a(this.g, j());
        if (a2 != 0) {
            b(1, (IInterface) null);
            a((c) new d(), a2, (PendingIntent) null);
            return;
        }
        a((c) new d());
    }

    @DexIgnore
    public final void q() {
        if (!c()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    @DexIgnore
    public boolean r() {
        return false;
    }

    @DexIgnore
    public Account s() {
        return null;
    }

    @DexIgnore
    public iv1[] t() {
        return A;
    }

    @DexIgnore
    public final Context u() {
        return this.g;
    }

    @DexIgnore
    public Bundle v() {
        return new Bundle();
    }

    @DexIgnore
    public String w() {
        return null;
    }

    @DexIgnore
    public Set<Scope> x() {
        return Collections.EMPTY_SET;
    }

    @DexIgnore
    public final T y() throws DeadObjectException {
        T t2;
        synchronized (this.k) {
            if (this.r != 5) {
                q();
                w12.b(this.o != null, "Client is connected but service is null");
                t2 = this.o;
            } else {
                throw new DeadObjectException();
            }
        }
        return t2;
    }

    @DexIgnore
    public abstract String z();

    @DexIgnore
    public void a(T t2) {
        this.c = System.currentTimeMillis();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends p12.a {
        @DexIgnore
        public c12 a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public i(c12 c12, int i) {
            this.a = c12;
            this.b = i;
        }

        @DexIgnore
        public final void a(int i, IBinder iBinder, Bundle bundle) {
            w12.a(this.a, (Object) "onPostInitComplete can be called only once per call to getRemoteService");
            this.a.a(i, iBinder, bundle, this.b);
            this.a = null;
        }

        @DexIgnore
        public final void b(int i, Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        }

        @DexIgnore
        public final void a(int i, IBinder iBinder, j32 j32) {
            w12.a(this.a, (Object) "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            w12.a(j32);
            this.a.a(j32);
            a(i, iBinder, j32.a);
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.a = i2;
        this.b = System.currentTimeMillis();
    }

    @DexIgnore
    public final void c(int i2) {
        int i3;
        if (E()) {
            i3 = 5;
            this.x = true;
        } else {
            i3 = 4;
        }
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(i3, this.z.get(), 16));
    }

    @DexIgnore
    public void a(gv1 gv1) {
        this.d = gv1.p();
        this.e = System.currentTimeMillis();
    }

    @DexIgnore
    public c12(Context context, Looper looper, k12 k12, kv1 kv1, int i2, a aVar, b bVar, String str) {
        this.k = new Object();
        this.l = new Object();
        this.p = new ArrayList<>();
        this.r = 1;
        this.w = null;
        this.x = false;
        this.y = null;
        this.z = new AtomicInteger(0);
        w12.a(context, (Object) "Context must not be null");
        this.g = context;
        w12.a(looper, (Object) "Looper must not be null");
        Looper looper2 = looper;
        w12.a(k12, (Object) "Supervisor must not be null");
        this.h = k12;
        w12.a(kv1, (Object) "API availability must not be null");
        this.i = kv1;
        this.j = new g(looper);
        this.u = i2;
        this.s = aVar;
        this.t = bVar;
        this.v = str;
    }

    @DexIgnore
    public final boolean a(int i2, int i3, T t2) {
        synchronized (this.k) {
            if (this.r != i2) {
                return false;
            }
            b(i3, t2);
            return true;
        }
    }

    @DexIgnore
    public void a(c cVar) {
        w12.a(cVar, (Object) "Connection progress callbacks cannot be null.");
        this.n = cVar;
        b(2, (IInterface) null);
    }

    @DexIgnore
    public void a() {
        this.z.incrementAndGet();
        synchronized (this.p) {
            int size = this.p.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.p.get(i2).a();
            }
            this.p.clear();
        }
        synchronized (this.l) {
            this.m = null;
        }
        b(1, (IInterface) null);
    }

    @DexIgnore
    public void a(c cVar, int i2, PendingIntent pendingIntent) {
        w12.a(cVar, (Object) "Connection progress callbacks cannot be null.");
        this.n = cVar;
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(3, this.z.get(), i2, pendingIntent));
    }

    @DexIgnore
    public void a(int i2, IBinder iBinder, Bundle bundle, int i3) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(1, i3, -1, new k(i2, iBinder, bundle)));
    }

    @DexIgnore
    public final void a(int i2, Bundle bundle, int i3) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(7, i3, -1, new l(i2, (Bundle) null)));
    }

    @DexIgnore
    public void a(n12 n12, Set<Scope> set) {
        Bundle v2 = v();
        h12 h12 = new h12(this.u);
        h12.d = this.g.getPackageName();
        h12.g = v2;
        if (set != null) {
            h12.f = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (m()) {
            h12.h = s() != null ? s() : new Account("<<default account>>", "com.google");
            if (n12 != null) {
                h12.e = n12.asBinder();
            }
        } else if (C()) {
            h12.h = s();
        }
        h12.i = A;
        h12.j = t();
        try {
            synchronized (this.l) {
                if (this.m != null) {
                    this.m.a(new i(this, this.z.get()), h12);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e2) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e2);
            b(1);
        } catch (SecurityException e3) {
            throw e3;
        } catch (RemoteException | RuntimeException e4) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e4);
            a(8, (IBinder) null, (Bundle) null, this.z.get());
        }
    }

    @DexIgnore
    public void b(int i2) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(6, this.z.get(), i2));
    }

    @DexIgnore
    public void a(e eVar) {
        eVar.a();
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i2;
        T t2;
        r12 r12;
        synchronized (this.k) {
            i2 = this.r;
            t2 = this.o;
        }
        synchronized (this.l) {
            r12 = this.m;
        }
        printWriter.append(str).append("mConnectState=");
        if (i2 == 1) {
            printWriter.print("DISCONNECTED");
        } else if (i2 == 2) {
            printWriter.print("REMOTE_CONNECTING");
        } else if (i2 == 3) {
            printWriter.print("LOCAL_CONNECTING");
        } else if (i2 == 4) {
            printWriter.print("CONNECTED");
        } else if (i2 != 5) {
            printWriter.print("UNKNOWN");
        } else {
            printWriter.print("DISCONNECTING");
        }
        printWriter.append(" mService=");
        if (t2 == null) {
            printWriter.append("null");
        } else {
            printWriter.append(z()).append("@").append(Integer.toHexString(System.identityHashCode(t2.asBinder())));
        }
        printWriter.append(" mServiceBroker=");
        if (r12 == null) {
            printWriter.println("null");
        } else {
            printWriter.append("IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(r12.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.c > 0) {
            PrintWriter append = printWriter.append(str).append("lastConnectedTime=");
            long j2 = this.c;
            String format = simpleDateFormat.format(new Date(j2));
            StringBuilder sb = new StringBuilder(String.valueOf(format).length() + 21);
            sb.append(j2);
            sb.append(" ");
            sb.append(format);
            append.println(sb.toString());
        }
        if (this.b > 0) {
            printWriter.append(str).append("lastSuspendedCause=");
            int i3 = this.a;
            if (i3 == 1) {
                printWriter.append("CAUSE_SERVICE_DISCONNECTED");
            } else if (i3 != 2) {
                printWriter.append(String.valueOf(i3));
            } else {
                printWriter.append("CAUSE_NETWORK_LOST");
            }
            PrintWriter append2 = printWriter.append(" lastSuspendedTime=");
            long j3 = this.b;
            String format2 = simpleDateFormat.format(new Date(j3));
            StringBuilder sb2 = new StringBuilder(String.valueOf(format2).length() + 21);
            sb2.append(j3);
            sb2.append(" ");
            sb2.append(format2);
            append2.println(sb2.toString());
        }
        if (this.e > 0) {
            printWriter.append(str).append("lastFailedStatus=").append(uv1.getStatusCodeString(this.d));
            PrintWriter append3 = printWriter.append(" lastFailedTime=");
            long j4 = this.e;
            String format3 = simpleDateFormat.format(new Date(j4));
            StringBuilder sb3 = new StringBuilder(String.valueOf(format3).length() + 21);
            sb3.append(j4);
            sb3.append(" ");
            sb3.append(format3);
            append3.println(sb3.toString());
        }
    }
}
