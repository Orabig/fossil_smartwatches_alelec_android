package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y64 extends x64 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = new ViewDataBinding.j(6);
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        x.a(1, new String[]{"item_calories_workout_day", "item_calories_workout_day"}, new int[]{2, 3}, new int[]{2131558645, 2131558645});
        y.put(2131362149, 4);
        y.put(2131362288, 5);
    }
    */

    @DexIgnore
    public y64(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 6, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
        ViewDataBinding.d(this.s);
        ViewDataBinding.d(this.t);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r6.t.e() == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.s.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 4;
        }
        this.s.f();
        this.t.f();
        g();
    }

    @DexIgnore
    public y64(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 2, objArr[4], objArr[5], objArr[2], objArr[3], objArr[1]);
        this.w = -1;
        this.u.setTag((Object) null);
        this.v = objArr[0];
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
