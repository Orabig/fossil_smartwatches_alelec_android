package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fa5 implements Factory<ea5> {
    @DexIgnore
    public static SearchMicroAppPresenter a(ba5 ba5, MicroAppRepository microAppRepository, an4 an4, PortfolioApp portfolioApp) {
        return new SearchMicroAppPresenter(ba5, microAppRepository, an4, portfolioApp);
    }
}
