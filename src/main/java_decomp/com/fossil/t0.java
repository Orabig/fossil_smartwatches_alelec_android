package com.fossil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import androidx.appcompat.widget.Toolbar;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.q1;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t0 extends ActionBar implements ActionBarOverlayLayout.d {
    @DexIgnore
    public static /* final */ Interpolator B; // = new AccelerateInterpolator();
    @DexIgnore
    public static /* final */ Interpolator C; // = new DecelerateInterpolator();
    @DexIgnore
    public /* final */ ea A; // = new c();
    @DexIgnore
    public Context a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public ActionBarOverlayLayout c;
    @DexIgnore
    public ActionBarContainer d;
    @DexIgnore
    public r2 e;
    @DexIgnore
    public ActionBarContextView f;
    @DexIgnore
    public View g;
    @DexIgnore
    public b3 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public d j;
    @DexIgnore
    public ActionMode k;
    @DexIgnore
    public ActionMode.Callback l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public ArrayList<ActionBar.a> n; // = new ArrayList<>();
    @DexIgnore
    public boolean o;
    @DexIgnore
    public int p; // = 0;
    @DexIgnore
    public boolean q; // = true;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u; // = true;
    @DexIgnore
    public i1 v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public /* final */ ca y; // = new a();
    @DexIgnore
    public /* final */ ca z; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends da {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void b(View view) {
            View view2;
            t0 t0Var = t0.this;
            if (t0Var.q && (view2 = t0Var.g) != null) {
                view2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                t0.this.d.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            t0.this.d.setVisibility(8);
            t0.this.d.setTransitioning(false);
            t0 t0Var2 = t0.this;
            t0Var2.v = null;
            t0Var2.l();
            ActionBarOverlayLayout actionBarOverlayLayout = t0.this.c;
            if (actionBarOverlayLayout != null) {
                x9.J(actionBarOverlayLayout);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends da {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void b(View view) {
            t0 t0Var = t0.this;
            t0Var.v = null;
            t0Var.d.requestLayout();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements ea {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(View view) {
            ((View) t0.this.d.getParent()).invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ActionMode implements q1.a {
        @DexIgnore
        public /* final */ Context c;
        @DexIgnore
        public /* final */ q1 d;
        @DexIgnore
        public ActionMode.Callback e;
        @DexIgnore
        public WeakReference<View> f;

        @DexIgnore
        public d(Context context, ActionMode.Callback callback) {
            this.c = context;
            this.e = callback;
            q1 q1Var = new q1(context);
            q1Var.c(1);
            this.d = q1Var;
            this.d.a((q1.a) this);
        }

        @DexIgnore
        public void a() {
            t0 t0Var = t0.this;
            if (t0Var.j == this) {
                if (!t0.a(t0Var.r, t0Var.s, false)) {
                    t0 t0Var2 = t0.this;
                    t0Var2.k = this;
                    t0Var2.l = this.e;
                } else {
                    this.e.a(this);
                }
                this.e = null;
                t0.this.f(false);
                t0.this.f.a();
                t0.this.e.k().sendAccessibilityEvent(32);
                t0 t0Var3 = t0.this;
                t0Var3.c.setHideOnContentScrollEnabled(t0Var3.x);
                t0.this.j = null;
            }
        }

        @DexIgnore
        public void b(CharSequence charSequence) {
            t0.this.f.setTitle(charSequence);
        }

        @DexIgnore
        public Menu c() {
            return this.d;
        }

        @DexIgnore
        public MenuInflater d() {
            return new h1(this.c);
        }

        @DexIgnore
        public CharSequence e() {
            return t0.this.f.getSubtitle();
        }

        @DexIgnore
        public CharSequence g() {
            return t0.this.f.getTitle();
        }

        @DexIgnore
        public void i() {
            if (t0.this.j == this) {
                this.d.s();
                try {
                    this.e.b(this, this.d);
                } finally {
                    this.d.r();
                }
            }
        }

        @DexIgnore
        public boolean j() {
            return t0.this.f.c();
        }

        @DexIgnore
        public boolean k() {
            this.d.s();
            try {
                return this.e.a((ActionMode) this, (Menu) this.d);
            } finally {
                this.d.r();
            }
        }

        @DexIgnore
        public void b(int i) {
            b((CharSequence) t0.this.a.getResources().getString(i));
        }

        @DexIgnore
        public View b() {
            WeakReference<View> weakReference = this.f;
            if (weakReference != null) {
                return (View) weakReference.get();
            }
            return null;
        }

        @DexIgnore
        public void a(View view) {
            t0.this.f.setCustomView(view);
            this.f = new WeakReference<>(view);
        }

        @DexIgnore
        public void a(CharSequence charSequence) {
            t0.this.f.setSubtitle(charSequence);
        }

        @DexIgnore
        public void a(int i) {
            a((CharSequence) t0.this.a.getResources().getString(i));
        }

        @DexIgnore
        public void a(boolean z) {
            super.a(z);
            t0.this.f.setTitleOptional(z);
        }

        @DexIgnore
        public boolean a(q1 q1Var, MenuItem menuItem) {
            ActionMode.Callback callback = this.e;
            if (callback != null) {
                return callback.a((ActionMode) this, menuItem);
            }
            return false;
        }

        @DexIgnore
        public void a(q1 q1Var) {
            if (this.e != null) {
                i();
                t0.this.f.e();
            }
        }
    }

    @DexIgnore
    public t0(Activity activity, boolean z2) {
        new ArrayList();
        View decorView = activity.getWindow().getDecorView();
        b(decorView);
        if (!z2) {
            this.g = decorView.findViewById(16908290);
        }
    }

    @DexIgnore
    public static boolean a(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        return !z2 && !z3;
    }

    @DexIgnore
    public final r2 a(View view) {
        if (view instanceof r2) {
            return (r2) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view != null ? view.getClass().getSimpleName() : "null");
        throw new IllegalStateException(sb.toString());
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public final void b(View view) {
        this.c = (ActionBarOverlayLayout) view.findViewById(f0.decor_content_parent);
        ActionBarOverlayLayout actionBarOverlayLayout = this.c;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.e = a(view.findViewById(f0.action_bar));
        this.f = (ActionBarContextView) view.findViewById(f0.action_context_bar);
        this.d = (ActionBarContainer) view.findViewById(f0.action_bar_container);
        r2 r2Var = this.e;
        if (r2Var == null || this.f == null || this.d == null) {
            throw new IllegalStateException(t0.class.getSimpleName() + " can only be used with a compatible window decor layout");
        }
        this.a = r2Var.getContext();
        boolean z2 = (this.e.l() & 4) != 0;
        if (z2) {
            this.i = true;
        }
        c1 a2 = c1.a(this.a);
        k(a2.a() || z2);
        i(a2.f());
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes((AttributeSet) null, j0.ActionBar, a0.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(j0.ActionBar_hideOnContentScroll, false)) {
            j(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(j0.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public void c() {
        if (!this.s) {
            this.s = true;
            l(true);
        }
    }

    @DexIgnore
    public void d(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    @DexIgnore
    public void e(boolean z2) {
        i1 i1Var;
        this.w = z2;
        if (!z2 && (i1Var = this.v) != null) {
            i1Var.a();
        }
    }

    @DexIgnore
    public void f(boolean z2) {
        ba baVar;
        ba baVar2;
        if (z2) {
            p();
        } else {
            n();
        }
        if (o()) {
            if (z2) {
                baVar = this.e.a(4, 100);
                baVar2 = this.f.a(0, 200);
            } else {
                baVar2 = this.e.a(0, 200);
                baVar = this.f.a(8, 100);
            }
            i1 i1Var = new i1();
            i1Var.a(baVar, baVar2);
            i1Var.c();
        } else if (z2) {
            this.e.setVisibility(4);
            this.f.setVisibility(0);
        } else {
            this.e.setVisibility(0);
            this.f.setVisibility(8);
        }
    }

    @DexIgnore
    public int g() {
        return this.e.l();
    }

    @DexIgnore
    public void h(boolean z2) {
        View view;
        View view2;
        i1 i1Var = this.v;
        if (i1Var != null) {
            i1Var.a();
        }
        this.d.setVisibility(0);
        if (this.p != 0 || (!this.w && !z2)) {
            this.d.setAlpha(1.0f);
            this.d.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            if (this.q && (view = this.g) != null) {
                view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            this.z.b((View) null);
        } else {
            this.d.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f2 = (float) (-this.d.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.d.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            this.d.setTranslationY(f2);
            i1 i1Var2 = new i1();
            ba a2 = x9.a(this.d);
            a2.b((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            a2.a(this.A);
            i1Var2.a(a2);
            if (this.q && (view2 = this.g) != null) {
                view2.setTranslationY(f2);
                ba a3 = x9.a(this.g);
                a3.b((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                i1Var2.a(a3);
            }
            i1Var2.a(C);
            i1Var2.a(250);
            i1Var2.a(this.z);
            this.v = i1Var2;
            i1Var2.c();
        }
        ActionBarOverlayLayout actionBarOverlayLayout = this.c;
        if (actionBarOverlayLayout != null) {
            x9.J(actionBarOverlayLayout);
        }
    }

    @DexIgnore
    public final void i(boolean z2) {
        this.o = z2;
        if (!this.o) {
            this.e.a((b3) null);
            this.d.setTabContainer(this.h);
        } else {
            this.d.setTabContainer((b3) null);
            this.e.a(this.h);
        }
        boolean z3 = true;
        boolean z4 = m() == 2;
        b3 b3Var = this.h;
        if (b3Var != null) {
            if (z4) {
                b3Var.setVisibility(0);
                ActionBarOverlayLayout actionBarOverlayLayout = this.c;
                if (actionBarOverlayLayout != null) {
                    x9.J(actionBarOverlayLayout);
                }
            } else {
                b3Var.setVisibility(8);
            }
        }
        this.e.b(!this.o && z4);
        ActionBarOverlayLayout actionBarOverlayLayout2 = this.c;
        if (this.o || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout2.setHasNonEmbeddedTabs(z3);
    }

    @DexIgnore
    public void j(boolean z2) {
        if (!z2 || this.c.j()) {
            this.x = z2;
            this.c.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    @DexIgnore
    public void k(boolean z2) {
        this.e.a(z2);
    }

    @DexIgnore
    public void l() {
        ActionMode.Callback callback = this.l;
        if (callback != null) {
            callback.a(this.k);
            this.k = null;
            this.l = null;
        }
    }

    @DexIgnore
    public int m() {
        return this.e.j();
    }

    @DexIgnore
    public final void n() {
        if (this.t) {
            this.t = false;
            ActionBarOverlayLayout actionBarOverlayLayout = this.c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(false);
            }
            l(false);
        }
    }

    @DexIgnore
    public final boolean o() {
        return x9.E(this.d);
    }

    @DexIgnore
    public final void p() {
        if (!this.t) {
            this.t = true;
            ActionBarOverlayLayout actionBarOverlayLayout = this.c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(true);
            }
            l(false);
        }
    }

    @DexIgnore
    public void d() {
        i1 i1Var = this.v;
        if (i1Var != null) {
            i1Var.a();
            this.v = null;
        }
    }

    @DexIgnore
    public void g(boolean z2) {
        View view;
        i1 i1Var = this.v;
        if (i1Var != null) {
            i1Var.a();
        }
        if (this.p != 0 || (!this.w && !z2)) {
            this.y.b((View) null);
            return;
        }
        this.d.setAlpha(1.0f);
        this.d.setTransitioning(true);
        i1 i1Var2 = new i1();
        float f2 = (float) (-this.d.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.d.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        ba a2 = x9.a(this.d);
        a2.b(f2);
        a2.a(this.A);
        i1Var2.a(a2);
        if (this.q && (view = this.g) != null) {
            ba a3 = x9.a(view);
            a3.b(f2);
            i1Var2.a(a3);
        }
        i1Var2.a(B);
        i1Var2.a(250);
        i1Var2.a(this.y);
        this.v = i1Var2;
        i1Var2.c();
    }

    @DexIgnore
    public void c(boolean z2) {
        if (!this.i) {
            d(z2);
        }
    }

    @DexIgnore
    public final void l(boolean z2) {
        if (a(this.r, this.s, this.t)) {
            if (!this.u) {
                this.u = true;
                h(z2);
            }
        } else if (this.u) {
            this.u = false;
            g(z2);
        }
    }

    @DexIgnore
    public void a(float f2) {
        x9.a((View) this.d, f2);
    }

    @DexIgnore
    public void a(Configuration configuration) {
        i(c1.a(this.a).f());
    }

    @DexIgnore
    public void a(int i2) {
        this.p = i2;
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.e.setTitle(charSequence);
    }

    @DexIgnore
    public void a(int i2, int i3) {
        int l2 = this.e.l();
        if ((i3 & 4) != 0) {
            this.i = true;
        }
        this.e.a((i2 & i3) | ((~i3) & l2));
    }

    @DexIgnore
    public t0(Dialog dialog) {
        new ArrayList();
        b(dialog.getWindow().getDecorView());
    }

    @DexIgnore
    public ActionMode a(ActionMode.Callback callback) {
        d dVar = this.j;
        if (dVar != null) {
            dVar.a();
        }
        this.c.setHideOnContentScrollEnabled(false);
        this.f.d();
        d dVar2 = new d(this.f.getContext(), callback);
        if (!dVar2.k()) {
            return null;
        }
        this.j = dVar2;
        dVar2.i();
        this.f.a(dVar2);
        f(true);
        this.f.sendAccessibilityEvent(32);
        return dVar2;
    }

    @DexIgnore
    public boolean f() {
        r2 r2Var = this.e;
        if (r2Var == null || !r2Var.h()) {
            return false;
        }
        this.e.collapseActionView();
        return true;
    }

    @DexIgnore
    public void b(boolean z2) {
        if (z2 != this.m) {
            this.m = z2;
            int size = this.n.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.n.get(i2).a(z2);
            }
        }
    }

    @DexIgnore
    public void a(boolean z2) {
        this.q = z2;
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.e.setWindowTitle(charSequence);
    }

    @DexIgnore
    public void a() {
        if (this.s) {
            this.s = false;
            l(true);
        }
    }

    @DexIgnore
    public boolean a(int i2, KeyEvent keyEvent) {
        Menu c2;
        d dVar = this.j;
        if (dVar == null || (c2 = dVar.c()) == null) {
            return false;
        }
        boolean z2 = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z2 = false;
        }
        c2.setQwertyMode(z2);
        return c2.performShortcut(i2, keyEvent, 0);
    }

    @DexIgnore
    public Context h() {
        if (this.b == null) {
            TypedValue typedValue = new TypedValue();
            this.a.getTheme().resolveAttribute(a0.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.b = new ContextThemeWrapper(this.a, i2);
            } else {
                this.b = this.a;
            }
        }
        return this.b;
    }
}
