package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.m24;
import com.fossil.mz4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yy4$c$b implements m24.e<mz4.d, mz4.b> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsPresenter.c a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1", f = "NotificationContactsPresenter.kt", l = {60}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ mz4.d $responseValue;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ yy4$c$b this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.yy4$c$b$a$a")
        @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1$1", f = "NotificationContactsPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.yy4$c$b$a$a  reason: collision with other inner class name */
        public static final class C0058a extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0058a(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                C0058a aVar = new C0058a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0058a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    for (ContactGroup contactGroup : this.this$0.$responseValue.a()) {
                        List contacts = contactGroup.getContacts();
                        wg6.a((Object) contacts, "it.contacts");
                        if (!contacts.isEmpty()) {
                            Contact contact = (Contact) contactGroup.getContacts().get(0);
                            wx4 wx4 = new wx4(contact, (String) null, 2, (qg6) null);
                            wx4.setAdded(true);
                            Contact contact2 = wx4.getContact();
                            if (contact2 != null) {
                                wg6.a((Object) contact, "contact");
                                contact2.setDbRowId(contact.getDbRowId());
                            }
                            Contact contact3 = wx4.getContact();
                            if (contact3 != null) {
                                wg6.a((Object) contact, "contact");
                                contact3.setUseSms(contact.isUseSms());
                            }
                            Contact contact4 = wx4.getContact();
                            if (contact4 != null) {
                                wg6.a((Object) contact, "contact");
                                contact4.setUseCall(contact.isUseCall());
                            }
                            wg6.a((Object) contact, "contact");
                            List phoneNumbers = contact.getPhoneNumbers();
                            wg6.a((Object) phoneNumbers, "contact.phoneNumbers");
                            if (!phoneNumbers.isEmpty()) {
                                Object obj2 = contact.getPhoneNumbers().get(0);
                                wg6.a(obj2, "contact.phoneNumbers[0]");
                                if (!TextUtils.isEmpty(((PhoneNumber) obj2).getNumber())) {
                                    wx4.setHasPhoneNumber(true);
                                    Object obj3 = contact.getPhoneNumbers().get(0);
                                    wg6.a(obj3, "contact.phoneNumbers[0]");
                                    wx4.setPhoneNumber(((PhoneNumber) obj3).getNumber());
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String a = NotificationContactsPresenter.m.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                    Object obj4 = contact.getPhoneNumbers().get(0);
                                    wg6.a(obj4, "contact.phoneNumbers[0]");
                                    sb.append(((PhoneNumber) obj4).getNumber());
                                    local.d(a, sb.toString());
                                }
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a2 = NotificationContactsPresenter.m.a();
                            local2.d(a2, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                            this.this$0.this$0.a.this$0.j().add(wx4);
                        }
                    }
                    return hf6.a(this.this$0.this$0.a.this$0.k().addAll(yd6.d(this.this$0.this$0.a.this$0.j())));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(yy4$c$b yy4_c_b, mz4.d dVar, xe6 xe6) {
            super(2, xe6);
            this.this$0 = yy4_c_b;
            this.$responseValue = dVar;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$responseValue, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.a.this$0.b();
                C0058a aVar = new C0058a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, aVar, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.a.this$0.g.a(this.this$0.a.this$0.j(), nx5.a.a());
            this.this$0.a.this$0.k.a(0, new Bundle(), this.this$0.a.this$0);
            return cd6.a;
        }
    }

    @DexIgnore
    public yy4$c$b(NotificationContactsPresenter.c cVar) {
        this.a = cVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(mz4.d dVar) {
        wg6.b(dVar, "responseValue");
        FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.m.a(), "GetAllContactGroup onSuccess");
        rm6 unused = ik6.b(this.a.this$0.e(), (af6) null, (ll6) null, new a(this, dVar, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(mz4.b bVar) {
        wg6.b(bVar, "errorValue");
        FLogger.INSTANCE.getLocal().d(NotificationContactsPresenter.m.a(), "GetAllContactGroup onError");
    }
}
