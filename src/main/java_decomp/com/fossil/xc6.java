package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc6 implements Collection<wc6>, ph6 {
    @DexIgnore
    public /* final */ long[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends qe6 {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ long[] b;

        @DexIgnore
        public a(long[] jArr) {
            wg6.b(jArr, "array");
            this.b = jArr;
        }

        @DexIgnore
        public long a() {
            int i = this.a;
            long[] jArr = this.b;
            if (i < jArr.length) {
                this.a = i + 1;
                long j = jArr[i];
                wc6.c(j);
                return j;
            }
            throw new NoSuchElementException(String.valueOf(i));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < this.b.length;
        }
    }

    @DexIgnore
    public static boolean a(long[] jArr, Object obj) {
        return (obj instanceof xc6) && wg6.a((Object) jArr, (Object) ((xc6) obj).b());
    }

    @DexIgnore
    public static int b(long[] jArr) {
        if (jArr != null) {
            return Arrays.hashCode(jArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean c(long[] jArr) {
        return jArr.length == 0;
    }

    @DexIgnore
    public static qe6 d(long[] jArr) {
        return new a(jArr);
    }

    @DexIgnore
    public static String e(long[] jArr) {
        return "ULongArray(storage=" + Arrays.toString(jArr) + ")";
    }

    @DexIgnore
    public int a() {
        return a(this.a);
    }

    @DexIgnore
    public /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(Collection<? extends wc6> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean b(long j) {
        return a(this.a, j);
    }

    @DexIgnore
    public final /* synthetic */ long[] b() {
        return this.a;
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof wc6) {
            return b(((wc6) obj).a());
        }
        return false;
    }

    @DexIgnore
    public boolean containsAll(Collection<? extends Object> collection) {
        return a(this.a, (Collection<wc6>) collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return b(this.a);
    }

    @DexIgnore
    public boolean isEmpty() {
        return c(this.a);
    }

    @DexIgnore
    public qe6 iterator() {
        return d(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public Object[] toArray() {
        return pg6.a(this);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        return pg6.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public static int a(long[] jArr) {
        return jArr.length;
    }

    @DexIgnore
    public static boolean a(long[] jArr, long j) {
        return nd6.a(jArr, j);
    }

    @DexIgnore
    public static boolean a(long[] jArr, Collection<wc6> collection) {
        boolean z;
        wg6.b(collection, "elements");
        if (collection.isEmpty()) {
            return true;
        }
        for (T next : collection) {
            if (!(next instanceof wc6) || !nd6.a(jArr, ((wc6) next).a())) {
                z = false;
                continue;
            } else {
                z = true;
                continue;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }
}
