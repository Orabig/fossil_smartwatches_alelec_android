package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum sh4 {
    ACTIVE_TIME(1),
    TOTAL_STEPS(2),
    CALORIES(3),
    GOAL_TRACKING(3),
    TOTAL_SLEEP(11),
    RESTFUL(12),
    LIGHT(13),
    AWAKE(14);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public int mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        Companion = new a((qg6) null);
    }
    */

    @DexIgnore
    public sh4(int i) {
        this.mValue = i;
    }

    @DexIgnore
    public final int getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(int i) {
        this.mValue = i;
    }
}
