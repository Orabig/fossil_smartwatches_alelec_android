package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import com.fossil.kr;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class or implements kr {
    @DexIgnore
    public static /* final */ String u; // = "or";
    @DexIgnore
    public int[] a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public /* final */ kr.a c;
    @DexIgnore
    public ByteBuffer d;
    @DexIgnore
    public byte[] e;
    @DexIgnore
    public short[] f;
    @DexIgnore
    public byte[] g;
    @DexIgnore
    public byte[] h;
    @DexIgnore
    public byte[] i;
    @DexIgnore
    public int[] j;
    @DexIgnore
    public int k;
    @DexIgnore
    public mr l;
    @DexIgnore
    public Bitmap m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public Boolean s;
    @DexIgnore
    public Bitmap.Config t;

    @DexIgnore
    public or(kr.a aVar, mr mrVar, ByteBuffer byteBuffer, int i2) {
        this(aVar);
        a(mrVar, byteBuffer, i2);
    }

    @DexIgnore
    public int a(int i2) {
        if (i2 >= 0) {
            mr mrVar = this.l;
            if (i2 < mrVar.c) {
                return mrVar.e.get(i2).i;
            }
        }
        return -1;
    }

    @DexIgnore
    public void b() {
        this.k = (this.k + 1) % this.l.c;
    }

    @DexIgnore
    public int c() {
        return this.l.c;
    }

    @DexIgnore
    public void clear() {
        this.l = null;
        byte[] bArr = this.i;
        if (bArr != null) {
            this.c.a(bArr);
        }
        int[] iArr = this.j;
        if (iArr != null) {
            this.c.a(iArr);
        }
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.c.a(bitmap);
        }
        this.m = null;
        this.d = null;
        this.s = null;
        byte[] bArr2 = this.e;
        if (bArr2 != null) {
            this.c.a(bArr2);
        }
    }

    @DexIgnore
    public int d() {
        int i2;
        if (this.l.c <= 0 || (i2 = this.k) < 0) {
            return 0;
        }
        return a(i2);
    }

    @DexIgnore
    public ByteBuffer e() {
        return this.d;
    }

    @DexIgnore
    public void f() {
        this.k = -1;
    }

    @DexIgnore
    public int g() {
        return this.k;
    }

    @DexIgnore
    public int h() {
        return this.d.limit() + this.i.length + (this.j.length * 4);
    }

    @DexIgnore
    public final Bitmap i() {
        Boolean bool = this.s;
        Bitmap a2 = this.c.a(this.r, this.q, (bool == null || bool.booleanValue()) ? Bitmap.Config.ARGB_8888 : this.t);
        a2.setHasAlpha(true);
        return a2;
    }

    @DexIgnore
    public final int j() {
        int k2 = k();
        if (k2 <= 0) {
            return k2;
        }
        ByteBuffer byteBuffer = this.d;
        byteBuffer.get(this.e, 0, Math.min(k2, byteBuffer.remaining()));
        return k2;
    }

    @DexIgnore
    public final int k() {
        return this.d.get() & 255;
    }

    @DexIgnore
    public final void b(lr lrVar) {
        lr lrVar2 = lrVar;
        int[] iArr = this.j;
        int i2 = lrVar2.d;
        int i3 = lrVar2.b;
        int i4 = lrVar2.c;
        int i5 = lrVar2.a;
        boolean z = this.k == 0;
        int i6 = this.r;
        byte[] bArr = this.i;
        int[] iArr2 = this.a;
        int i7 = 0;
        byte b2 = -1;
        while (i7 < i2) {
            int i8 = (i7 + i3) * i6;
            int i9 = i8 + i5;
            int i10 = i9 + i4;
            int i11 = i8 + i6;
            if (i11 < i10) {
                i10 = i11;
            }
            int i12 = lrVar2.c * i7;
            int i13 = i9;
            while (i13 < i10) {
                byte b3 = bArr[i12];
                byte b4 = b3 & 255;
                if (b4 != b2) {
                    int i14 = iArr2[b4];
                    if (i14 != 0) {
                        iArr[i13] = i14;
                    } else {
                        b2 = b3;
                    }
                }
                i12++;
                i13++;
                lr lrVar3 = lrVar;
            }
            i7++;
            lrVar2 = lrVar;
        }
        Boolean bool = this.s;
        this.s = Boolean.valueOf((bool != null && bool.booleanValue()) || (this.s == null && z && b2 != -1));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v5, resolved type: byte} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=int, for r4v16, types: [short] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void c(lr lrVar) {
        int i2;
        int i3;
        int i4;
        or orVar = this;
        lr lrVar2 = lrVar;
        if (lrVar2 != null) {
            orVar.d.position(lrVar2.j);
        }
        if (lrVar2 == null) {
            mr mrVar = orVar.l;
            i2 = mrVar.f;
            i3 = mrVar.g;
        } else {
            i2 = lrVar2.c;
            i3 = lrVar2.d;
        }
        int i5 = i2 * i3;
        byte[] bArr = orVar.i;
        if (bArr == null || bArr.length < i5) {
            orVar.i = orVar.c.b(i5);
        }
        byte[] bArr2 = orVar.i;
        if (orVar.f == null) {
            orVar.f = new short[4096];
        }
        short[] sArr = orVar.f;
        if (orVar.g == null) {
            orVar.g = new byte[4096];
        }
        byte[] bArr3 = orVar.g;
        if (orVar.h == null) {
            orVar.h = new byte[4097];
        }
        byte[] bArr4 = orVar.h;
        int k2 = k();
        int i6 = 1 << k2;
        int i7 = i6 + 1;
        int i8 = i6 + 2;
        int i9 = k2 + 1;
        int i10 = (1 << i9) - 1;
        int i11 = 0;
        for (int i12 = 0; i12 < i6; i12++) {
            sArr[i12] = 0;
            bArr3[i12] = (byte) i12;
        }
        byte[] bArr5 = orVar.e;
        int i13 = i9;
        int i14 = i8;
        int i15 = i10;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        int i20 = 0;
        int i21 = -1;
        int i22 = 0;
        int i23 = 0;
        while (true) {
            if (i11 < i5) {
                if (i16 == 0) {
                    i16 = j();
                    if (i16 <= 0) {
                        orVar.o = 3;
                        break;
                    }
                    i19 = 0;
                }
                i18 += (bArr5[i19] & 255) << i17;
                i19++;
                i16--;
                int i24 = i17 + 8;
                int i25 = i21;
                int i26 = i22;
                int i27 = i14;
                int i28 = i20;
                int i29 = i11;
                int i30 = i13;
                while (true) {
                    if (i24 < i30) {
                        i13 = i30;
                        i22 = i26;
                        i11 = i29;
                        i20 = i28;
                        i17 = i24;
                        i14 = i27;
                        i21 = i25;
                        orVar = this;
                        break;
                    }
                    int i31 = i18 & i15;
                    i18 >>= i30;
                    i24 -= i30;
                    if (i31 == i6) {
                        i30 = i9;
                        i27 = i8;
                        i15 = i10;
                        i25 = -1;
                    } else if (i31 == i7) {
                        i17 = i24;
                        i13 = i30;
                        i11 = i29;
                        i20 = i28;
                        i14 = i27;
                        i22 = i26;
                        i21 = i25;
                        break;
                    } else if (i25 == -1) {
                        bArr2[i28] = bArr3[i31];
                        i28++;
                        i29++;
                        orVar = this;
                        i25 = i31;
                        i26 = i25;
                    } else {
                        int i32 = i27;
                        int i33 = i24;
                        if (i31 >= i32) {
                            bArr4[i23] = (byte) i26;
                            i23++;
                            i4 = i25;
                        } else {
                            i4 = i31;
                        }
                        while (i4 >= i6) {
                            bArr4[i23] = bArr3[i4];
                            i23++;
                            i4 = sArr[i4];
                        }
                        int i34 = bArr3[i4] & 255;
                        int i35 = i9;
                        byte b2 = (byte) i34;
                        bArr2[i28] = b2;
                        while (true) {
                            i28++;
                            i29++;
                            if (i23 <= 0) {
                                break;
                            }
                            i23--;
                            bArr2[i28] = bArr4[i23];
                        }
                        int i36 = i34;
                        if (i32 < 4096) {
                            sArr[i32] = (short) i25;
                            bArr3[i32] = b2;
                            i32++;
                            if ((i32 & i15) == 0 && i32 < 4096) {
                                i30++;
                                i15 += i32;
                            }
                        }
                        i25 = i31;
                        i24 = i33;
                        i9 = i35;
                        i26 = i36;
                        i27 = i32;
                        orVar = this;
                    }
                }
            } else {
                break;
            }
        }
        Arrays.fill(bArr2, i20, i5, (byte) 0);
    }

    @DexIgnore
    public or(kr.a aVar) {
        this.b = new int[256];
        this.t = Bitmap.Config.ARGB_8888;
        this.c = aVar;
        this.l = new mr();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f7, code lost:
        return null;
     */
    @DexIgnore
    public synchronized Bitmap a() {
        if (this.l.c <= 0 || this.k < 0) {
            if (Log.isLoggable(u, 3)) {
                String str = u;
                Log.d(str, "Unable to decode frame, frameCount=" + this.l.c + ", framePointer=" + this.k);
            }
            this.o = 1;
        }
        if (this.o != 1) {
            if (this.o != 2) {
                this.o = 0;
                if (this.e == null) {
                    this.e = this.c.b(255);
                }
                lr lrVar = this.l.e.get(this.k);
                int i2 = this.k - 1;
                lr lrVar2 = i2 >= 0 ? this.l.e.get(i2) : null;
                this.a = lrVar.k != null ? lrVar.k : this.l.a;
                if (this.a == null) {
                    if (Log.isLoggable(u, 3)) {
                        String str2 = u;
                        Log.d(str2, "No valid color table found for frame #" + this.k);
                    }
                    this.o = 1;
                    return null;
                }
                if (lrVar.f) {
                    System.arraycopy(this.a, 0, this.b, 0, this.a.length);
                    this.a = this.b;
                    this.a[lrVar.h] = 0;
                    if (lrVar.g == 2 && this.k == 0) {
                        this.s = true;
                    }
                }
                return a(lrVar, lrVar2);
            }
        }
        if (Log.isLoggable(u, 3)) {
            String str3 = u;
            Log.d(str3, "Unable to decode frame, status=" + this.o);
        }
    }

    @DexIgnore
    public synchronized void a(mr mrVar, ByteBuffer byteBuffer, int i2) {
        if (i2 > 0) {
            int highestOneBit = Integer.highestOneBit(i2);
            this.o = 0;
            this.l = mrVar;
            this.k = -1;
            this.d = byteBuffer.asReadOnlyBuffer();
            this.d.position(0);
            this.d.order(ByteOrder.LITTLE_ENDIAN);
            this.n = false;
            Iterator<lr> it = mrVar.e.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().g == 3) {
                        this.n = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            this.p = highestOneBit;
            this.r = mrVar.f / highestOneBit;
            this.q = mrVar.g / highestOneBit;
            this.i = this.c.b(mrVar.f * mrVar.g);
            this.j = this.c.a(this.r * this.q);
        } else {
            throw new IllegalArgumentException("Sample size must be >=0, not: " + i2);
        }
    }

    @DexIgnore
    public void a(Bitmap.Config config) {
        if (config == Bitmap.Config.ARGB_8888 || config == Bitmap.Config.RGB_565) {
            this.t = config;
            return;
        }
        throw new IllegalArgumentException("Unsupported format: " + config + ", must be one of " + Bitmap.Config.ARGB_8888 + " or " + Bitmap.Config.RGB_565);
    }

    @DexIgnore
    public final Bitmap a(lr lrVar, lr lrVar2) {
        int i2;
        int i3;
        Bitmap bitmap;
        int[] iArr = this.j;
        int i4 = 0;
        if (lrVar2 == null) {
            Bitmap bitmap2 = this.m;
            if (bitmap2 != null) {
                this.c.a(bitmap2);
            }
            this.m = null;
            Arrays.fill(iArr, 0);
        }
        if (lrVar2 != null && lrVar2.g == 3 && this.m == null) {
            Arrays.fill(iArr, 0);
        }
        if (lrVar2 != null && (i3 = lrVar2.g) > 0) {
            if (i3 == 2) {
                if (!lrVar.f) {
                    mr mrVar = this.l;
                    int i5 = mrVar.l;
                    if (lrVar.k == null || mrVar.j != lrVar.h) {
                        i4 = i5;
                    }
                }
                int i6 = lrVar2.d;
                int i7 = this.p;
                int i8 = i6 / i7;
                int i9 = lrVar2.b / i7;
                int i10 = lrVar2.c / i7;
                int i11 = lrVar2.a / i7;
                int i12 = this.r;
                int i13 = (i9 * i12) + i11;
                int i14 = (i8 * i12) + i13;
                while (i13 < i14) {
                    int i15 = i13 + i10;
                    for (int i16 = i13; i16 < i15; i16++) {
                        iArr[i16] = i4;
                    }
                    i13 += this.r;
                }
            } else if (i3 == 3 && (bitmap = this.m) != null) {
                int i17 = this.r;
                bitmap.getPixels(iArr, 0, i17, 0, 0, i17, this.q);
            }
        }
        c(lrVar);
        if (lrVar.e || this.p != 1) {
            a(lrVar);
        } else {
            b(lrVar);
        }
        if (this.n && ((i2 = lrVar.g) == 0 || i2 == 1)) {
            if (this.m == null) {
                this.m = i();
            }
            Bitmap bitmap3 = this.m;
            int i18 = this.r;
            bitmap3.setPixels(iArr, 0, i18, 0, 0, i18, this.q);
        }
        Bitmap i19 = i();
        int i20 = this.r;
        i19.setPixels(iArr, 0, i20, 0, 0, i20, this.q);
        return i19;
    }

    @DexIgnore
    public final void a(lr lrVar) {
        boolean z;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        lr lrVar2 = lrVar;
        int[] iArr = this.j;
        int i7 = lrVar2.d;
        int i8 = this.p;
        int i9 = i7 / i8;
        int i10 = lrVar2.b / i8;
        int i11 = lrVar2.c / i8;
        int i12 = lrVar2.a / i8;
        boolean z2 = true;
        boolean z3 = this.k == 0;
        int i13 = this.p;
        int i14 = this.r;
        int i15 = this.q;
        byte[] bArr = this.i;
        int[] iArr2 = this.a;
        Boolean bool = this.s;
        int i16 = 0;
        int i17 = 0;
        int i18 = 1;
        int i19 = 8;
        while (i16 < i9) {
            Boolean bool2 = z2;
            if (lrVar2.e) {
                if (i17 >= i9) {
                    i2 = i9;
                    i6 = i18 + 1;
                    if (i6 == 2) {
                        i17 = 4;
                    } else if (i6 == 3) {
                        i17 = 2;
                        i19 = 4;
                    } else if (i6 == 4) {
                        i17 = 1;
                        i19 = 2;
                    }
                } else {
                    i2 = i9;
                    i6 = i18;
                }
                i3 = i17 + i19;
                i18 = i6;
            } else {
                i2 = i9;
                i3 = i17;
                i17 = i16;
            }
            int i20 = i17 + i10;
            boolean z4 = i13 == 1;
            if (i20 < i15) {
                int i21 = i20 * i14;
                int i22 = i21 + i12;
                int i23 = i22 + i11;
                int i24 = i21 + i14;
                if (i24 < i23) {
                    i23 = i24;
                }
                i4 = i10;
                int i25 = i16 * i13 * lrVar2.c;
                if (z4) {
                    int i26 = i22;
                    while (i26 < i23) {
                        int i27 = i11;
                        int i28 = iArr2[bArr[i25] & 255];
                        if (i28 != 0) {
                            iArr[i26] = i28;
                        } else if (z3 && bool == null) {
                            bool = bool2;
                        }
                        i25 += i13;
                        i26++;
                        i11 = i27;
                    }
                } else {
                    i5 = i11;
                    int i29 = ((i23 - i22) * i13) + i25;
                    int i30 = i22;
                    while (i30 < i23) {
                        int i31 = i23;
                        int a2 = a(i25, i29, lrVar2.c);
                        if (a2 != 0) {
                            iArr[i30] = a2;
                        } else if (z3 && bool == null) {
                            bool = bool2;
                        }
                        i25 += i13;
                        i30++;
                        i23 = i31;
                    }
                    i16++;
                    i17 = i3;
                    i11 = i5;
                    z2 = bool2;
                    i9 = i2;
                    i10 = i4;
                }
            } else {
                i4 = i10;
            }
            i5 = i11;
            i16++;
            i17 = i3;
            i11 = i5;
            z2 = bool2;
            i9 = i2;
            i10 = i4;
        }
        if (this.s == null) {
            if (bool == null) {
                z = false;
            } else {
                z = bool.booleanValue();
            }
            this.s = Boolean.valueOf(z);
        }
    }

    @DexIgnore
    public final int a(int i2, int i3, int i4) {
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        for (int i10 = i2; i10 < this.p + i2; i10++) {
            byte[] bArr = this.i;
            if (i10 >= bArr.length || i10 >= i3) {
                break;
            }
            int i11 = this.a[bArr[i10] & 255];
            if (i11 != 0) {
                i5 += (i11 >> 24) & 255;
                i6 += (i11 >> 16) & 255;
                i7 += (i11 >> 8) & 255;
                i8 += i11 & 255;
                i9++;
            }
        }
        int i12 = i2 + i4;
        for (int i13 = i12; i13 < this.p + i12; i13++) {
            byte[] bArr2 = this.i;
            if (i13 >= bArr2.length || i13 >= i3) {
                break;
            }
            int i14 = this.a[bArr2[i13] & 255];
            if (i14 != 0) {
                i5 += (i14 >> 24) & 255;
                i6 += (i14 >> 16) & 255;
                i7 += (i14 >> 8) & 255;
                i8 += i14 & 255;
                i9++;
            }
        }
        if (i9 == 0) {
            return 0;
        }
        return ((i5 / i9) << 24) | ((i6 / i9) << 16) | ((i7 / i9) << 8) | (i8 / i9);
    }
}
