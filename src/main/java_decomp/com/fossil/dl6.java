package com.fossil;

import com.fossil.af6;
import com.fossil.ye6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dl6 extends ve6 implements ye6 {
    @DexIgnore
    public dl6() {
        super(ye6.l);
    }

    @DexIgnore
    public abstract void a(af6 af6, Runnable runnable);

    @DexIgnore
    public void b(xe6<?> xe6) {
        wg6.b(xe6, "continuation");
        ye6.a.a((ye6) this, xe6);
    }

    @DexIgnore
    public boolean b(af6 af6) {
        wg6.b(af6, "context");
        return true;
    }

    @DexIgnore
    public final <T> xe6<T> c(xe6<? super T> xe6) {
        wg6.b(xe6, "continuation");
        return new vl6(this, xe6);
    }

    @DexIgnore
    public <E extends af6.b> E get(af6.c<E> cVar) {
        wg6.b(cVar, "key");
        return ye6.a.a((ye6) this, cVar);
    }

    @DexIgnore
    public af6 minusKey(af6.c<?> cVar) {
        wg6.b(cVar, "key");
        return ye6.a.b(this, cVar);
    }

    @DexIgnore
    public String toString() {
        return ol6.a((Object) this) + '@' + ol6.b(this);
    }

    @DexIgnore
    public void b(af6 af6, Runnable runnable) {
        wg6.b(af6, "context");
        wg6.b(runnable, "block");
        a(af6, runnable);
    }
}
