package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yj6 extends xj6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xg6 implements ig6<CharSequence, Integer, lc6<? extends Integer, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ char[] $delimiters;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $ignoreCase;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(char[] cArr, boolean z) {
            super(2);
            this.$delimiters = cArr;
            this.$ignoreCase = z;
        }

        @DexIgnore
        public final lc6<Integer, Integer> invoke(CharSequence charSequence, int i) {
            wg6.b(charSequence, "$receiver");
            int a = yj6.a(charSequence, this.$delimiters, i, this.$ignoreCase);
            if (a < 0) {
                return null;
            }
            return qc6.a(Integer.valueOf(a), 1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
            return invoke((CharSequence) obj, ((Number) obj2).intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements ig6<CharSequence, Integer, lc6<? extends Integer, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ List $delimitersList;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $ignoreCase;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(List list, boolean z) {
            super(2);
            this.$delimitersList = list;
            this.$ignoreCase = z;
        }

        @DexIgnore
        public final lc6<Integer, Integer> invoke(CharSequence charSequence, int i) {
            wg6.b(charSequence, "$receiver");
            lc6 a = yj6.b(charSequence, (Collection<String>) this.$delimitersList, i, this.$ignoreCase, false);
            if (a != null) {
                return qc6.a(a.getFirst(), Integer.valueOf(((String) a.getSecond()).length()));
            }
            return null;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
            return invoke((CharSequence) obj, ((Number) obj2).intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements hg6<wh6, String> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $this_splitToSequence;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CharSequence charSequence) {
            super(1);
            this.$this_splitToSequence = charSequence;
        }

        @DexIgnore
        public final String invoke(wh6 wh6) {
            wg6.b(wh6, "it");
            return yj6.a(this.$this_splitToSequence, wh6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends xg6 implements hg6<wh6, String> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $this_splitToSequence;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(CharSequence charSequence) {
            super(1);
            this.$this_splitToSequence = charSequence;
        }

        @DexIgnore
        public final String invoke(wh6 wh6) {
            wg6.b(wh6, "it");
            return yj6.a(this.$this_splitToSequence, wh6);
        }
    }

    @DexIgnore
    public static final wh6 b(CharSequence charSequence) {
        wg6.b(charSequence, "$this$indices");
        return new wh6(0, charSequence.length() - 1);
    }

    @DexIgnore
    public static final int c(CharSequence charSequence) {
        wg6.b(charSequence, "$this$lastIndex");
        return charSequence.length() - 1;
    }

    @DexIgnore
    public static final CharSequence d(CharSequence charSequence) {
        wg6.b(charSequence, "$this$trim");
        int length = charSequence.length() - 1;
        int i = 0;
        boolean z = false;
        while (i <= length) {
            boolean a2 = cj6.a(charSequence.charAt(!z ? i : length));
            if (!z) {
                if (!a2) {
                    z = true;
                } else {
                    i++;
                }
            } else if (!a2) {
                break;
            } else {
                length--;
            }
        }
        return charSequence.subSequence(i, length + 1);
    }

    @DexIgnore
    public static final CharSequence a(CharSequence charSequence, int i, char c2) {
        wg6.b(charSequence, "$this$padStart");
        if (i < 0) {
            throw new IllegalArgumentException("Desired length " + i + " is less than zero.");
        } else if (i <= charSequence.length()) {
            return charSequence.subSequence(0, charSequence.length());
        } else {
            StringBuilder sb = new StringBuilder(i);
            int length = i - charSequence.length();
            int i2 = 1;
            if (1 <= length) {
                while (true) {
                    sb.append(c2);
                    if (i2 == length) {
                        break;
                    }
                    i2++;
                }
            }
            sb.append(charSequence);
            return sb;
        }
    }

    @DexIgnore
    public static final String b(String str, char c2, String str2) {
        wg6.b(str, "$this$substringBefore");
        wg6.b(str2, "missingDelimiterValue");
        int a2 = a((CharSequence) str, c2, 0, false, 6, (Object) null);
        if (a2 == -1) {
            return str2;
        }
        String substring = str.substring(0, a2);
        wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String c(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return c(str, str2, str3);
    }

    @DexIgnore
    public static final String c(String str, String str2, String str3) {
        wg6.b(str, "$this$substringBeforeLast");
        wg6.b(str2, "delimiter");
        wg6.b(str3, "missingDelimiterValue");
        int b2 = b((CharSequence) str, str2, 0, false, 6, (Object) null);
        if (b2 == -1) {
            return str3;
        }
        String substring = str.substring(0, b2);
        wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String b(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return b(str, str2, str3);
    }

    @DexIgnore
    public static final String b(String str, String str2, String str3) {
        wg6.b(str, "$this$substringBefore");
        wg6.b(str2, "delimiter");
        wg6.b(str3, "missingDelimiterValue");
        int a2 = a((CharSequence) str, str2, 0, false, 6, (Object) null);
        if (a2 == -1) {
            return str3;
        }
        String substring = str.substring(0, a2);
        wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ boolean c(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return c(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final boolean c(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        wg6.b(charSequence, "$this$startsWith");
        wg6.b(charSequence2, "prefix");
        if (!z && (charSequence instanceof String) && (charSequence2 instanceof String)) {
            return xj6.c((String) charSequence, (String) charSequence2, false, 2, (Object) null);
        }
        return a(charSequence, 0, charSequence2, 0, charSequence2.length(), z);
    }

    @DexIgnore
    public static /* synthetic */ boolean b(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return b(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final boolean b(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        wg6.b(charSequence, "$this$endsWith");
        wg6.b(charSequence2, "suffix");
        if (!z && (charSequence instanceof String) && (charSequence2 instanceof String)) {
            return xj6.a((String) charSequence, (String) charSequence2, false, 2, (Object) null);
        }
        return a(charSequence, charSequence.length() - charSequence2.length(), charSequence2, 0, charSequence2.length(), z);
    }

    @DexIgnore
    public static final String a(String str, int i, char c2) {
        wg6.b(str, "$this$padStart");
        return a((CharSequence) str, i, c2).toString();
    }

    @DexIgnore
    public static final String a(CharSequence charSequence, wh6 wh6) {
        wg6.b(charSequence, "$this$substring");
        wg6.b(wh6, "range");
        return charSequence.subSequence(wh6.e().intValue(), wh6.d().intValue() + 1).toString();
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, char c2, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return b(str, c2, str2);
    }

    @DexIgnore
    public static final int b(CharSequence charSequence, char[] cArr, int i, boolean z) {
        wg6.b(charSequence, "$this$lastIndexOfAny");
        wg6.b(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            for (int b2 = ci6.b(i, c(charSequence)); b2 >= 0; b2--) {
                char charAt = charSequence.charAt(b2);
                int length = cArr.length;
                boolean z2 = false;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    } else if (dj6.a(cArr[i2], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z2) {
                    return b2;
                }
            }
            return -1;
        }
        return ((String) charSequence).lastIndexOf(nd6.a(cArr), i);
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return a(str, str2, str3);
    }

    @DexIgnore
    public static final String a(String str, String str2, String str3) {
        wg6.b(str, "$this$substringAfter");
        wg6.b(str2, "delimiter");
        wg6.b(str3, "missingDelimiterValue");
        int a2 = a((CharSequence) str, str2, 0, false, 6, (Object) null);
        if (a2 == -1) {
            return str3;
        }
        String substring = str.substring(a2 + str2.length(), str.length());
        wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String a(String str, char c2, String str2) {
        wg6.b(str, "$this$substringAfterLast");
        wg6.b(str2, "missingDelimiterValue");
        int b2 = b((CharSequence) str, c2, 0, false, 6, (Object) null);
        if (b2 == -1) {
            return str2;
        }
        String substring = str.substring(b2 + 1, str.length());
        wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String a(String str, CharSequence charSequence, CharSequence charSequence2) {
        wg6.b(str, "$this$removeSurrounding");
        wg6.b(charSequence, "prefix");
        wg6.b(charSequence2, "suffix");
        if (str.length() < charSequence.length() + charSequence2.length() || !c((CharSequence) str, charSequence, false, 2, (Object) null) || !b((CharSequence) str, charSequence2, false, 2, (Object) null)) {
            return str;
        }
        String substring = str.substring(charSequence.length(), str.length() - charSequence2.length());
        wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final lc6<Integer, String> b(CharSequence charSequence, Collection<String> collection, int i, boolean z, boolean z2) {
        T t;
        T t2;
        if (z || collection.size() != 1) {
            uh6 wh6 = !z2 ? new wh6(ci6.a(i, 0), charSequence.length()) : ci6.c(ci6.b(i, c(charSequence)), 0);
            if (charSequence instanceof String) {
                int a2 = wh6.a();
                int b2 = wh6.b();
                int c2 = wh6.c();
                if (c2 < 0 ? a2 >= b2 : a2 <= b2) {
                    while (true) {
                        Iterator<T> it = collection.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t2 = null;
                                break;
                            }
                            t2 = it.next();
                            String str = (String) t2;
                            if (xj6.a(str, 0, (String) charSequence, a2, str.length(), z)) {
                                break;
                            }
                        }
                        String str2 = (String) t2;
                        if (str2 == null) {
                            if (a2 == b2) {
                                break;
                            }
                            a2 += c2;
                        } else {
                            return qc6.a(Integer.valueOf(a2), str2);
                        }
                    }
                }
            } else {
                int a3 = wh6.a();
                int b3 = wh6.b();
                int c3 = wh6.c();
                if (c3 < 0 ? a3 >= b3 : a3 <= b3) {
                    while (true) {
                        Iterator<T> it2 = collection.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it2.next();
                            String str3 = (String) t;
                            if (a((CharSequence) str3, 0, charSequence, a3, str3.length(), z)) {
                                break;
                            }
                        }
                        String str4 = (String) t;
                        if (str4 == null) {
                            if (a3 == b3) {
                                break;
                            }
                            a3 += c3;
                        } else {
                            return qc6.a(Integer.valueOf(a3), str4);
                        }
                    }
                }
            }
            return null;
        }
        String str5 = (String) yd6.i(collection);
        CharSequence charSequence2 = charSequence;
        String str6 = str5;
        int i2 = i;
        int a4 = !z2 ? a(charSequence2, str6, i2, false, 4, (Object) null) : b(charSequence2, str6, i2, false, 4, (Object) null);
        if (a4 < 0) {
            return null;
        }
        return qc6.a(Integer.valueOf(a4), str5);
    }

    @DexIgnore
    public static final boolean a(CharSequence charSequence, int i, CharSequence charSequence2, int i2, int i3, boolean z) {
        wg6.b(charSequence, "$this$regionMatchesImpl");
        wg6.b(charSequence2, "other");
        if (i2 < 0 || i < 0 || i > charSequence.length() - i3 || i2 > charSequence2.length() - i3) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!dj6.a(charSequence.charAt(i + i4), charSequence2.charAt(i2 + i4), z)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static final int a(CharSequence charSequence, char[] cArr, int i, boolean z) {
        boolean z2;
        wg6.b(charSequence, "$this$indexOfAny");
        wg6.b(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            int a2 = ci6.a(i, 0);
            int c2 = c(charSequence);
            if (a2 > c2) {
                return -1;
            }
            while (true) {
                char charAt = charSequence.charAt(a2);
                int length = cArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z2 = false;
                        break;
                    } else if (dj6.a(cArr[i2], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z2) {
                    return a2;
                }
                if (a2 == c2) {
                    return -1;
                }
                a2++;
            }
        } else {
            return ((String) charSequence).indexOf(nd6.a(cArr), i);
        }
    }

    @DexIgnore
    public static /* synthetic */ int a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2, int i3, Object obj) {
        return a(charSequence, charSequence2, i, i2, z, (i3 & 16) != 0 ? false : z2);
    }

    @DexIgnore
    public static final int a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2) {
        uh6 uh6;
        if (!z2) {
            uh6 = new wh6(ci6.a(i, 0), ci6.b(i2, charSequence.length()));
        } else {
            uh6 = ci6.c(ci6.b(i, c(charSequence)), ci6.a(i2, 0));
        }
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int a2 = uh6.a();
            int b2 = uh6.b();
            int c2 = uh6.c();
            if (c2 >= 0) {
                if (a2 > b2) {
                    return -1;
                }
            } else if (a2 < b2) {
                return -1;
            }
            while (true) {
                if (a(charSequence2, 0, charSequence, a2, charSequence2.length(), z)) {
                    return a2;
                }
                if (a2 == b2) {
                    return -1;
                }
                a2 += c2;
            }
        } else {
            int a3 = uh6.a();
            int b3 = uh6.b();
            int c3 = uh6.c();
            if (c3 >= 0) {
                if (a3 > b3) {
                    return -1;
                }
            } else if (a3 < b3) {
                return -1;
            }
            while (true) {
                if (xj6.a((String) charSequence2, 0, (String) charSequence, a3, charSequence2.length(), z)) {
                    return a3;
                }
                if (a3 == b3) {
                    return -1;
                }
                a3 += c3;
            }
        }
    }

    @DexIgnore
    public static /* synthetic */ int b(CharSequence charSequence, char c2, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = c(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return b(charSequence, c2, i, z);
    }

    @DexIgnore
    public static final int b(CharSequence charSequence, char c2, int i, boolean z) {
        wg6.b(charSequence, "$this$lastIndexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(c2, i);
        }
        return b(charSequence, new char[]{c2}, i, z);
    }

    @DexIgnore
    public static /* synthetic */ int a(CharSequence charSequence, char c2, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return a(charSequence, c2, i, z);
    }

    @DexIgnore
    public static /* synthetic */ int b(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = c(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return b(charSequence, str, i, z);
    }

    @DexIgnore
    public static final int a(CharSequence charSequence, char c2, int i, boolean z) {
        wg6.b(charSequence, "$this$indexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(c2, i);
        }
        return a(charSequence, new char[]{c2}, i, z);
    }

    @DexIgnore
    public static final int b(CharSequence charSequence, String str, int i, boolean z) {
        wg6.b(charSequence, "$this$lastIndexOf");
        wg6.b(str, "string");
        if (z || !(charSequence instanceof String)) {
            return a(charSequence, (CharSequence) str, i, 0, z, true);
        }
        return ((String) charSequence).lastIndexOf(str, i);
    }

    @DexIgnore
    public static /* synthetic */ int a(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return a(charSequence, str, i, z);
    }

    @DexIgnore
    public static /* synthetic */ ti6 b(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return b(charSequence, strArr, z, i);
    }

    @DexIgnore
    public static final int a(CharSequence charSequence, String str, int i, boolean z) {
        wg6.b(charSequence, "$this$indexOf");
        wg6.b(str, "string");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(str, i);
        }
        return a(charSequence, str, i, charSequence.length(), z, false, 16, (Object) null);
    }

    @DexIgnore
    public static final ti6<String> b(CharSequence charSequence, String[] strArr, boolean z, int i) {
        wg6.b(charSequence, "$this$splitToSequence");
        wg6.b(strArr, "delimiters");
        return aj6.c(a(charSequence, strArr, 0, z, i, 2, (Object) null), new c(charSequence));
    }

    @DexIgnore
    public static /* synthetic */ boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return a(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        wg6.b(charSequence, "$this$contains");
        wg6.b(charSequence2, "other");
        if (charSequence2 instanceof String) {
            if (a(charSequence, (String) charSequence2, 0, z, 2, (Object) null) >= 0) {
                return true;
            }
        } else {
            if (a(charSequence, charSequence2, 0, charSequence.length(), z, false, 16, (Object) null) >= 0) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static /* synthetic */ ti6 a(CharSequence charSequence, char[] cArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return a(charSequence, cArr, i, z, i2);
    }

    @DexIgnore
    public static final ti6<wh6> a(CharSequence charSequence, char[] cArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new fj6(charSequence, i, i2, new a(cArr, z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    @DexIgnore
    public static /* synthetic */ ti6 a(CharSequence charSequence, String[] strArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return a(charSequence, strArr, i, z, i2);
    }

    @DexIgnore
    public static final ti6<wh6> a(CharSequence charSequence, String[] strArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new fj6(charSequence, i, i2, new b(md6.b(strArr), z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    @DexIgnore
    public static /* synthetic */ List a(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return a(charSequence, strArr, z, i);
    }

    @DexIgnore
    public static final List<String> a(CharSequence charSequence, String[] strArr, boolean z, int i) {
        wg6.b(charSequence, "$this$split");
        wg6.b(strArr, "delimiters");
        if (strArr.length == 1) {
            boolean z2 = false;
            String str = strArr[0];
            if (str.length() == 0) {
                z2 = true;
            }
            if (!z2) {
                return a(charSequence, str, z, i);
            }
        }
        Iterable<wh6> b2 = aj6.b(a(charSequence, strArr, 0, z, i, 2, (Object) null));
        ArrayList arrayList = new ArrayList(rd6.a(b2, 10));
        for (wh6 a2 : b2) {
            arrayList.add(a(charSequence, a2));
        }
        return arrayList;
    }

    @DexIgnore
    public static /* synthetic */ ti6 a(CharSequence charSequence, char[] cArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return a(charSequence, cArr, z, i);
    }

    @DexIgnore
    public static final ti6<String> a(CharSequence charSequence, char[] cArr, boolean z, int i) {
        wg6.b(charSequence, "$this$splitToSequence");
        wg6.b(cArr, "delimiters");
        return aj6.c(a(charSequence, cArr, 0, z, i, 2, (Object) null), new d(charSequence));
    }

    @DexIgnore
    public static final List<String> a(CharSequence charSequence, String str, boolean z, int i) {
        int i2 = 0;
        if (i >= 0) {
            int a2 = a(charSequence, str, 0, z);
            if (a2 == -1 || i == 1) {
                return pd6.a(charSequence.toString());
            }
            boolean z2 = i > 0;
            int i3 = 10;
            if (z2) {
                i3 = ci6.b(i, 10);
            }
            ArrayList arrayList = new ArrayList(i3);
            do {
                arrayList.add(charSequence.subSequence(i2, a2).toString());
                i2 = str.length() + a2;
                if ((z2 && arrayList.size() == i - 1) || (a2 = a(charSequence, str, i2, z)) == -1) {
                    arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
                }
                arrayList.add(charSequence.subSequence(i2, a2).toString());
                i2 = str.length() + a2;
                break;
            } while ((a2 = a(charSequence, str, i2, z)) == -1);
            arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }
}
