package com.fossil;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.fossil.rv1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jx1 implements gy1 {
    @DexIgnore
    public /* final */ jy1 a;
    @DexIgnore
    public boolean b; // = false;

    @DexIgnore
    public jx1(jy1 jy1) {
        this.a = jy1;
    }

    @DexIgnore
    public final <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(T t) {
        try {
            this.a.r.y.a(t);
            ay1 ay1 = this.a.r;
            rv1.f fVar = ay1.p.get(t.i());
            w12.a(fVar, (Object) "Appropriate Api was not requested.");
            if (fVar.c() || !this.a.g.containsKey(t.i())) {
                boolean z = fVar instanceof b22;
                rv1.b bVar = fVar;
                if (z) {
                    bVar = ((b22) fVar).H();
                }
                t.b(bVar);
                return t;
            }
            t.c(new Status(17));
            return t;
        } catch (DeadObjectException unused) {
            this.a.a((iy1) new mx1(this, this));
        }
    }

    @DexIgnore
    public final void a(gv1 gv1, rv1<?> rv1, boolean z) {
    }

    @DexIgnore
    public final <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T b(T t) {
        a(t);
        return t;
    }

    @DexIgnore
    public final void c() {
    }

    @DexIgnore
    public final void d() {
        if (this.b) {
            this.b = false;
            this.a.r.y.a();
            a();
        }
    }

    @DexIgnore
    public final void f(Bundle bundle) {
    }

    @DexIgnore
    public final void g(int i) {
        this.a.a((gv1) null);
        this.a.s.a(i, this.b);
    }

    @DexIgnore
    public final void b() {
        if (this.b) {
            this.b = false;
            this.a.a((iy1) new lx1(this, this));
        }
    }

    @DexIgnore
    public final boolean a() {
        if (this.b) {
            return false;
        }
        if (this.a.r.p()) {
            this.b = true;
            for (gz1 a2 : this.a.r.x) {
                a2.a();
            }
            return false;
        }
        this.a.a((gv1) null);
        return true;
    }
}
