package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cz4 implements Factory<bz4> {
    @DexIgnore
    public static /* final */ cz4 a; // = new cz4();

    @DexIgnore
    public static cz4 a() {
        return a;
    }

    @DexIgnore
    public static bz4 b() {
        return new bz4();
    }

    @DexIgnore
    public bz4 get() {
        return b();
    }
}
