package com.fossil;

import java.io.Serializable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn3<T> extends jn3<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ jn3<? super T> forwardOrder;

    @DexIgnore
    public vn3(jn3<? super T> jn3) {
        jk3.a(jn3);
        this.forwardOrder = jn3;
    }

    @DexIgnore
    public int compare(T t, T t2) {
        return this.forwardOrder.compare(t2, t);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof vn3) {
            return this.forwardOrder.equals(((vn3) obj).forwardOrder);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return -this.forwardOrder.hashCode();
    }

    @DexIgnore
    public <E extends T> E max(E e, E e2) {
        return this.forwardOrder.min(e, e2);
    }

    @DexIgnore
    public <E extends T> E min(E e, E e2) {
        return this.forwardOrder.max(e, e2);
    }

    @DexIgnore
    public <S extends T> jn3<S> reverse() {
        return this.forwardOrder;
    }

    @DexIgnore
    public String toString() {
        return this.forwardOrder + ".reverse()";
    }

    @DexIgnore
    public <E extends T> E max(E e, E e2, E e3, E... eArr) {
        return this.forwardOrder.min(e, e2, e3, eArr);
    }

    @DexIgnore
    public <E extends T> E min(E e, E e2, E e3, E... eArr) {
        return this.forwardOrder.max(e, e2, e3, eArr);
    }

    @DexIgnore
    public <E extends T> E max(Iterator<E> it) {
        return this.forwardOrder.min(it);
    }

    @DexIgnore
    public <E extends T> E min(Iterator<E> it) {
        return this.forwardOrder.max(it);
    }

    @DexIgnore
    public <E extends T> E max(Iterable<E> iterable) {
        return this.forwardOrder.min(iterable);
    }

    @DexIgnore
    public <E extends T> E min(Iterable<E> iterable) {
        return this.forwardOrder.max(iterable);
    }
}
