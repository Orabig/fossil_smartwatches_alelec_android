package com.fossil;

import com.fossil.h60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class r51 extends tg6 implements hg6<byte[], h60> {
    @DexIgnore
    public r51(h60.c cVar) {
        super(1, cVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(h60.c.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/BiometricProfile;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((h60.c) this.receiver).a((byte[]) obj);
    }
}
