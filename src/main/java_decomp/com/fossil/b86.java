package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b86 implements l86 {
    @DexIgnore
    public int a;

    @DexIgnore
    public b86(int i) {
        this.a = i;
    }

    @DexIgnore
    public void a(String str, String str2, Throwable th) {
        if (isLoggable(str, 5)) {
            Log.w(str, str2, th);
        }
    }

    @DexIgnore
    public void b(String str, String str2, Throwable th) {
        if (isLoggable(str, 3)) {
            Log.d(str, str2, th);
        }
    }

    @DexIgnore
    public void c(String str, String str2, Throwable th) {
        if (isLoggable(str, 4)) {
            Log.i(str, str2, th);
        }
    }

    @DexIgnore
    public void d(String str, String str2, Throwable th) {
        if (isLoggable(str, 2)) {
            Log.v(str, str2, th);
        }
    }

    @DexIgnore
    public void e(String str, String str2, Throwable th) {
        if (isLoggable(str, 6)) {
            Log.e(str, str2, th);
        }
    }

    @DexIgnore
    public void i(String str, String str2) {
        c(str, str2, (Throwable) null);
    }

    @DexIgnore
    public boolean isLoggable(String str, int i) {
        return this.a <= i || Log.isLoggable(str, i);
    }

    @DexIgnore
    public void log(int i, String str, String str2) {
        a(i, str, str2, false);
    }

    @DexIgnore
    public void v(String str, String str2) {
        d(str, str2, (Throwable) null);
    }

    @DexIgnore
    public void w(String str, String str2) {
        a(str, str2, (Throwable) null);
    }

    @DexIgnore
    public b86() {
        this.a = 4;
    }

    @DexIgnore
    public void a(int i, String str, String str2, boolean z) {
        if (z || isLoggable(str, i)) {
            Log.println(i, str, str2);
        }
    }

    @DexIgnore
    public void d(String str, String str2) {
        b(str, str2, (Throwable) null);
    }

    @DexIgnore
    public void e(String str, String str2) {
        e(str, str2, (Throwable) null);
    }
}
