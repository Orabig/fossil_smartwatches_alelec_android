package com.fossil;

import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hq3 extends iq3 {
    @DexIgnore
    public /* final */ List<wp3<?>> componentsInCycle;

    @DexIgnore
    public hq3(List<wp3<?>> list) {
        super("Dependency cycle detected: " + Arrays.toString(list.toArray()));
        this.componentsInCycle = list;
    }

    @DexIgnore
    public List<wp3<?>> getComponentsInCycle() {
        return this.componentsInCycle;
    }
}
