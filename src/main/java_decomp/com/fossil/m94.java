package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m94 extends l94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ScrollView y;
    @DexIgnore
    public long z;

    /*
    static {
        B.put(2131362851, 1);
        B.put(2131362237, 2);
        B.put(2131362577, 3);
        B.put(2131362337, 4);
        B.put(2131362336, 5);
        B.put(2131362335, 6);
        B.put(2131362843, 7);
        B.put(2131362307, 8);
        B.put(2131362418, 9);
    }
    */

    @DexIgnore
    public m94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 10, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[2], objArr[8], objArr[6], objArr[5], objArr[4], objArr[9], objArr[3], objArr[7], objArr[1]);
        this.z = -1;
        this.y = objArr[0];
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
