package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;

import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.CustomizeWidget;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.APPEND)
public class WatchFacePreviewFragmentMapping extends WatchFacePreviewFragmentBinding {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131363218, 1);
        A.put(2131363097, 2);
        A.put(2131363071, 3);  // tv_accept
        A.put(2131362025, 4);
        A.put(2131362546, 5);
        A.put(2131362001, 6);
        A.put(2131363339, 7);  // wc_top
        A.put(2131363331, 8);  // wc_bottom
        A.put(2131363338, 9);  // wc_start
        A.put(2131363335, 10); // wc_end
    }
    */

    static {
        A.put(R.id.tv_complication_circle, 11);
        A.put(R.id.tv_complication_blank, 12);
        A.put(R.id.tv_complication_none, 13);
    }

    @DexReplace
    public WatchFacePreviewFragmentMapping(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 14, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexReplace
    public WatchFacePreviewFragmentMapping(jb jbVar, View view, Object[] objArr) {
        super(
                jbVar,
                view,
                0,
                (View) objArr[6],
                (ConstraintLayout) objArr[4],
                (ConstraintLayout) objArr[0],
                (ImageView) objArr[5],
                (TextView) objArr[3],
                (TextView) objArr[2],
                (TextView) objArr[1],
                (CustomizeWidget) objArr[8],  //bottom
                (CustomizeWidget) objArr[10], //end
                (CustomizeWidget) objArr[9],  //start
                (CustomizeWidget) objArr[7],  //top
                (RadioButton) objArr[11],
                (RadioButton) objArr[12],
                (RadioButton) objArr[13]);
        this.y = -1;
        super.q.setTag(null);
        a(view);
        f();
    }
}
