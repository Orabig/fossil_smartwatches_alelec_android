package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.background.BackgroundImgData;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.MetaData;
import com.portfolio.platform.data.model.diana.preset.RingStyle;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceHelper {
    @DexIgnore
    public static final List<WatchFaceWrapper> a(List<WatchFace> list, List<DianaPresetComplicationSetting> list2) {
        wg6.b(list, "$this$toListOfWatchFaceWrappers");
        ArrayList arrayList = new ArrayList();
        for (WatchFace b : list) {
            arrayList.add(b(b, list2));
        }
        return arrayList;
    }

    @DexIgnore
    @SuppressLint("ResourceType")
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r0v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0151  */
    public static final WatchFaceWrapper b(WatchFace watchFace, List<DianaPresetComplicationSetting> list) {
        int i;
        ArrayList<RingStyleItem> ringStyleItems;
        WatchFaceWrapper.MetaData metaData = null;
        WatchFaceWrapper.MetaData metaData2 = null;
        WatchFaceWrapper.MetaData metaData3 = null;
        WatchFaceWrapper.MetaData metaData4 = null;
        Drawable drawable = null;
        Drawable drawable2 = null;
        Drawable drawable3 = null;
        Drawable drawable4 = null;
        wg6.b(watchFace, "$this$toWatchFaceWrapper");
        String id = watchFace.getId();
        Drawable c = w6.c(PortfolioApp.get.instance(), 2131231278);
        Drawable drawable5 = null;
        if (c != null) {
            c.getIntrinsicHeight();
            Drawable c2 = w6.c(PortfolioApp.get.instance(), 2131231278);
            Integer valueOf = c2 != null ? c2.getMinimumWidth() : null;
            if (valueOf != null) {
                i = valueOf.intValue();
                int dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165379);
                Drawable b = FileHelper.a.b(watchFace.getPreviewUrl(), dimensionPixelSize, dimensionPixelSize);
                Drawable a = FileHelper.a.a(watchFace.getBackground().getData().getPreviewUrl(), i, i, watchFace.getWatchFaceType());
                int dimensionPixelSize2 = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165419);
                ringStyleItems = watchFace.getRingStyleItems();
                if (ringStyleItems == null) {
                    Drawable drawable6 = null;
                    Drawable drawable7 = null;
                    Drawable drawable8 = null;
                    WatchFaceWrapper.MetaData metaData5 = null;
                    WatchFaceWrapper.MetaData metaData6 = null;
                    WatchFaceWrapper.MetaData metaData7 = null;
                    WatchFaceWrapper.MetaData metaData8 = null;
                    for (RingStyleItem next : ringStyleItems) {
                        String component1 = next.component1();
                        RingStyle component2 = next.component2();
                        switch (component1.hashCode()) {
                            case -1383228885:
                                if (!component1.equals("bottom")) {
                                    break;
                                } else {
                                    Drawable b2 = FileHelper.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize2, dimensionPixelSize2);
                                    MetaData metadata = component2.getMetadata();
                                    if (metadata != null) {
                                        metaData7 = a(metadata);
                                    }
                                    drawable7 = b2;
                                    break;
                                }
                            case 115029:
                                if (!component1.equals("top")) {
                                    break;
                                } else {
                                    Drawable b3 = FileHelper.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize2, dimensionPixelSize2);
                                    MetaData metadata2 = component2.getMetadata();
                                    if (metadata2 != null) {
                                        metaData5 = a(metadata2);
                                    }
                                    drawable5 = b3;
                                    break;
                                }
                            case 3317767:
                                if (!component1.equals("left")) {
                                    break;
                                } else {
                                    Drawable b4 = FileHelper.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize2, dimensionPixelSize2);
                                    MetaData metadata3 = component2.getMetadata();
                                    if (metadata3 != null) {
                                        metaData8 = a(metadata3);
                                    }
                                    drawable8 = b4;
                                    break;
                                }
                            case 108511772:
                                if (!component1.equals("right")) {
                                    break;
                                } else {
                                    Drawable b5 = FileHelper.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize2, dimensionPixelSize2);
                                    MetaData metadata4 = component2.getMetadata();
                                    if (metadata4 != null) {
                                        metaData6 = a(metadata4);
                                    }
                                    drawable6 = b5;
                                    break;
                                }
                        }
                        WatchFace watchFace2 = watchFace;
                    }
                    drawable4 = drawable5;
                    drawable3 = drawable6;
                    drawable2 = drawable7;
                    drawable = drawable8;
                    metaData4 = metaData5;
                    metaData3 = metaData6;
                    metaData2 = metaData7;
                    metaData = metaData8;
                } else {
                    drawable4 = null;
                    drawable3 = null;
                    drawable2 = null;
                    drawable = null;
                    metaData4 = null;
                    metaData3 = null;
                    metaData2 = null;
                    metaData = null;
                }
                return WatchFaceWrapper.Companion.createBackgroundWrapper(id, b, a, drawable4, drawable3, drawable2, drawable, metaData4, metaData3, metaData2, metaData, watchFace.getName(), loadBackgroundConfig(watchFace, list), ai4.Companion.a(watchFace.getWatchFaceType()));
            }
        }
        i = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165417);
        int dimensionPixelSize3 = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165379);
        Drawable b6 = FileHelper.a.b(watchFace.getPreviewUrl(), dimensionPixelSize3, dimensionPixelSize3);
        Drawable a2 = FileHelper.a.a(watchFace.getBackground().getData().getPreviewUrl(), i, i, watchFace.getWatchFaceType());
        int dimensionPixelSize22 = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165419);
        ringStyleItems = watchFace.getRingStyleItems();
        if (ringStyleItems == null) {
        }
        return WatchFaceWrapper.Companion.createBackgroundWrapper(id, b6, a2, null, drawable3, drawable2, drawable, metaData4, metaData3, metaData2, metaData, watchFace.getName(), loadBackgroundConfig(watchFace, list), ai4.Companion.a(watchFace.getWatchFaceType()));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0030, code lost:
        if (r0 != null) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x003f, code lost:
        if (r0 != null) goto L_0x0043;
     */
    @DexReplace
    public static final BackgroundConfig loadBackgroundConfig(WatchFace watchFace, List<DianaPresetComplicationSetting> list) {
        String str = "";
        String str2;
        String str3;
        String str4;
        RingStyleItem t;
        RingStyleItem t2;
        RingStyleItem t3;
        RingStyleItem t4;
        wg6.b(watchFace, "$this$toBackgroundConfig");
        String str5 = "";
        if (watchFace.getWatchFaceType() == ai4.BACKGROUND.getValue()) {
            String url = watchFace.getBackground().getData().getUrl();
            if (url != null) {
                FileHelper fh = FileHelper.a;
                // String a = nm4.a(url);
                if (url != null && !TextUtils.isEmpty(url)) {
                    str = fh.readFile(url);
                }
            }
        } else {
            str = watchFace.getBackground().getData().getUrl();
        }
        if (list != null) {
            str4 = str5;
            str3 = str4;
            str2 = str3;
            for (DianaPresetComplicationSetting next : list) {
                String component1 = next.component1();
                String component2 = next.component2();
                RingStyleItem ringStyleItem = null;
                switch (component1.hashCode()) {
                    case -1383228885:
                        if (component1.equals("bottom") && (!wg6.a((Object) component2, (Object) "empty"))) {
                            ArrayList<RingStyleItem> ringStyleItems = watchFace.getRingStyleItems();
                            if (ringStyleItems != null) {
                                Iterator<RingStyleItem> it = ringStyleItems.iterator();
                                while (true) {
                                    if (it.hasNext()) {
                                        t = it.next();
                                        if (wg6.a((Object) ((RingStyleItem) t).getPosition(), (Object) "bottom")) {
                                            break;
                                        }
                                    } else {
                                        t = null;
                                        break;
                                    }
                                }
                                ringStyleItem = (RingStyleItem) t;
                            }
                            if (ringStyleItem == null) {
                                break;
                            } else {
                                FileHelper FileHelper2 = FileHelper.a;
                                String a2 = ringStyleItem.getRingStyle().getData().getUrl();
                                if (a2 != null && !TextUtils.isEmpty(a2)) {
                                    str3 = FileHelper2.readFile(a2);
                                }
                                break;
                            }
                        }
                    case 115029:
                        if (component1.equals("top") && (!wg6.a((Object) component2, (Object) "empty"))) {
                            ArrayList<RingStyleItem> ringStyleItems2 = watchFace.getRingStyleItems();
                            if (ringStyleItems2 != null) {
                                Iterator<RingStyleItem> it2 = ringStyleItems2.iterator();
                                while (true) {
                                    if (it2.hasNext()) {
                                        t2 = it2.next();
                                        if (wg6.a((Object) ((RingStyleItem) t2).getPosition(), (Object) "top")) {
                                            break;
                                        }
                                    } else {
                                        t2 = null;
                                        break;
                                    }
                                }
                                ringStyleItem = (RingStyleItem) t2;
                            }
                            if (ringStyleItem == null) {
                                break;
                            } else {
                                FileHelper FileHelper3 = FileHelper.a;
                                String a3 = ringStyleItem.getRingStyle().getData().getUrl();
                                if (a3 != null && !TextUtils.isEmpty(a3)) {
                                    str5 = FileHelper3.readFile(a3);
                                }
                                break;
                            }
                        }
                    case 3317767:
                        if (component1.equals("left") && (!wg6.a((Object) component2, (Object) "empty"))) {
                            ArrayList<RingStyleItem> ringStyleItems3 = watchFace.getRingStyleItems();
                            if (ringStyleItems3 != null) {
                                Iterator<RingStyleItem> it3 = ringStyleItems3.iterator();
                                while (true) {
                                    if (it3.hasNext()) {
                                        t3 = it3.next();
                                        if (wg6.a((Object) ((RingStyleItem) t3).getPosition(), (Object) "left")) {
                                            break;
                                        }
                                    } else {
                                        t3 = null;
                                        break;
                                    }
                                }
                                ringStyleItem = (RingStyleItem) t3;
                            }
                            if (ringStyleItem == null) {
                                break;
                            } else {
                                FileHelper FileHelper4 = FileHelper.a;
                                String a4 = ringStyleItem.getRingStyle().getData().getUrl();
                                if (a4 != null && !TextUtils.isEmpty(a4)) {
                                    str2 = FileHelper4.readFile(a4);
                                }
                                break;
                            }
                        }
                    case 108511772:
                        if (component1.equals("right") && (!wg6.a((Object) component2, (Object) "empty"))) {
                            ArrayList<RingStyleItem> ringStyleItems4 = watchFace.getRingStyleItems();
                            if (ringStyleItems4 != null) {
                                Iterator<RingStyleItem> it4 = ringStyleItems4.iterator();
                                while (true) {
                                    if (it4.hasNext()) {
                                        t4 = it4.next();
                                        if (wg6.a((Object) ((RingStyleItem) t4).getPosition(), (Object) "right")) {
                                            break;
                                        }
                                    } else {
                                        t4 = null;
                                        break;
                                    }
                                }
                                ringStyleItem = (RingStyleItem) t4;
                            }
                            if (ringStyleItem == null) {
                                break;
                            } else {
                                FileHelper FileHelper5 = FileHelper.a;
                                String a5 = ringStyleItem.getRingStyle().getData().getUrl();
                                if (a5 != null && !TextUtils.isEmpty(a5)) {
                                    str4 = FileHelper5.readFile(a5);
                                }
                                break;
                            }
                        }
                }
            }
        } else {
            str4 = str5;
            str3 = str4;
            str2 = str3;
        }
        return new BackgroundConfig(System.currentTimeMillis(), new BackgroundImgData(Constants.MAIN_BACKGROUND_NAME, str), new BackgroundImgData(Constants.TOP_BACKGROUND_NAME, str5), new BackgroundImgData(Constants.RIGHT_BACKGROUND_NAME, str4), new BackgroundImgData(Constants.BOTTOM_BACKGROUND_NAME, str3), new BackgroundImgData(Constants.LEFT_BACKGROUND_NAME, str2));
    }

    @DexIgnore
    public static final WatchFaceWrapper.MetaData a(MetaData metaData) {
        Integer num;
        Integer num2;
        Integer num3;
        wg6.b(metaData, "$this$toColorMetaData");
        Integer num4 = null;
        Exception e;
        try {
            num3 = Color.parseColor(metaData.getSelectedForegroundColor());
            try {
                num2 = Color.parseColor(metaData.getSelectedBackgroundColor());
                try {
                    num = Color.parseColor(metaData.getUnselectedForegroundColor());
                } catch (Exception e2) {
                    e = e2;
                    num = null;
                    e.printStackTrace();
                    return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
                }
                try {
                    num4 = Color.parseColor(metaData.getUnselectedBackgroundColor());
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
                }
            } catch (Exception e3) {
                e = e3;
                num2 = null;
                num = num2;
                e.printStackTrace();
                return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
            }
        } catch (Exception e4) {
            e = e4;
            num3 = null;
            num2 = null;
            num = num2;
            e.printStackTrace();
            return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
        }
        return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
    }
}
