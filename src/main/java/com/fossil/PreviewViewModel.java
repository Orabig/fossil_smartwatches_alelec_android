package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.lifecycle.MutableLiveData;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.OutputFormat;
import com.fossil.imagefilters.OutputSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.BitmapUtils;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.diana.preset.Data;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.MetaData;
import com.portfolio.platform.data.model.diana.preset.RingStyle;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewFragment;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class PreviewViewModel extends td {
    @DexIgnore
    public MutableLiveData<b> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<d> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<c> c; // = new MutableLiveData<>();
    @DexIgnore
    public FilterType d; // = FilterType.ORDERED_DITHERING;
    @DexIgnore
    public ArrayList<o35> e;
    @DexIgnore
    public Bitmap f;
    @DexIgnore
    public /* final */ WatchFaceRepository g;
    @DexIgnore
    public /* final */ RingStyleRepository h;

    @DexIgnore
    PreviewViewModel() {}

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ BitmapDrawable a;

        @DexIgnore
        public b(BitmapDrawable bitmapDrawable) {
            wg6.b(bitmapDrawable, ResourceManager.DRAWABLE);
            this.a = bitmapDrawable;
        }

        @DexIgnore
        public final BitmapDrawable a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ Drawable a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public c(Drawable drawable, String str, boolean z) {
            this.a = drawable;
            this.b = str;
            this.c = z;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final Drawable b() {
            return this.a;
        }

        @DexIgnore
        public final boolean c() {
            return this.c;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(Drawable drawable, String str, boolean z, int i, qg6 qg6) {
            this((i & 1) != 0 ? null : drawable, (i & 2) != 0 ? null : str, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public d(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(String str, boolean z, int i, qg6 qg6) {
            this((i & 1) != 0 ? null : str, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadBackgroundPhoto$1", f = "PreviewViewModel.kt", l = {59, 60}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadBackgroundPhoto$1$drawable$1", f = "PreviewViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super BitmapDrawable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $previewPhotoDir;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, String str, xe6 xe6) {
                super(2, xe6);
                this.this$0 = eVar;
                this.$previewPhotoDir = str;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, this.$previewPhotoDir, xe6);
                aVar.p$ = (il6) obj;
                return (xe6)aVar;
            }

            @DexIgnore
            public final Object invoke(il6 obj, xe6 obj2) {
                throw null;
                //    return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
            public final Object invokeSuspend(Object obj) {
                throw null;
            //     ff6.a();
            //     if (this.label == 0) {
            //         nc6.a(obj);
            //         BitmapUtils bitmapUtils = BitmapUtils.INSTANCE;
            //         String str = this.$previewPhotoDir;
            //         int i = this.this$0.$size;
            //         Bitmap decodeBitmapFromDirectory = bitmapUtils.decodeBitmapFromDirectory(str, i, i);
            //         if (decodeBitmapFromDirectory == null) {
            //             return null;
            //         }
            //         this.this$0.this$0.f = decodeBitmapFromDirectory;
            //         EInkImageFilter create = EInkImageFilter.create();
            //         wg6.a((Object) create, "EInkImageFilter.create()");
            //         FilterResult apply = create.apply(decodeBitmapFromDirectory, this.this$0.this$0.b(), false, false, new OutputSettings(480, 480, OutputFormat.BACKGROUND));
            //         wg6.a((Object) apply, "algorithm.apply(bitmap, \u2026e, false, outputSettings)");
            //         Bitmap preview = apply.getPreview();
            //         int i2 = this.this$0.$size;
            //         return new BitmapDrawable(PortfolioApp.get.instance().getResources(), Bitmap.createScaledBitmap(preview, i2, i2, false));
            //     }
            //     throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadBackgroundPhoto$1$previewPhotoDir$1", f = "PreviewViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends sf6 implements ig6<il6, xe6<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Context $context;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(Context context, xe6 xe6) {
                super(2, xe6);
                this.$context = context;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                b bVar = new b(this.$context, xe6);
                bVar.p$ = (il6) obj;
                return (xe6)bVar;
            }

            @DexIgnore
            public final Object invoke(il6 obj, xe6 obj2) {
                throw null;
                //    return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    StringBuilder sb = new StringBuilder();
                    Context context = this.$context;
                    wg6.a((Object) context, "context");
                    sb.append(context.getFilesDir());
                    sb.append(File.separator);
                    sb.append("previewPhoto");
                    return sb.toString();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(PreviewViewModel previewViewModel, int i, xe6 xe6) {
            super(2, xe6);
            this.this$0 = previewViewModel;
            this.$size = i;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$size, xe6);
            eVar.p$ = (il6) obj;
            return (xe6)eVar;
        }

        @DexIgnore
        public final Object invoke(il6 obj, xe6 obj2) {
            throw null;
            //return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v8, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x007b  */
        public final Object invokeSuspend(Object obj) {
            String str;
            BitmapDrawable bitmapDrawable;
            il6 il6;
            Context context;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                context = PortfolioApp.get.instance().getApplicationContext();
                dl6 b2 = zl6.b();
                b bVar = new b(context, (xe6) null);
                this.L$0 = il62;
                this.L$1 = context;
                this.label = 1;
                throw null;
                // Object a3 = gk6.a(b2, bVar, this);
                // if (a3 == a2) {
                //     return a2;
                // }
                // Object obj2 = a3;
                // il6 = il62;
                // obj = obj2;
            } else if (i == 1) {
                context = (Context) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                str = (String) this.L$2;
                Context context2 = (Context) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                bitmapDrawable = (BitmapDrawable) obj;
                if (bitmapDrawable != null) {
                    FLogger.INSTANCE.getLocal().d("PreviewViewModel", "load photo success");
                    im4 b3 = im4.b();
                    b3.a(str + this.$size + this.$size, bitmapDrawable);
                    throw null;
                    // this.this$0.c().b(new b(bitmapDrawable));
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str2 = (String) obj;
            dl6 b4 = zl6.b();
            a aVar = new a(this, str2, (xe6) null);
            this.L$0 = il6;
            this.L$1 = context;
            this.L$2 = str2;
            this.label = 2;
            throw null;
            // Object a4 = gk6.a(b4, aVar, this);
            // if (a4 == a2) {
            //     return a2;
            // }
            // str = str2;
            // obj = a4;
            // bitmapDrawable = (BitmapDrawable) obj;
            // if (bitmapDrawable != null) {
            // }
            // return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadComplicationSetting$1", f = "PreviewViewModel.kt", l = {120}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadComplicationSetting$1$1", f = "PreviewViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = fVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                throw null;
                // return aVar;
            }

            @DexIgnore
            public final Object invoke(il6 obj, xe6 obj2) {
                throw null;
                // return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    DianaComplicationRingStyle ringStylesBySerial = this.this$0.this$0.h.getRingStylesBySerial(PortfolioApp.get.instance().e());
                    if (ringStylesBySerial != null) {
                        throw null;
                        // gk4 gk4 = gk4.a;
                        // String previewUrl = ringStylesBySerial.getData().getPreviewUrl();
                        // int i = this.this$0.$size;
                        // Drawable b = gk4.b(previewUrl, i, i);
                        // String unselectedForegroundColor = ringStylesBySerial.getMetaData().getUnselectedForegroundColor();
                        // if (unselectedForegroundColor == null || b == null) {
                        //     this.this$0.this$0.d().a(new c((Drawable) null, (String) null, false, 3, (qg6) null));
                        // } else {
                        //     FLogger.INSTANCE.getLocal().d("PreviewViewModel", "loadComplicationSetting() success");
                        //     this.this$0.this$0.d().a(new c(b, unselectedForegroundColor, true));
                        // }
                    } else {
                        this.this$0.this$0.d().a(new c((Drawable) null, (String) null, false, 3, (qg6) null));
                    }
                    return cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(PreviewViewModel previewViewModel, int i, xe6 xe6) {
            super(2, xe6);
            this.this$0 = previewViewModel;
            this.$size = i;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, this.$size, xe6);
            fVar.p$ = (il6) obj;
            return (xe6)fVar;
        }

        @DexIgnore
        public final Object invoke(il6 obj, xe6 obj2) {
            throw null;
            //return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                a aVar = new a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$saveTheme$1", f = "PreviewViewModel.kt", l = {92}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $srcBitmap;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(Bitmap bitmap, xe6 xe6, g gVar) {
                super(2, xe6);
                this.$srcBitmap = bitmap;
                this.this$0 = gVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.$srcBitmap, xe6, this.this$0);
                aVar.p$ = (il6) obj;
                return (xe6)aVar;
            }

            @DexIgnore
            public final Object invoke(il6 obj, xe6 obj2) {
                throw null;
                //return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r5v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
            /* JADX WARNING: type inference failed for: r7v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
            /* JADX WARNING: type inference failed for: r1v14, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    String valueOf = String.valueOf(System.currentTimeMillis());
                    String str = valueOf + Constants.PHOTO_IMAGE_NAME_SUFFIX;
                    String str2 = valueOf + Constants.PHOTO_BINARY_NAME_SUFFIX;
                    StringBuilder sb = new StringBuilder();
                    Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                    wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    sb.append(applicationContext.getFilesDir());
                    sb.append(File.separator);
                    String sb2 = sb.toString();
                    EInkImageFilter create = EInkImageFilter.create();
                    wg6.a((Object) create, "EInkImageFilter.create()");
                    EInkImageFilter eInkImageFilter = create;
                    FilterResult apply = eInkImageFilter.apply(this.$srcBitmap, this.this$0.this$0.b(), false, false, OutputSettings.BACKGROUND);
                    wg6.a((Object) apply, "algorithm.apply(srcBitma\u2026utputSettings.BACKGROUND)");
                    OutputSettings outputSettings = new OutputSettings(480, 480, OutputFormat.BACKGROUND);
                    Bitmap bitmap = this.$srcBitmap;
                    FilterResult apply2 = eInkImageFilter.apply(bitmap.copy(bitmap.getConfig(), true), this.this$0.this$0.b(), false, false, outputSettings);
                    wg6.a((Object) apply2, "algorithm.apply(srcBitma\u2026e, false, outputSettings)");
                    Bitmap a = lw4.a(apply2.getPreview());
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.get.instance().getResources(), a);
                    im4.b().a(sb2 + valueOf + this.this$0.$size + this.this$0.$size, bitmapDrawable);
                    FLogger.INSTANCE.getLocal().d("PreviewViewModel", "save theme to repository");
                    WatchFaceRepository c = this.this$0.this$0.g;
                    Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                    wg6.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                    wg6.a((Object) a, "bitmap");
                    byte[] data = apply.getData();
                    wg6.a((Object) data, "thumbnailResult.data");
                    c.saveBackgroundPhoto(applicationContext2, sb2, str, a, str2, data);
                    PreviewViewModel previewViewModel = this.this$0.this$0;
                    String str3 = File.separator;
                    wg6.a((Object) str3, "File.separator");
                    throw null;
                    // previewViewModel.saveWatchFace(yj6.c(sb2, str3, (String) null, 2, (Object) null), str, str2);
                    // return cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(PreviewViewModel previewViewModel, int i, xe6 xe6) {
            super(2, xe6);
            this.this$0 = previewViewModel;
            this.$size = i;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, this.$size, xe6);
            gVar.p$ = (il6) obj;
            return (xe6)gVar;
        }

        @DexIgnore
        public final Object invoke(il6 obj, xe6 obj2) {
            throw null;
            //return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.f == null) {
                    FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveTheme() error mBitmap == null");
                    this.this$0.e().a(new d((String) null, false, 1, (qg6) null));
                    return cd6.a;
                }
                Bitmap a3 = this.this$0.f;
                if (a3 != null) {
                    if (a3.isRecycled()) {
                        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveWatchFace fail bitmap is recycled???");
                        this.this$0.e().a(new d((String) null, false, 1, (qg6) null));
                    } else {
                        dl6 b = zl6.b();
                        a aVar = new a(a3, (xe6) null, this);
                        this.L$0 = il6;
                        this.L$1 = a3;
                        this.label = 1;
                        throw null;
                        // if (gk6.a(b, aVar, this) == a2) {
                        //     return a2;
                        // }
                    }
                }
            } else if (i == 1) {
                Bitmap bitmap = (Bitmap) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public PreviewViewModel(WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository) {
        wg6.b(watchFaceRepository, "watchFaceRepository");
        wg6.b(ringStyleRepository, "ringStyleRepository");
        this.g = watchFaceRepository;
        this.h = ringStyleRepository;
    }

    @DexIgnore
    public final MutableLiveData<c> d() {
        return this.c;
    }

    @DexIgnore
    public final MutableLiveData<d> e() {
        return this.b;
    }

    @DexIgnore
    public void onCleared() {
        throw null;
        // jl6.a(ud.a(this), (CancellationException) null, 1, (Object) null);
        // Bitmap bitmap = this.f;
        // if (bitmap != null) {
        //     bitmap.recycle();
        // }
        // PreviewViewModel.super.onCleared();
    }

    @DexIgnore
    public final FilterType b() {
        return this.d;
    }

    @DexIgnore
    public final MutableLiveData<b> c() {
        return this.a;
    }

    @DexIgnore
    public final void b(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PreviewViewModel", "loadComplicationSetting() size=" + i);
        throw null;
        // rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new f(this, i, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void c(int i) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveTheme()");
        throw null;
        // rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new g(this, i, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(FilterType filterType) {
        wg6.b(filterType, "<set-?>");
        this.d = filterType;
    }

    @DexIgnore
    public final ArrayList<o35> a() {
        return this.e;
    }

    @DexIgnore
    public final void a(ArrayList<o35> arrayList) {
        this.e = arrayList;
    }

    @DexIgnore
    public final void a(int i) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "loadBackgroundPhoto()");
        throw null;
        // rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new e(this, i, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> a(DianaComplicationRingStyle dianaComplicationRingStyle) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "createPhotoRingStyleList()");
        String url = dianaComplicationRingStyle.getData().getUrl();
        String previewUrl = dianaComplicationRingStyle.getData().getPreviewUrl();
        String selectedBackgroundColor = dianaComplicationRingStyle.getMetaData().getSelectedBackgroundColor();
        String unselectedBackgroundColor = dianaComplicationRingStyle.getMetaData().getUnselectedBackgroundColor();
        String selectedForegroundColor = dianaComplicationRingStyle.getMetaData().getSelectedForegroundColor();
        String unselectedForegroundColor = dianaComplicationRingStyle.getMetaData().getUnselectedForegroundColor();
        ArrayList<o35> arrayList = this.e;
        ArrayList<RingStyleItem> arrayList2 = null;
        if (arrayList == null) {
            wg6.a();
            throw null;
        }
        if (!arrayList.isEmpty()) {
            arrayList2 = new ArrayList<>();
            ArrayList<o35> arrayList3 = this.e;
            if (arrayList3 != null) {
                for (o35 c2 : arrayList3) {
                    String c3 = c2.c();
                    if (c3 != null) {
                        switch (c3.hashCode()) {
                            case -1383228885:
                                if (!c3.equals("bottom")) {
                                    break;
                                } else {
                                    arrayList2.add(new RingStyleItem("bottom", new RingStyle("bottomRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                    break;
                                }
                            case 115029:
                                if (!c3.equals("top")) {
                                    break;
                                } else {
                                    arrayList2.add(new RingStyleItem("top", new RingStyle("topRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                    break;
                                }
                            case 3317767:
                                if (!c3.equals("left")) {
                                    break;
                                } else {
                                    arrayList2.add(new RingStyleItem("left", new RingStyle("leftRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                    break;
                                }
                            case 108511772:
                                if (!c3.equals("right")) {
                                    break;
                                } else {
                                    arrayList2.add(new RingStyleItem("right", new RingStyle("rightRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                    break;
                                }
                        }
                    }
                }
            }
        }
        return arrayList2;
    }

    @DexReplace
    public final void saveWatchFace(String directory, String filename, String binaryName) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveWatchFace");
        //List<DianaComplicationRingStyle> ringStyles = this.h.getRingStyles();
        //FLogger.INSTANCE.getLocal().v("PreviewViewModel","ringStyles: " + ringStyles.toString());

        DianaComplicationRingStyle ringStylesBySerial = this.h.getRingStylesBySerial(PortfolioApp.get.instance().e());

        if (ringStylesBySerial != null) {

            if (PreviewFragment.selectedComplication == PreviewFragment.SelectedComplication.NONE) {
                ringStylesBySerial.data = new Data(null, null);
                ringStylesBySerial.metaData = new MetaData(
                        /* selectedForegroundColor */ "#242424",
                        /* selectedBackgroundColor */ "#FFFFFF",
                        /* unselectedForegroundColor */ "#FFFFFF",
                        /* unselectedBackgroundColor */ "#636363"
                );
            } else if (PreviewFragment.selectedComplication == PreviewFragment.SelectedComplication.BLANK) {
                ringStylesBySerial.data = new Data(
                        /* previewUrl */ "file:///android_asset/background/blank_complication.png",
                        /* url        */ "file:///android_asset/background/blank_complication.bin");
                ringStylesBySerial.metaData = new MetaData(
                        /* selectedForegroundColor */ "#242424",
                        /* selectedBackgroundColor */ "#FFFFFF",
                        /* unselectedForegroundColor */ "#FFFFFF",
                        /* unselectedBackgroundColor */ "#636363"
                );
            }


            FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveWatchFace success: " + ringStylesBySerial);
            this.g.insertWatchFace(directory, filename, binaryName, a(ringStylesBySerial));
            this.b.a(new d(filename, true));
            return;
        }
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveWatchFace fail");
        this.b.a(new d((String) null, false, 1, (qg6) null));
    }
}
