package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.BitmapUtils;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class FileHelper {
    @DexIgnore
    public static /* final */ FileHelper a = new FileHelper();

    @DexIgnore
    public final boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            if (str != null) {
                return new File(str).exists();
            }
            wg6.a();
            throw null;
        } catch (Exception unused) {
            return false;
        }
    }

    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0063, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        com.fossil.yf6.a(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0067, code lost:
        throw r2;
     */
    @DexReplace
    public final String readFile(String str) {
        wg6.b(str, "nameFile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FileHelper", "open nameFile " + str);
        String str2 = "";
        if (!TextUtils.isEmpty(str)) {
            try {
                InputStream inputStream;
                AssetFileDescriptor ap = assetPath(str);
                if (ap != null) {
                    inputStream = ap.createInputStream();
                } else {
                    File filesDir = PortfolioApp.get.instance().getFilesDir();
                    inputStream = new FileInputStream(new File(filesDir + File.separator + nm4.a(str)));
                }
                String encodeToString = Base64.encodeToString(vu6.b(inputStream), 0);
                yf6.a(inputStream, (Throwable) null);
                str2 = encodeToString;
            } catch (Exception e) {
                e.printStackTrace();
            }
            wg6.a((Object) str2, "try {\n                va\u2026         \"\"\n            }");
        }
        return str2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0041 A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0046 A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x004d A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0052 A[Catch:{ IOException -> 0x0056 }] */
    public final boolean a(zq6 zq6, String str) {
        throw null;
        // InputStream inputStream;
        // wg6.b(zq6, "body");
        // wg6.b(str, "path");
        // FileOutputStream fileOutputStream = null;
        // try {
        //     byte[] bArr = new byte[4096];
        //     inputStream = zq6.byteStream();
        //     try {
        //         FileOutputStream fileOutputStream2 = new FileOutputStream(str);
        //         if (inputStream != null) {
        //             while (true) {
        //                 try {
        //                     int read = inputStream.read(bArr);
        //                     if (read == -1) {
        //                         break;
        //                     }
        //                     fileOutputStream2.write(bArr, 0, read);
        //                 } catch (IOException unused) {
        //                     fileOutputStream = fileOutputStream2;
        //                     if (inputStream != null) {
        //                     }
        //                     if (fileOutputStream != null) {
        //                     }
        //                     return false;
        //                 } catch (Throwable th) {
        //                     th = th;
        //                     fileOutputStream = fileOutputStream2;
        //                     if (inputStream != null) {
        //                     }
        //                     if (fileOutputStream != null) {
        //                     }
        //                     throw th;
        //                 }
        //             }
        //         }
        //         fileOutputStream2.flush();
        //         if (inputStream != null) {
        //             try {
        //                 inputStream.close();
        //             } catch (IOException unused2) {
        //                 return false;
        //             }
        //         }
        //         fileOutputStream2.close();
        //         return true;
        //     } catch (IOException unused3) {
        //         if (inputStream != null) {
        //         }
        //         if (fileOutputStream != null) {
        //         }
        //         return false;
        //     } catch (Throwable th2) {
        //         th = th2;
        //         if (inputStream != null) {
        //         }
        //         if (fileOutputStream != null) {
        //         }
        //         throw th;
        //     }
        // } catch (IOException unused4) {
        //     inputStream = null;
        //     if (inputStream != null) {
        //         inputStream.close();
        //     }
        //     if (fileOutputStream != null) {
        //         fileOutputStream.close();
        //     }
        //     return false;
        // } catch (Throwable th3) {
        //     th = th3;
        //     inputStream = null;
        //     if (inputStream != null) {
        //         inputStream.close();
        //     }
        //     if (fileOutputStream != null) {
        //         fileOutputStream.close();
        //     }
        //     throw th;
        // }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final Drawable b(String str, int i, int i2) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        BitmapDrawable b = im4.b().b(str);
        if (b != null) {
            return b;
        }
        try {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.get.instance().getResources(), loadBitmap(str, i, i2));
            im4.b().a(str, bitmapDrawable);
            return bitmapDrawable;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final Drawable a(String str, int i, int i2, int i3) {
        String str2;
        Bitmap bitmap;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (i3 == ai4.BACKGROUND.getValue()) {
            str2 = str;
        } else if (i3 == ai4.PHOTO.getValue()) {
            str2 = str + i + i2;
        } else {
            str2 = null;
        }
        BitmapDrawable b = im4.b().b(str2);
        if (b != null) {
            return b;
        }
        try {
            if (i3 == ai4.BACKGROUND.getValue()) {
                bitmap = loadBitmap(str, i, i2);
            } else if (i3 == ai4.PHOTO.getValue()) {
                BitmapUtils bitmapUtils = BitmapUtils.INSTANCE;
                if (str != null) {
                    bitmap = bitmapUtils.decodeBitmapFromDirectory(str, i, i2);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                bitmap = null;
            }
            if (bitmap == null) {
                return null;
            }
            BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.get.instance().getResources(), Bitmap.createScaledBitmap(bitmap, i, i2, false));
            im4.b().a(str2, bitmapDrawable);
            return bitmapDrawable;
        } catch (Exception unused) {
            return null;
        }
    }

    @DexAdd
    public AssetFileDescriptor assetPath(String path) throws IOException {
        Uri uri = Uri.parse(path);
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return null;
        }
        AssetManager am = PortfolioApp.get.instance().getAssets();
        String ap = uri.getPath().substring("android_asset".length()+2);
        FLogger.INSTANCE.getLocal().d("FileHelper", "open asset " + ap);
        return am.openFd(ap);
        // return true;
    }


    @DexReplace
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final Bitmap loadBitmap(String path, int i, int i2) {
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        File filesDir = PortfolioApp.get.instance().getFilesDir();

        InputStream stream;

        try {
            AssetFileDescriptor af = assetPath(path);
            if (af != null) {
                stream = af.createInputStream();
            } else {
                String filePath =  filesDir + File.separator + nm4.a(path);
                FLogger.INSTANCE.getLocal().d("FileHelper", "open file " + filePath);
                stream = new FileInputStream(new File(filePath));
            }
            return lk4.a(stream, i, i2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
