package com.portfolio.platform.helper;

import android.os.Build;
import android.text.TextUtils;
import com.fossil.an4;
import com.fossil.ck4;
import com.fossil.kc6;
import com.fossil.qg6;
import com.fossil.qh4;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xj6;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceHelper {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static float[] e;
    @DexIgnore
    public static /* final */ String[] f; // = {"HW.0.0", "HL.0.0", "HM.0.0", "DN.0.0", "DN.1.0"};
    @DexIgnore
    public static DeviceHelper g;
    @DexIgnore
    public static /* final */ MFDeviceFamily[] h; // = {MFDeviceFamily.DEVICE_FAMILY_SAM, MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM, MFDeviceFamily.DEVICE_FAMILY_SAM_MINI, MFDeviceFamily.DEVICE_FAMILY_RMM};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] i; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI, FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM, FossilDeviceSerialPatternUtil.DEVICE.DIANA};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] j; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI, FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION, FossilDeviceSerialPatternUtil.DEVICE.DIANA};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] k; // = {FossilDeviceSerialPatternUtil.DEVICE.RMM, FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] l; // = {FossilDeviceSerialPatternUtil.DEVICE.DIANA};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] m; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI};
    @DexIgnore
    public static /* final */ FossilDeviceSerialPatternUtil.DEVICE[] n; // = {FossilDeviceSerialPatternUtil.DEVICE.SAM, FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM, FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI, FossilDeviceSerialPatternUtil.DEVICE.DIANA};
    @DexIgnore
    public static /* final */ a o; // = new a(null);
    @DexIgnore
    public an4 a;
    @DexIgnore
    public DeviceRepository b;
    @DexIgnore
    public List<String> c; // = new ArrayList();

    @DexIgnore
    public enum ImageStyle {
        SMALL(0),
        NORMAL(1),
        LARGE(2),
        HYBRID_WATCH_HOUR(3),
        HYBRID_WATCH_MINUTE(4),
        HYBRID_WATCH_SUBEYE(5),
        DIANA_WATCH_HOUR(6),
        DIANA_WATCH_MINUTE(7),
        WATCH_COMPLETED(8);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        ImageStyle(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(int i) {
            if (i < 25) {
                return 2131231049;
            }
            if (i < 50) {
                return 2131231051;
            }
            return i < 75 ? 2131231053 : 2131231047;
        }

        @DexIgnore
        public final void a(float[] fArr) {
            DeviceHelper.e = fArr;
        }

        @DexIgnore
        public final String[] b() {
            return DeviceHelper.f;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] c() {
            return DeviceHelper.l;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] d() {
            return DeviceHelper.m;
        }

        @DexIgnore
        public final synchronized DeviceHelper e() {
            DeviceHelper f;
            if (DeviceHelper.o.f() == null) {
                DeviceHelper.o.a(new DeviceHelper());
            }
            f = DeviceHelper.o.f();
            if (f == null) {
                wg6.a();
                throw null;
            }
            return f;
        }

        @DexIgnore
        public final DeviceHelper f() {
            return DeviceHelper.g;
        }

        @DexIgnore
        public final float[] g() {
            return DeviceHelper.e;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] h() {
            return DeviceHelper.n;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] i() {
            return DeviceHelper.i;
        }

        @DexIgnore
        public final FossilDeviceSerialPatternUtil.DEVICE[] j() {
            return DeviceHelper.k;
        }

        @DexIgnore
        public final void k() {
            a(new float[71]);
            int i = 0;
            while (i <= 20) {
                float[] g = g();
                if (g != null) {
                    g[i] = 0.005f * ((float) i) * 3.28f;
                    i++;
                } else {
                    wg6.a();
                    throw null;
                }
            }
            while (i <= 35) {
                float[] g2 = g();
                if (g2 != null) {
                    g2[i] = ((((float) (i - 20)) * 0.06f) + 0.1f) * 3.28f;
                    i++;
                } else {
                    wg6.a();
                    throw null;
                }
            }
            while (i <= 50) {
                float[] g3 = g();
                if (g3 != null) {
                    g3[i] = ((((float) (i - 35)) * 0.06666667f) + 1.0f) * 3.28f;
                    i++;
                } else {
                    wg6.a();
                    throw null;
                }
            }
            while (i <= 60) {
                float[] g4 = g();
                if (g4 != null) {
                    g4[i] = ((((float) (i - 50)) * 0.1f) + 2.0f) * 3.28f;
                    i++;
                } else {
                    wg6.a();
                    throw null;
                }
            }
            while (i <= 63) {
                float[] g5 = g();
                if (g5 != null) {
                    g5[i] = ((((float) (i - 60)) * 0.33333334f) + 3.0f) * 3.28f;
                    i++;
                } else {
                    wg6.a();
                    throw null;
                }
            }
            while (i <= 70) {
                float[] g6 = g();
                if (g6 != null) {
                    g6[i] = ((((float) (i - 60)) * 0.14285715f) + 4.0f) * 3.28f;
                    i++;
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public final boolean l() {
            return Build.VERSION.SDK_INT >= 26;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(DeviceHelper deviceHelper) {
            DeviceHelper.g = deviceHelper;
        }

        @DexIgnore
        public final float b(int i) {
            if (g() == null) {
                k();
            }
            int i2 = (-i) - 30;
            if (i2 >= 0) {
                float[] g = g();
                if (g == null) {
                    wg6.a();
                    throw null;
                } else if (i2 < g.length) {
                    float[] g2 = g();
                    if (g2 != null) {
                        return g2[i2];
                    }
                    wg6.a();
                    throw null;
                }
            }
            return i2 < 0 ? 0.0f : 16.4f;
        }

        @DexIgnore
        public final List<String> c(String str) {
            wg6.b(str, "serial");
            throw null;
            // ArrayList arrayList = new ArrayList();
            // FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
            // if (deviceBySerial != null) {
            //     int i = ck4.b[deviceBySerial.ordinal()];
            //     if (i == 1) {
            //         arrayList.add("HW.0.0");
            //     } else if (i == 2) {
            //         arrayList.add("HM.0.0");
            //     } else if (i == 3) {
            //         arrayList.add("HL.0.0");
            //     } else if (i == 4) {
            //         arrayList.add("DN.0.0");
            //         arrayList.add("DN.1.0");
            //     }
            // }
            // return arrayList;
        }

        @DexIgnore
        public final boolean d(String str) {
            wg6.b(str, "serial");
            return a(str, i());
        }

        @DexIgnore
        public final boolean f(String str) {
            wg6.b(str, "serial");
            return a(str, c());
        }

        @DexIgnore
        public final boolean g(String str) {
            wg6.b(str, "serial");
            return a(str, d());
        }

        @DexIgnore
        public final boolean h(String str) {
            wg6.b(str, "serial");
            return a(str, j());
        }

        @DexIgnore
        public final MFDeviceFamily[] a() {
            return DeviceHelper.h;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
         arg types: [com.misfit.frameworks.buttonservice.enums.MFDeviceFamily, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
        @DexIgnore
        public final MFDeviceFamily a(String str) {
            wg6.b(str, "serial");
            MFDeviceFamily deviceFamily = DeviceIdentityUtils.getDeviceFamily(str);
            wg6.a((Object) deviceFamily, "DeviceIdentityUtils.getDeviceFamily(serial)");
            return deviceFamily;
        }

        @DexIgnore
        public final boolean e(String str) {
            wg6.b(str, "serial");
            return a(str, h());
        }

        @DexIgnore
        public final boolean a(String str, FossilDeviceSerialPatternUtil.DEVICE[] deviceArr) {
            wg6.b(str, "serial");
            wg6.b(deviceArr, "supportedDevices");
            FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
            for (FossilDeviceSerialPatternUtil.DEVICE device : deviceArr) {
                if (device == deviceBySerial) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public final int a(String str, ImageStyle imageStyle) {
            if (!e(str)) {
                return 2131231364;
            }
            throw null;
            // switch (ck4.a[imageStyle.ordinal()]) {
            //     case 1:
            //         return 2131231366;
            //     case 2:
            //         return 2131231365;
            //     case 3:
            //     case 4:
            //         return 2131231364;
            //     case 5:
            //         return 2131231010;
            //     case 6:
            //         return 2131231011;
            //     case 7:
            //         return 2131231012;
            //     case 8:
            //         return 2131230986;
            //     case 9:
            //         return 2131230987;
            //     default:
            //         throw new kc6();
            // }
        }

        @DexIgnore
        public final int b(String str, ImageStyle imageStyle) {
            wg6.b(str, "serial");
            wg6.b(imageStyle, "style");
            return a(str, imageStyle);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
        @DexIgnore
        public final String b(String str) {
            if (TextUtils.isEmpty(str)) {
                return "";
            }
            if (str == null) {
                wg6.a();
                throw null;
            } else if (str.length() < 5) {
                return "";
            } else {
                if (FossilDeviceSerialPatternUtil.isQMotion(str)) {
                    String substring = str.substring(0, 5);
                    wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    return substring;
                }
                String substring2 = str.substring(0, 6);
                wg6.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                return substring2;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /*
    static {
        String simpleName = DeviceHelper.class.getSimpleName();
        wg6.a((Object) simpleName, "DeviceHelper::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public DeviceHelper() {
        PortfolioApp.get.instance().g().a(this);
        a();
    }

    @DexIgnore
    public final boolean b(String str) {
        wg6.b(str, "serial");
        return o.a(str, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @DexIgnore
    public final void a() {
        this.c.clear();
        if (!xj6.b("release", "release", true)) {
            an4 an4 = this.a;
            if (an4 == null) {
                wg6.d("sharedPreferencesManager");
                throw null;
            } else if (an4.U() || wg6.a((Object) PortfolioApp.get.instance().i(), (Object) qh4.PORTFOLIO.getName())) {
                this.c.add(DeviceIdentityUtils.RAY_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX);
                List<String> list = this.c;
                String[] strArr = DeviceIdentityUtils.Q_MOTION_PREFIX;
                List asList = Arrays.asList((String[]) Arrays.copyOf(strArr, strArr.length));
                wg6.a((Object) asList, "Arrays.asList(*DeviceIde\u2026ityUtils.Q_MOTION_PREFIX)");
                list.addAll(asList);
                this.c.add(DeviceIdentityUtils.RMM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.FAKE_SAM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.SAM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.SAM_SLIM_SERIAL_NUMBER_PREFIX);
                this.c.add(DeviceIdentityUtils.SAM_DIANA_SERIAL_NUMBER_PREFIX);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        StringBuilder sb = new StringBuilder();
        sb.append("debug=");
        sb.append(PortfolioApp.get.e());
        sb.append(", BUILD_TYPE=");
        sb.append("release");
        sb.append(", filterList=");
        Object[] array = this.c.toArray(new String[0]);
        if (array != null) {
            sb.append(Arrays.toString(array));
            local.d(str, sb.toString());
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0047, code lost:
        if (r0.U() == false) goto L_0x0050;
     */
    @DexIgnore
    public final boolean a(String str, List<SKUModel> list) {
        SKUModel t;
        wg6.b(str, "serial");
        wg6.b(list, "allSkuModel");
        if (!o.e(str)) {
            FLogger.INSTANCE.getLocal().d(d, "device " + str + " is not supported");
            return false;
        }
        if (PortfolioApp.get.e()) {
            an4 an4 = this.a;
            if (an4 == null) {
                wg6.d("sharedPreferencesManager");
                throw null;
            }
        }
        if (!wg6.a((Object) PortfolioApp.get.instance().i(), (Object) qh4.PORTFOLIO.getName())) {
            FLogger.INSTANCE.getLocal().d(d, "prefix " + o.b(str));
            Iterator<SKUModel> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                SKUModel sKUModel = (SKUModel) t;
                FLogger.INSTANCE.getLocal().d(d, "compare sku " + sKUModel + " to " + str);
                if (wg6.a((Object) o.b(str), (Object) sKUModel.getSerialNumberPrefix())) {
                    break;
                }
            }
            if (((SKUModel) t) != null) {
                return true;
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final MisfitDeviceProfile a(String str) {
        String str2 = str;
        wg6.b(str2, "serial");
        IButtonConnectivity b2 = PortfolioApp.get.b();
        MisfitDeviceProfile misfitDeviceProfile = null;
        Exception e = null;
        if (b2 == null) {
            return null;
        }
        try {
            MisfitDeviceProfile deviceProfile = b2.getDeviceProfile(str2);
            if (deviceProfile == null) {
                try {
                    DeviceRepository deviceRepository = this.b;
                    if (deviceRepository != null) {
                        Device deviceBySerial = deviceRepository.getDeviceBySerial(str2);
                        if (deviceBySerial != null) {
                            String macAddress = deviceBySerial.getMacAddress();
                            if (macAddress != null) {
                                String productDisplayName = deviceBySerial.getProductDisplayName();
                                if (productDisplayName != null) {
                                    String deviceId = deviceBySerial.getDeviceId();
                                    String sku = deviceBySerial.getSku();
                                    if (sku != null) {
                                        String firmwareRevision = deviceBySerial.getFirmwareRevision();
                                        if (firmwareRevision != null) {
                                            return new MisfitDeviceProfile(macAddress, productDisplayName, deviceId, sku, firmwareRevision, deviceBySerial.getBatteryLevel(), "", 0, 0, (short) deviceBySerial.getMajor(), (short) deviceBySerial.getMinor(), "");
                                        }
                                        wg6.a();
                                        throw null;
                                    }
                                    wg6.a();
                                    throw null;
                                }
                                wg6.a();
                                throw null;
                            }
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.d("mDeviceRepository");
                        throw null;
                    }
                } catch (Exception e2) {
                    e = e2;
                    misfitDeviceProfile = deviceProfile;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = d;
                    local.e(str3, "getDeviceProfileFromSerial exception=" + e);
                    return misfitDeviceProfile;
                }
            }
            return deviceProfile;
        } catch (Exception e3) {
            e = e3;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str32 = d;
            local2.e(str32, "getDeviceProfileFromSerial exception=" + e);
            return misfitDeviceProfile;
        }
    }
}
