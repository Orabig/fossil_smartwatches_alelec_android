package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.WatchFaceHelper;
import com.fossil.af6;
import com.fossil.ai4;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.fh6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.q65;
import com.fossil.qg6;
import com.fossil.r65;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeThemePresenter extends q65 {
    @DexIgnore
    public DianaCustomizeViewModel e;
    @DexIgnore
    public LiveData<List<WatchFace>> f;
    @DexIgnore
    public List<WatchFace> g; // = new ArrayList();
    @DexIgnore
    public int h;
    @DexIgnore
    public String i;
    @DexIgnore
    public DianaPreset j;
    @DexIgnore
    public String k;
    @DexIgnore
    public ld<String> l; // = new c(this);
    @DexIgnore
    public ld<List<WatchFace>> m; // = new f(this);
    @DexIgnore
    public /* final */ r65 n;
    @DexIgnore
    public /* final */ WatchFaceRepository o;
    @DexIgnore
    public /* final */ DianaPresetRepository p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    // @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    // @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$checkToRemovePhoto$1", f = "CustomizeThemePresenter.kt", l = {240}, m = "invokeSuspend")
    // public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //     @DexIgnore
    //     public /* final */ /* synthetic */ WatchFaceWrapper $watchFaceWrapper;
    //     @DexIgnore
    //     public Object L$0;
    //     @DexIgnore
    //     public Object L$1;
    //     @DexIgnore
    //     public int label;
    //     @DexIgnore
    //     public il6 p$;
    //     @DexIgnore
    //     public /* final */ /* synthetic */ CustomizeThemePresenter this$0;
    //
    //     @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    //     @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$checkToRemovePhoto$1$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
    //     public static final class a extends sf6 implements ig6<il6, xe6<? super DianaPreset>, Object> {
    //         @DexIgnore
    //         public int label;
    //         @DexIgnore
    //         public il6 p$;
    //         @DexIgnore
    //         public /* final */ /* synthetic */ b this$0;
    //
    //         @DexIgnore
    //         /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //         public a(b bVar, xe6 xe6) {
    //             super(2, xe6);
    //             this.this$0 = bVar;
    //         }
    //
    //         @DexIgnore
    //         public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //             wg6.b(xe6, "completion");
    //             a aVar = new a(this.this$0, xe6);
    //             aVar.p$ = (il6) obj;
    //             return aVar;
    //         }
    //
    //         @DexIgnore
    //         public final Object invoke(Object obj, Object obj2) {
    //             return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //         }
    //
    //         @DexIgnore
    //         public final Object invokeSuspend(Object obj) {
    //             ff6.a();
    //             if (this.label == 0) {
    //                 nc6.a(obj);
    //                 return this.this$0.this$0.p.getActivePresetBySerial(PortfolioApp.get.instance().e());
    //             }
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //     }
    //
    //     @DexIgnore
    //     /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //     public b(CustomizeThemePresenter customizeThemePresenter, WatchFaceWrapper watchFaceWrapper, xe6 xe6) {
    //         super(2, xe6);
    //         this.this$0 = customizeThemePresenter;
    //         this.$watchFaceWrapper = watchFaceWrapper;
    //     }
    //
    //     @DexIgnore
    //     public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //         wg6.b(xe6, "completion");
    //         b bVar = new b(this.this$0, this.$watchFaceWrapper, xe6);
    //         bVar.p$ = (il6) obj;
    //         return bVar;
    //     }
    //
    //     @DexIgnore
    //     public final Object invoke(Object obj, Object obj2) {
    //         return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //     }
    //
    //     @DexIgnore
    //     public final Object invokeSuspend(Object obj) {
    //         CustomizeThemePresenter customizeThemePresenter;
    //         Object a2 = ff6.a();
    //         int i = this.label;
    //         if (i == 0) {
    //             nc6.a(obj);
    //             il6 il6 = this.p$;
    //             if (this.this$0.j == null) {
    //                 this.this$0.n.b();
    //                 CustomizeThemePresenter customizeThemePresenter2 = this.this$0;
    //                 dl6 d = customizeThemePresenter2.c();
    //                 a aVar = new a(this, (xe6) null);
    //                 this.L$0 = il6;
    //                 this.L$1 = customizeThemePresenter2;
    //                 this.label = 1;
    //                 obj = gk6.a(d, aVar, this);
    //                 if (obj == a2) {
    //                     return a2;
    //                 }
    //                 customizeThemePresenter = customizeThemePresenter2;
    //             } else {
    //                 CustomizeThemePresenter customizeThemePresenter3 = this.this$0;
    //                 customizeThemePresenter3.a(customizeThemePresenter3.j, this.$watchFaceWrapper);
    //                 return cd6.a;
    //             }
    //         } else if (i == 1) {
    //             customizeThemePresenter = (CustomizeThemePresenter) this.L$1;
    //             il6 il62 = (il6) this.L$0;
    //             nc6.a(obj);
    //         } else {
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //         customizeThemePresenter.j = (DianaPreset) obj;
    //         this.this$0.n.a();
    //         CustomizeThemePresenter customizeThemePresenter4 = this.this$0;
    //         customizeThemePresenter4.a(customizeThemePresenter4.j, this.$watchFaceWrapper);
    //         return cd6.a;
    //     }
    // }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeThemePresenter a;

        @DexIgnore
        public c(CustomizeThemePresenter customizeThemePresenter) {
            this.a = customizeThemePresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemePresenter", "onLiveDataChanged SelectedCustomizeTheme value=" + str);
            if (str != null) {
                WatchFaceWrapper e = CustomizeThemePresenter.e(this.a).e(str);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("CustomizeThemePresenter", " show watchface " + e);
                if (e != null) {
                    this.a.n.a(str, e.getType());
                    this.a.a(e.getType());
                }
            }
        }
    }

    // @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    // @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$invalidateBackgroundDataWatchFace$2", f = "CustomizeThemePresenter.kt", l = {188}, m = "invokeSuspend")
    // public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //     @DexIgnore
    //     public Object L$0;
    //     @DexIgnore
    //     public int label;
    //     @DexIgnore
    //     public il6 p$;
    //     @DexIgnore
    //     public /* final */ /* synthetic */ CustomizeThemePresenter this$0;
    //
    //     @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    //     @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$invalidateBackgroundDataWatchFace$2$1", f = "CustomizeThemePresenter.kt", l = {188}, m = "invokeSuspend")
    //     public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //         @DexIgnore
    //         public Object L$0;
    //         @DexIgnore
    //         public int label;
    //         @DexIgnore
    //         public il6 p$;
    //         @DexIgnore
    //         public /* final */ /* synthetic */ d this$0;
    //
    //         @DexIgnore
    //         /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //         public a(d dVar, xe6 xe6) {
    //             super(2, xe6);
    //             this.this$0 = dVar;
    //         }
    //
    //         @DexIgnore
    //         public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //             wg6.b(xe6, "completion");
    //             a aVar = new a(this.this$0, xe6);
    //             aVar.p$ = (il6) obj;
    //             return aVar;
    //         }
    //
    //         @DexIgnore
    //         public final Object invoke(Object obj, Object obj2) {
    //             return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //         }
    //
    //         @DexIgnore
    //         public final Object invokeSuspend(Object obj) {
    //             Object a = ff6.a();
    //             int i = this.label;
    //             if (i == 0) {
    //                 nc6.a(obj);
    //                 il6 il6 = this.p$;
    //                 WatchFaceRepository k = this.this$0.this$0.o;
    //                 String e = PortfolioApp.get.instance().e();
    //                 this.L$0 = il6;
    //                 this.label = 1;
    //                 if (k.getWatchFacesFromServer(e, this) == a) {
    //                     return a;
    //                 }
    //             } else if (i == 1) {
    //                 il6 il62 = (il6) this.L$0;
    //                 nc6.a(obj);
    //             } else {
    //                 throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //             }
    //             return cd6.a;
    //         }
    //     }
    //
    //     @DexIgnore
    //     /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //     public d(CustomizeThemePresenter customizeThemePresenter, xe6 xe6) {
    //         super(2, xe6);
    //         this.this$0 = customizeThemePresenter;
    //     }
    //
    //     @DexIgnore
    //     public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //         wg6.b(xe6, "completion");
    //         d dVar = new d(this.this$0, xe6);
    //         dVar.p$ = (il6) obj;
    //         return dVar;
    //     }
    //
    //     @DexIgnore
    //     public final Object invoke(Object obj, Object obj2) {
    //         return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //     }
    //
    //     @DexIgnore
    //     public final Object invokeSuspend(Object obj) {
    //         Object a2 = ff6.a();
    //         int i = this.label;
    //         if (i == 0) {
    //             nc6.a(obj);
    //             il6 il6 = this.p$;
    //             dl6 b = zl6.b();
    //             a aVar = new a(this, (xe6) null);
    //             this.L$0 = il6;
    //             this.label = 1;
    //             if (gk6.a(b, aVar, this) == a2) {
    //                 return a2;
    //             }
    //         } else if (i == 1) {
    //             il6 il62 = (il6) this.L$0;
    //             nc6.a(obj);
    //         } else {
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //         return cd6.a;
    //     }
    // }
    //
    // @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    // @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1", f = "CustomizeThemePresenter.kt", l = {197}, m = "invokeSuspend")
    // public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //     @DexIgnore
    //     public /* final */ /* synthetic */ WatchFaceWrapper $watchFaceWrapper;
    //     @DexIgnore
    //     public Object L$0;
    //     @DexIgnore
    //     public Object L$1;
    //     @DexIgnore
    //     public int label;
    //     @DexIgnore
    //     public il6 p$;
    //     @DexIgnore
    //     public /* final */ /* synthetic */ CustomizeThemePresenter this$0;
    //
    //     @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    //     @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1", f = "CustomizeThemePresenter.kt", l = {213, 222, 227}, m = "invokeSuspend")
    //     public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //         @DexIgnore
    //         public /* final */ /* synthetic */ String $id;
    //         @DexIgnore
    //         public Object L$0;
    //         @DexIgnore
    //         public Object L$1;
    //         @DexIgnore
    //         public Object L$2;
    //         @DexIgnore
    //         public Object L$3;
    //         @DexIgnore
    //         public Object L$4;
    //         @DexIgnore
    //         public Object L$5;
    //         @DexIgnore
    //         public int label;
    //         @DexIgnore
    //         public il6 p$;
    //         @DexIgnore
    //         public /* final */ /* synthetic */ e this$0;
    //
    //         @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$e$a$a")
    //         @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1$2", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
    //         /* renamed from: com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$e$a$a  reason: collision with other inner class name */
    //         public static final class C0065a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //             @DexIgnore
    //             public int label;
    //             @DexIgnore
    //             public il6 p$;
    //             @DexIgnore
    //             public /* final */ /* synthetic */ a this$0;
    //
    //             @DexIgnore
    //             /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //             public C0065a(a aVar, xe6 xe6) {
    //                 super(2, xe6);
    //                 this.this$0 = aVar;
    //             }
    //
    //             @DexIgnore
    //             public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //                 wg6.b(xe6, "completion");
    //                 C0065a aVar = new C0065a(this.this$0, xe6);
    //                 aVar.p$ = (il6) obj;
    //                 return aVar;
    //             }
    //
    //             @DexIgnore
    //             public final Object invoke(Object obj, Object obj2) {
    //                 return ((C0065a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //             }
    //
    //             @DexIgnore
    //             public final Object invokeSuspend(Object obj) {
    //                 ff6.a();
    //                 if (this.label == 0) {
    //                     nc6.a(obj);
    //                     this.this$0.this$0.this$0.n.b(this.this$0.this$0.$watchFaceWrapper);
    //                     this.this$0.this$0.this$0.n.a();
    //                     return cd6.a;
    //                 }
    //                 throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //             }
    //         }
    //
    //         @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    //         @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$removePhotoWatchFace$1$1$3", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
    //         public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //             @DexIgnore
    //             public int label;
    //             @DexIgnore
    //             public il6 p$;
    //             @DexIgnore
    //             public /* final */ /* synthetic */ a this$0;
    //
    //             @DexIgnore
    //             /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //             public b(a aVar, xe6 xe6) {
    //                 super(2, xe6);
    //                 this.this$0 = aVar;
    //             }
    //
    //             @DexIgnore
    //             public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //                 wg6.b(xe6, "completion");
    //                 b bVar = new b(this.this$0, xe6);
    //                 bVar.p$ = (il6) obj;
    //                 return bVar;
    //             }
    //
    //             @DexIgnore
    //             public final Object invoke(Object obj, Object obj2) {
    //                 return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //             }
    //
    //             @DexIgnore
    //             public final Object invokeSuspend(Object obj) {
    //                 ff6.a();
    //                 if (this.label == 0) {
    //                     nc6.a(obj);
    //                     this.this$0.this$0.this$0.n.a();
    //                     return cd6.a;
    //                 }
    //                 throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //             }
    //         }
    //
    //         @DexIgnore
    //         /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //         public a(e eVar, String str, xe6 xe6) {
    //             super(2, xe6);
    //             this.this$0 = eVar;
    //             this.$id = str;
    //         }
    //
    //         @DexIgnore
    //         public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //             wg6.b(xe6, "completion");
    //             a aVar = new a(this.this$0, this.$id, xe6);
    //             aVar.p$ = (il6) obj;
    //             return aVar;
    //         }
    //
    //         @DexIgnore
    //         public final Object invoke(Object obj, Object obj2) {
    //             return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //         }
    //
    //         @DexIgnore
    //         /* JADX WARNING: type inference failed for: r9v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    //         public final Object invokeSuspend(Object obj) {
    //             il6 il6;
    //             List<WatchFace> list;
    //             String str;
    //             Object a = ff6.a();
    //             int i = this.label;
    //             if (i == 0) {
    //                 nc6.a(obj);
    //                 il6 = this.p$;
    //                 this.this$0.this$0.i = this.$id;
    //                 str = PortfolioApp.get.instance().e();
    //                 list = this.this$0.this$0.o.getWatchFacesWithType(str, ai4.BACKGROUND);
    //                 if (!list.isEmpty()) {
    //                     String id = ((WatchFace) yd6.d(list)).getId();
    //                     List<DianaPreset> allPresets = this.this$0.this$0.p.getAllPresets(str);
    //                     fh6 fh6 = new fh6();
    //                     fh6.element = false;
    //                     for (DianaPreset next : allPresets) {
    //                         if (wg6.a((Object) next.getWatchFaceId(), (Object) this.$id)) {
    //                             fh6.element = true;
    //                             next.setWatchFaceId(id);
    //                         }
    //                     }
    //                     if (fh6.element) {
    //                         DianaPresetRepository f = this.this$0.this$0.p;
    //                         this.L$0 = il6;
    //                         this.L$1 = str;
    //                         this.L$2 = list;
    //                         this.L$3 = id;
    //                         this.L$4 = allPresets;
    //                         this.L$5 = fh6;
    //                         this.label = 1;
    //                         if (f.upsertPresetList(allPresets, this) == a) {
    //                             return a;
    //                         }
    //                     }
    //                 }
    //             } else if (i == 1) {
    //                 fh6 fh62 = (fh6) this.L$5;
    //                 List list2 = (List) this.L$4;
    //                 String str2 = (String) this.L$3;
    //                 List<WatchFace> list3 = (List) this.L$2;
    //                 String str3 = (String) this.L$1;
    //                 il6 = (il6) this.L$0;
    //                 try {
    //                     nc6.a(obj);
    //                     list = list3;
    //                     str = str3;
    //                 } catch (Exception e) {
    //                     e = e;
    //                     cn6 h = this.this$0.this$0.d();
    //                     b bVar = new b(this, (xe6) null);
    //                     this.L$0 = il6;
    //                     this.L$1 = e;
    //                     this.label = 3;
    //                     if (gk6.a(h, bVar, this) == a) {
    //                         return a;
    //                     }
    //                     FLogger.INSTANCE.getLocal().e("CustomizeThemePresenter", e.getMessage());
    //                     e.printStackTrace();
    //                     return cd6.a;
    //                 }
    //             } else if (i == 2) {
    //                 String str4 = (String) this.L$5;
    //                 String str5 = (String) this.L$4;
    //                 String str6 = (String) this.L$3;
    //                 List list4 = (List) this.L$2;
    //                 String str7 = (String) this.L$1;
    //                 il6 il62 = (il6) this.L$0;
    //                 try {
    //                     nc6.a(obj);
    //                 } catch (Exception e2) {
    //                     e = e2;
    //                     il6 = il62;
    //                 }
    //                 return cd6.a;
    //             } else if (i == 3) {
    //                 e = (Exception) this.L$1;
    //                 il6 il63 = (il6) this.L$0;
    //                 nc6.a(obj);
    //                 FLogger.INSTANCE.getLocal().e("CustomizeThemePresenter", e.getMessage());
    //                 e.printStackTrace();
    //                 return cd6.a;
    //             } else {
    //                 throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //             }
    //             this.this$0.this$0.o.deleteWatchFace(this.$id, ai4.PHOTO);
    //             StringBuilder sb = new StringBuilder();
    //             Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
    //             wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
    //             sb.append(applicationContext.getFilesDir());
    //             sb.append(File.separator);
    //             String sb2 = sb.toString();
    //             String str8 = this.$id + Constants.PHOTO_IMAGE_NAME_SUFFIX;
    //             String str9 = this.$id + Constants.PHOTO_BINARY_NAME_SUFFIX;
    //             this.this$0.this$0.o.deleteBackgroundPhoto(sb2, str8, str9);
    //             cn6 h2 = this.this$0.this$0.d();
    //             C0065a aVar = new C0065a(this, (xe6) null);
    //             this.L$0 = il6;
    //             this.L$1 = str;
    //             this.L$2 = list;
    //             this.L$3 = sb2;
    //             this.L$4 = str8;
    //             this.L$5 = str9;
    //             this.label = 2;
    //             if (gk6.a(h2, aVar, this) == a) {
    //                 return a;
    //             }
    //             return cd6.a;
    //         }
    //     }
    //
    //     @DexIgnore
    //     /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //     public e(CustomizeThemePresenter customizeThemePresenter, WatchFaceWrapper watchFaceWrapper, xe6 xe6) {
    //         super(2, xe6);
    //         this.this$0 = customizeThemePresenter;
    //         this.$watchFaceWrapper = watchFaceWrapper;
    //     }
    //
    //     @DexIgnore
    //     public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //         wg6.b(xe6, "completion");
    //         e eVar = new e(this.this$0, this.$watchFaceWrapper, xe6);
    //         eVar.p$ = (il6) obj;
    //         return eVar;
    //     }
    //
    //     @DexIgnore
    //     public final Object invoke(Object obj, Object obj2) {
    //         return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //     }
    //
    //     @DexIgnore
    //     public final Object invokeSuspend(Object obj) {
    //         Object a2 = ff6.a();
    //         int i = this.label;
    //         if (i == 0) {
    //             nc6.a(obj);
    //             il6 il6 = this.p$;
    //             this.this$0.n.b();
    //             String id = this.$watchFaceWrapper.getId();
    //             dl6 d = this.this$0.c();
    //             a aVar = new a(this, id, (xe6) null);
    //             this.L$0 = il6;
    //             this.L$1 = id;
    //             this.label = 1;
    //             if (gk6.a(d, aVar, this) == a2) {
    //                 return a2;
    //             }
    //         } else if (i == 1) {
    //             String str = (String) this.L$1;
    //             il6 il62 = (il6) this.L$0;
    //             nc6.a(obj);
    //         } else {
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //         return cd6.a;
    //     }
    // }
    //
    // @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    // public static final class f<T> implements ld<List<? extends WatchFace>> {
    //     @DexIgnore
    //     public /* final */ /* synthetic */ CustomizeThemePresenter a;
    //
    //     @DexIgnore
    //     public f(CustomizeThemePresenter customizeThemePresenter) {
    //         this.a = customizeThemePresenter;
    //     }
    //
    //     @DexIgnore
    //     /* renamed from: a */
    //     public final void onChanged(List<WatchFace> list) {
    //         ILocalFLogger local = FLogger.INSTANCE.getLocal();
    //         local.d("CustomizeThemePresenter", "loadWatchFace " + list);
    //         CustomizeThemePresenter customizeThemePresenter = this.a;
    //         wg6.a((Object) list, "it");
    //         customizeThemePresenter.b(list);
    //         this.a.n.A(true);
    //     }
    // }
    //
    // @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    // @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$showCurrentWatchFace$1", f = "CustomizeThemePresenter.kt", l = {148, 149}, m = "invokeSuspend")
    // public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //     @DexIgnore
    //     public /* final */ /* synthetic */ List $watchFaceList;
    //     @DexIgnore
    //     public Object L$0;
    //     @DexIgnore
    //     public Object L$1;
    //     @DexIgnore
    //     public Object L$2;
    //     @DexIgnore
    //     public Object L$3;
    //     @DexIgnore
    //     public int label;
    //     @DexIgnore
    //     public il6 p$;
    //     @DexIgnore
    //     public /* final */ /* synthetic */ CustomizeThemePresenter this$0;
    //
    //     @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    //     @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$showCurrentWatchFace$1$backgroundWrappers$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
    //     public static final class a extends sf6 implements ig6<il6, xe6<? super List<? extends WatchFaceWrapper>>, Object> {
    //         @DexIgnore
    //         public /* final */ /* synthetic */ List $backgroundList;
    //         @DexIgnore
    //         public int label;
    //         @DexIgnore
    //         public il6 p$;
    //         @DexIgnore
    //         public /* final */ /* synthetic */ g this$0;
    //
    //         @DexIgnore
    //         /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //         public a(g gVar, List list, xe6 xe6) {
    //             super(2, xe6);
    //             this.this$0 = gVar;
    //             this.$backgroundList = list;
    //         }
    //
    //         @DexIgnore
    //         public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //             wg6.b(xe6, "completion");
    //             a aVar = new a(this.this$0, this.$backgroundList, xe6);
    //             aVar.p$ = (il6) obj;
    //             return aVar;
    //         }
    //
    //         @DexIgnore
    //         public final Object invoke(Object obj, Object obj2) {
    //             return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //         }
    //
    //         @DexIgnore
    //         public final Object invokeSuspend(Object obj) {
    //             ff6.a();
    //             if (this.label == 0) {
    //                 nc6.a(obj);
    //                 List list = this.$backgroundList;
    //                 DianaPreset dianaPreset = (DianaPreset) CustomizeThemePresenter.e(this.this$0.this$0).a().a();
    //                 return WatchFaceHelper.a(list, dianaPreset != null ? dianaPreset.getComplications() : null);
    //             }
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //     }
    //
    //     @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    //     @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$showCurrentWatchFace$1$photoWrappers$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
    //     public static final class b extends sf6 implements ig6<il6, xe6<? super List<? extends WatchFaceWrapper>>, Object> {
    //         @DexIgnore
    //         public /* final */ /* synthetic */ List $photoList;
    //         @DexIgnore
    //         public int label;
    //         @DexIgnore
    //         public il6 p$;
    //         @DexIgnore
    //         public /* final */ /* synthetic */ g this$0;
    //
    //         @DexIgnore
    //         /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //         public b(g gVar, List list, xe6 xe6) {
    //             super(2, xe6);
    //             this.this$0 = gVar;
    //             this.$photoList = list;
    //         }
    //
    //         @DexIgnore
    //         public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //             wg6.b(xe6, "completion");
    //             b bVar = new b(this.this$0, this.$photoList, xe6);
    //             bVar.p$ = (il6) obj;
    //             return bVar;
    //         }
    //
    //         @DexIgnore
    //         public final Object invoke(Object obj, Object obj2) {
    //             return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //         }
    //
    //         @DexIgnore
    //         public final Object invokeSuspend(Object obj) {
    //             ff6.a();
    //             if (this.label == 0) {
    //                 nc6.a(obj);
    //                 List list = this.$photoList;
    //                 DianaPreset dianaPreset = (DianaPreset) CustomizeThemePresenter.e(this.this$0.this$0).a().a();
    //                 return WatchFaceHelper.a(list, dianaPreset != null ? dianaPreset.getComplications() : null);
    //             }
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //     }
    //
    //     @DexIgnore
    //     /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //     public g(CustomizeThemePresenter customizeThemePresenter, List list, xe6 xe6) {
    //         super(2, xe6);
    //         this.this$0 = customizeThemePresenter;
    //         this.$watchFaceList = list;
    //     }
    //
    //     @DexIgnore
    //     public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //         wg6.b(xe6, "completion");
    //         g gVar = new g(this.this$0, this.$watchFaceList, xe6);
    //         gVar.p$ = (il6) obj;
    //         return gVar;
    //     }
    //
    //     @DexIgnore
    //     public final Object invoke(Object obj, Object obj2) {
    //         return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //     }
    //
    //     @DexIgnore
    //     /* JADX WARNING: Removed duplicated region for block: B:59:0x01bd  */
    //     /* JADX WARNING: Removed duplicated region for block: B:65:0x01e3  */
    //     public final Object invokeSuspend(Object obj) {
    //         List list;
    //         WatchFaceWrapper e;
    //         il6 il6;
    //         List list2;
    //         List list3;
    //         boolean z;
    //         String watchFaceId;
    //         Object a2 = ff6.a();
    //         int i = this.label;
    //         if (i == 0) {
    //             nc6.a(obj);
    //             il6 = this.p$;
    //             ILocalFLogger local = FLogger.INSTANCE.getLocal();
    //             local.d("CustomizeThemePresenter", "showCurrentWatchFace cacheList " + this.this$0.g + " updatelist " + this.$watchFaceList);
    //             if (wg6.a((Object) this.this$0.g, (Object) this.$watchFaceList)) {
    //                 return cd6.a;
    //             }
    //             List list4 = this.$watchFaceList;
    //             list3 = new ArrayList();
    //             Iterator it = list4.iterator();
    //             while (true) {
    //                 boolean z2 = false;
    //                 if (!it.hasNext()) {
    //                     break;
    //                 }
    //                 Object next = it.next();
    //                 if (((WatchFace) next).getWatchFaceType() == ai4.PHOTO.getValue()) {
    //                     z2 = true;
    //                 }
    //                 if (hf6.a(z2).booleanValue()) {
    //                     list3.add(next);
    //                 }
    //             }
    //             if (list3.size() >= this.this$0.h) {
    //                 z = true;
    //             } else if (list3.isEmpty()) {
    //                 DianaPreset a3 = this.this$0.j;
    //                 if (!(a3 == null || (watchFaceId = a3.getWatchFaceId()) == null)) {
    //                     this.this$0.a(watchFaceId);
    //                 }
    //                 this.this$0.g = this.$watchFaceList;
    //                 return cd6.a;
    //             } else {
    //                 String c = this.this$0.i;
    //                 DianaPreset dianaPreset = (DianaPreset) CustomizeThemePresenter.e(this.this$0).a().a();
    //                 z = wg6.a((Object) c, (Object) dianaPreset != null ? dianaPreset.getWatchFaceId() : null);
    //             }
    //             this.this$0.h = list3.size();
    //             if (z) {
    //                 FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "showCurrentWatchFace() needUpdate == true");
    //                 List list5 = this.$watchFaceList;
    //                 list2 = new ArrayList();
    //                 for (Object next2 : list5) {
    //                     if (hf6.a(((WatchFace) next2).getWatchFaceType() == ai4.BACKGROUND.getValue()).booleanValue()) {
    //                         list2.add(next2);
    //                     }
    //                 }
    //                 dl6 d = this.this$0.c();
    //                 a aVar = new a(this, list2, (xe6) null);
    //                 this.L$0 = il6;
    //                 this.L$1 = list3;
    //                 this.L$2 = list2;
    //                 this.label = 1;
    //                 obj = gk6.a(d, aVar, this);
    //                 if (obj == a2) {
    //                     return a2;
    //                 }
    //             }
    //             return cd6.a;
    //         } else if (i == 1) {
    //             il6 = (il6) this.L$0;
    //             nc6.a(obj);
    //             List list6 = (List) this.L$1;
    //             list2 = (List) this.L$2;
    //             list3 = list6;
    //         } else if (i == 2) {
    //             list = (List) this.L$3;
    //             List list7 = (List) this.L$2;
    //             List list8 = (List) this.L$1;
    //             il6 il62 = (il6) this.L$0;
    //             nc6.a(obj);
    //             List list9 = (List) obj;
    //             this.this$0.a((List<WatchFaceWrapper>) list);
    //             this.this$0.n.a((List<WatchFaceWrapper>) list, ai4.BACKGROUND);
    //             this.this$0.n.a((List<WatchFaceWrapper>) list9, ai4.PHOTO);
    //             if (this.this$0.g.size() > this.$watchFaceList.size()) {
    //                 String str = (String) CustomizeThemePresenter.e(this.this$0).g().a();
    //                 if (!(str == null || (e = CustomizeThemePresenter.e(this.this$0).e(str)) == null)) {
    //                     this.this$0.n.a(str, e.getType());
    //                     this.this$0.a(e.getType());
    //                 }
    //             } else if (!list9.isEmpty()) {
    //                 this.this$0.c((WatchFaceWrapper) yd6.f(list9));
    //             } else if (!list.isEmpty()) {
    //                 this.this$0.c((WatchFaceWrapper) yd6.f(list));
    //             }
    //             this.this$0.g = this.$watchFaceList;
    //             return cd6.a;
    //         } else {
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //         List list10 = (List) obj;
    //         dl6 d2 = this.this$0.c();
    //         b bVar = new b(this, list3, (xe6) null);
    //         this.L$0 = il6;
    //         this.L$1 = list3;
    //         this.L$2 = list2;
    //         this.L$3 = list10;
    //         this.label = 2;
    //         Object a4 = gk6.a(d2, bVar, this);
    //         if (a4 == a2) {
    //             return a2;
    //         }
    //         list = list10;
    //         obj = a4;
    //         List list92 = (List) obj;
    //         this.this$0.a((List<WatchFaceWrapper>) list);
    //         this.this$0.n.a((List<WatchFaceWrapper>) list, ai4.BACKGROUND);
    //         this.this$0.n.a((List<WatchFaceWrapper>) list92, ai4.PHOTO);
    //         if (this.this$0.g.size() > this.$watchFaceList.size()) {
    //         }
    //         this.this$0.g = this.$watchFaceList;
    //         return cd6.a;
    //     }
    // }

    // @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    // public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //     @DexIgnore
    //     public /* final */ /* synthetic */ DianaPreset $currentPreset;
    //     @DexIgnore
    //     public Object L$0;
    //     @DexIgnore
    //     public Object L$1;
    //     @DexIgnore
    //     public Object L$2;
    //     @DexIgnore
    //     public int label;
    //     @DexIgnore
    //     public il6 p$;
    //     @DexIgnore
    //     public /* final */ /* synthetic */ CustomizeThemePresenter this$0;
    //
    //     @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    //     public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //         @DexIgnore
    //         public /* final */ /* synthetic */ WatchFace $activeWatchFace;
    //         @DexIgnore
    //         public int label;
    //         @DexIgnore
    //         public il6 p$;
    //         @DexIgnore
    //         public /* final */ /* synthetic */ h this$0;
    //
    //         @DexIgnore
    //         /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //         public a(h hVar, WatchFace watchFace, xe6 xe6) {
    //             super(2, xe6);
    //             this.this$0 = hVar;
    //             this.$activeWatchFace = watchFace;
    //         }
    //
    //         @DexIgnore
    //         public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //             wg6.b(xe6, "completion");
    //             a aVar = new a(this.this$0, this.$activeWatchFace, xe6);
    //             aVar.p$ = (il6) obj;
    //             return aVar;
    //         }
    //
    //         @DexIgnore
    //         public final Object invoke(Object obj, Object obj2) {
    //             return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //         }
    //
    //         @DexIgnore
    //         public final Object invokeSuspend(Object obj) {
    //             ff6.a();
    //             if (this.label == 0) {
    //                 nc6.a(obj);
    //                 WatchFace watchFace = this.$activeWatchFace;
    //                 if (watchFace == null || watchFace.getWatchFaceType() != ai4.PHOTO.getValue()) {
    //                     this.this$0.this$0.n.M0();
    //                 } else {
    //                     this.this$0.this$0.n.R0();
    //                 }
    //                 return cd6.a;
    //             }
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //     }
    //
    //     @DexIgnore
    //     /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //     public h(DianaPreset dianaPreset, xe6 xe6, CustomizeThemePresenter customizeThemePresenter) {
    //         super(2, xe6);
    //         this.$currentPreset = dianaPreset;
    //         this.this$0 = customizeThemePresenter;
    //     }
    //
    //     @DexIgnore
    //     public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //         wg6.b(xe6, "completion");
    //         h hVar = new h(this.$currentPreset, xe6, this.this$0);
    //         hVar.p$ = (il6) obj;
    //         return hVar;
    //     }
    //
    //     @DexIgnore
    //     public final Object invoke(Object obj, Object obj2) {
    //         return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //     }
    //
    //     @DexIgnore
    //     public final Object invokeSuspend(Object obj) {
    //         Object a2 = ff6.a();
    //         int i = this.label;
    //         if (i == 0) {
    //             nc6.a(obj);
    //             il6 il6 = this.p$;
    //             CustomizeThemePresenter customizeThemePresenter = this.this$0;
    //             customizeThemePresenter.j = customizeThemePresenter.p.getActivePresetBySerial(PortfolioApp.get.instance().e());
    //             String watchFaceId = this.$currentPreset.getWatchFaceId();
    //             WatchFace watchFaceWithId = this.this$0.o.getWatchFaceWithId(watchFaceId);
    //             ILocalFLogger local = FLogger.INSTANCE.getLocal();
    //             local.d("CustomizeThemePresenter", "currentPreset observer activeWatchFace " + watchFaceWithId);
    //             cn6 c = zl6.c();
    //             a aVar = new a(this, watchFaceWithId, (xe6) null);
    //             this.L$0 = il6;
    //             this.L$1 = watchFaceId;
    //             this.L$2 = watchFaceWithId;
    //             this.label = 1;
    //             if (gk6.a(c, aVar, this) == a2) {
    //                 return a2;
    //             }
    //         } else if (i == 1) {
    //             WatchFace watchFace = (WatchFace) this.L$2;
    //             String str = (String) this.L$1;
    //             il6 il62 = (il6) this.L$0;
    //             nc6.a(obj);
    //         } else {
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //         return cd6.a;
    //     }
    // }
    //
    // @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    // @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$updateSelectedPhotoWatchFaces$1", f = "CustomizeThemePresenter.kt", l = {106, 108}, m = "invokeSuspend")
    // public static final class i extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    //     @DexIgnore
    //     public /* final */ /* synthetic */ String $watchFaceId;
    //     @DexIgnore
    //     public Object L$0;
    //     @DexIgnore
    //     public Object L$1;
    //     @DexIgnore
    //     public Object L$2;
    //     @DexIgnore
    //     public int label;
    //     @DexIgnore
    //     public il6 p$;
    //     @DexIgnore
    //     public /* final */ /* synthetic */ CustomizeThemePresenter this$0;
    //
    //     @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    //     public static final class a extends sf6 implements ig6<il6, xe6<? super WatchFaceWrapper>, Object> {
    //         @DexIgnore
    //         public /* final */ /* synthetic */ WatchFace $it;
    //         @DexIgnore
    //         public int label;
    //         @DexIgnore
    //         public il6 p$;
    //         @DexIgnore
    //         public /* final */ /* synthetic */ i this$0;
    //
    //         @DexIgnore
    //         /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //         public a(WatchFace watchFace, xe6 xe6, i iVar) {
    //             super(2, xe6);
    //             this.$it = watchFace;
    //             this.this$0 = iVar;
    //         }
    //
    //         @DexIgnore
    //         public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //             wg6.b(xe6, "completion");
    //             a aVar = new a(this.$it, xe6, this.this$0);
    //             aVar.p$ = (il6) obj;
    //             return aVar;
    //         }
    //
    //         @DexIgnore
    //         public final Object invoke(Object obj, Object obj2) {
    //             return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //         }
    //
    //         @DexIgnore
    //         public final Object invokeSuspend(Object obj) {
    //             ff6.a();
    //             if (this.label == 0) {
    //                 nc6.a(obj);
    //                 WatchFace watchFace = this.$it;
    //                 DianaPreset dianaPreset = (DianaPreset) CustomizeThemePresenter.e(this.this$0.this$0).a().a();
    //                 return WatchFaceHelper.b(watchFace, dianaPreset != null ? dianaPreset.getComplications() : null);
    //             }
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //     }
    //
    //     @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    //     @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter$updateSelectedPhotoWatchFaces$1$watchFace$1", f = "CustomizeThemePresenter.kt", l = {}, m = "invokeSuspend")
    //     public static final class b extends sf6 implements ig6<il6, xe6<? super WatchFace>, Object> {
    //         @DexIgnore
    //         public int label;
    //         @DexIgnore
    //         public il6 p$;
    //         @DexIgnore
    //         public /* final */ /* synthetic */ i this$0;
    //
    //         @DexIgnore
    //         /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //         public b(i iVar, xe6 xe6) {
    //             super(2, xe6);
    //             this.this$0 = iVar;
    //         }
    //
    //         @DexIgnore
    //         public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //             wg6.b(xe6, "completion");
    //             b bVar = new b(this.this$0, xe6);
    //             bVar.p$ = (il6) obj;
    //             return bVar;
    //         }
    //
    //         @DexIgnore
    //         public final Object invoke(Object obj, Object obj2) {
    //             return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //         }
    //
    //         @DexIgnore
    //         public final Object invokeSuspend(Object obj) {
    //             ff6.a();
    //             if (this.label == 0) {
    //                 nc6.a(obj);
    //                 return this.this$0.this$0.o.getWatchFaceWithId(this.this$0.$watchFaceId);
    //             }
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //     }
    //
    //     @DexIgnore
    //     /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    //     public i(CustomizeThemePresenter customizeThemePresenter, String str, xe6 xe6) {
    //         super(2, xe6);
    //         this.this$0 = customizeThemePresenter;
    //         this.$watchFaceId = str;
    //     }
    //
    //     @DexIgnore
    //     public final xe6<cd6> create(Object obj, xe6<?> xe6) {
    //         wg6.b(xe6, "completion");
    //         i iVar = new i(this.this$0, this.$watchFaceId, xe6);
    //         iVar.p$ = (il6) obj;
    //         return iVar;
    //     }
    //
    //     @DexIgnore
    //     public final Object invoke(Object obj, Object obj2) {
    //         return ((i) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    //     }
    //
    //     @DexIgnore
    //     /* JADX WARNING: Removed duplicated region for block: B:20:0x0090  */
    //     public final Object invokeSuspend(Object obj) {
    //         WatchFaceWrapper watchFaceWrapper;
    //         il6 il6;
    //         Object a2 = ff6.a();
    //         int i = this.label;
    //         if (i == 0) {
    //             nc6.a(obj);
    //             il6 = this.p$;
    //             if (!wg6.a((Object) this.this$0.k, (Object) this.$watchFaceId)) {
    //                 this.this$0.k = this.$watchFaceId;
    //                 dl6 d = this.this$0.c();
    //                 b bVar = new b(this, (xe6) null);
    //                 this.L$0 = il6;
    //                 this.label = 1;
    //                 obj = gk6.a(d, bVar, this);
    //                 if (obj == a2) {
    //                     return a2;
    //                 }
    //             }
    //             return cd6.a;
    //         } else if (i == 1) {
    //             il6 = (il6) this.L$0;
    //             nc6.a(obj);
    //         } else if (i == 2) {
    //             WatchFace watchFace = (WatchFace) this.L$2;
    //             WatchFace watchFace2 = (WatchFace) this.L$1;
    //             il6 il62 = (il6) this.L$0;
    //             nc6.a(obj);
    //             watchFaceWrapper = (WatchFaceWrapper) obj;
    //             if (!CustomizeThemePresenter.e(this.this$0).j().contains(watchFaceWrapper)) {
    //                 CustomizeThemePresenter.e(this.this$0).j().add(watchFaceWrapper);
    //             }
    //             this.this$0.c(watchFaceWrapper);
    //             return cd6.a;
    //         } else {
    //             throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    //         }
    //         WatchFace watchFace3 = (WatchFace) obj;
    //         if (watchFace3 != null) {
    //             dl6 d2 = this.this$0.c();
    //             a aVar = new a(watchFace3, (xe6) null, this);
    //             this.L$0 = il6;
    //             this.L$1 = watchFace3;
    //             this.L$2 = watchFace3;
    //             this.label = 2;
    //             obj = gk6.a(d2, aVar, this);
    //             if (obj == a2) {
    //                 return a2;
    //             }
    //             watchFaceWrapper = (WatchFaceWrapper) obj;
    //             if (!CustomizeThemePresenter.e(this.this$0).j().contains(watchFaceWrapper)) {
    //             }
    //             this.this$0.c(watchFaceWrapper);
    //         }
    //         return cd6.a;
    //     }
    // }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public CustomizeThemePresenter(r65 r65, WatchFaceRepository watchFaceRepository, DianaPresetRepository dianaPresetRepository) {
        wg6.b(r65, "mView");
        wg6.b(watchFaceRepository, "watchFaceRepository");
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        this.n = r65;
        this.o = watchFaceRepository;
        this.p = dianaPresetRepository;
    }

    @DexIgnore
    public static final /* synthetic */ DianaCustomizeViewModel e(CustomizeThemePresenter customizeThemePresenter) {
        DianaCustomizeViewModel dianaCustomizeViewModel = customizeThemePresenter.e;
        if (dianaCustomizeViewModel != null) {
            return dianaCustomizeViewModel;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "onStart");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            MutableLiveData<String> g2 = dianaCustomizeViewModel.g();
            r65 r65 = this.n;
            if (r65 != null) {
                g2.a((CustomizeThemeFragment) r65, this.l);
                this.f = this.o.getWatchFacesLiveDataWithSerial(PortfolioApp.get.instance().e());
                LiveData<List<WatchFace>> liveData = this.f;
                if (liveData != null) {
                    throw null;
                    // liveData.a(this.n, this.m);
                }
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.e;
                if (dianaCustomizeViewModel2 != null) {
                    DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel2.a().a();
                    if (dianaPreset != null) {
                        throw null;
                        // rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new h(dianaPreset, (xe6) null, this), 3, (Object) null);
                        // return;
                    }
                    return;
                }
                wg6.d("mDianaCustomizeViewModel");
                throw null;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment");
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "stop()");
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.g().b(this.l);
            LiveData<List<WatchFace>> liveData = this.f;
            if (liveData != null) {
                liveData.b(this.m);
                return;
            }
            return;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexReplace
    public void h() {
        ArrayList arrayList;
        List list;
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "checkAddingPhoto()");
        LiveData<List<WatchFace>> liveData = this.f;
        if (liveData == null || (list = liveData.a()) == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList();
            for (Object next : list) {
                if (((WatchFace) next).getWatchFaceType() == ai4.PHOTO.getValue()) {
                    arrayList.add(next);
                }
            }
        }
        /* Remove the 20 watch-face limit */
        // if (arrayList == null || arrayList.size() != 20) {
            this.n.P0();
        // } else {
        //     this.n.h0();
        // }
    }

    @DexIgnore
    public void i() {
        this.n.a(this);
    }

    @DexIgnore
    public void c(WatchFaceWrapper watchFaceWrapper) {
        wg6.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "setWatchFace " + watchFaceWrapper);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            DianaPreset dianaPreset = (DianaPreset) dianaCustomizeViewModel.a().a();
            if (dianaPreset != null) {
                dianaPreset.setWatchFaceId(watchFaceWrapper.getId());
                wg6.a((Object) dianaPreset, "currentPreset");
                a(dianaPreset);
                return;
            }
            return;
        }
        wg6.d("mDianaCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final void b(List<WatchFace> list) {
        throw null;
        // rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new g(this, list, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void b(WatchFaceWrapper watchFaceWrapper) {
        wg6.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "removePhotoWatchFace, watchFaceWrapper = " + watchFaceWrapper);
        throw null;
        // rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, watchFaceWrapper, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "updateCurrentPreset=" + dianaPreset);
        DianaCustomizeViewModel dianaCustomizeViewModel = this.e;
        if (dianaCustomizeViewModel != null) {
            dianaCustomizeViewModel.a(dianaPreset);
        } else {
            wg6.d("mDianaCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "watchFaceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "updateSelectedPhotoWatchFaces - watchFaceId=" + str);
        throw null;
        // rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new i(this, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(DianaCustomizeViewModel dianaCustomizeViewModel) {
        wg6.b(dianaCustomizeViewModel, "viewModel");
        this.e = dianaCustomizeViewModel;
    }

    @DexIgnore
    public void a(WatchFaceWrapper watchFaceWrapper) {
        wg6.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemePresenter", "checkToRemovePhoto watchFaceWrapper = " + watchFaceWrapper);
        throw null;
        // rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, watchFaceWrapper, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset, WatchFaceWrapper watchFaceWrapper) {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "showRemovePhotoDialog()");
        throw null;
        // if (wg6.a((Object) dianaPreset != null ? dianaPreset.getWatchFaceId() : null, (Object) watchFaceWrapper.getId())) {
        //     this.n.t0();
        // } else {
        //     this.n.J0();
        // }
    }

    @DexIgnore
    public final void a(ai4 ai4) {
        FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "navigateToActiveWatchFace()");
        String str = this.i;
        throw null;
        // if (!(str == null || xj6.a(str))) {
        //     this.i = null;
        //     this.n.R0();
        // } else if (ai4 == ai4.BACKGROUND) {
        //     this.n.M0();
        // } else if (ai4 == ai4.PHOTO) {
        //     this.n.R0();
        // }
    }

    @DexIgnore
    public final void a(List<WatchFaceWrapper> list) {
        boolean z = false;
        for (WatchFaceWrapper watchFaceWrapper : list) {
            if (watchFaceWrapper.getBackground() != null || watchFaceWrapper.getCombination() == null || watchFaceWrapper.getTopComplication() == null || watchFaceWrapper.getBottomComplication() == null || watchFaceWrapper.getLeftComplication() == null || watchFaceWrapper.getRightComplication() == null) {
                z = true;
            }
        }
        if (z) {
            FLogger.INSTANCE.getLocal().d("CustomizeThemePresenter", "Some watchface files are missing, retry download for those files");
            throw null;
            // rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
        }
    }
}
