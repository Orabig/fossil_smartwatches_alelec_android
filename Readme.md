# Fossil Smartwatches alelec

This project provides a patched version of the Fossil Smartwatches android phone app to support Fossil's hybrid smart watch range.

Currently based on the v4.3.0 release of the Fossil App, this project adds:
* Support setting alarms from Google Assistant.
* Android "Do Not Disturb" is honored.
* Convenient access to unique key for GadgetBridge
* Support for notifications with no text/message content.
* Support for correct notification icons for all apps.

Other than this the app should look and behave exactly the same as the official one.

The notifications for each app can be turned on and off the same way as the original app settings page.
The correct app icon should come through to your watch, however a generic icon often shows for the very first
notification for each app before the correct one is transferred to your watch for subsequent notifications.

#Installation

As mentioned, this is completely unofficial, and slightly annoying to install.
My patched app cannot be installed at the same time as the official one, so you'll need to uninstall it first (which wipes all your settings).

If you have Titanium Backup, make a backup of the official one first, then uninstall it.

My app is available to download as an apk from https://gitlab.com/alelec/fossil_smartwatches_alelec_android/-/releases

You should be able to install the apk directly from the download, though your phone will probably
warn you about installing from an unofficial source, which you'll need to allow to install the app.

If you're on android 10 and / or using chrome it'll generally work against you when it comes to
downloading an apk, this might help: https://www.reddit.com/r/android_beta/comments/d0wvi3/android_10can_install_apk_that_are_not_from/

Sometimes it also helps to hold down on the link to get the popup menu, then select "Download link".

Now if you made a titanium backup beforehand, you can now restore data only of the fossil app and
it'll continue to work with all your previous settings in place.

# Upgrading the base app

Download new apk and put it in the `orig` folder.

Delete the existing apk there, as well as any `src` or `Anon` folder

Make sure you've got python installed on your computer with the `poetry` package manager.

run `decompile.bat`

Once that's finished, the src/main/java_decomp folder will be updated with new template decomp files

The mapping.txt file in the root of the directory will need to be examined, the new/matching file 
for each entry in it will need to be found in the new copy. In many cases they wont have changed, 
but some most likely will. 

For each of the file in it, if you check the git diff on the java_decomp folder before and after 
you'll hopefully identify obfuscated files that have changed names, and update mapping.txt to suit.

Once mapping.txt has been updated, delete orig/src and src/main/java_decomp and run decompile.bat again.
 

# Credits

This has all been possible thanks to the ability to decompile android/java and the magic of
[DexPatcher](https://github.com/DexPatcher/dexpatcher-gradle) which allows me to make these kind of
patches relatively quickly and, more importantly, reliably!